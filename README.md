# E-Cubs Reading Club Web Application
This e-commerce site for selling e-books consists of two sections:

* The Customer Site:

Clients may search for, order, review and download e-books. The front page will also show personalized book recommendations based on the previously-browsed books of a user, even if not logged in. If you have any troubles understanding the client-side features, you may visit the help page by clicking the '?' to the right side of the navigation bar.

* The Manager Site:

Managers may access sales reports and may consult, add, edit or delete a number of items from books to orders to survey questions. They may also approve or disapprove reviews, as well as decide which ads or surveys to activate.
It may be accessed by users whose accounts are marked as Managers by selecting the 'Manage' dropdown menu button and the 'Management' option of the navigation bar.

[You may visit our webapp by clicking here.](http://waldo.dawsoncollege.qc.ca:8080/ga1w20)


### Demo Users
* Customer
    
    Login Name: cst.send@gmail.com
    
    Password: dawsoncollege

* Manager 
    
    Login Name: cst.receive@gmail.com
    
    Password: collegedawson

### Technical details
Our project has been developed with JDK 11 and Jakarta 8.0 using Netbeans 11.2 and the Maven build system. 

For the presentation layer, we used Java Server Faces (JSF) 2.0 and also a mix of Primefaces 8.0 and Bootsfaces 1.4.2 libraries. 

For the database and persistence layer, we used MySQL 8 with Eclipse Link JPA 2.7, Javax Persistence 2.1 and the JPA Model Generator 2.7.

For testing, we used Junit 4.13, Arquillian 1.6.0 and Selenium.

The application server used is Payara Server 5.194. 

Our POM file has a parent (version1.1), made by Ken Fogel @omniprof, [click here to see.](https://gitlab.com/omniprof/web_project_dependencies.git)


### Arquillian Testing
If you wish to run the tests, make sure you make set the value of skipTests to false in the POM file:

```xml
   <properties>
        <skipTests>false</skipTests>
    </properties>
```

You may change the level of logs to see more details, by going to src>main>resources>log4j2.xml, and change the level attribute of Root to your desired level.
```xml
    <Loggers>
        <Root level="trace">
            <appender-ref ref="Servlet" />
        </Root>
    </Loggers>
```

### Selenium Testing
Selenium is an automated testing framework that interacts with the website and the browser. 

Most of the test cases this project have will test in a desktop environment, and there are some cases that will test on a mobile environment. The current test browser used is Google Chrome (using ChromeDriver).

Likewise, in order to test with Selenium, the value of skipTests in the POM file must be set to false. On the NetBeans IDE, once your project is compiled, you may right click on the opened project and click on 'Run Selenium Tests' to run the tests.

### Database
You may refer to the ERD of our database in the root folder.

If you wish to recreate the database behind this web app, the MySQL scripts can be found in ga1w20>test>resources.


### List of Known Defects
 
###### Downloading .inf file using Firefox
* When attempting to download an e-book in .inf format from Firefox, the .xht file extension will be automatically appended. We believe this may be due to either the local operating system or Firefox itself failing to recognise .inf as a valid file format.

###### Icons disappearing when switching langage
* If the FR/EN button from the navbar is clicked, the cart icon (going to Cart) and the question mark icon (going to the Help page) will disppear. They will reappear when you go to another page.

###### Warnings

* JSF1091 ```No mime type could be found for file null.```
We have tried mapping all the mime types we *know* we are using, but this warning persists. As it's just a warning, tracking
down why this is has been low on our list of priorities.

* ```Unable to save dynamic action with clientId 'j_id...'  ```
As of the finalization of this project, thoses warnings are a known unsolved
bug in JSF/BootsFaces/Primefaces, visit this page for more information 
: https://github.com/TheCoder4eu/BootsFaces-OSP/issues/325

* ```Component 'j_id..' should be inside a form or should reference a form via its form attribute.```
We are seeing this warning on dynamically-changed fields on the manager site, despite the fact that the manager tables are enclosed in forms.


### Developers
This application has been built for the winter 2020 semester class 420-625 Java Server Side taught by Ken Fogel at Dawson College. 

The team of developers is composed of:
* Timmy-Pierre Keo @timpkko
* Michael Masciotra @MyCoalMasciotra
* Yehoshua Fish @FishYeho
* Eira Garrett  @eironymous
* Camillia Elachqar @celachqar

### A Note for our Teacher
Hi Ken! Here is a note to let you know, we have not completed the ability 
for managers to edit ALL tables in the database. By lack of time, 
we made a team decision to value quality of all our other pages over trying to 
cram buggy pages. The remaining management pages are Author, Format, Publisher,
Genre, and Tax Rates. Note that even if managers do not have the ability to edit 
an Author, Format, Publisher or Genre, they may create new ones in the "Inventory" manager page.
