USE ga1w20;

DELETE FROM banners;

INSERT INTO banners(ad_id,url,active) VALUES
(1,'http://schadenmraz.com/',0),
(2,'http://www.keeling.org/',0),
(3,'http://www.sawayn.com/',0),
(4,'http://www.cassin.com/',0),
(5,'http://www.reillyzemlak.com/',0),
(6,'http://www.wolff.biz/',0),
(7,'http://dickinsonbernhard.com/',0),
(8,'http://brekke.com/',0),
(9,'http://www.hueldare.com/',0),
(10,'http://www.ziemann.com/',0),
(11,'http://www.braunborer.net/',0),
(12,'http://hartmannbailey.com/',0),
(13,'http://schinner.info/',0),
(14,'http://reilly.com/',0),
(15,'http://www.zulauf.com/',0),
(16,'http://kunze.com/',0),
(17,'http://www.barton.com/',0),
(18,'http://www.runolfsdottir.net/',0),
(19,'http://grimeskshlerin.info/',0),
(20,'http://www.christiansen.biz/',1);