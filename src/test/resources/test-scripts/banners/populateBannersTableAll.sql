USE ga1w20;

DELETE FROM banners;

INSERT INTO banners(ad_id,url,active) VALUES
(1,'http://schadenmraz.com/',1),
(2,'http://www.keeling.org/',1),
(3,'http://www.sawayn.com/',1),
(4,'http://www.cassin.com/',1),
(5,'http://www.reillyzemlak.com/',1),
(6,'http://www.wolff.biz/',1),
(7,'http://dickinsonbernhard.com/',1),
(8,'http://brekke.com/',1),
(9,'http://www.hueldare.com/',1),
(10,'http://www.ziemann.com/',1),
(11,'http://www.braunborer.net/',1),
(12,'http://hartmannbailey.com/',1),
(13,'http://schinner.info/',1),
(14,'http://reilly.com/',1),
(15,'http://www.zulauf.com/',1),
(16,'http://kunze.com/',1),
(17,'http://www.barton.com/',1),
(18,'http://www.runolfsdottir.net/',1),
(19,'http://grimeskshlerin.info/',1),
(20,'http://www.christiansen.biz/',1);