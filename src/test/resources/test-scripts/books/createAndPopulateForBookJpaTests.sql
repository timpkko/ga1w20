USE ga1w20;

DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS orderitems;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS bookformat;
DROP TABLE IF EXISTS bookauthor;
DROP TABLE IF EXISTS bookgenre;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS formats;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS credentials;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS publishers;
DROP TABLE IF EXISTS genres;
DROP TABLE IF EXISTS surveys;
DROP TABLE IF EXISTS rssfeeds;
DROP TABLE IF EXISTS taxrates;
DROP TABLE IF EXISTS banners;

-- Create banner ads table
CREATE TABLE banners
(
    ad_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(2083),
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create tax rate table
CREATE TABLE taxrates
(
    region VARCHAR(2) PRIMARY KEY,
    gst_rate DECIMAL(6,3),
    pst_rate DECIMAL(6,3),
    hst_rate DECIMAL(6,3)
);

-- Create RSS feed table
CREATE TABLE rssfeeds
(
    feed_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(2083) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create surveys table:
CREATE TABLE surveys
(
    survey_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    question VARCHAR(400) NOT NULL,
    answer_1 VARCHAR(100) NOT NULL,
    answer_2 VARCHAR(100) NOT NULL,
    answer_3 VARCHAR(100),
    answer_4 VARCHAR(100),
    vote_1 INTEGER DEFAULT 0,
    vote_2 INTEGER DEFAULT 0,
    vote_3 INTEGER DEFAULT 0,
    vote_4 INTEGER DEFAULT 0,
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create genres table:
CREATE TABLE genres
(
    genre_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create publishers table:
CREATE TABLE publishers
(
    publisher_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL
);


-- Create users table:
CREATE TABLE users
(
    user_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(120) UNIQUE NOT NULL,
    title VARCHAR(60),
    lastname VARCHAR(60) NOT NULL,
    firstname VARCHAR(60) NOT NULL,
    companyname VARCHAR(150),
    address1 VARCHAR(300) NOT NULL,
    address2 VARCHAR(300),
    city VARCHAR(60) NOT NULL,
    province VARCHAR(2) NOT NULL,
    country VARCHAR(20) NOT NULL DEFAULT 'CANADA',
    postalcode VARCHAR(7) NOT NULL,
    homephone VARCHAR(20),
    cellphone VARCHAR(20),
    
    FOREIGN KEY (province)
        REFERENCES taxrates(region)
);

-- Create credentials table:
CREATE TABLE credentials
(
    user_id INTEGER PRIMARY KEY,
    hash BINARY(32) NOT NULL,
    salt CHAR(28) NOT NULL,
    manager BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);

-- Create authors table:
CREATE TABLE authors
(
    author_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(60) NOT NULL,
    lastname VARCHAR(60),
    title VARCHAR(20) DEFAULT NULL
);

-- Create books table:
-- FK: publisher_id --> publishers
CREATE TABLE books
(
    isbn BIGINT PRIMARY KEY,
    title VARCHAR(260) NOT NULL,
    publisher_id INTEGER NOT NULL,
    pub_date DATE NOT NULL,
    num_pages INTEGER NOT NULL,
    description text NOT NULL,
    wholesale_price DECIMAL(6,2) NOT NULL,
    list_price DECIMAL(6,2) NOT NULL,
    sale_price DECIMAL(6,2) NOT NULL,
    min_age INTEGER NOT NULL,
    max_age INTEGER,
    acquired_stamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    removed BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (publisher_id)
        REFERENCES publishers(publisher_id)
);

-- Create book-genre bridging table:
-- FK: isbn --> books
-- FK: genre_id --> genres
CREATE TABLE bookgenre
(
    bookgenre_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT,
    genre_id INTEGER NOT NULL,

    FOREIGN KEY (genre_id)
        REFERENCES genres(genre_id),

    FOREIGN KEY (isbn)
        REFERENCES books(isbn)
);

-- Create author-book bridging table:
-- FK: isbn --> books
-- FK: author_id --> authors
CREATE TABLE bookauthor
(
    bookauthor_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    author_id INTEGER NOT NULL,

    FOREIGN KEY (isbn)
        REFERENCES books(isbn),

    FOREIGN KEY (author_id)
        REFERENCES authors(author_id)
);

-- Create formats table:
CREATE TABLE formats
(
    format_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    extension VARCHAR(5) NOT NULL,
    name VARCHAR(60) NOT NULL
);

-- Create book-format bridging table:
-- FK: isbn --> books
-- FK: format_id --> formats
CREATE TABLE bookformat
(
    bookformat_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    format_id INTEGER NOT NULL,
    
    FOREIGN KEY (isbn)
        REFERENCES books(isbn),

    FOREIGN KEY (format_id)
        REFERENCES formats(format_id)
);

-- Create order table:
-- FK: user_id --> users
CREATE TABLE orders
(
    order_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    user_id INTEGER NOT NULL,
    purchase_date DATE NOT NULL,
    gst_rate DECIMAL(6,3),
    pst_rate DECIMAL(6,3),
    hst_rate DECIMAL(6,3),

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);

-- Create orderitem table:
-- FK: order_id --> orders
-- FK: isbn --> books
CREATE TABLE orderitems
(
    orderitems_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    order_id INTEGER NOT NULL,
    isbn BIGINT NOT NULL,
    price DECIMAL(6,2),
    removed BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (order_id)
        REFERENCES orders(order_id),

    FOREIGN KEY (isbn)
        REFERENCES books(isbn)
);


-- Create reviews table:
-- FK: isbn --> books
-- FK: user_id --> users
CREATE TABLE reviews
(
    review_id bigint AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    postdate TIMESTAMP NOT NULL DEFAULT NOW(),
    user_id INTEGER NOT NULL,
    rating INTEGER NOT NULL,
    text VARCHAR(750) NOT NULL,
    approved BOOLEAN DEFAULT FALSE,


    FOREIGN KEY (isbn)
        REFERENCES books(isbn),

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);

INSERT INTO formats(format_id,extension,name) VALUES 
(1, 'cbr', 'Comic Book Archive'),
(2, 'cbz', 'Comic Book Archive'),
(3, 'cb7', 'Comic Book Archive'),
(4, 'cbt', 'Comic Book Archive'),
(5, 'cba', 'Comic Book Archive'),
(6, 'cbr', 'Comic Book Archive'),
(7, 'djvu', 'DjVu'),
(8, 'doc', 'DOC'),
(9, 'docx', 'DOCX'),
(10, 'epub', 'EPUB(IDPF)'),
(11, 'fb2', 'FictionBook'),
(12, 'html', 'HTML'),
(13, 'ibook', 'iBooks'),
(14, 'inf', 'INF'),
(15, 'azw', 'Kindle'),
(16, 'lit', 'Microsoft Reader'),
(17, 'prc', 'Mobipocket'),
(18, 'mobi', 'Mobipocket'),
(19, 'exe', 'Multimedia EBook'),
(20, 'pkg', 'Newton Book'),
(21, 'pdb', 'eReader'),
(22, 'txt', 'Plain text'),
(23, 'pdb', 'Plucker'),
(24, 'pdf', 'Portable Document Format'),
(25, 'ps', 'PostScript'),
(26, 'tr2', 'Tome Raider'),
(27, 'tr3', 'Tome Raider'),
(28, 'oxps', 'OpenXPS'),
(29, 'xps', 'OpenXPS'),
(30, '.', 'Lonely format');

INSERT INTO taxrates(region, gst_rate, pst_rate, hst_rate) VALUES
('AB', 5.000, 0.000, 0.000),
('BC', 5.000, 7.000, 0.000),
('MB', 5.000, 7.000, 0.000),
('NB', 0.000, 0.000, 15.000),
('NL', 0.000, 0.000, 15.000),
('NS', 0.000, 0.000, 15.000),
('NT', 5.000, 0.000, 0.000),
('NU', 5.000, 0.000, 0.000),
('ON', 0.000, 0.000, 13.000),
('PE', 0.000, 0.000, 15.000),
('QC', 5.000, 9.975, 0.000),
('SK', 5.000, 6.000, 0.000),
('YT', 5.000, 0.000, 0.000);

INSERT INTO genres(genre_id, name) VALUES
(5, 'social themes'),
(3, 'comic book'),
(1, 'educational'),
(7, 'lonely genre');

INSERT INTO publishers(publisher_id, name) VALUES 
(8, 'Balzer + Bray'),
(13, 'Feiwel & Friends'),
(17, 'Nancy Paulsen Books'),
(18, 'Candlewick Press'),
(21, 'Dover Publications'),
(23, 'Elon Books'),
(24, 'HMH Books for Young Readers'),
(26, 'Grosset & Dunlap'),
(28, 'HarperCollins'),
(30, 'Delacorte Books for Young Readers'),
(38, 'First Second'),
(39, 'Scholastic GRAPHIX'),
(40, 'Amulet Books'),
(42, 'Random House'),
(45, 'Cartoon Books'),
(47, 'Three Rivers Press');

INSERT INTO books(isbn,title,publisher_id,pub_date,num_pages,description,wholesale_price,list_price,sale_price,min_age,max_age,removed) VALUES 
(9780062498564, 'On the Come Up', 8, '2019-02-05', 447, 'Sixteen-year-old Bri wants to be one of the greatest rappers of all time. Or at least make it out of her neighborhood one day. As the daughter of an underground rap legend who died before he hit big, BriÃ¢â‚¬â„¢s got big shoes to fill. But now that her mom has unexpectedly lost her job, food banks and shutoff notices are as much a part of BriÃ¢â‚¬â„¢s life as beats and rhymes. With bills piling up and homelessness staring her family down, Bri no longer just wants to make itÃ¢â‚¬â€�she has to make it.On the Come Up is Angie ThomasÃ¢â‚¬â„¢s homage to hip-hop, the art that sparked her passion for storytelling and continues to inspire her to this day. It is the story of fighting for your dreams, even as the odds are stacked against you', 12.00, 23.99, -1, 14, 17, 0),
(9780062941008, 'Peanut Goes for the Gold', 28, '2020-03-31', 32, 'Jonathan Van Ness, the star of Netflix''s hit show Queer Eye, brings his signature humor and positivity to his empowering first picture book, inspiring readers of all ages to love being exactly who they are.Peanut Goes for the Gold is a charming, funny, and heartfelt picture book that follows the adventures of Peanut, a gender nonbinary guinea pig who does everything with their own personal flare.Peanut just has their own unique way of doing things. Whether it''s cartwheeling during basketball practice or cutting their own hair, this little guinea pig puts their own special twist on life. So when Peanut decides to be a rhythmic gymnast, they come up with a routine that they know is absolutely perfect, because it is absolutely, one hundred percent Peanut.This upbeat and hilarious picture book, inspired by Jonathan''s own childhood guinea pig, encourages children to not just be themselvesÃ¢â‚¬â€¢but to boldly and unapologetically love being themselves.Jonathan Van Ness brings his signature message of warmth, positivity, and self-love to this boldly original picture book that celebrates the joys of being true to yourself and the magic that comes from following your dreams.', 8.00, 15.16, 15.16, 4, 6, 0),
(9780062953414, 'The Candy Caper', 28, '2020-03-03', 96, 'Molly gets things stuck in her head sometimes. When she sees a jar of candy on Principal SheltonÃ¢â‚¬â„¢s desk, she absolutely needs to know how many candies are in that jar! Luckily, her two best friends, Simon and Rosie, are ready to help her find the answerÃ¢â‚¬â€�even if it means detention for all of them!', 3.00, 5.99, 2.99, 6, 8, 0),
(9780062953452, 'Busted by Breakfast', 28, '2020-03-03', 96, 'Simon always has a lot to say. And sometimes he canÃ¢â‚¬â„¢t stop talkingÃ¢â‚¬â€œeven in the middle of class.When Simon gets in trouble for jabbering at school, his best friends, Molly and Rosie, think up a plan to keep him from getting grounded at home! It involves cars, suds, and pink plastic flamingos! But will their big plan turn into an even bigger disaster?', 3.00, 5.99, 6.00, 6, 8, 0),
(9780312625993, 'The Forgiveness Garden', 13, '2012-10-30', 32, 'A long time ago and far away--although it could be here, and it could be now--a boy threw a stone and injured a girl. For as long as anyone could remember, their families had been enemies, and their towns as well, so it was no surprise that something bad had happened. Hate had happened. Revenge had happened. And that inspired more hate and more calls for revenge. But this time, a young girl decided to try something different...Inspired by the original Garden of Forgiveness in Beirut, Lebanon, and the movement that has grown up around it, Lauren Thompson has created a timeless parable for all ages that shows readers a better way to resolve conflicts and emphasizes the importance of moving forward together.', 4.00, 7.99, 7.98, 4, 6, 0),
(9780375832291, 'Babymouse #1: Queen of the World!', 42, '2011-11-30', 96, 'With multiple Children''s Choice Awards and over 1.8 million books sold, kids, parents, and teachers agree that Babymouse is perfect for fans of Junie B. Jones, Ivy and Bean, Bad Kitty, and Dork Diaries!Meet Babymouseâ€”Her dreams are big! Her imagination is wild! Her whiskers are ALWAYS a mess! In her mind, she''s Queen of the World! In real lifeâ€¦she''s not even Queen of the lunch table.NEW YORK TIMES bestselling, three-time Newbery Honor winning author Jennifer Holm teams up with Matthew Holm to bring you a fully illustrated graphic novel series packed with humor and kid appealâ€”BABYMOUSE! It''s the same thing every day for Babymouse. Where is the glamour? The excitement? The fame?!? Nothing ever changes, untilâ€¦Babymouse hears about Felicia Furrypaws''s exclusive slumber party. Will Babymouse get invited? Will her best friend, Wilson, forgive her if she misses their monster movie marathon? Find out in Babymouse #1: Queen of the World! All new BABYMOUSE BONUS: How to Draw Babymouse & Create-Your-Own-Comic!', 4.00, 7.99, 4.00, 7, 10, 0),
(9780385265201, 'The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great', 47, '2014-10-28', 368, 'An entertaining and informative illustrated guide that makes world history accessible, appealing, and funny.', 10.00, 19.99, 0.00, 10, 15, 0),
(9780385320436, 'Mistakes That Worked: 40 Familiar Inventions & How They Came to Be', 47, '1994-05-01', 96, 'Do you know how many things in your daily life were invented by accident? SANDWICHES came about when an English earl was too busy gambling to eat his meal and needed to keep one hand free. POTATO CHIPS were first cooked by a chef who was furious when a customer complained that his fried potatoes werenâ€™t thin enough. Coca-Cola, Silly Putty, and X rays have fascinating stories behind them too! Their unusual tales, and many more, along with hilarious cartoons and weird, amazing facts, make up this fun-filled book about everyday items that had surprisingly haphazard beginnings. And don''t miss Eat Your Words about the fascinating language of food! Praise for Mistakes That Worked: â€œA splendid book that is as informative as it is entertaining . . . a gem.â€� â€”Booklist, Starred', 5.00, 9.71, 0.00, 8, 12, 0),
(9780399246531, 'The Day You Begin', 17, '2018-08-01', 32, 'National Book Award winner Jacqueline Woodson and two-time Pura Belpre Illustrator Award winner Rafael Lopez have teamed up to create a poignant, yet heartening book about finding courage to connect, even when you feel scared and alone. There will be times when you walk into a room and no one there is quite like you.There are many reasons to feel different. Maybe it''s how you look or talk, or where you''re from.', 10.00, 20.69, 17.00, 5, 8, 0),
(9780439706407, 'Bone #1: Out from Boneville', 45, '2014-05-01', 145, 'The three Bone cousins -- Fone Bone, Phoney Bone, and Smiley Bone -- are separated and lost in a vast, uncharted desert. One by one, they find their way into a deep, forested valley filled with wonderful and terrifying creatures. Eventually, the cousins are reunited at a farmstead run by tough Gran''ma Ben and her spirited granddaughter. But little do the Bones know, there are dark forces conspiring against them, and their adventures are only just beginning!', 5.00, 10.99, 0.00, 11, 16, 0),
(9780439846806, 'The Stonekeeper (Amulet #1)', 39, '2013-12-17', 192, 'After the tragic death of their father, Emily and Navin move with their mother to the home of her deceased great-grandfather, but the strange house proves to be dangerous. Before long, a sinister creature lures the kids'' mom through a door in the basement. Em and Navin, desperate not to lose her, follow her into an underground world inhabited by demons, robots, and talking animals.', 5.00, 9.99, 0.00, 8, 12, 0),
(9780448405179, 'What''s Out There?: A Book about Space', 26, '1993-03-24', 32, 'What is the sun made of? What causes night and day? Why does the moon change shape? Colorful collage illustrations and an easy-to-understand text bring planets, stars, comets, and the wondrous things out there in space right down to earth in a simple introduction to the solar system for young armchair astronauts.', 2.00, 3.99, 0.00, 4, 8, 0),
(9780486468211, 'My First Human Body Book', 21, '2009-01-19', 32, 'It''s easy to learn about your body! This cool coloring book includes 28 drawings that explore muscles, bones, lungs, and more. You can read how your voice box works, how your tongue tastes food, how your DNA is different from your family and friends, and more.', 2.00, 3.99, 3, 6, 10, 1),
(9780545132060, 'Smile', 39, '2010-02-01', 224, 'Raina Telgemeier''s #1 New York Times bestselling, Eisner Award-winning graphic memoir based on her childhood!Raina just wants to be a normal sixth grader. But one night after Girl Scouts she trips and falls, severely injuring her two front teeth. What follows is a long and frustrating journey with on-again, off-again braces, surgery, embarrassing headgear, and even a retainer with fake teeth attached. And on top of all that, there''s still more to deal with: a major earthquake, boy confusion, and friends who turn out to be not so friendly.', 5.00, 9.89, 6.00, 6, 9, 0),
(9780547557991, 'The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth', 24, '2013-10-03', 208, 'Animals smooth and spiky, fast and slow, hop and waddle through the two hundred plus pages of the Caldecott Honor artist Steve Jenkinsâ€™s most impressive nonfiction offering yet. Sections such as â€œAnimal Senses,â€� â€œAnimal Extremes,â€� and â€œThe Story of Lifeâ€� burst with fascinating facts and infographics that will have trivia buffs breathlessly asking, â€œDo you know a termite queen can produce up to 30,000 eggs a day?â€� Jenkinsâ€™s color-rich cut- and torn-paper artwork is as strikingly vivid as ever. Rounding out this bountiful browsersâ€™ almanac of more than three hundred animals is a discussion of the artistâ€™s bookmaking process, an animal index, a glossary, and a bibliography. A bookshelf essential!', 8.00, 15.00, 11.00, 6, 9, 0);


--Books with specified aquired time stamps
INSERT INTO books(isbn,title,publisher_id,pub_date,num_pages,description,wholesale_price,list_price,sale_price,min_age,max_age,removed, acquired_stamp) VALUES 
(9780606144841, 'American Born Chinese', 38, '2006-09-06', 240, 'Perfectly Logical helps curious kids ages 8-12 develop logical reasoning and critical thinking skills while having a blast (thatâ€™s the most important part). With puzzles that progressively increase in difficulty, this book engages and challenges kids for hours on end.', 5.00, 10.99, 6.00, 12, 18, 0, '2021-01-01 00:00:00'),
(9780606267380, 'Drama', 40, '2012-09-01', 240, 'Callie loves theater. And while she would totally try out for her middle school''s production of Moon over Mississippi, she can''t really sing. Instead she''s the set designer for the drama department''s stage crew, and this year she''s determined to create a set worthy of Broadway on a middle-school budget. But how can she, when she doesn''t know much about carpentry, ticket sales are down, and the crew members are having trouble working together? Not to mention the onstage AND offstage drama that occurs once the actors are chosen. And when two cute brothers enter the picture, things get even crazier!', 4.00, 7.99, 0.00, 10, 14, 1, '2021-01-01 00:00:01'),
(9780692848388, 'What Should Danny Do?', 40, '2017-05-17', 68, 'FUN. INTERACTIVE. EMPOWERING. THE BOOK THEY''LL LOVE TO READ AGAIN AND AGAIN! With 9 Stories in 1, the fun never ends! What Should Danny Do? is an innovative, interactive book that empowers kids with the understanding that their choices will shape their days, and ultimately their lives into what they will be. Written in a "Choose Your Own Story" style, the book follows Danny, a Superhero-in-Training, through his day as he encounters choices that kids face on a daily basis. As your children navigate through the different story lines, they will begin to realize that their choices for Danny shaped his day into what it became. And in turn, their choices for themselves will shape their days, and ultimately their lives, into what they will be. Boys and girls both love and relate to Danny, while enjoying the interactive nature of the book--they never know what will come next! Parents and Teachers love the social-emotional skills the book teaches through empowering kids to make positive choices while demonstrating the natural consequences to negative choices. A "must-have" on every bookshelf.', 4.00, 8.00, 0.00, 3, 6, 0, '2021-01-01 00:00:01'),
(9780763693558, 'Alma and How She Got Her Name', 40, '2018-05-10', 32, 'What''s in a name? For one little girl, her very long name tells the vibrant story of where she came from and who she may one day be. If you ask her, Alma Sofia Esperanza Jose Pura Candela has way too many names: six! How did such a small person wind up with such a large name? Alma turns to Daddy for an answer and learns of Sofia, the grandmother who loved books and flowers.', 11.00, 21.99, 0.00, 4, null, 0, '2021-01-02 00:00:01'),
(9780810984226, 'How Mirka Got Her Sword (Hereville Book 1)', 40, '2010-11-01', 144, 'Spunky, strong-willed eleven-year-old Mirka Herschberg isnâ€™t interested in knitting lessons from her stepmother, or how-to-find-a-husband advice from her sister, or you-better-not warnings from her brother. Thereâ€™s only one thing she does want: to fight dragons!Granted, no dragons have been breathing fire around Hereville, the Orthodox Jewish community where Mirka lives, but that doesnâ€™t stop the plucky girl from honing her skills. She fearlessly stands up to local bullies. She battles a very large, very menacing pig. And she boldly accepts a challenge from a mysterious witch, a challenge that could bring Mirka her heartâ€™s desire: a dragon-slaying sword! All she has to do is findâ€”and outwitâ€”the giant troll whoâ€™s got it!A delightful mix of fantasy, adventure, cultural traditions, and preteen commotion, Hereville will captivate middle-school readers with its exciting visuals and entertaining new heroine.', 5.00, 9.56, 6.00, 8, 12, 0, '2021-01-03 00:00:01');

INSERT INTO bookgenre(isbn,genre_id) VALUES
(9780062498564, 5),
(9780062498564, 3),
(9780062941008, 5),
(9780062941008, 3),
(9780062941008, 1),
(9780062953414, 5),
(9780062953452, 5),
(9780312625993, 5),
(9780375832291, 3),
(9780385265201, 3),
(9780385320436, 1),
(9780399246531, 5),
(9780439706407, 3),
(9780439846806, 3),
(9780448405179, 1),
(9780486468211, 1),
(9780545132060, 3),
(9780547557991, 1),
(9780606144841, 3),
(9780606267380, 3),
(9780692848388, 1),
(9780763693558, 5),
(9780810984226, 3);

INSERT INTO authors(author_id,firstname,lastname, title) VALUES
(84, 'Angie', 'Thomas', NULL),
(88, 'Jonathan', 'Van Ness', NULL),
(89, 'Tom', 'Watson', NULL),
(85, 'Lauren', 'Thompson', NULL),
(33, 'Jennifer L.', 'Holm', 'Dr'),
(34, 'Matthew', 'Holm', NULL),
(30, 'Larry', 'Gonick', NULL),
(65, 'John', 'O''Brien', NULL),
(37, 'Charlotte Foltz', 'Jones', NULL),
(92, 'Jacqueline', 'Woodson', NULL),
(78, 'Jeff', 'Smith', NULL),
(39, 'Kazu', 'Kibuishi', NULL),
(91, 'Lynn', 'Wilson', NULL),
(4, 'Paige', 'Billin-Frye', NULL),
(93, 'Patricia J.', 'Wynne', 'Dr'),
(75, 'Donald M.', 'Silver', NULL),
(82, 'Raina', 'Telgemeier', NULL),
(94, 'Gene Luen', 'Yang', NULL),
(36,'Steve','Jenkins',NULL),
(45, 'Adir', 'Levy', NULL),
(46, 'Ganit', 'Levy', NULL),
(72, 'Mat', 'Sadler', NULL),
(50, 'Juana', 'Martinez-Neal', NULL),
(17, 'Barry', 'Deutsch', NULL),
(200, 'Hi', 'IDontHaveAnyBooks', NULL);

INSERT INTO bookauthor(isbn,author_id) VALUES
(9780062498564, 84),
(9780062941008, 88),
(9780062953414, 89),
(9780062953452, 89),
(9780312625993, 85),
(9780375832291, 33),
(9780375832291, 34),
(9780385265201, 30),
(9780385320436, 65),
(9780385320436, 37),
(9780399246531, 92),
(9780439706407, 78),
(9780439846806, 39),
(9780448405179, 91),
(9780448405179, 4),
(9780486468211, 93),
(9780486468211, 75),
(9780545132060, 82),
(9780547557991, 36),
(9780606144841, 94),
(9780606267380, 82),
(9780692848388, 45),
(9780692848388, 46),
(9780692848388, 72),
(9780763693558, 50),
(9780810984226, 17),
(9780763693558, 33),
(9780810984226, 33),
(9780606267380,33);

INSERT INTO users(user_id,email,title,lastname,firstname,companyname,address1,address2,city,province,country,postalcode,homephone,cellphone) VALUES
(1,'cst.send@gmail.com','Mx.','Consumer','Consumer','Dawson','6812 Serena Fords',NULL,'Blaiseland','NB','CANADA','E5A 6N8','935.469.0056x691','9255585725'),
(2,'cst.receive@gmail.com','Mx.','Manager','Manager','Dawson','908 Kautzer Lodge Suite 765',NULL,'Port Jadaport','MB','CANADA','R5A 9A2','949-302-7236x3218','868.030.5571'),
(3,'christopher78@example.net','Mx.','Veum','Maybell','Powlowski Ltd','7873 Runte Street',NULL,'McLaughlinchester','ON','CANADA','N8A 4T3','1-772-968-0112','(644)897-3429'),
(4,'upton.veda@example.org','Mx.','Turner','Carey',NULL,'4458 Drew Ville Suite 555',NULL,'Armandmouth','NS','CANADA','B2S 9T4','479-249-3018','1-971-771-5817'),
(5,'saul.gleason@example.com','Mx.','Wolff','Krystina','Mayert LLC','2180 Russel Crossing',NULL,'South Alexandrine','NB','CANADA','E4K 5P6','544-438-7359','711-118-3268'),
(6,'evalyn61@example.net','Mrs.','Flatley','Vesta','Oberbrunner, Wilkinson and Hane','795 Julius Lodge Suite 854',NULL,'South Jayden','NS','CANADA','B1K 9M5','657.577.8930x44855','735-539-1079'),
(7,'alverta30@example.org','Mx.','Waters','Louvenia','Quigley, Fahey and Mueller','51096 Edwin Crest Apt. 144',NULL,'South Tyshawntown','QC','CANADA','G4X 7J6','(836)657-4617x4788','(115)541-9434'),
(8,'marcella88@example.org','Mr.','Haag','Giovanni','Jaskolski Inc','963 Jacquelyn Garden',NULL,'Parisianborough','QC','CANADA','G6L 7V9','676-477-2053x82402','07563939668'),
(9,'erik88@example.org','Mx.','Schimmel','Daphne','Botsford-Hartmann','785 Waelchi Unions',NULL,'Chandlerville','BC','CANADA','V9X 4L8','630-651-8487x83563','02662638593'),
(10,'kuphal.jarret@example.net','Mrs.','Schuppe','Shany','Stehr Inc','507 VonRueden Island Suite 537',NULL,'Adrielview','QC','CANADA','H9W 4V6','726-006-7526','1-913-348-2830'),
(17,'seth77@example.net','Mx.','Reichert','Santa','Jacobson-Cassin','6811 Luis Ford Apt. 389',NULL,'Treutelfurt','MB','CANADA','R1N 1H9','(565)753-8787','127-055-2665'),
(18,'charlotte.mante@example.net','Mx.','Leuschke','Wanda',NULL,'55099 Berge Springs',NULL,'North Jomouth','MB','CANADA','R9A 7H0','1-753-917-9818','1-131-051-7065'),
(23,'pouros.audrey@example.org','Mx.','Hayes','Caesar','Langworth, Rice and Rodriguez','3476 Leanne Lodge',NULL,'Shaniefort','PE','CANADA','A1A 1A1','(648)512-2765','888.701.7408'),
(29,'eliseo39@example.net','Mx.','Kris','Major','Reichert, Kuhlman and Schowalter','78831 Hank Viaduct Apt. 067',NULL,'West Wilhelm','BC','CANADA','A1A 1A1','1-450-564-4527','262-783-1845'),
(35,'magali18@example.org','Miss','Sawayn','Henry','Grimes Ltd','648 Jovan Canyon',NULL,'Prosaccomouth','PE','CANADA','A1A 1A1','(345)706-8775x55069','(135)501-7458'),
(36,'nrempel@example.com','Mr.','Prosacco','Nelda','Murray-Keeling','054 Ansley Brooks','Apt. 890','Chandlerberg','NS','CANADA','A1A 1A1','663-041-5486x1052','(585)585-7718'),
(39,'buford.hoeger@example.com','Mx.','Thompson','Tremaine','Gibson, Boyle and Skiles','374 Freddie Fall','Apt. 897','Giovanniburgh','MB','CANADA','A1A 1A1','1-391-806-8098','140.174.1534'),
(40,'cedrick.nienow@example.net','Mx.','Hackett','Skye','Rogahn-Berge','1672 Rebecca Light',NULL,'Vonton','ON','CANADA','A1A 1A1','1-083-983-5084x1513','(564)311-3714'),
(42,'fkuphal@example.org','Mx.','Barrows','Kali','Stracke, Swift and Ernser','1963 Hand Inlet',NULL,'Corkerymouth','AB','CANADA','A1A 1A1','1-128-556-1227x920','1-717-958-5453x6189'),
(43,'antonetta36@example.org','Mrs.','Murphy','Cooper','Hoeger, Baumbach and Roberts','97835 Braulio Ridge',NULL,'Rhettshire','BC','CANADA','A1A 1A1','05235116248','00957760948'),
(44,'ludwig.mccullough@example.net','Mx.','Spinka','Adriel','Schulist, Ullrich and Stiedemann','799 Brekke Branch',NULL,'New Gianniville','YT','CANADA','A1A 1A1','491-947-5792','1-742-906-5504'),
(45,'chuels@example.org','Mx.','Kilback','Madaline','Larson, Torphy and Casper','79086 Gregg Lodge',NULL,'Jamesonstad','NT','CANADA','A1A 1A1','585-727-5730','848.761.1809'),
(52,'durward29@example.org','Mr.','Kuhlman','Blanca','Barrows PLC','7713 Jamar Way Suite 083',NULL,'Beerchester','NL','CANADA','B3B 4B4','467-191-8914','683-604-6955'),
(53,'hayes.aric@example.com','Mrs.','OHara','Lillie','Murray, Weber and Kub','59163 Corkery Lodge',NULL,'Zoeyland','BC','CANADA','A1A 1A1','+97(2)9901626451','814.131.8049'),
(62,'giles89@example.org','Mx.','Block','Arjun','Grimes, Batz and Hahn','0545 Hoyt Stream Suite 354',NULL,'Wolffstad','YT','CANADA','A1A 1A1','126.526.9502x0606','1-865-285-9044'),
(63,'xjohns@example.com','Ms.','Hilll','Donny','Kautzer and Sons','349 Brian Lights',NULL,'West Arianestad','NT','CANADA','A1A 1A1','012.199.1028','(562)303-1263'),
(69,'block.golda@example.net','Mx.','Wisozk','Boris','Walsh and Sons','166 Waelchi Route Suite 067',NULL,'New Barneystad','BC','CANADA','A1A 1A1','1-724-689-0006x171','1-961-748-9545'),
(88,'eino.dickinson@example.net','Mx.','Abshire','Kirsten',NULL,'953 Gorczany Park',NULL,'Gabriellemouth','AB','CANADA','A1A 1A1','02271930898','(221)495-5872'),
(89,'fay.ruthe@example.net','Mx.','Langosh','Noel','Hilpert LLC','1624 Kasandra Way',NULL,'Cassinburgh','BC','CANADA','A1A 1A1','(267)297-7445','171-294-7280'),
(90,'pbahringer@example.org','Ms.','Steuber','Idell','Sipes-VonRueden','0625 Gulgowski Trace',NULL,'North Tyrese','YT','CANADA','A1A 1A1','277-560-0349','01755540456'),
(92,'avery.klein@example.org','Mr.','Stamm','Imogene',NULL,'5776 Adella Stravenue',NULL,'Whiteton','NU','CANADA','A1A 1A1','(046)549-3039','193.182.3293'),
(93,'elna.romaguera@example.org','Mr.','Rohan','Henri','Ritchie-Goyette','27782 Muller Plains Apt. 993',NULL,'Hegmannburgh','QC','CANADA','A1A 1A1','(988)891-3612','256-373-4074'),
(95,'zwolf@example.com','Ms.','Moore','Heidi','Zemlak Group','789 Greta Springs Apt. 784',NULL,'Harmonchester','PE','CANADA','A1A 1A1','536-993-1781','(478)624-8222'),
(99, 'email@email.com', 'Mr.', 'Skate', 'Cheap', 'Hi', 'somewhere', NULL, 'Somewhere else', 'QC', 'CANADA', 'A1A 1A1', '555-555-5555', '555-555-5551');

INSERT INTO credentials (user_id, salt, hash, manager) VALUES 
(1, 'cmt3bhdmkfo6qi78lcujlkdhcck4',x'E72248EA2F153824EA6D5F806A1B7445741C29327920DEB325A94BA93892A829', 0),
(2, 'n162c3f65l8jj0394788k07mbcp6',x'4FE347332FFC006D5AF96D662EA7CD70AC48F8ECF9694AAF7BDE185F7778E64C', 1),
(3, 'peg97b68fo6jfglaidie293uqket',x'1E66DF2DF2EC6316BBE4F647BCC73B4E3D331FCB1ADA4180C2858B1A7518CD22', 1),
(4, 'dcbk21lafg20jeutqktkotokva19',x'F2BB82F81B4FA4048AE5C08FF45754C0D2C54BE13096D2746E4CCDE66A8C9934', 1),
(5, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(6, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(7, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(8, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(9, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(10, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(17, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(18, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(23, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(29, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(35, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(36, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(39, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(40, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(42, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(43, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(44, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(45, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 1),
(52, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(53, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(62, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(63, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(69, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(88, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(89, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(90, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(92, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(93, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(95, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0);

INSERT INTO orders(order_id,user_id,purchase_date,gst_rate,pst_rate,hst_rate) VALUES
(1,1,'2010-03-07',0.000,0.000,15.000),
(2,2,'2018-06-03',5.000,7.000,0.000),
(3,3,'2019-09-03',0.000,0.000,13.000),
(4,4,'2017-04-20',0.000,0.000,15.000),
(5,4,'2011-12-16',0.000,0.000,15.000),
(6,6,'2014-06-25',0.000,0.000,15.000),
(7,7,'2011-12-05',5.000,9.975,0.000),
(8,8,'2011-12-05',5.000,9.975,0.000),
(9,9,'2017-01-12',5.000,7.000,0.000),
(10,10,'2016-08-01',5.000,9.975,0.000),
(17,17,'2019-07-18',5.000,7.000,0.000),
(18,18,'2017-08-19',5.000,7.000,0.000),
(23,6,'2012-05-15',0.000,0.000,15.000),
(29,29,'2018-06-24',5.000,7.000,0.000),
(35,35,'2006-06-08',0.000,0.000,15.000),
(36,6,'2014-10-05',0.000,0.000,15.000),
(39,39,'2019-04-14',5.000,7.000,0.000),
(40,40,'2019-04-14',0.000,0.000,13.000),
(42,42,'2019-04-14',5.000,0.000,0.000),
(43,43,'2012-06-21',5.000,7.000,0.000),
(44,44,'2011-12-26',5.000,0.000,0.000),
(45,45,'2015-03-26',5.000,0.000,0.000),
(52,6,'2003-03-07',0.000,0.000,15.000),
(53,53,'2008-06-25',5.000,7.000,0.000),
(62,62,'2019-05-27',5.000,0.000,0.000),
(63,63,'2003-05-20',5.000,0.000,0.000),
(69,69,'2019-09-20',5.000,7.000,0.000),
(88,88,'2019-04-29',5.000,0.000,0.000),
(89,88,'2015-10-13',5.000,7.000,0.000),
(90,90,'2016-11-06',5.000,0.000,0.000),
(92,92,'2016-11-06',5.000,0.000,0.000),
(93,93,'2016-11-06',5.000,9.975,0.000),
(95,6,'2016-11-06',0.000,0.000,15.000),
(101,2,'2018-06-07',5.000,7.000,0.000);

INSERT INTO orderitems(orderitems_id,order_id,isbn,price) VALUES
(1,1,9780062941008,19.47),
(2,2,9780062941008,18.95),
(3,3,9780062941008,21.66),
(4,4,9780062941008,18.78),
(5,5,9780062941008,8.00),
(6,6,9780062941008,8.00),
(7,7,9780062941008,28.89),
(8,8,9780062941008,15.94),
(9,9,9780062941008,28.54),
(10,10,9780062941008,8.00),
(17,17,9780062953452,21.70),
(18,18,9780062953414,36.04),
(23,23,9780062498564,25.14),
(29,29,9780312625993,26.42),
(35,35,9780399246531,26.22),
(36,36,9780763693558,11.00),
(39,39,9780486468211,29.67),
(40,40,9780486468211,11.84),
(42,42,9780692848388,26.01),
(43,43,9780692848388,7.45),
(44,44,9780692848388,25.46),
(45,45,9780547557991,8.00),
(52,52,9780448405179,20.36),
(53,53,9780448405179,5.04),
(62,62,9780385320436,24.58),
(63,63,9780385320436,20.66),
(64,63,9780547557991,18),
(66,63,9780062941008,25),
(69,69,9780448405179,17),
(70,69,9780062941008,19.47),
(89,88,9780606144841,20.56),
(90,89,9780606144841,21.29),
(91,90,9780810984226,26.77),
(93,92,9780375832291,24.50),
(94,93,9780375832291,29.51),
(96,95,9780606267380,25.94),
(102,101,9780062953452,21.70);


INSERT INTO orderitems(orderitems_id,order_id,isbn,price, removed) VALUES
(65,63,9780448405179,17, 1),
(103,101,9780375832291,31.70, 1);

INSERT INTO reviews(review_id,isbn,postdate,user_id,rating,text,approved) VALUES
(11, 9780062941008, '2009-04-07 20:42:35', 1, 4, 'Et et placeat sunt officiis ut quis. Quis cupiditate suscipit non nulla repellendus eaque voluptas. Architecto est quidem eum laboriosam numquam. Eos modi corporis et nihil et voluptas explicabo.', 1),
(18, 9780062953452, '2016-01-06 18:31:33', 1, 3, 'Commodi quod quibusdam dolor aut ut labore. Voluptatem qui sed nulla unde est. Excepturi iusto corrupti numquam est autem voluptas explicabo. Voluptates similique ea rerum ut voluptatem quidem.', 0),
(19, 9780062953414, '2019-07-15 22:09:28', 2, 2, 'Architecto sit voluptatem corporis ad perspiciatis quaerat. Facilis deserunt id rerum dolor. Beatae atque minus sunt ut non veniam.',1),
(24,9780062498564,'2016-02-02 03:53:48',17,1,'Ab rerum commodi cupiditate neque. Quasi facere quaerat facilis quae aut quasi. Ipsa excepturi inventore maiores quos. Laudantium id non est est.',1),
(30,9780312625993,'2009-08-21 12:19:25',35,1,'Vel velit quas quas dolore quam quasi reprehenderit. Non nulla praesentium cupiditate alias. Quam nisi non perferendis quasi cum. Ex ea animi est tenetur quo.',1),
(36,9780399246531,'2012-01-26 10:07:37',36,5,'Beatae unde nemo sit. Exercitationem modi quia tenetur ut iusto ullam. Qui id earum sunt veritatis maiores impedit ut.',1),
(37,9780763693558,'2000-07-10 20:05:25',10,3,'Similique delectus ipsam dolor quo id non inventore aspernatur. Ut recusandae reprehenderit cumque facere. Voluptatum voluptatum dolor eos rerum vitae vero reiciendis.',1),
(40,9780486468211,'2015-06-15 19:55:25',17,5,'Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.',1),
(41,9780486468211,'2002-03-01 13:48:55',36,2,'Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.',1),
(43,9780692848388,'2017-11-13 03:22:28',43,2,'Dolores earum eos sed voluptatibus sit. Delectus culpa repellat porro magnam delectus minima assumenda. Velit in minus ad est omnis. Nobis numquam nulla quia.',0),
(44,9780692848388,'2019-02-23 10:15:01',44,2,'Voluptatem veritatis nobis omnis voluptatem. Esse assumenda quo sequi dignissimos omnis. Sequi minima rerum accusamus tempore ea quaerat.',1),
(45,9780692848388,'2019-08-12 00:07:19',45,3,'Aut ipsum sapiente quia sit. Eaque ut quis voluptas consequuntur odit quia. Excepturi impedit cupiditate rerum provident dolores aut.',1),
(46,9780547557991,'2005-11-14 20:39:22',52,1,'Sunt et est voluptas aspernatur. Error soluta amet inventore. Laudantium vitae in ut dolor corporis qui.',1),
(53,9780448405179,'2019-04-07 18:43:46',53,2,'Cumque quia eligendi ipsum reprehenderit harum velit id hic. A corporis deserunt unde at. Impedit ipsum quae ea velit quia occaecati velit.',1),
(54,9780448405179,'2010-05-07 15:24:16',52,4,'Aut hic ex necessitatibus cupiditate saepe. Quasi sapiente illo illum est. Nemo est et tempora laudantium iste. Accusantium est est laboriosam repellat pariatur perferendis voluptatem.',1),
(63,9780385320436,'2010-07-05 13:38:03',63,2,'Ipsum qui assumenda rerum fugit voluptates temporibus. Eos voluptas ipsa tempore omnis modi eius. Perspiciatis voluptas nam laboriosam mollitia nesciunt ad.',0),
(64,9780385320436,'2016-04-22 22:28:05',69,2,'Illo voluptatem dolorem ea eum fugit dolorem est. Asperiores impedit optio quo sint ab maiores. Dolores qui explicabo soluta deserunt. Velit accusantium eum ratione porro sint quibusdam.',1),
(89,9780606144841,'2011-02-27 07:31:24',69,5,'Harum quisquam sint voluptatibus fugiat et. Qui et alias qui hic. Minima ut est repudiandae id culpa minus.',1),
(90,9780545132060,'2010-09-04 23:59:30',88,1,'Harum provident explicabo accusamus asperiores ea ex quae. Laborum repudiandae et eligendi id. Eveniet expedita recusandae voluptas expedita. Molestias iure consectetur omnis fuga.',1),
(91,9780810984226,'2010-09-27 06:12:09',88,1,'Molestias omnis vel nemo omnis commodi et. Incidunt ad harum unde deserunt atque rerum. Aspernatur ducimus aut dolor tempore necessitatibus delectus sint. Odit et architecto eligendi fugit.',1),
(93,9780375832291,'2002-08-31 05:18:29',88,4,'At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.',1),
(94,9780375832291,'2016-10-22 09:40:58',92,3,'Illo consectetur et eos. Aperiam aut soluta optio omnis exercitationem. Voluptas atque dolores laborum assumenda rem.',1),
(96,9780606267380,'2018-03-30 01:45:24',95,3,'In ut non saepe consequatur dolor nulla. Cumque qui et corrupti. Nesciunt est et commodi qui.',1),
(99,9780439846806,'2010-11-12 13:21:04',95,2,'Sit ut maiores fugit accusantium. Nostrum eveniet est perspiciatis aut beatae. Saepe asperiores enim adipisci voluptas soluta. Vitae in alias est optio excepturi.',1);

INSERT INTO banners(ad_id,url,active) VALUES
(1,'http://schadenmraz.com/',1),
(2,'http://www.keeling.org/',1),
(3,'http://www.sawayn.com/',1),
(4,'http://www.cassin.com/',0),
(5,'http://www.reillyzemlak.com/',0),
(6,'http://www.wolff.biz/',0),
(7,'http://dickinsonbernhard.com/',0),
(8,'http://brekke.com/',0),
(9,'http://www.hueldare.com/',0),
(10,'http://www.ziemann.com/',0),
(11,'http://www.braunborer.net/',0),
(12,'http://hartmannbailey.com/',0),
(13,'http://schinner.info/',0),
(14,'http://reilly.com/',0),
(15,'http://www.zulauf.com/',0),
(16,'http://kunze.com/',0),
(17,'http://www.barton.com/',0),
(18,'http://www.runolfsdottir.net/',0),
(19,'http://grimeskshlerin.info/',0),
(20,'http://www.christiansen.biz/',0);

INSERT INTO surveys(survey_id,question,answer_1,answer_2,answer_3,answer_4,vote_1,vote_2,vote_3,vote_4,active) VALUES
(1,'Ab architecto aut et molestiae est aspernatur.','Numquam amet sint aut aliquid est soluta ut.','Alias alias quidem voluptas quibusdam ad.','Corporis aliquam aliquid eligendi corrupti autem asperiores.','Dolorem nihil corporis molestiae iusto asperiores fuga.',0,0,0,0,1),
(2,'Adipisci sint aliquam repudiandae laboriosam.','Quia animi laudantium quos iure qui ut.','Impedit fugit dolore itaque.','Voluptatem ipsa nobis minima veritatis unde.','Aspernatur eius sunt et et delectus.',0,0,0,0,1),
(3,'Aliquam voluptas cupiditate ipsam aut odio.','Pariatur veritatis velit et qui dolorum illum ipsa.','Doloribus sed aut harum ut omnis.','Provident voluptate expedita culpa reiciendis est at numquam.','Aut repellat omnis nisi pariatur distinctio.',0,0,0,0,1),
(4,'Animi et molestiae rerum saepe a.','Reprehenderit necessitatibus hic consequatur.','Labore molestiae architecto voluptas enim voluptatem hic qui.','Ipsa minus aliquid quos id sequi itaque.','Repellendus voluptas occaecati veritatis.',0,0,0,0,0),
(5,'Assumenda dolore esse accusantium dignissimos ipsa tempora.','A aut ut rerum et.','Dolor saepe illo consequatur beatae eos beatae porro.','Vel rerum ea illo itaque labore qui.','Omnis debitis omnis quaerat eligendi exercitationem.',0,0,0,0,0),
(6,'Aut ad itaque vero rerum sapiente cupiditate.','Asperiores architecto et doloremque tempora a.','Occaecati et aliquid consectetur ea aut totam dolorem sed.','Dolores quia exercitationem ullam eius.','Alias laborum aut blanditiis suscipit dolores.',0,0,0,0,0),
(7,'Aut deserunt doloribus totam.','Accusamus error distinctio vero maiores.','Facere dolor enim quisquam laudantium ut.','Molestiae rerum iure fuga asperiores.','Officiis eum iste harum neque officia odio.',0,0,0,0,0),
(8,'Autem maxime voluptas atque ipsa ex non inventore.','Sequi id sed id qui laboriosam reiciendis repellat fugit.','Harum alias consequatur vitae qui numquam odio aut voluptatem.','Cupiditate rem impedit esse omnis illum qui tempora.','Magnam aut commodi corrupti doloremque.',0,0,0,0,0),
(9,'Commodi odit blanditiis pariatur eos tenetur est.','Quaerat quis cum ad magnam non laboriosam nobis.','At veritatis autem occaecati provident in perferendis et.','Omnis hic a impedit officia necessitatibus.','Nisi dignissimos ex voluptatem consequuntur.',0,0,0,0,0),
(10,'Consequatur culpa et commodi assumenda culpa.','Autem aut suscipit illum labore temporibus.','Cumque voluptas sed quam sit.','Iste culpa ut temporibus atque.','0',0,0,0,0,0),
(11,'Consequatur dolore laborum amet iure sunt et debitis.','Odit fugiat aliquam eos amet.','Dignissimos fugit repudiandae accusamus est.','Est sint quos dolor numquam.','Quo excepturi atque quia dignissimos quaerat in.',0,0,0,0,0),
(12,'Corporis dolores consequuntur rerum perspiciatis odio optio.','Laborum dolorum ipsum quibusdam in tenetur ex.','Sed fugit blanditiis vel quis qui.','Reiciendis dolore qui voluptas ab.','0',0,0,0,0,0),
(13,'Corrupti provident aut est illo.','Non officia velit tempore vero.','Nesciunt reiciendis rerum quidem quasi debitis provident mollitia.','Recusandae est quaerat qui unde molestias explicabo rerum dolor.','Nobis voluptates mollitia quibusdam quia qui tempora omnis.',0,0,0,0,0),
(14,'Cum molestiae omnis est dolor odit odio.','Suscipit non excepturi quibusdam nisi quis.','Magni quibusdam natus itaque debitis aut.','Voluptas itaque voluptas laudantium et iste debitis et esse.','Nulla sapiente quis est est doloremque molestiae dolore rem.',0,0,0,0,0),
(15,'Deserunt molestias similique officia suscipit.','Quod aut quidem omnis eum.','Sunt adipisci qui aspernatur.','Et aut odit quisquam magni consectetur.','Ut enim sunt vel provident minus enim unde.',0,0,0,0,0),
(16,'Dicta repellat non odit veniam enim et cum fugit.','Aut qui earum sint beatae.','Dolore nesciunt eveniet magnam ipsa possimus in ut autem.','Vitae vero voluptatem consequatur.','Ullam harum non adipisci amet est saepe dolor.',0,0,0,0,0),
(17,'Dignissimos natus qui officiis id eos ea.','Eum quo cupiditate veritatis et quasi esse qui distinctio.','Recusandae et omnis nemo sint quia quo.','Consectetur aut reprehenderit quis ea magnam quia harum.','Consequatur illum quis temporibus distinctio id est et.',0,0,0,0,0),
(18,'Dolor alias voluptates architecto consequatur quidem a ut labore.','Minus architecto neque minus qui voluptas dolorem.','Sint consequuntur quo nihil nostrum quasi.','Est eligendi quia quod.','Nemo dolorem nihil impedit cupiditate.',0,0,0,0,0),
(19,'Dolor voluptas et est vel dolorem.','Modi veritatis quia enim.','Quisquam est quia in reiciendis sapiente dolore.','Est molestias suscipit necessitatibus dignissimos minima.','Et impedit et corrupti eum optio eius exercitationem.',0,0,0,0,0),
(20,'Dolorem odit ducimus doloremque non voluptates.','Quae quia corporis sed possimus doloremque ut voluptatem.','Et facere voluptatem unde voluptas saepe.','Inventore officia vel porro cupiditate tempora voluptate.','Et nisi omnis vitae adipisci.',0,0,0,0,0);


INSERT INTO bookformat(isbn,format_id) VALUES
(9780062498564, 13),
(9780062498564, 27),
(9780062498564, 12),
(9780062498564, 11),
(9780062498564, 24),
(9780062498564, 2),
(9780062941008, 12),
(9780062941008, 11),
(9780062941008, 25),
(9780062941008, 9),
(9780062941008, 23),
(9780062941008, 1),
(9780062953414, 13),
(9780062953414, 27),
(9780062953414, 12),
(9780062953414, 10),
(9780062953414, 24),
(9780062953414, 2),
(9780062953452, 14),
(9780062953452, 28),
(9780062953452, 13),
(9780062953452, 27),
(9780062953452, 11),
(9780062953452, 25),
(9780062953452, 3),
(9780312625993, 18),
(9780312625993, 3),
(9780312625993, 17),
(9780312625993, 2),
(9780312625993, 16),
(9780312625993, 29),
(9780312625993, 7),
(9780375832291, 12),
(9780375832291, 11),
(9780375832291, 25),
(9780375832291, 9),
(9780375832291, 23),
(9780375832291, 1),
(9780385265201, 21),
(9780385265201, 6),
(9780385265201, 20),
(9780385265201, 5),
(9780385265201, 18),
(9780385265201, 3),
(9780385265201, 10),
(9780385320436, 16),
(9780399246531, 22),
(9780399246531, 7),
(9780399246531, 21),
(9780399246531, 6),
(9780399246531, 20),
(9780399246531, 4),
(9780399246531, 11),
(9780439706407, 8),
(9780439846806, 7),
(9780439846806, 21),
(9780439846806, 6),
(9780439846806, 20),
(9780439846806, 4),
(9780439846806, 18),
(9780439846806, 25),
(9780448405179, 10),
(9780448405179, 24),
(9780448405179, 9),
(9780448405179, 23),
(9780448405179, 7),
(9780448405179, 21),
(9780448405179, 28),
(9780486468211, 4),
(9780486468211, 18),
(9780486468211, 3),
(9780486468211, 17),
(9780486468211, 1),
(9780486468211, 15),
(9780486468211, 22),
(9780545132060, 8),
(9780545132060, 22),
(9780545132060, 7),
(9780545132060, 21),
(9780545132060, 5),
(9780545132060, 19),
(9780547557991, 8),
(9780547557991, 22),
(9780547557991, 7),
(9780547557991, 21),
(9780547557991, 5),
(9780547557991, 19),
(9780547557991, 26),
(9780606144841, 4),
(9780606144841, 18),
(9780606144841, 3),
(9780606267380, 9),
(9780606267380, 23),
(9780606267380, 8),
(9780606267380, 22),
(9780606267380, 6),
(9780606267380, 20),
(9780606267380, 27),
(9780692848388, 7),
(9780692848388, 21),
(9780692848388, 6),
(9780692848388, 20),
(9780692848388, 4),
(9780692848388, 18),
(9780692848388, 25),
(9780763693558, 23),
(9780763693558, 8),
(9780763693558, 22),
(9780763693558, 7),
(9780763693558, 21),
(9780763693558, 5),
(9780763693558, 12),
(9780810984226, 10),
(9780810984226, 24);

INSERT INTO rssfeeds(feed_id, url, active) VALUES
(1, 'http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml', 1),
(2, 'https://edavis.github.io/hnrss/', 0),
(3, 'http://feeds.bbci.co.uk/news/world/africa/rss.xml', 1),
(4, 'http://feeds.bbci.co.uk/news/world/latin_america/rss.xml', 1),
(5, 'https://rss.nytimes.com/services/xml/rss/nyt/World.xml', 1),
(6, 'https://rss.nytimes.com/services/xml/rss/nyt/Arts.xml', 0),
(7, 'https://www.npr.org/rss/podcast.php?id=510298', 1),
(8, 'https://www.wrh.noaa.gov/FXC/wxstory.php?output=xml', 0),
(9, 'https://www.ndbc.noaa.gov/rss_access.shtml', 0),
(10, 'https://www.erh.noaa.gov/car/RSS_feeds/car_top_news.xml', 1);