USE ga1w20;

INSERT INTO formats(format_id,extension,name) VALUES 
(1, 'cbr', 'Comic Book Archive'),
(2, 'cbz', 'Comic Book Archive'),
(3, 'cb7', 'Comic Book Archive'),
(4, 'cbt', 'Comic Book Archive'),
(5, 'cba', 'Comic Book Archive'),
(6, 'cbr', 'Comic Book Archive'),
(7, 'djvu', 'DjVu'),
(8, 'doc', 'DOC'),
(9, 'docx', 'DOCX'),
(10, 'epub', 'EPUB(IDPF)'),
(11, 'fb2', 'FictionBook'),
(12, 'html', 'HTML'),
(13, 'ibook', 'iBooks'),
(14, 'inf', 'INF'),
(15, 'azw', 'Kindle'),
(16, 'lit', 'Microsoft Reader'),
(17, 'prc', 'Mobipocket'),
(18, 'mobi', 'Mobipocket'),
(19, 'exe', 'Multimedia EBook'),
(20, 'pkg', 'Newton Book'),
(21, 'pdb', 'eReader'),
(22, 'txt', 'Plain text'),
(23, 'pdb', 'Plucker'),
(24, 'pdf', 'Portable Document Format'),
(25, 'ps', 'PostScript'),
(26, 'tr2', 'Tome Raider'),
(27, 'tr3', 'Tome Raider'),
(28, 'oxps', 'OpenXPS'),
(29, 'xps', 'OpenXPS'),
(30, '.', 'Lonely format');

INSERT INTO genres(genre_id, name) VALUES
(5, 'social themes'),
(3, 'comic book'),
(1, 'educational'),
(7, 'lonely genre');

INSERT INTO publishers(publisher_id, name) VALUES 
(8, 'Balzer + Bray'),
(13, 'Feiwel & Friends'),
(17, 'Nancy Paulsen Books'),
(18, 'Candlewick Press'),
(21, 'Dover Publications'),
(23, 'Elon Books'),
(24, 'HMH Books for Young Readers'),
(26, 'Grosset & Dunlap'),
(28, 'HarperCollins'),
(30, 'Delacorte Books for Young Readers'),
(38, 'First Second'),
(39, 'Scholastic GRAPHIX'),
(40, 'Amulet Books'),
(42, 'Random House'),
(45, 'Cartoon Books'),
(47, 'Three Rivers Press');

INSERT INTO books(isbn,title,publisher_id,pub_date,num_pages,description,wholesale_price,list_price,sale_price,min_age,max_age,removed) VALUES 
(9780486468211, 'My First Human Body Book', 21, '2009-01-19', 32, 'It''s easy to learn about your body! This cool coloring book includes 28 drawings that explore muscles, bones, lungs, and more. You can read how your voice box works, how your tongue tastes food, how your DNA is different from your family and friends, and more.', 2.00, 3.99, 3, 6, 10, 0);
