USE ga1w20;

DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS orderitems;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS bookformat;
DROP TABLE IF EXISTS bookauthor;
DROP TABLE IF EXISTS bookgenre;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS formats;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS credentials;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS publishers;
DROP TABLE IF EXISTS genres;
DROP TABLE IF EXISTS surveys;
DROP TABLE IF EXISTS rssfeeds;
DROP TABLE IF EXISTS taxrates;
DROP TABLE IF EXISTS banners;

-- Create banner ads table
CREATE TABLE banners
(
    ad_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(2083),
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create tax rate table
CREATE TABLE taxrates
(
    region VARCHAR(2) PRIMARY KEY,
    gst_rate DECIMAL(6,3),
    pst_rate DECIMAL(6,3),
    hst_rate DECIMAL(6,3)
);

-- Create RSS feed table
CREATE TABLE rssfeeds
(
    feed_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(2083) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create surveys table:
CREATE TABLE surveys
(
    survey_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    question VARCHAR(400) NOT NULL,
    answer_1 VARCHAR(100) NOT NULL,
    answer_2 VARCHAR(100) NOT NULL,
    answer_3 VARCHAR(100),
    answer_4 VARCHAR(100),
    vote_1 INTEGER DEFAULT 0,
    vote_2 INTEGER DEFAULT 0,
    vote_3 INTEGER DEFAULT 0,
    vote_4 INTEGER DEFAULT 0,
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create genres table:
CREATE TABLE genres
(
    genre_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create publishers table:
CREATE TABLE publishers
(
    publisher_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create users table:
CREATE TABLE users
(
    user_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(120) UNIQUE NOT NULL,
    title VARCHAR(60),
    lastname VARCHAR(60) NOT NULL,
    firstname VARCHAR(60) NOT NULL,
    companyname VARCHAR(150),
    address1 VARCHAR(300) NOT NULL,
    address2 VARCHAR(300),
    city VARCHAR(60) NOT NULL,
    province VARCHAR(2) NOT NULL,
    country VARCHAR(20) NOT NULL DEFAULT 'CANADA',
    postalcode VARCHAR(7) NOT NULL,
    homephone VARCHAR(20),
    cellphone VARCHAR(20),
    
    FOREIGN KEY (province)
        REFERENCES taxrates(region)
);

-- Create credentials table:
CREATE TABLE credentials
(
    user_id INTEGER PRIMARY KEY,
    hash BINARY(32) NOT NULL,
    salt CHAR(28) NOT NULL,
    manager BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);

-- Create authors table:
CREATE TABLE authors
(
    author_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(60) NOT NULL,
    lastname VARCHAR(60),
    title VARCHAR(20) DEFAULT NULL
);

-- Create books table:
-- FK: publisher_id --> publishers
CREATE TABLE books
(
    isbn BIGINT PRIMARY KEY,
    title VARCHAR(260) NOT NULL,
    publisher_id INTEGER NOT NULL,
    pub_date DATE NOT NULL,
    num_pages INTEGER NOT NULL,
    description text NOT NULL,
    wholesale_price DECIMAL(6,2) NOT NULL,
    list_price DECIMAL(6,2) NOT NULL,
    sale_price DECIMAL(6,2) NOT NULL,
    min_age INTEGER NOT NULL,
    max_age INTEGER,
    acquired_stamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    removed BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (publisher_id)
        REFERENCES publishers(publisher_id)
);

-- Create book-genre bridging table:
-- FK: isbn --> books
-- FK: genre_id --> genres
CREATE TABLE bookgenre
(
    bookgenre_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT,
    genre_id INTEGER NOT NULL,

    FOREIGN KEY (genre_id)
        REFERENCES genres(genre_id),

    FOREIGN KEY (isbn)
        REFERENCES books(isbn)
);

-- Create author-book bridging table:
-- FK: isbn --> books
-- FK: author_id --> authors
CREATE TABLE bookauthor
(
    bookauthor_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    author_id INTEGER NOT NULL,

    FOREIGN KEY (isbn)
        REFERENCES books(isbn),

    FOREIGN KEY (author_id)
        REFERENCES authors(author_id)
);

-- Create formats table:
CREATE TABLE formats
(
    format_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    extension VARCHAR(5) NOT NULL,
    name VARCHAR(60) NOT NULL
);

-- Create book-format bridging table:
-- FK: isbn --> books
-- FK: format_id --> formats
CREATE TABLE bookformat
(
    bookformat_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    format_id INTEGER NOT NULL,
    
    FOREIGN KEY (isbn)
        REFERENCES books(isbn),

    FOREIGN KEY (format_id)
        REFERENCES formats(format_id)
);

-- Create order table:
-- FK: user_id --> users
CREATE TABLE orders
(
    order_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    user_id INTEGER NOT NULL,
    purchase_date DATE NOT NULL,
    gst_rate DECIMAL(6,3),
    pst_rate DECIMAL(6,3),
    hst_rate DECIMAL(6,3),

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);

-- Create orderitem table:
-- FK: order_id --> orders
-- FK: isbn --> books
CREATE TABLE orderitems
(
    orderitems_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    order_id INTEGER NOT NULL,
    isbn BIGINT NOT NULL,
    price DECIMAL(6,2),
    removed BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (order_id)
        REFERENCES orders(order_id),

    FOREIGN KEY (isbn)
        REFERENCES books(isbn)
);


-- Create reviews table:
-- FK: isbn --> books
-- FK: user_id --> users
CREATE TABLE reviews
(
    review_id bigint AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    postdate TIMESTAMP NOT NULL DEFAULT NOW(),
    user_id INTEGER NOT NULL,
    rating INTEGER NOT NULL,
    text VARCHAR(750) NOT NULL,
    approved BOOLEAN DEFAULT FALSE,


    FOREIGN KEY (isbn)
        REFERENCES books(isbn),

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);
