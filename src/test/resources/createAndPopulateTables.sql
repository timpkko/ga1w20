USE ga1w20;

DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS orderitems;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS bookformat;
DROP TABLE IF EXISTS bookauthor;
DROP TABLE IF EXISTS bookgenre;
DROP TABLE IF EXISTS books;
DROP TABLE IF EXISTS formats;
DROP TABLE IF EXISTS authors;
DROP TABLE IF EXISTS credentials;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS publishers;
DROP TABLE IF EXISTS genres;
DROP TABLE IF EXISTS surveys;
DROP TABLE IF EXISTS rssfeeds;
DROP TABLE IF EXISTS taxrates;
DROP TABLE IF EXISTS banners;

-- Create banner ads table
CREATE TABLE banners
(
    ad_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(2083),
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create tax rate table
CREATE TABLE taxrates
(
    region VARCHAR(2) PRIMARY KEY,
    gst_rate DECIMAL(6,3),
    pst_rate DECIMAL(6,3),
    hst_rate DECIMAL(6,3)
);

-- Create RSS feed table
CREATE TABLE rssfeeds
(
    feed_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(2083) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create surveys table:
CREATE TABLE surveys
(
    survey_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    question VARCHAR(400) NOT NULL,
    answer_1 VARCHAR(100) NOT NULL,
    answer_2 VARCHAR(100) NOT NULL,
    answer_3 VARCHAR(100),
    answer_4 VARCHAR(100),
    vote_1 INTEGER DEFAULT 0,
    vote_2 INTEGER DEFAULT 0,
    vote_3 INTEGER DEFAULT 0,
    vote_4 INTEGER DEFAULT 0,
    active BOOLEAN NOT NULL DEFAULT FALSE
);

-- Create genres table:
CREATE TABLE genres
(
    genre_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create publishers table:
CREATE TABLE publishers
(
    publisher_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(60) UNIQUE NOT NULL
);

-- Create users table:
CREATE TABLE users
(
    user_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(120) UNIQUE NOT NULL,
    title VARCHAR(60),
    lastname VARCHAR(60) NOT NULL,
    firstname VARCHAR(60) NOT NULL,
    companyname VARCHAR(150),
    address1 VARCHAR(300) NOT NULL,
    address2 VARCHAR(300),
    city VARCHAR(60) NOT NULL,
    province VARCHAR(2) NOT NULL,
    country VARCHAR(20) NOT NULL DEFAULT 'CANADA',
    postalcode VARCHAR(7) NOT NULL,
    homephone VARCHAR(20),
    cellphone VARCHAR(20),
    
    FOREIGN KEY (province)
        REFERENCES taxrates(region)
);

-- Create credentials table:
CREATE TABLE credentials
(
    user_id INTEGER PRIMARY KEY,
    hash BINARY(32) NOT NULL,
    salt CHAR(28) NOT NULL,
    manager BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);

-- Create authors table:
CREATE TABLE authors
(
    author_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(60) NOT NULL,
    lastname VARCHAR(60),
    title VARCHAR(20) DEFAULT NULL
);

-- Create books table:
-- FK: publisher_id --> publishers
CREATE TABLE books
(
    isbn BIGINT PRIMARY KEY,
    title VARCHAR(260) NOT NULL,
    publisher_id INTEGER NOT NULL,
    pub_date DATE NOT NULL,
    num_pages INTEGER NOT NULL,
    description text NOT NULL,
    wholesale_price DECIMAL(6,2) NOT NULL,
    list_price DECIMAL(6,2) NOT NULL,
    sale_price DECIMAL(6,2) NOT NULL,
    min_age INTEGER NOT NULL,
    max_age INTEGER,
    acquired_stamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    removed BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (publisher_id)
        REFERENCES publishers(publisher_id)
);

-- Create book-genre bridging table:
-- FK: isbn --> books
-- FK: genre_id --> genres
CREATE TABLE bookgenre
(
    bookgenre_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT,
    genre_id INTEGER NOT NULL,

    FOREIGN KEY (genre_id)
        REFERENCES genres(genre_id) ON DELETE CASCADE,

    FOREIGN KEY (isbn)
        REFERENCES books(isbn) ON DELETE CASCADE
);

-- Create author-book bridging table:
-- FK: isbn --> books
-- FK: author_id --> authors
CREATE TABLE bookauthor
(
    bookauthor_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    author_id INTEGER NOT NULL,

    FOREIGN KEY (isbn)
        REFERENCES books(isbn) ON DELETE CASCADE,

    FOREIGN KEY (author_id)
        REFERENCES authors(author_id) ON DELETE CASCADE
);

-- Create formats table:
CREATE TABLE formats
(
    format_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    extension VARCHAR(5) NOT NULL,
    name VARCHAR(60) NOT NULL
);

-- Create book-format bridging table:
-- FK: isbn --> books
-- FK: format_id --> formats
CREATE TABLE bookformat
(
    bookformat_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    format_id INTEGER NOT NULL,
    
    FOREIGN KEY (isbn)
        REFERENCES books(isbn) ON DELETE CASCADE,

    FOREIGN KEY (format_id)
        REFERENCES formats(format_id) ON DELETE CASCADE
);

-- Create order table:
-- FK: user_id --> users
CREATE TABLE orders
(
    order_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    user_id INTEGER NOT NULL,
    purchase_date DATE NOT NULL,
    gst_rate DECIMAL(6,3),
    pst_rate DECIMAL(6,3),
    hst_rate DECIMAL(6,3),

    FOREIGN KEY (user_id)
        REFERENCES users(user_id)
);

-- Create orderitem table:
-- FK: order_id --> orders
-- FK: isbn --> books
CREATE TABLE orderitems
(
    orderitems_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    order_id INTEGER NOT NULL,
    isbn BIGINT NOT NULL,
    price DECIMAL(6,2),
    removed BOOLEAN NOT NULL DEFAULT FALSE,

    FOREIGN KEY (order_id)
        REFERENCES orders(order_id),

    FOREIGN KEY (isbn)
        REFERENCES books(isbn)
);


-- Create reviews table:
-- FK: isbn --> books
-- FK: user_id --> users
CREATE TABLE reviews
(
    review_id bigint AUTO_INCREMENT PRIMARY KEY,
    isbn BIGINT NOT NULL,
    postdate TIMESTAMP NOT NULL DEFAULT NOW(),
    user_id INTEGER NOT NULL,
    rating INTEGER NOT NULL,
    text VARCHAR(750) NOT NULL,
    approved BOOLEAN DEFAULT FALSE,


    FOREIGN KEY (isbn)
        REFERENCES books(isbn) ON DELETE CASCADE,

    FOREIGN KEY (user_id)
        REFERENCES users(user_id) ON DELETE CASCADE
);

-- Formats
INSERT INTO formats(format_id,extension,name) VALUES 
(1,'cbr','Comic Book Archive'),
(2,'cbz','Comic Book Archive'),
(3,'cb7','Comic Book Archive'),
(4,'cbt','Comic Book Archive'),
(5,'cba','Comic Book Archive'),
(6,'cbr','Comic Book Archive'),
(7,'djvu','DjVu'),
(8,'doc','DOC'),
(9,'docx','DOCX'),
(10,'epub','EPUB (IDPF)'),
(11,'fb2','FictionBook'),
(12,'html','HTML'),
(13,'ibook','iBooks'),
(14,'inf','INF'),
(15,'azw','Kindle'),
(16,'lit','Microsoft Reader'),
(17,'prc','Mobipocket'),
(18,'mobi','Mobipocket'),
(19,'exe','Multimedia EBook'),
(20,'pkg','Newton Book'),
(21,'pdb','eReader'),
(22,'txt','Plain text'),
(23,'pdb','Plucker'),
(24,'pdf','Portable Document Format'),
(25,'ps','PostScript'),
(26,'tr2','Tome Raider'),
(27,'tr3','Tome Raider'),
(28,'oxps','OpenXPS'),
(29,'xps','OpenXPS');

-- Tax rates
INSERT INTO taxrates(region, gst_rate, pst_rate, hst_rate) VALUES ('QC', 5, 9.975, 0),
('NL', 0, 0, 15),
('PE', 0, 0, 15),
('NS', 0, 0, 15),
('NB', 0, 0, 15),
('ON', 0, 0, 13),
('MB', 5, 7, 0),
('SK', 5, 6, 0),
('AB', 5, 0, 0),
('BC', 5, 7, 0),
('YT', 5, 0, 0),
('NT', 5, 0, 0),
('NU', 5, 0, 0);

-- Genres
INSERT INTO genres VALUES (1, 'Educational'),
(2, 'Puzzle'),
(3, 'Comic Book'),
(4, 'Fantasy'),
(5, 'Social Themes');

-- Publishers Table
INSERT INTO publishers(publisher_id, name) VALUES 
(1, 'Independently published'),
(2, 'Red Deer Press'),
(3, 'Theytus Books'),
(4, 'Groundwood Books'),
(5, 'Second Story Press'),
(6, 'Orca Book Publishers'),
(7, 'Tu Books'),
(8, 'Balzer + Bray'),
(9, 'Schwartz & Wade Books'),
(10, 'Atheneum Books for Young Readers'),
(11, 'Scholastic Australia'),
(12, 'Pajama Press'),
(13, 'Feiwel & Friends'),
(14, 'Hodder Children''s Books'),
(15, 'Kar-Ben Publishing'),
(16, 'Capstone Press'),
(17, 'Nancy Paulsen Books'),
(18, 'Candlewick Press'),
(19, 'National Geographic Children''s Books'),
(20, 'Everything'),
(21, 'Dover Publications'),
(22, 'Althea Press'),
(23, 'Elon Books'),
(24, 'HMH Books for Young Readers'),
(25, 'Boundless Movement'),
(26, 'Grosset & Dunlap'),
(27, 'Engage Books'),
(28, 'HarperCollins'),
(29, 'Walter Foster Jr.'),
(30, 'Delacorte Books for Young Readers'),
(31, 'DK'),
(32, 'Rodale Kids'),
(33, 'Jacquelyn Stagg'),
(34, 'Charlesbridge'),
(35, 'Buster Books'),
(36, 'Highlights Press'),
(37, 'Zephyros Press'),
(38, 'First Second'),
(39, 'Scholastic GRAPHIX'),
(40, 'Amulet Books'),
(41, 'Top Shelf Productions'),
(42, 'Random House'),
(43, 'KaBOOM!'),
(44, 'Andrews McMeel Publishing'),
(45, 'Cartoon Books'),
(46, 'Abrams'),
(47, 'Three Rivers Press'),
(48, 'BOOM!Box'),
(49, 'DC Comics'),
(50, 'Disney Press'),
(51, 'DG Books Publishing'),
(52, 'Phidal Publishing Inc.'),
(53, 'Marvel Press'),
(54, 'Bloomsbury USA');

-- Books Table
INSERT INTO books(isbn,title,publisher_id,pub_date,num_pages,description,wholesale_price,list_price,sale_price,min_age,max_age,removed) VALUES 
(9780062941008,'Peanut Goes for the Gold',28,'2020-03-31',32,'Jonathan Van Ness, the star of Netflix''s hit show Queer Eye, brings his signature humor and positivity to his empowering first picture book, inspiring readers of all ages to love being exactly who they are.Peanut Goes for the Gold is a charming, funny, and heartfelt picture book that follows the adventures of Peanut, a gender nonbinary guinea pig who does everything with their own personal flare.Peanut just has their own unique way of doing things. Whether it''s cartwheeling during basketball practice or cutting their own hair, this little guinea pig puts their own special twist on life. So when Peanut decides to be a rhythmic gymnast, they come up with a routine that they know is absolutely perfect, because it is absolutely, one hundred percent Peanut.This upbeat and hilarious picture book, inspired by Jonathan''s own childhood guinea pig, encourages children to not just be themselves—but to boldly and unapologetically love being themselves.Jonathan Van Ness brings his signature message of warmth, positivity, and self-love to this boldly original picture book that celebrates the joys of being true to yourself and the magic that comes from following your dreams.',8.00,15.16,0.00,4,6,FALSE)
, (9780889953673,'Nokum Is My Teacher',2,'2007-05-01',32,'Will you walk with me, Grandmother? Will you talk with me a while? I''m finding life confusing And I_m looking for some answers To questions all around me At that school and on the street. You have always been here for me - Will you help me learn to see? Nokum Is My Teacher is the poetically told story of a young aboriginal boy, posing questions to his grandmother, his "Nokum", about the wider world beyond the familiarity of their home and community. Through a series of questions, Nokum guides her grandson towards an understanding of his need to fit into and learn more about this large world beyond the reserve. Nokum offers her grandson a vision of a world he can enter through imagination and reading, while retaining respect for the ways of his people. By the conclusion of the book, the young grandson has learned many new ideas from his grandmother and discovered his own wisdom in dealing with the changes in his life. Nokum Is My Teacher is a delightfully packaged book and audio CD, combining the written text in English and Cree with the mesmerizing voice of author/storyteller extraordinaire David Bouchard. It is illustrated by the hauntingly beautiful artworks of Allan Sapp, Cree elder, Governor General''s Award-winner, and Officer of the Order of Canada. The singing and drumming are done by Alberta''s Northern Cree, who have been nominated for a Grammy Award (2007) in the ''Native American music album'' category. Nokum Is My Teacher is also available in French/Cree text and audio. This is the first of a series of aboriginal books David Bouchard is developing with Red Deer Press.''',25.00,50.00,40.00,5,NULL,FALSE)
, (9781894778145,'The Moccasins',3, '2007-07-28',16,'This is an endearing story of a young Aboriginal foster child who is given a special gift by his foster mother. Her gift of warmth and thoughtfulness helps her young foster children by encouraging self-esteem, acceptance and love. Written as a simple story, it speaks of a positive foster experience.''',5.00,10.95,9.00,4,NULL,FALSE)
, (9780888996596,'Shi-shi-etko',4,'2005-07-03',32,'Winner of the Anskohk Aboriginal Children''s Book of the Year Award. Finalist for the TD Canadian Children''s Literature Award, the Marilyn Baillie Picture Book Award and the Ruth Schwartz AwardIn just four days young Shi-shi-etko will have to leave her family and all that she knows to attend residential school.She spends her last days at home treasuring the beauty of her world -- the dancing sunlight, the tall grass, each shiny rock, the tadpoles in the creek, her grandfather''s paddle song. Her mother, father and grandmother, each in turn, share valuable teachings that they want her to remember. And so Shi-shi-etko carefully gathers her memories for safekeeping.Richly hued illustrations complement this gently moving and poetic account of a child who finds solace all around her, even though she is on the verge of great loss -- a loss that native people have endured for generations because of the residential schools system.',7.00,13.59,11.00,4,NULL,FALSE)
, (9781772600384,'The Water Walker',5,'2017-09-15',36,'The determined story of an Ojibwe grandmother (nokomis), Josephine Mandamin, and her great love for nibi (water). Nokomis walks to raise awareness of our need to protect nibi for future generations and for all life on the planet. She, along with other women, men and youth, has walked around all the Great Lakes from the four salt waters, or oceans, to Lake Superior. The walks are full of challenges, and by her example she challenges us all to take up our responsibility to protect our water, the giver of life, and to protect our planet for all generations.''',7.00,14.96,0.00,6,9,FALSE)
, (9781459802483,'Little You',6,'2013-04-01',24,'Richard Van Camp, internationally renowned storyteller and bestselling author of the hugely successful Welcome Song for Baby: A Lullaby for Newborns, has partnered with talented illustrator Julie Flett to create a tender board book for babies and toddlers that honors the child in everyone. With its delightful contemporary illustrations, Little You is perfect to be shared, read or sung to all the little people in your life--and the new little ones on the way!',5.00,9.9,0.00,3,6,FALSE)
, (9780062953452,'Busted by Breakfast',28,'2020-03-03',96,'Simon always has a lot to say. And sometimes he can''t stop talking—even in the middle of class. When Simon gets in trouble for jabbering at school, his best friends, Molly and Rosie, think up a plan to keep him from getting grounded at home! It involves cars, suds, and pink plastic flamingos! But will their big plan turn into an even bigger disaster?',3.00,5.99,0.00,6,8,FALSE)
, (9780062953414,'The Candy Caper',28,'2020-03-03',96,'Molly gets things stuck in her head sometimes. When she sees a jar of candy on Principal Shelton''s desk, she absolutely needs to know how many candies are in that jar! Luckily, her two best friends, Simon and Rosie, are ready to help her find the answer—even if it means detention for all of them!''',3.00,5.99,0.00,6,8,FALSE)
, (9781554988334,'Coyote Tales',4,'2017-10-01',56,'Two tales, set in a time when animals and human beings still talked to each other, display Thomas King''s cheeky humor and master storytelling skills. Freshly illustrated and reissued as an early chapter book, these stories are perfect for newly independent readers.In Coyote Sings to the Moon, Coyote is at first the cause of misfortune. In those days, when the moon was much brighter and closer to the earth, Old Woman and the animals would sing to her each night. Coyote attempts to join them, but his voice is so terrible they beg him to stop. He is crushed and lashes out — who needs Moon anyway? Furious, Moon dives into a pond, plunging the world into darkness. But clever Old Woman comes up with a plan to send Moon back up into the sky and, thanks to Coyote, there she stays.In Coyote''s New Suit, mischievous Raven wreaks havoc when she suggests that Coyote''s toasty brown suit is not the finest in the forest, thus prompting him to steal suits belonging to all the other animals. Meanwhile, Raven tells the other animals to borrow clothes from the humans'' camp. When Coyote finds that his closet is too full, Raven slyly suggests he hold a yard sale, then sends the human beings (in their underwear) and the animals (in their ill-fitting human clothes) along for the fun. A hilarious illustration of the consequences of wanting more than we need.''',8.00,16.95,0.00,6,NULL,FALSE)
, (9781620142639,'I Am Alfonso Jones',7,'2017-10-12',168,'Alfonso Jones can''t wait to play the role of Hamlet in his school''s hip-hop rendition of the classic Shakespearean play. He also wants to let his best friend, Danetta, know how he really feels about her. But as he is buying his first suit, an off-duty police officer mistakes a clothes hanger for a gun, and he shoots Alfonso.When Alfonso wakes up in the afterlife, he''s on a ghost train guided by well-known victims of police shootings, who teach him what he needs to know about this subterranean spiritual world. Meanwhile, Alfonso''s family and friends struggle with their grief and seek justice for Alfonso in the streets. As they confront their new realities, both Alfonso and those he loves realize the work that lies ahead in the fight for justice.In the first graphic novel for young readers to focus on police brutality and the Black Lives Matter movement, as in Hamlet, the dead shall speak—and the living yield even more surprises.Foreword by Bryan Stevenson, Executive Director of the Equal Justice Initiative and author of Just Mercy''',12.00,24.95,0.00,11,NULL,FALSE)
, (9780062498564,'On the Come Up',8,'2019-02-05',447,'Sixteen-year-old Bri wants to be one of the greatest rappers of all time. Or at least make it out of her neighborhood one day. As the daughter of an underground rap legend who died before he hit big, Bri''s got big shoes to fill. But now that her mom has unexpectedly lost her job, food banks and shutoff notices are as much a part of Bri''s life as beats and rhymes. With bills piling up and homelessness staring her family down, Bri no longer just wants to make it—she has to make it.On the Come Up is Angie Thomas''s homage to hip-hop, the art that sparked her passion for storytelling and continues to inspire her to this day. It is the story of fighting for your dreams, even as the odds are stacked against you',12.00,23.99,0.00,14,17,FALSE)
, (9781524769567,'I Walk with Vanessa: A Story about a Simple Act of Kindness',9,'2018-04-28',32,'This simple yet powerful picture book tells the story of an elementary school girl named Vanessa who is bullied and a fellow student who witnesses the act and is at first unsure of how to help. I Walk with Vanessa explores the feelings of helplessness and anger that arise in the wake of seeing a classmate treated badly, and shows how a single act of kindness can lead to an entire community joining in to help. With themes of acceptance, kindness, and strength in numbers, this timeless and profound feel-good story will resonate with readers young and old.''',6.00,12.99,0.00,4,8,FALSE)
, (9781481400701,'The Youngest Marcher: The Story of Audrey Faye Hendricks, a Young Civil Rights Activist',10,'2017-01-17',40,'Meet the youngest known child to be arrested for a civil rights protest in Birmingham, Alabama, 1963, in this moving picture book that proves you''re never too little to make a difference.Nine-year-old Audrey Faye Hendricks intended to go places and do things like anybody else. So when she heard grown-ups talk about wiping out Birmingham''s segregation laws, she spoke up. As she listened to the preacher''s words, smooth as glass, she sat up tall. And when she heard the plan—picket those white stores! March to protest those unfair laws! Fill the jails!—she stepped right up and said, I''ll do it! She was going to j-a-a-il! Audrey Faye Hendricks was confident and bold and brave as can be, and hers is the remarkable and inspiring story of one child''s role in the Civil Rights Movement.''',8.00,16.79,15.00,5,NULL,FALSE)
, (9781743629000,'Out',11,'2016-01-06',32,'I''m called an asylum seeker, but that''s not my name.\n
 A little girl and her mother have fled their homeland, making the long and treacherous journey by boat to seek asylum. Timely, powerful and moving, Out celebrates the triumph of the human spirit in the darkest times, and the many paths people take to build a new life.',12.00,24.99,0.00,4,7,FALSE)
, (9781772780109,'My Beautiful Birds',12,'2018-04-08',32,'Behind Sami, the Syrian skyline is full of smoke. The boy follows his family and all his neighbours in a long line, as they trudge through the sands and hills to escape the bombs that have destroyed their homes. But all Sami can think of is his pet pigeons--will they escape too? When they reach a refugee camp and are safe at last, everyone settles into the tent city. But though the children start to play and go to school again, Sami can''t join in. When he is given paper and paint, all he can do is smear his painting with black. He can''t forget his birds and what his family has left behind. One day a canary, a dove, and a rose finch fly into the camp. They flutter around Sami and settle on his outstretched arms. For Sami it is one step in a long healing process at last. A gentle yet moving story of refugees of the Syrian civil war, My Beautiful Birds illuminates the ongoing crisis as it affects its children. It shows the reality of the refugee camps, where people attempt to pick up their lives and carry on. And it reveals the hope of generations of people as they struggle to redefine home.',8.00,16.95,0.00,4,10,FALSE)
, (9780312625993,'The Forgiveness Garden',13,'2012-10-30',32,'A long time ago and far away--although it could be here, and it could be now--a boy threw a stone and injured a girl. For as long as anyone could remember, their families had been enemies, and their towns as well, so it was no surprise that something bad had happened. Hate had happened. Revenge had happened. And that inspired more hate and more calls for revenge. But this time, a young girl decided to try something different...Inspired by the original Garden of Forgiveness in Beirut, Lebanon, and the movement that has grown up around it, Lauren Thompson has created a timeless parable for all ages that shows readers a better way to resolve conflicts and emphasizes the importance of moving forward together.''',4.00,8.99,0.00,4,6,FALSE)
, (9781492662143,'Illegal',14,'2017-10-05',144,'This is a powerful and timely story about one boy''s epic journey across Africa to Europe, a graphic novel for all children with glorious colour artwork throughout. From Eoin Colfer, previously Irish Children''s Laureate, and the team behind his bestselling Artemis Fowl graphic novels. \nEbo: alone.His sister left months ago. Now his brother has disappeared too, and Ebo knows it can only be to make the hazardous journey to Europe. Ebo''s epic journey takes him across the Sahara Desert to the dangerous streets of Tripoli, and finally out to the merciless sea. But with every step he holds on to his hope for a new life, and a reunion with his sister.\n*Winner of the Judges'' Special Award at the Children''s Books Ireland Book of the Year Awards*\n\n''Beautifully realised and punchily told.'' Alex O''Connell, The Times Children''s Book of the Week\n\n''A powerful, compelling work, evocatively illustrated ... It would take a hard heart not to be moved by this book.'' Financial Times\n',4.00,7.95,0.00,10,NULL,FALSE)
, (9781467711944,'The Whispering Town',15,'2015-02-07',32,'The dramatic story of neighbors in a small Danish fishing village who, during the Holocaust, shelter a Jewish family waiting to be ferried to safety in Sweden. It is 1943 in Nazi-occupied Denmark. Anett and her parents are hiding a Jewish woman and her son, Carl, in their cellar until a fishing boat can take them across the sound to neutral Sweden. The soldiers patrolling their street are growing suspicious, so Carl and his mama must make their way to the harbor despite a cloudy sky with no moon to guide them. Worried about their safety, Anett devises a clever and unusual plan for their safe passage to the harbor. Based on a true story.''',13.00,25.95,0.00,8,NULL,FALSE)
, (9781515782223,'Hedy''s Journey: The True Story of a Hungarian Girl Fleeing the Holocaust',16,'2017-06-01',40,'It is 1941. Hedy and her family are Jewish, and the Nazi party is rising. Hedy''s family is no longer safe in their home in Hungary. They decide to flee to America, but because of their circumstances, sixteen-year-old Hedy must make her way through Europe alone. Will luck be with her? Will she be brave? Join Hedy on her journey-where she encounters good fortune and misfortune, a kind helper and cruel soldiers, a reunion and a tragedy-and discover how Hedy is both lucky and brave. Hedy''s Journey adds an important voice to the canon of Holocaust stories, and her courage will make a lasting impact on young readers.',7.00,14.25,0.00,10,NULL,FALSE)
, (9780399246531,'The Day You Begin',17,'2018-08-01',32,'National Book Award winner Jacqueline Woodson and two-time Pura Belpre Illustrator Award winner Rafael Lopez have teamed up to create a poignant, yet heartening book about finding courage to connect, even when you feel scared and alone. There will be times when you walk into a room and no one there is quite like you.There are many reasons to feel different. Maybe it''s how you look or talk, or where you''re from.',10.00,20.69,17.00,5,8,FALSE)
, (9780763693558,'Alma and How She Got Her Name',18,'2018-05-10',32,'What''s in a name? For one little girl, her very long name tells the vibrant story of where she came from and who she may one day be. If you ask her, Alma Sofia Esperanza Jose Pura Candela has way too many names: six! How did such a small person wind up with such a large name? Alma turns to Daddy for an answer and learns of Sofia, the grandmother who loved books and flowers.',11.00,21.99,0.00,4,8,FALSE)
, (9781426310492,'5,000 Awesome Facts (About Everything!)',19,'2012-08-14',224,'Presenting the next must-have, fun-filled gift book from the team that created Ultimate Weird But True, 5,000 Cool Facts About Everything treats kids to brain candy and eye candy all rolled into one treasure trove of high-interest fascinating facts.\n
Lively and information-packed, this book is literally busting its covers with fascinating, fun-tastic facts on super, sensational topics that kids love. Who knew that there were so many sweet things to learn about chocolate or that a dozen delicious details about peanut butter would show up on a page with a few splotches of jelly to whet our appetites? Keep turning and a terrifyingly toothy shark tells you all about himself, while other spreads lay out tons of tips on toys and games, mysteries of history, robots and reptiles, sports and spies, wacky words, and so much more! A visual feast of colorful photographs surrounded by swirling, tipping, expanding, and climbing bits of information in a high-energy design, this book will satisfy both the casual browser and the truly fact obsessed.',6.00,12.99,0.00,8,12,FALSE)
, (9781580625579,'The Everything Kids'' Science Experiments Book: Boil Ice, Float Water, Measure Gravity-Challenge the World Around You!',20,'2001-10-01',144,'With The Everything Kids'' Science Experiments Book, all you need to do is gather a few household items and you can recreate dozens of mind-blowing, kid-tested science experiments. High school science teacher Tom Robinson shows you how to expand your scientific horizons from biology, chemistry, physics, technology, and engineering—to outer space.\n
You''ll discover answers to questions like:\n
—Is it possible to blow up a balloon without actually blowing into it?\n
—What is inside coins?\n
—Can a magnet ever be "turned off"?\n
—Do toilets always flush in the same direction?\n
—Can a swimming pool be cleaned with just the breath of one person?\n
Whether you''ve always been interested in STEM or you''re looking for a cool science fair project, you''ll want to test these fun and educational experiments for yourself!',4.00,8.00,5.00,7,12,FALSE)
, (9780486468211,'My First Human Body Book',21,'2009-01-19',32,'It''s easy to learn about your body! This cool coloring book includes 28 drawings that explore muscles, bones, lungs, and more.\n
You can read how your voice box works, how your tongue tastes food, how your DNA is different from your family and friends, and more.',2.00,3.99,0.00,6,10,TRUE)
, (9781641520416,'Thriving with ADHD Workbook for Kids: 60 Fun Activities to Help Children Self-Regulate, Focus, and Succeed',22,'2018-06-26',148,'A toolbox for kids to understand their ADHD and live happier, healthier lives\n
For millions of kids who live with ADHD, feelings of loneliness, frustration, and helplessness are all too common. Thriving with ADHD is a workbook specially designed to helps kids with ADHD develop essential skills for managing their ADHD symptoms, while also providing a powerful message of hope and encouragement for their future.\n
In Thriving with ADHD family therapist Kelli Miller draws upon both her professional expertise, as well as her experience as the mother of two ADHD children, to help kids reframe the way they think about ADHD and discover that they have special talents that are unique to them. With fun activities that engage their busy minds, Thriving with ADHD offers kids a better understanding of themselves, their ADHD, and the simple things they can do to feel more confident and in control.\n
Thriving with ADHD includes:\n
 • An overview of ADHD that includes common symptoms, ADHD types, how ADHD can be an asset, and areas where kids could use additional support.\n
 • Exercises that build helpful skills for dealing with anger, staying focused, controlling impulses, and making mindful decisions.\n
 • Action-oriented lessons for daily life that teach practical tools such as creating a morning routine, making a homework chart, and expressing themselves when they''re upset.\n
 • Techniques for self-regulation and organization that help kids handle any emotion or obstacle, so they can spend their energy having fun and just being kids!\n
Though ADHD is very common, its symptoms can make life challenging for kids. Through the knowledgeable guidance and support in Thriving with ADHD kids will see that ADHD isn''t in charge of their lives—they are.',4.00,7.66,0.00,6,12,FALSE)
, (9780692848388,'What Should Danny Do?',23,'2017-05-17',68,'FUN. INTERACTIVE. EMPOWERING. THE BOOK THEY''LL LOVE TO READ AGAIN AND AGAIN!\n
With 9 Stories in 1, the fun never ends! What Should Danny Do? is an innovative, interactive book that empowers kids with the understanding that their choices will shape their days, and ultimately their lives into what they will be. Written in a "Choose Your Own Story" style, the book follows Danny, a Superhero-in-Training, through his day as he encounters choices that kids face on a daily basis. As your children navigate through the different story lines, they will begin to realize that their choices for Danny shaped his day into what it became. And in turn, their choices for themselves will shape their days, and ultimately their lives, into what they will be.\n
Boys and girls both love and relate to Danny, while enjoying the interactive nature of the book--they never know what will come next! Parents and Teachers love the social-emotional skills the book teaches through empowering kids to make positive choices while demonstrating the natural consequences to negative choices. A "must-have" on every bookshelf.',4.00,8.00,0.00,3,NULL,FALSE)
, (9780547557991,'The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest—and Most Surprising—Animals on Earth',24,'2013-10-03',208,'Animals smooth and spiky, fast and slow, hop and waddle through the two hundred plus pages of the Caldecott Honor artist Steve Jenkins''s most impressive nonfiction offering yet. Sections such as "Animal Senses," "Animal Extremes," and "The Story of Life" burst with fascinating facts and infographics that will have trivia buffs breathlessly asking, "Do you know a termite queen can produce up to 30,000 eggs a day?" Jenkins''s color-rich cut- and torn-paper artwork is as strikingly vivid as ever. Rounding out this bountiful browsers'' almanac of more than three hundred animals is a discussion of the artist''s bookmaking process, an animal index, a glossary, and a bibliography. A bookshelf essential!',8.00,15.00,11.00,6,9,FALSE)
, (9781426307041,'National Geographic Little Kids First Big Book of Animals',19,'2010-10-12',128,'The National Geographic Little Kids First Big Book of Animals is an adorable animal reference sure to be welcomed by parents and librarians alike. Filled with fluffy and scaly creatures big and small, this appealing book introduces the youngest explorers to the world of wildlife, using a child-friendly format inspired by the blockbuster National Geographic Little Kids magazine. This exciting new reference for the very young mirrors the magazine''s square shape, readable fonts, and fun content, to keep little ones thrilled with every colorful page.\n
Little Kids First Big Book of Animals devotes four pages each to 32 high-interest creatures, including dolphins, tigers, butterflies, frogs, penguins, wolves, and pandas. More than 150 of National Geographic''s most charming animal photos illustrate the profiles, which feature just the kind of facts that little kids want to know—the creature''s size, diet, home, and more.\n
Child-friendly text explains how animal parents take care of their young, how baby animals change as they grow, and how they learn to hunt and eat. The brief text, large type, and appealing profiles are perfect for young readers to enjoy on their own, or for parents and other caregivers to read aloud. These animal tales will quickly become favorites at storytime, bedtime, and any other time.',6.00,12.00,0.00,4,8,FALSE)
, (9781426310140,'National Geographic Little Kids First Big Book of Space',19,'2012-10-09',128,'This beautiful book is the latest addition to the National Geographic Little Kids First Big Book series. These colorful pages will introduce young children to the wonders of space, with colorful illustrations by David Aguilar and simple text that is perfect for beginning readers or for reading aloud. The book will explain basic concepts of space, beginning with what is most familiar to kids and expanding out into the universe.',4.00,7.66,0.00,4,8,FALSE)
, (9781426307935,'National Geographic Little Kids First Big Book of Why',19,'2011-05-10',128,'Linking to a popular feature in the super successful National Geographic Little Kids magazine, this book brings the browsable fun of the bestselling National Geographic Kids Almanac to a new audience: preschoolers! Using an interactive question-and-answer format and content grounded in a child''s immediate world, the Big Book of Why delivers lively information, hands-on games, simple recipes, crafts, and more. What makes a car go? How does mushy dough become a crispy cookie? What does the doctor see in my throat? An essential parent reference, The Big Book of Why invites children to ask big questions, think big thoughts, and get answers that are accurate, engaging, level-appropriate, and based on sound educational findings. It helps prepare preschoolers for school in an interactive way—the very best way to foster learning at this age, according to research. Highly photographic and playful, this big book is an adventure in exploration.',7.00,13.00,0.00,3,7,FALSE)
, (9781426308468,'National Geographic Little Kids First Big Book of Dinosaurs',19,'2011-10-11',128,'The third title in National Geographic Little Kids First Big Book series, this book is for kids 4- to 8-years-old who LOVE dinos! The prehistoric world comes alive with dinosaurs small, big, giant, and gigantic, with stunning illustrations by Franco Tempesta—who illustrated National Geographic Kids The Ultimate Dinopedia. Bursting with fun facts and age appropriate information, each spread features a different dinosaur, along with simple text in big type that is perfect for little kids. Young dino fans will love the interactivity included in every chapter, and parents will appreciate tips to help carry readers'' experience beyond the page.',4.00,7.65,0.00,4,8,FALSE)
, (9781732596344,'My Magical Words',25,'2019-05-14',40,'Do you want to increase confidence in your children and promote strong self-esteem?\n
Teach children their words have power and how they speak about themselves is important!\n
What we say we feel! What we feel we believe! What we believe we become! Have you ever heard your child say...I am bad at it! Nobody likes me. I can''t do it!\n
It''s heartbreaking to hear children say negative things about themselves, but ALL children struggle with learning to manage emotions. And now you can help!\n
My Magical Words is the experience children need to grow self-love and build confidence. It is a first book of positive affirmations.\n
Children will learn to SAY I am statements that empower them to FEEL and BELIEVE they are special, loved, smart, beautiful, strong, healthy, and more.\n
Your special time with your children will soar to a new level as you show them that words have power and what they say about themselves matters. Teaching your children positive self-talk at an early age will lay the foundation for future success!\n
A perfect book for boys and girls ages 3, 4, 5, 6, 7 or 8. Get the child or classroom teacher in your life a copy today!\n
If you loved other books to inspire young thinkers to greatness such as Kindness Starts with You, I Can Handle It, The Wonderful Things You Will Be, or I Wish You More, then this book will be the perfect addition to your child''s library collection.',3.00,5.00,0.00,4,8,FALSE)
, (9780448405179,'What''s Out There?: A Book about Space',26,'1993-03-24',32,'What is the sun made of? What causes night and day? Why does the moon change shape? Colorful collage illustrations and an easy-to-understand text bring planets, stars, comets, and the wondrous things out there in space right down to earth in a simple introduction to the solar system for young armchair astronauts.',2.00,3.99,0.00,4,8,FALSE)
, (9781641520928,'Anger Management Workbook for Kids: 50 Fun Activities to Help Children Stay Calm and Make Better Choices When They Feel Mad',22,'2018-11-27',160,'Help your child gain control of their emotions with interactive exercises and activities\n
Everyone gets angry, but teaching kids how to respond to anger is what really matters. The Anger Management Workbook for Kids offers fun, interactive activities to help kids handle powerful emotions for a lifetime of healthy behavioral choices.\n
From drawing a picture of what anger looks like to building a vocabulary for communicating feelings, the activities in this workbook give kids ages 6-12 the skills to understand and talk about anger habits and triggers. With this foundation, kids will learn positive and proactive strategies to deal with anger through gratitude, friendliness, and self-kindness.\n
At home, school, or with friends, the Anger Management Workbook for Kids equips kids to take control of anger, with:\n
    A close look at anger that helps kids and parents identify habits and triggers, and recognize how anger feels to them.\n
    Interactive exercises that provide a fun format for learning how to communicate feelings, needs, and wants to take control of angry outbursts.\n
    Feel-good habits that help kids develop better responses to anger by cultivating self-kindness, joy, and appreciation.\n
Anger is a regular emotion just like joy, sadness, and fear—but sometimes anger acts bossy. Give your kids to the power to say STOP to anger with the Anger Management Workbook for Kids.',6.00,12.00,0.00,6,11,FALSE)
, (9781772261059,'The Toddler''s Handbook: Numbers, Colors, Shapes, Sizes, ABC Animals, Opposites, and Sounds, with over 100 Words that every Kid should Know',27,'2015-09-01',48,'The Toddler''s Handbook introduces 17 basic concepts. Included are numbers, colors, shapes, sizes, ABCs, animals, opposites, sounds, actions, sports, food, tableware, clothes, engines, emotions, body, and time. This book develops early language skills using 174 words that every kid should know. Vibrant colors and images are designed to attract the attention of babies and toddlers. This book will help children learn a variety of important concepts before preschool.',2.00,3.00,0.00,3,6,FALSE)
, (9781097522132,'I Need You to Know: The ABC''s of Black Girl Magic',1,'2019-05-08',46,'A children''s coloring book filled with all things Black Girl Magic from A to Z! The "I Need You To Know..." series depicts our children beautifully from their skin complexion to their hair type. The book is educational and have encouraging words from A-Z. These coloring books have proven to speak to diversity through coloring when shared with children of all backgrounds!',5.00,9.14,0.00,3,7,FALSE)
, (9781633226241,'ABC for Me: ABC What Can She Be?: Girls can be anything they want to be, from A to Z',29,'2018-10-02',36,'ABC What Can She Be? presents a world of possibilities—from astronaut to zoologist and everything in between—for all little girls with big dreams.\n
Not even the sky is the limit with this fun approach to learning the alphabet! In this title from Walter Foster Jr, ABC What Can She Be? encourages young girls by presenting a colorful variety of choices for their future careers. Talented illustrator Jessie Ford artfully pairs the letters of the alphabet with vibrant, eye-catching illustrations that paint an inspiring picture for budding trailblazers everywhere. Representing all kinds of girls,ABC What Can She Be? depicts girls with different colors, sizes, shapes, and abilities in both traditional and nontraditional occupations.\n
ABC What Can She Be? explores 26 different career paths, including engineer, writer, neurosurgeon, software engineer, and pilot. Each page introduces a letter of the alphabet with bright artwork and highlights a career that is fun, challenging, and makes a big impact in its own way. These 26 careers are just some of the things she can be!\n
A boldly illustrated, fun family read, ABC What Can She Be? is a great way for parents to introduce their small children to the bright futures before them. Girls can dream big and do anything!\n
With endearing illustrations and mindful concepts, the ABC for Me series pairs each letter of the alphabet with words that promote big dreams and healthy living',5.00,9.50,0.00,3,5,FALSE)
, (9780385320436,'Mistakes That Worked: 40 Familiar Inventions & How They Came to Be',30,'1994-05-01',96,'Do you know how many things in your daily life were invented by accident?\n
SANDWICHES came about when an English earl was too busy gambling to eat his meal and needed to keep one hand free. POTATO CHIPS were first cooked by a chef who was furious when a customer complained that his fried potatoes weren''t thin enough. Coca-Cola, Silly Putty, and X rays have fascinating stories behind them too! Their unusual tales, and many more, along with hilarious cartoons and weird, amazing facts, make up this fun-filled book about everyday items that had surprisingly haphazard beginnings.\n
And don''t miss Eat Your Words about the fascinating language of food!\n
Praise for Mistakes That Worked:\n
"A splendid book that is as informative as it is entertaining . . . a gem." —Booklist, Starred',5.00,9.71,0.00,8,12,FALSE)
, (9781465414182,'History Year by Year: The History of the World, from the Stone Age to the Digital Age',31,'2013-08-19',320,'Featuring more than 1,500 images that bring the story of the past to life through a detailed timeline, this visual world history book helps children navigate the influences, patterns, and connections between historical events, beginning with prehistory and running up to the Arab Spring.\n
Budding historians will learn about the history of humans across the world in History Year by Year. Spreads highlight major historical eras including the Renaissance and the French Revolution, while quotations from primary and secondary sources provide further insight and give proper historical context. Kids will love the "child of the time" feature, which details the experience of children during important historical periods, including Ancient Egypt, Viking England, the Industrial Revolution, and World War II.\n
Created in association with the Smithsonian Institution, History Year by Year is a visual journey throughout time and an invaluable reference for kids who want to connect the dots of history across the globe.',9.00,18.69,0.00,9,12,FALSE)
, (9781623368890,'The Fantastic Body: What Makes You Tick & How You Get Sick',32,'2017-11-07',256,'A comprehensive guide to the human body meets hilarious kid humor in this gorgeous full color book filled with more than 1,000 diagrams and fun facts!\n
Written by a pediatrician, this is the ultimate kids'' reference guide to the human body! Jam-packed with fun facts, cool diagrams, and gross stories galore, this go-to guide will captivate curious readers for hours on end. Kids can also take their learning beyond reading the book, with DIY projects that demonstrate different body functions and tips for making their regular checkups less scary.\n
Through humor, science, and engaging illustrations, this fun and comprehensive anatomy book is the perfect gift for kids wanting to know more about the mysterious stuff going on inside their bodies.',4.00,7.93,0.00,8,12,TRUE)
, (9781775183310,'Kindness Starts With You - At School',33,'2018-04-22',27,'Kindness is the single most powerful thing that we can teach our children.
Follow Maddy through her day at school, where your child will learn how easy it can be to spread kindness!\n
From taking turns on the swing to including everyone in the game - this storybook shows that no act of kindness, no matter how small, is ever wasted!\n
A lightbulb lesson of kindness is found on each page!\n
Included in the book is a Kindness BINGO download and a Weekly Kindness Challenge to help encourage your child to: Say Sorry, Be Polite, Take Turns, Be a Helping Hand, Include Others, and Show Respect.\n
If you value raising kind kids that make the world a better place, then this book is for you!',1.00,2.29,0.00,5,10,FALSE)
, (9781936140404,'Tough Puzzles for Smart Kids',34,'2011-09-01',80,'Smart solvers will have a blast putting together jigsaw-style pieces to create larger shapes, decoding cryptograms to reveal a secret message, and investigating optical illusions. With futoshiki to tackle, wickedly difficult mazes to weave through, and loads of wordplay, this compilation offers hours of satisfaction.',3.00,5.89,0.00,8,12,FALSE)
, (9781780552491,'Brain Games for Clever Kids: Puzzles to Exercise Your Mind',35,'2014-10-01',192,'The perfect companion for vacation, this collection contains more than 90 puzzles, including memory, word, and number workouts; codes; battleships; and mind-bending spot-the-differences. There are hours of fun to be had with Japanese puzzles, including hanjie, kakuro, hitori, sudoku, and lots more. Let the brain games begin.',5.00,9.68,0.00,8,12,FALSE)
, (9781580626873,'The Everything Kids'' Puzzle Book: Mazes, Word Games, Puzzles & More! Hours of Fun!',20,'2002-03-01',144,'Wind your way through pages of endless fun! Decode a secret message using the phases of the moon. Wind your way through a pizza maze. Find hidden presidents, borrow some drachmas, and unscramble an invention time line. Play super duper tic-tac-toe, classic hangman, and match up snowflakes. And look for Mervin the Mouse every time you turn the page—he''s watching from his hiding place to help you through this wild and whacky jam-packed puzzle book! Sharpen more than one pencil--there are enough puzzles here to keep you entertained for hours!',6.00,11.69,0.00,6,10,FALSE)
, (9781629794228,'Brain Teasers: Mind-boggling quizzes, trivia, and logic puzzles',36,'2015-09-01',64,'From the editors of Highlights'' ever-popular Puzzlemania and Puzzle Buzz series, this curated collection of brain-boggling fun will keep challenge seekers and logic-puzzle lovers entertained for hours. Formatted in an on-the-go, fun-to-share activity pad format, it contains more than 30 trivia, logic, and super-challenge puzzles, plus lots of bonus jokes,cartoons, quizzes (and more) that make it an experience that is uniquely Highlights. The four-color puzzles are on one side of the page, with the bonus content on the other side, making it an engaging back-to-back experience.',3.00,6.92,0.00,6,NULL,FALSE)
, (9781629797694,'Animal Friends',36,'2017-02-28',48,'Nobody does puzzles like Highlights™! With bright illustrations, photos and puzzles at various levels of complexity, the activities in Puzzlemania® Animal Friends will entertain kids for hours. This animal-themed collection is packed with a variety of kids'' favorite puzzles, including mazes, number puzzles, wordplay, brainteasers, matching, and our ever-popular Hidden Pictures® puzzles. From the jungle to the farm, this title features all the animals that kids love to interact with.',10.00,19.80,0.00,6,NULL,FALSE)
, (9781629796987,'Maze Craze: Dozens of mazes, string paths, super challenges, and more',36,'2016-08-23',64,'Maze Craze is specially formulated for Highlights® readers who love finding their way through our mazes. From the editors of Highlights'' ever-popular Puzzlemania® and Puzzle Buzz® series, this curated collection of mazes is sure to keep your budding puzzlers busy scribbling answers and finding their way to finish lines. The puzzles are on one side of the page, with bonus content on the other side, making the Puzzle Pad an engaging back-to-back experience.',3.00,6.92,0.00,6,NULL,FALSE)
, (9781629794235,'Word Searches: Hundreds of hidden words to find',36,'2015-09-01',64,'From the editors of Highlights'' ever-popular Puzzlemania and Puzzle Buzz series, this curated collection of word-seeking fun will keep word-loving kids entertained for hours. Formatted in an on-the-go, fun-to-share activity pad format, it contains more than 30 fun and colorful word searches, plus lots of bonus jokes,cartoons, quizzes (and more) that make it an experience that is uniquely Highlights. The four-color puzzles are on one side of the page, with the bonus content on the other side, making it an engaging back-to-back experience.',3.00,6.92,0.00,6,NULL,FALSE)
, (9781629794242,'Picture Puzzles: Eye-popping matching, mazes, scrambles, and more',36,'2015-09-01',64,'From the editors of Highlights'' ever-popular Puzzlemania and Puzzle Buzz series, this curated collection of picture-perfect fun will keep kids of all interests and abilities entertained for hours. Formatted in an on-the-go, fun-to-share activity pad format, it contains more than 30 matching puzzles, picture scrambles, and more, plus lots of bonus jokes,cartoons, quizzes (and more) that make it an experience that is uniquely Highlights. The four-color puzzles are on one side of the page, with the bonus content on the other side, making it an engaging back-to-back experience.',3.00,6.92,0.00,6,NULL,FALSE)
, (9781629792033,'Travel Puzzles',36,'2015-03-03',144,'This travel–themed collection is packed with a wide range of kids'' favorite puzzles, including mazes, number puzzles, wordplay, brainteasers, matching, and our ever–popular Hidden Pictures puzzles. With bright illustrations, varied levels of complexity, and plenty of humor, this book is sure to challenge, entertain, and amuse kids of every interest and ability.',6.00,12.06,0.00,6,NULL,FALSE)
, (9781629798301,'Christmas Puzzles',36,'2018-10-02',144,'This Christmas-themed collection is packed with a wide range of kids'' favorite puzzles, including mazes, number puzzles, wordplay, brainteasers, matching, and Highlights'' ever-popular Hidden Pictures® puzzles. With puzzles themed around Christmas presents, snowflakes, reindeer, elves, and more, this book will be the perfect Christmas gift to keep kids busy during the holidays.',6.00,12.95,0.00,6,NULL,FALSE)
, (9781629792668,'Winter Puzzles',36,'2014-10-01',144,'This winter-themed collection is packed with a wide range of kids'' favorite puzzles, including mazes, number puzzles, wordplay, brainteasers, matching, and our ever-popular Hidden Pictures® puzzles. With bright illustrations, varied levels of complexity, and plenty of humor, this book is sure to challenge, entertain, and amuse kids of every interest and ability.',5.00,10.77,0.00,6,NULL,FALSE)
, (9781629797687,'Creepy Crawlies',36,'2017-02-28',48,'Nobody does puzzles like Highlights™! With bright illustrations and photos and puzzles at various levels of complexity, the activities in Puzzlemania® Creepy Crawlies will keep kids busy and entertained for hours. This critter-themed collection is packed with a wide range of kids'' favorite puzzles, including mazes, number puzzles, wordplay, brainteasers, matching, and our ever-popular Hidden Pictures® puzzles. With frogs, snakes, beetles, bats, and more, this book will have kids puzzling and learning fun facts about all the critters that make them say, "Ew!"',3.00,6.44,0.00,6,NULL,FALSE)
, (9781620917640,'Adventure Puzzles',36,'2013-10-01',96,'Stickers bring an exciting new element to everyone''s favorite puzzle—Hidden Pictures®! This book is filled with full-color and black-and-white Hidden Pictures scenes about all kinds of outdoor activities and adventures, along with 8 pages of vibrant stickers to mark the hidden objects. For Hidden Pictures fans of all ages, here is an unbeatable combination that makes a terrific gift!',6.00,12.82,0.00,6,NULL,FALSE)
, (9781426330179,'Brain Games: Big Book of Boredom Busters',19,'2018-05-22',160,'Calling all fans of the Brain Games TV show! Excercise your mental muscle with awesome challenges, wacky logic puzzles, optical illusions, and brain-busting riddles. Write-in pages include both games and short explanations of the neuroscience at work. Have fun and challenge yourself as you unleash your inner creativity and become the genius we all know you are.',8.00,16.95,0.00,8,NULL,FALSE)
, (9781426332852,'Brain Games: Mighty Book of Mind Benders',19,'2019-05-21',160,'Train your brain with all kinds of amazing new challenges that will unleash your creativity and bring out the genius within. You''ll find crosswords, word searches, cryptograms, tough logic puzzles, memory tests, wacky riddles, and exercises to try with a friend. Time trials test your skills in each chapter. Write-in pages include puzzles and games as well as short explanations of the brain science at work. Tuning and proving your mental mettle has never been so much fun.',9.00,17.05,0.00,8,NULL,FALSE)
, (9781096123163,'The Little Big Book of Brain Games for Smart Kids: Creative Mind Games, Riddles, Jokes, and Brain teasers',1,'2019-04-27',131,'During the time of the old West, a cowboy rides into town on Sunday. He stays for three days, and leaves on Sunday. How can this be?',8.00,15.94,0.00,5,15,FALSE)
, (9781095698655,'Riddles for Kids: Creative Riddles, Puns, and Knock Knock Jokes for Smart Kids',1,'2019-04-23',79,'When you have me you want to share me, but if you share me then you no longer have me. What am I?',3.00,6.99,0.00,5,15,FALSE)
, (9781095712245,'Ultimate Brain Games and Teasers: Creative Mind Games for Smart Kids of All Ages',1,'2019-04-24',60,'It has cities, but they are void of houses. It also has mountains but is devoid of trees. It has water, but it lacks fish. What exactly is it?',4.00,8.95,8.00,5,15,FALSE)
, (9781097142804,'RIDDLES & BRAIN TEASERS: BEST RIDDLES FOR CHALLENGING SMART KIDS',1,'2019-05-06',189,'This Book is oriented to kids but can be read by everyone, It contains hundred of riddles, brain teasers, and Riddle-jokes oriented to develop lateral thinking, deduction, and practical skills while having fun.',7.00,13.03,8.00,8,NULL,FALSE)
, (9781641525312,'Perfectly Logical!: Challenging Fun Brain Teasers and Logic Puzzles for Smart Kids',37,'2019-08-06',126,'Find the missing pattern pieces. Break codes and secret messages. Discover visual connections. With ten chapters of puzzles, each with its own set of unique challenges, this book has all the logic and brain teasing fun a child could want!',8.00,16.82,9.00,8,12,FALSE)
, (9780606144841,'American Born Chinese',38,'2006-09-06',240,'Perfectly Logical helps curious kids ages 8-12 develop logical reasoning and critical thinking skills while having a blast (that''s the most important part). With puzzles that progressively increase in difficulty, this book engages and challenges kids for hours on end.',5.00,10.99,6.00,12,18,FALSE)
, (9780545132060,'Smile',39,'2010-02-01',224,'Raina Telgemeier''s #1 New York Times bestselling, Eisner Award-winning graphic memoir based on her childhood!Raina just wants to be a normal sixth grader. But one night after Girl Scouts she trips and falls, severely injuring her two front teeth. What follows is a long and frustrating journey with on-again, off-again braces, surgery, embarrassing headgear, and even a retainer with fake teeth attached. And on top of all that, there''s still more to deal with: a major earthquake, boy confusion, and friends who turn out to be not so friendly.',5.00,9.89,6.00,8,12,FALSE)
, (9780810984226,'How Mirka Got Her Sword (Hereville Book 1)',40,'2010-11-01',144,'Spunky, strong-willed eleven-year-old Mirka Herschberg isn''t interested in knitting lessons from her stepmother, or how-to-find-a-husband advice from her sister, or you-better-not warnings from her brother. There''s only one thing she does want: to fight dragons!Granted, no dragons have been breathing fire around Hereville, the Orthodox Jewish community where Mirka lives, but that doesn''t stop the plucky girl from honing her skills. She fearlessly stands up to local bullies. She battles a very large, very menacing pig. And she boldly accepts a challenge from a mysterious witch, a challenge that could bring Mirka her heart''s desire: a dragon-slaying sword! All she has to do is find—and outwit—the giant troll who''s got it!A delightful mix of fantasy, adventure, cultural traditions, and preteen commotion, Hereville will captivate middle-school readers with its exciting visuals and entertaining new heroine.',5.00,9.56,6.00,8,12,FALSE)
, (9781603090858,'The Dragon Puncher Series',41,'2011-11-02',42,'We know the Dragon Puncher is a heroic warrior kitty in a robot battle-suit. But who is this new fierce fighting feline? And what outrageous adventures await on . . . DRAGON PUNCHER ISLAND?! Twice the punching and twice the laughs, that''s for sure!',2.00,4.63,0.00,6,8,FALSE)
, (9780375832291,'Babymouse #1: Queen of the World!',42,'2011-11-30',96,'With multiple Children''s Choice Awards and over 1.8 million books sold, kids, parents, and teachers agree that Babymouse is perfect for fans of Junie B. Jones, Ivy and Bean, Bad Kitty, and Dork Diaries!Meet Babymouse—Her dreams are big! Her imagination is wild! Her whiskers are ALWAYS a mess! In her mind, she''s Queen of the World! In real life... she''s not even Queen of the lunch table.NEW YORK TIMES bestselling, three-time Newbery Honor winning author Jennifer Holm teams up with Matthew Holm to bring you a fully illustrated graphic novel series packed with humor and kid appeal—BABYMOUSE! It''s the same thing every day for Babymouse. Where is the glamour? The excitement? The fame?!? Nothing ever changes, until... Babymouse hears about Felicia Furrypaws''s exclusive slumber party. Will Babymouse get invited? Will her best friend, Wilson, forgive her if she misses their monster movie marathon? Find out in Babymouse #1: Queen of the World! All new BABYMOUSE BONUS: How to Draw Babymouse & Create-Your-Own-Comic!',4.00,7.99,0.00,7,10,FALSE)
, (9781608862801,'Adventure Time Vol. 1',43,'2011-12-31',121,'The totally algebraic adventures of Finn and Jake have come to the comic book page! The Lich, a super-lame, SUPER-SCARY skeleton dude, has returned to the the Land of Ooo, and he''s bent on total destruction! Luckily, Finn and Jake are on the case...but can they succeed against their most destructive foe yet? Featuring fan-favorite characters Marceline the Vampire Queen, Princess Bubblegum, Lumpy Space Princess and the Ice King!',4.00,8.99,0.00,9,12,FALSE)
, (9780606267380,'Drama',39,'2012-09-01',240,'Callie loves theater. And while she would totally try out for her middle school''s production of Moon over Mississippi, she can''t really sing. Instead she''s the set designer for the drama department''s stage crew, and this year she''s determined to create a set worthy of Broadway on a middle-school budget. But how can she, when she doesn''t know much about carpentry, ticket sales are down, and the crew members are having trouble working together? Not to mention the onstage AND offstage drama that occurs once the actors are chosen. And when two cute brothers enter the picture, things get even crazier!',4.00,7.99,0.00,10,14,TRUE)
, (9781596437135,'Anya''s Ghost',38,'2013-05-21',224,'Anya could really use a friend. But her new BFF isn''t kidding about the "Forever" part . . .Of all the things Anya expected to find at the bottom of an old well, a new friend was not one of them. Especially not a new friend who''s been dead for a century.Falling down a well is bad enough, but Anya''s normal life might actually be worse. She''s embarrassed by her family, self-conscious about her body, and she''s pretty much given up on fitting in at school. A new friend—even a ghost—is just what she needs.Or so she thinks.Spooky, sardonic, and secretly sincere, Anya''s Ghost is a wonderfully entertaining debut from author/artist Vera Brosgol.',5.00,10.99,0.00,12,17,FALSE)
, (9781449472337,'The Essential Calvin and Hobbes: A Calvin and Hobbes Treasury',44,'2013-11-12',256,'Bill Watterson''s Calvin and Hobbes has been a worldwide favorite since its introduction in 1985. The strip follows the richly imaginative adventures of Calvin and his trusty tiger, Hobbes. Whether a poignant look at serious family issues or a round of time-travel (with the aid of a well-labeled cardboard box), Calvin and Hobbes will astound and delight you.Beginning with the day Hobbes sprang into Calvin''s tuna fish trap, the first two Calvin and Hobbes collections, Calvin and Hobbes and Something Under The Bed Is Drooling, are brought together in this treasury. Including black-and-white dailies and color Sundays, The Essential Calvin and Hobbes also features an original full-color 16-page story.',5.00,9.99,0.00,12,12,FALSE)
, (9780439846806,'The Stonekeeper (Amulet #1)',39,'2013-12-17',192,'After the tragic death of their father, Emily and Navin move with their mother to the home of her deceased great-grandfather, but the strange house proves to be dangerous. Before long, a sinister creature lures the kids'' mom through a door in the basement. Em and Navin, desperate not to lose her, follow her into an underground world inhabited by demons, robots, and talking animals.',5.00,9.99,0.00,8,12,FALSE)
, (9780439706407,'Bone #1: Out from Boneville',45,'2014-05-01',145,'The three Bone cousins -- Fone Bone, Phoney Bone, and Smiley Bone -- are separated and lost in a vast, uncharted desert. One by one, they find their way into a deep, forested valley filled with wonderful and terrifying creatures. Eventually, the cousins are reunited at a farmstead run by tough Gran''ma Ben and her spirited granddaughter. But little do the Bones know, there are dark forces conspiring against them, and their adventures are only just beginning!',5.00,10.99,0.00,11,16,FALSE)
, (9781419712173,'El Deafo',46,'2014-09-02',248,'Going to school and making new friends can be tough. But going to school and making new friends while wearing a bulky hearing aid strapped to your chest? That requires superpowers! In this funny, poignant graphic novel memoir, author/illustrator Cece Bell chronicles her hearing loss at a young age and her subsequent experiences with the Phonic Ear, a very powerful—and very awkward—hearing aid. The Phonic Ear gives Cece the ability to hear—sometimes things she shouldn''t—but also isolates her from her classmates. She really just wants to fit in and find a true friend, someone who appreciates her as she is. After some trouble, she is finally able to harness the power of the Phonic Ear and become "El Deafo, Listener for All." And more importantly, declare a place for herself in the world and find the friend she''s longed for.',6.00,12.88,0.00,8,12,FALSE)
, (9780385265201,'The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great',47,'2014-10-28',368,'An entertaining and informative illustrated guide that makes world history accessible, appealing, and funny.',10.00,19.99,0.00,10,15,FALSE)
, (9781608869008,'Abigail & The Snowman (Volume 1)',43,'2014-12-31',28,'Abigail is a nine-year-old girl with a huge imagination who moves to a small town where she''s the new kid at school, struggling to make friends. All that changes when she meets a Yeti named Claude who has escaped a top-secret government facility. Abigail and Claude become the best of friends, but to make sure he can truly be free from the "Shadow Men" chasing him, they must go on an adventure to find Claude''s real home!',1.00,2.29,0.00,6,8,FALSE)
, (9781449483500,'Unicorn vs. Goblins (Phoebe and Her Unicorn Series Book 3): Another Phoebe and Her Unicorn Adventure',44,'2016-02-23',176,'School''s out! That means no more teachers, no more books, and lots of time to compliment Marigold Heavenly Nostrils on her good looks. In this third volume, Phoebe and her obligational best friend, Marigold, learn that summer still has plenty of surprises for the both of them. All of our old friends are back—Phoebe''s part-time "frenemy" Dakota, upon whom Marigold has bestowed sentient hair, Phoebe''s goofy parents, and even Lord Splendid Humility (but please, ignore his magnificence if you can)! Have fun as Phoebe and Marigold continue the "Phoebegold Detective Agency," spend a week at Wolfgang Music Camp, and find themselves in more misadventures, thanks to Marigold''s enchanted sparkles! Along the way, Phoebe makes some new friends, such as Sue—her unique clarinet-playing bunkmate, Florence Unfortunate Nostrils, Marigold''s estranged sister, and Camp Wolfgang''s lake monster who enjoys tacos and Wi-Fi. When school resumes, read along as Phoebe enjoys (or suffers from) a brief case of popularity, mentally catalogs her grievances against dodge ball, and, with Marigold''s help, rescues Dakota and her hair from the queen of the goblins. Through these wacky adventures, Phoebe and Marigold learn that their friendship is the second most magical thing of all, after Marigold''s beauty, of course.',5.00,9.59,0.00,8,12,FALSE)
, (9781608868018,'Garfield Vol. 8',43,'2016-05-25',114,'Garfield''s comic book adventures, Garfield and Odie go on strange trips with a mailman and a genie, while also finding a magic wand. Also, Pooky is abducted by aliens, Nermal makes new friends in the forest, and a Garfield cosplayer solves a mystery at a comic convention. Collects issues #29-32.',4.00,7.99,0.00,7,10,FALSE)
, (9781626721067,'Bera the One-Headed Troll',38,'2016-08-02',128,'Bera doesn''t ask for much in life. She''s a solitary, humble troll, tending her island pumpkin patch in cheerful isolation. She isn''t looking for any trouble.But when trouble comes to find her, it comes in spades. A human baby has arrived in the realm of the trolls, and nobody knows where it came from, but Bera seems to be the only person who doesn''t want it dead. There''s nothing to it but to return the adorable little thing to its parents.Like it or not, Bera''s gone and found herself a quest.From noted picture book illustrator and graphic novelist (Maddy Kettle) Eric Orchard comes Bera the One-Headed Troll, a delightful new fantasy adventure with all the sweetness, spookiness, and satisfaction of your favorite childhood bedtime story.',5.00,10.99,0.00,10,14,FALSE)
, (9781608869930,'The Backstagers Vol. 1',48,'2017-07-19',109,'James Tynion IV (Detective Comics, The Woods) teams up with artist Rian Sygh (Munchkin, Stolen Forest) for an incredibly earnest story that explores what it means to find a place to fit in when you''re kinda an outcast. When Jory transfers to an all-boys private high school, he''s taken in by the lowly stage crew known as the Backstagers. Hunter, Aziz, Sasha, and Beckett become his new best friends and introduce him to an entire magical world that lives beyond the curtain that the rest of the school doesn''t know about, filled with strange creatures, changing hallways, and a decades-old legend of a backstage crew that went missing and was never found. Collects the first four issues. "With heart and chutzpah to spare, [The Backstagers] soars as a sincere love letter to the unsung heroes of the theater world." - Newsarama',4.00,7.19,0.00,11,14,FALSE)
, (9781401273941,'Batman: A Lot of Li''l Gotham (Batman: Li''l Gotham)',49,'2018-09-04',271,'Featuring versions of Batman, Robin, the Joker and the other heroes and villains of Gotham City as children, this Eisner Award-nominated series follows some of DC''s most famous (and infamous) heroes and villains through the holidays. Gorgeously rendered by Dustin Nguyen, this all-ages series is perfect for new readers and veteran fans alike! Now in hardcover, you can collect the entire BATMAN: LI''L GOTHAM series right here in BATMAN: A LOT OF LI''L GOTHAM!',8.00,15.94,0.00,11,14,FALSE)
, (9781524852269,'Charlie Brown: All Tied Up (PEANUTS AMP Series Book 13): A PEANUTS Collection',44,'2019-10-15',176,'While Charlie Brown is all tied up, the rest of the gang doesn''t hold back on having fun. Pig-Pen unexpectedly charms Peppermint Patty at the Valentine''s dance, Marcie and Snoopy run a commercial airline, and Lucy tries her hardest to win Schroeder''s affection. Whether you''re safe on the ground or tangled up in a tree like Charlie Brown, you won''t want to miss the fun in this latest Peanuts for kids adventure.',5.00,9.99,0.00,7,12,FALSE)
, (9781423160342,'Disney Bedtime Favorites',50,'2012-07-17',304,'Perfect for bedtime, the second edition of the popular Bedtime Favorites storybook collection has 19 stories to choose from. Updated story selections will feature characters from Finding Nemo, Cars 2, Toy Story 3, The Lion King, and more. Gilded pages and over 250 illustrations make this an ideal gift.',7.00,14.99,0.00,4,8,FALSE)
, (9781368043632,'Frozen 2: Forest of Shadows',50,'2019-10-04',416,'Anna of Arendelle wants nothing more than to be helpful to her older sister, Elsa. But as far as Anna can see, ever since Elsa''s coronation, her sister has been doing just fine without her. And now, Elsa will be setting sail for a grand tour of the world--leaving Anna behind. But a mysterious sickness strikes Arendelle, and Elsa''s tour is delayed, giving Anna the perfect opportunity to finally help. When Anna discovers a secret room in the castle and incants a magic spell, she hopes it will make her dream of curing the sickness come true. Instead, a more sinister dream comes to life. This thrilling original middle grade novel bridges the epic adventures of Frozen and Frozen 2.',10.00,19.99,0.00,6,12,FALSE)
, (9781948040686,'Help Your Dragon Deal With Anxiety: Train Your Dragon To Overcome Anxiety',51,'2018-12-17',44,'Having a pet dragon is very fun!
He can sit, roll over, and play...
He can candle a birthday cake, lit a campfire, or so many other cool things...
But what if your dragon is constantly worrying about so many things?
What if he''s worried about his math test even though he has studied very hard?
What if he''s so nervous about his upcoming book report in front of the class?
What if he gets so anxious when he has to go get a shot from the doctor office? So anxious that he has a meltdown?
What if your dragon is always asking about "What If"?
What should you do?
You teach him how to deal with his anxiety!
How?
Get this book and learn how!
Fun, cute, and entertaining with beautiful illustrations, this is a must have book for children, parents and teachers to teach kids the proper way to think, and deal with worry and anxiety!',10.00,19.99,0.00,4,8,TRUE)
, (9782764336953,'Disney Classics My Busy Books',52,'2018-09-01',10,'An engaging storybook and toy in one activity kit! My Busy Books offer full-page illustrations, a story, 12 figurines, and a playmat that bring the characters to life and ignite your child''s imagination. 3 years and up.
2018 Disney Enterprises, Inc.
Features:
Sturdy board book
Colorful images
Lively story
plastic figurines
Durable playmat
Figurines and playmat stored in the book
Portable
10 pages of fun!
Since 1979, Phidal Publishing has introduced children to the joys of reading by creating colorful, safe, and fun products. Our goal is to both educate and entertain children by engaging their imaginations. Our books feature familiar faces and original characters in innovative formats developed to meet the needs of parents and children. We at Phidal are honored that families and educators around the world continue to rely on our collection of carefully crafted books, which spark young imaginations and introduce children to the joys of reading and learning.
2018 Produced and Published by Phidal Publishing, Inc.',9.00,17.99,0.00,3,6,FALSE)
, (9782764333655,'Disney Beauty & The Beast My Busy Book',52,'2017-02-01',10,'An engaging storybook and toy in one activity kit! My Busy Books offer full-page illustrations, a story, figurines, and a playmat that bring the characters to life and ignite your child''s imagination. 3 years and up.
2017 Disney
Features:
Sturdy board book
Colorful images
Lively story
Plastic figurines
Durable playmat
Figurines and playmat stored in the book
Portable
10 pages of fun!
Since 1979, Phidal Publishing has introduced children to the joys of reading by creating colorful, safe, and fun products. Our goal is to both educate and entertain children by engaging their imaginations. Our books feature familiar faces and original characters in innovative formats developed to meet the needs of parents and children. We at Phidal are honored that families and educators around the world continue to rely on our collection of carefully crafted books, which spark young imaginations and introduce children to the joys of reading and learning.',9.00,17.99,0.00,3,6,FALSE)
, (9782764323519,'Disney Frozen My Busy Book',52,'2013-10-01',10,'An engaging storybook and toy in one activity kit! My Busy Books offer full-page illustrations, a story, figurines, and a playmat that bring the characters to life and ignite your child''s imagination. 3 years and up.
© Disney
Features:
Sturdy board book
Colorful images
Lively story
Plastic figurines
Durable playmat
Figurines and playmat stored in the book
Portable
10 pages of fun!
Since 1979, Phidal Publishing has introduced children to the joys of reading by creating colorful, safe, and fun products. Our goal is to both educate and entertain children by engaging their imaginations. Our books feature familiar faces and original characters in innovative formats developed to meet the needs of parents and children. We at Phidal are honored that families and educators around the world continue to rely on our collection of carefully crafted books, which spark young imaginations and introduce children to the joys of reading and learning.',9.00,17.99,0.00,3,6,FALSE)
, (9781368023481,'A Place for Mulan',50,'2020-02-11',40,'A stylized picture book that will tell an all-new, original story based on the world and characters of the upcoming Walt Disney Studios'' live action Mulan film.',8.00,16.99,0.00,6,8,FALSE)
, (9781368006187,'Vampirina Vee''s Fangtastic World',50,'2018-02-16',12,'Peek under all the flaps of this giant board book and have a ghoulishly good time discovering Vampirina''s unique and fang-tastic world!',5.00,9.99,0.00,3,5,FALSE)
, (9781484782002,'Marvel Avengers: Happy Holidays!',53,'2016-09-06',24,'It was supposed to be the most wonderful time of the year, but after battling monsters and villains, the mighty Avengers just weren''t in the holiday spirit. Not even gifts from the Wasp could make their day merry. But who was that dashing through the snow? Could he save the Avengers and their holiday season? Includes two sheets of gift tag stickers!',3.00,6.99,5.00,8,12,FALSE)
, (9781368026697,'This is Captain Marvel',53,'2019-02-05',32,'The Marvel World of Reading line of early readers is designed to offer reluctant readers books that they will want to read by featuring characters they love. The series is broken into three levels that invoke the rigorous training courses their favorite Marvel heroes must engage in to perfect their super powers. Discover how an ordinary pilot named Carol Danvers became the powerful Captain Marvel!',3.00,6.99,5.00,3,5,FALSE)
, (9781368007610,'Sam Saves the Night',50,'2019-10-01',304,'What would you do if you could stay out all night and not get in trouble ? Thirteen-year-old Sam has no friends, but you can''t really blame her. She lives her life in a state of chronic exhaustion thanks to her nightly sleepwalking jaunts, which include trips to the store, treehouse-building projects, and breaking-and-entering escapades—none of which she remembers in the morning. Her condition is taking its toll on her family (and her life), so when her mom takes her to see a wacky strip-mall sleep specialist, Sam is wary, but 100 percent in. The night after the doc works his mojo, Sam wakes up outside her body, watching herself sleep. FREAKY! But once she gets over the panic attack, she realizes there''s a whole world of detached-souls out there, called SleepWakers—cliques of kids like the Achieves, who use their sleep time to learn new things; the Numbs, who eat junk food and play video games all night long, and the OCDeeds who search for missing things and organize other people''s stuff. And then there are the Mean Dreams, led by Madalynn Sucret, the nicest girl in Sam''s school, who shows Sam that she can use her power to get back at a bully who''s been tormenting her. Sam is intrigued—until it becomes clear that Madalynn is the real bully and the "tormentor" is just, well... sad. Now Sam is faced with uniting the various tribes of SleepWakers to fight back against Madalynn and the Mean Dreams in the most epic battle the night has ever seen.',7.00,13.99,12.00,9,12,FALSE)
, (9781368051828,'The Galaxy Needs You',50,'2019-12-17',48,'Have you ever stopped to think about how there is nobody else in the galaxy who is exactly like you? This empowering picture book celebrates young heroes-in-the-making and features illustrations that follow Rey on her own hero''s journey.',9.00,17.99,16.00,6,8,FALSE)
, (9781368053280,'R2-D2 is LOST!',50,'2020-02-11',64,'The droids are in for an Endor-able adventure! While C-3PO is pleased to once again be treated like a god by the Ewoks, and BB-8 is curious about his new furry companions, R2-D2 finds himself in a wilderness experience gone wrong when he comes across a small Ewok in need of help. With charming illustrations by Brian Kesinger, this next installment of the fun new Droid Tales picture book series is a delightful addition to any youngling''s growing library.',5.00,10.99,8.00,6,8,FALSE)
, (9781408855652,'Harry Potter And The Philosopher''s Stone',54,'2014-09-01',352,'Harry Potter has never even heard of Hogwarts when the letters start dropping on the doormat at number four, Privet Drive. Addressed in green ink on yellowish parchment with a purple seal, they are swiftly confiscated by his grisly aunt and uncle. Then, on Harry''s eleventh birthday, a great beetle-eyed giant of a man called Rubeus Hagrid bursts in with some astonishing news: Harry Potter is a wizard, and he has a place at Hogwarts School of Witchcraft and Wizardry. An incredible adventure is about to begin!
These new editions of the classic and internationally bestselling, multi-award-winning series feature instantly pick-up-able new jackets by Jonny Duddle, with huge child appeal, to bring Harry Potter to the next generation of readers. It''s time to PASS THE MAGIC ON.',7.00,14.99,0.00,12,NULL,FALSE)
, (9781408855669,'Harry Potter And The Chamber Of Secrets',54,'2014-09-01',384,'Harry Potter''s summer has included the worst birthday ever, doomy warnings from a house-elf called Dobby, and rescue from the Dursleys by his friend Ron Weasley in a magical flying car! Back at Hogwarts School of Witchcraft and Wizardry for his second year, Harry hears strange whispers echo through empty corridors - and then the attacks start. Students are found as though turned to stone . Dobby''s sinister predictions seem to be coming true.
These new editions of the classic and internationally bestselling, multi-award-winning series feature instantly pick-up-able new jackets by Jonny Duddle, with huge child appeal, to bring Harry Potter to the next generation of readers. It''s time to PASS THE MAGIC ON.',6.00,12.50,0.00,12,NULL,TRUE)
, (9781408855676,'Harry Potter And The Prisoner Of Azkaban',54,'2014-09-01',480,'When the Knight Bus crashes through the darkness and screeches to a halt in front of him, it''s the start of another far from ordinary year at Hogwarts for Harry Potter. Sirius Black, escaped mass-murderer and follower of Lord Voldemort, is on the run - and they say he is coming after Harry. In his first ever Divination class, Professor Trelawney sees an omen of death in Harry''s tea leaves . But perhaps most terrifying of all are the Dementors patrolling the school grounds, with their soul-sucking kiss.
These new editions of the classic and internationally bestselling, multi-award-winning series feature instantly pick-up-able new jackets by Jonny Duddle, with huge child appeal, to bring Harry Potter to the next generation of readers. It''s time to PASS THE MAGIC ON.',6.00,12.50,0.00,12,NULL,FALSE)
, (9781408855683,'Harry Potter And The Goblet Of Fire',54,'2014-09-01',640,'The Triwizard Tournament is to be held at Hogwarts. Only wizards who are over seventeen are allowed to enter - but that doesn''t stop Harry dreaming that he will win the competition. Then at Hallowe''en, when the Goblet of Fire makes its selection, Harry is amazed to find his name is one of those that the magical cup picks out. He will face death-defying tasks, dragons and Dark wizards, but with the help of his best friends, Ron and Hermione, he might just make it through - alive!
These new editions of the classic and internationally bestselling, multi-award-winning series feature instantly pick-up-able new jackets by Jonny Duddle, with huge child appeal, to bring Harry Potter to the next generation of readers. It''s time to PASS THE MAGIC ON.',8.00,16.00,0.00,12,NULL,FALSE)
, (9781408855690,'Harry Potter And The Order Of The Phoenix',54,'2014-09-01',816,'Dark times have come to Hogwarts. After the Dementors'' attack on his cousin Dudley, Harry Potter knows that Voldemort will stop at nothing to find him. There are many who deny the Dark Lord''s return, but Harry is not alone: a secret order gathers at Grimmauld Place to fight against the Dark forces. Harry must allow Professor Snape to teach him how to protect himself from Voldemort''s savage assaults on his mind. But they are growing stronger by the day and Harry is running out of time.
These new editions of the classic and internationally bestselling, multi-award-winning series feature instantly pick-up-able new jackets by Jonny Duddle, with huge child appeal, to bring Harry Potter to the next generation of readers. It''s time to PASS THE MAGIC ON.',8.00,16.00,0.00,12,NULL,TRUE)
, (9781408855706,'Harry Potter And The Half-blood Prince',54,'2014-09-01',560,'When Dumbledore arrives at Privet Drive one summer night to collect Harry Potter, his wand hand is blackened and shrivelled, but he does not reveal why. Secrets and suspicion are spreading through the wizarding world, and Hogwarts itself is not safe. Harry is convinced that Malfoy bears the Dark Mark: there is a Death Eater amongst them. Harry will need powerful magic and true friends as he explores Voldemort''s darkest secrets, and Dumbledore prepares him to face his destiny.
These new editions of the classic and internationally bestselling, multi-award-winning series feature instantly pick-up-able new jackets by Jonny Duddle, with huge child appeal, to bring Harry Potter to the next generation of readers. It''s time to PASS THE MAGIC ON .',9.00,18.00,0.00,12,NULL,FALSE), 
(9781408855713,'Harry Potter And The Deathly Hallows',54,'2014-09-01',640,'As he climbs into the sidecar of Hagrid''s motorbike and takes to the skies, leaving Privet Drive for the last time, Harry Potter knows that Lord Voldemort and the Death Eaters are not far behind. The protective charm that has kept Harry safe until now is now broken, but he cannot keep hiding. The Dark Lord is breathing fear into everything Harry loves, and to stop him Harry will have to find and destroy the remaining Horcruxes. The final battle must begin - Harry must stand and face his enemy.
These new editions of the classic and internationally bestselling, multi-award-winning series feature instantly pick-up-able new jackets by Jonny Duddle, with huge child appeal, to bring Harry Potter to the next generation of readers. It''s time to PASS THE MAGIC ON .',9.00,18.00,0.00,12,NULL,FALSE);

-- Bookgenre
INSERT INTO bookgenre(isbn,genre_id) VALUES
 (9780385320436,1)
,(9780448405179,1)
,(9780486468211,1)
,(9780547557991,1)
,(9780692848388,1)
,(9781097522132,1)
,(9781426307041,1)
,(9781426307935,1)
,(9781426308468,1)
,(9781426310140,1)
,(9781426310492,1)
,(9781465414182,1)
,(9781580625579,1)
,(9781623368890,1)
,(9781633226241,1)
,(9781641520416,1)
,(9781641520928,1)
,(9781732596344,1)
,(9781772261059,1)
,(9781775183310,1)
,(9781095698655,2)
,(9781095712245,2)
,(9781096123163,2)
,(9781097142804,2)
,(9781426330179,2)
,(9781426332852,2)
,(9781580626873,2)
,(9781620917640,2)
,(9781629792033,2)
,(9781629792668,2)
,(9781629794228,2)
,(9781629794235,2)
,(9781629794242,2)
,(9781629796987,2)
,(9781629797687,2)
,(9781629797694,2)
,(9781629798301,2)
,(9781641525312,2)
,(9781780552491,2)
,(9781936140404,2)
,(9780375832291,3)
,(9780385265201,3)
,(9780439706407,3)
,(9780439846806,3)
,(9780545132060,3)
,(9780606144841,3)
,(9780606267380,3)
,(9780810984226,3)
,(9781401273941,3)
,(9781419712173,3)
,(9781449472337,3)
,(9781449483500,3)
,(9781524852269,3)
,(9781596437135,3)
,(9781603090858,3)
,(9781608862801,3)
,(9781608868018,3)
,(9781608869008,3)
,(9781608869930,3)
,(9781626721067,3)
,(9781368006187,4)
,(9781368007610,4)
,(9781368023481,4)
,(9781368026697,4)
,(9781368043632,4)
,(9781368051828,4)
,(9781368053280,4)
,(9781408855652,4)
,(9781408855669,4)
,(9781408855676,4)
,(9781408855683,4)
,(9781408855690,4)
,(9781408855706,4)
,(9781408855713,4)
,(9781423160342,4)
,(9781484782002,4)
,(9781948040686,4)
,(9782764323519,4)
,(9782764333655,4)
,(9782764336953,4)
,(9780062498564,5)
,(9780062941008,5)
,(9780062953414,5)
,(9780062953452,5)
,(9780312625993,5)
,(9780399246531,5)
,(9780763693558,5)
,(9780888996596,5)
,(9780889953673,5)
,(9781459802483,5)
,(9781467711944,5)
,(9781481400701,5)
,(9781492662143,5)
,(9781515782223,5)
,(9781524769567,5)
,(9781554988334,5)
,(9781620142639,5)
,(9781743629000,5)
,(9781772600384,5)
,(9781772780109,5)
,(9781894778145,5);

-- Authors Table
  INSERT INTO authors(author_id,firstname,lastname,title) VALUES (1,'Cece','Bell',NULL),
  (2,'Kamilla','Benko',NULL),
  (3,'Howard','Bennett',NULL),
  (4,'Paige','Billin-Frye',NULL),
  (5,'Michelle','Bisson',NULL),
  (6,'Beth L','Blair',NULL),
  (7,'David','Bouchard,',NULL),
  (8,'Vanessa','Brantley-Newton',NULL),
  (9,'Vera','Brosgol',NULL),
  (10,'Nicola I.','Campbell',NULL),
  (11,'Marie','Chow',NULL),
  (12,'Eoin','Colfer',NULL),
  (13,'Rusty','Cove-Smith',NULL),
  (14,'Becky','Cummings',NULL),
  (15,'Jim','Davis',NULL),
  (16,'Suzanne','Del Rizzo',NULL),
  (17,'Barry','Deutsch',NULL),
  (18,'Andrew','Donkin',NULL),
  (19,'Cecece','Bellel','Ms.'),
  (20,'Stephanie Warren','Drimmer',NULL),
  (21,'Byron','Eggenschwiler',NULL),
  (22,'Earl','Einarson',NULL),
  (23,'Jennifer Riesmeyer','Elvgren',NULL),
  (24,'Jennifer A.','Ericsson',NULL),
  (25,'Annabelle','Erikson',NULL),
  (26,'Julie','Flett',NULL),
  (27,'Jessie','Ford',NULL),
  (28,'Derek','Fridolfs',NULL),
  (29,'Angela May','George',NULL),
  (30,'Larry','Gonick',NULL),
  (31,'Steve','Herman',NULL),
  (32,'Andrew','Hill','Ph.D.'),
  (33,'Jennifer L.','Holm',NULL),
  (34,'Matthew','Holm',NULL),
  (35,'Catherine D.','Hughes',NULL),
  (36,'Steve','Jenkins',NULL),
  (37,'Charlotte Foltz','Jones',NULL),
  (38,'Caitlin','Kennedy',NULL),
  (39,'Kazu','Kibuishi',NULL),
  (40,'Thomas','King',NULL),
  (41,'James','Kochalka','Dr'),
  (42,'Roger','Langridge',NULL),
  (43,'Jenn','Larson',NULL),
  (44,'Cynthia','Levinson',NULL),
  (45,'Adir','Levy',NULL),
  (46,'Ganit','Levy',NULL),
  (47,'Asia','Lewis-Ross',NULL),
  (48,'Thomas','Macri',NULL),
  (49,'Dayna','Martin',NULL),
  (50,'Juana','Martinez-Neal',NULL),
  (51,'Lora','McClain-Muhammad',NULL),
  (52,'Tony','Medina',NULL),
  (53,'Kelli','Miller','LCSW, MSW'),
  (54,'Gareth','Moore',NULL),
  (55,'Dustin','Nguyen',NULL),
  (56,'Ryan','North',NULL),
  (57,'Disney Book Group',NULL,NULL),
  (58,'DK',NULL,NULL),
  (59,'Giovanni','Rigano',NULL),
  (60,'Highlights',NULL,NULL),
  (61,'Kerascoet',NULL,NULL),
  (62,'Marvel Press Book Group',NULL,NULL),
  (63,'National Geographic Kids',NULL,NULL),
  (64,'Sugar Snap Studio',NULL,NULL),
  (65,'John','O''Brien',NULL),
  (66,'Eric','Orchard',NULL),
  (67,'Joanne','Robertson',NULL),
  (68,'Stacey','Robinson',NULL),
  (69,'Tom','Robinson',NULL),
  (70,'A.R.','Roumanis',NULL),
  (71,'J.K.','Rowling',NULL),
  (72,'Mat','Sadler',NULL),
  (73,'Charles M.','Schulz',NULL),
  (74,'Amy','Shields',NULL),
  (75,'Donald M.','Silver',NULL),
  (76,'Dana','Simpson',NULL),
  (77,'Shari','Simpson',NULL),
  (78,'Jeff','Smith',NULL),
  (79,'Samantha','Snowden','M.A.'),
  (80,'Jacquelyn','Stagg',NULL),
  (81,'Terry','Stickels',NULL),
  (82,'Raina','Telgemeier',NULL),
  (83,'Franco','Tempesta',NULL),
  (84,'Angie','Thomas',NULL),
  (85,'Lauren','Thompson',NULL),
  (86,'James IV','Tynion',NULL),
  (87,'Richard','Van Camp',NULL),
  (88,'Jonathan','Van Ness',NULL),
  (89,'Tom','Watson',NULL),
  (90,'Bill','Watterson',NULL),
  (91,'Lynn','Wilson',NULL),
  (92,'Jacqueline','Woodson',NULL),
  (93,'Patricia J.','Wynne',NULL),
  (94,'Gene Luen','Yang',NULL);

-- Author/Book Bridging Table
INSERT INTO bookauthor(isbn,author_id) VALUES (9780062941008,88),
(9780889953673,7),
(9781894778145,22),
(9781894778145,26),
(9780888996596,10),
(9781772600384,67),
(9781459802483,87),
(9780062953452,89),
(9780062953414,89),
(9781554988334,40),
(9781554988334,21),
(9781620142639,52),
(9781620142639,68),
(9780062498564,84),
(9781524769567,61),
(9781481400701,44),
(9781481400701,8),
(9781743629000,29),
(9781772780109,16),
(9780312625993,85),
(9781492662143,59),
(9781492662143,18),
(9781492662143,12),
(9781467711944,23),
(9781515782223,5),
(9780399246531,92),
(9780763693558,50),
(9781426310492,63),
(9781580625579,69),
(9780486468211,93),
(9780486468211,75),
(9781641520416,53),
(9780692848388,45),
(9780692848388,46),
(9780692848388,72),
(9780547557991,36),
(9781426307041,35),
(9781426310140,35),
(9781426307935,74),
(9781426308468,35),
(9781426308468,83),
(9781732596344,14),
(9780448405179,91),
(9780448405179,4),
(9781641520928,79),
(9781641520928,32),
(9781772261059,49),
(9781772261059,70),
(9781097522132,47),
(9781097522132,51),
(9781633226241,27),
(9781633226241,64),
(9780385320436,65),
(9780385320436,37),
(9781465414182,58),
(9781623368890,3),
(9781775183310,30),
(9781936140404,81),
(9781780552491,54),
(9781580626873,24),
(9781580626873,6),
(9781629794228,60),
(9781629797694,60),
(9781629796987,60),
(9781629794235,60),
(9781629794242,60),
(9781629792033,60),
(9781629798301,60),
(9781629792668,60),
(9781629797687,60),
(9781620917640,60),
(9781426330179,20),
(9781426332852,20),
(9781096123163,25),
(9781095698655,25),
(9781095712245,25),
(9781097142804,13),
(9781641525312,43),
(9780606144841,94),
(9780545132060,82),
(9780810984226,17),
(9781603090858,41),
(9780375832291,33),
(9780375832291,34),
(9781608862801,56),
(9780606267380,82),
(9781596437135,9),
(9781449472337,90),
(9780439846806,39),
(9780439706407,78),
(9781419712173,1),
(9780385265201,30),
(9781608869008,42),
(9781449483500,76),
(9781608868018,15),
(9781626721067,66),
(9781608869930,86),
(9781401273941,28),
(9781401273941,55),
(9781524852269,73),
(9781423160342,57),
(9781368043632,2),
(9781948040686,31),
(9782764336953,57),
(9782764333655,57),
(9782764323519,57),
(9781368023481,11),
(9781368006187,57),
(9781484782002,48),
(9781368026697,62),
(9781368007610,77),
(9781368051828,38),
(9781368053280,38),
(9781408855652,71),
(9781408855669,71),
(9781408855676,71),
(9781408855683,71),
(9781408855690,71),
(9781408855706,71),
(9781408855713,71);

-- Users Table
INSERT INTO users (`user_id`, `email`, `title`, `lastname`, `firstname`, `companyname`, `address1`, `address2`, `city`, `province`, `country`, `postalcode`, `homephone`, `cellphone`) VALUES 
(1, 'cst.send@gmail.com', 'Mx.', 'Consumer', 'Consumer', 'Dawson', '6812 Serena Fords', NULL, 'Blaiseland', 'NB', 'CANADA', 'E5A 6N8', '935.469.0056x691', '9255585725'),
(2, 'cst.receive@gmail.com', 'Mx.', 'Manager', 'Manager', 'Dawson', '908 Kautzer Lodge Suite 765', NULL, 'Port Jadaport', 'MB', 'CANADA', 'R5A 9A2', '949-302-7236x3218', '868.030.5571'),
(3, 'christopher78@example.net', 'Mx.', 'Veum', 'Maybell', 'Powlowski Ltd', '7873 Runte Street', NULL, 'McLaughlinchester', 'ON', 'CANADA', 'N8A 4T3', '1-772-968-0112', '(644)897-3429'),
(4, 'upton.veda@example.org', 'Mx.', 'Turner', 'Carey', NULL, '4458 Drew Ville Suite 555', NULL, 'Armandmouth', 'NS', 'CANADA', 'B2S 9T4', '479-249-3018', '1-971-771-5817'),
(5, 'saul.gleason@example.com', 'Mx.', 'Wolff', 'Krystina', 'Mayert LLC', '2180 Russel Crossing', NULL, 'South Alexandrine', 'NB', 'CANADA', 'E4K 5P6', '544-438-7359', '711-118-3268'),
(6, 'evalyn61@example.net', 'Mrs.', 'Flatley', 'Vesta', 'Oberbrunner, Wilkinson and Hane', '795 Julius Lodge Suite 854', NULL, 'South Jayden', 'NS', 'CANADA', 'B1K 9M5', '657.577.8930x44855', '735-539-1079'),
(7, 'alverta30@example.org', 'Mx.', 'Waters', 'Louvenia', 'Quigley, Fahey and Mueller', '51096 Edwin Crest Apt. 144', NULL, 'South Tyshawntown', 'QC', 'CANADA', 'G4X 7J6', '(836)657-4617x4788', '(115)541-9434'),
(8, 'marcella88@example.org', 'Mr.', 'Haag', 'Giovanni', 'Jaskolski Inc', '963 Jacquelyn Garden', NULL, 'Parisianborough', 'QC', 'CANADA', 'G6L 7V9', '676-477-2053x82402', '07563939668'),
(9, 'erik88@example.org', 'Mx.', 'Schimmel', 'Daphne', 'Botsford-Hartmann', '785 Waelchi Unions', NULL, 'Chandlerville', 'BC', 'CANADA', 'V9X 4L8', '630-651-8487x83563', '02662638593'),
(10,'kuphal.jarret@example.net', 'Mrs.', 'Schuppe', 'Shany', 'Stehr Inc', '507 VonRueden Island Suite 537', NULL, 'Adrielview', 'QC', 'CANADA', 'H9W 4V6', '726-006-7526', '1-913-348-2830'),
(11,'yrowe@example.net', 'Mr.', 'Gibson', 'Brannon', 'Champlin, Hahn and Mueller', '0140 Turner Walk Suite 655', NULL, 'Amparoshire', 'QC', 'CANADA', 'J5W 3K1', '1-186-482-3299', '(363)987-9933'),
(12,'pschimmel@example.org', 'Mr.', 'Bergnaum', 'Ara', 'Padberg Ltd', '7125 Orn Camp Suite 527', NULL, 'Bednarbury', 'NB', 'CANADA', 'E1X 8X1', '1-217-514-1582', '02390367766'),
(13,'ova.fisher@example.com', 'Mx.', 'Stoltenberg', 'Scottie', 'Lindgren, Weissnat and White', '964 Gennaro Curve', NULL, 'North Walterland', 'BC', 'CANADA', 'V3Y 4S3', '246.167.8428', '(184)397-7747'),
(14,'tfadel@example.org', 'Mx.', 'Hermiston', 'Kameron', 'Emmerich-Koch', '8226 Lang Parks Suite 032', NULL, 'North Celestine', 'BC', 'CANADA', 'V1C 5S4', '1-556-614-1155', '109.153.6377'),
(15,'rogahn.berta@example.com', 'Mx.', 'Reynolds', 'Marina', 'Douglas, Lockman and Herzog', '895 Alexandro Burgs', NULL, 'South Jazlynton', 'NB', 'CANADA', 'E6H 3J9', '728.621.8068', '025-878-0696'),
(16,'considine.macie@example.org', 'Mx.', 'Turner', 'Norma', 'Anderson, Mante and Nitzsche', '63008 Kenna Plain Suite 108', NULL, 'Lake Wilmerton', 'QC', 'CANADA', 'G7B 7G3', '108.947.5853x54580', '479.594.3759'),
(17,'seth77@example.net', 'Mx.', 'Reichert', 'Santa', 'Jacobson-Cassin', '6811 Luis Ford Apt. 389', NULL, 'Treutelfurt', 'MB', 'CANADA', 'R1N 1H9', '(565)753-8787', '127-055-2665'),
(18,'charlotte.mante@example.net', 'Mx.', 'Leuschke', 'Wanda', NULL, '55099 Berge Springs', NULL, 'North Jomouth', 'MB', 'CANADA', 'R9A 7H0', '1-753-917-9818', '1-131-051-7065'),
(19,'mrussel@example.net', 'Mx.', 'Schamberger', 'Lelia', 'Murazik-Fay', '7755 Klocko Mews Suite 260', NULL, 'Hauckland', 'QC', 'CANADA',  'J2W 1V1', '906-523-3664', '057-038-3189'),
(20,'hilpert.dianna@example.com', 'Mx.', 'Barton', 'Judy', 'Koch Group', '396 Leannon Pine', NULL, 'Lake Shakiraburgh', 'NL', 'CANADA', 'A1A 1A1', '1-886-576-8352', '(558)930-5785'),
(21,'hgorczany@example.org', 'Ms.', 'Reilly', 'Eliane', 'Mohr-Sporer', '6078 Raven Court', NULL, 'South Henderson', 'PE', 'CANADA', 'A1A 1A1', '1-003-694-2772', '352.753.8606'),
(22,'howe.hassan@example.net', 'Mx.', 'Koepp', 'Damian', 'Koss Inc', '0023 Madisen Loaf', NULL, 'Boehmbury', 'PE', 'CANADA', 'A1A 1A1', '961-950-1150', '09050941767'),
(23,'pouros.audrey@example.org', 'Mx.', 'Hayes', 'Caesar', 'Langworth, Rice and Rodriguez', '3476 Leanne Lodge', NULL, 'Shaniefort', 'PE', 'CANADA', 'A1A 1A1', '(648)512-2765', '888.701.7408'),
(24,'juanita.koss@example.net', 'Mr.', 'Jacobi', 'Ariane', 'Adams, Schinner and Funk', '245 Mills Isle Suite 382', NULL, 'New Jarod', 'PE', 'CANADA', 'A1A 1A1', '03072356076', '535.934.6903'),
(25,'willms.sabrina@example.org', 'Ms.', 'Bins', 'Alexie', 'Mraz Group', '7076 Denesik Flat Suite 094', NULL, 'South Annabell', 'NS', 'CANADA', 'A1A 1A1', '1-368-715-5837x882', '1-069-758-8517'),
(26,'hilpert.milo@example.com', 'Ms.', 'Adams', 'Johanna', 'Blanda Ltd', '77630 Langworth Manors', NULL, 'West Wilhelm', 'MB', 'CANADA', 'A1A 1A1', '881.908.5362x1046', '100.776.1363'),
(27,'hilpert.alvah@example.com', 'Mr.', 'Lehner', 'Kitty', 'Legros-Collins', '81588 Ayla Trail', NULL, 'Port Theodoraport', 'SK', 'CANADA', 'A1A 1A1', '111-685-0237x5837', '+70(0)1073846859'),
(28,'schaefer.emie@example.net', 'Mrs.', 'Koch', 'Virgil', NULL, '6748 Paucek Stravenue Apt. 574', NULL, 'Conroyside', 'AB', 'CANADA', 'A1A 1A1', '9606263228', '612-952-6946'),
(29,'eliseo39@example.net', 'Mx.', 'Kris', 'Major', 'Reichert, Kuhlman and Schowalter', '78831 Hank Viaduct Apt. 067', NULL, 'West Wilhelm', 'BC', 'CANADA', 'A1A 1A1', '1-450-564-4527', '262-783-1845'),
(30,'katelyn.breitenberg@example.com', 'Mx.', 'DuBuque', 'Estelle', 'Barton-Fritsch', '388 Bennie Manor Apt. 014', NULL, 'New Lesterport', 'YT', 'CANADA', 'A1A 1A1', '352-357-8058', '(215)143-8266'),
(31,'kunze.kameron@example.net', 'Ms.', 'Cummerata', 'Chesley', 'Leuschke LLC', '719 Weissnat Inlet Suite 444', NULL, 'Lake Raheem', 'NT', 'CANADA', 'A1A 1A1', '(910)926-9575', '063.725.1612'),
(32,'qrussel@example.net', 'Mx.', 'Legros', 'Terrance', 'Beier-Lang', '8182 Bonnie Cliffs Apt. 790', NULL, 'Nyafurt', 'NU', 'CANADA', 'A1A 1A1', '262-281-7853x03698', '1-889-032-1376'),
(33,'dominic39@example.net', 'Mrs.', 'Hauck', 'Hassan', 'Littel and Sons', '008 Pierce Isle Suite 023', NULL, 'Port Lulaview', 'QC', 'CANADA', 'A1A 1A1', '457-577-5452x384', '327-857-5197'),
(34,'shanie09@example.org', 'Mx.', 'Beahan', 'Eve', 'Erdman-Bartell', '133 Vandervort Roads Suite 346', NULL, 'West Fernandoside', 'NL', 'CANADA', 'A1A 1A1', '1-981-020-6506x59620', '00616783196'),
(35,'magali18@example.org', 'Miss', 'Sawayn', 'Henry', 'Grimes Ltd', '648 Jovan Canyon', NULL, 'Prosaccomouth', 'PE', 'CANADA', 'A1A 1A1', '(345)706-8775x55069', '(135)501-7458'),
(36,'nrempel@example.com', 'Mr.', 'Prosacco', 'Nelda', 'Murray-Keeling', '054 Ansley Brooks', 'Apt. 890', 'Chandlerberg', 'NS', 'CANADA', 'A1A 1A1', '663-041-5486x1052', '(585)585-7718'),
(37,'dherzog@example.com', 'Miss', 'Moen', 'Katlynn', 'Kilback and Sons', '5984 Grant Course', NULL, 'Runteburgh', 'NB', 'CANADA', 'A1A 1A1', '05023869348', '902.028.6019x3334'),
(38,'buck.barton@example.net', 'Ms.', 'Jacobson', 'Van', 'Corkery Ltd', '75972 Dominique Crossroad', NULL, 'North Ramon', 'ON', 'CANADA', 'A1A 1A1', '1-607-685-7003', '+21(3)4419575664'),
(39,'buford.hoeger@example.com', 'Mx.', 'Thompson', 'Tremaine', 'Gibson, Boyle and Skiles', '374 Freddie Fall', 'Apt. 897', 'Giovanniburgh', 'MB', 'CANADA', 'A1A 1A1', '1-391-806-8098', '140.174.1534'),
(40,'cedrick.nienow@example.net', 'Mx.', 'Hackett', 'Skye', 'Rogahn-Berge', '1672 Rebecca Light', NULL, 'Vonton', 'ON', 'CANADA', 'A1A 1A1', '1-083-983-5084x1513', '(564)311-3714'),
(41,'osmitham@example.org', 'Ms.', 'Sanford', 'Tremaine', 'Schowalter, Franecki and Beier', '81879 Borer Plaza Suite 078', NULL, 'Mortonfort', 'SK', 'CANADA', 'A1A 1A1', '(279)373-2549', '(813)490-2594'),
(42,'fkuphal@example.org', 'Mx.', 'Barrows', 'Kali', 'Stracke, Swift and Ernser', '1963 Hand Inlet', NULL, 'Corkerymouth', 'AB', 'CANADA', 'A1A 1A1', '1-128-556-1227x920', '1-717-958-5453x6189'),
(43,'antonetta36@example.org', 'Mrs.', 'Murphy', 'Cooper', 'Hoeger, Baumbach and Roberts', '97835 Braulio Ridge', NULL, 'Rhettshire', 'BC', 'CANADA', 'A1A 1A1', '05235116248', '00957760948'),
(44,'ludwig.mccullough@example.net', 'Mx.', 'Spinka', 'Adriel', 'Schulist, Ullrich and Stiedemann', '799 Brekke Branch', NULL, 'New Gianniville', 'YT', 'CANADA', 'A1A 1A1', '491-947-5792', '1-742-906-5504'),
(45,'chuels@example.org', 'Mx.', 'Kilback', 'Madaline', 'Larson, Torphy and Casper', '79086 Gregg Lodge', NULL, 'Jamesonstad', 'NT', 'CANADA', 'A1A 1A1', '585-727-5730', '848.761.1809'),
(46,'ckerluke@example.org', 'Ms.', 'Bahringer', 'Jennyfer', 'Herman-Bogisich', '28285 Renner Village', NULL, 'North Jarred', 'NU', 'CANADA', 'A1A 1A1', '1-512-367-9745x5357', '(142)558-3048'),
(47,'brandyn.gleichner@example.org', 'Mx.', 'Steuber', 'Cedrick', 'Dickens and Sons', '84743 Buckridge Burgs', NULL, 'Port Major', 'NU', 'CANADA', 'A1A 1A1', '727-304-2299', '(227)959-0125'),
(48,'kelley31@example.com', 'Mx.', 'Wiza', 'Breanne', 'Jast, Goldner and Thompson', '07426 Maiya Branch', NULL, 'North Edisonhaven', 'QC', 'CANADA', 'A1A 1A1', '537.214.5134x971', '054.332.0465'),
(49,'emery36@example.com', 'Mr.', 'Quitzon', 'Chesley', 'Greenholt Ltd', '84786 Kassulke Street', NULL, 'Nienowhaven', 'NL', 'CANADA', 'A1A 1A1', '(845)723-0137', '183.021.7738x037'),
(50,'collins.daphnee@example.com', 'Mx.', 'Hegmann', 'Audra', 'Windler-Bosco', '40327 Volkman Roads', NULL, 'Lake Luciouschester', 'PE', 'CANADA', 'A1A 1A1', '(911)155-3041x332', '1-763-692-9077'),
(55,'collier.hillary@example.org', 'Mx.', 'Littel', 'Mose', 'Gulgowski Inc', '9349 Vern Gateway', NULL, 'Burdettechester', 'NS', 'CANADA', 'A1A 1A1', '1-561-221-7005x18979', '(027)793-1071'),
(56,'lueilwitz.earline@example.com', 'Mx.', 'Rau', 'Kira', 'Jenkins-Sawayn', '523 Larue Plains', NULL, 'New Tabithafurt', 'NB', 'CANADA', 'A1A 1A1', '+60(8)1164031338', '(922)453-9519'),
(57,'darron.halvorson@example.com', 'Mx.', 'Skiles', 'Hal', 'Reynolds LLC', '5011 Jones Light', NULL, 'Fadelhaven', 'ON', 'CANADA', 'A1A 1A1', '802.103.0674', '849.121.4266'),
(58,'alyce43@example.net', 'Mr.', 'Graham', 'Raymond', 'Padberg Group', '9732 Graham Pass', 'Suite 843', 'East Cecile', 'MB', 'CANADA', 'A1A 1A1', '(332)291-8214x585', '(466)700-8349'),
(59,'angie77@example.com', 'Ms.', 'Leannon', 'Will', 'Bernhard LLC', '8829 Mosciski Island Apt. 068', NULL, 'South Abigayleview', 'SK', 'CANADA', 'A1A 1A1', '771-718-9045x880', '547-451-9152'),
(60,'beier.audreanne@example.org', 'Mx.', 'Kling', 'Tressie', 'Miller-Deckow', '50375 Cyrus Ridge Apt. 280', NULL, 'North Carissaton', 'AB', 'CANADA', 'A1A 1A1', '1-977-882-5105x20164', '713.394.0801'),
(61,'grady.pierce@example.com', 'Mx.', 'Howell', 'Savanna', 'Kilback-Simonis', '98044 Bayer Avenue', NULL, 'Shieldshaven', 'BC', 'CANADA', 'A1A 1A1', '687.148.9604x83934', '021-364-7983'),
(62,'giles89@example.org', 'Mx.', 'Block', 'Arjun', 'Grimes, Batz and Hahn', '0545 Hoyt Stream Suite 354', NULL, 'Wolffstad', 'YT', 'CANADA', 'A1A 1A1', '126.526.9502x0606', '1-865-285-9044'),
(63,'xjohns@example.com', 'Ms.', 'Hilll', 'Donny', 'Kautzer and Sons', '349 Brian Lights', NULL, 'West Arianestad', 'NT', 'CANADA', 'A1A 1A1', '012.199.1028', '(562)303-1263'),
(64,'rparisian@example.com', 'Mx.', 'Heidenreich', 'Raven', 'Hahn-Steuber', '554 Spencer Prairie Suite 936', NULL, 'Mattstad', 'NU', 'CANADA', 'A1A 1A1', '+76(9)6195728348', '366.898.7613'),
(65,'conroy.israel@example.net', 'Mr.', 'Reichert', 'Holly', 'Hegmann, Crist and Macejkovic', '42254 Bailey Mountain', NULL, 'Lake Marlonland', 'QC', 'CANADA', 'A1A 1A1', '+07(0)6412480586', '223.264.6823'),
(66,'lwintheiser@example.net', 'Mx.', 'Koelpin', 'Irving', 'Weimann PLC', '5047 Graham Pike Apt. 144', NULL, 'South Kaylee', 'ON', 'CANADA', 'A1A 1A1', '(366)010-2436', '683.060.4438'),
(67,'claire96@example.org', 'Mx.', 'Yost', 'Prudence', 'Satterfield Group', '139 Sylvia Extension Suite 972', NULL, 'Port Estella', 'ON', 'CANADA', 'A1A 1A1', '7777821595', '7983692010'),
(68,'yherzog@example.com', 'Mx.', 'Kilback', 'Rosemarie', 'Haley and Sons', '5714 Adan Hills', NULL, 'New Dexter', 'ON', 'CANADA', 'A1A 1A1', '413-573-5580', '902-903-0863'),
(69,'block.golda@example.net', 'Mx.', 'Wisozk', 'Boris', 'Walsh and Sons', '166 Waelchi Route Suite 067', NULL, 'New Barneystad', 'BC', 'CANADA', 'A1A 1A1', '1-724-689-0006x171', '1-961-748-9545'),
(70,'magdalena56@example.org', 'Mrs.', 'Wintheiser', 'Kip', 'Dooley, Goyette and Buckridge', '8530 Medhurst Manor Suite 255', NULL, 'Rebabury', 'BC', 'CANADA', 'A1A 1A1', '931-272-3518', '646.184.0203'),
(71,'von.ryder@example.com', 'Mx.', 'Frami', 'Brooks', 'Lubowitz, Bernier and Macejkovic', '2901 Fritsch Court', NULL, 'Irvingfort', 'QC', 'CANADA', 'A1A 1A1', '442-494-4911', '246.131.5887'),
(72,'nleffler@example.net', 'Miss', 'Powlowski', 'Herminio', 'Koepp-Ward', '623 Kennedy Lodge', NULL, 'Reynaburgh', 'BC', 'CANADA', 'A1A 1A1', '1-092-604-4763x27228', '1-223-347-7342'),
(73,'reanna43@example.net', 'Mx.', 'Runolfsdottir', 'Rickie', 'Johnston, Reilly and Simonis', '20989 Heaney Rest', NULL, 'Romainetown', 'SK', 'CANADA', 'A1A 1A1', '499-757-8850', '721-707-5468'),
(74,'shaniya.goyette@example.com', 'Mx.', 'Upton', 'Dina', 'McKenzie-Abernathy', '6391 Madelynn Branch', NULL, 'East Daphnee', 'AB', 'CANADA', 'A1A 1A1', '830-373-9431x80058', '1-874-646-2468'),
(75,'schoen.diamond@example.net', 'Mr.', 'Vandervort', 'Maximus', 'Wiegand Ltd', '3127 Kertzmann Ways', NULL, 'West Stewartview', 'BC', 'CANADA', 'A1A 1A1', '064.013.0119x76889', '(275)251-5927'),
(76,'parker.destinee@example.net', 'Mx.', 'Herman', 'Graham', 'Bashirian Ltd', '0754 Hilll Spur', NULL, 'East Daija', 'YT', 'CANADA', 'A1A 1A1', '(355)136-0445', '+15(9)4062899470'),
(77,'jaylon.koss@example.org', 'Mr.', 'Zieme', 'Marion', 'Schuster Inc', '49199 Dortha Divide', NULL, 'South Emil', 'NT', 'CANADA', 'A1A 1A1', '1-503-253-8277', '639.035.7058x93662'),
(78,'shemar19@example.org', 'Mr.', 'Dicki', 'Luz', 'Gaylord LLC', '126 Batz Cove Suite 353', NULL, 'Port Lelahview', 'NU', 'CANADA', 'A1A 1A1', '410-836-6212x0399', '1-563-187-6321'),
(79,'lang.uriah@example.net', 'Mx.', 'Murazik', 'Jayne', 'Ebert Group', '45332 Shannon Stravenue', NULL, 'New Noble', 'QC', 'CANADA', 'A1A 1A1', '05156709178', '1-593-105-8635'),
(80,'stacey11@example.org', 'Mr.', 'Kassulke', 'Marianne', 'Durgan-Bartoletti', '0436 Zion Mountains', NULL, 'Port Elijah', 'AB', 'CANADA', 'A1A 1A1', '09954372272', '(255)619-6528'),
(81,'unique.willms@example.com', 'Mx.', 'Gorczany', 'Giuseppe', 'Rogahn-Lubowitz', '6396 Garret Corner Apt. 663', NULL, 'Port Anastacioberg', 'NL', 'CANADA', 'A1A 1A1', '1-174-221-0546x825', '687.930.1227'),
(82,'vida60@example.org', 'Ms.', 'Kilback', 'Brendon', 'Kerluke Ltd', '42126 Alysa Knolls', NULL, 'New Michelside', 'PE', 'CANADA', 'A1A 1A1', '(662)821-5768', '277.194.9254'),
(83,'yost.ken@example.com', 'Mx.', 'Koch', 'Martine', 'Roob-Koss', '70381 Leuschke Valley Apt. 466', NULL, 'New Jaleelfurt', 'NS', 'CANADA', 'A1A 1A1', '211.853.8851x51519', '1-144-546-3693'),
(84,'qratke@example.org', 'Mx.', 'Klocko', 'Jennie', 'Kozey-Zulauf', '78055 Turner Ridges Apt. 682', NULL, 'Lake Jaylen', 'NB', 'CANADA', 'A1A 1A1', '754-330-7013', '1-938-259-4838'),
(85,'montana.harber@example.com', 'Mr.', 'Wintheiser', 'Destini', 'Ward-Kilback', '9822 Noelia Springs', NULL, 'Lefflerland', 'ON', 'CANADA', 'A1A 1A1', '359-932-3389x21452', '1-625-520-6366'),
(86,'pleffler@example.com', 'Mx.', 'Ryan', 'Dave', 'Russel, Moen and Hamill', '4253 Meaghan Meadows', NULL, 'East Valentine', 'MB', 'CANADA', 'A1A 1A1', '011-941-6794', '797-551-9673'),
(87,'ygulgowski@example.net', 'Miss', 'Feest', 'Ladarius', 'Shanahan-Dach', '73146 Hilll Dale Apt. 737', NULL, 'Marquardtfurt', 'SK', 'CANADA', 'A1A 1A1', '+55(9)2921895924', '(807)570-5852'),
(88,'eino.dickinson@example.net', 'Mx.', 'Abshire', 'Kirsten', NULL, '953 Gorczany Park', NULL, 'Gabriellemouth', 'AB', 'CANADA', 'A1A 1A1', '02271930898', '(221)495-5872'),
(89,'fay.ruthe@example.net', 'Mx.', 'Langosh', 'Noel', 'Hilpert LLC', '1624 Kasandra Way', NULL, 'Cassinburgh', 'BC', 'CANADA', 'A1A 1A1', '(267)297-7445', '171-294-7280'),
(90,'pbahringer@example.org', 'Ms.', 'Steuber', 'Idell', 'Sipes-VonRueden', '0625 Gulgowski Trace', NULL, 'North Tyrese', 'YT', 'CANADA', 'A1A 1A1', '277-560-0349', '01755540456'),
(91,'bernier.abagail@example.com', 'Ms.', 'Bashirian', 'Freeda', 'Sauer-Haag', '90117 Collins Stream Suite 062', NULL, 'Lake Myrna', 'NT', 'CANADA', 'A1A 1A1', '918-716-7769x42100', '1-466-469-1875'),
(92,'avery.klein@example.org', 'Mr.', 'Stamm', 'Imogene', NULL, '5776 Adella Stravenue', NULL, 'Whiteton', 'NU', 'CANADA', 'A1A 1A1', '(046)549-3039', '193.182.3293'),
(93,'elna.romaguera@example.org', 'Mr.', 'Rohan', 'Henri', 'Ritchie-Goyette', '27782 Muller Plains Apt. 993', NULL, 'Hegmannburgh', 'QC', 'CANADA', 'A1A 1A1', '(988)891-3612', '256-373-4074'),
(94,'hessel.felipe@example.net', 'Mx.', 'Denesik', 'Derick', 'Pouros, Zboncak and Bogisich', '58692 Jakubowski Loop', NULL, 'Wardville', 'NL', 'CANADA', 'A1A 1A1', '358.389.3345x71082', '746.365.2330'),
(95,'zwolf@example.com', 'Ms.', 'Moore', 'Heidi', 'Zemlak Group', '789 Greta Springs Apt. 784', NULL, 'Harmonchester', 'PE', 'CANADA', 'A1A 1A1', '536-993-1781', '(478)624-8222'),
(96,'mattie.ondricka@example.net', 'Mx.', 'Bednar', 'Presley', 'Wuckert Ltd', '767 Schroeder Port', NULL, 'Port Kaciechester', 'NS', 'CANADA', 'A1A 1A1', '1-857-597-1894', '656-463-0797x6926'),
(97,'unolan@example.net', 'Mrs.', 'Kihn', 'Irwin', 'McClure, Cartwright and Berge', '21335 Jalyn Prairie Apt. 422', NULL, 'Hodkiewiczshire', 'NB', 'CANADA', 'A1A 1A1', '1-288-434-9574x35548', '+50(2)7569188919'),
(98,'kuhic.armand@example.org', 'Mx.', 'Bahringer', 'Hershel', 'Jenkins Inc', '1352 Johnny Mall', NULL, 'Dareland', 'ON', 'CANADA', 'A1A 1A1', '481-853-5336x286', '+89(9)5437885916'),
(99,'dframi@example.net', 'Mr.', 'Kuhic', 'Dion', NULL, '020 Mohr Pike', 'Apt 2', 'Port Stefanie', 'MB', 'CANADA', 'A1A 1A1', '(275)610-7809x6938', '(335)949-2164'),
(100,'dorris.von@example.net', 'Mx.', 'Auer', 'Mireille', 'Cartwright PLC', '82453 Mosciski Divide Suite 206', NULL, 'New Kaylie', 'SK', 'CANADA ', 'A1A 1A1', '1-203-976-0565x5176', '010.436.0423'),
(51,'vita.dicki@example.org', 'Mrs.', 'Eichmann', 'Zander', 'Collier, Fritsch and Wilderman', '460 Becker Views', NULL, 'South Zellashire', 'AB', 'CANADA ', 'A1A 1A1', '(487)033-0089', '649-171-9487'),
(52,'durward29@example.org', 'Mr.', 'Kuhlman', 'Blanca', 'Barrows PLC', '7713 Jamar Way Suite 083', NULL, 'Beerchester', 'NL', 'CANADA', 'B3B 4B4', '467-191-8914', '683-604-6955'),
(53,'hayes.aric@example.com', 'Mrs.', 'OHara', 'Lillie', 'Murray, Weber and Kub', '59163 Corkery Lodge', NULL, 'Zoeyland', 'BC', 'CANADA ', 'A1A 1A1', '+97(2)9901626451', '814.131.8049'),
(54,'declan83@example.org', 'Mx.', 'Halvorson', 'Sibyl', 'Stoltenberg, Gutmann and Wiza', '11510 Corrine Village Suite 378', NULL, 'VonRuedenfort', 'NT', 'CANADA', 'A1A 1A1', '(256)899-2297', '319-840-6981'),
(101,'karen19@example.com', 'Mx.', 'Renner', 'Breana', 'Harvey Ltd', '6812 Serena Fords', NULL, 'Blaiseland', 'NB', 'CANADA', 'E5A 6N8', '935.469.0056x691', '9255585725'),
(102,'esteban19@example.org', 'Miss', 'Batz', 'Dorthy', 'Schaefer, Anderson and Macejkovic', '908 Kautzer Lodge Suite 765', NULL, 'Port Jadaport', 'MB', 'CANADA', 'R5A 9A2', '949-302-7236x3218', '868.030.5571');

-- Credentials Table
INSERT INTO credentials (user_id, salt, hash, manager) VALUES 
(1, 'cmt3bhdmkfo6qi78lcujlkdhcck4',x'E72248EA2F153824EA6D5F806A1B7445741C29327920DEB325A94BA93892A829', 0),
(2, 'n162c3f65l8jj0394788k07mbcp6',x'4FE347332FFC006D5AF96D662EA7CD70AC48F8ECF9694AAF7BDE185F7778E64C', 1),
(3, 'peg97b68fo6jfglaidie293uqket',x'1E66DF2DF2EC6316BBE4F647BCC73B4E3D331FCB1ADA4180C2858B1A7518CD22', 1),
(4, 'dcbk21lafg20jeutqktkotokva19',x'F2BB82F81B4FA4048AE5C08FF45754C0D2C54BE13096D2746E4CCDE66A8C9934', 1),
(5, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(6, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(7, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(8, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(9, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(10, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(11, 'mjk5vtgfkeldcvdo9avlrgpbs3h1',x'317B7BBBC1BBC4A40BE7FADC16538C32C3E01C86583D4F62492474DD9A7EB6F2', 0),
(12, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(13, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(14, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(15, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(16, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 1),
(17, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(18, 'pfpeonh3sudoajet0a0ofnorlfgc',x'69FE940E10794AD96172C4EA98375CD944C1522984AA85E946D71E7EF1A0C0E9', 0),
(19, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(20, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(21, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(22, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(23, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(24, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(25, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(26, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(27, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 1),
(28, 'u86uiu0jfva407tdb2o91k4co6h8',x'652789C1DA6296DC6C901322CD9FF460A39578D1BBF5F0A16E3BE5EC5B651ED0', 0),
(29, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(30, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(31, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(32, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(33, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(34, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(35, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(36, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(37, 'fr17i4fl1cvmk69ll02tqm7u2pcj',x'CF5D33CE136C62AA15C2FA065AC7FA84F0627E099AE8CCFC8975FA523F9839B3', 0),
(38, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(39, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(40, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(41, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(42, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(43, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(44, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(45, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 1),
(46, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(47, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(48, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(49, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(50, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(51, 'q8pef6i05oul0rppqpfocmlpctph',x'2E6F0E02DB52A13FB73F8949118DBB44CB12E61DB182EB6A573641082CA5495C', 0),
(52, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(53, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(54, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(55, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(56, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(57, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(58, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(59, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(60, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(61, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(62, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(63, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(64, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(65, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(66, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(67, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(68, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(69, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(70, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(71, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(72, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(73, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(74, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(75, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(76, 'mulsn26s056vaik4uaut6r3rhg2m',x'DE009F01D5190FB94D5EB1214F708BE51DB769B7E68603942BBE0475DC892F7C', 0),
(77, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(78, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(79, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(80, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(81, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(82, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(83, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(84, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(85, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 1),
(86, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(87, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(88, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(89, 'c8ude8cidi17spgfl7aervl8mn92',x'E213AB75221441B3AFA83FD402016C3396241F669CFDF82FB54728418906D516', 0),
(90, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(91, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(92, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(93, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(94, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(95, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(96, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(97, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(98, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(99, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(100, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 1),
(101, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0),
(102, 'cjr00pvp87sfmc9m3lmijspkmfnt',x'A80DF1ED5B37E0C8B308D8139C9D70620CE6294EAC33A423E6363ACE8AD09EA5', 0);

-- Orders Table
INSERT INTO orders(order_id,user_id,purchase_date,gst_rate,pst_rate,hst_rate) 
VALUES
 (1,1,'2010-03-07',0.000,0.000,15.000)
,(2,2,'2018-06-03',5.000,7.000,0.000)
,(3,3,'2019-09-03',0.000,0.000,13.000)
,(4,4,'2017-04-20',0.000,0.000,15.000)
,(5,5,'2011-12-16',0.000,0.000,15.000)
,(6,6,'2014-06-25',0.000,0.000,15.000)
,(7,7,'2010-11-04',5.000,9.975,0.000)
,(8,8,'2011-12-05',5.000,9.975,0.000)
,(9,9,'2017-01-12',5.000,7.000,0.000)
,(10,10,'2016-08-01',5.000,9.975,0.000)
,(11,11,'2010-06-26',5.000,9.975,0.000)
,(12,12,'2013-07-30',0.000,0.000,15.000)
,(13,13,'2018-06-17',5.000,7.000,0.000)
,(14,14,'2015-07-05',5.000,7.000,0.000)
,(15,15,'2017-12-02',0.000,0.000,15.000)
,(16,16,'2013-01-01',5.000,9.975,0.000)
,(17,17,'2019-07-18',5.000,7.000,0.000)
,(18,18,'2017-08-19',5.000,7.000,0.000)
,(19,19,'2010-06-04',5.000,9.975,0.000)
,(20,20,'2011-02-25',0.000,0.000,15.000)
,(21,21,'2016-07-26',0.000,0.000,15.000)
,(22,22,'2010-01-26',0.000,0.000,15.000)
,(23,23,'2012-05-15',0.000,0.000,15.000)
,(24,24,'2014-03-01',0.000,0.000,15.000)
,(25,25,'2015-03-23',0.000,0.000,15.000)
,(26,26,'2019-12-23',5.000,7.000,0.000)
,(27,27,'2013-04-18',5.000,6.000,0.000)
,(28,28,'2016-09-28',5.000,0.000,0.000)
,(29,29,'2018-06-24',5.000,7.000,0.000)
,(30,30,'2015-04-01',5.000,0.000,0.000)
,(31,31,'2010-10-02',5.000,0.000,0.000)
,(32,32,'2018-08-11',5.000,0.000,0.000)
,(33,33,'2011-04-22',5.000,9.975,0.000)
,(34,34,'2013-05-28',0.000,0.000,15.000)
,(35,35,'2016-06-08',0.000,0.000,15.000)
,(36,36,'2014-10-05',0.000,0.000,15.000)
,(37,37,'2015-08-20',0.000,0.000,15.000)
,(38,38,'2012-09-30',0.000,0.000,13.000)
,(39,39,'2017-05-21',5.000,7.000,0.000)
,(40,40,'2016-08-09',0.000,0.000,13.000)
,(41,41,'2013-04-30',5.000,6.000,0.000)
,(42,42,'2019-04-14',5.000,0.000,0.000)
,(43,43,'2012-06-21',5.000,7.000,0.000)
,(44,44,'2011-12-26',5.000,0.000,0.000)
,(45,45,'2015-03-26',5.000,0.000,0.000)
,(46,46,'2011-06-25',5.000,0.000,0.000)
,(47,47,'2011-09-12',5.000,0.000,0.000)
,(48,48,'2015-12-07',5.000,9.975,0.000)
,(49,49,'2011-03-26',0.000,0.000,15.000)
,(50,50,'2012-09-22',0.000,0.000,15.000)
,(51,51,'2013-03-07',5.000,0.000,0.000)
,(52,52,'2013-03-07',0.000,0.000,15.000)
,(53,53,'2018-06-25',5.000,7.000,0.000)
,(54,54,'2010-07-05',5.000,0.000,0.000)
,(55,55,'2016-09-28',0.000,0.000,15.000)
,(56,56,'2014-12-03',0.000,0.000,15.000)
,(57,57,'2016-09-29',0.000,0.000,13.000)
,(58,58,'2016-11-12',5.000,7.000,0.000)
,(59,59,'2012-09-20',5.000,6.000,0.000)
,(60,60,'2018-01-16',5.000,0.000,0.000)
,(61,61,'2015-06-24',5.000,7.000,0.000)
,(62,62,'2019-05-27',5.000,0.000,0.000)
,(63,63,'2013-05-20',5.000,0.000,0.000)
,(64,64,'2018-02-10',5.000,0.000,0.000)
,(65,65,'2013-02-26',5.000,9.975,0.000)
,(66,66,'2017-08-20',0.000,0.000,13.000)
,(67,67,'2014-05-17',0.000,0.000,13.000)
,(68,68,'2018-02-07',0.000,0.000,13.000)
,(69,69,'2019-09-20',5.000,7.000,0.000)
,(70,70,'2016-08-05',5.000,7.000,0.000)
,(71,71,'2016-07-23',5.000,9.975,0.000)
,(72,72,'2019-08-10',5.000,7.000,0.000)
,(73,73,'2014-12-31',5.000,6.000,0.000)
,(74,74,'2013-01-29',5.000,0.000,0.000)
,(75,75,'2019-08-20',5.000,7.000,0.000)
,(76,76,'2019-02-28',5.000,0.000,0.000)
,(77,77,'2015-11-30',5.000,0.000,0.000)
,(78,78,'2018-05-19',5.000,0.000,0.000)
,(79,79,'2012-04-29',5.000,9.975,0.000)
,(80,80,'2019-01-15',5.000,0.000,0.000)
,(81,81,'2015-12-26',0.000,0.000,15.000)
,(82,82,'2017-12-02',0.000,0.000,15.000)
,(83,83,'2019-04-07',0.000,0.000,15.000)
,(84,84,'2010-09-11',0.000,0.000,15.000)
,(85,85,'2013-10-09',0.000,0.000,13.000)
,(86,86,'2014-04-08',5.000,7.000,0.000)
,(87,87,'2017-05-07',5.000,6.000,0.000)
,(88,88,'2019-04-29',5.000,0.000,0.000)
,(89,89,'2015-10-13',5.000,7.000,0.000)
,(90,90,'2010-03-06',5.000,0.000,0.000)
,(91,91,'2015-01-03',5.000,0.000,0.000)
,(92,92,'2015-03-05',5.000,0.000,0.000)
,(93,93,'2016-11-06',5.000,9.975,0.000)
,(94,93,'2016-11-06',5.000,9.975,0.000)
,(95,95,'2019-08-25',0.000,0.000,15.000)
,(96,96,'2015-12-15',0.000,0.000,15.000)
,(97,97,'2017-10-31',0.000,0.000,15.000)
,(98,98,'2014-01-21',0.000,0.000,13.000)
,(99,99,'2015-06-30',5.000,7.000,0.000)
,(100,99,'2015-09-06',5.000,7.000,0.000)
,(101,2,'2018-06-07',5.000,7.000,0.000),
(102,1,'2014-08-05',0.000,0.000,15.000),
(103,2,'2017-07-03',5.000,7.000,0.000),
(104,3,'2014-02-18',0.000,0.000,13.000),
(105,4,'2011-12-15',0.000,0.000,15.000),
(106,5,'2016-03-16',0.000,0.000,15.000),
(107,6,'2014-04-15',0.000,0.000,15.000),
(108,7,'2019-12-25',5.000,9.975,0.000),
(109,8,'2016-05-07',5.000,9.975,0.000),
(110,9,'2013-10-06',5.000,7.000,0.000),
(111,10,'2010-12-27',5.000,9.975,0.000),
(112,11,'2013-09-05',5.000,9.975,0.000),
(113,12,'2012-05-17',0.000,0.000,15.000),
(114,13,'2014-07-21',5.000,7.000,0.000),
(115,14,'2017-08-02',5.000,7.000,0.000),
(116,15,'2011-10-17',0.000,0.000,15.000),
(117,16,'2010-06-29',5.000,9.975,0.000),
(118,17,'2016-12-23',5.000,7.000,0.000),
(119,18,'2019-09-17',5.000,7.000,0.000),
(120,19,'2017-10-25',5.000,9.975,0.000),
(121,20,'2011-09-10',0.000,0.000,15.000),
(122,21,'2014-05-24',0.000,0.000,15.000),
(123,22,'2012-08-05',0.000,0.000,15.000),
(124,23,'2010-07-04',0.000,0.000,15.000),
(125,24,'2011-12-23',0.000,0.000,15.000),
(126,25,'2019-11-18',0.000,0.000,15.000),
(127,26,'2012-12-27',5.000,7.000,0.000),
(128,27,'2016-11-23',5.000,6.000,0.000),
(129,28,'2017-01-11',5.000,0.000,0.000),
(130,29,'2013-03-08',5.000,7.000,0.000),
(131,30,'2016-05-20',5.000,0.000,0.000);

-- Orderitems Table
INSERT INTO orderitems(order_id, isbn, price) VALUES 
(1,9780062941008,'19.47'),
(2,9780062941008,'18.95'),
(3,9780062941008,'21.66'),
(4,9780062941008,'18.78'),
(5,9780062941008,'8'),
(6,9780062941008,'8'),
(7,9780062941008,'28.89'),
(8,9780062941008,'15.94'),
(9,9780062941008,'28.54'),
(10,9780062941008,'8'),
(11,9780889953673,'27.16'),
(12,9781894778145,'24.93'),
(13,9781894778145,'25.78'),
(14,9780888996596,'26.79'),
(15,9781772600384,'24.05'),
(16,9781459802483,'25.94'),
(17,9780062953452,'21.70'),
(18,9780062953414,'36.04'),
(19,9781554988334,'39.00'),
(20,9781554988334,'8.54'),
(21,9781620142639,'12'),
(22,9781620142639,'28.86'),
(23,9780062498564,'25.14'),
(24,9781524769567,'21.80'),
(25,9781481400701,'21.35'),
(26,9781481400701,'22.12'),
(27,9781743629000,'20.79'),
(28,9781772780109,'27.51'),
(29,9780312625993,'26.42'),
(30,9781492662143,'20.58'),
(31,9781492662143,'24.02'),
(32,9781492662143,'24.69'),
(33,9781467711944,'14.64'),
(34,9781515782223,'21.70'),
(35,9780399246531,'26.22'),
(36,9780763693558,'11'),
(37,9781426310492,'22.85'),
(38,9781580625579,'27.03'),
(39,9780486468211,'29.67'),
(40,9780486468211,'11.84'),
(41,9781641520416,'14.26'),
(42,9780692848388,'26.01'),
(43,9780692848388,'7.45'),
(44,9780692848388,'25.46'),
(45,9780547557991,'8'),
(46,9781426307041,'39.46'),
(47,9781426310140,'31.96'),
(48,9781426307935,'37.10'),
(49,9781426308468,'29.98'),
(50,9781426308468,'20.58'),
(51,9781732596344,'60.57'),
(52,9780448405179,'20.36'),
(53,9780448405179,'5.04'),
(54,9781641520928,'34.99'),
(55,9781641520928,'27.39'),
(56,9781772261059,'34.04'),
(57,9781772261059,'28.48'),
(58,9781097522132,'29.26'),
(59,9781097522132,'26.80'),
(60,9781633226241,'29.53'),
(61,9781633226241,'9.25'),
(62,9780385320436,'24.58'),
(63,9780385320436,'20.66'),
(64,9781465414182,'34.50'),
(65,9781623368890,'16.43'),
(66,9781775183310,'21.00'),
(67,9781936140404,'3.89'),
(68,9781780552491,'5'),
(69,9781580626873,'21.72'),
(69,9780062941008,'19.47'),
(70,9781580626873,'6'),
(71,9781629794228,'19.31'),
(72,9781629797694,'29.99'),
(73,9781629796987,'28.48'),
(74,9781629794235,'22.41'),
(75,9781629794242,'23.07'),
(76,9781629792033,'23.62'),
(77,9781629798301,'27.83'),
(78,9781629792668,'20.79'),
(79,9781629797687,'21.32'),
(80,9781620917640,'27.02'),
(81,9781426330179,'26.75'),
(82,9781426332852,'25.53'),
(83,9781096123163,'27.12'),
(84,9781095698655,'38.31'),
(85,9781095712245,'28.18'),
(86,9781097142804,'25.16'),
(87,9781641525312,'8'),
(88,9780606144841,'20.56'),
(89,9780545132060,'21.29'),
(90,9780810984226,'26.77'),
(91,9781603090858,'28.92'),
(92,9780375832291,'24.50'),
(93,9780375832291,'29.51'),
(94,9781608862801,'23.24'),
(95,9780606267380,'25.94'),
(96,9781596437135,'31.23'),
(97,9781449472337,'22.31'),
(98,9781449472337,'5'),
(99,9781449472337,'27.38'),
(100,9781449472337,'24.71'),
(101,9780062953452,'21.70'),
(101,9780375832291,'31.70'),
(102, 9781368007610, '16.51'),
(103, 9781408855690, '32.73'),
(104, 9781368023481, '26.79'),
(105, 9781368026697, '29.17'),
(106, 9781368023481, '41.90'),
(107, 9781484782002, '30.07'),
(108, 9781368026697, '34.22'),
(109, 9781368043632, '35.08'),
(110, 9781408855676, '17.17'),
(111, 9781368006187, '30.39'),
(112, 9781368006187, '22.63'),
(113, 9781948040686, '41.66'),
(114, 9781484782002, '29.85'),
(115, 9781368007610, '41.62'),
(116, 9781368007610, '41.18'),
(117, 9781368006187, '27.81'),
(118, 9781408855706, '15.81'),
(119, 9781368007610, '29.28'),
(120, 9781408855676, '22.90'),
(121, 9782764323519, '40.45'),
(122, 9781368006187, '25.80'),
(123, 9781408855683, '21.68'),
(124, 9781368026697, '19.59'),
(125, 9781368043632, '37.51'),
(126, 9781408855706, '44.30'),
(127, 9781368043632, '15.58'),
(128, 9781368051828, '15.43'),
(129, 9781948040686, '22.08'),
(130, 9781368043632, '29.03'),
(131, 9781368043632, '40.65'),
(102, 9781408855713, '18.51'),
(103, 9781408855676, '27.13'),
(104, 9781408855676, '30.23'),
(105, 9781408855690, '31.60'),
(106, 9781368006187, '22.00'),
(107, 9781368023481, '12.59'),
(108, 9781368053280, '10.17'),
(109, 9781408855690, '23.93'),
(110, 9781368023481, '22.24'),
(111, 9781408855690, '20.83'),
(112, 9781368051828, '33.28'),
(113, 9782764336953, '34.01'),
(114, 9781368053280, '30.09'),
(115, 9781368026697, '15.70'),
(116, 9782764336953, '10.44'),
(117, 9781408855669, '22.20'),
(118, 9781408855676, '11.64'),
(119, 9781408855676, '27.76'),
(120, 9781408855713, '28.23'),
(121, 9781368007610, '14.70'),
(122, 9781408855669, '36.91'),
(123, 9782764336953, '13.02'),
(124, 9781408855676, '30.66'),
(125, 9782764333655, '13.93'),
(126, 9782764336953, '29.44'),
(127, 9782764336953, '28.56'),
(128, 9781368053280, '29.27'),
(129, 9781368043632, '28.45'),
(130, 9781368026697, '15.37'),
(131, 9781368023481, '27.98'),
(1,9780889953673,'27.16'),
(2,9781894778145,'24.93'),
(3,9781894778145,'25.78'),
(4,9780888996596,'26.79'),
(1,9781772600384,'24.05'),
(6,9781459802483,'25.94'),
(7,9780062953452,'21.70'),
(8,9780062953414,'36.04'),
(9,9781554988334,'39.00'),
(1,9781620142639,'12'),
(11,9780062941008,'19.47'),
(12,9780062941008,'18.95'),
(13,9780062941008,'21.66'),
(14,9780062941008,'18.78'),
(15,9780062941008,'8'),
(16,9780062941008,'8'),
(17,9780062941008,'28.89'),
(18,9780062941008,'15.94'),
(19,9780062941008,'28.54'),
(32,9781620142639,'28.86'),
(43,9780062498564,'25.14'),
(54,9781524769567,'21.80'),
(65,9781481400701,'21.35'),
(66,9781481400701,'22.12'),
(67,9781743629000,'20.79'),
(78,9781772780109,'27.51'),
(89,9780312625993,'26.42'),
(90,9781492662143,'20.58'),
(91,9781492662143,'24.02'),
(102,9781492662143,'24.69'),
(103,9781467711944,'14.64'),
(114,9781515782223,'21.70'),
(115,9780399246531,'26.22'),
(126,9780763693558,'11');

-- Reviews Table
INSERT INTO `reviews`  (review_id,isbn,postdate,user_id,rating,text,approved) VALUES
('1',9781368006187,'2019-09-15 20:05:22','1','2','Quo numquam quia sed accusantium amet et ut. Earum provident id fugit nisi fugiat veniam natus et. Eos magnam id porro eos animi. Deserunt dolorum est nostrum quidem odit nobis.','1'),
('2',9781368006187,'2015-02-25 21:24:20','2','5','Eveniet adipisci aut eum quidem amet iure. Totam incidunt exercitationem eum et. Voluptatem qui rerum officiis asperiores voluptas velit eveniet. Est vitae pariatur quia est.','1'),
('3',9781368006187,'2011-10-31 23:01:03','3','5','Nam ea ipsa ex et accusamus dolores. Dolores eius nam quaerat enim necessitatibus et. Facere qui similique et incidunt eos magnam. Fugit id blanditiis ut ut.','1'),
('4',9781368006187,'2015-10-04 21:47:59','4','3','Veniam reiciendis et sed labore. Delectus optio ad alias esse in dolorem. Tenetur fugiat quaerat ut. Qui fugit sit quo omnis dolores.','1'),
('5',9781368006187,'2011-06-05 05:22:44','5','5','Excepturi sunt soluta labore est nihil. Corrupti blanditiis eaque autem est est veniam. Nesciunt possimus omnis quia vel et.','1'),
('6',9781368006187,'2018-12-19 10:33:33','6','4','Voluptate unde corrupti veritatis numquam ullam quis. Numquam sapiente consectetur et quidem. Dolor explicabo sit aut earum occaecati autem et placeat. Aliquam non quia rem cumque in alias.','1'),
('7',9781626721067,'2011-02-20 04:55:27','7','2','Dolorem maxime esse quas dignissimos consectetur illum dicta. Corrupti tempora molestiae velit quia ab. Dolorem architecto blanditiis quos non ipsa sed aut.','1'),
('8',9781608868018,'2011-06-06 23:43:32','8','3','Sequi debitis et ut expedita aliquid maxime dignissimos. Sed soluta recusandae cumque voluptate fugit. Omnis velit quasi ipsam optio impedit.','1'),
('9',9781608869008,'2019-12-17 01:21:19','9','2','Cum rerum assumenda aut sit ut. Unde blanditiis sint nihil voluptatem ipsa ut exercitationem enim. Libero ut facere molestias.','1'),
('10',9781449483500,'2014-06-14 10:20:38','10','5','Corporis esse voluptatem qui mollitia. Laudantium deleniti similique nihil quia voluptatum quia eveniet.','1'),
('11',9780062941008,'2019-04-07 20:42:35','11','4','Et et placeat sunt officiis ut quis. Quis cupiditate suscipit non nulla repellendus eaque voluptas. Architecto est quidem eum laboriosam numquam. Eos modi corporis et nihil et voluptas explicabo.','1'),
('12',9780889953673,'2010-01-13 22:30:01','12','2','Aut et ut earum repellat corrupti autem. Illo corrupti id eos omnis fuga necessitatibus aut. Modi ea ratione qui vero velit. Rerum omnis ratione aut enim.','1'),
('13',9781894778145,'2017-09-07 18:52:27','13','5','Suscipit et exercitationem dolor et a est. Est architecto sequi eos cum rerum corrupti.','1'),
('14',9781894778145,'2020-02-15 06:54:01','14','3','Animi voluptatem iure esse. Cum voluptatem veniam voluptatum harum. Molestiae nostrum quis recusandae aut fuga cum.','0'),
('15',9780888996596,'2016-09-12 23:59:10','15','4','Deserunt aliquam est doloribus animi rerum molestiae. Quam sed est vel illo aut aut. Veniam dolor ea quam deserunt quis. Id velit et dolore et.','1'),
('16',9781772600384,'2010-01-31 04:52:57','16','5','Totam temporibus error eos non placeat quod autem quia. Aut eveniet facere adipisci illum.','1'),
('17',9781459802483,'2016-12-25 22:11:23','17','5','Porro est dolore magnam quo ut qui. Vel repellendus ut hic vel molestias similique. Expedita vel ad sed laborum sint eos et consequatur. Id rerum quidem minima tenetur modi.','1'),
('18',9780062953452,'2020-01-06 18:31:33','18','3','Commodi quod quibusdam dolor aut ut labore. Voluptatem qui sed nulla unde est. Excepturi iusto corrupti numquam est autem voluptas explicabo. Voluptates similique ea rerum ut voluptatem quidem.','0'),
('19',9780062953414,'2019-07-15 22:09:28','19','2','Architecto sit voluptatem corporis ad perspiciatis quaerat. Facilis deserunt id rerum dolor. Beatae atque minus sunt ut non veniam.','1'),
('20',9781554988334,'2013-09-23 10:24:11','20','2','Dolor consectetur voluptatem mollitia quidem voluptate illo quae. Sint voluptatibus ipsam illo possimus. Dolor enim et veniam sunt libero nihil ea facilis. Quibusdam non voluptatem minima sequi.','1'),
('21',9781554988334,'2010-11-03 02:16:53','21','5','Recusandae unde in dolorum officia. Non et est incidunt in eos esse. Fuga fuga ut ut nostrum soluta. Molestiae vel necessitatibus eius temporibus enim aut.','1'),
('22',9781620142639,'2013-09-19 12:44:25','22','5','Quos sit id ea cum. Commodi et provident maxime quibusdam. Et minus quam veniam dolor molestiae.','1'),
('23',9781620142639,'2011-05-19 19:36:15','23','4','Deserunt consequatur ipsa doloribus deserunt molestias maiores officiis. Rerum harum earum voluptatibus deserunt fugit. Consequatur qui ea qui placeat dolorem sed rerum tenetur.','1'),
('24',9780062498564,'2016-02-02 03:53:48','24','5','Ab rerum commodi cupiditate neque. Quasi facere quaerat facilis quae aut quasi. Ipsa excepturi inventore maiores quos. Laudantium id non est est.','1'),
('25',9781524769567,'2019-03-18 01:43:30','25','2','Animi aut iure voluptatibus explicabo. Voluptas nihil omnis beatae nostrum consectetur odio nihil. Modi est laborum iusto ut culpa. Et mollitia reprehenderit rerum.','1'),
('26',9781481400701,'2019-07-27 06:29:20','26','4','Deserunt ducimus dignissimos velit eum quasi. Omnis soluta ut impedit iste. Maiores voluptate iure quas molestiae. Quia ut iusto aut deserunt provident dicta laborum.','1'),
('27',9781481400701,'2020-02-11 16:36:49','27','5','Error iusto quia ducimus facilis. Dolorem animi ratione iure et distinctio et ratione et. Quis minus porro minima ut ipsa quaerat animi molestias. Dolorem quia laboriosam dignissimos aut culpa.','0'),
('28',9781743629000,'2019-03-11 07:47:51','28','5','Non suscipit voluptates aut quo dignissimos. Et eius sed consectetur quod voluptatem non vero. Illo et et nisi qui labore nihil.','1'),
('29',9781772780109,'2010-12-26 10:11:59','29','4','Eos aliquam maxime enim repudiandae unde at eum. Eius atque et odit aut praesentium quia. Minus numquam officia vel non voluptates ut beatae impedit.','1'),
('30',9780312625993,'2019-08-21 12:19:25','30','5','Vel velit quas quas dolore quam quasi reprehenderit. Non nulla praesentium cupiditate alias. Quam nisi non perferendis quasi cum. Ex ea animi est tenetur quo.','1'),
('31',9781492662143,'2016-01-10 09:14:43','31','3','Alias velit omnis natus recusandae molestiae eius sit. Quis doloribus repellendus eligendi voluptas sit sit excepturi. Aut dolorem dolorem ut modi doloribus ut harum. Eos enim sed nesciunt odio.','1'),
('32',9781492662143,'2011-12-14 18:56:30','32','5','Dicta laudantium excepturi reiciendis voluptatem rerum culpa aut. Non amet quaerat ea distinctio voluptatum. Id vel sunt nihil aut assumenda.','1'),
('33',9781492662143,'2019-03-12 13:06:02','33','3','Ut voluptas maxime in omnis explicabo magni. Ut ut est non voluptatibus id excepturi ut. Qui rerum praesentium sapiente ipsa illum. Dolorem et reiciendis totam reprehenderit rerum quaerat.','1'),
('34',9781467711944,'2015-01-23 01:56:42','34','4','Voluptatem ea velit laborum quidem aliquid vero magnam. Nostrum asperiores quam officiis totam ad aut consequatur.','0'),
('35',9781515782223,'2019-01-06 15:23:27','35','5','Vel id esse veniam voluptatibus. Doloribus qui est quis quod in aut dolores. Alias labore ipsa deleniti molestias rerum vero.','1'),
('36',9780399246531,'2012-01-26 10:07:37','36','5','Beatae unde nemo sit. Exercitationem modi quia tenetur ut iusto ullam. Qui id earum sunt veritatis maiores impedit ut.','1'),
('37',9780763693558,'2010-07-10 20:05:25','37','3','Similique delectus ipsam dolor quo id non inventore aspernatur. Ut recusandae reprehenderit cumque facere. Voluptatum voluptatum dolor eos rerum vitae vero reiciendis.','1'),
('38',9781426310492,'2019-08-04 10:45:24','38','4','Voluptatibus fugiat qui quaerat et. Voluptatem ea perspiciatis libero esse libero in. Earum officia inventore dolorum quis.','1'),
('39',9781580625579,'2018-12-27 05:30:33','39','5','Voluptatum qui ipsum veniam a rem officia. Sunt repudiandae quidem voluptatem aut sequi. Provident esse sint nihil et impedit.','1'),
('40',9780486468211,'2015-06-15 19:55:25','40','5','Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.','1'),
('41',9780486468211,'2012-03-01 13:48:55','41','2','Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.','1'),
('42',9781641520416,'2016-02-18 22:34:49','42','2','Doloribus explicabo eos repellat. Laboriosam deleniti ut dolorem illum. Laborum non omnis nihil.','1'),
('43',9780692848388,'2017-11-13 03:22:28','43','2','Dolores earum eos sed voluptatibus sit. Delectus culpa repellat porro magnam delectus minima assumenda. Velit in minus ad est omnis. Nobis numquam nulla quia.','0'),
('44',9780692848388,'2019-02-23 10:15:01','44','2','Voluptatem veritatis nobis omnis voluptatem. Esse assumenda quo sequi dignissimos omnis. Sequi minima rerum accusamus tempore ea quaerat.','1'),
('45',9780692848388,'2019-08-12 00:07:19','45','3','Aut ipsum sapiente quia sit. Eaque ut quis voluptas consequuntur odit quia. Excepturi impedit cupiditate rerum provident dolores aut.','1'),
('46',9780547557991,'2015-11-14 20:39:22','46','5','Sunt et est voluptas aspernatur. Error soluta amet inventore. Laudantium vitae in ut dolor corporis qui.','1'),
('47',9781426307041,'2019-11-30 20:13:38','47','5','Ullam dolores distinctio illum hic cupiditate quisquam. Optio velit in ullam voluptas illum sed a sed.','1'),
('48',9781426310140,'2016-04-07 04:14:34','48','4','Totam nulla est est accusantium assumenda accusantium doloremque. Voluptate id in voluptatem et enim fuga magni. Sed repellat nihil vero cumque suscipit consequatur.','0'),
('49',9781426307935,'2016-06-19 05:12:42','49','5','Nisi ipsa qui voluptatem eligendi. Enim deleniti magnam quaerat consequatur ipsum id. Iure dolorem sunt accusamus itaque qui. Voluptate accusantium architecto quam repellendus aut voluptate.','1'),
('50',9781426308468,'2011-12-24 10:51:42','50','5','Totam eveniet ut ut sunt id rerum dicta. Aut aut dolore animi.\nAspernatur est est et fugiat aut qui blanditiis. Soluta quia et possimus. Quasi autem quasi aut hic earum aut.','1'),
('51',9781426308468,'2017-05-28 15:50:09','51','2','Qui dolores animi odio rerum porro. Sapiente veniam architecto molestiae animi magni. Maxime quos odit dolor dolorem ut. A sed sit quis.','1'),
('52',9781732596344,'2017-01-23 04:27:29','52','3','Consequatur reprehenderit possimus consequatur nulla. Blanditiis deserunt dolorem ut voluptatem est qui. Quam provident doloribus quam et aliquid voluptas et. Excepturi numquam ipsum qui error.','1'),
('53',9780448405179,'2019-04-07 18:43:46','53','2','Cumque quia eligendi ipsum reprehenderit harum velit id hic. A corporis deserunt unde at. Impedit ipsum quae ea velit quia occaecati velit.','1'),
('54',9780448405179,'2010-05-07 15:24:16','54','4','Aut hic ex necessitatibus cupiditate saepe. Quasi sapiente illo illum est. Nemo est et tempora laudantium iste. Accusantium est est laboriosam repellat pariatur perferendis voluptatem.','1'),
('55',9781641520928,'2010-11-22 03:38:28','55','2','Laboriosam ut culpa dolorem sunt debitis. Voluptatem esse accusamus dolorem impedit. Corporis est rem temporibus odit sapiente facilis. Est labore eos possimus non.','0'),
('56',9781641520928,'2010-05-24 10:36:42','56','5','Dolorem sed et itaque ipsa eaque occaecati saepe. Earum et error totam quisquam nam aut voluptatibus. Perferendis vero eius reiciendis aut. Eos exercitationem magni occaecati ad.','1'),
('57',9781772261059,'2018-02-24 21:53:58','57','5','Rem aut et molestiae temporibus aut. A eos accusamus ut maxime quibusdam laborum. Nihil veritatis velit voluptatem.','1'),
('58',9781772261059,'2010-10-03 07:29:52','58','5','Vero ipsum quisquam enim et maiores voluptatem ut. Enim quibusdam error temporibus totam. Cum eos quia voluptas repudiandae est.','1'),
('59',9781097522132,'2012-02-17 03:56:19','59','5','Dolorum est voluptatibus modi quia quis dolores. Aut ab quis cumque ducimus quas ab iure ullam. Omnis id tempore quos nihil occaecati nihil aspernatur. Nihil qui eum fuga recusandae quia qui et.','1'),
('60',9781097522132,'2016-06-03 17:40:12','60','2','Explicabo ratione numquam corporis ut est temporibus. Aut possimus sit numquam aut. Iusto pariatur quaerat et consectetur consequatur.','1'),
('61',9781633226241,'2015-11-24 13:00:44','61','4','Non ut debitis occaecati et. Quae sit esse est ut quia.','1'),
('62',9781633226241,'2014-05-17 00:46:37','62','5','Saepe cupiditate eos ut nesciunt nihil porro minima. Id mollitia maxime dolorem et similique. Maxime numquam at nam animi qui ut.','1'),
('63',9780385320436,'2010-07-05 13:38:03','63','2','Ipsum qui assumenda rerum fugit voluptates temporibus. Eos voluptas ipsa tempore omnis modi eius. Perspiciatis voluptas nam laboriosam mollitia nesciunt ad.','0'),
('64',9780385320436,'2016-04-22 22:28:05','64','2','Illo voluptatem dolorem ea eum fugit dolorem est. Asperiores impedit optio quo sint ab maiores. Dolores qui explicabo soluta deserunt. Velit accusantium eum ratione porro sint quibusdam.','1'),
('65',9781465414182,'2014-11-20 07:26:22','65','5','Officiis quaerat sunt rerum dolore. Non suscipit alias similique. In hic velit quia est autem. Voluptate molestias facere ut provident.','1'),
('66',9781623368890,'2019-03-02 04:40:38','66','5','Et saepe asperiores vel voluptates sunt est sit. Quod et sit dolorem vitae atque. Temporibus maiores ullam ut qui occaecati quis est sint.','1'),
('67',9781775183310,'2016-04-08 09:49:47','67','2','Ut nemo numquam corporis esse sit repudiandae molestiae eum. Sed accusantium ut est ipsa autem voluptas doloremque deserunt. Velit vel autem est vitae quisquam.','1'),
('68',9781936140404,'2019-02-25 00:13:27','68','5','Harum reprehenderit et eos provident animi deserunt. Voluptas voluptatem quis repellat ut sed cum. Corporis nihil aperiam quas ab laborum.','1'),
('69',9781780552491,'2011-12-18 03:37:31','69','2','Et omnis id minima dolorem veniam explicabo eius cum. Earum deserunt suscipit voluptatem rerum. Debitis non architecto quia cupiditate. Repellat nihil aut qui ipsum voluptas quisquam.','1'),
('70',9781580626873,'2016-08-05 02:41:36','70','2','Exercitationem dolores similique aut dignissimos accusamus tempora iusto a. Corrupti rerum velit consequatur et.','0'),
('71',9781580626873,'2017-01-28 01:11:45','71','3','Voluptas debitis quia fugit non libero ut. Quibusdam ut sit ea consequatur natus. Fuga iure debitis amet quo exercitationem autem et. Ut inventore mollitia aut eum deserunt dolores.','1'),
('72',9781629794228,'2017-08-08 23:56:00','72','2','Laborum accusamus voluptatum architecto est. Iste cupiditate est sequi similique dolorum mollitia ratione. Sunt ut alias fuga et.','1'),
('73',9781629797694,'2010-07-10 15:33:49','73','2','In quidem numquam esse suscipit. Labore est ea veritatis pariatur. Velit cum excepturi delectus. Et veritatis quos vel et.','1'),
('74',9781629796987,'2019-03-08 02:05:15','74','4','Provident ut laboriosam vero harum cupiditate vel nostrum consequatur. Et enim error consectetur fugiat tempora voluptas. Sunt repudiandae cumque rerum maxime facilis.','1'),
('75',9781629794235,'2018-05-21 14:02:38','75','3','Quasi velit provident sunt cum doloribus consequatur. Et impedit harum omnis nesciunt cum nihil porro non.','1'),
('76',9781629794242,'2018-05-17 04:12:47','76','3','Nam nulla consectetur id omnis quibusdam. Magni ducimus ut vero et et. Ducimus porro quidem sit. Saepe explicabo laborum id a quis pariatur asperiores.','1'),
('77',9781629792033,'2017-07-08 11:37:41','77','3','Nam inventore labore accusamus eveniet ex voluptatem. Ut odit et veniam quia voluptate. Eos laboriosam culpa voluptatem omnis minus qui.','1'),
('78',9781629798301,'2013-02-26 00:52:17','78','5','Consectetur unde esse autem omnis sed ut voluptatibus. Odit iusto quia ab voluptatem. Id voluptatem et eos iure sit consequatur.','1'),
('79',9781629792668,'2019-07-28 18:27:45','79','3','Aut numquam sed et reiciendis adipisci voluptas qui. Distinctio et est et dicta rem.','1'),
('80',9781629797687,'2019-10-14 16:09:56','80','5','Dolorum delectus quia autem. Ea voluptates maiores qui. Cum aut ipsum ducimus qui dolores nostrum.','1'),
('81',9781620917640,'2011-02-21 17:12:49','81','3','Laboriosam eum rerum voluptates. Quis excepturi consequatur aut sed adipisci. Cumque aut aperiam dolores tenetur. Voluptas similique quis ea ipsam.','1'),
('82',9781426330179,'2011-05-30 16:05:04','82','5','Fugiat quis placeat et dolores voluptatem. Velit occaecati cum sit ratione. Mollitia officia ea reiciendis vel aliquid. Quaerat excepturi nostrum dolorem magni non nemo omnis.','1'),
('83',9781426332852,'2012-03-22 18:53:37','83','2','Soluta laboriosam maiores et ea. Vel odio error blanditiis quis.\nAutem voluptas harum nemo laboriosam quasi sed. Delectus et odio aut nobis reprehenderit excepturi adipisci quas.','1'),
('84',9781096123163,'2015-08-10 23:04:34','84','3','Qui nesciunt sit dolorem ipsa fugiat. Illum autem cupiditate aliquid et atque animi. Sed iure reiciendis consequatur ea occaecati. Non quisquam aspernatur quaerat ullam nobis.','1'),
('85',9781095698655,'2011-06-13 12:41:46','85','5','Voluptatem animi in cumque autem necessitatibus ipsa amet. Et eum praesentium consequatur error. Et reprehenderit sint facilis excepturi.','1'),
('86',9781095712245,'2017-06-24 19:43:32','86','5','Voluptates cum accusamus molestias dolorem. Ut aliquam quasi et nobis. Explicabo deleniti esse dolorem cumque.','1'),
('87',9781097142804,'2017-12-11 02:35:06','87','3','Reprehenderit unde sint laudantium eveniet eum dolores ducimus. Nisi aut architecto repellat est.','1'),
('88',9781641525312,'2015-02-10 09:15:28','88','4','Ex atque pariatur quas facilis. Alias voluptatem modi et deserunt ad consequatur. Quo libero dolores harum dolor ut.','1'),
('89',9780606144841,'2011-02-27 07:31:24','89','5','Harum quisquam sint voluptatibus fugiat et. Qui et alias qui hic. Minima ut est repudiandae id culpa minus.','1'),
('90',9780545132060,'2010-09-04 23:59:30','90','5','Harum provident explicabo accusamus asperiores ea ex quae. Laborum repudiandae et eligendi id. Eveniet expedita recusandae voluptas expedita. Molestias iure consectetur omnis fuga.','1'),
('91',9780810984226,'2010-09-27 06:12:09','91','5','Molestias omnis vel nemo omnis commodi et. Incidunt ad harum unde deserunt atque rerum. Aspernatur ducimus aut dolor tempore necessitatibus delectus sint. Odit et architecto eligendi fugit.','1'),
('92',9781603090858,'2016-10-22 14:14:53','92','2','Architecto sunt nam est qui ut. Voluptatem debitis error quos labore voluptatem blanditiis earum rerum. Consequatur sed facere sequi natus aliquam.','1'),
('93',9780375832291,'2012-08-31 05:18:29','93','4','At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.','1'),
('94',9780375832291,'2016-10-22 09:40:58','94','3','Illo consectetur et eos. Aperiam aut soluta optio omnis exercitationem. Voluptas atque dolores laborum assumenda rem.','1'),
('95',9781608862801,'2011-11-05 06:44:03','95','5','Provident ad consequatur quam consequatur ducimus cumque eum. Sunt eligendi amet quidem assumenda quae. Voluptas sint sed ut odio.','1'),
('96',9780606267380,'2018-03-30 01:45:24','96','3','In ut non saepe consequatur dolor nulla. Cumque qui et corrupti. Nesciunt est et commodi qui.','1'),
('97',9781596437135,'2017-07-28 00:52:08','97','2','Ad quisquam dolorem incidunt. Laboriosam et dolores magni quisquam omnis. In ut repellendus rerum placeat officia molestias. Deleniti cum iste dolores qui rem cum voluptatem.','1'),
('98',9781449472337,'2014-06-02 19:50:18','98','4','Est aspernatur voluptates autem. Eveniet soluta qui quibusdam asperiores. Ea nulla quos aperiam ratione sequi cumque ut.','1'),
('99',9780439846806,'2010-11-12 13:21:04','99','2','Sit ut maiores fugit accusantium. Nostrum eveniet est perspiciatis aut beatae. Saepe asperiores enim adipisci voluptas soluta. Vitae in alias est optio excepturi.','1'),
('100',9781524852269,'2015-08-31 05:38:41','100','3','Praesentium repellendus illum dolor sed ducimus. Modi itaque laudantium molestiae repellat delectus. Laboriosam earum est est occaecati cupiditate ut soluta.','1'),
('301',9780439706407,'2019-09-15 20:05:22','11','2','Quo numquam quia sed accusantium amet et ut. Earum provident id fugit nisi fugiat veniam natus et. Eos magnam id porro eos animi. Deserunt dolorum est nostrum quidem odit nobis.','1'),
('302',9781419712173,'2015-02-25 21:24:20','21','5','Eveniet adipisci aut eum quidem amet iure. Totam incidunt exercitationem eum et. Voluptatem qui rerum officiis asperiores voluptas velit eveniet. Est vitae pariatur quia est.','1'),
('303',9780385265201,'2011-10-31 23:01:03','31','5','Nam ea ipsa ex et accusamus dolores. Dolores eius nam quaerat enim necessitatibus et. Facere qui similique et incidunt eos magnam. Fugit id blanditiis ut ut.','1'),
('304',9781608869930,'2015-10-04 21:47:59','41','3','Veniam reiciendis et sed labore. Delectus optio ad alias esse in dolorem. Tenetur fugiat quaerat ut. Qui fugit sit quo omnis dolores.','1'),
('305',9781401273941,'2011-06-05 05:22:44','51','5','Excepturi sunt soluta labore est nihil. Corrupti blanditiis eaque autem est est veniam. Nesciunt possimus omnis quia vel et.','1'),
('306',9781368007610,'2018-12-19 10:33:33','61','4','Voluptate unde corrupti veritatis numquam ullam quis. Numquam sapiente consectetur et quidem. Dolor explicabo sit aut earum occaecati autem et placeat. Aliquam non quia rem cumque in alias.','1'),
('307',9781368023481,'2011-02-20 04:55:27','71','2','Dolorem maxime esse quas dignissimos consectetur illum dicta. Corrupti tempora molestiae velit quia ab. Dolorem architecto blanditiis quos non ipsa sed aut.','1'),
('308',9781368043632,'2011-06-06 23:43:32','18','3','Sequi debitis et ut expedita aliquid maxime dignissimos. Sed soluta recusandae cumque voluptate fugit. Omnis velit quasi ipsam optio impedit.','1'),
('309',9781368051828,'2019-12-17 01:21:19','19','2','Cum rerum assumenda aut sit ut. Unde blanditiis sint nihil voluptatem ipsa ut exercitationem enim. Libero ut facere molestias.','1'),
('310',9781368053280,'2014-06-14 10:20:38','1','5','Corporis esse voluptatem qui mollitia. Laudantium deleniti similique nihil quia voluptatum quia eveniet.','1'),
('311',9780062941008,'2019-04-07 20:42:35','1','4','Et et placeat sunt officiis ut quis. Quis cupiditate suscipit non nulla repellendus eaque voluptas. Architecto est quidem eum laboriosam numquam. Eos modi corporis et nihil et voluptas explicabo.','1'),
('312',9780889953673,'2010-01-13 22:30:01','2','2','Aut et ut earum repellat corrupti autem. Illo corrupti id eos omnis fuga necessitatibus aut. Modi ea ratione qui vero velit. Rerum omnis ratione aut enim.','1'),
('313',9781894778145,'2017-09-07 18:52:27','3','5','Suscipit et exercitationem dolor et a est. Est architecto sequi eos cum rerum corrupti.','1'),
('314',9781894778145,'2010-02-15 06:54:01','4','3','Animi voluptatem iure esse. Cum voluptatem veniam voluptatum harum. Molestiae nostrum quis recusandae aut fuga cum.','1'),
('315',9780888996596,'2016-09-12 23:59:10','19','4','Deserunt aliquam est doloribus animi rerum molestiae. Quam sed est vel illo aut aut. Veniam dolor ea quam deserunt quis. Id velit et dolore et.','1'),
('316',9781772600384,'2010-01-31 04:52:57','19','5','Totam temporibus error eos non placeat quod autem quia. Aut eveniet facere adipisci illum.','1'),
('317',9781459802483,'2016-12-25 22:11:23','19','5','Porro est dolore magnam quo ut qui. Vel repellendus ut hic vel molestias similique. Expedita vel ad sed laborum sint eos et consequatur. Id rerum quidem minima tenetur modi.','1'),
('318',9780062953452,'2016-01-06 18:31:33','19','3','Commodi quod quibusdam dolor aut ut labore. Voluptatem qui sed nulla unde est. Excepturi iusto corrupti numquam est autem voluptas explicabo. Voluptates similique ea rerum ut voluptatem quidem.','1'),
('319',9780062953414,'2019-07-15 22:09:28','99','2','Architecto sit voluptatem corporis ad perspiciatis quaerat. Facilis deserunt id rerum dolor. Beatae atque minus sunt ut non veniam.','1'),
('320',9781554988334,'2013-09-23 10:24:11','42','2','Dolor consectetur voluptatem mollitia quidem voluptate illo quae. Sint voluptatibus ipsam illo possimus. Dolor enim et veniam sunt libero nihil ea facilis. Quibusdam non voluptatem minima sequi.','1'),
('321',9781554988334,'2010-11-03 02:16:53','42','5','Recusandae unde in dolorum officia. Non et est incidunt in eos esse. Fuga fuga ut ut nostrum soluta. Molestiae vel necessitatibus eius temporibus enim aut.','1'),
('322',9781620142639,'2013-09-19 12:44:25','45','5','Quos sit id ea cum. Commodi et provident maxime quibusdam. Et minus quam veniam dolor molestiae.','1'),
('323',9781620142639,'2011-05-19 19:36:15','53','4','Deserunt consequatur ipsa doloribus deserunt molestias maiores officiis. Rerum harum earum voluptatibus deserunt fugit. Consequatur qui ea qui placeat dolorem sed rerum tenetur.','1'),
('324',9780062498564,'2016-02-02 03:53:48','54','5','Ab rerum commodi cupiditate neque. Quasi facere quaerat facilis quae aut quasi. Ipsa excepturi inventore maiores quos. Laudantium id non est est.','1'),
('325',9781524769567,'2019-03-18 01:43:30','55','2','Animi aut iure voluptatibus explicabo. Voluptas nihil omnis beatae nostrum consectetur odio nihil. Modi est laborum iusto ut culpa. Et mollitia reprehenderit rerum.','1'),
('326',9781481400701,'2019-07-27 06:29:20','56','4','Deserunt ducimus dignissimos velit eum quasi. Omnis soluta ut impedit iste. Maiores voluptate iure quas molestiae. Quia ut iusto aut deserunt provident dicta laborum.','1'),
('327',9781481400701,'2014-12-11 16:36:49','57','5','Error iusto quia ducimus facilis. Dolorem animi ratione iure et distinctio et ratione et. Quis minus porro minima ut ipsa quaerat animi molestias. Dolorem quia laboriosam dignissimos aut culpa.','1'),
('328',9781743629000,'2019-03-11 07:47:51','58','5','Non suscipit voluptates aut quo dignissimos. Et eius sed consectetur quod voluptatem non vero. Illo et et nisi qui labore nihil.','1'),
('329',9781772780109,'2010-12-26 10:11:59','25','4','Eos aliquam maxime enim repudiandae unde at eum. Eius atque et odit aut praesentium quia. Minus numquam officia vel non voluptates ut beatae impedit.','1'),
('330',9780312625993,'2019-08-21 12:19:25','35','5','Vel velit quas quas dolore quam quasi reprehenderit. Non nulla praesentium cupiditate alias. Quam nisi non perferendis quasi cum. Ex ea animi est tenetur quo.','1'),
('331',9781492662143,'2016-01-10 09:14:43','35','3','Alias velit omnis natus recusandae molestiae eius sit. Quis doloribus repellendus eligendi voluptas sit sit excepturi. Aut dolorem dolorem ut modi doloribus ut harum. Eos enim sed nesciunt odio.','1'),
('332',9781492662143,'2011-12-14 18:56:30','35','5','Dicta laudantium excepturi reiciendis voluptatem rerum culpa aut. Non amet quaerat ea distinctio voluptatum. Id vel sunt nihil aut assumenda.','1'),
('333',9781492662143,'2019-03-12 13:06:02','35','3','Ut voluptas maxime in omnis explicabo magni. Ut ut est non voluptatibus id excepturi ut. Qui rerum praesentium sapiente ipsa illum. Dolorem et reiciendis totam reprehenderit rerum quaerat.','1'),
('334',9781467711944,'2015-01-23 01:56:42','35','4','Voluptatem ea velit laborum quidem aliquid vero magnam. Nostrum asperiores quam officiis totam ad aut consequatur.','1'),
('335',9781515782223,'2019-01-06 15:23:27','55','5','Vel id esse veniam voluptatibus. Doloribus qui est quis quod in aut dolores. Alias labore ipsa deleniti molestias rerum vero.','1'),
('336',9780399246531,'2012-01-26 10:07:37','56','5','Beatae unde nemo sit. Exercitationem modi quia tenetur ut iusto ullam. Qui id earum sunt veritatis maiores impedit ut.','1'),
('337',9780763693558,'2010-07-10 20:05:25','57','3','Similique delectus ipsam dolor quo id non inventore aspernatur. Ut recusandae reprehenderit cumque facere. Voluptatum voluptatum dolor eos rerum vitae vero reiciendis.','1'),
('338',9781426310492,'2019-08-04 10:45:24','58','4','Voluptatibus fugiat qui quaerat et. Voluptatem ea perspiciatis libero esse libero in. Earum officia inventore dolorum quis.','1'),
('339',9781580625579,'2018-12-27 05:30:33','59','5','Voluptatum qui ipsum veniam a rem officia. Sunt repudiandae quidem voluptatem aut sequi. Provident esse sint nihil et impedit.','1'),
('340',9780486468211,'2015-06-15 19:55:25','50','5','Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.','1'),
('341',9780486468211,'2012-03-01 13:48:55','51','2','Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.','1'),
('342',9781641520416,'2016-02-18 22:34:49','52','2','Doloribus explicabo eos repellat. Laboriosam deleniti ut dolorem illum. Laborum non omnis nihil.','1'),
('343',9780692848388,'2017-11-13 03:22:28','53','2','Dolores earum eos sed voluptatibus sit. Delectus culpa repellat porro magnam delectus minima assumenda. Velit in minus ad est omnis. Nobis numquam nulla quia.','1'),
('344',9780692848388,'2019-02-23 10:15:01','54','2','Voluptatem veritatis nobis omnis voluptatem. Esse assumenda quo sequi dignissimos omnis. Sequi minima rerum accusamus tempore ea quaerat.','1'),
('345',9780692848388,'2019-08-12 00:07:19','55','3','Aut ipsum sapiente quia sit. Eaque ut quis voluptas consequuntur odit quia. Excepturi impedit cupiditate rerum provident dolores aut.','1'),
('346',9780547557991,'2015-11-14 20:39:22','56','5','Sunt et est voluptas aspernatur. Error soluta amet inventore. Laudantium vitae in ut dolor corporis qui.','1'),
('347',9781426307041,'2019-11-30 20:13:38','57','5','Ullam dolores distinctio illum hic cupiditate quisquam. Optio velit in ullam voluptas illum sed a sed.','1'),
('348',9781426310140,'2016-04-07 04:14:34','58','4','Totam nulla est est accusantium assumenda accusantium doloremque. Voluptate id in voluptatem et enim fuga magni. Sed repellat nihil vero cumque suscipit consequatur.','1'),
('349',9781426307935,'2016-06-19 05:12:42','59','5','Nisi ipsa qui voluptatem eligendi. Enim deleniti magnam quaerat consequatur ipsum id. Iure dolorem sunt accusamus itaque qui. Voluptate accusantium architecto quam repellendus aut voluptate.','1'),
('350',9781426308468,'2011-12-24 10:51:42','60','5','Totam eveniet ut ut sunt id rerum dicta. Aut aut dolore animi.\nAspernatur est est et fugiat aut qui blanditiis. Soluta quia et possimus. Quasi autem quasi aut hic earum aut.','1'),
('351',9781426308468,'2017-05-28 15:50:09','61','2','Qui dolores animi odio rerum porro. Sapiente veniam architecto molestiae animi magni. Maxime quos odit dolor dolorem ut. A sed sit quis.','1'),
('352',9781732596344,'2017-01-23 04:27:29','62','3','Consequatur reprehenderit possimus consequatur nulla. Blanditiis deserunt dolorem ut voluptatem est qui. Quam provident doloribus quam et aliquid voluptas et. Excepturi numquam ipsum qui error.','1'),
('353',9780448405179,'2019-04-07 18:43:46','63','2','Cumque quia eligendi ipsum reprehenderit harum velit id hic. A corporis deserunt unde at. Impedit ipsum quae ea velit quia occaecati velit.','1'),
('354',9780448405179,'2010-05-07 15:24:16','64','4','Aut hic ex necessitatibus cupiditate saepe. Quasi sapiente illo illum est. Nemo est et tempora laudantium iste. Accusantium est est laboriosam repellat pariatur perferendis voluptatem.','1'),
('355',9781641520928,'2010-11-22 03:38:28','65','2','Laboriosam ut culpa dolorem sunt debitis. Voluptatem esse accusamus dolorem impedit. Corporis est rem temporibus odit sapiente facilis. Est labore eos possimus non.','1'),
('356',9781641520928,'2010-05-24 10:36:42','66','5','Dolorem sed et itaque ipsa eaque occaecati saepe. Earum et error totam quisquam nam aut voluptatibus. Perferendis vero eius reiciendis aut. Eos exercitationem magni occaecati ad.','1'),
('357',9781772261059,'2018-02-24 21:53:58','67','5','Rem aut et molestiae temporibus aut. A eos accusamus ut maxime quibusdam laborum. Nihil veritatis velit voluptatem.','1'),
('358',9781772261059,'2010-10-03 07:29:52','68','5','Vero ipsum quisquam enim et maiores voluptatem ut. Enim quibusdam error temporibus totam. Cum eos quia voluptas repudiandae est.','1'),
('359',9781097522132,'2012-02-17 03:56:19','69','5','Dolorum est voluptatibus modi quia quis dolores. Aut ab quis cumque ducimus quas ab iure ullam. Omnis id tempore quos nihil occaecati nihil aspernatur. Nihil qui eum fuga recusandae quia qui et.','1'),
('360',9781097522132,'2016-06-03 17:40:12','80','2','Explicabo ratione numquam corporis ut est temporibus. Aut possimus sit numquam aut. Iusto pariatur quaerat et consectetur consequatur.','1'),
('361',9781633226241,'2015-11-24 13:00:44','81','4','Non ut debitis occaecati et. Quae sit esse est ut quia.','1'),
('362',9781633226241,'2014-05-17 00:46:37','82','5','Saepe cupiditate eos ut nesciunt nihil porro minima. Id mollitia maxime dolorem et similique. Maxime numquam at nam animi qui ut.','1'),
('363',9780385320436,'2010-07-05 13:38:03','83','2','Ipsum qui assumenda rerum fugit voluptates temporibus. Eos voluptas ipsa tempore omnis modi eius. Perspiciatis voluptas nam laboriosam mollitia nesciunt ad.','1'),
('364',9780385320436,'2016-04-22 22:28:05','84','2','Illo voluptatem dolorem ea eum fugit dolorem est. Asperiores impedit optio quo sint ab maiores. Dolores qui explicabo soluta deserunt. Velit accusantium eum ratione porro sint quibusdam.','1'),
('365',9781465414182,'2014-11-20 07:26:22','85','5','Officiis quaerat sunt rerum dolore. Non suscipit alias similique. In hic velit quia est autem. Voluptate molestias facere ut provident.','1'),
('366',9781623368890,'2019-03-02 04:40:38','86','5','Et saepe asperiores vel voluptates sunt est sit. Quod et sit dolorem vitae atque. Temporibus maiores ullam ut qui occaecati quis est sint.','1'),
('367',9781775183310,'2016-04-08 09:49:47','87','2','Ut nemo numquam corporis esse sit repudiandae molestiae eum. Sed accusantium ut est ipsa autem voluptas doloremque deserunt. Velit vel autem est vitae quisquam.','1'),
('368',9781936140404,'2019-02-25 00:13:27','88','5','Harum reprehenderit et eos provident animi deserunt. Voluptas voluptatem quis repellat ut sed cum. Corporis nihil aperiam quas ab laborum.','1'),
('369',9781780552491,'2011-12-18 03:37:31','89','2','Et omnis id minima dolorem veniam explicabo eius cum. Earum deserunt suscipit voluptatem rerum. Debitis non architecto quia cupiditate. Repellat nihil aut qui ipsum voluptas quisquam.','1'),
('370',9781580626873,'2016-08-05 02:41:36','70','2','Exercitationem dolores similique aut dignissimos accusamus tempora iusto a. Corrupti rerum velit consequatur et.','1'),
('371',9781580626873,'2017-01-28 01:11:45','71','3','Voluptas debitis quia fugit non libero ut. Quibusdam ut sit ea consequatur natus. Fuga iure debitis amet quo exercitationem autem et. Ut inventore mollitia aut eum deserunt dolores.','1'),
('372',9781629794228,'2017-08-08 23:56:00','72','2','Laborum accusamus voluptatum architecto est. Iste cupiditate est sequi similique dolorum mollitia ratione. Sunt ut alias fuga et.','1'),
('373',9781629797694,'2010-07-10 15:33:49','73','2','In quidem numquam esse suscipit. Labore est ea veritatis pariatur. Velit cum excepturi delectus. Et veritatis quos vel et.','1'),
('374',9781629796987,'2019-03-08 02:05:15','74','4','Provident ut laboriosam vero harum cupiditate vel nostrum consequatur. Et enim error consectetur fugiat tempora voluptas. Sunt repudiandae cumque rerum maxime facilis.','1'),
('375',9781629794235,'2018-05-21 14:02:38','75','3','Quasi velit provident sunt cum doloribus consequatur. Et impedit harum omnis nesciunt cum nihil porro non.','1'),
('376',9781629794242,'2018-05-17 04:12:47','76','3','Nam nulla consectetur id omnis quibusdam. Magni ducimus ut vero et et. Ducimus porro quidem sit. Saepe explicabo laborum id a quis pariatur asperiores.','1'),
('377',9781629792033,'2017-07-08 11:37:41','77','3','Nam inventore labore accusamus eveniet ex voluptatem. Ut odit et veniam quia voluptate. Eos laboriosam culpa voluptatem omnis minus qui.','1'),
('378',9781629798301,'2013-02-26 00:52:17','78','5','Consectetur unde esse autem omnis sed ut voluptatibus. Odit iusto quia ab voluptatem. Id voluptatem et eos iure sit consequatur.','1'),
('379',9781629792668,'2019-07-28 18:27:45','79','3','Aut numquam sed et reiciendis adipisci voluptas qui. Distinctio et est et dicta rem.','1'),
('380',9781629797687,'2019-10-14 16:09:56','80','5','Dolorum delectus quia autem. Ea voluptates maiores qui. Cum aut ipsum ducimus qui dolores nostrum.','1'),
('381',9781620917640,'2011-02-21 17:12:49','81','3','Laboriosam eum rerum voluptates. Quis excepturi consequatur aut sed adipisci. Cumque aut aperiam dolores tenetur. Voluptas similique quis ea ipsam.','1'),
('382',9781426330179,'2011-05-30 16:05:04','82','5','Fugiat quis placeat et dolores voluptatem. Velit occaecati cum sit ratione. Mollitia officia ea reiciendis vel aliquid. Quaerat excepturi nostrum dolorem magni non nemo omnis.','1'),
('383',9781426332852,'2012-03-22 18:53:37','83','2','Soluta laboriosam maiores et ea. Vel odio error blanditiis quis.\nAutem voluptas harum nemo laboriosam quasi sed. Delectus et odio aut nobis reprehenderit excepturi adipisci quas.','1'),
('384',9781096123163,'2015-08-10 23:04:34','84','3','Qui nesciunt sit dolorem ipsa fugiat. Illum autem cupiditate aliquid et atque animi. Sed iure reiciendis consequatur ea occaecati. Non quisquam aspernatur quaerat ullam nobis.','1'),
('385',9781095698655,'2011-06-13 12:41:46','85','5','Voluptatem animi in cumque autem necessitatibus ipsa amet. Et eum praesentium consequatur error. Et reprehenderit sint facilis excepturi.','1'),
('386',9781095712245,'2017-06-24 19:43:32','86','5','Voluptates cum accusamus molestias dolorem. Ut aliquam quasi et nobis. Explicabo deleniti esse dolorem cumque.','1'),
('387',9781097142804,'2017-12-11 02:35:06','87','3','Reprehenderit unde sint laudantium eveniet eum dolores ducimus. Nisi aut architecto repellat est.','1'),
('388',9781641525312,'2015-02-10 09:15:28','88','4','Ex atque pariatur quas facilis. Alias voluptatem modi et deserunt ad consequatur. Quo libero dolores harum dolor ut.','1'),
('389',9780606144841,'2011-02-27 07:31:24','89','5','Harum quisquam sint voluptatibus fugiat et. Qui et alias qui hic. Minima ut est repudiandae id culpa minus.','1'),
('390',9780545132060,'2010-09-04 23:59:30','20','5','Harum provident explicabo accusamus asperiores ea ex quae. Laborum repudiandae et eligendi id. Eveniet expedita recusandae voluptas expedita. Molestias iure consectetur omnis fuga.','1'),
('391',9780810984226,'2010-09-27 06:12:09','21','5','Molestias omnis vel nemo omnis commodi et. Incidunt ad harum unde deserunt atque rerum. Aspernatur ducimus aut dolor tempore necessitatibus delectus sint. Odit et architecto eligendi fugit.','1'),
('392',9781603090858,'2016-10-22 14:14:53','22','2','Architecto sunt nam est qui ut. Voluptatem debitis error quos labore voluptatem blanditiis earum rerum. Consequatur sed facere sequi natus aliquam.','1'),
('393',9780375832291,'2012-08-31 05:18:29','23','4','At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.','1'),
('394',9780375832291,'2016-10-22 09:40:58','24','3','Illo consectetur et eos. Aperiam aut soluta optio omnis exercitationem. Voluptas atque dolores laborum assumenda rem.','1'),
('395',9781608862801,'2011-11-05 06:44:03','25','5','Provident ad consequatur quam consequatur ducimus cumque eum. Sunt eligendi amet quidem assumenda quae. Voluptas sint sed ut odio.','1'),
('396',9780606267380,'2018-03-30 01:45:24','26','3','In ut non saepe consequatur dolor nulla. Cumque qui et corrupti. Nesciunt est et commodi qui.','1'),
('397',9781596437135,'2017-07-28 00:52:08','27','2','Ad quisquam dolorem incidunt. Laboriosam et dolores magni quisquam omnis. In ut repellendus rerum placeat officia molestias. Deleniti cum iste dolores qui rem cum voluptatem.','1'),
('398',9781449472337,'2014-06-02 19:50:18','28','4','Est aspernatur voluptates autem. Eveniet soluta qui quibusdam asperiores. Ea nulla quos aperiam ratione sequi cumque ut.','1'),
('399',9780439846806,'2010-11-12 13:21:04','29','2','Sit ut maiores fugit accusantium. Nostrum eveniet est perspiciatis aut beatae. Saepe asperiores enim adipisci voluptas soluta. Vitae in alias est optio excepturi.','1'),
('400',9781423160342,'2015-08-31 05:38:41','100','3','Praesentium repellendus illum dolor sed ducimus. Modi itaque laudantium molestiae repellat delectus. Laboriosam earum est est occaecati cupiditate ut soluta.','1'),
('101',9781948040686,'2019-09-15 20:05:22','12','2','Quo numquam quia sed accusantium amet et ut. Earum provident id fugit nisi fugiat veniam natus et. Eos magnam id porro eos animi. Deserunt dolorum est nostrum quidem odit nobis.','1'),
('102',9782764323519,'2015-02-25 21:24:20','23','5','Eveniet adipisci aut eum quidem amet iure. Totam incidunt exercitationem eum et. Voluptatem qui rerum officiis asperiores voluptas velit eveniet. Est vitae pariatur quia est.','1'),
('103',9782764333655,'2011-10-31 23:01:03','3','5','Nam ea ipsa ex et accusamus dolores. Dolores eius nam quaerat enim necessitatibus et. Facere qui similique et incidunt eos magnam. Fugit id blanditiis ut ut.','1'),
('104',9782764336953,'2015-10-04 21:47:59','4','3','Veniam reiciendis et sed labore. Delectus optio ad alias esse in dolorem. Tenetur fugiat quaerat ut. Qui fugit sit quo omnis dolores.','1'),
('105',9781368026697,'2011-06-05 05:22:44','5','5','Excepturi sunt soluta labore est nihil. Corrupti blanditiis eaque autem est est veniam. Nesciunt possimus omnis quia vel et.','1'),
('106',9781484782002,'2018-12-19 10:33:33','6','4','Voluptate unde corrupti veritatis numquam ullam quis. Numquam sapiente consectetur et quidem. Dolor explicabo sit aut earum occaecati autem et placeat. Aliquam non quia rem cumque in alias.','1'),
('107',9781408855652,'2011-02-20 04:55:27','7','2','Dolorem maxime esse quas dignissimos consectetur illum dicta. Corrupti tempora molestiae velit quia ab. Dolorem architecto blanditiis quos non ipsa sed aut.','1'),
('108',9781408855669,'2011-06-06 23:43:32','8','3','Sequi debitis et ut expedita aliquid maxime dignissimos. Sed soluta recusandae cumque voluptate fugit. Omnis velit quasi ipsam optio impedit.','1'),
('109',9781408855676,'2019-12-17 01:21:19','19','2','Cum rerum assumenda aut sit ut. Unde blanditiis sint nihil voluptatem ipsa ut exercitationem enim. Libero ut facere molestias.','1'),
('110',9781408855683,'2014-06-14 10:20:38','12','5','Corporis esse voluptatem qui mollitia. Laudantium deleniti similique nihil quia voluptatum quia eveniet.','1'),
('111',9780062941008,'2019-04-07 20:42:35','10','4','Et et placeat sunt officiis ut quis. Quis cupiditate suscipit non nulla repellendus eaque voluptas. Architecto est quidem eum laboriosam numquam. Eos modi corporis et nihil et voluptas explicabo.','1'),
('112',9780889953673,'2010-01-13 22:30:01','13','2','Aut et ut earum repellat corrupti autem. Illo corrupti id eos omnis fuga necessitatibus aut. Modi ea ratione qui vero velit. Rerum omnis ratione aut enim.','1'),
('113',9781894778145,'2017-09-07 18:52:27','1','5','Suscipit et exercitationem dolor et a est. Est architecto sequi eos cum rerum corrupti.','1'),
('114',9781894778145,'2010-02-15 06:54:01','4','3','Animi voluptatem iure esse. Cum voluptatem veniam voluptatum harum. Molestiae nostrum quis recusandae aut fuga cum.','1'),
('115',9780888996596,'2016-09-12 23:59:10','5','4','Deserunt aliquam est doloribus animi rerum molestiae. Quam sed est vel illo aut aut. Veniam dolor ea quam deserunt quis. Id velit et dolore et.','1'),
('116',9781772600384,'2010-01-31 04:52:57','6','5','Totam temporibus error eos non placeat quod autem quia. Aut eveniet facere adipisci illum.','1'),
('117',9781459802483,'2016-12-25 22:11:23','7','5','Porro est dolore magnam quo ut qui. Vel repellendus ut hic vel molestias similique. Expedita vel ad sed laborum sint eos et consequatur. Id rerum quidem minima tenetur modi.','1'),
('118',9780062953452,'2016-01-06 18:31:33','8','3','Commodi quod quibusdam dolor aut ut labore. Voluptatem qui sed nulla unde est. Excepturi iusto corrupti numquam est autem voluptas explicabo. Voluptates similique ea rerum ut voluptatem quidem.','1'),
('119',9780062953414,'2019-07-15 22:09:28','8','2','Architecto sit voluptatem.','1'),
('120',9781554988334,'2013-09-23 10:24:11','61','2','Dolor consectetur voluptatem mollitia quidem voluptate illo quae. Sint voluptatibus ipsam illo possimus. Dolor enim et veniam sunt libero nihil ea facilis. Quibusdam non voluptatem minima sequi.','1'),
('121',9781554988334,'2010-11-03 02:16:53','62','5','Recusandae unde in dolorum officia. Non et est incidunt in eos esse. Fuga fuga ut ut nostrum soluta. Molestiae vel necessitatibus eius temporibus enim aut.','1'),
('122',9781620142639,'2013-09-19 12:44:25','63','5','Quos sit id ea cum. Commodi et provident maxime quibusdam. Et minus quam veniam dolor molestiae.','1'),
('123',9781620142639,'2011-05-19 19:36:15','3','4','Deserunt consequatur ipsa doloribus deserunt molestias maiores officiis. Rerum harum earum voluptatibus deserunt fugit. Consequatur qui ea qui placeat dolorem sed rerum tenetur.','1'),
('124',9780062498564,'2016-02-02 03:53:48','6','5','Ab rerum commodi cupiditate neque. Quasi facere quaerat facilis quae aut quasi. Ipsa excepturi inventore maiores quos. Laudantium id non est est.','1'),
('125',9781524769567,'2019-03-18 01:43:30','6','2','Animi aut iure voluptatibus explicabo. Voluptas nihil omnis beatae nostrum consectetur odio nihil. Modi est laborum iusto ut culpa. Et mollitia reprehenderit rerum.','1'),
('126',9781481400701,'2019-07-27 06:29:20','6','4','Deserunt ducimus dignissimos velit eum quasi. Omnis soluta ut impedit iste. Maiores voluptate iure quas molestiae. Quia ut iusto aut deserunt provident dicta laborum.','1'),
('127',9781481400701,'2014-12-11 16:36:49','6','5','Error iusto quia ducimus facilis. Dolorem animi ratione iure et distinctio et ratione et. Quis minus porro minima ut ipsa quaerat animi molestias. Dolorem quia laboriosam dignissimos aut culpa.','1'),
('128',9781743629000,'2019-03-11 07:47:51','2','5','Non suscipit voluptates aut quo dignissimos. Et eius sed consectetur quod voluptatem non vero. Illo et et nisi qui labore nihil.','1'),
('129',9781772780109,'2010-12-26 10:11:59','2','4','Eos aliquam maxime enim repudiandae unde at eum. Eius atque et odit aut praesentium quia. Minus numquam officia vel non voluptates ut beatae impedit.','1'),
('130',9780312625993,'2019-08-21 12:19:25','8','5','Vel velit quas quas dolore quam quasi reprehenderit. Non nulla praesentium cupiditate alias. Quam nisi non perferendis quasi cum. Ex ea animi est tenetur quo.','1'),
('131',9781492662143,'2016-01-10 09:14:43','81','3','Alias velit omnis natus recusandae molestiae eius sit. Quis doloribus repellendus eligendi voluptas sit sit excepturi. Aut dolorem dolorem ut modi doloribus ut harum. Eos enim sed nesciunt odio.','1'),
('132',9781492662143,'2011-12-14 18:56:30','82','5','Dicta laudantium excepturi reiciendis voluptatem rerum culpa aut. Non amet quaerat ea distinctio voluptatum. Id vel sunt nihil aut assumenda.','1'),
('133',9781492662143,'2019-03-12 13:06:02','83','3','Ut voluptas maxime in omnis explicabo magni. Ut ut est non voluptatibus id excepturi ut. Qui rerum praesentium sapiente ipsa illum. Dolorem et reiciendis totam reprehenderit rerum quaerat.','1'),
('134',9781467711944,'2015-01-23 01:56:42','84','4','Voluptatem ea velit laborum quidem aliquid vero magnam. Nostrum asperiores quam officiis totam ad aut consequatur.','1'),
('135',9781515782223,'2019-01-06 15:23:27','85','5','Vel id esse veniam voluptatibus. Doloribus qui est quis quod in aut dolores. Alias labore ipsa deleniti molestias rerum vero.','1'),
('136',9780399246531,'2012-01-26 10:07:37','86','5','Beatae unde nemo sit. Exercitationem modi quia tenetur ut iusto ullam. Qui id earum sunt veritatis maiores impedit ut.','1'),
('137',9780763693558,'2010-07-10 20:05:25','87','3','Similique delectus ipsam dolor quo id non inventore aspernatur. Ut recusandae reprehenderit cumque facere. Voluptatum voluptatum dolor eos rerum vitae vero reiciendis.','1'),
('138',9781426310492,'2019-08-04 10:45:24','88','4','Voluptatibus fugiat qui quaerat et. Voluptatem ea perspiciatis libero esse libero in. Earum officia inventore dolorum quis.','1'),
('139',9781580625579,'2018-12-27 05:30:33','89','5','Voluptatum qui ipsum veniam a rem officia. Sunt repudiandae quidem voluptatem aut sequi. Provident esse sint nihil et impedit.','1'),
('140',9780486468211,'2015-06-15 19:55:25','80','5','Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.','1'),
('141',9780486468211,'2012-03-01 13:48:55','81','2','Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.','1'),
('142',9781641520416,'2016-02-18 22:34:49','82','2','Doloribus explicabo eos repellat. Laboriosam deleniti ut dolorem illum. Laborum non omnis nihil.','1'),
('143',9780692848388,'2017-11-13 03:22:28','83','2','Dolores earum eos sed voluptatibus sit. Delectus culpa repellat porro magnam delectus minima assumenda. Velit in minus ad est omnis. Nobis numquam nulla quia.','1'),
('144',9780692848388,'2019-02-23 10:15:01','84','2','Voluptatem veritatis nobis omnis voluptatem. Esse assumenda quo sequi dignissimos omnis. Sequi minima rerum accusamus tempore ea quaerat.','1'),
('145',9780692848388,'2019-08-12 00:07:19','85','3','Aut ipsum sapiente quia sit. Eaque ut quis voluptas consequuntur odit quia. Excepturi impedit cupiditate rerum provident dolores aut.','1'),
('146',9780547557991,'2015-11-14 20:39:22','86','5','Sunt et est voluptas aspernatur. Error soluta amet inventore. Laudantium vitae in ut dolor corporis qui.','1'),
('147',9781426307041,'2019-11-30 20:13:38','87','5','Ullam dolores distinctio illum hic cupiditate quisquam. Optio velit in ullam voluptas illum sed a sed.','1'),
('148',9781426310140,'2016-04-07 04:14:34','88','4','Totam nulla est est accusantium assumenda accusantium doloremque. Voluptate id in voluptatem et enim fuga magni. Sed repellat nihil vero cumque suscipit consequatur.','1'),
('149',9781426307935,'2016-06-19 05:12:42','89','5','Nisi ipsa qui voluptatem eligendi. Enim deleniti magnam quaerat consequatur ipsum id. Iure dolorem sunt accusamus itaque qui. Voluptate accusantium architecto quam repellendus aut voluptate.','1'),
('150',9781426308468,'2011-12-24 10:51:42','70','5','Totam eveniet ut ut sunt id rerum dicta. Aut aut dolore animi.\nAspernatur est est et fugiat aut qui blanditiis. Soluta quia et possimus. Quasi autem quasi aut hic earum aut.','1'),
('151',9781426308468,'2017-05-28 15:50:09','71','2','Qui dolores animi odio rerum porro. Sapiente veniam architecto molestiae animi magni. Maxime quos odit dolor dolorem ut. A sed sit quis.','1'),
('152',9781732596344,'2017-01-23 04:27:29','72','3','Consequatur reprehenderit possimus consequatur nulla. Blanditiis deserunt dolorem ut voluptatem est qui. Quam provident doloribus quam et aliquid voluptas et. Excepturi numquam ipsum qui error.','1'),
('153',9780448405179,'2019-04-07 18:43:46','73','2','Cumque quia eligendi ipsum reprehenderit harum velit id hic. A corporis deserunt unde at. Impedit ipsum quae ea velit quia occaecati velit.','1'),
('154',9780448405179,'2010-05-07 15:24:16','74','4','Aut hic ex necessitatibus cupiditate saepe. Quasi sapiente illo illum est. Nemo est et tempora laudantium iste. Accusantium est est laboriosam repellat pariatur perferendis voluptatem.','1'),
('155',9781641520928,'2010-11-22 03:38:28','75','2','Laboriosam ut culpa dolorem sunt debitis. Voluptatem esse accusamus dolorem impedit. Corporis est rem temporibus odit sapiente facilis. Est labore eos possimus non.','1'),
('156',9781641520928,'2010-05-24 10:36:42','76','5','Dolorem sed et itaque ipsa eaque occaecati saepe. Earum et error totam quisquam nam aut voluptatibus. Perferendis vero eius reiciendis aut. Eos exercitationem magni occaecati ad.','1'),
('157',9781772261059,'2018-02-24 21:53:58','77','5','Rem aut et molestiae temporibus aut. A eos accusamus ut maxime quibusdam laborum. Nihil veritatis velit voluptatem.','1'),
('158',9781772261059,'2010-10-03 07:29:52','78','5','Vero ipsum quisquam enim et maiores voluptatem ut. Enim quibusdam error temporibus totam. Cum eos quia voluptas repudiandae est.','1'),
('159',9781097522132,'2012-02-17 03:56:19','79','5','Dolorum est voluptatibus modi quia quis dolores. Aut ab quis cumque ducimus quas ab iure ullam. Omnis id tempore quos nihil occaecati nihil aspernatur. Nihil qui eum fuga recusandae quia qui et.','1'),
('160',9781097522132,'2016-06-03 17:40:12','90','2','Explicabo ratione numquam corporis ut est temporibus. Aut possimus sit numquam aut. Iusto pariatur quaerat et consectetur consequatur.','1'),
('161',9781633226241,'2015-11-24 13:00:44','91','4','Non ut debitis occaecati et. Quae sit esse est ut quia.','1'),
('162',9781633226241,'2014-05-17 00:46:37','92','5','Saepe cupiditate eos ut nesciunt nihil porro minima. Id mollitia maxime dolorem et similique. Maxime numquam at nam animi qui ut.','1'),
('163',9780385320436,'2010-07-05 13:38:03','93','2','Ipsum qui assumenda rerum fugit voluptates temporibus. Eos voluptas ipsa tempore omnis modi eius. Perspiciatis voluptas nam laboriosam mollitia nesciunt ad.','1'),
('164',9780385320436,'2016-04-22 22:28:05','94','2','Illo voluptatem dolorem ea eum fugit dolorem est. Asperiores impedit optio quo sint ab maiores. Dolores qui explicabo soluta deserunt. Velit accusantium eum ratione porro sint quibusdam.','1'),
('165',9781465414182,'2014-11-20 07:26:22','95','5','Officiis quaerat sunt rerum dolore. Non suscipit alias similique. In hic velit quia est autem. Voluptate molestias facere ut provident.','1'),
('166',9781623368890,'2019-03-02 04:40:38','96','5','Et saepe asperiores vel voluptates sunt est sit. Quod et sit dolorem vitae atque. Temporibus maiores ullam ut qui occaecati quis est sint.','1'),
('167',9781775183310,'2016-04-08 09:49:47','97','2','Ut nemo numquam corporis esse sit repudiandae molestiae eum. Sed accusantium ut est ipsa autem voluptas doloremque deserunt. Velit vel autem est vitae quisquam.','1'),
('168',9781936140404,'2019-02-25 00:13:27','98','5','Harum reprehenderit et eos provident animi deserunt. Voluptas voluptatem quis repellat ut sed cum. Corporis nihil aperiam quas ab laborum.','1'),
('169',9781780552491,'2011-12-18 03:37:31','99','2','Et omnis id minima dolorem veniam explicabo eius cum. Earum deserunt suscipit voluptatem rerum. Debitis non architecto quia cupiditate. Repellat nihil aut qui ipsum voluptas quisquam.','1'),
('170',9781580626873,'2016-08-05 02:41:36','70','2','Exercitationem dolores similique aut dignissimos accusamus tempora iusto a. Corrupti rerum velit consequatur et.','1'),
('171',9781580626873,'2017-01-28 01:11:45','71','3','Voluptas debitis quia fugit non libero ut. Quibusdam ut sit ea consequatur natus. Fuga iure debitis amet quo exercitationem autem et. Ut inventore mollitia aut eum deserunt dolores.','1'),
('172',9781629794228,'2017-08-08 23:56:00','72','2','Laborum accusamus voluptatum architecto est. Iste cupiditate est sequi similique dolorum mollitia ratione. Sunt ut alias fuga et.','1'),
('173',9781629797694,'2010-07-10 15:33:49','73','2','In quidem numquam esse suscipit. Labore est ea veritatis pariatur. Velit cum excepturi delectus. Et veritatis quos vel et.','1'),
('174',9781629796987,'2019-03-08 02:05:15','74','4','Provident ut laboriosam vero harum cupiditate vel nostrum consequatur. Et enim error consectetur fugiat tempora voluptas. Sunt repudiandae cumque rerum maxime facilis.','1'),
('175',9781629794235,'2018-05-21 14:02:38','75','3','Quasi velit provident sunt cum doloribus consequatur. Et impedit harum omnis nesciunt cum nihil porro non.','1'),
('176',9781629794242,'2018-05-17 04:12:47','76','3','Nam nulla consectetur id omnis quibusdam. Magni ducimus ut vero et et. Ducimus porro quidem sit. Saepe explicabo laborum id a quis pariatur asperiores.','1'),
('177',9781629792033,'2017-07-08 11:37:41','77','3','Nam inventore labore accusamus eveniet ex voluptatem. Ut odit et veniam quia voluptate. Eos laboriosam culpa voluptatem omnis minus qui.','1'),
('178',9781629798301,'2013-02-26 00:52:17','78','5','Consectetur unde esse autem omnis sed ut voluptatibus. Odit iusto quia ab voluptatem. Id voluptatem et eos iure sit consequatur.','1'),
('179',9781629792668,'2019-07-28 18:27:45','79','3','Aut numquam sed et reiciendis adipisci voluptas qui. Distinctio et est et dicta rem.','1'),
('180',9781629797687,'2019-10-14 16:09:56','10','5','Dolorum delectus quia autem. Ea voluptates maiores qui. Cum aut ipsum ducimus qui dolores nostrum.','1'),
('181',9781620917640,'2011-02-21 17:12:49','11','3','Laboriosam eum rerum voluptates. Quis excepturi consequatur aut sed adipisci. Cumque aut aperiam dolores tenetur. Voluptas similique quis ea ipsam.','1'),
('182',9781426330179,'2011-05-30 16:05:04','12','5','Fugiat quis placeat et dolores voluptatem. Velit occaecati cum sit ratione. Mollitia officia ea reiciendis vel aliquid. Quaerat excepturi nostrum dolorem magni non nemo omnis.','1'),
('183',9781426332852,'2012-03-22 18:53:37','13','2','Soluta laboriosam maiores et ea. Vel odio error blanditiis quis.\nAutem voluptas harum nemo laboriosam quasi sed. Delectus et odio aut nobis reprehenderit excepturi adipisci quas.','1'),
('184',9781096123163,'2015-08-10 23:04:34','14','3','Qui nesciunt sit dolorem ipsa fugiat. Illum autem cupiditate aliquid et atque animi. Sed iure reiciendis consequatur ea occaecati. Non quisquam aspernatur quaerat ullam nobis.','1'),
('185',9781095698655,'2011-06-13 12:41:46','15','5','Voluptatem animi in cumque autem necessitatibus ipsa amet. Et eum praesentium consequatur error. Et reprehenderit sint facilis excepturi.','1'),
('186',9781095712245,'2017-06-24 19:43:32','16','5','Voluptates cum accusamus molestias dolorem. Ut aliquam quasi et nobis. Explicabo deleniti esse dolorem cumque.','1'),
('187',9781097142804,'2017-12-11 02:35:06','17','3','Reprehenderit unde sint laudantium eveniet eum dolores ducimus. Nisi aut architecto repellat est.','1'),
('188',9781641525312,'2015-02-10 09:15:28','18','4','Ex atque pariatur quas facilis. Alias voluptatem modi et deserunt ad consequatur. Quo libero dolores harum dolor ut.','1'),
('189',9780606144841,'2011-02-27 07:31:24','19','5','Harum quisquam sint voluptatibus fugiat et. Qui et alias qui hic. Minima ut est repudiandae id culpa minus.','1'),
('190',9780545132060,'2010-09-04 23:59:30','90','5','Harum provident explicabo accusamus asperiores ea ex quae. Laborum repudiandae et eligendi id. Eveniet expedita recusandae voluptas expedita. Molestias iure consectetur omnis fuga.','1'),
('191',9780810984226,'2010-09-27 06:12:09','91','5','Molestias omnis vel nemo omnis commodi et. Incidunt ad harum unde deserunt atque rerum. Aspernatur ducimus aut dolor tempore necessitatibus delectus sint. Odit et architecto eligendi fugit.','1'),
('192',9781603090858,'2016-10-22 14:14:53','92','2','Architecto sunt nam est qui ut. Voluptatem debitis error quos labore voluptatem blanditiis earum rerum. Consequatur sed facere sequi natus aliquam.','1'),
('193',9780375832291,'2012-08-31 05:18:29','93','4','At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.','1'),
('194',9780375832291,'2016-10-22 09:40:58','94','3','Illo consectetur et eos. Aperiam aut soluta optio omnis exercitationem. Voluptas atque dolores laborum assumenda rem.','1'),
('195',9781608862801,'2011-11-05 06:44:03','95','5','Provident ad consequatur quam consequatur ducimus cumque eum. Sunt eligendi amet quidem assumenda quae. Voluptas sint sed ut odio.','1'),
('196',9780606267380,'2018-03-30 01:45:24','96','3','In ut non saepe consequatur dolor nulla. Cumque qui et corrupti. Nesciunt est et commodi qui.','1'),
('197',9781596437135,'2017-07-28 00:52:08','97','2','Ad quisquam dolorem incidunt. Laboriosam et dolores magni quisquam omnis. In ut repellendus rerum placeat officia molestias. Deleniti cum iste dolores qui rem cum voluptatem.','1'),
('198',9781449472337,'2014-06-02 19:50:18','98','4','Est aspernatur voluptates autem. Eveniet soluta qui quibusdam asperiores. Ea nulla quos aperiam ratione sequi cumque ut.','1'),
('199',9780439846806,'2010-11-12 13:21:04','99','2','Sit ut maiores fugit accusantium. Nostrum eveniet est perspiciatis aut beatae. Saepe asperiores enim adipisci voluptas soluta. Vitae in alias est optio excepturi.','1'),
('200',9781408855690,'2015-08-31 05:38:41','12','5','My kids LOVED it!','1'),
('201',9781408855706,'2019-09-15 20:05:22','12','5','My kids LOVED it!','1'),
('202',9781408855713,'2015-02-25 21:24:20','2','5','My kids LOVED it!','1'),
('203',9781408855713,'2011-10-31 23:01:03','3','5','My kids LOVED it!','1'),
('204',9781408855713,'2015-10-04 21:47:59','4','5','My kids LOVED it!','1'),
('205',9780062941008,'2011-06-05 05:22:44','5','5','My kids LOVED it!','1'),
('206',9780062941008,'2018-12-19 10:33:33','6','5','My kids LOVED it!','1'),
('207',9780062941008,'2011-02-20 04:55:27','5','5','My kids LOVED it!','1'),
('208',9780062941008,'2011-06-06 23:43:32','18','5','My kids LOVED it!','1'),
('209',9780062941008,'2019-12-17 01:21:19','29','5','My kids LOVED it!','1'),
('210',9780062941008,'2014-06-14 10:20:38','1','5','My kids LOVED it!','1'),
('211',9780062941008,'2019-04-07 20:42:35','1','5','My kids LOVED it!','1'),
('212',9780889953673,'2010-01-13 22:30:01','2','5','My kids LOVED it!','1'),
('213',9781894778145,'2017-09-07 18:52:27','3','5','My kids LOVED it!','1'),
('214',9781894778145,'2010-02-15 06:54:01','4','5','My kids LOVED it!','1'),
('215',9780888996596,'2016-09-12 23:59:10','5','5','My kids LOVED it!','1'),
('216',9781772600384,'2010-01-31 04:52:57','6','5','My kids LOVED it!','1'),
('217',9781459802483,'2016-12-25 22:11:23','7','5','My kids LOVED it!','1'),
('218',9780062953452,'2016-01-06 18:31:33','8','5','My kids LOVED it!','1'),
('219',9780062953414,'2019-07-15 22:09:28','9','5','My kids LOVED it!','1'),
('220',9781554988334,'2013-09-23 10:24:11','80','5','My kids LOVED it!','1'),
('221',9781554988334,'2010-11-03 02:16:53','81','5','My kids LOVED it!','1'),
('222',9781620142639,'2013-09-19 12:44:25','82','5','My kids LOVED it!','1'),
('223',9781620142639,'2011-05-19 19:36:15','83','5','My kids LOVED it!','1'),
('224',9780062498564,'2016-02-02 03:53:48','84','5','My kids LOVED it!','1'),
('225',9781524769567,'2019-03-18 01:43:30','85','5','My kids LOVED it!','1'),
('226',9781481400701,'2019-07-27 06:29:20','86','5','My kids LOVED it!','1'),
('227',9781481400701,'2014-12-11 16:36:49','87','5','My kids LOVED it!','1'),
('228',9781743629000,'2019-03-11 07:47:51','88','5','My kids LOVED it!','1'),
('229',9781772780109,'2010-12-26 10:11:59','89','5','My kids LOVED it!','1'),
('230',9780312625993,'2019-08-21 12:19:25','80','5','My kids LOVED it!','1'),
('231',9781492662143,'2016-01-10 09:14:43','81','5','My kids LOVED it!','1'),
('232',9781492662143,'2011-12-14 18:56:30','82','5','My kids LOVED it!','1'),
('233',9781492662143,'2019-03-12 13:06:02','83','5','My kids LOVED it!','1'),
('234',9781467711944,'2015-01-23 01:56:42','84','5','My kids LOVED it!','1'),
('235',9781515782223,'2019-01-06 15:23:27','85','5','My kids LOVED it!','1'),
('236',9780399246531,'2012-01-26 10:07:37','86','5','My kids LOVED it!','1'),
('237',9780763693558,'2010-07-10 20:05:25','87','5','My kids LOVED it!','1'),
('238',9781426310492,'2019-08-04 10:45:24','88','5','My kids LOVED it!','1'),
('239',9781580625579,'2018-12-27 05:30:33','89','5','My kids LOVED it!','1'),
('240',9780486468211,'2015-06-15 19:55:25','30','5','My kids LOVED it!','1'),
('241',9780486468211,'2012-03-01 13:48:55','31','5','My kids LOVED it!','1'),
('242',9781641520416,'2016-02-18 22:34:49','32','5','My kids LOVED it!','1'),
('243',9780692848388,'2017-11-13 03:22:28','33','5','My kids LOVED it!','1'),
('244',9780692848388,'2019-02-23 10:15:01','34','5','My kids LOVED it!','1'),
('245',9780692848388,'2019-08-12 00:07:19','35','5','My kids LOVED it!','1'),
('246',9780547557991,'2015-11-14 20:39:22','36','5','My kids LOVED it!','1'),
('247',9781426307041,'2019-11-30 20:13:38','37','5','My kids LOVED it!','1'),
('248',9781426310140,'2016-04-07 04:14:34','38','5','My kids LOVED it!','1'),
('249',9781426307935,'2016-06-19 05:12:42','39','5','My kids LOVED it!','1'),
('250',9781426308468,'2011-12-24 10:51:42','80','5','My kids LOVED it!','1'),
('251',9781426308468,'2017-05-28 15:50:09','81','5','My kids LOVED it!','1'),
('252',9781732596344,'2017-01-23 04:27:29','82','5','My kids LOVED it!','1'),
('253',9780448405179,'2019-04-07 18:43:46','83','5','My kids LOVED it!','1'),
('254',9780448405179,'2010-05-07 15:24:16','84','5','My kids LOVED it!','1'),
('255',9781641520928,'2010-11-22 03:38:28','85','5','My kids LOVED it!','1'),
('256',9781641520928,'2010-05-24 10:36:42','86','5','My kids LOVED it!','1'),
('257',9781772261059,'2018-02-24 21:53:58','87','5','My kids LOVED it!','1'),
('258',9781772261059,'2010-10-03 07:29:52','88','5','My kids LOVED it!','1'),
('259',9781097522132,'2012-02-17 03:56:19','89','5','My kids LOVED it!','1'),
('260',9781097522132,'2016-06-03 17:40:12','50','5','My kids LOVED it!','1'),
('261',9781633226241,'2015-11-24 13:00:44','51','5','My kids LOVED it!','1'),
('262',9781633226241,'2014-05-17 00:46:37','52','5','My kids LOVED it!','1'),
('263',9780385320436,'2010-07-05 13:38:03','53','5','My kids LOVED it!','1'),
('264',9780385320436,'2016-04-22 22:28:05','54','5','My kids LOVED it!','1'),
('265',9781465414182,'2014-11-20 07:26:22','55','5','My kids LOVED it!','1'),
('266',9781623368890,'2019-03-02 04:40:38','56','5','My kids LOVED it!','1'),
('267',9781775183310,'2016-04-08 09:49:47','57','5','My kids LOVED it!','1'),
('268',9781936140404,'2019-02-25 00:13:27','58','5','My kids LOVED it!','1'),
('269',9781780552491,'2011-12-18 03:37:31','59','5','My kids LOVED it!','1'),
('270',9781580626873,'2016-08-05 02:41:36','50','5','My kids LOVED it!','1'),
('271',9781580626873,'2017-01-28 01:11:45','51','5','My kids LOVED it!','1'),
('272',9781629794228,'2017-08-08 23:56:00','52','5','My kids LOVED it!','1'),
('273',9781629797694,'2010-07-10 15:33:49','53','5','My kids LOVED it!','1'),
('274',9781629796987,'2019-03-08 02:05:15','54','5','My kids LOVED it!','1'),
('275',9781629794235,'2018-05-21 14:02:38','55','5','My kids LOVED it!','1'),
('276',9781629794242,'2018-05-17 04:12:47','56','5','My kids LOVED it!','1'),
('277',9781629792033,'2017-07-08 11:37:41','57','5','My kids LOVED it!','1'),
('278',9781629798301,'2013-02-26 00:52:17','58','5','My kids LOVED it!','1'),
('279',9781629792668,'2019-07-28 18:27:45','59','5','My kids LOVED it!','1'),
('280',9781629797687,'2019-10-14 16:09:56','30','5','My kids LOVED it!','1'),
('281',9781620917640,'2011-02-21 17:12:49','31','5','My kids LOVED it!','1'),
('282',9781426330179,'2011-05-30 16:05:04','32','5','My kids LOVED it!','1'),
('283',9781426332852,'2012-03-22 18:53:37','33','5','My kids LOVED it!','1'),
('284',9781096123163,'2015-08-10 23:04:34','34','5','My kids LOVED it!','1'),
('285',9781095698655,'2011-06-13 12:41:46','35','5','My kids LOVED it!','1'),
('286',9781095712245,'2017-06-24 19:43:32','36','5','My kids LOVED it!','1'),
('287',9781097142804,'2017-12-11 02:35:06','37','5','My kids LOVED it!','1'),
('288',9781641525312,'2015-02-10 09:15:28','38','5','My kids LOVED it!','1'),
('289',9780606144841,'2011-02-27 07:31:24','39','5','My kids LOVED it!','1'),
('290',9780545132060,'2010-09-04 23:59:30','80','5','My kids LOVED it!','1'),
('291',9780810984226,'2010-09-27 06:12:09','81','5','My kids LOVED it!','1'),
('292',9781603090858,'2016-10-22 14:14:53','82','5','My kids LOVED it!','1'),
('293',9780375832291,'2012-08-31 05:18:29','93','5','My kids LOVED it!','1'),
('294',9780375832291,'2016-10-22 09:40:58','84','5','My kids LOVED it!','1'),
('295',9781608862801,'2011-11-05 06:44:03','85','5','My kids LOVED it!','1'),
('296',9780606267380,'2018-03-30 01:45:24','86','5','My kids LOVED it!','1'),
('297',9781596437135,'2017-07-28 00:52:08','87','5','My kids LOVED it!','1'),
('298',9781449472337,'2014-06-02 19:50:18','88','5','My kids LOVED it!','1'),
('299',9780439846806,'2010-11-12 13:21:04','89','5','My kids LOVED it!','1'),
('300',9780062941008,'2015-08-31 05:38:41','101','5','My kids LOVED it!','1');

-- Banners
INSERT INTO `banners` (ad_id, url, active) VALUES 
('1','http://www.neopets.com/', 1), 
('2','https://www.omfgdogs.com/', 1),
('3','https://findtheinvisiblecow.com/', 0),
('4','http://burymewithmymoney.com/', 0);

-- Surveys
INSERT INTO `surveys` (question, answer_1, answer_2, answer_3, answer_4, vote_1, vote_2, vote_3, vote_4) VALUES 
('Do you like JSF?','Yes','NO', 'Now that I am quanrantined and have time to decode those mysterious stack traces, I do not mind it.', NULL,'8','20','12','0'),
('Toilet paper, over or under?','Over','Under','Who cares? #lota4life',NULL,'2','20','30','0'),
('Lets talk pets!','I am a dog lover.','I am a cat lover.','I am a burrito cat lover.','I can not care less for pets.','10','3','5','7'),
('Describe your mood today','Sunshine','Cloudy','Rainy','Rainbow','0','0','0','0'),
('Pineapples on a pizza...Yay or Nay?','Hell ya!','No! Criminal!!!',NULL,NULL,'20','20','0','0'),
('Who would you rather fight?','10 duck-sized horses','1 horse-sized duck',NULL,NULL,'10','3','0','0'),
('Aut ad itaque vero rerum sapiente cupiditate.','Asperiores architecto et doloremque tempora a.','Occaecati et aliquid consectetur ea aut totam dolorem sed.','Dolores quia exercitationem ullam eius.','Alias laborum aut blanditiis suscipit dolores.','8','2','1','1'),
('Corporis dolores consequuntur rerum perspiciatis odio optio.','Laborum dolorum ipsum quibusdam in tenetur ex.','Sed fugit blanditiis vel quis qui.','Reiciendis dolore qui voluptas ab.','0','0','0','0', '0'),
('Corrupti provident aut est illo.','Non officia velit tempore vero.','Nesciunt reiciendis rerum quidem quasi debitis provident mollitia.','Recusandae est quaerat qui unde molestias explicabo rerum dolor.','Nobis voluptates mollitia quibusdam quia qui tempora omnis.','8','2','1','1'),
('Ea et similique tempora quas assumenda.','Inventore quidem delectus est et doloremque.','Fugiat quo est aliquam atque ex nihil quo consequatur.','Ut aut sit officiis mollitia est quo voluptates quia.','Iste soluta iure in error vel iure.','0','0','0','0'),
('Similique ad tempore temporibus quo autem minus velit ipsam.','Et iste officia sit ratione adipisci.','Amet et omnis minima qui.', NULL, NULL, '9','3','0','0'),
('Soluta ut omnis odio porro.','Hic et aliquam voluptate fugit.','Reprehenderit est voluptatem qui omnis quis unde.',NULL,NULL,'12','9', '0', '0'),
('Totam laboriosam repellat hic dolor laborum.','Amet et omnis minima qui.','Qui maiores maxime numquam reprehenderit.','Quisquam ratione provident tempore.','Dolores tenetur sit quod ex dolorem.','8','2','1','1'),
('Ullam aut eligendi ut ipsam.','Veritatis voluptatum optio excepturi sequi dolores nisi.','Eius id et sit et et optio.','Dolorem totam est omnis ex maiores omnis ipsa.','Doloribus et laborum libero aliquid reprehenderit ratione assumenda.','0','0','0','0'),
('Ullam est fugit ut non iusto porro.','Ea adipisci labore et aut id.','Eligendi culpa voluptatem alias.','Eius qui et dignissimos molestiae.','Molestiae sed in quod aspernatur.','8','2','1','1'),
('Ullam fugiat fuga dolore minima necessitatibus nisi.','Tempora molestiae blanditiis maiores debitis sed.','Vitae provident molestiae est iure porro provident officia.','Nostrum aut aut reprehenderit sit fugit.','Culpa sed expedita voluptatem est et sint.','8','2','1','1'),
('Vel esse numquam necessitatibus distinctio deserunt.','Corrupti repudiandae quidem qui enim.','Eligendi nihil enim blanditiis totam.',NULL, NULL,'10','20','0','0'),
('Vel nihil id vel autem vel.','Et iusto hic illo doloremque exercitationem quia.','Delectus in qui eaque architecto autem non.',NULL,NULL,'3','4','0','0'),
('Velit sit expedita ut et non quia et.','Inventore nihil amet nihil quae.','Placeat qui deleniti provident ex tempore voluptas qui.','Eligendi nihil enim blanditiis totam.','Aut itaque et veritatis iusto provident.','0','0','0','0'),
('Veritatis perferendis laboriosam voluptatum ut.','Quo dolorem sapiente qui nobis rerum vero perspiciatis.','Occaecati ipsam consequatur qui.','Deserunt eum odio exercitationem ratione.','Quia et iure voluptas.','8','2','1','1'),
('Voluptas eius consectetur atque accusantium quam dolorem.','Repellendus nam qui omnis nam officia dolorum suscipit non.','Exercitationem nostrum aut non quis.',NULL, NULL,'1','1','0','0'),
('Voluptatem dolorem deleniti provident facere.','Nulla quo labore dolores et aliquam quam.','Placeat accusamus facilis fugit ad ratione.',NULL, NULL,'5','3','0','0'); 

-- Make surveys active
UPDATE surveys SET active = 1 WHERE survey_id = 1;
UPDATE surveys SET active = 1 WHERE survey_id = 2;
UPDATE surveys SET active = 1 WHERE survey_id = 3;
UPDATE surveys SET active = 1 WHERE survey_id = 4;
UPDATE surveys SET active = 1 WHERE survey_id = 5;
UPDATE surveys SET active = 1 WHERE survey_id = 6;

-- Bookformat table
INSERT INTO bookformat(isbn,format_id) VALUES
 (9781095698655,1)
,(9781095712245,2)
,(9781096123163,3)
,(9781097142804,4)
,(9781097522132,5)
,(9780889953673,6)
,(9781894778145,7)
,(9780888996596,8)
,(9781554988334,9)
,(9781772600384,10)
,(9781459802483,11)
,(9781620142639,12)
,(9780062498564,13)
,(9781524769567,14)
,(9781481400701,15)
,(9781743629000,16)
,(9781772780109,17)
,(9780312625993,18)
,(9781492662143,19)
,(9781467711944,20)
,(9781515782223,21)
,(9780399246531,22)
,(9780763693558,23)
,(9781426307041,24)
,(9781426307935,25)
,(9781426308468,26)
,(9781426310140,27)
,(9781426310492,28)
,(9781426330179,29)
,(9781426332852,1)
,(9781580625579,2)
,(9781580626873,3)
,(9780486468211,4)
,(9781641520416,5)
,(9781641520928,6)
,(9780692848388,7)
,(9780547557991,8)
,(9781732596344,9)
,(9780448405179,10)
,(9781772261059,11)
,(9780062941008,12)
,(9780062953414,13)
,(9780062953452,14)
,(9781633226241,15)
,(9780385320436,16)
,(9781465414182,17)
,(9781623368890,18)
,(9781775183310,19)
,(9781936140404,20)
,(9781780552491,21)
,(9781620917640,22)
,(9781629792033,23)
,(9781629792668,24)
,(9781629794228,25)
,(9781629794235,26)
,(9781629794242,27)
,(9781629796987,28)
,(9781629797687,29)
,(9781629797694,1)
,(9781629798301,2)
,(9781641525312,3)
,(9780606144841,4)
,(9781596437135,5)
,(9781626721067,6)
,(9780439846806,7)
,(9780545132060,8)
,(9780606267380,9)
,(9780810984226,10)
,(9781603090858,11)
,(9780375832291,12)
,(9781608862801,13)
,(9781608868018,14)
,(9781608869008,15)
,(9781449472337,16)
,(9781449483500,17)
,(9781524852269,18)
,(9780439706407,19)
,(9781419712173,20)
,(9780385265201,21)
,(9781608869930,22)
,(9781401273941,23)
,(9781368006187,24)
,(9781368007610,25)
,(9781368023481,26)
,(9781368043632,27)
,(9781368051828,28)
,(9781368053280,29)
,(9781423160342,1)
,(9781948040686,2)
,(9782764323519,3)
,(9782764333655,4)
,(9782764336953,5)
,(9781368026697,6)
,(9781484782002,7)
,(9781408855652,8)
,(9781408855669,9)
,(9781408855676,10)
,(9781408855683,11)
,(9781408855690,12)
,(9781408855706,13)
,(9781408855713,14)
,(9781095698655,15)
,(9781095712245,16)
,(9781096123163,17)
,(9781097142804,18)
,(9781097522132,19)
,(9780889953673,20)
,(9781894778145,21)
,(9780888996596,22)
,(9781554988334,23)
,(9781772600384,24)
,(9781459802483,25)
,(9781620142639,26)
,(9780062498564,27)
,(9781524769567,28)
,(9781481400701,29)
,(9781743629000,1)
,(9781772780109,2)
,(9780312625993,3)
,(9781492662143,4)
,(9781467711944,5)
,(9781515782223,6)
,(9780399246531,7)
,(9780763693558,8)
,(9781426307041,9)
,(9781426307935,10)
,(9781426308468,11)
,(9781426310140,12)
,(9781426310492,13)
,(9781426330179,14)
,(9781426332852,15)
,(9781580625579,16)
,(9781580626873,17)
,(9780486468211,18)
,(9781641520416,19)
,(9781641520928,20)
,(9780692848388,21)
,(9780547557991,22)
,(9781732596344,23)
,(9780448405179,24)
,(9781772261059,25)
,(9780062941008,26)
,(9780062953414,27)
,(9780062953452,28)
,(9781633226241,29)
,(9780385320436,1)
,(9781465414182,2)
,(9781623368890,3)
,(9781775183310,4)
,(9781936140404,5)
,(9781780552491,6)
,(9781620917640,7)
,(9781629792033,8)
,(9781629792668,9)
,(9781629794228,10)
,(9781629794235,11)
,(9781629794242,12)
,(9781629796987,13)
,(9781629797687,14)
,(9781629797694,15)
,(9781629798301,16)
,(9781641525312,17)
,(9780606144841,18)
,(9781596437135,19)
,(9781626721067,20)
,(9780439846806,21)
,(9780545132060,22)
,(9780606267380,23)
,(9780810984226,24)
,(9781603090858,25)
,(9780375832291,26)
,(9781608862801,27)
,(9781608868018,28)
,(9781608869008,29)
,(9781449472337,1)
,(9781449483500,2)
,(9781524852269,3)
,(9780439706407,4)
,(9781419712173,5)
,(9780385265201,6)
,(9781608869930,7)
,(9781401273941,8)
,(9781368006187,9)
,(9781368007610,10)
,(9781368023481,11)
,(9781368043632,12)
,(9781368051828,13)
,(9781368053280,14)
,(9781423160342,15)
,(9781948040686,16)
,(9782764323519,17)
,(9782764333655,18)
,(9782764336953,19)
,(9781368026697,20)
,(9781484782002,21)
,(9781408855652,22)
,(9781408855669,23)
,(9781408855676,24)
,(9781408855683,25)
,(9781408855690,26)
,(9781408855706,27)
,(9781408855713,28)
,(9781095698655,29)
,(9781095712245,1)
,(9781096123163,2)
,(9781097142804,3)
,(9781097522132,4)
,(9780889953673,5)
,(9781894778145,6)
,(9780888996596,7)
,(9781554988334,8)
,(9781772600384,9)
,(9781459802483,10)
,(9781620142639,11)
,(9780062498564,12)
,(9781524769567,13)
,(9781481400701,14)
,(9781743629000,15)
,(9781772780109,16)
,(9780312625993,17)
,(9781492662143,18)
,(9781467711944,19)
,(9781515782223,20)
,(9780399246531,21)
,(9780763693558,22)
,(9781426307041,23)
,(9781426307935,24)
,(9781426308468,25)
,(9781426310140,26)
,(9781426310492,27)
,(9781426330179,28)
,(9781426332852,29)
,(9781580625579,1)
,(9781580626873,2)
,(9780486468211,3)
,(9781641520416,4)
,(9781641520928,5)
,(9780692848388,6)
,(9780547557991,7)
,(9781732596344,8)
,(9780448405179,9)
,(9781772261059,10)
,(9780062941008,11)
,(9780062953414,12)
,(9780062953452,13)
,(9781633226241,14)
,(9780385320436,15)
,(9781465414182,16)
,(9781623368890,17)
,(9781775183310,18)
,(9781936140404,19)
,(9781780552491,20)
,(9781620917640,21)
,(9781629792033,22)
,(9781629792668,23)
,(9781629794228,24)
,(9781629794235,25)
,(9781629794242,26)
,(9781629796987,27)
,(9781629797687,28)
,(9781629797694,29)
,(9781629798301,1)
,(9781641525312,2)
,(9780606144841,3)
,(9781596437135,4)
,(9781626721067,5)
,(9780439846806,6)
,(9780545132060,7)
,(9780606267380,8)
,(9780810984226,9)
,(9781603090858,10)
,(9780375832291,11)
,(9781608862801,12)
,(9781608868018,13)
,(9781608869008,14)
,(9781449472337,15)
,(9781449483500,16)
,(9781524852269,17)
,(9780439706407,18)
,(9781419712173,19)
,(9780385265201,20)
,(9781608869930,21)
,(9781401273941,22)
,(9781368006187,23)
,(9781368007610,24)
,(9781368023481,25)
,(9781368043632,26)
,(9781368051828,27)
,(9781368053280,28)
,(9781423160342,29)
,(9781948040686,1)
,(9782764323519,2)
,(9782764333655,3)
,(9782764336953,4)
,(9781368026697,5)
,(9781484782002,6)
,(9781408855652,7)
,(9781408855669,8)
,(9781408855676,9)
,(9781408855683,10)
,(9781408855690,11)
,(9781408855706,12)
,(9781408855713,13)
,(9781095698655,14)
,(9781095712245,15)
,(9781096123163,16)
,(9781097142804,17)
,(9781097522132,18)
,(9780889953673,19)
,(9781894778145,20)
,(9780888996596,21)
,(9781554988334,22)
,(9781772600384,23)
,(9781459802483,24)
,(9781620142639,25)
,(9780062498564,26)
,(9781524769567,27)
,(9781481400701,28)
,(9781743629000,29)
,(9781772780109,1)
,(9780312625993,2)
,(9781492662143,3)
,(9781467711944,4)
,(9781515782223,5)
,(9780399246531,6)
,(9780763693558,7)
,(9781426307041,8)
,(9781426307935,9)
,(9781426308468,10)
,(9781426310140,11)
,(9781426310492,12)
,(9781426330179,13)
,(9781426332852,14)
,(9781580625579,15)
,(9781580626873,16)
,(9780486468211,17)
,(9781641520416,18)
,(9781641520928,19)
,(9780692848388,20)
,(9780547557991,21)
,(9781732596344,22)
,(9780448405179,23)
,(9781772261059,24)
,(9780062941008,25)
,(9780062953414,26)
,(9780062953452,27)
,(9781633226241,28)
,(9780385320436,29)
,(9781465414182,1)
,(9781623368890,2)
,(9781775183310,3)
,(9781936140404,4)
,(9781780552491,5)
,(9781620917640,6)
,(9781629792033,7)
,(9781629792668,8)
,(9781629794228,9)
,(9781629794235,10)
,(9781629794242,11)
,(9781629796987,12)
,(9781629797687,13)
,(9781629797694,14)
,(9781629798301,15)
,(9781641525312,16)
,(9780606144841,17)
,(9781596437135,18)
,(9781626721067,19)
,(9780439846806,20)
,(9780545132060,21)
,(9780606267380,22)
,(9780810984226,23)
,(9781603090858,24)
,(9780375832291,25)
,(9781608862801,26)
,(9781608868018,27)
,(9781608869008,28)
,(9781449472337,29)
,(9781449483500,1)
,(9781524852269,2)
,(9780439706407,3)
,(9781419712173,4)
,(9780385265201,5)
,(9781608869930,6)
,(9781401273941,7)
,(9781368006187,8)
,(9781368007610,9)
,(9781368023481,10)
,(9781368043632,11)
,(9781368051828,12)
,(9781368053280,13)
,(9781423160342,14)
,(9781948040686,15)
,(9782764323519,16)
,(9782764333655,17)
,(9782764336953,18)
,(9781368026697,19)
,(9781484782002,20)
,(9781408855652,21)
,(9781408855669,22)
,(9781408855676,23)
,(9781408855683,24)
,(9781408855690,25)
,(9781408855706,26)
,(9781408855713,27)
,(9781095698655,28)
,(9781095712245,29)
,(9781096123163,1)
,(9781097142804,2)
,(9781097522132,3)
,(9780889953673,4)
,(9781894778145,5)
,(9780888996596,6)
,(9781554988334,7)
,(9781772600384,8)
,(9781459802483,9)
,(9781620142639,10)
,(9780062498564,11)
,(9781524769567,12)
,(9781481400701,13)
,(9781743629000,14)
,(9781772780109,15)
,(9780312625993,16)
,(9781492662143,17)
,(9781467711944,18)
,(9781515782223,19)
,(9780399246531,20)
,(9780763693558,21)
,(9781426307041,22)
,(9781426307935,23)
,(9781426308468,24)
,(9781426310140,25)
,(9781426310492,26)
,(9781426330179,27)
,(9781426332852,28)
,(9781580625579,29)
,(9781580626873,25)
,(9780486468211,1)
,(9781641520416,2)
,(9781641520928,3)
,(9780692848388,4)
,(9780547557991,5)
,(9781732596344,6)
,(9780448405179,7)
,(9781772261059,8)
,(9780062941008,9)
,(9780062953414,10)
,(9780062953452,11)
,(9781633226241,12)
,(9780385320436,13)
,(9781465414182,14)
,(9781623368890,15)
,(9781775183310,16)
,(9781936140404,17)
,(9781780552491,18)
,(9781620917640,19)
,(9781629792033,20)
,(9781629792668,21)
,(9781629794228,22)
,(9781629794235,23)
,(9781629794242,24)
,(9781629796987,25)
,(9781629797687,26)
,(9781629797694,27)
,(9781629798301,28)
,(9781641525312,29)
,(9780606144841,1)
,(9781596437135,2)
,(9781626721067,3)
,(9780439846806,4)
,(9780545132060,5)
,(9780606267380,6)
,(9780810984226,7)
,(9781603090858,8)
,(9780375832291,9)
,(9781608862801,10)
,(9781608868018,11)
,(9781608869008,12)
,(9781449472337,13)
,(9781449483500,14)
,(9781524852269,15)
,(9780439706407,16)
,(9781419712173,17)
,(9780385265201,18)
,(9781608869930,19)
,(9781401273941,20)
,(9781368006187,21)
,(9781368007610,22)
,(9781368023481,23)
,(9781368043632,24)
,(9781368051828,25)
,(9781368053280,26)
,(9781423160342,27)
,(9781948040686,28)
,(9782764323519,29)
,(9782764333655,1)
,(9782764336953,2)
,(9781368026697,3)
,(9781484782002,4)
,(9781408855652,5)
,(9781408855669,6)
,(9781408855676,7)
,(9781408855683,8)
,(9781408855690,9)
,(9781408855706,10)
,(9781408855713,11)
,(9781095698655,12)
,(9781095712245,13)
,(9781096123163,14)
,(9781097142804,15)
,(9781097522132,16)
,(9780889953673,17)
,(9781894778145,18)
,(9780888996596,19)
,(9781554988334,20)
,(9781772600384,21)
,(9781459802483,22)
,(9781620142639,23)
,(9780062498564,24)
,(9781524769567,25)
,(9781481400701,26)
,(9781743629000,27)
,(9781772780109,28)
,(9780312625993,29)
,(9781492662143,1)
,(9781467711944,2)
,(9781515782223,3)
,(9780399246531,4)
,(9780763693558,5)
,(9781426307041,6)
,(9781426307935,7)
,(9781426308468,8)
,(9781426310140,9)
,(9781426310492,10)
,(9781426330179,11)
,(9781426332852,12)
,(9781580625579,13)
,(9781580626873,14)
,(9780486468211,15)
,(9781641520416,16)
,(9781641520928,17)
,(9780692848388,18)
,(9780547557991,19)
,(9781732596344,20)
,(9780448405179,21)
,(9781772261059,22)
,(9780062941008,23)
,(9780062953414,24)
,(9780062953452,25)
,(9781633226241,26)
,(9780385320436,27)
,(9781465414182,28)
,(9781623368890,29)
,(9781775183310,1)
,(9781936140404,2)
,(9781780552491,3)
,(9781620917640,4)
,(9781629792033,5)
,(9781629792668,6)
,(9781629794228,7)
,(9781629794235,8)
,(9781629794242,9)
,(9781629796987,10)
,(9781629797687,11)
,(9781629797694,12)
,(9781629798301,13)
,(9781641525312,14)
,(9780606144841,15)
,(9781596437135,16)
,(9781626721067,17)
,(9780439846806,18)
,(9780545132060,19)
,(9780606267380,20)
,(9780810984226,21)
,(9781603090858,22)
,(9780375832291,23)
,(9781608862801,24)
,(9781608868018,25)
,(9781608869008,26)
,(9781449472337,27)
,(9781449483500,28)
,(9781524852269,29)
,(9780439706407,1)
,(9781419712173,2)
,(9780385265201,3)
,(9781608869930,4)
,(9781401273941,5)
,(9781368006187,6)
,(9781368007610,7)
,(9781368023481,8)
,(9781368043632,9)
,(9781368051828,10)
,(9781368053280,11)
,(9781423160342,12)
,(9781948040686,13)
,(9782764323519,14)
,(9782764333655,15)
,(9782764336953,16)
,(9781368026697,17)
,(9781484782002,18)
,(9781408855652,19)
,(9781408855669,20)
,(9781408855676,21)
,(9781408855683,22)
,(9781408855690,23)
,(9781408855706,24)
,(9781408855713,25)
,(9781095698655,26)
,(9781095712245,27)
,(9781096123163,28)
,(9781097142804,29)
,(9781097522132,23)
,(9780889953673,24)
,(9781894778145,25)
,(9780888996596,26)
,(9781554988334,27)
,(9781772600384,28)
,(9781459802483,29)
,(9781620142639,1)
,(9780062498564,2)
,(9781524769567,3)
,(9781481400701,4)
,(9781743629000,5)
,(9781772780109,6)
,(9780312625993,7)
,(9781492662143,8)
,(9781467711944,9)
,(9781515782223,10)
,(9780399246531,11)
,(9780763693558,12)
,(9781426307041,13)
,(9781426307935,14)
,(9781426308468,15)
,(9781426310140,16)
,(9781426310492,17)
,(9781426330179,18)
,(9781426332852,19)
,(9781580625579,20)
,(9781580626873,21)
,(9780486468211,22)
,(9781641520416,23)
,(9781641520928,24)
,(9780692848388,25)
,(9780547557991,26)
,(9781732596344,27)
,(9780448405179,28)
,(9781772261059,29)
,(9780062941008,1)
,(9780062953414,2)
,(9780062953452,3)
,(9781633226241,4)
,(9780385320436,5)
,(9781465414182,6)
,(9781623368890,7)
,(9781775183310,8)
,(9781936140404,9)
,(9781780552491,10)
,(9781620917640,11)
,(9781629792033,12)
,(9781629792668,13)
,(9781629794228,14)
,(9781629794235,15)
,(9781629794242,16)
,(9781629796987,17)
,(9781629797687,18)
,(9781629797694,19)
,(9781629798301,20)
,(9781641525312,21)
,(9780606144841,22)
,(9781596437135,23)
,(9781626721067,24)
,(9780439846806,25)
,(9780545132060,26)
,(9780606267380,27)
,(9780810984226,28)
,(9781603090858,29)
,(9780375832291,1)
,(9781608862801,2)
,(9781608868018,3)
,(9781608869008,4)
,(9781449472337,5)
,(9781449483500,6)
,(9781524852269,7)
,(9780439706407,8)
,(9781419712173,9)
,(9780385265201,10)
,(9781608869930,11)
,(9781401273941,12)
,(9781368006187,13)
,(9781368007610,14)
,(9781368023481,15)
,(9781368043632,16)
,(9781368051828,17)
,(9781368053280,18)
,(9781423160342,19)
,(9781948040686,20)
,(9782764323519,21)
,(9782764333655,22)
,(9782764336953,23)
,(9781368026697,24)
,(9781484782002,25)
,(9781408855652,26)
,(9781408855669,27)
,(9781408855676,28)
,(9781408855683,29)
,(9781408855690,1)
,(9781408855706,2)
,(9781408855713,3);

INSERT INTO rssfeeds(feed_id, url, active) VALUES
(1, 'http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml', 1),
(2, 'http://feeds.bbci.co.uk/news/world/africa/rss.xml', 1),
(3, 'http://feeds.bbci.co.uk/news/world/latin_america/rss.xml', 0),
(4, 'https://rss.nytimes.com/services/xml/rss/nyt/World.xml', 0),
(5, 'https://rss.nytimes.com/services/xml/rss/nyt/Arts.xml', 0),
(6, 'https://www.npr.org/rss/podcast.php?id=510298', 0),
(7, 'http://rss.cnn.com/rss/cnn_topstories.xml', 0),
(8, 'http://rss.cnn.com/rss/cnn_latest.rss', 0);