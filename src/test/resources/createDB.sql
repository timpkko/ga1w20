-- Was ran once by Ken (do not re-run)

CREATE database ga1w20;

USE ga1w20;

CREATE USER ga1w20@'localhost' IDENTIFIED WITH mysql_native_password BY 'goats4sofa' REQUIRE NONE;

CREATE USER ga1w20@'%' IDENTIFIED WITH mysql_native_password BY 'goats4sofa' REQUIRE NONE;

GRANT ALL ON ga1w20.* TO ga1w20@'localhost';

GRANT ALL ON ga1w20.* TO ga1w20@'%';

FLUSH PRIVILEGES;