package com.teama1.ga1w20.arquillian.reportqueries;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.SimpleBookQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TopQueryBean;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class ZeroSalesReportTest {

    private final static Logger LOG = LoggerFactory.getLogger(ZeroSalesReportTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<List<Date>, List<SimpleBookQueryBean>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<SimpleBookQueryBean> actualOutputList;

    @Inject
    private ReportQueries queries;

    private ReportQueriesRulePopulator populator;

    public ZeroSalesReportTest() {
        populator = new ReportQueriesRulePopulator();

        try {
            testCases = populator.getZeroSalesRule();
        } catch (ParseException ex) {
            LOG.error("Error parsing dates for getZeroSales test: " + Arrays.toString(ex.getStackTrace()));
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void testZeroSalesResults() {
        List<Date> dates = dynamicParameterHolder.getInput();

        actualOutputList = queries.getZeroSales(dates.get(0), dates.get(1));

        LOG.debug("getZeroSales returned results: " + actualOutputList.toString());

        assertTrue(actualOutputList.containsAll(dynamicParameterHolder.getListOutput()) && dynamicParameterHolder.getListOutput().containsAll(actualOutputList));
    }

    @Test
    public void testNumZeroSalesResults() {
        List<Date> dates = dynamicParameterHolder.getInput();

        actualOutputList = queries.getZeroSales(dates.get(0), dates.get(1));

        assertEquals(dynamicParameterHolder.getListOutput().size(), actualOutputList.size());
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(TopBookSalesTest.class.getPackage())
                .addPackage(ReportQueries.class.getPackage())
                .addPackage(TopQueryBean.class.getPackage());
    }
}
