package com.teama1.ga1w20.arquillian.reportqueries;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.SimpleBookQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TopQueryBean;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class ReportQueriesTest {

    private final static Logger LOG = LoggerFactory.getLogger(ReportQueriesTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    private ReportQueries queries;

    private final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void testNumResultsGetStock() {
        List<SimpleBookQueryBean> stock = queries.getStock();

        assertEquals(20, stock.size());
    }

    @Test
    public void testGetStock() {
        List<SimpleBookQueryBean> expected = fillStockList();

        List<SimpleBookQueryBean> actual = queries.getStock();

        assertTrue("Expected values: " + expected.toString() + "actual values: " + actual.toString(),
                expected.containsAll(actual) && actual.containsAll(expected)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(TopBookSalesTest.class.getPackage())
                .addPackage(ReportQueries.class.getPackage())
                .addPackage(TopQueryBean.class.getPackage())
                .addPackage(BigDecimalHandler.class.getPackage());
    }

    ///////////////////////////////////////////////////////////
    // DECLARE STOCK LIST
    /////////////////////////////////////////////////////////
    public List<SimpleBookQueryBean> fillStockList() {
        List<SimpleBookQueryBean> stockList = new ArrayList<>();

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(23.99),
                        new BigDecimal(0.00),
                        new BigDecimal(12.00),
                        9780062498564L,
                        "On the Come Up"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(15.16),
                        new BigDecimal(0.00),
                        new BigDecimal(8.00),
                        9780062941008L,
                        "Peanut Goes for the Gold"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(5.99),
                        new BigDecimal(0.00),
                        new BigDecimal(3.00),
                        9780062953414L,
                        "The Candy Caper"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(5.99),
                        new BigDecimal(0.00),
                        new BigDecimal(3.00),
                        9780062953452L,
                        "Busted by Breakfast"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(7.99),
                        new BigDecimal(0.00),
                        new BigDecimal(4.00),
                        9780312625993L,
                        "The Forgiveness Garden"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(7.99),
                        new BigDecimal(0.00),
                        new BigDecimal(4.00),
                        9780375832291L,
                        "Babymouse #1: Queen of the World!"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(19.99),
                        new BigDecimal(0.00),
                        new BigDecimal(10.00),
                        9780385265201L,
                        "The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(9.71),
                        new BigDecimal(0.00),
                        new BigDecimal(5.00),
                        9780385320436L,
                        "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(20.69),
                        new BigDecimal(17.00),
                        new BigDecimal(10.00),
                        9780399246531L,
                        "The Day You Begin"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(10.99),
                        new BigDecimal(0.00),
                        new BigDecimal(5.00),
                        9780439706407L,
                        "Bone #1: Out from Boneville"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(9.99),
                        new BigDecimal(0.00),
                        new BigDecimal(5.00),
                        9780439846806L,
                        "The Stonekeeper (Amulet #1)"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(3.99),
                        new BigDecimal(0.00),
                        new BigDecimal(2.00),
                        9780448405179L,
                        "What's Out There?: A Book about Space"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(3.99),
                        new BigDecimal(0.00),
                        new BigDecimal(2.00),
                        9780486468211L,
                        "My First Human Body Book"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(9.89),
                        new BigDecimal(6.00),
                        new BigDecimal(5.00),
                        9780545132060L,
                        "Smile"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(15.00),
                        new BigDecimal(11.00),
                        new BigDecimal(8.00),
                        9780547557991L,
                        "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(10.99),
                        new BigDecimal(6.00),
                        new BigDecimal(5.00),
                        9780606144841L,
                        "American Born Chinese"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(7.99),
                        new BigDecimal(0.00),
                        new BigDecimal(4.00),
                        9780606267380L,
                        "Drama"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(8.00),
                        new BigDecimal(0.00),
                        new BigDecimal(4.00),
                        9780692848388L,
                        "What Should Danny Do?"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(21.99),
                        new BigDecimal(0.00),
                        new BigDecimal(11.00),
                        9780763693558L,
                        "Alma and How She Got Her Name"
                )
        );

        stockList.add(
                new SimpleBookQueryBean(
                        new BigDecimal(9.56),
                        new BigDecimal(6.00),
                        new BigDecimal(5.00),
                        9780810984226L,
                        "How Mirka Got Her Sword (Hereville Book 1)"
                )
        );

        return stockList;
    }
}
