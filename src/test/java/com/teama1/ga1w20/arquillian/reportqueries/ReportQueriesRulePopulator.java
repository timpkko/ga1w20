package com.teama1.ga1w20.arquillian.reportqueries;

import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.queries.beans.AllSalesQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.SimpleBookQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TopQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TotalSalesQueryBean;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Populates rulesets for parameterized testing of Report queries.
 *
 * @author Eira Garrett
 */
public class ReportQueriesRulePopulator {

    private final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

    //Stores start and end dates
    private final List<Date> DATERANGE1 = new ArrayList<>();
    private final List<Date> DATERANGE2 = new ArrayList<>();
    private final List<Date> DATERANGE3 = new ArrayList<>();
    private final List<Date> DATERANGE4 = new ArrayList<>();
    private final List<Date> DATERANGE5 = new ArrayList<>();

    private Long ISBN1 = null;
    private Long ISBN2 = null;
    private Long ISBN3 = null;
    private Long ISBN4 = null;
    private Long ISBN5 = null;

    //Integer = clientId, authorId, or publisherId; Date[] = start and end dates
    private final HashMap<Integer, List<Date>> TOTALSALESINPUT1 = new HashMap<>();
    private final HashMap<Integer, List<Date>> TOTALSALESINPUT2 = new HashMap<>();
    private final HashMap<Integer, List<Date>> TOTALSALESINPUT3 = new HashMap<>();
    private final HashMap<Integer, List<Date>> TOTALSALESINPUT4 = new HashMap<>();
    private final HashMap<Integer, List<Date>> TOTALSALESINPUT5 = new HashMap<>();

    private final List<TopQueryBean> TOPQUERYRESULTLIST1 = new ArrayList<>();
    private final List<TopQueryBean> TOPQUERYRESULTLIST2 = new ArrayList<>();
    private final List<TopQueryBean> TOPQUERYRESULTLIST3 = new ArrayList<>();
    private final List<TopQueryBean> TOPQUERYRESULTLIST4 = new ArrayList<>();
    private final List<TopQueryBean> TOPQUERYRESULTLIST5 = new ArrayList<>();

    private final List<SimpleBookQueryBean> SIMPLEBOOKQUERYRESULTLIST1 = new ArrayList<>();
    private final List<SimpleBookQueryBean> SIMPLEBOOKQUERYRESULTLIST2 = new ArrayList<>();
    private final List<SimpleBookQueryBean> SIMPLEBOOKQUERYRESULTLIST3 = new ArrayList<>();
    private final List<SimpleBookQueryBean> SIMPLEBOOKQUERYRESULTLIST4 = new ArrayList<>();
    private final List<SimpleBookQueryBean> SIMPLEBOOKQUERYRESULTLIST5 = new ArrayList<>();

    private final List<BigDecimal> PRICELIST1 = new ArrayList<>();
    private final List<BigDecimal> PRICELIST2 = new ArrayList<>();
    private final List<BigDecimal> PRICELIST3 = new ArrayList<>();
    private final List<BigDecimal> PRICELIST4 = new ArrayList<>();
    private final List<BigDecimal> PRICELIST5 = new ArrayList<>();

    private final List<TotalSalesQueryBean> TOTALSALESLIST1 = new ArrayList<>();
    private final List<TotalSalesQueryBean> TOTALSALESLIST2 = new ArrayList<>();
    private final List<TotalSalesQueryBean> TOTALSALESLIST3 = new ArrayList<>();
    private final List<TotalSalesQueryBean> TOTALSALESLIST4 = new ArrayList<>();
    private final List<TotalSalesQueryBean> TOTALSALESLIST5 = new ArrayList<>();

    private final List<AllSalesQueryBean> ALLSALESLIST1 = new ArrayList<>();
    private final List<AllSalesQueryBean> ALLSALESLIST2 = new ArrayList<>();
    private final List<AllSalesQueryBean> ALLSALESLIST3 = new ArrayList<>();
    private final List<AllSalesQueryBean> ALLSALESLIST4 = new ArrayList<>();
    private final List<AllSalesQueryBean> ALLSALESLIST5 = new ArrayList<>();

    private final List<Integer> ORDERIDLIST1 = new ArrayList<>();
    private final List<Integer> ORDERIDLIST2 = new ArrayList<>();
    private final List<Integer> ORDERIDLIST3 = new ArrayList<>();
    private final List<Integer> ORDERIDLIST4 = new ArrayList<>();
    private final List<Integer> ORDERIDLIST5 = new ArrayList<>();

    private final List<Long> ISBNLIST1 = new ArrayList<>();
    private final List<Long> ISBNLIST2 = new ArrayList<>();
    private final List<Long> ISBNLIST3 = new ArrayList<>();
    private final List<Long> ISBNLIST4 = new ArrayList<>();
    private final List<Long> ISBNLIST5 = new ArrayList<>();

    public ReportQueriesRulePopulator() {
    }

    /**
     * getTopSellers: returns TopQueryBean TopQueryBean contains fields: -
     * BigDecimal totalSales: total sales of book between given dates - Long id:
     * id of... something - String object: title or firstname + " " + lastname
     *
     * @return
     * @throws ParseException
     */
    public ParameterRule getTopSellersRule() throws ParseException {
        //1 - 2014-04-04 --> 2016-01-01 // 5 books
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2014-04-04"));
        DATERANGE1.add(SDF.parse("2016-01-01"));

        TopQueryBean b1 = new TopQueryBean(
                new BigDecimal(8.00),
                9780062941008L,
                "Peanut Goes for the Gold"
        );

        TopQueryBean b2 = new TopQueryBean(
                new BigDecimal(11.00),
                9780763693558L,
                "Alma and How She Got Her Name"
        );

        TopQueryBean b3 = new TopQueryBean(
                new BigDecimal(8.00),
                9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
        );

        TopQueryBean b4 = new TopQueryBean(
                new BigDecimal(21.29),
                9780545132060L,
                "Smile"
        );

        TopQueryBean b5 = new TopQueryBean(
                new BigDecimal(24.50),
                9780375832291L,
                "Babymouse #1: Queen of the World!"
        );

        TOPQUERYRESULTLIST1.clear();
        TOPQUERYRESULTLIST1.add(b1);
        TOPQUERYRESULTLIST1.add(b2);
        TOPQUERYRESULTLIST1.add(b3);
        TOPQUERYRESULTLIST1.add(b4);
        TOPQUERYRESULTLIST1.add(b5);

        //2 - 2016-01-01 --> 2017-07-07 // 3 books
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2016-01-01"));
        DATERANGE2.add(SDF.parse("2017-07-07"));

        TopQueryBean b6 = new TopQueryBean(
                new BigDecimal(55.32),
                9780062941008L,
                "Peanut Goes for the Gold"
        );

        TopQueryBean b7 = new TopQueryBean(
                new BigDecimal(41.51),
                9780486468211L,
                "My First Human Body Book"
        );

        TopQueryBean b8 = new TopQueryBean(
                new BigDecimal(29.51),
                9780375832291L,
                "Babymouse #1: Queen of the World!"
        );

        TOPQUERYRESULTLIST2.clear();
        TOPQUERYRESULTLIST2.add(b6);
        TOPQUERYRESULTLIST2.add(b7);
        TOPQUERYRESULTLIST2.add(b8);

        //3 - 2000-01-01 --> 2006-01-01 // 3 books
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-01-01"));
        DATERANGE3.add(SDF.parse("2006-01-01"));

        TopQueryBean b9 = new TopQueryBean(
                new BigDecimal(53.89),
                9780062941008L,
                "Peanut Goes for the Gold"
        );

        TopQueryBean b10 = new TopQueryBean(
                new BigDecimal(37.36),
                9780448405179L,
                "What's Out There?: A Book about Space"
        );

        TopQueryBean b11 = new TopQueryBean(
                new BigDecimal(20.66),
                9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        TopQueryBean surprise = new TopQueryBean(
                new BigDecimal(18.00),
                9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
        );

        TOPQUERYRESULTLIST3.clear();
        TOPQUERYRESULTLIST3.add(b9);
        TOPQUERYRESULTLIST3.add(b10);
        TOPQUERYRESULTLIST3.add(b11);
        TOPQUERYRESULTLIST3.add(surprise);

        //4 - 2000-01-01 --> 2015-01-01 // 10 books
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2000-01-01"));
        DATERANGE4.add(SDF.parse("2015-01-01"));

        TopQueryBean b12 = new TopQueryBean(
                new BigDecimal(105.30),
                9780062941008L,
                "Peanut Goes for the Gold"
        );

        TopQueryBean b13 = new TopQueryBean(
                new BigDecimal(25.14),
                9780062498564L,
                "On the Come Up"
        );

        TopQueryBean b14 = new TopQueryBean(
                new BigDecimal(26.22),
                9780399246531L,
                "The Day You Begin"
        );

        TopQueryBean b15 = new TopQueryBean(
                new BigDecimal(11.00),
                9780763693558L,
                "Alma and How She Got Her Name"
        );

        TopQueryBean b16 = new TopQueryBean(
                new BigDecimal(32.91),
                9780692848388L,
                "What Should Danny Do?"
        );

        TopQueryBean b17 = new TopQueryBean(
                new BigDecimal(42.40),
                9780448405179L,
                "What's Out There?: A Book about Space"
        );

        TopQueryBean b18 = new TopQueryBean(
                new BigDecimal(20.66),
                9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        TopQueryBean b19 = new TopQueryBean(
                new BigDecimal(26.77),
                9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)"
        );

        TopQueryBean b20 = new TopQueryBean(
                new BigDecimal(25.94),
                9780606267380L,
                "Drama"
        );

        TopQueryBean surprise2 = new TopQueryBean(
                new BigDecimal(18.00),
                9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
        );

        TOPQUERYRESULTLIST4.clear();
        TOPQUERYRESULTLIST4.add(b12);
        TOPQUERYRESULTLIST4.add(b13);
        TOPQUERYRESULTLIST4.add(b14);
        TOPQUERYRESULTLIST4.add(b15);
        TOPQUERYRESULTLIST4.add(b16);
        TOPQUERYRESULTLIST4.add(b17);
        TOPQUERYRESULTLIST4.add(b18);
        TOPQUERYRESULTLIST4.add(b19);
        TOPQUERYRESULTLIST4.add(b20);
        TOPQUERYRESULTLIST4.add(surprise2);

        //5 - 2010-01-01 --> 2020-01-01 // 15 books
        DATERANGE5.add(SDF.parse("2010-01-01"));
        DATERANGE5.add(SDF.parse("2020-01-01"));

        TopQueryBean b21 = new TopQueryBean(
                new BigDecimal(166.81),
                9780062941008L,
                "Peanut Goes for the Gold"
        );

        TopQueryBean b22 = new TopQueryBean(
                new BigDecimal(43.40),
                9780062953452L,
                "Busted by Breakfast"
        );

        TopQueryBean b23 = new TopQueryBean(
                new BigDecimal(36.04),
                9780062953414L,
                "The Candy Caper"
        );

        TopQueryBean b24 = new TopQueryBean(
                new BigDecimal(25.14),
                9780062498564L,
                "On the Come Up"
        );

        TopQueryBean b25 = new TopQueryBean(
                new BigDecimal(26.42),
                9780312625993L,
                "The Forgiveness Garden"
        );

        TopQueryBean b26 = new TopQueryBean(
                new BigDecimal(11.00),
                9780763693558L,
                "Alma and How She Got Her Name"
        );

        TopQueryBean b27 = new TopQueryBean(
                new BigDecimal(41.51),
                9780486468211L,
                "My First Human Body Book"
        );

        TopQueryBean b28 = new TopQueryBean(
                new BigDecimal(58.92),
                9780692848388L,
                "What Should Danny Do?"
        );

        TopQueryBean b29 = new TopQueryBean(
                new BigDecimal(8.00),
                9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
        );

        TopQueryBean b30 = new TopQueryBean(
                new BigDecimal(24.58),
                9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        TopQueryBean b31 = new TopQueryBean(
                new BigDecimal(20.56),
                9780606144841L,
                "American Born Chinese"
        );

        TopQueryBean b32 = new TopQueryBean(
                new BigDecimal(21.29),
                9780545132060L,
                "Smile"
        );

        TopQueryBean b33 = new TopQueryBean(
                new BigDecimal(26.77),
                9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)"
        );

        TopQueryBean b34 = new TopQueryBean(
                new BigDecimal(85.71),
                9780375832291L,
                "Babymouse #1: Queen of the World!"
        );

        TopQueryBean surprise3 = new TopQueryBean(
                new BigDecimal(17.00),
                9780448405179L,
                "What's Out There?: A Book about Space"
        );

        TOPQUERYRESULTLIST5.clear();
        TOPQUERYRESULTLIST5.add(b21);
        TOPQUERYRESULTLIST5.add(b22);
        TOPQUERYRESULTLIST5.add(b23);
        TOPQUERYRESULTLIST5.add(b24);
        TOPQUERYRESULTLIST5.add(b25);
        TOPQUERYRESULTLIST5.add(b26);
        TOPQUERYRESULTLIST5.add(b27);
        TOPQUERYRESULTLIST5.add(b28);
        TOPQUERYRESULTLIST5.add(b29);
        TOPQUERYRESULTLIST5.add(b30);
        TOPQUERYRESULTLIST5.add(b31);
        TOPQUERYRESULTLIST5.add(b32);
        TOPQUERYRESULTLIST5.add(b33);
        TOPQUERYRESULTLIST5.add(b34);
        TOPQUERYRESULTLIST5.add(surprise3);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(DATERANGE1, TOPQUERYRESULTLIST1),
                new ParameterHolder<>(DATERANGE2, TOPQUERYRESULTLIST2),
                new ParameterHolder<>(DATERANGE3, TOPQUERYRESULTLIST3),
                new ParameterHolder<>(DATERANGE4, TOPQUERYRESULTLIST4),
                new ParameterHolder<>(DATERANGE5, TOPQUERYRESULTLIST5)
        );

        return ruleSet;
    }

    /**
     * getTopSellers: returns TopQueryBean TopQueryBean contains fields: -
     * BigDecimal: totalSales - Long id: client ID - String object: firstname +
     * lastname
     *
     * @return
     * @throws ParseException
     */
    public ParameterRule getTopClientsRule() throws ParseException {
        //1 - 2000-01-01 --> 2005-01-01 // 3 users
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2005-01-01"));

        TopQueryBean b1 = new TopQueryBean(new BigDecimal(28.89), 7, "Louvenia Waters");
        TopQueryBean b2 = new TopQueryBean(new BigDecimal(20.36), 52, "Blanca Kuhlman");
        TopQueryBean b3 = new TopQueryBean(new BigDecimal(80.66), 63, "Donny Hilll");

        TOPQUERYRESULTLIST1.clear();
        TOPQUERYRESULTLIST1.add(b1);
        TOPQUERYRESULTLIST1.add(b2);
        TOPQUERYRESULTLIST1.add(b3);

        //2 - 2000-01-01 --> 2010-01-01 // 6 users
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2010-01-01"));

        TopQueryBean b4 = new TopQueryBean(new BigDecimal(28.89), 7, "Louvenia Waters");
        TopQueryBean b5 = new TopQueryBean(new BigDecimal(26.22), 35, "Henry Sawayn");
        TopQueryBean b6 = new TopQueryBean(new BigDecimal(20.36), 52, "Blanca Kuhlman");
        TopQueryBean b7 = new TopQueryBean(new BigDecimal(5.04), 53, "Lillie OHara");
        TopQueryBean b8 = new TopQueryBean(new BigDecimal(80.66), 63, "Donny Hilll");
        TopQueryBean b9 = new TopQueryBean(new BigDecimal(25.94), 95, "Heidi Moore");

        TOPQUERYRESULTLIST2.clear();
        TOPQUERYRESULTLIST2.add(b4);
        TOPQUERYRESULTLIST2.add(b5);
        TOPQUERYRESULTLIST2.add(b6);
        TOPQUERYRESULTLIST2.add(b7);
        TOPQUERYRESULTLIST2.add(b8);
        TOPQUERYRESULTLIST2.add(b9);

        //3 - 2010-01-01 --> 2015-01-01 // 9 users
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2010-01-01"));
        DATERANGE3.add(SDF.parse("2015-01-01"));

        TopQueryBean b10 = new TopQueryBean(new BigDecimal(19.47), 1, "Consumer Consumer");
        TopQueryBean b11 = new TopQueryBean(new BigDecimal(8.00), 5, "Krystina Wolff");
        TopQueryBean b12 = new TopQueryBean(new BigDecimal(8.00), 6, "Vesta Flatley");
        TopQueryBean b13 = new TopQueryBean(new BigDecimal(15.94), 8, "Giovanni Haag");
        TopQueryBean b14 = new TopQueryBean(new BigDecimal(25.14), 23, "Caesar Hayes");
        TopQueryBean b15 = new TopQueryBean(new BigDecimal(11.00), 36, "Nelda Prosacco");
        TopQueryBean b16 = new TopQueryBean(new BigDecimal(7.45), 43, "Cooper Murphy");
        TopQueryBean b17 = new TopQueryBean(new BigDecimal(25.46), 44, "Adriel Spinka");
        TopQueryBean b18 = new TopQueryBean(new BigDecimal(26.77), 90, "Idell Steuber");

        TOPQUERYRESULTLIST3.clear();
        TOPQUERYRESULTLIST3.add(b10);
        TOPQUERYRESULTLIST3.add(b11);
        TOPQUERYRESULTLIST3.add(b12);
        TOPQUERYRESULTLIST3.add(b13);
        TOPQUERYRESULTLIST3.add(b14);
        TOPQUERYRESULTLIST3.add(b15);
        TOPQUERYRESULTLIST3.add(b16);
        TOPQUERYRESULTLIST3.add(b17);
        TOPQUERYRESULTLIST3.add(b18);

        //4 - 2013-01-01 --> 2017-01-01 // 7 users
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2013-01-01"));
        DATERANGE4.add(SDF.parse("2017-01-01"));

        TopQueryBean b19 = new TopQueryBean(new BigDecimal(8.00), 6, "Vesta Flatley");
        TopQueryBean b20 = new TopQueryBean(new BigDecimal(8.00), 10, "Shany Schuppe");
        TopQueryBean b21 = new TopQueryBean(new BigDecimal(11.00), 36, "Nelda Prosacco");
        TopQueryBean b22 = new TopQueryBean(new BigDecimal(11.84), 40, "Skye Hackett");
        TopQueryBean b23 = new TopQueryBean(new BigDecimal(21.29), 88, "Kirsten Abshire");
        TopQueryBean b24 = new TopQueryBean(new BigDecimal(24.50), 92, "Imogene Stamm");
        TopQueryBean b25 = new TopQueryBean(new BigDecimal(29.51), 93, "Henri Rohan");

        TOPQUERYRESULTLIST4.clear();
        TOPQUERYRESULTLIST4.add(b19);
        TOPQUERYRESULTLIST4.add(b20);
        TOPQUERYRESULTLIST4.add(b21);
        TOPQUERYRESULTLIST4.add(b22);
        TOPQUERYRESULTLIST4.add(b23);
        TOPQUERYRESULTLIST4.add(b24);
        TOPQUERYRESULTLIST4.add(b25);

        //5 - 2015-01-01 --> 2020-01-01 // 14 users
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2015-01-01"));
        DATERANGE5.add(SDF.parse("2020-01-01"));

        TopQueryBean b26 = new TopQueryBean(new BigDecimal(28.54), 9, "Daphne Schimmel");
        TopQueryBean b27 = new TopQueryBean(new BigDecimal(8.00), 10, "Shany Schuppe");
        TopQueryBean b28 = new TopQueryBean(new BigDecimal(21.70), 17, "Santa Reichert");
        TopQueryBean b29 = new TopQueryBean(new BigDecimal(36.04), 18, "Wanda Leuschke");
        TopQueryBean b30 = new TopQueryBean(new BigDecimal(26.42), 29, "Major Kris");
        TopQueryBean b31 = new TopQueryBean(new BigDecimal(29.67), 39, "Tremaine Thompson");
        TopQueryBean b32 = new TopQueryBean(new BigDecimal(11.84), 40, "Skye Hackett");
        TopQueryBean b33 = new TopQueryBean(new BigDecimal(26.01), 42, "Kali Barrows");
        TopQueryBean b34 = new TopQueryBean(new BigDecimal(24.58), 62, "Arjun Block");
        TopQueryBean b35 = new TopQueryBean(new BigDecimal(36.47), 69, "Boris Wisozk");
        TopQueryBean b36 = new TopQueryBean(new BigDecimal(41.85), 88, "Kirsten Abshire");
        //TopQueryBean b37 = new TopQueryBean(new BigDecimal(21.29), 89, "Noel Langosh");
        TopQueryBean b38 = new TopQueryBean(new BigDecimal(24.50), 92, "Imogene Stamm");
        TopQueryBean b39 = new TopQueryBean(new BigDecimal(29.51), 93, "Henri Rohan");

        TOPQUERYRESULTLIST5.clear();
        TOPQUERYRESULTLIST5.add(b26);
        TOPQUERYRESULTLIST5.add(b27);
        TOPQUERYRESULTLIST5.add(b28);
        TOPQUERYRESULTLIST5.add(b29);
        TOPQUERYRESULTLIST5.add(b30);
        TOPQUERYRESULTLIST5.add(b31);
        TOPQUERYRESULTLIST5.add(b32);
        TOPQUERYRESULTLIST5.add(b33);
        TOPQUERYRESULTLIST5.add(b34);
        TOPQUERYRESULTLIST5.add(b35);
        TOPQUERYRESULTLIST5.add(b36);
        //TOPQUERYRESULTLIST5.add(b37);
        TOPQUERYRESULTLIST5.add(b38);
        TOPQUERYRESULTLIST5.add(b39);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(DATERANGE1, TOPQUERYRESULTLIST1),
                new ParameterHolder<>(DATERANGE2, TOPQUERYRESULTLIST2),
                new ParameterHolder<>(DATERANGE3, TOPQUERYRESULTLIST3),
                new ParameterHolder<>(DATERANGE4, TOPQUERYRESULTLIST4),
                new ParameterHolder<>(DATERANGE5, TOPQUERYRESULTLIST5)
        );

        return ruleSet;
    }

    /**
     * getZeroSales returns a list of SimpleBookQueryBean objects.
     * SimpleBookQueryBean stores the following: - listPrice (BigDecimal) -
     * salePrice (BigDecimal) - wholesalePrice (BigDecimal) - isbn (Long) -
     * title (String)
     *
     * @return
     */
    public ParameterRule getZeroSalesRule() throws ParseException {
        //1 - '2010-01-01' --> '2020-01-01' // 5 results
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2010-01-01"));
        DATERANGE1.add(SDF.parse("2020-01-01"));

        SimpleBookQueryBean b1 = new SimpleBookQueryBean(
                new BigDecimal(19.99),
                new BigDecimal(0.00),
                new BigDecimal(10.00),
                9780385265201L,
                "The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great"
        );

        SimpleBookQueryBean b2 = new SimpleBookQueryBean(
                new BigDecimal(20.69),
                new BigDecimal(17.00),
                new BigDecimal(10.00),
                9780399246531L,
                "The Day You Begin"
        );

        SimpleBookQueryBean b3 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439706407L,
                "Bone #1: Out from Boneville"
        );

        SimpleBookQueryBean b4 = new SimpleBookQueryBean(
                new BigDecimal(9.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439846806L,
                "The Stonekeeper (Amulet #1)"
        );

        SimpleBookQueryBean b5 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780606267380L,
                "Drama"
        );

        SIMPLEBOOKQUERYRESULTLIST1.clear();
        SIMPLEBOOKQUERYRESULTLIST1.add(b1);
        SIMPLEBOOKQUERYRESULTLIST1.add(b2);
        SIMPLEBOOKQUERYRESULTLIST1.add(b3);
        SIMPLEBOOKQUERYRESULTLIST1.add(b4);
        SIMPLEBOOKQUERYRESULTLIST1.add(b5);

        //2 - '2010-01-01' --> '2018-01-01' // 10 results
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2010-01-01"));
        DATERANGE2.add(SDF.parse("2018-01-01"));

        SimpleBookQueryBean b6 = new SimpleBookQueryBean(
                new BigDecimal(5.99),
                new BigDecimal(0.00),
                new BigDecimal(3.00),
                9780062953452L,
                "Busted by Breakfast"
        );

        SimpleBookQueryBean b7 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780312625993L,
                "The Forgiveness Garden"
        );

        SimpleBookQueryBean b8 = new SimpleBookQueryBean(
                new BigDecimal(19.99),
                new BigDecimal(0.00),
                new BigDecimal(10.00),
                9780385265201L,
                "The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great"
        );

        SimpleBookQueryBean b9 = new SimpleBookQueryBean(
                new BigDecimal(9.71),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        SimpleBookQueryBean b10 = new SimpleBookQueryBean(
                new BigDecimal(20.69),
                new BigDecimal(17.00),
                new BigDecimal(10.00),
                9780399246531L,
                "The Day You Begin"
        );

        SimpleBookQueryBean b11 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439706407L,
                "Bone #1: Out from Boneville"
        );

        SimpleBookQueryBean b12 = new SimpleBookQueryBean(
                new BigDecimal(9.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439846806L,
                "The Stonekeeper (Amulet #1)"
        );

        SimpleBookQueryBean b13 = new SimpleBookQueryBean(
                new BigDecimal(3.99),
                new BigDecimal(0.00),
                new BigDecimal(2.00),
                9780448405179L,
                "What's Out There?: A Book about Space"
        );

        SimpleBookQueryBean b14 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(6.00),
                new BigDecimal(5.00),
                9780606144841L,
                "American Born Chinese"
        );

        SimpleBookQueryBean b15 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780606267380L,
                "Drama"
        );

        SIMPLEBOOKQUERYRESULTLIST2.clear();
        SIMPLEBOOKQUERYRESULTLIST2.add(b6);
        SIMPLEBOOKQUERYRESULTLIST2.add(b7);
        SIMPLEBOOKQUERYRESULTLIST2.add(b8);
        SIMPLEBOOKQUERYRESULTLIST2.add(b9);
        SIMPLEBOOKQUERYRESULTLIST2.add(b10);
        SIMPLEBOOKQUERYRESULTLIST2.add(b11);
        SIMPLEBOOKQUERYRESULTLIST2.add(b12);
        SIMPLEBOOKQUERYRESULTLIST2.add(b13);
        SIMPLEBOOKQUERYRESULTLIST2.add(b14);
        SIMPLEBOOKQUERYRESULTLIST2.add(b15);

        //3 - '2010-01-01' --> '2015-01-01' // 15 results
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2010-01-01"));
        DATERANGE3.add(SDF.parse("2015-01-01"));

        SimpleBookQueryBean b16 = new SimpleBookQueryBean(
                new BigDecimal(5.99),
                new BigDecimal(0.00),
                new BigDecimal(3.00),
                9780062953414L,
                "The Candy Caper"
        );

        SimpleBookQueryBean b17 = new SimpleBookQueryBean(
                new BigDecimal(5.99),
                new BigDecimal(0.00),
                new BigDecimal(3.00),
                9780062953452L,
                "Busted by Breakfast"
        );

        SimpleBookQueryBean b18 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780312625993L,
                "The Forgiveness Garden"
        );

        SimpleBookQueryBean b19 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780375832291L,
                "Babymouse #1: Queen of the World!"
        );

        SimpleBookQueryBean b20 = new SimpleBookQueryBean(
                new BigDecimal(19.99),
                new BigDecimal(0.00),
                new BigDecimal(10.00),
                9780385265201L,
                "The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great"
        );

        SimpleBookQueryBean b21 = new SimpleBookQueryBean(
                new BigDecimal(9.71),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        SimpleBookQueryBean b22 = new SimpleBookQueryBean(
                new BigDecimal(20.69),
                new BigDecimal(17.00),
                new BigDecimal(10.00),
                9780399246531L,
                "The Day You Begin"
        );

        SimpleBookQueryBean b23 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439706407L,
                "Bone #1: Out from Boneville"
        );

        SimpleBookQueryBean b24 = new SimpleBookQueryBean(
                new BigDecimal(9.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439846806L,
                "The Stonekeeper (Amulet #1)"
        );

        SimpleBookQueryBean b25 = new SimpleBookQueryBean(
                new BigDecimal(3.99),
                new BigDecimal(0.00),
                new BigDecimal(2.00),
                9780448405179L,
                "What's Out There?: A Book about Space"
        );

        SimpleBookQueryBean b26 = new SimpleBookQueryBean(
                new BigDecimal(3.99),
                new BigDecimal(0.00),
                new BigDecimal(2.00),
                9780486468211L,
                "My First Human Body Book"
        );

        SimpleBookQueryBean b27 = new SimpleBookQueryBean(
                new BigDecimal(9.89),
                new BigDecimal(6.00),
                new BigDecimal(5.00),
                9780545132060L,
                "Smile"
        );

        SimpleBookQueryBean b28 = new SimpleBookQueryBean(
                new BigDecimal(15.00),
                new BigDecimal(11.00),
                new BigDecimal(8.00),
                9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
        );

        SimpleBookQueryBean b29 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(6.00),
                new BigDecimal(5.00),
                9780606144841L,
                "American Born Chinese"
        );

        SimpleBookQueryBean b30 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780606267380L,
                "Drama"
        );

        SIMPLEBOOKQUERYRESULTLIST3.clear();
        SIMPLEBOOKQUERYRESULTLIST3.add(b16);
        SIMPLEBOOKQUERYRESULTLIST3.add(b17);
        SIMPLEBOOKQUERYRESULTLIST3.add(b18);
        SIMPLEBOOKQUERYRESULTLIST3.add(b19);
        SIMPLEBOOKQUERYRESULTLIST3.add(b20);
        SIMPLEBOOKQUERYRESULTLIST3.add(b21);
        SIMPLEBOOKQUERYRESULTLIST3.add(b22);
        SIMPLEBOOKQUERYRESULTLIST3.add(b23);
        SIMPLEBOOKQUERYRESULTLIST3.add(b24);
        SIMPLEBOOKQUERYRESULTLIST3.add(b25);
        SIMPLEBOOKQUERYRESULTLIST3.add(b26);
        SIMPLEBOOKQUERYRESULTLIST3.add(b27);
        SIMPLEBOOKQUERYRESULTLIST3.add(b28);
        SIMPLEBOOKQUERYRESULTLIST3.add(b29);
        SIMPLEBOOKQUERYRESULTLIST3.add(b30);

        //4 - '2015-01-01' --> '2020-01-01' // 8 result
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2015-01-01"));
        DATERANGE4.add(SDF.parse("2020-01-01"));

        SimpleBookQueryBean b31 = new SimpleBookQueryBean(
                new BigDecimal(23.99),
                new BigDecimal(0.00),
                new BigDecimal(12.00),
                9780062498564L,
                "On the Come Up"
        );

        SimpleBookQueryBean b32 = new SimpleBookQueryBean(
                new BigDecimal(19.99),
                new BigDecimal(0.00),
                new BigDecimal(10.00),
                9780385265201L,
                "The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great"
        );

        SimpleBookQueryBean b33 = new SimpleBookQueryBean(
                new BigDecimal(20.69),
                new BigDecimal(17.00),
                new BigDecimal(10.00),
                9780399246531L,
                "The Day You Begin"
        );

        SimpleBookQueryBean b34 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439706407L,
                "Bone #1: Out from Boneville"
        );

        SimpleBookQueryBean b35 = new SimpleBookQueryBean(
                new BigDecimal(9.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439846806L,
                "The Stonekeeper (Amulet #1)"
        );

        SimpleBookQueryBean b36 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780606267380L,
                "Drama"
        );

        SimpleBookQueryBean b37 = new SimpleBookQueryBean(
                new BigDecimal(21.99),
                new BigDecimal(0.00),
                new BigDecimal(11.00),
                9780763693558L,
                "Alma and How She Got Her Name"
        );

        SimpleBookQueryBean b38 = new SimpleBookQueryBean(
                new BigDecimal(9.56),
                new BigDecimal(6.00),
                new BigDecimal(5.00),
                9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)"
        );

        SIMPLEBOOKQUERYRESULTLIST4.clear();
        SIMPLEBOOKQUERYRESULTLIST4.add(b31);
        SIMPLEBOOKQUERYRESULTLIST4.add(b32);
        SIMPLEBOOKQUERYRESULTLIST4.add(b33);
        SIMPLEBOOKQUERYRESULTLIST4.add(b34);
        SIMPLEBOOKQUERYRESULTLIST4.add(b35);
        SIMPLEBOOKQUERYRESULTLIST4.add(b36);
        SIMPLEBOOKQUERYRESULTLIST4.add(b37);
        SIMPLEBOOKQUERYRESULTLIST4.add(b38);

        //5 - '2000-01-01' --> '2010-01-01' // 14 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2010-01-01"));

        SimpleBookQueryBean b39 = new SimpleBookQueryBean(
                new BigDecimal(23.99),
                new BigDecimal(0.00),
                new BigDecimal(12.00),
                9780062498564L,
                "On the Come Up"
        );

        SimpleBookQueryBean b40 = new SimpleBookQueryBean(
                new BigDecimal(5.99),
                new BigDecimal(0.00),
                new BigDecimal(3.00),
                9780062953414L,
                "The Candy Caper"
        );

        SimpleBookQueryBean b41 = new SimpleBookQueryBean(
                new BigDecimal(5.99),
                new BigDecimal(0.00),
                new BigDecimal(3.00),
                9780062953452L,
                "Busted by Breakfast"
        );

        SimpleBookQueryBean b42 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780312625993L,
                "The Forgiveness Garden"
        );

        SimpleBookQueryBean b43 = new SimpleBookQueryBean(
                new BigDecimal(7.99),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780375832291L,
                "Babymouse #1: Queen of the World!"
        );

        SimpleBookQueryBean b44 = new SimpleBookQueryBean(
                new BigDecimal(19.99),
                new BigDecimal(0.00),
                new BigDecimal(10.00),
                9780385265201L,
                "The Cartoon History of the Universe: Volumes 1-7: From the Big Bang to Alexander the Great"
        );

        SimpleBookQueryBean b45 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439706407L,
                "Bone #1: Out from Boneville"
        );

        SimpleBookQueryBean b46 = new SimpleBookQueryBean(
                new BigDecimal(9.99),
                new BigDecimal(0.00),
                new BigDecimal(5.00),
                9780439846806L,
                "The Stonekeeper (Amulet #1)"
        );

        SimpleBookQueryBean b47 = new SimpleBookQueryBean(
                new BigDecimal(3.99),
                new BigDecimal(0.00),
                new BigDecimal(2.00),
                9780486468211L,
                "My First Human Body Book"
        );

        SimpleBookQueryBean b48 = new SimpleBookQueryBean(
                new BigDecimal(9.89),
                new BigDecimal(6.00),
                new BigDecimal(5.00),
                9780545132060L,
                "Smile"
        );

        SimpleBookQueryBean b49 = new SimpleBookQueryBean(
                new BigDecimal(10.99),
                new BigDecimal(6.00),
                new BigDecimal(5.00),
                9780606144841L,
                "American Born Chinese"
        );

        SimpleBookQueryBean b50 = new SimpleBookQueryBean(
                new BigDecimal(8.00),
                new BigDecimal(0.00),
                new BigDecimal(4.00),
                9780692848388L,
                "What Should Danny Do?"
        );

        SimpleBookQueryBean b51 = new SimpleBookQueryBean(
                new BigDecimal(21.99),
                new BigDecimal(0.00),
                new BigDecimal(11.00),
                9780763693558L,
                "Alma and How She Got Her Name"
        );

        SimpleBookQueryBean b52 = new SimpleBookQueryBean(
                new BigDecimal(9.56),
                new BigDecimal(6.00),
                new BigDecimal(5.00),
                9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)"
        );

        SIMPLEBOOKQUERYRESULTLIST5.clear();
        SIMPLEBOOKQUERYRESULTLIST5.add(b39);
        SIMPLEBOOKQUERYRESULTLIST5.add(b40);
        SIMPLEBOOKQUERYRESULTLIST5.add(b41);
        SIMPLEBOOKQUERYRESULTLIST5.add(b42);
        SIMPLEBOOKQUERYRESULTLIST5.add(b43);
        SIMPLEBOOKQUERYRESULTLIST5.add(b44);
        SIMPLEBOOKQUERYRESULTLIST5.add(b45);
        SIMPLEBOOKQUERYRESULTLIST5.add(b46);
        SIMPLEBOOKQUERYRESULTLIST5.add(b47);
        SIMPLEBOOKQUERYRESULTLIST5.add(b48);
        SIMPLEBOOKQUERYRESULTLIST5.add(b49);
        SIMPLEBOOKQUERYRESULTLIST5.add(b50);
        SIMPLEBOOKQUERYRESULTLIST5.add(b51);
        SIMPLEBOOKQUERYRESULTLIST5.add(b52);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(DATERANGE1, SIMPLEBOOKQUERYRESULTLIST1),
                new ParameterHolder<>(DATERANGE2, SIMPLEBOOKQUERYRESULTLIST2),
                new ParameterHolder<>(DATERANGE3, SIMPLEBOOKQUERYRESULTLIST3),
                new ParameterHolder<>(DATERANGE4, SIMPLEBOOKQUERYRESULTLIST4),
                new ParameterHolder<>(DATERANGE5, SIMPLEBOOKQUERYRESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getTotalBookSalesRule() {
        //1 - isbn: 9780062498564, total: 25.14
        ISBN1 = 9780062498564L;

        PRICELIST1.clear();
        PRICELIST1.add(new BigDecimal(25.14));

        //2 - isbn: 9780062941008, total: 220.70
        ISBN2 = 9780062941008L;

        PRICELIST2.clear();
        PRICELIST2.add(new BigDecimal(220.70));

        //3 - isbn: 9780545132060, total: 21.29
        ISBN3 = 9780545132060L;

        PRICELIST3.clear();
        PRICELIST3.add(new BigDecimal(21.29));

        //4 - isbn: 9780062953452, total: 43.40
        ISBN4 = 9780062953452L;

        PRICELIST4.clear();
        PRICELIST4.add(new BigDecimal(43.40));

        //5 - isbn: 9780606267380, total: 25.94
        ISBN5 = 9780606267380L;

        PRICELIST5.clear();
        PRICELIST5.add(new BigDecimal(25.94));

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(ISBN1, PRICELIST1),
                new ParameterHolder<>(ISBN2, PRICELIST2),
                new ParameterHolder<>(ISBN3, PRICELIST3),
                new ParameterHolder<>(ISBN4, PRICELIST4),
                new ParameterHolder<>(ISBN5, PRICELIST5)
        );

        return ruleSet;
    }

    /**
     * getTotalSales(Date, Date) returns a TotalSalesQueryBean containing: -
     * total sales - an empty string
     *
     * @return
     */
    public ParameterRule getTotalSalesBetweenDatesRule() throws ParseException {
        //1 - '2000-01-01' --> '2005-01-01' // 129.91
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2005-01-01"));

        TotalSalesQueryBean b1 = new TotalSalesQueryBean(
                new BigDecimal(129.91),
                ""
        );

        TOTALSALESLIST1.clear();
        TOTALSALESLIST1.add(b1);

        //2 - '2000-01-01' --> '2020-01-01' // 800.26
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2020-01-01"));

        TOTALSALESLIST2.clear();
        TOTALSALESLIST2.add(new TotalSalesQueryBean(
                new BigDecimal(800.26),
                ""
        ));

        //3 - '1995-01-01' --> '2000-01-01' // 0.00
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("1995-01-01"));
        DATERANGE3.add(SDF.parse("2000-01-01"));

        TOTALSALESLIST3.clear();
        TOTALSALESLIST3.add(new TotalSalesQueryBean(
                new BigDecimal(0.00),
                ""
        ));

        //4 - '2015-01-01' --> '2030-01-01' // 465.92
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2015-01-01"));
        DATERANGE4.add(SDF.parse("2030-01-01"));

        TOTALSALESLIST4.clear();
        TOTALSALESLIST4.add(new TotalSalesQueryBean(
                new BigDecimal(465.92),
                ""
        ));

        //5 - 2007-01-01 --> 2020-01-01 // 644.13
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2007-01-01"));
        DATERANGE5.add(SDF.parse("2020-01-01"));

        TOTALSALESLIST5.clear();
        TOTALSALESLIST5.add(new TotalSalesQueryBean(
                new BigDecimal(644.13),
                ""
        ));

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(DATERANGE1, TOTALSALESLIST1),
                new ParameterHolder<>(DATERANGE2, TOTALSALESLIST2),
                new ParameterHolder<>(DATERANGE3, TOTALSALESLIST3),
                new ParameterHolder<>(DATERANGE4, TOTALSALESLIST4),
                new ParameterHolder<>(DATERANGE5, TOTALSALESLIST5)
        );

        return ruleSet;
    }

    /**
     * getAllSales retrieves a list of AllSalesQueryBeans
     *
     * AllSalesQueryBean stores: - BigDecimal price - Date purchaseDate - Long
     * isbn - String name (empty in this case) - String title
     *
     * @return
     */
    public ParameterRule getAllSalesBetweenDatesRule() throws ParseException {
        //1 - '2000-01-01' --> 2000-09-09 // 0 results
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2000-09-09"));

        ALLSALESLIST1.clear();

        //2 - 2000-01-01 --> 2005-01-01 // 6 results
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2005-01-01"));

        AllSalesQueryBean b1 = new AllSalesQueryBean(
                new BigDecimal(28.89),
                SDF.parse("2000-11-04"),
                9780062941008L,
                "",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b2 = new AllSalesQueryBean(
                new BigDecimal(20.36),
                SDF.parse("2003-03-07"),
                9780448405179L,
                "",
                "What's Out There?: A Book about Space"
        );

        AllSalesQueryBean b3 = new AllSalesQueryBean(
                new BigDecimal(20.66),
                SDF.parse("2003-05-20"),
                9780385320436L,
                "",
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        AllSalesQueryBean b4 = new AllSalesQueryBean(
                new BigDecimal(18.00),
                SDF.parse("2003-05-20"),
                9780547557991L,
                "",
                "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
        );

        AllSalesQueryBean b5 = new AllSalesQueryBean(
                new BigDecimal(17.00),
                SDF.parse("2003-05-20"),
                9780448405179L,
                "",
                "What's Out There?: A Book about Space"
        );

        AllSalesQueryBean b6 = new AllSalesQueryBean(
                new BigDecimal(25.00),
                SDF.parse("2003-05-20"),
                9780062941008L,
                "",
                "Peanut Goes for the Gold"
        );

        ALLSALESLIST2.clear();
        ALLSALESLIST2.add(b1);
        ALLSALESLIST2.add(b2);
        ALLSALESLIST2.add(b3);
        ALLSALESLIST2.add(b4);
        ALLSALESLIST2.add(b5);
        ALLSALESLIST2.add(b6);

        //3 - 2005-01-01 --> 2007-01-01 // 1 result
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2005-01-01"));
        DATERANGE3.add(SDF.parse("2007-01-01"));

        ALLSALESLIST3.clear();
        ALLSALESLIST3.add(
                new AllSalesQueryBean(
                        new BigDecimal(26.22),
                        SDF.parse("2006-06-08"),
                        9780399246531L,
                        "",
                        "The Day You Begin"
                )
        );

        //4 - 2010-01-01 --> 2011-01-01 // 2 results
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2010-01-01"));
        DATERANGE4.add(SDF.parse("2011-01-01"));

        AllSalesQueryBean b7 = new AllSalesQueryBean(
                new BigDecimal(19.47),
                SDF.parse("2010-03-07"),
                9780062941008L,
                "",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b8 = new AllSalesQueryBean(
                new BigDecimal(26.77),
                SDF.parse("2010-03-06"),
                9780810984226L,
                "",
                "How Mirka Got Her Sword (Hereville Book 1)"
        );

        ALLSALESLIST4.clear();
        ALLSALESLIST4.add(b7);
        ALLSALESLIST4.add(b8);

        //5 - 2018-01-01 --> 2020-01-01 // 11 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2018-01-01"));
        DATERANGE5.add(SDF.parse("2020-01-01"));

        AllSalesQueryBean b9 = new AllSalesQueryBean(
                new BigDecimal(18.95),
                SDF.parse("2018-06-03"),
                9780062941008L,
                "",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b10 = new AllSalesQueryBean(
                new BigDecimal(21.66),
                SDF.parse("2019-09-03"),
                9780062941008L,
                "",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b11 = new AllSalesQueryBean(
                new BigDecimal(21.70),
                SDF.parse("2019-07-18"),
                9780062953452L,
                "",
                "Busted by Breakfast"
        );

        AllSalesQueryBean b12 = new AllSalesQueryBean(
                new BigDecimal(26.42),
                SDF.parse("2018-06-24"),
                9780312625993L,
                "",
                "The Forgiveness Garden"
        );

        AllSalesQueryBean b13 = new AllSalesQueryBean(
                new BigDecimal(26.01),
                SDF.parse("2019-04-14"),
                9780692848388L,
                "",
                "What Should Danny Do?"
        );

        AllSalesQueryBean b14 = new AllSalesQueryBean(
                new BigDecimal(24.58),
                SDF.parse("2019-05-27"),
                9780385320436L,
                "",
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        AllSalesQueryBean b15 = new AllSalesQueryBean(
                new BigDecimal(17.00),
                SDF.parse("2019-09-20"),
                9780448405179L,
                "",
                "What's Out There?: A Book about Space"
        );

        AllSalesQueryBean b16 = new AllSalesQueryBean(
                new BigDecimal(19.47),
                SDF.parse("2019-09-20"),
                9780062941008L,
                "",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b17 = new AllSalesQueryBean(
                new BigDecimal(20.56),
                SDF.parse("2019-04-29"),
                9780606144841L,
                "",
                "American Born Chinese"
        );

        AllSalesQueryBean b18 = new AllSalesQueryBean(
                new BigDecimal(21.70),
                SDF.parse("2018-06-07"),
                9780062953452L,
                "",
                "Busted by Breakfast"
        );

        AllSalesQueryBean b19 = new AllSalesQueryBean(
                new BigDecimal(31.70),
                SDF.parse("2018-06-07"),
                9780375832291L,
                "",
                "Babymouse #1: Queen of the World!"
        );

        ALLSALESLIST5.clear();
        ALLSALESLIST5.add(b9);
        ALLSALESLIST5.add(b10);
        ALLSALESLIST5.add(b11);
        ALLSALESLIST5.add(b12);
        ALLSALESLIST5.add(b13);
        ALLSALESLIST5.add(b14);
        ALLSALESLIST5.add(b15);
        ALLSALESLIST5.add(b16);
        ALLSALESLIST5.add(b17);
        ALLSALESLIST5.add(b18);
        ALLSALESLIST5.add(b19);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(DATERANGE1, ALLSALESLIST1),
                new ParameterHolder<>(DATERANGE2, ALLSALESLIST2),
                new ParameterHolder<>(DATERANGE3, ALLSALESLIST3),
                new ParameterHolder<>(DATERANGE4, ALLSALESLIST4),
                new ParameterHolder<>(DATERANGE5, ALLSALESLIST5)
        );

        return ruleSet;
    }

    /**
     * TotalSalesQueryBean contains total purchase and name of client.
     */
    public ParameterRule getTotalSalesByClientRule() throws ParseException {
        //1 - user_id 1, 2000-01-01 --> 2020-01-01 // 19.47, Consumer Consumer
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT1.clear();
        TOTALSALESINPUT1.put(1, DATERANGE1);

        TOTALSALESLIST1.clear();
        TOTALSALESLIST1.add(
                new TotalSalesQueryBean(
                        new BigDecimal(19.47),
                        "Consumer Consumer"
                )
        );

        //2 - user_id 8, 2000-01-01 --> 2020-01-01 // 15.94, Giovanni Haag
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT2.clear();
        TOTALSALESINPUT2.put(8, DATERANGE2);

        TOTALSALESLIST2.clear();
        TOTALSALESLIST2.add(
                new TotalSalesQueryBean(
                        new BigDecimal(15.94),
                        "Giovanni Haag"
                )
        );

        //3 - user_id 36, 2000-01-01 --> 2020-01-01 // 11.00, Nelda Prosacco
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-01-01"));
        DATERANGE3.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT3.clear();
        TOTALSALESINPUT3.put(36, DATERANGE3);

        TOTALSALESLIST3.clear();
        TOTALSALESLIST3.add(
                new TotalSalesQueryBean(
                        new BigDecimal(11.00),
                        "Nelda Prosacco"
                )
        );

        //4 - user_id 7, 2000-01-01 --> 2020-01-01 // 28.89, Louvenia Waters
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2000-01-01"));
        DATERANGE4.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT4.clear();
        TOTALSALESINPUT4.put(7, DATERANGE4);

        TOTALSALESLIST4.clear();
        TOTALSALESLIST4.add(
                new TotalSalesQueryBean(
                        new BigDecimal(28.89),
                        "Louvenia Waters"
                )
        );

        //5 - user_id 99, 2000-01-01 --> 2020-01-01 // 0 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT5.clear();
        TOTALSALESINPUT5.put(99, DATERANGE5);

        TOTALSALESLIST5.clear();
        TOTALSALESLIST5.add(
                new TotalSalesQueryBean(new BigDecimal(0.00), "Cheap Skate")
        );

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(TOTALSALESINPUT1, TOTALSALESLIST1),
                new ParameterHolder<>(TOTALSALESINPUT2, TOTALSALESLIST2),
                new ParameterHolder<>(TOTALSALESINPUT3, TOTALSALESLIST3),
                new ParameterHolder<>(TOTALSALESINPUT4, TOTALSALESLIST4),
                new ParameterHolder<>(TOTALSALESINPUT5, TOTALSALESLIST5)
        );

        return ruleSet;
    }

    //AllSalesQueryBean : BigDecimal price, Date purchase_date, Long isbn, client name, title of book
    public ParameterRule getAllSalesByClientRule() throws ParseException {
        //1 - client_id 1, 2000-01-01 --> 2020-01-01 // 1 result
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT1.clear();
        TOTALSALESINPUT1.put(1, DATERANGE1);

        ALLSALESLIST1.clear();
        ALLSALESLIST1.add(
                new AllSalesQueryBean(
                        new BigDecimal(19.47),
                        SDF.parse("2010-03-07"),
                        9780062941008L,
                        "Consumer Consumer",
                        "Peanut Goes for the Gold"
                )
        );

        //2 - client_id 63, 2000-01-01 --> 2020-01-01 // 4 results
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT2.clear();
        TOTALSALESINPUT2.put(63, DATERANGE2);

        AllSalesQueryBean b1 = new AllSalesQueryBean(
                new BigDecimal(20.66),
                SDF.parse("2003-05-20"),
                9780385320436L,
                "Donny Hilll",
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        AllSalesQueryBean b2 = new AllSalesQueryBean(
                new BigDecimal(18.00),
                SDF.parse("2003-05-20"),
                9780547557991L,
                "Donny Hilll",
                "The Animal Book: A Collection of the Fastest, Fiercest, Toughest, Cleverest, Shyest and Most Surprising Animals on Earth"
        );

        AllSalesQueryBean b3 = new AllSalesQueryBean(
                new BigDecimal(17.00),
                SDF.parse("2003-05-20"),
                9780448405179L,
                "Donny Hilll",
                "What's Out There?: A Book about Space"
        );

        AllSalesQueryBean b4 = new AllSalesQueryBean(
                new BigDecimal(25.00),
                SDF.parse("2003-05-20"),
                9780062941008L,
                "Donny Hilll",
                "Peanut Goes for the Gold"
        );

        ALLSALESLIST2.clear();
        ALLSALESLIST2.add(b1);
        ALLSALESLIST2.add(b2);
        ALLSALESLIST2.add(b3);
        ALLSALESLIST2.add(b4);

        //3 - user_id: 18, 2000-01-01-->2020-01-01 // 1 result
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-01-01"));
        DATERANGE3.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT3.clear();
        TOTALSALESINPUT3.put(18, DATERANGE3);

        AllSalesQueryBean b5 = new AllSalesQueryBean(
                new BigDecimal(36.04),
                SDF.parse("2017-08-19"),
                9780062953414L,
                "Wanda Leuschke",
                "The Candy Caper"
        );

        ALLSALESLIST3.clear();
        ALLSALESLIST3.add(b5);

        //4 - user_id 17, 2018-06-06 --> 2020-01-01 // 1 result
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2018-06-06"));
        DATERANGE4.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT4.clear();
        TOTALSALESINPUT4.put(17, DATERANGE4);

        AllSalesQueryBean b7 = new AllSalesQueryBean(
                new BigDecimal(21.70),
                SDF.parse("2019-07-18"),
                9780062953452L,
                "Santa Reichert",
                "Busted by Breakfast"
        );

        ALLSALESLIST4.clear();
        ALLSALESLIST4.add(b7);

        //5 - user_id 99, 2000-01-01 --> 2020-01-01 // 0 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT5.clear();
        TOTALSALESINPUT5.put(99, DATERANGE5);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(TOTALSALESINPUT1, ALLSALESLIST1),
                new ParameterHolder<>(TOTALSALESINPUT2, ALLSALESLIST2),
                new ParameterHolder<>(TOTALSALESINPUT3, ALLSALESLIST3),
                new ParameterHolder<>(TOTALSALESINPUT4, ALLSALESLIST4),
                new ParameterHolder<>(TOTALSALESINPUT5, ALLSALESLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getTotalSalesByAuthorRule() throws ParseException {
        //1 - author_id 88, 2000-01-01-->2020-01-01 // 220.70, Jonathan Van Ness
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT1.clear();
        TOTALSALESINPUT1.put(88, DATERANGE1);

        TOTALSALESLIST1.clear();
        TOTALSALESLIST1.add(
                new TotalSalesQueryBean(
                        new BigDecimal(220.70),
                        "Jonathan Van Ness"
                )
        );

        //2 - author_id 88, 2000-11-03 --> 2011-12-05 // 89.30, Jonathan Van Ness
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-11-03"));
        DATERANGE2.add(SDF.parse("2011-12-05"));

        TOTALSALESINPUT2.clear();
        TOTALSALESINPUT2.put(88, DATERANGE2);

        TOTALSALESLIST2.clear();
        TOTALSALESLIST2.add(
                new TotalSalesQueryBean(
                        new BigDecimal(89.30),
                        "Jonathan Van Ness"
                )
        );

        //3 - author_id 88, 2000-11-03 --> 2000-12-05 // 28.89, Jonathan Van Ness
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-11-03"));
        DATERANGE3.add(SDF.parse("2000-12-05"));

        TOTALSALESINPUT3.clear();
        TOTALSALESINPUT3.put(88, DATERANGE3);

        TOTALSALESLIST3.clear();
        TOTALSALESLIST3.add(
                new TotalSalesQueryBean(
                        new BigDecimal(28.89),
                        "Jonathan Van Ness"
                )
        );

        //4 - author_id 33, 2017-11-03-->2020-12-30 // 31.70, Jennifer L. Holm
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2017-11-03"));
        DATERANGE4.add(SDF.parse("2020-12-30"));

        TOTALSALESINPUT4.clear();
        TOTALSALESINPUT4.put(33, DATERANGE4);

        TOTALSALESLIST4.clear();
        TOTALSALESLIST4.add(
                new TotalSalesQueryBean(
                        new BigDecimal(31.70),
                        "Jennifer L. Holm"
                )
        );

        //5 - author_id 33, 2017-10-10-->2017-10-11 // 0 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2017-10-10"));
        DATERANGE5.add(SDF.parse("2017-10-11"));

        TOTALSALESINPUT5.clear();
        TOTALSALESINPUT5.put(33, DATERANGE5);

        TOTALSALESLIST5.clear();
        TOTALSALESLIST5.add(
                new TotalSalesQueryBean(new BigDecimal(0.00), "Jennifer L. Holm")
        );

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(TOTALSALESINPUT1, TOTALSALESLIST1),
                new ParameterHolder<>(TOTALSALESINPUT2, TOTALSALESLIST2),
                new ParameterHolder<>(TOTALSALESINPUT3, TOTALSALESLIST3),
                new ParameterHolder<>(TOTALSALESINPUT4, TOTALSALESLIST4),
                new ParameterHolder<>(TOTALSALESINPUT5, TOTALSALESLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getAllSalesByAuthorRule() throws ParseException {
        //1 - author_id 88, 2000-01-01-->2020-01-01 // 12 results
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT1.clear();
        TOTALSALESINPUT1.put(88, DATERANGE1);

        AllSalesQueryBean b1 = new AllSalesQueryBean(
                new BigDecimal(19.47),
                SDF.parse("2010-03-07"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b2 = new AllSalesQueryBean(
                new BigDecimal(18.95),
                SDF.parse("2018-06-03"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b3 = new AllSalesQueryBean(
                new BigDecimal(21.66),
                SDF.parse("2019-09-03"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b4 = new AllSalesQueryBean(
                new BigDecimal(18.78),
                SDF.parse("2017-04-20"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b5 = new AllSalesQueryBean(
                new BigDecimal(8.00),
                SDF.parse("2011-12-16"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b6 = new AllSalesQueryBean(
                new BigDecimal(8.00),
                SDF.parse("2014-06-25"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b7 = new AllSalesQueryBean(
                new BigDecimal(28.89),
                SDF.parse("2000-11-04"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b8 = new AllSalesQueryBean(
                new BigDecimal(15.94),
                SDF.parse("2011-12-05"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b9 = new AllSalesQueryBean(
                new BigDecimal(28.54),
                SDF.parse("2017-01-12"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b10 = new AllSalesQueryBean(
                new BigDecimal(8.00),
                SDF.parse("2016-08-01"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b11 = new AllSalesQueryBean(
                new BigDecimal(25.00),
                SDF.parse("2003-05-20"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b12 = new AllSalesQueryBean(
                new BigDecimal(19.47),
                SDF.parse("2019-09-20"),
                9780062941008L,
                "Jonathan Van Ness",
                "Peanut Goes for the Gold"
        );

        ALLSALESLIST1.clear();
        ALLSALESLIST1.add(b1);
        ALLSALESLIST1.add(b2);
        ALLSALESLIST1.add(b3);
        ALLSALESLIST1.add(b4);
        ALLSALESLIST1.add(b5);
        ALLSALESLIST1.add(b6);
        ALLSALESLIST1.add(b7);
        ALLSALESLIST1.add(b8);
        ALLSALESLIST1.add(b9);
        ALLSALESLIST1.add(b10);
        ALLSALESLIST1.add(b11);
        ALLSALESLIST1.add(b12);

        //2 - author_id 33, 2000-01-01-->2020-01-01 // 3 results
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT2.clear();
        TOTALSALESINPUT2.put(33, DATERANGE2);

        AllSalesQueryBean b13 = new AllSalesQueryBean(
                new BigDecimal(24.50),
                SDF.parse("2015-03-05"),
                9780375832291L,
                "Jennifer L. Holm",
                "Babymouse #1: Queen of the World!"
        );

        AllSalesQueryBean b14 = new AllSalesQueryBean(
                new BigDecimal(29.51),
                SDF.parse("2016-11-06"),
                9780375832291L,
                "Jennifer L. Holm",
                "Babymouse #1: Queen of the World!"
        );

        AllSalesQueryBean b15 = new AllSalesQueryBean(
                new BigDecimal(31.70),
                SDF.parse("2018-06-07"),
                9780375832291L,
                "Jennifer L. Holm",
                "Babymouse #1: Queen of the World!"
        );

        AllSalesQueryBean surprisenewdatabean = new AllSalesQueryBean(
                new BigDecimal(11.00),
                SDF.parse("2014-10-05"),
                9780763693558L,
                "Jennifer L. Holm",
                "Alma and How She Got Her Name"
        );

        AllSalesQueryBean surprisenewdatabean2 = new AllSalesQueryBean(
                new BigDecimal(26.77),
                SDF.parse("2010-03-06"),
                9780810984226L,
                "Jennifer L. Holm",
                "How Mirka Got Her Sword (Hereville Book 1)"
        );

        AllSalesQueryBean surprisenewdatabean3 = new AllSalesQueryBean(
                new BigDecimal(25.94),
                SDF.parse("2009-08-25"),
                9780606267380L,
                "Jennifer L. Holm",
                "Drama"
        );

        ALLSALESLIST2.clear();
        ALLSALESLIST2.add(b13);
        ALLSALESLIST2.add(b14);
        ALLSALESLIST2.add(b15);
        ALLSALESLIST2.add(surprisenewdatabean);
        ALLSALESLIST2.add(surprisenewdatabean2);
        ALLSALESLIST2.add(surprisenewdatabean3);

        //3 - author_id 17, 2000-01-01 --> 2020-01-01 // 1 result
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-01-01"));
        DATERANGE3.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT3.clear();
        TOTALSALESINPUT3.put(17, DATERANGE3);

        ALLSALESLIST3.clear();
        ALLSALESLIST3.add(
                new AllSalesQueryBean(
                        new BigDecimal(26.77),
                        SDF.parse("2010-03-06"),
                        9780810984226L,
                        "Barry Deutsch",
                        "How Mirka Got Her Sword (Hereville Book 1)"
                )
        );

        //4 - author_id 65, 2000-01-01 --> 2020-01-01 // 2 results
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2000-01-01"));
        DATERANGE4.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT4.clear();
        TOTALSALESINPUT4.put(65, DATERANGE4);

        AllSalesQueryBean b16 = new AllSalesQueryBean(
                new BigDecimal(24.58),
                SDF.parse("2019-05-27"),
                9780385320436L,
                "John O'Brien",
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        AllSalesQueryBean b17 = new AllSalesQueryBean(
                new BigDecimal(20.66),
                SDF.parse("2003-05-20"),
                9780385320436L,
                "John O'Brien",
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        ALLSALESLIST4.clear();
        ALLSALESLIST4.add(b16);
        ALLSALESLIST4.add(b17);

        //5 - author_id 65, 2000-01-01 --> 2001-01-01 // - 0 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2001-01-01"));

        TOTALSALESINPUT5.clear();
        TOTALSALESINPUT5.put(65, DATERANGE5);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(TOTALSALESINPUT1, ALLSALESLIST1),
                new ParameterHolder<>(TOTALSALESINPUT2, ALLSALESLIST2),
                new ParameterHolder<>(TOTALSALESINPUT3, ALLSALESLIST3),
                new ParameterHolder<>(TOTALSALESINPUT4, ALLSALESLIST4),
                new ParameterHolder<>(TOTALSALESINPUT5, ALLSALESLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getTotalSalesByPublisherRule() throws ParseException {
        //1 - publisher_id 28, 2000-01-01 --> 2020-01-01 // 300.14, HarperCollins
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT1.clear();
        TOTALSALESINPUT1.put(28, DATERANGE1);

        TOTALSALESLIST1.add(
                new TotalSalesQueryBean(
                        new BigDecimal(300.14),
                        "HarperCollins"
                )
        );

        //2 - publisher_id 28, 2015-01-01 --> 2020-01-01 // 194.84, HarperCollins
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2015-01-01"));
        DATERANGE2.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT2.clear();
        TOTALSALESINPUT2.put(28, DATERANGE2);

        TOTALSALESLIST2.clear();
        TOTALSALESLIST2.add(
                new TotalSalesQueryBean(
                        new BigDecimal(194.84),
                        "HarperCollins"
                )
        );

        //3 - publisher_id 42, 2015-01-01 --> 2020-01-01 // 85.71, Random House
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2015-01-01"));
        DATERANGE3.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT3.clear();
        TOTALSALESINPUT3.put(42, DATERANGE3);

        TOTALSALESLIST3.clear();
        TOTALSALESLIST3.add(
                new TotalSalesQueryBean(
                        new BigDecimal(85.71),
                        "Random House"
                )
        );

        //4 - publisher_id 40, 2000-01-01 --> 2020-01-01 // 122.63, Amulet Books
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2000-01-01"));
        DATERANGE4.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT4.clear();
        TOTALSALESINPUT4.put(40, DATERANGE4);

        TOTALSALESLIST4.clear();
        TOTALSALESLIST4.add(
                new TotalSalesQueryBean(
                        new BigDecimal(122.63),
                        "Amulet Books"
                )
        );

        //5 - publisher_id 40, 2000-01-01 --> 2001-01-01 // 0.00, Amulet Books
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2001-01-01"));

        TOTALSALESINPUT5.clear();
        TOTALSALESINPUT5.put(40, DATERANGE5);

        TOTALSALESLIST5.clear();
        TOTALSALESLIST5.add(
                new TotalSalesQueryBean(
                        new BigDecimal(0.00),
                        "Amulet Books"
                )
        );

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(TOTALSALESINPUT1, TOTALSALESLIST1),
                new ParameterHolder<>(TOTALSALESINPUT2, TOTALSALESLIST2),
                new ParameterHolder<>(TOTALSALESINPUT3, TOTALSALESLIST3),
                new ParameterHolder<>(TOTALSALESINPUT4, TOTALSALESLIST4),
                new ParameterHolder<>(TOTALSALESINPUT5, TOTALSALESLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getAllSalesByPublisherRule() throws ParseException {
        //1 - publisher_id 40, 2000-01-01 --> 2020-01-01 // 6 results
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT1.clear();
        TOTALSALESINPUT1.put(40, DATERANGE1);

        AllSalesQueryBean b1 = new AllSalesQueryBean(
                new BigDecimal(25.94),
                SDF.parse("2009-08-25"),
                9780606267380L,
                "Amulet Books",
                "Drama"
        );

        AllSalesQueryBean b2 = new AllSalesQueryBean(
                new BigDecimal(26.01),
                SDF.parse("2019-04-14"),
                9780692848388L,
                "Amulet Books",
                "What Should Danny Do?"
        );

        AllSalesQueryBean b3 = new AllSalesQueryBean(
                new BigDecimal(7.45),
                SDF.parse("2012-06-21"),
                9780692848388L,
                "Amulet Books",
                "What Should Danny Do?"
        );

        AllSalesQueryBean b4 = new AllSalesQueryBean(
                new BigDecimal(25.46),
                SDF.parse("2011-12-26"),
                9780692848388L,
                "Amulet Books",
                "What Should Danny Do?"
        );

        AllSalesQueryBean b5 = new AllSalesQueryBean(
                new BigDecimal(11.00),
                SDF.parse("2014-10-05"),
                9780763693558L,
                "Amulet Books",
                "Alma and How She Got Her Name"
        );

        AllSalesQueryBean b6 = new AllSalesQueryBean(
                new BigDecimal(26.77),
                SDF.parse("2010-03-06"),
                9780810984226L,
                "Amulet Books",
                "How Mirka Got Her Sword (Hereville Book 1)"
        );

        ALLSALESLIST1.clear();
        ALLSALESLIST1.add(b1);
        ALLSALESLIST1.add(b2);
        ALLSALESLIST1.add(b3);
        ALLSALESLIST1.add(b4);
        ALLSALESLIST1.add(b5);
        ALLSALESLIST1.add(b6);

        //2 - publisher_id 40, 2015-01-01 --> 2020-01-01 // 1 result
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2015-01-01"));
        DATERANGE2.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT2.clear();
        TOTALSALESINPUT2.put(40, DATERANGE2);

        ALLSALESLIST2.clear();
        ALLSALESLIST2.add(
                new AllSalesQueryBean(
                        new BigDecimal(26.01),
                        SDF.parse("2019-04-14"),
                        9780692848388L,
                        "Amulet Books",
                        "What Should Danny Do?"
                )
        );

        //3 - publisher_id 47, 2000-01-01 --> 2020-01-01 // 2 results
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-01-01"));
        DATERANGE3.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT3.clear();
        TOTALSALESINPUT3.put(47, DATERANGE3);

        AllSalesQueryBean b7 = new AllSalesQueryBean(
                new BigDecimal(24.58),
                SDF.parse("2019-05-27"),
                9780385320436L,
                "Three Rivers Press",
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        AllSalesQueryBean b8 = new AllSalesQueryBean(
                new BigDecimal(20.66),
                SDF.parse("2003-05-20"),
                9780385320436L,
                "Three Rivers Press",
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"
        );

        ALLSALESLIST3.clear();
        ALLSALESLIST3.add(b7);
        ALLSALESLIST3.add(b8);

        //4 - publisher_id 28, 2015-01-01 --> 2020-01-01 // 9 results
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2015-01-01"));
        DATERANGE4.add(SDF.parse("2020-01-01"));

        TOTALSALESINPUT4.clear();
        TOTALSALESINPUT4.put(28, DATERANGE4);

        AllSalesQueryBean b9 = new AllSalesQueryBean(
                new BigDecimal(18.95),
                SDF.parse("2018-06-03"),
                9780062941008L,
                "HarperCollins",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b10 = new AllSalesQueryBean(
                new BigDecimal(21.66),
                SDF.parse("2019-09-03"),
                9780062941008L,
                "HarperCollins",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b11 = new AllSalesQueryBean(
                new BigDecimal(18.78),
                SDF.parse("2017-04-20"),
                9780062941008L,
                "HarperCollins",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b12 = new AllSalesQueryBean(
                new BigDecimal(28.54),
                SDF.parse("2017-01-12"),
                9780062941008L,
                "HarperCollins",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b13 = new AllSalesQueryBean(
                new BigDecimal(8.00),
                SDF.parse("2016-08-01"),
                9780062941008L,
                "HarperCollins",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b14 = new AllSalesQueryBean(
                new BigDecimal(19.47),
                SDF.parse("2019-09-20"),
                9780062941008L,
                "HarperCollins",
                "Peanut Goes for the Gold"
        );

        AllSalesQueryBean b15 = new AllSalesQueryBean(
                new BigDecimal(36.04),
                SDF.parse("2017-08-19"),
                9780062953414L,
                "HarperCollins",
                "The Candy Caper"
        );

        AllSalesQueryBean b16 = new AllSalesQueryBean(
                new BigDecimal(21.70),
                SDF.parse("2019-07-18"),
                9780062953452L,
                "HarperCollins",
                "Busted by Breakfast"
        );

        AllSalesQueryBean b17 = new AllSalesQueryBean(
                new BigDecimal(21.70),
                SDF.parse("2018-06-07"),
                9780062953452L,
                "HarperCollins",
                "Busted by Breakfast"
        );

        ALLSALESLIST4.clear();
        ALLSALESLIST4.add(b9);
        ALLSALESLIST4.add(b10);
        ALLSALESLIST4.add(b11);
        ALLSALESLIST4.add(b12);
        ALLSALESLIST4.add(b13);
        ALLSALESLIST4.add(b14);
        ALLSALESLIST4.add(b15);
        ALLSALESLIST4.add(b16);
        ALLSALESLIST4.add(b17);

        //5 - publisher_id 28, 2000-01-01 --> 2000-01-03 // 0 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2000-01-03"));

        TOTALSALESINPUT5.clear();
        TOTALSALESINPUT5.put(28, DATERANGE5);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(TOTALSALESINPUT1, ALLSALESLIST1),
                new ParameterHolder<>(TOTALSALESINPUT2, ALLSALESLIST2),
                new ParameterHolder<>(TOTALSALESINPUT3, ALLSALESLIST3),
                new ParameterHolder<>(TOTALSALESINPUT4, ALLSALESLIST4),
                new ParameterHolder<>(TOTALSALESINPUT5, ALLSALESLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getOrderIdsBetweenTwoDatesRule() throws ParseException {
        //1 - 2000-01-01 --> 2000-12-30 // 1 result
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2000-12-30"));

        ORDERIDLIST1.clear();
        ORDERIDLIST1.add(7);

        //2 - 2000-01-01 --> 2003-12-30 // 3 results
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2003-12-30"));

        ORDERIDLIST2.clear();
        ORDERIDLIST2.add(7);
        ORDERIDLIST2.add(52);
        ORDERIDLIST2.add(63);

        //3 - 2000-01-01 --> 2010-12-30 // 8 results
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-01-01"));
        DATERANGE3.add(SDF.parse("2010-12-30"));

        ORDERIDLIST3.clear();
        ORDERIDLIST3.add(1);
        ORDERIDLIST3.add(7);
        ORDERIDLIST3.add(35);
        ORDERIDLIST3.add(52);
        ORDERIDLIST3.add(53);
        ORDERIDLIST3.add(63);
        ORDERIDLIST3.add(90);
        ORDERIDLIST3.add(95);

        //4 - 2000-01-01 --> 2020-12-30 // 34 results
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2000-01-01"));
        DATERANGE4.add(SDF.parse("2020-12-30"));

        ORDERIDLIST4.clear();
        ORDERIDLIST4.add(1);
        ORDERIDLIST4.add(2);
        ORDERIDLIST4.add(3);
        ORDERIDLIST4.add(4);
        ORDERIDLIST4.add(5);
        ORDERIDLIST4.add(6);
        ORDERIDLIST4.add(7);
        ORDERIDLIST4.add(8);
        ORDERIDLIST4.add(9);
        ORDERIDLIST4.add(10);
        ORDERIDLIST4.add(17);
        ORDERIDLIST4.add(18);
        ORDERIDLIST4.add(23);
        ORDERIDLIST4.add(29);
        ORDERIDLIST4.add(35);
        ORDERIDLIST4.add(36);
        ORDERIDLIST4.add(39);
        ORDERIDLIST4.add(40);
        ORDERIDLIST4.add(42);
        ORDERIDLIST4.add(43);
        ORDERIDLIST4.add(44);
        ORDERIDLIST4.add(45);
        ORDERIDLIST4.add(52);
        ORDERIDLIST4.add(53);
        ORDERIDLIST4.add(62);
        ORDERIDLIST4.add(63);
        ORDERIDLIST4.add(69);
        ORDERIDLIST4.add(88);
        ORDERIDLIST4.add(89);
        ORDERIDLIST4.add(90);
        ORDERIDLIST4.add(92);
        ORDERIDLIST4.add(93);
        ORDERIDLIST4.add(95);
        ORDERIDLIST4.add(101);

        //5 - 2000-01-01 --> 2000-01-30 // - 0 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2000-01-30"));

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(DATERANGE1, ORDERIDLIST1),
                new ParameterHolder<>(DATERANGE2, ORDERIDLIST2),
                new ParameterHolder<>(DATERANGE3, ORDERIDLIST3),
                new ParameterHolder<>(DATERANGE4, ORDERIDLIST4),
                new ParameterHolder<>(DATERANGE5, ORDERIDLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getIsbnsBetweenTwoDatesRule() throws ParseException {
        //1 - 2000-01-01 --> 2001-12-30 // 1 result
        DATERANGE1.clear();
        DATERANGE1.add(SDF.parse("2000-01-01"));
        DATERANGE1.add(SDF.parse("2001-12-30"));

        ISBNLIST1.clear();
        ISBNLIST1.add(9780062941008L);

        //2 - 2000-01-01 --> 2003-12-30 // 4 results
        DATERANGE2.clear();
        DATERANGE2.add(SDF.parse("2000-01-01"));
        DATERANGE2.add(SDF.parse("2003-12-30"));

        ISBNLIST2.clear();
        ISBNLIST2.add(9780062941008L);
        ISBNLIST2.add(9780448405179L);
        ISBNLIST2.add(9780385320436L);
        ISBNLIST2.add(9780547557991L);

        //3 - 2000-01-01 --> 2010-12-30 // 7 results
        DATERANGE3.clear();
        DATERANGE3.add(SDF.parse("2000-01-01"));
        DATERANGE3.add(SDF.parse("2010-12-30"));

        ISBNLIST3.clear();
        ISBNLIST3.add(9780062941008L);
        ISBNLIST3.add(9780399246531L);
        ISBNLIST3.add(9780448405179L);
        ISBNLIST3.add(9780385320436L);
        ISBNLIST3.add(9780547557991L);
        ISBNLIST3.add(9780810984226L);
        ISBNLIST3.add(9780606267380L);

        //4 - 2000-01-01 --> 2020-12-30 // 17 results
        DATERANGE4.clear();
        DATERANGE4.add(SDF.parse("2000-01-01"));
        DATERANGE4.add(SDF.parse("2020-12-30"));

        ISBNLIST4.clear();
        ISBNLIST4.add(9780062941008L);
        ISBNLIST4.add(9780062953452L);
        ISBNLIST4.add(9780062953414L);
        ISBNLIST4.add(9780062498564L);
        ISBNLIST4.add(9780312625993L);
        ISBNLIST4.add(9780399246531L);
        ISBNLIST4.add(9780763693558L);
        ISBNLIST4.add(9780486468211L);
        ISBNLIST4.add(9780692848388L);
        ISBNLIST4.add(9780547557991L);
        ISBNLIST4.add(9780448405179L);
        ISBNLIST4.add(9780385320436L);
        ISBNLIST4.add(9780606144841L);
        ISBNLIST4.add(9780545132060L);
        ISBNLIST4.add(9780810984226L);
        ISBNLIST4.add(9780375832291L);
        ISBNLIST4.add(9780606267380L);

        //5 - 2000-01-01 --> 2000-01-30 // 0 results
        DATERANGE5.clear();
        DATERANGE5.add(SDF.parse("2000-01-01"));
        DATERANGE5.add(SDF.parse("2000-01-30"));

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(DATERANGE1, ISBNLIST1),
                new ParameterHolder<>(DATERANGE2, ISBNLIST2),
                new ParameterHolder<>(DATERANGE3, ISBNLIST3),
                new ParameterHolder<>(DATERANGE4, ISBNLIST4),
                new ParameterHolder<>(DATERANGE5, ISBNLIST5)
        );

        return ruleSet;
    }
}
