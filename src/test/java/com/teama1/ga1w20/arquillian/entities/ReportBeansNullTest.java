package com.teama1.ga1w20.arquillian.entities;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import com.teama1.ga1w20.persistence.queries.beans.*;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test the equals(), hashCode() and toString() methods of entities, to
 * make sure they do not throw NullPointerExceptions, even when their fields are
 * null. Test do not have any asserts. They are run to make sure no NullPointer
 * exception is being thrown.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class ReportBeansNullTest {

    private final static Logger LOG = LoggerFactory.getLogger(ReportBeansNullTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    GenresJpaController genresJpaController;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void allSalesQueryBean_equals_Test() {
        AllSalesQueryBean entity1 = new AllSalesQueryBean();
        AllSalesQueryBean entity2 = new AllSalesQueryBean();
        entity1.equals(entity2);
    }

    @Test
    public void allSalesQueryBean_hashCode_Test() {
        AllSalesQueryBean entity1 = new AllSalesQueryBean();
        entity1.hashCode();
    }

    @Test
    public void allSalesQueryBean_toString_Test() {
        AllSalesQueryBean entity1 = new AllSalesQueryBean();
        entity1.toString();
    }

    @Test
    public void ordersQueryBean_equals_Test() {
        OrdersQueryBean entity1 = new OrdersQueryBean();
        OrdersQueryBean entity2 = new OrdersQueryBean();
        entity1.equals(entity2);
    }

    @Test
    public void ordersQueryBean_hashCode_Test() {
        OrdersQueryBean entity1 = new OrdersQueryBean();
        entity1.hashCode();
    }

    @Test
    public void ordersQueryBean_toString_Test() {
        OrdersQueryBean entity1 = new OrdersQueryBean();
        entity1.toString();
    }

    @Test
    public void simpleBookQueryBean_equals_Test() {
        SimpleBookQueryBean entity1 = new SimpleBookQueryBean();
        SimpleBookQueryBean entity2 = new SimpleBookQueryBean();
        entity1.equals(entity2);
    }

    @Test
    public void simpleBookQueryBean_hashCode_Test() {
        SimpleBookQueryBean entity1 = new SimpleBookQueryBean();
        entity1.hashCode();
    }

    @Test
    public void simpleBookQueryBean_toString_Test() {
        SimpleBookQueryBean entity1 = new SimpleBookQueryBean();
        entity1.toString();
    }

    @Test
    public void topQueryBean_equals_Test() {
        TopQueryBean entity1 = new TopQueryBean();
        TopQueryBean entity2 = new TopQueryBean();
        entity1.equals(entity2);
    }

    @Test
    public void topQueryBean_hashCode_Test() {
        TopQueryBean entity1 = new TopQueryBean();
        entity1.hashCode();
    }

    @Test
    public void topQueryBean_toString_Test() {
        TopQueryBean entity1 = new TopQueryBean();
        entity1.toString();
    }

    @Test
    public void totalSalesQueryBean_equals_Test() {
        TotalSalesQueryBean entity1 = new TotalSalesQueryBean();
        TotalSalesQueryBean entity2 = new TotalSalesQueryBean();
        entity1.equals(entity2);
    }

    @Test
    public void totalSalesQueryBean_hashCode_Test() {
        TotalSalesQueryBean entity1 = new TotalSalesQueryBean();
        entity1.hashCode();
    }

    @Test
    public void totalSalesQueryBean_toString_Test() {
        TotalSalesQueryBean entity1 = new TotalSalesQueryBean();
        entity1.toString();
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(AllSalesQueryBean.class.getPackage());
    }
}
