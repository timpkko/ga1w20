package com.teama1.ga1w20.arquillian.entities;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.*;
import java.util.List;
import javax.annotation.Resource;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test the equals(), hashCode() and toString() methods of entities, to
 * make sure they do not throw NullPointerExceptions, even when their fields are
 * null. The tests do not have any asserts. They are ran to make sure no
 * NullPointer exception is being thrown.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class PersistenceEntitiesNullTest {

    private final static Logger LOG = LoggerFactory.getLogger(PersistenceEntitiesNullTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////  
    //Dynamic field which is populated according to case, representing the expected value(s)
    //In this case, only the input field is used 
    // which holds two objects of the same type to test against.
    private ParameterHolder<Object[], List<Object>> dynamicParameterHolder;

    //Arrays of empty entities for each test case
    Authors[] emptyAuthors = new Authors[]{new Authors(), new Authors()};
    Banners[] emptyBanners = new Banners[]{new Banners(), new Banners()};
    Books[] emptyBooks = new Books[]{new Books(), new Books()};
    Credentials[] emptyCredentials = new Credentials[]{new Credentials(), new Credentials()};
    Formats[] emptyFormats = new Formats[]{new Formats(), new Formats()};
    Genres[] emptyGenres = new Genres[]{new Genres(), new Genres()};
    Orderitems[] emptyOrderitems = new Orderitems[]{new Orderitems(), new Orderitems()};
    Orders[] emptyOrders = new Orders[]{new Orders(), new Orders()};
    Publishers[] emptyPublishers = new Publishers[]{new Publishers(), new Publishers()};
    Reviews[] emptyReviews = new Reviews[]{new Reviews(), new Reviews()};
    Rssfeeds[] emptyRssfeeds = new Rssfeeds[]{new Rssfeeds(), new Rssfeeds()};
    Surveys[] emptySurveys = new Surveys[]{new Surveys(), new Surveys()};
    Taxrates[] emptyTaxrates = new Taxrates[]{new Taxrates(), new Taxrates()};
    Users[] emptyUsers = new Users[]{new Users(), new Users()};

    //RuleSet containing testCases with empty entities
    @Rule
    public ParameterRule testCases = new ParameterRule("dynamicParameterHolder",
            new ParameterHolder<Object[], List<Object>>(emptyAuthors, null),
            new ParameterHolder<Object[], List<Object>>(emptyBanners, null),
            new ParameterHolder<Object[], List<Object>>(emptyBooks, null),
            new ParameterHolder<Object[], List<Object>>(emptyCredentials, null),
            new ParameterHolder<Object[], List<Object>>(emptyFormats, null),
            new ParameterHolder<Object[], List<Object>>(emptyGenres, null),
            new ParameterHolder<Object[], List<Object>>(emptyOrderitems, null),
            new ParameterHolder<Object[], List<Object>>(emptyOrders, null),
            new ParameterHolder<Object[], List<Object>>(emptyPublishers, null),
            new ParameterHolder<Object[], List<Object>>(emptyReviews, null),
            new ParameterHolder<Object[], List<Object>>(emptyRssfeeds, null),
            new ParameterHolder<Object[], List<Object>>(emptySurveys, null),
            new ParameterHolder<Object[], List<Object>>(emptyTaxrates, null),
            new ParameterHolder<Object[], List<Object>>(emptyUsers, null)
    );

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void equals_Test() {
        Object entity1 = dynamicParameterHolder.getInput()[0];
        Object entity2 = dynamicParameterHolder.getInput()[1];
        entity1.equals(entity2);
    }

    @Test
    public void hashCode_Test() {
        Object entity = dynamicParameterHolder.getInput()[0];
        entity.hashCode();
    }

    @Test
    public void toString_Test() {
        Object entity = dynamicParameterHolder.getInput()[0];
        entity.toString();
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
