package com.teama1.ga1w20.arquillian.jpacontrollers.taxrates;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Taxrates;
import com.teama1.ga1w20.persistence.jpacontrollers.TaxratesJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class TaxratesByRegionTest {

    private final static Logger LOG = LoggerFactory.getLogger(TaxratesByRegionTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //JPA Controller
    @Inject
    private TaxratesJpaController taxratesController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Ruleset populator
    private TaxratesRulePopulator rulePopulator;

    @Rule
    public ParameterRule ratesByRegion;
    private ParameterHolder<String, List<Taxrates>> dynamic;
    private ParameterHolder<String, List<Taxrates>> resultHolder;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    public TaxratesByRegionTest() {
        rulePopulator = new TaxratesRulePopulator();
        ratesByRegion = rulePopulator.getTaxratesByLocaleRule();
    }

    /**
     * Ensures that the correct Taxrates objects are returned based on a String
     * representing the region.
     */
    @Test
    public void testRatesByRegion() {
        ArrayList<Taxrates> list = new ArrayList<>();
        try {
            list.clear();
            list.add(taxratesController.getRateByRegion(dynamic.getInput()));
            resultHolder = new ParameterHolder<>(dynamic.getInput(), list);
        } catch (NonexistentEntityException | NoResultException | NonUniqueResultException ex) {
            LOG.error("Something went funky: ", ex);
        }

        assertEquals(dynamic.getListOutput().get(0), resultHolder.getListOutput().get(0));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {

        return ArchiveUtils.getWebArchive()
                .addPackage(TaxratesRulePopulator.class.getPackage());
    }
}
