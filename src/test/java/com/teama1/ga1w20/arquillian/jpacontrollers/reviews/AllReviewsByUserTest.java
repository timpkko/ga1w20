package com.teama1.ga1w20.arquillian.jpacontrollers.reviews;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.ReviewsJpaController;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class AllReviewsByUserTest {

    private final static Logger LOG = LoggerFactory.getLogger(AllReviewsByUserTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Users, List<Reviews>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<Reviews> actualOutputList;

    @Inject
    private ReviewsJpaController controller;

    private ReviewsRulePopulator populator;

    public AllReviewsByUserTest() {
        populator = new ReviewsRulePopulator();
        try {
            testCases = populator.getAllReviewsByUserRule();
        } catch (ParseException ex) {
            LOG.error("Whoopsies! " + Arrays.toString(ex.getStackTrace()));
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void testApprovedReviewsByUser() {
        actualOutputList = controller.getAllReviewsByUser(dynamicParameterHolder.getInput());

        assertTrue("expected: " + dynamicParameterHolder.getListOutput().toString() + "actual: " + actualOutputList.toString(), actualOutputList.containsAll(dynamicParameterHolder.getListOutput()) && dynamicParameterHolder.getListOutput().containsAll(actualOutputList));
    }

    @Test
    public void testApprovedReviewsByUserId() {
        actualOutputList = controller.getAllReviewsByUser(dynamicParameterHolder.getInput().getUserId());

        assertTrue("expected: " + dynamicParameterHolder.getListOutput().toString() + "actual: " + actualOutputList.toString(), actualOutputList.containsAll(dynamicParameterHolder.getListOutput()) && dynamicParameterHolder.getListOutput().containsAll(actualOutputList));
    }

    @Test
    public void testNumApprovedReviewsByUser() {
        actualOutputList = controller.getAllReviewsByUser(dynamicParameterHolder.getInput());

        assertEquals(dynamicParameterHolder.getListOutput().size(), actualOutputList.size());
    }

    @Test
    public void testNumApprovedReviewsByUserId() {
        actualOutputList = controller.getAllReviewsByUser(dynamicParameterHolder.getInput().getUserId());

        assertEquals(dynamicParameterHolder.getListOutput().size(), actualOutputList.size());
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(ReviewsRulePopulator.class.getPackage());
    }
}
