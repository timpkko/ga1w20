package com.teama1.ga1w20.arquillian.jpacontrollers.orderitems;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.jpacontrollers.OrderitemsJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A test class to test the getOrderitemsFromOrderId(Integer) method of
 * OrderitemsJpaController.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class GetByOrderIdParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetByOrderIdParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Jpa Controller
    @Inject
    private OrderitemsJpaController orderitemController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Integer, List<Orderitems>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<Orderitems> actualOutputList;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    OrderitemsTestUtils orderitemsTestUtils = new OrderitemsTestUtils();

    Integer input1, input2, input3, input4;

    private final List<Orderitems> resultList1 = new ArrayList<>();
    private final List<Orderitems> resultList2 = new ArrayList<>();
    private final List<Orderitems> resultList3 = new ArrayList<>();
    private final List<Orderitems> resultList4 = new ArrayList<>();

    /**
     * Constructor
     */
    public GetByOrderIdParamTest() {
        //Case : 1 result, order has another removed orderitem which should not be returned
        resultList1.clear();
        input1 = 101;
        resultList1.add(orderitemsTestUtils.createOrderitem(102, 101, 9780062953452L, 21.70, false));

        //Case : 2 results
        input2 = 69;
        resultList2.clear();
        resultList2.add(orderitemsTestUtils.createOrderitem(69, 69, 9780448405179L, 17, false));
        resultList2.add(orderitemsTestUtils.createOrderitem(70, 69, 9780062941008L, 19.47, false));

        //Case : 3 results
        input3 = 63;
        resultList3.clear();
        resultList3.add(orderitemsTestUtils.createOrderitem(63, 63, 9780385320436L, 20.66, false));
        resultList3.add(orderitemsTestUtils.createOrderitem(64, 63, 9780547557991L, 18, false));
        //resultList3.add(orderitemsTestUtils.createOrderitem(65, 63, 9780448405179L, 17, true));
        resultList3.add(orderitemsTestUtils.createOrderitem(66, 63, 9780062941008L, 25, false));

        //Case :  orderid does not exist, 0 results
        input4 = 2000;
        resultList4.clear();

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(input1, resultList1),
                new ParameterHolder<>(input2, resultList2),
                new ParameterHolder<>(input3, resultList3),
                new ParameterHolder<>(input4, resultList4)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////=
    /**
     * Test if the actual list of Orderitems found is the same as the expected
     * list of Orderitems found
     */
    @Test
    public void getOrderitemsFromOrderId_test() {
        actualOutputList = orderitemController.
                getOrderitemsFromOrderId(dynamicParameterHolder.getInput());
        List<Orderitems> expectedOutputList = dynamicParameterHolder.getListOutput();

        //Test list size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive().addPackage(GetByOrderIdIncludeParamTest.class.getPackage());
    }
}
