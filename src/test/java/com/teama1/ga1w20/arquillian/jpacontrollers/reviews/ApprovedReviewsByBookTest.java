package com.teama1.ga1w20.arquillian.jpacontrollers.reviews;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.jpacontrollers.ReviewsJpaController;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class ApprovedReviewsByBookTest {

    private final static Logger LOG = LoggerFactory.getLogger(ApprovedReviewsByBookTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //Rule storing test parameters
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the expected value(s)
    private ParameterHolder<Books, List<Reviews>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<Reviews> actualOutputList;

    private ReviewsRulePopulator rulesPopulator;

    @Inject
    private ReviewsJpaController controller;

    public ApprovedReviewsByBookTest() {
        rulesPopulator = new ReviewsRulePopulator();
        try {
            testCases = rulesPopulator.getApprovedReviewsByBookRule();
        } catch (Exception e) {
            LOG.error("Oopsies! " + Arrays.toString(e.getStackTrace()));
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void testApprovedReviewsByBookCount() {
        actualOutputList = controller.getApprovedReviewsByBook(dynamicParameterHolder.getInput());

        assertEquals(dynamicParameterHolder.getListOutput().size(), actualOutputList.size());
    }

    @Test
    public void testApprovedReviewsByIsbnCount() {
        actualOutputList = controller.getApprovedReviewsByBook(dynamicParameterHolder.getInput().getIsbn());

        assertEquals(dynamicParameterHolder.getListOutput().size(), actualOutputList.size());
    }

    @Test
    public void testApprovedReviewsByBook() {
        actualOutputList = controller.getApprovedReviewsByBook(dynamicParameterHolder.getInput());

        assertTrue("expected: " + dynamicParameterHolder.getListOutput().toString() + "actual: " + actualOutputList.toString(), actualOutputList.containsAll(dynamicParameterHolder.getListOutput()) && dynamicParameterHolder.getListOutput().containsAll(actualOutputList));
    }

    @Test
    public void testApprovedReviewsByIsbn() {
        actualOutputList = controller.getApprovedReviewsByBook(dynamicParameterHolder.getInput().getIsbn());

        assertTrue("expected: " + dynamicParameterHolder.getListOutput().toString() + "actual: " + actualOutputList.toString(), actualOutputList.containsAll(dynamicParameterHolder.getListOutput()) && dynamicParameterHolder.getListOutput().containsAll(actualOutputList));
    }

    @Test
    public void testLimitedReviewListByBook() {
        actualOutputList = controller.getApprovedReviewsByBook(dynamicParameterHolder.getInput(), 1, 1);

        assertEquals(1, actualOutputList.size());
    }

    @Test
    public void testLimitedReviewListByIsbn() {
        actualOutputList = controller.getApprovedReviewsByBook(dynamicParameterHolder.getInput().getIsbn(), 1, 1);

        assertEquals(1, actualOutputList.size());
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(ReviewsRulePopulator.class.getPackage());
    }
}
