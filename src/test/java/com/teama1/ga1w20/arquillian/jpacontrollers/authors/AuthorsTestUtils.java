package com.teama1.ga1w20.arquillian.jpacontrollers.authors;

import com.teama1.ga1w20.persistence.entities.Authors;
import java.util.ArrayList;
import java.util.List;

/**
 * Utils class holding navigationBean methods allowing, amongst other things to
 * access objects representing all the entities inserted in the database when
 * populated by the default test script.
 *
 * @see ArquillianUtils for default test script
 * @author Camillia E.
 */
public class AuthorsTestUtils {

    private List<Authors> allAuthorsList;

    public List<Authors> getAllAuthorsExpected() {
        if (allAuthorsList != null) {
            return allAuthorsList;
        }

        return createList();
    }

    public int getTotalNumOfAuthorsExpected() {
        if (allAuthorsList != null) {
            return allAuthorsList.size();
        }

        return createList().size();
    }

    private List<Authors> createList() {
        allAuthorsList = new ArrayList<>();
        allAuthorsList.add(new Authors(4, "Paige", "Billin-Frye"));
        allAuthorsList.add(new Authors(17, "Barry", "Deutsch"));
        allAuthorsList.add(new Authors(30, "Larry", "Gonick"));
        allAuthorsList.add(new Authors(33, "Jennifer L.", "Holm", "Dr"));
        allAuthorsList.add(new Authors(34, "Matthew", "Holm"));
        allAuthorsList.add(new Authors(36, "Steve", "Jenkins"));
        allAuthorsList.add(new Authors(37, "Charlotte Foltz", "Jones"));
        allAuthorsList.add(new Authors(39, "Kazu", "Kibuishi"));
        allAuthorsList.add(new Authors(45, "Adir", "Levy"));
        allAuthorsList.add(new Authors(46, "Ganit", "Levy"));
        allAuthorsList.add(new Authors(50, "Juana", "Martinez-Neal"));
        allAuthorsList.add(new Authors(65, "John", "O'Brien"));
        allAuthorsList.add(new Authors(72, "Mat", "Sadler"));
        allAuthorsList.add(new Authors(75, "Donald M.", "Silver"));
        allAuthorsList.add(new Authors(78, "Jeff", "Smith"));
        allAuthorsList.add(new Authors(82, "Raina", "Telgemeier"));
        allAuthorsList.add(new Authors(84, "Angie", "Thomas"));
        allAuthorsList.add(new Authors(85, "Lauren", "Thompson"));
        allAuthorsList.add(new Authors(88, "Jonathan", "Van Ness"));
        allAuthorsList.add(new Authors(89, "Tom", "Watson"));
        allAuthorsList.add(new Authors(91, "Lynn", "Wilson"));
        allAuthorsList.add(new Authors(92, "Jacqueline", "Woodson"));
        allAuthorsList.add(new Authors(93, "Patricia J.", "Wynne", "Dr"));
        allAuthorsList.add(new Authors(94, "Gene Luen", "Yang"));
        allAuthorsList.add(new Authors(200, "Hi", "Idonthaveanybooks"));
        return allAuthorsList;
    }
}
