package com.teama1.ga1w20.arquillian.jpacontrollers.orders;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.jpacontrollers.OrdersJpaController;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to test the getOrdersByDate(Date, Date) method of the
 * OrdersJpaController.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class GetOrdersBetweenDatesParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetOrdersBetweenDatesParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //Format to read date strings
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static final int DATE_ARRAY_SIZE = 2;
    private static final int STARTING_DATE_IDX = 0;
    private static final int ENDING_DATE_IDX = 1;

    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Date[], List<Orders>> dynamicParameterHolder;

    OrdersTestUtils ordersTestUtils = new OrdersTestUtils();

    Date[] input1, input2, input3, input4, input5;
    private final List<Orders> resultList1 = new ArrayList<>();
    private final List<Orders> resultList2 = new ArrayList<>();
    private final List<Orders> resultList3 = new ArrayList<>();
    private final List<Orders> resultList4 = new ArrayList<>();
    private final List<Orders> emptyResultList = new ArrayList<>();

    @Inject
    OrdersJpaController ordersJpaController;

    public GetOrdersBetweenDatesParamTest() throws ParseException {
        LOG.debug("GetOrdersByUserParamTest Constructor");
        //Case : 1 order
        input1 = new Date[DATE_ARRAY_SIZE];
        input1[STARTING_DATE_IDX] = DATE_FORMAT.parse("2003-02-06");
        input1[ENDING_DATE_IDX] = DATE_FORMAT.parse("2003-04-08");
        resultList1.clear();
        resultList1.add(ordersTestUtils.createOrders(52, 6,
                "2003-03-07"));

        //Case : 2 orders
        input2 = new Date[DATE_ARRAY_SIZE];
        input2[STARTING_DATE_IDX] = DATE_FORMAT.parse("2003-02-06");
        input2[ENDING_DATE_IDX] = DATE_FORMAT.parse("2006-04-06");
        resultList2.clear();
        resultList2.add(ordersTestUtils.createOrders(52, 6,
                "2003-03-07"));
        resultList2.add(ordersTestUtils.createOrders(63, 63,
                "2003-05-20"));

        //Case : many orders
        input3 = new Date[DATE_ARRAY_SIZE];
        input3[STARTING_DATE_IDX] = DATE_FORMAT.parse("2003-02-06");
        input3[ENDING_DATE_IDX] = DATE_FORMAT.parse("2011-03-07");
        resultList3.clear();
        resultList3.add(ordersTestUtils.createOrders(52, 6,
                "2003-03-07"));
        resultList3.add(ordersTestUtils.createOrders(63, 63,
                "2003-05-20"));
        resultList3.add(ordersTestUtils.createOrders(35, 35,
                "2006-06-08"));
        resultList3.add(ordersTestUtils.createOrders(53, 53,
                "2008-06-25"));
        resultList3.add(ordersTestUtils.createOrders(1, 1,
                "2010-03-07"));

        //Case : many orders
        input4 = new Date[DATE_ARRAY_SIZE];
        input4[STARTING_DATE_IDX] = DATE_FORMAT.parse("2019-03-07");
        input4[ENDING_DATE_IDX] = DATE_FORMAT.parse("2019-08-07");
        resultList4.clear();
        resultList4.add(ordersTestUtils.createOrders(39, 39,
                "2019-04-14"));
        resultList4.add(ordersTestUtils.createOrders(42, 42,
                "2019-04-14"));
        resultList4.add(ordersTestUtils.createOrders(40, 40,
                "2019-04-14"));
        resultList4.add(ordersTestUtils.createOrders(88, 88,
                "2019-04-29"));
        resultList4.add(ordersTestUtils.createOrders(62, 62,
                "2019-05-27"));
        resultList4.add(ordersTestUtils.createOrders(17, 17,
                "2019-07-18"));

        //Case : 0 order
        input5 = new Date[DATE_ARRAY_SIZE];
        input5[STARTING_DATE_IDX] = DATE_FORMAT.parse("1970-03-07");
        input5[ENDING_DATE_IDX] = DATE_FORMAT.parse("1998-03-07");

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(input1, resultList1),
                new ParameterHolder<>(input2, resultList2),
                new ParameterHolder<>(input3, resultList3),
                new ParameterHolder<>(input4, resultList4),
                new ParameterHolder<>(input5, emptyResultList)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getOrdersBetweenDates_test() {
        List<Orders> expectedOutputList = dynamicParameterHolder.getListOutput();
        List<Orders> actualOutputList = ordersJpaController
                .getOrdersByDate(dynamicParameterHolder.getInput()[STARTING_DATE_IDX],
                        dynamicParameterHolder.getInput()[ENDING_DATE_IDX]);

        //Test size and content of returned list
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeTestScript(OrdersTestUtils.ORDER_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(OrdersTestUtils.class.getPackage());
    }
}
