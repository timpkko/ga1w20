package com.teama1.ga1w20.arquillian.jpacontrollers.books.byid;

import com.teama1.ga1w20.arquillian.jpacontrollers.books.BooksTestUtils;
import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to test the getBookByIsbn(Long) method of the BooksJpaController.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class GetByIsbnParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetByIsbnParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Long, List<Books>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<Books> actualOutputList;

    BooksTestUtils booksTestUtils = new BooksTestUtils();

    Long input1, input2, input3, input4, input5;
    private final List<Books> resultList1 = new ArrayList<Books>();
    private final List<Books> resultList2 = new ArrayList<Books>();
    private final List<Books> resultList3 = new ArrayList<Books>();
    private final List<Books> resultList4 = new ArrayList<Books>();
    private final List<Books> resultList5 = new ArrayList<Books>();

    @Inject
    BooksJpaController booksJpaController;

    public GetByIsbnParamTest() {

        //Valid isbn for existent book -> 1 book
        input1 = 9780062498564L;
        resultList1.clear();
        resultList1.add(booksTestUtils.createTestBook(9780062498564L,
                "On the Come Up", 8));

        //Valid isbn for existent book with null field in maxAge -> 1 book
        input2 = 9780763693558L;
        resultList2.clear();
        resultList2.add(booksTestUtils.createTestBook(9780763693558L,
                "Alma and How She Got Her Name", 40));

        //Valid isbn for existent book marked as removed -> 1 book
        input3 = 9780606267380L;
        resultList3.clear();
        resultList3.add(booksTestUtils.createTestBook(9780606267380L,
                "Drama", 40));

        //Valid isbn for existent book -> 1 book
        input4 = 9780810984226L;
        resultList4.clear();
        resultList4.add(booksTestUtils.createTestBook(9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)", 40));

        //Valid isbn for existent book -> 1 book
        input5 = 9780545132060L;
        resultList5.clear();
        resultList5.add(booksTestUtils.createTestBook(9780545132060L,
                "Smile", 39));

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<Long, List<Books>>(input1, resultList1),
                new ParameterHolder<Long, List<Books>>(input2, resultList2),
                new ParameterHolder<Long, List<Books>>(input3, resultList3),
                new ParameterHolder<Long, List<Books>>(input4, resultList4),
                new ParameterHolder<Long, List<Books>>(input5, resultList5)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getBooksByIsbn_Test() {
        Books expectedBook = dynamicParameterHolder.getListOutput().get(0);
        Books actualBook = booksJpaController.getBookByIsbn(dynamicParameterHolder.getInput());
        assertEquals(expectedBook, actualBook);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeTestScript(BooksTestUtils.BOOK_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(GetByAuthorParamTest.class.getPackage())
                .addPackage(BooksTestUtils.class.getPackage());
    }
}
