package com.teama1.ga1w20.arquillian.jpacontrollers.books;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testing BooksJpaController getMostRecentlyAdded()
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class GetMostRecentlyAddedBooksJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetMostRecentlyAddedBooksJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Utils
    private final ArquillianUtils testUtils = new ArquillianUtils();
    private final BooksTestUtils booksTestUtils = new BooksTestUtils();

    //Jpa Controller
    @Inject
    private BooksJpaController bookController;

    //Test scripts
    private final static String BOOK_SCRIPT_0 = "test-scripts/books/populateBooksTable0.sql";
    private final static String BOOK_SCRIPT_1 = "test-scripts/books/populateBooksTable1.sql";

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Populate the Books table with less than 3 books (which is the number of
     * books returned by this method) and only all marked as removed (which
     * should not be returned).
     */
    @Test
    public void getMostRecentlyAdded_onlyOneRemovedBook_test() {
        //Run script appropriate for this test method
        testUtils.executeTestScript(BOOK_SCRIPT_0, dataSource);

        //Actual
        List<Books> actualOutputList = bookController.getMostRecentlyAdded();

        //Exepected : empty list
        List<Books> expectedOutputList = new ArrayList<Books>();

        //Test size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    /**
     * Populate the Books table with less than 3 books. (which is the number of
     * books returned by this method.
     */
    @Test
    public void getMostRecentlyAdded_onlyOneBook_test() {
        //Run script appropriate for this test method
        testUtils.executeTestScript(BOOK_SCRIPT_1, dataSource);

        //Actual
        List<Books> actualOutputList = bookController.getMostRecentlyAdded();

        //Expected
        List<Books> expectedOutputList = new ArrayList<Books>();
        expectedOutputList.add(new Books(9780486468211L));

        //Test size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    /**
     * Populate the Books table with the default script, testing if it returns
     * the last 3 added books.
     */
    @Test
    public void getMostRecentlyAdded_defaultScript_test() {
        //Run script appropriate for this test method
        testUtils.executeTestScript(BooksTestUtils.BOOK_TESTS_SCRIPT, dataSource);

        //Actual
        List<Books> actualOutputList = bookController.getMostRecentlyAdded();

        //Expected : The last 3 books to be added in db
        List<Books> expectedOutputList = new ArrayList<Books>();
        expectedOutputList.add(new Books(9780692848388L));
        expectedOutputList.add(new Books(9780763693558L));
        expectedOutputList.add(new Books(9780810984226L));

        //Test size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeCreateTablesTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        final WebArchive webArchive = ArchiveUtils.getWebArchive()
                .addAsResource(BOOK_SCRIPT_0)
                .addAsResource(BOOK_SCRIPT_1)
                .addPackage(BooksTestUtils.class.getPackage());
        return webArchive;
    }
}
