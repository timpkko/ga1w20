package com.teama1.ga1w20.arquillian.jpacontrollers.users;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Credentials;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tests public methods for the Tax Rates JPA Controller. Taxrates objects can
 * be retrieved by: - Region (String) - User
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class UsersJPATest {

    private final static Logger LOG = LoggerFactory.getLogger(UsersJPATest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Controller
    @Inject
    public UsersJpaController controller;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test(expected = NoResultException.class)
    public void testInvalidInputUsersByCredentials() throws NonexistentEntityException, NoResultException, NonUniqueResultException {
        Credentials creds = new Credentials();

        controller.getUserByCredentials(creds);
    }

    @Test(expected = NoResultException.class)
    public void testInvalidInputUsersByReview() throws NonexistentEntityException, NoResultException, NonUniqueResultException {
        Reviews review = new Reviews();

        controller.getUserByReview(review);
    }

    @Test(expected = NullPointerException.class)
    public void testNullInputUsersByCredentials() throws NonexistentEntityException, NoResultException, NonUniqueResultException {
        Credentials cred = null;

        controller.getUserByCredentials(cred);
    }

    @Test(expected = NullPointerException.class)
    public void testNullInputUsersByReview() throws NonexistentEntityException, NoResultException, NonUniqueResultException {
        Reviews review = null;

        controller.getUserByReview(review);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(UsersRulePopulator.class.getPackage());
    }
}
