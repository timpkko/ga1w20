package com.teama1.ga1w20.arquillian.jpacontrollers.books.byid;

import com.teama1.ga1w20.arquillian.jpacontrollers.books.BooksTestUtils;
import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to test the getBookByAuthor(Author) method of the
 * BooksJpaController.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class GetByAuthorParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetByAuthorParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Authors, List<Books>> dynamicParameterHolder;

    BooksTestUtils booksTestUtils = new BooksTestUtils();

    Authors input1, input2, input3, input4, input5;
    private final List<Books> resultList1 = new ArrayList<Books>();
    private final List<Books> resultList2 = new ArrayList<Books>();
    private final List<Books> resultList3 = new ArrayList<Books>();
    private final List<Books> emptyResultList = new ArrayList<Books>();

    @Inject
    BooksJpaController booksJpaController;

    public GetByAuthorParamTest() {

        //Case 1 -> 1 book
        input1 = new Authors(88);
        resultList1.clear();
        resultList1.add(booksTestUtils.createTestBook(9780062941008L,
                "Peanut Goes for the Gold", 28));

        //Case 2 -> 2 books
        input2 = new Authors(89);
        resultList2.clear();
        resultList2.add(booksTestUtils.createTestBook(9780062953414L,
                "The Candy Caper",
                28));
        resultList2.add(booksTestUtils.createTestBook(9780062953452L,
                "Busted by Breakfast",
                28));

        //Case 3 -> 3 books, (this author has 4 but one marked as removed)
        input3 = new Authors(33);
        resultList3.clear();
        resultList3.add(booksTestUtils.createTestBook(9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)", 40));
        resultList3.add(booksTestUtils.createTestBook(9780763693558L,
                "Alma and How She Got Her Name", 40));
        resultList3.add(booksTestUtils.createTestBook(9780375832291L,
                "Babymouse #1: Queen of the World!",
                42));

        //Case 4 -> not existent author, no books
        input4 = new Authors(300);

        //Case 5 -> existent author with no associated books
        input5 = new Authors(200);

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(input1, resultList1),
                new ParameterHolder<>(input2, resultList2),
                new ParameterHolder<>(input3, resultList3),
                new ParameterHolder<>(input4, emptyResultList),
                new ParameterHolder<>(input5, emptyResultList)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getByAuthorObject_test() {
        List<Books> expectedOutputList = dynamicParameterHolder.getListOutput();
        List<Books> actualOutputList = booksJpaController.getBooksByAuthor(dynamicParameterHolder.getInput());

        //Test for list content and size
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeTestScript(BooksTestUtils.BOOK_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(BooksTestUtils.class.getPackage())
                .addPackage(GetByAuthorParamTest.class.getPackage());
    }
}
