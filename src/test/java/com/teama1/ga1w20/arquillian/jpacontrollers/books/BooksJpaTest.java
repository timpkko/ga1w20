package com.teama1.ga1w20.arquillian.jpacontrollers.books;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.*;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test BooksJpaController -getBooksCount() -findBooksEntities()
 * -getBooksOnSale()
 *
 * And to test exception throwing of all custom queries in BooksJpaController.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class BooksJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(BooksJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    BooksJpaController booksJpaController;

    BooksTestUtils booksTestUtils = new BooksTestUtils();

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Test getBooksCount()
     */
    @Test
    public void getCountTest() {
        int actualCount = booksJpaController.getBooksCount();
        int expectedCount = booksTestUtils.getTotalNumOfBooksExpected();
        Assert.assertEquals(expectedCount, actualCount);
    }

    /**
     * Test findBooksEntities()
     */
    @Test
    public void findEntitiesTest() {
        List<Books> expectedOutputList = booksTestUtils.getAllBooksExpected();
        List<Books> actualOutputList = booksJpaController.findBooksEntities();
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    /*
    Test getBooksOnSale()
        Books that SHOULD NOT be returned by getBooksOnSale():
            9780486468211: On sale but marked as removed 
            9780062498564: Negative sale price
            9780062941008 : Sale price equal to list price
            9780062953414: Sale price just beneath wholesale price
            9780062953452 : Sale price higher than list price
            Few books: Sale price of 0

        Books that SHOULD be returned by getBooksOnSale():
            9780312625993: Sale price just beneath list price
            9780375832291: Sale price equal to wholesale price
            Few books: Sale price between wholesale price and list price
     */
    @Test
    public void getBooksOnSale_Test() {
        List<Books> expectedOutputList = booksTestUtils.getExpectedBooksOnSale();
        List<Books> actualOutputList = booksJpaController.getBooksOnSale();

        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS - EXCEPTION THROWING
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Test getBookByIsbn(Long isbn)
     */
    @Test(expected = NoResultException.class)
    public void getByIsbn_NoResultTest_1() {
        //Should not return the book with isbn 9780062498564L
        Long input = 978006249856L;
        Books book = booksJpaController.getBookByIsbn(input);
        fail("The expected exception was not thrown");
    }

    /**
     * Test getBookByIsbn(Long isbn)
     */
    @Test(expected = NoResultException.class)
    public void getByIsbn_NoResultTest_2() {
        //Should not return the book with isbn 9780062498564L
        Long input = 978006249856L;
        Books book = booksJpaController.getBookByIsbn(input);
        fail("The expected exception was not thrown");
    }

    /**
     * Test getBookByIsbn(Long isbn)
     */
    @Test(expected = NoResultException.class)
    public void getByIsbn_NoResultTest_3() {
        //Should not return a book
        Long input = 30L;
        Books book = booksJpaController.getBookByIsbn(input);
        fail("The expected exception was not thrown");
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS - NULL EXCEPTION THROWING
    ////////////////////////////////////////////////////////////////////////////
    @Test(expected = NullPointerException.class)
    public void getByIsbn_NullTest() {
        booksJpaController.getBookByIsbn((Long) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByGenreObject_NullTest() {
        booksJpaController.getBooksByGenre((Genres) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByGenreHint_NullTest() {
        booksJpaController.getBooksByGenreHint((String) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByAuthorObject_NullTest() {
        booksJpaController.getBooksByAuthor((Authors) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByAuthorHint_NullTest() {
        booksJpaController.getBooksByAuthorHint((String) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByPublisherObject_NullTest() {
        booksJpaController.getBooksByPublisher((Publishers) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByPublisherHint_NullTest() {
        booksJpaController.getBooksByPublisherHint((String) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByTitleHint_NullTest() {
        booksJpaController.getBooksByTitleHint((String) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByFormat_NullTest() {
        booksJpaController.getBooksByFormat((Formats) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByOrderObject_NullTest() {
        booksJpaController.getBooksByOrder((Orders) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByOrderId_NullTest() {
        booksJpaController.getBooksByOrder((Integer) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksByDescription_NullTest() {
        booksJpaController.getBooksByDescription((String) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksBoughtByUserObject_NullTest() {
        booksJpaController.getBooksBoughtByUser((Users) null);
    }

    @Test(expected = NullPointerException.class)
    public void getBooksBoughtByUserId_NullTest() {
        booksJpaController.getBooksBoughtByUser((Integer) null);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeTestScript(BooksTestUtils.BOOK_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(BooksTestUtils.class.getPackage());
    }
}
