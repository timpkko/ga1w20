package com.teama1.ga1w20.arquillian.jpacontrollers.orders;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.OrdersJpaController;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to test the getOrdersByUser(User) and getOrdersByUser(Integer
 * userId) methods of the OrdersJpaController.
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class GetOrdersByUserParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetOrdersByUserParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Users, List<Orders>> dynamicParameterHolder;

    OrdersTestUtils ordersTestUtils = new OrdersTestUtils();

    Users input1, input2, input3, input4, input5;
    private final List<Orders> resultList1 = new ArrayList<>();
    private final List<Orders> resultList2 = new ArrayList<>();
    private final List<Orders> resultList3 = new ArrayList<>();
    private final List<Orders> emptyResultList = new ArrayList<>();

    @Inject
    OrdersJpaController ordersJpaController;

    public GetOrdersByUserParamTest() throws ParseException {
        //Case : 1 order
        input1 = new Users(1);
        resultList1.clear();
        resultList1.add(ordersTestUtils.createOrders(1, 1,
                "2010-03-07"));

        //Case : 2 orders
        input2 = new Users(4);
        resultList2.clear();
        resultList2.add(ordersTestUtils.createOrders(4, 4,
                "2017-04-20"));
        resultList2.add(ordersTestUtils.createOrders(5, 4,
                "2011-12-16"));

        //Case : many orders
        input3 = new Users(6);
        resultList3.clear();
        resultList3.add(ordersTestUtils.createOrders(6, 6,
                "2014-06-25"));
        resultList3.add(ordersTestUtils.createOrders(23, 6,
                "2012-05-15"));
        resultList3.add(ordersTestUtils.createOrders(36, 6,
                "2014-10-05"));
        resultList3.add(ordersTestUtils.createOrders(52, 6,
                "2003-03-07"));
        resultList3.add(ordersTestUtils.createOrders(95, 6,
                "2016-11-06"));

        //Case -> not existent user, no orders
        input4 = new Users(200);

        //Case -> existent user with no associated orders
        input5 = new Users(89);

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(input1, resultList1),
                new ParameterHolder<>(input2, resultList2),
                new ParameterHolder<>(input3, resultList3),
                new ParameterHolder<>(input4, emptyResultList),
                new ParameterHolder<>(input5, emptyResultList)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getOrdersByUserObject_test() {
        List<Orders> expectedOutputList = dynamicParameterHolder.getListOutput();
        List<Orders> actualOutputList = ordersJpaController.getOrdersByUser(dynamicParameterHolder.getInput());

        //Test size and content of returned list
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    @Test
    public void getOrdersByUserId_test() {
        List<Orders> expectedOutputList = dynamicParameterHolder.getListOutput();
        List<Orders> actualOutputList = ordersJpaController
                .getOrdersByUser(dynamicParameterHolder.getInput().getUserId());

        //Test size and content of returned list
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeTestScript(OrdersTestUtils.ORDER_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(OrdersTestUtils.class.getPackage());
    }
}
