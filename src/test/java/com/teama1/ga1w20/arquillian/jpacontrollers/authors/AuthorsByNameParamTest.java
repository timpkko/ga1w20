package com.teama1.ga1w20.arquillian.jpacontrollers.authors;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A test class for the getAuthorsByBook(Book) and getAuthorsByBook(Long isbn)
 * methods of AuthorsJpaController.
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class AuthorsByNameParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(AuthorsByNameParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Jpa Controller
    @Inject
    private AuthorsJpaController authorController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<String, List<Authors>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<Authors> actualOutputList;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    AuthorsTestUtils authorsTestUtils = new AuthorsTestUtils();

    String input1, input2, input3, input4, input5, input6;

    private final List<Authors> resultList1 = new ArrayList<>();
    private final List<Authors> resultList2 = new ArrayList<>();
    private final List<Authors> resultList3 = new ArrayList<>();
    private final List<Authors> resultList4 = new ArrayList<>();
    private List<Authors> resultList5 = new ArrayList<>();

    /**
     * Constructor
     */
    public AuthorsByNameParamTest() {
        //Case : 1 result, testing for case-sensitivity
        resultList1.clear();
        input1 = "llin";
        resultList1.add(new Authors(4, "Paige", "Billin-Frye"));

        //Case : 2 results
        input2 = "Thom";
        resultList2.clear();
        resultList2.add(new Authors(85, "Lauren", "Thompson"));
        resultList2.add(new Authors(84, "Angie", "Thomas"));

        //Case : 4 results
        input3 = "ar";
        resultList3.clear();
        resultList3.add(new Authors(17, "Barry", "Deutsch"));
        resultList3.add(new Authors(30, "Larry", "Gonick"));
        resultList3.add(new Authors(37, "Charlotte Foltz", "Jones"));
        resultList3.add(new Authors(50, "Juana", "Martinez-Neal"));

        //Case :  20 results
        input4 = "e";
        resultList4.clear();
        resultList4.add(new Authors(4, "Paige", "Billin-Frye"));
        resultList4.add(new Authors(17, "Barry", "Deutsch"));
        resultList4.add(new Authors(33, "Jennifer L.", "Holm", "Dr"));
        resultList4.add(new Authors(34, "Matthew", "Holm"));
        resultList4.add(new Authors(36, "Steve", "Jenkins"));
        resultList4.add(new Authors(37, "Charlotte Foltz", "Jones"));
        resultList4.add(new Authors(45, "Adir", "Levy"));
        resultList4.add(new Authors(46, "Ganit", "Levy"));
        resultList4.add(new Authors(50, "Juana", "Martinez-Neal"));
        resultList4.add(new Authors(65, "John", "O'Brien"));
        resultList4.add(new Authors(72, "Mat", "Sadler"));
        resultList4.add(new Authors(75, "Donald M.", "Silver"));
        resultList4.add(new Authors(78, "Jeff", "Smith"));
        resultList4.add(new Authors(82, "Raina", "Telgemeier"));
        resultList4.add(new Authors(84, "Angie", "Thomas"));
        resultList4.add(new Authors(85, "Lauren", "Thompson"));
        resultList4.add(new Authors(88, "Jonathan", "Van Ness"));
        resultList4.add(new Authors(92, "Jacqueline", "Woodson"));
        resultList4.add(new Authors(93, "Patricia J.", "Wynne", "Dr"));
        resultList4.add(new Authors(94, "Gene Luen", "Yang"));
        resultList4.add(new Authors(200, "Hi", "IDontHaveAnyBooks"));

        //Case :  Empty string, all authors results
        input5 = "";
        resultList5 = authorsTestUtils.getAllAuthorsExpected();

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(input1, resultList1),
                new ParameterHolder<>(input2, resultList2),
                new ParameterHolder<>(input3, resultList3),
                new ParameterHolder<>(input4, resultList4),
                new ParameterHolder<>(input5, resultList5)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Test if the actual number of Authors found is the same as the expected
     * number of Authors.
     */
    @Test
    public void authorsByName_ListSizeTest() {
        actualOutputList = authorController.
                getAuthorLikeName(dynamicParameterHolder.getInput());
        List<Authors> expectedOutputList = dynamicParameterHolder.getListOutput();
        assertEquals(expectedOutputList.size(), actualOutputList.size());
    }

    /**
     * Test if the actual list of Authors found is the same as the expected list
     * of Authors.
     */
    @Test
    public void authorsByName_ListContentTest() {
        actualOutputList = authorController.
                getAuthorLikeName(dynamicParameterHolder.getInput());
        List<Authors> expectedOutputList = dynamicParameterHolder.getListOutput();

        assertTrue("The actual list seem to be containing extra expected objects." + expectedOutputList.toString() + "actual" + actualOutputList.toString(),
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive().addPackage(AuthorsTestUtils.class.getPackage());
    }
}
