package com.teama1.ga1w20.arquillian.jpacontrollers.reviews;

import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.entities.Users;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Populates rulesets for parameterized testing of ReviewsJpaController. Reviews
 * may be retrieved by: - Book (Books object or ISBN) - User (Users object or
 * userId)
 *
 * @author Eira Garrett
 */
public class ReviewsRulePopulator {

    private final static Logger LOG = LoggerFactory.getLogger(ReviewsRulePopulator.class);

    private final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final Books BOOK1 = new Books();
    private final Books BOOK2 = new Books();
    private final Books BOOK3 = new Books();
    private final Books BOOK4 = new Books();
    private final Books BOOK5 = new Books();

    private final Users USER1 = new Users();
    private final Users USER2 = new Users();
    private final Users USER3 = new Users();
    private final Users USER4 = new Users();
    private final Users USER5 = new Users();

    private final List<Reviews> RESULTLIST1 = new ArrayList<>();
    private final List<Reviews> RESULTLIST2 = new ArrayList<>();
    private final List<Reviews> RESULTLIST3 = new ArrayList<>();
    private final List<Reviews> RESULTLIST4 = new ArrayList<>();
    private final List<Reviews> RESULTLIST5 = new ArrayList<>();

    public ReviewsRulePopulator() {
    }

    public ParameterRule getApprovedReviewsByBookRule() throws ParseException {
        //1 - isbn: 9780692848388, 2 results - 44, 45
        BOOK1.setIsbn(9780692848388L);

        Reviews r1 = new Reviews();
        r1.setReviewId(44L);
        r1.setPostdate(SDF.parse("2019-02-23 10:15:01"));
        r1.setRating(2);
        r1.setText("Voluptatem veritatis nobis omnis voluptatem. Esse assumenda quo sequi dignissimos omnis. Sequi minima rerum accusamus tempore ea quaerat.");
        r1.setApproved(true);

        Reviews r2 = new Reviews();
        r2.setReviewId(45L);
        r2.setPostdate(SDF.parse("2019-08-12 00:07:19"));
        r2.setRating(3);
        r2.setText("Aut ipsum sapiente quia sit. Eaque ut quis voluptas consequuntur odit quia. Excepturi impedit cupiditate rerum provident dolores aut.");
        r2.setApproved(true);

        RESULTLIST1.clear();
        RESULTLIST1.add(r1);
        RESULTLIST1.add(r2);

        //2 - isbn: 9780385320436, 1 result - 64
        BOOK2.setIsbn(9780385320436L);

        Reviews r3 = new Reviews();
        r3.setReviewId(64L);
        r3.setPostdate(SDF.parse("2016-04-22 22:28:05"));
        r3.setRating(2);
        r3.setText("Illo voluptatem dolorem ea eum fugit dolorem est. Asperiores impedit optio quo sint ab maiores. Dolores qui explicabo soluta deserunt. Velit accusantium eum ratione porro sint quibusdam.");
        r3.setApproved(true);

        RESULTLIST2.clear();
        RESULTLIST2.add(r3);

        //3 - isbn: 9780486468211, 2 results: 40, 41
        BOOK3.setIsbn(9780486468211L);

        Reviews r4 = new Reviews();
        r4.setReviewId(40L);
        r4.setPostdate(SDF.parse("2015-06-15 19:55:25"));
        r4.setText("Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.");
        r4.setRating(5);
        r4.setApproved(true);

        Reviews r5 = new Reviews();
        r5.setReviewId(41L);
        r5.setPostdate(SDF.parse("2002-03-01 13:48:55"));
        //r5.setPostdate(new Date(2002, 3, 1, 13, 48, 55));
        r5.setRating(2);
        r5.setText("Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.");
        r5.setApproved(true);

        RESULTLIST3.clear();
        RESULTLIST3.add(r4);
        RESULTLIST3.add(r5);

        //4 - isbn: 9780375832291, 2 results: 93, 94
        BOOK4.setIsbn(9780375832291L);

        Reviews r6 = new Reviews();
        r6.setReviewId(93L);
        r6.setPostdate(SDF.parse("2002-08-31 05:18:29"));
        r6.setRating(4);
        r6.setText("At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.");
        r6.setApproved(true);

        Reviews r7 = new Reviews();
        r7.setReviewId(94L);
        r7.setPostdate(SDF.parse("2016-10-22 09:40:58"));
        r7.setRating(3);
        r7.setText("Illo consectetur et eos. Aperiam aut soluta optio omnis exercitationem. Voluptas atque dolores laborum assumenda rem.");
        r7.setApproved(true);

        RESULTLIST4.clear();
        RESULTLIST4.add(r6);
        RESULTLIST4.add(r7);

        //5 - isbn: 9780448405179, 2 results: 53, 54
        BOOK5.setIsbn(9780448405179L);

        Reviews r8 = new Reviews();
        r8.setReviewId(53L);
        r8.setPostdate(SDF.parse("2019-04-07 18:43:46"));
        r8.setRating(2);
        r8.setText("Cumque quia eligendi ipsum reprehenderit harum velit id hic. A corporis deserunt unde at. Impedit ipsum quae ea velit quia occaecati velit.");
        r8.setApproved(true);

        Reviews r9 = new Reviews();
        r9.setReviewId(54L);
        r9.setPostdate(SDF.parse("2010-05-07 15:24:16"));
        r9.setRating(4);
        r9.setText("Aut hic ex necessitatibus cupiditate saepe. Quasi sapiente illo illum est. Nemo est et tempora laudantium iste. Accusantium est est laboriosam repellat pariatur perferendis voluptatem.");
        r9.setApproved(true);

        RESULTLIST5.clear();
        RESULTLIST5.add(r8);
        RESULTLIST5.add(r9);

        LOG.debug("Populating rule for retrieving approved reviews by book with expected results: (isbn: 9780692848388 - review_ids: {44, 45}) // (isbn: 9780385320436 - review_id 64) // (isbn: 9780375832291 - review_ids: {40, 41}) // (isbn: 9780375832291 - review_ids: {93, 94}) // (isbn: 9780448405179 - review_ids: {53, 54})");

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(BOOK1, RESULTLIST1),
                new ParameterHolder<>(BOOK2, RESULTLIST2),
                new ParameterHolder<>(BOOK3, RESULTLIST3),
                new ParameterHolder<>(BOOK4, RESULTLIST4),
                new ParameterHolder<>(BOOK5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getAllReviewsByBookRule() throws ParseException {
        //1 - isbn: 9780692848388, 3 results - 43, 44, 45
        BOOK1.setIsbn(9780692848388L);

        Reviews r1 = new Reviews();
        r1.setReviewId(43L);
        r1.setPostdate(SDF.parse("2017-11-13 03:22:28"));
        r1.setRating(2);
        r1.setText("Dolores earum eos sed voluptatibus sit. Delectus culpa repellat porro magnam delectus minima assumenda. Velit in minus ad est omnis. Nobis numquam nulla quia.");
        r1.setApproved(false);

        Reviews r2 = new Reviews();
        r2.setReviewId(44L);
        r2.setPostdate(SDF.parse("2019-02-23 10:15:01"));
        r2.setRating(2);
        r2.setText("Voluptatem veritatis nobis omnis voluptatem. Esse assumenda quo sequi dignissimos omnis. Sequi minima rerum accusamus tempore ea quaerat.");
        r2.setApproved(true);

        Reviews r3 = new Reviews();
        r3.setReviewId(45L);
        r3.setPostdate(SDF.parse("2019-08-12 00:07:19"));
        r3.setRating(3);
        r3.setText("Aut ipsum sapiente quia sit. Eaque ut quis voluptas consequuntur odit quia. Excepturi impedit cupiditate rerum provident dolores aut.");
        r3.setApproved(true);

        RESULTLIST1.clear();
        RESULTLIST1.add(r1);
        RESULTLIST1.add(r2);
        RESULTLIST1.add(r3);

        //2 - isbn: 9780385320436, 2 results - 63, 64
        BOOK2.setIsbn(9780385320436L);

        Reviews r4 = new Reviews();
        r4.setReviewId(63L);
        r4.setPostdate(SDF.parse("2010-07-05 13:38:03"));
        r4.setRating(2);
        r4.setText("Ipsum qui assumenda rerum fugit voluptates temporibus. Eos voluptas ipsa tempore omnis modi eius. Perspiciatis voluptas nam laboriosam mollitia nesciunt ad.");
        r4.setApproved(false);

        Reviews r5 = new Reviews();
        r5.setReviewId(64L);
        r5.setPostdate(SDF.parse("2016-04-22 22:28:05"));
        r5.setRating(2);
        r5.setText("Illo voluptatem dolorem ea eum fugit dolorem est. Asperiores impedit optio quo sint ab maiores. Dolores qui explicabo soluta deserunt. Velit accusantium eum ratione porro sint quibusdam.");
        r5.setApproved(true);

        RESULTLIST2.clear();
        RESULTLIST2.add(r4);
        RESULTLIST2.add(r5);

        //3 - isbn: 9780486468211, 2 results: 40, 41
        BOOK3.setIsbn(9780486468211L);

        Reviews r6 = new Reviews();
        r6.setReviewId(40L);
        r6.setPostdate(SDF.parse("2015-06-15 19:55:25"));
        r6.setText("Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.");
        r6.setRating(5);
        r6.setApproved(true);

        Reviews r7 = new Reviews();
        r7.setReviewId(41L);
        r7.setPostdate(SDF.parse("2002-03-01 13:48:55"));
        r7.setRating(2);
        r7.setText("Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.");
        r7.setApproved(true);

        RESULTLIST3.clear();
        RESULTLIST3.add(r6);
        RESULTLIST3.add(r7);

        //4 - isbn: 9780375832291, 2 results: 93, 94
        BOOK4.setIsbn(9780375832291L);

        Reviews r8 = new Reviews();
        r8.setReviewId(93L);
        r8.setPostdate(SDF.parse("2002-08-31 05:18:29"));
        r8.setRating(4);
        r8.setText("At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.");
        r8.setApproved(true);

        Reviews r9 = new Reviews();
        r9.setReviewId(94L);
        r9.setPostdate(SDF.parse("2016-10-22 09:40:58"));
        r9.setRating(3);
        r9.setText("Illo consectetur et eos. Aperiam aut soluta optio omnis exercitationem. Voluptas atque dolores laborum assumenda rem.");
        r9.setApproved(true);

        RESULTLIST4.clear();
        RESULTLIST4.add(r8);
        RESULTLIST4.add(r9);

        //5 - isbn: 9780448405179, 2 results: 53, 54
        BOOK5.setIsbn(9780448405179L);

        Reviews r10 = new Reviews();
        r10.setReviewId(53L);
        r10.setPostdate(SDF.parse("2019-04-07 18:43:46"));
        r10.setRating(2);
        r10.setText("Cumque quia eligendi ipsum reprehenderit harum velit id hic. A corporis deserunt unde at. Impedit ipsum quae ea velit quia occaecati velit.");
        r10.setApproved(true);

        Reviews r11 = new Reviews();
        r11.setPostdate(SDF.parse("2010-05-07 15:24:16"));
        r11.setReviewId(54L);
        r11.setRating(4);
        r11.setText("Aut hic ex necessitatibus cupiditate saepe. Quasi sapiente illo illum est. Nemo est et tempora laudantium iste. Accusantium est est laboriosam repellat pariatur perferendis voluptatem.");
        r11.setApproved(true);

        RESULTLIST5.clear();
        RESULTLIST5.add(r10);
        RESULTLIST5.add(r11);

        LOG.debug("Populating rule for retrieving all reviews by book with expected results: (isbn: 9780692848388 - review_ids: {43, 44, 45}) // (isbn: 9780385320436 = review_ids {63, 64}) // (isbn: 9780375832291 - review_ids: {40, 41}) // (isbn: 9780375832291 - review_ids: {93, 94}) // (isbn: 9780448405179 - review_ids: {53, 54})");

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(BOOK1, RESULTLIST1),
                new ParameterHolder<>(BOOK2, RESULTLIST2),
                new ParameterHolder<>(BOOK3, RESULTLIST3),
                new ParameterHolder<>(BOOK4, RESULTLIST4),
                new ParameterHolder<>(BOOK5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getApprovedReviewsByUserRule() throws ParseException {
        //1 - user_id 88, 3 results - 90, 91, 93
        USER1.setUserId(88);

        Reviews r1 = new Reviews();
        r1.setReviewId(90L);
        r1.setPostdate(SDF.parse("2010-09-04 23:59:30"));
        r1.setRating(1);
        r1.setText("Harum provident explicabo accusamus asperiores ea ex quae. Laborum repudiandae et eligendi id. Eveniet expedita recusandae voluptas expedita. Molestias iure consectetur omnis fuga.");
        r1.setApproved(true);

        Reviews r2 = new Reviews();
        r2.setReviewId(91L);
        r2.setPostdate(SDF.parse("2010-09-27 06:12:09"));
        r2.setRating(1);
        r2.setText("Molestias omnis vel nemo omnis commodi et. Incidunt ad harum unde deserunt atque rerum. Aspernatur ducimus aut dolor tempore necessitatibus delectus sint. Odit et architecto eligendi fugit.");
        r2.setApproved(true);

        Reviews r3 = new Reviews();
        r3.setReviewId(93L);
        r3.setPostdate(SDF.parse("2002-08-31 05:18:29"));
        r3.setRating(4);
        r3.setText("At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.");
        r3.setApproved(true);

        RESULTLIST1.clear();
        RESULTLIST1.add(r1);
        RESULTLIST1.add(r2);
        RESULTLIST1.add(r3);

        //2 - user_id: 1, 1 result - 11
        USER2.setUserId(1);

        Reviews r4 = new Reviews();
        r4.setReviewId(11L);
        r4.setPostdate(SDF.parse("2009-04-07 20:42:35"));
        r4.setRating(4);
        r4.setText("Et et placeat sunt officiis ut quis. Quis cupiditate suscipit non nulla repellendus eaque voluptas. Architecto est quidem eum laboriosam numquam. Eos modi corporis et nihil et voluptas explicabo.");
        r4.setApproved(true);

        RESULTLIST2.clear();
        RESULTLIST2.add(r4);

        //3 - user_id: 17, 2 results: 24, 40
        USER3.setUserId(17);

        Reviews r5 = new Reviews();
        r5.setReviewId(24L);
        r5.setPostdate(SDF.parse("2016-02-02 03:53:48"));
        r5.setRating(1);
        r5.setText("Ab rerum commodi cupiditate neque. Quasi facere quaerat facilis quae aut quasi. Ipsa excepturi inventore maiores quos. Laudantium id non est est.");
        r5.setApproved(true);

        Reviews r6 = new Reviews();
        r6.setReviewId(40L);
        r6.setPostdate(SDF.parse("2015-06-15 19:55:25"));
        r6.setRating(5);
        r6.setText("Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.");
        r6.setApproved(true);

        RESULTLIST3.clear();
        RESULTLIST3.add(r5);
        RESULTLIST3.add(r6);

        //4 - user_id: 36, 2 results: 36, 41
        USER4.setUserId(36);

        Reviews r7 = new Reviews();
        r7.setReviewId(36L);
        r7.setPostdate(SDF.parse("2012-01-26 10:07:37"));
        r7.setRating(5);
        r7.setText("Beatae unde nemo sit. Exercitationem modi quia tenetur ut iusto ullam. Qui id earum sunt veritatis maiores impedit ut.");
        r7.setApproved(true);

        Reviews r8 = new Reviews();
        r8.setReviewId(41L);
        r8.setPostdate(SDF.parse("2002-03-01 13:48:55"));
        r8.setRating(2);
        r8.setText("Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.");
        r8.setApproved(true);

        RESULTLIST4.clear();
        RESULTLIST4.add(r7);
        RESULTLIST4.add(r8);

        //5 - user_id: 95, 2 results: 94, 96
        USER5.setUserId(95);

        Reviews r9 = new Reviews();
        r9.setReviewId(96L);
        r9.setPostdate(SDF.parse("2018-03-30 01:45:24"));
        r9.setRating(3);
        r9.setText("In ut non saepe consequatur dolor nulla. Cumque qui et corrupti. Nesciunt est et commodi qui.");
        r9.setApproved(true);

        Reviews r10 = new Reviews();
        r10.setReviewId(99L);
        r10.setPostdate(SDF.parse("2010-11-12 13:21:04"));
        r10.setRating(2);
        r10.setText("Sit ut maiores fugit accusantium. Nostrum eveniet est perspiciatis aut beatae. Saepe asperiores enim adipisci voluptas soluta. Vitae in alias est optio excepturi.");
        r10.setApproved(true);

        RESULTLIST5.clear();
        RESULTLIST5.add(r9);
        RESULTLIST5.add(r10);

        LOG.debug("Populating ruleset for retrieving approved reviews by user with values: (user_id: 88, review_ids: {90, 91, 93}) // (user_id: 1, review_id: 11) // (user_id: 17, review_ids: {24, 40}) // (user_id: 36, review_ids: {36, 41) // (user_id: 95, review_ids: {96, 99})");

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(USER1, RESULTLIST1),
                new ParameterHolder<>(USER2, RESULTLIST2),
                new ParameterHolder<>(USER3, RESULTLIST3),
                new ParameterHolder<>(USER4, RESULTLIST4),
                new ParameterHolder<>(USER5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getAllReviewsByUserRule() throws ParseException {
        //1 - user_id 88, 3 results - 90, 91, 93
        USER1.setUserId(88);

        Reviews r1 = new Reviews();
        r1.setReviewId(90L);
        r1.setPostdate(SDF.parse("2010-09-04 23:59:30"));
        r1.setRating(1);
        r1.setText("Harum provident explicabo accusamus asperiores ea ex quae. Laborum repudiandae et eligendi id. Eveniet expedita recusandae voluptas expedita. Molestias iure consectetur omnis fuga.");
        r1.setApproved(true);

        Reviews r2 = new Reviews();
        r2.setReviewId(91L);
        r2.setPostdate(SDF.parse("2010-09-27 06:12:09"));
        r2.setRating(1);
        r2.setText("Molestias omnis vel nemo omnis commodi et. Incidunt ad harum unde deserunt atque rerum. Aspernatur ducimus aut dolor tempore necessitatibus delectus sint. Odit et architecto eligendi fugit.");
        r2.setApproved(true);

        Reviews r3 = new Reviews();
        r3.setReviewId(93L);
        r3.setPostdate(SDF.parse("2002-08-31 05:18:29"));
        r3.setRating(4);
        r3.setText("At voluptates ut itaque adipisci molestias id consequatur. Molestiae ratione dicta tempora eum labore dolorem. Et cum eveniet ut ratione. Qui perferendis corporis est adipisci accusamus atque.");
        r3.setApproved(true);

        RESULTLIST1.clear();
        RESULTLIST1.add(r1);
        RESULTLIST1.add(r2);
        RESULTLIST1.add(r3);

        //2 - user_id: 1, 2 results - 11, 18
        USER2.setUserId(1);

        Reviews r4 = new Reviews();
        r4.setReviewId(11L);
        r4.setPostdate(SDF.parse("2009-04-07 20:42:35"));
        r4.setRating(4);
        r4.setText("Et et placeat sunt officiis ut quis. Quis cupiditate suscipit non nulla repellendus eaque voluptas. Architecto est quidem eum laboriosam numquam. Eos modi corporis et nihil et voluptas explicabo.");
        r4.setApproved(true);

        Reviews r5 = new Reviews();
        r5.setReviewId(18L);
        r5.setPostdate(SDF.parse("2016-01-06 18:31:33"));
        r5.setRating(3);
        r5.setText("Commodi quod quibusdam dolor aut ut labore. Voluptatem qui sed nulla unde est. Excepturi iusto corrupti numquam est autem voluptas explicabo. Voluptates similique ea rerum ut voluptatem quidem.");
        r5.setApproved(false);

        RESULTLIST2.clear();
        RESULTLIST2.add(r4);
        RESULTLIST2.add(r5);

        //3 - user_id: 17, 2 results: 24, 40
        USER3.setUserId(17);

        Reviews r6 = new Reviews();
        r6.setReviewId(24L);
        r6.setPostdate(SDF.parse("2016-02-02 03:53:48"));
        r6.setRating(1);
        r6.setText("Ab rerum commodi cupiditate neque. Quasi facere quaerat facilis quae aut quasi. Ipsa excepturi inventore maiores quos. Laudantium id non est est.");
        r6.setApproved(true);

        Reviews r7 = new Reviews();
        r7.setReviewId(40L);
        r7.setPostdate(SDF.parse("2015-06-15 19:55:25"));
        r7.setRating(5);
        r7.setText("Voluptatem iure deserunt repudiandae nemo. Eum reiciendis nemo pariatur magnam quod. Ipsam qui laudantium aut ea culpa.");
        r7.setApproved(true);

        RESULTLIST3.clear();
        RESULTLIST3.add(r6);
        RESULTLIST3.add(r7);

        //4 - user_id: 36, 2 results: 36, 41
        USER4.setUserId(36);

        Reviews r8 = new Reviews();
        r8.setReviewId(36L);
        r8.setPostdate(SDF.parse("2012-01-26 10:07:37"));
        r8.setRating(5);
        r8.setText("Beatae unde nemo sit. Exercitationem modi quia tenetur ut iusto ullam. Qui id earum sunt veritatis maiores impedit ut.");
        r8.setApproved(true);

        Reviews r9 = new Reviews();
        r9.setReviewId(41L);
        r9.setPostdate(SDF.parse("2002-03-01 13:48:55"));
        r9.setRating(2);
        r9.setText("Id reprehenderit eos a nulla ea voluptatem. Dolor et fugiat voluptas repudiandae iure. Sit doloribus suscipit reiciendis debitis rerum dolor necessitatibus.");
        r9.setApproved(true);

        RESULTLIST4.clear();
        RESULTLIST4.add(r8);
        RESULTLIST4.add(r9);

        //5 - user_id: 95, 2 results: 94, 96
        USER5.setUserId(95);

        Reviews r10 = new Reviews();
        r10.setReviewId(96L);
        r10.setPostdate(SDF.parse("2018-03-30 01:45:24"));
        r10.setRating(3);
        r10.setText("In ut non saepe consequatur dolor nulla. Cumque qui et corrupti. Nesciunt est et commodi qui.");
        r10.setApproved(true);

        Reviews r11 = new Reviews();
        r11.setReviewId(99L);
        r11.setPostdate(SDF.parse("2010-11-12 13:21:04"));
        r11.setRating(2);
        r11.setText("Sit ut maiores fugit accusantium. Nostrum eveniet est perspiciatis aut beatae. Saepe asperiores enim adipisci voluptas soluta. Vitae in alias est optio excepturi.");
        r11.setApproved(true);

        RESULTLIST5.clear();
        RESULTLIST5.add(r10);
        RESULTLIST5.add(r11);

        LOG.debug("Populating ruleset for retrieving all reviews by user with values: (user_id: 88, review_ids: {90, 91, 93}) // (user_id: 1, review_ids: {11, 18}) // (user_id: 17, review_ids: {24, 40}) // (user_id: 36, review_ids: {36, 41}) // (user_id: 95, review_ids: {96, 99})");

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(USER1, RESULTLIST1),
                new ParameterHolder<>(USER2, RESULTLIST2),
                new ParameterHolder<>(USER3, RESULTLIST3),
                new ParameterHolder<>(USER4, RESULTLIST4),
                new ParameterHolder<>(USER5, RESULTLIST5)
        );

        return ruleSet;
    }
}
