package com.teama1.ga1w20.arquillian.jpacontrollers.reviews;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.ReviewsJpaController;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class ReviewsJPATest {

    private final static Logger LOG = LoggerFactory.getLogger(ReviewsJPATest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    private ReviewsJpaController controller;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void testGetModerationQueue() {
        List<Reviews> queue = controller.getModerationQueue();

        assertEquals(3, queue.size());
    }

    @Test(expected = NullPointerException.class)
    public void testApprovedReviewsByBookNullInput() {
        Books b = null;

        controller.getApprovedReviewsByBook(b);
    }

    @Test(expected = NullPointerException.class)
    public void testApprovedReviewsByBookNullIsbn() {
        Long l = null;

        controller.getApprovedReviewsByBook(l);
    }

    @Test(expected = NullPointerException.class)
    public void testApprovedReviewsByBookPaginatedNullInput() {
        Books b = null;

        controller.getApprovedReviewsByBook(b, 1, 1);
    }

    @Test(expected = NullPointerException.class)
    public void testApprovedReviewsByIsbnPaginatedNullInput() {
        Long l = null;

        controller.getApprovedReviewsByBook(l, 1, 1);
    }

    @Test(expected = NullPointerException.class)
    public void testGetAllReviewsByBookNullInput() {
        Books b = null;

        controller.getAllReviewsByBook(b);
    }

    @Test(expected = NullPointerException.class)
    public void testGetAllReviewsByIsbnInput() {
        Long l = null;

        controller.getAllReviewsByBook(l);
    }

    @Test(expected = NullPointerException.class)
    public void testGetApprovedReviewsByUserNullInput() {
        Users u = null;

        controller.getApprovedReviewsByUser(u);
    }

    @Test(expected = NullPointerException.class)
    public void testGetApprovedReviewsByUserIdNullInput() {
        Integer i = null;

        controller.getApprovedReviewsByUser(i);
    }

    @Test(expected = NullPointerException.class)
    public void testGetAllReviewsByUserNullInput() {
        Users u = null;

        controller.getAllReviewsByUser(u);
    }

    @Test(expected = NullPointerException.class)
    public void testGetAllReviewsByUserIdNullInput() {
        Integer i = null;

        controller.getAllReviewsByUser(i);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
