package com.teama1.ga1w20.arquillian.jpacontrollers.users;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class UsersByReviewTest {

    private final static Logger LOG = LoggerFactory.getLogger(UsersByReviewTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //JPA Controller
    @Inject
    private UsersJpaController usersController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Ruleset populator
    private UsersRulePopulator rulePopulator;

    @Rule
    public ParameterRule userByReview;
    private ParameterHolder<Reviews, List<Users>> dynamic;
    private ParameterHolder<String, List<Users>> resultHolder;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    public UsersByReviewTest() {
        rulePopulator = new UsersRulePopulator();
        userByReview = rulePopulator.getUsersByReview();
    }

    @Test
    public void testUsersByReview() {
        Users user = new Users();

        try {
            user = usersController.getUserByReview(dynamic.getInput());
        } catch (NoResultException | NonUniqueResultException | NonexistentEntityException ex) {
            LOG.error("Exception thrown while testing retrieval of user by review: ", ex);
        }

        assertEquals(dynamic.getListOutput().get(0), user);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive().addPackage(UsersByEmailTest.class.getPackage());
    }
}
