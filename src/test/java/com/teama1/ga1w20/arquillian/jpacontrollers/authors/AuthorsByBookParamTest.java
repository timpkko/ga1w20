package com.teama1.ga1w20.arquillian.jpacontrollers.authors;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A test class for the getAuthorsByBook(Book) and getAuthorsByBook(Long isbn)
 * methods of AuthorsJpaController.
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class AuthorsByBookParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(AuthorsByBookParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Jpa Controller
    @Inject
    private AuthorsJpaController authorController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //RuleSet containing testCases to test
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Books, List<Authors>> dynamicParameterHolder;

    //Field representing the actual value(s)
    List<Authors> actualOutputList;

    private final Books BOOK1 = new Books();
    private final Books BOOK2 = new Books();
    private final Books BOOK3 = new Books();
    private final Books BOOK4 = new Books();

    private final List<Authors> RESULTLIST1 = new ArrayList<>();
    private final List<Authors> RESULTLIST2 = new ArrayList<>();
    private final List<Authors> RESULTLIST3 = new ArrayList<>();
    private final List<Authors> RESULTLIST4 = new ArrayList<>();
    private final List<Authors> EMPTY_RESULTLIST = new ArrayList<>();

    /**
     * Constructor
     */
    public AuthorsByBookParamTest() {
        //Case with one result
        BOOK1.setIsbn(9780062498564L);
        RESULTLIST1.clear();
        RESULTLIST1.add(new Authors(84, "Angie", "Thomas"));

        //Case with 2 results
        BOOK1.setIsbn(9780375832291L);
        RESULTLIST1.clear();
        RESULTLIST1.add(new Authors(34, "Matthew", "Holm"));
        RESULTLIST1.add(new Authors(33, "Jennifer L.", "Holm", "Dr"));

        //Case with 3 results
        BOOK2.setIsbn(9780692848388L);
        RESULTLIST2.clear();
        RESULTLIST2.add(new Authors(45, "Adir", "Levy"));
        RESULTLIST2.add(new Authors(46, "Ganit", "Levy"));
        RESULTLIST2.add(new Authors(72, "Mat", "Sadler"));

        //Case with 2 results
        BOOK3.setIsbn(9780448405179L);
        RESULTLIST3.clear();
        RESULTLIST3.add(new Authors(4, "Paige", "Billin-Frye"));
        RESULTLIST3.add(new Authors(91, "Lynn", "Wilson"));

        //Case with 0 results
        BOOK4.setIsbn(30L); //Inexistent book
        RESULTLIST4.clear();

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(BOOK1, RESULTLIST1),
                new ParameterHolder<>(BOOK2, RESULTLIST2),
                new ParameterHolder<>(BOOK3, RESULTLIST3),
                new ParameterHolder<>(BOOK4, EMPTY_RESULTLIST));
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Tests that the correct number of authors are returned based on the book
     * object input.
     */
    @Test
    public void authorsByBookObject_SizeTest() {
        actualOutputList = authorController.getAuthorsByBook(dynamicParameterHolder.getInput());
        List<Authors> expectedOutputList = dynamicParameterHolder.getListOutput();
        assertEquals("expected: " + expectedOutputList.toString() + "actual: " + actualOutputList.toString(), expectedOutputList.size(), actualOutputList.size());
    }

    /**
     * Tests that the correct number of authors are returned when retrieving
     * authors by book isbn.
     */
    @Test
    public void authorsByIsbn_SizeTest() {
        actualOutputList = authorController.
                getAuthorsByBook(dynamicParameterHolder.getInput().getIsbn());
        List<Authors> expectedOutputList = dynamicParameterHolder.getListOutput();
        assertEquals(expectedOutputList.size(),
                actualOutputList.size());
    }

    /**
     * Tests that the correct number of authors are returned when retrieving
     * authors by book isbn.
     */
    @Test
    public void authorsByIsbn_ListTest() {
        actualOutputList = authorController.
                getAuthorsByBook(dynamicParameterHolder.getInput().getIsbn());
        List<Authors> expectedOutputList = dynamicParameterHolder.getListOutput();
        assertTrue(actualOutputList.containsAll(expectedOutputList));
        assertTrue(expectedOutputList.containsAll(actualOutputList));
    }

    /**
     * Ensures the correct authors are retrieved for a given book object.
     */
    @Test
    public void authorsByBookObject_ListTest() {
        actualOutputList = authorController.
                getAuthorsByBook(dynamicParameterHolder.getInput());
        List<Authors> expectedOutputList = dynamicParameterHolder.getListOutput();
        assertTrue(actualOutputList.containsAll(expectedOutputList));
        assertTrue(expectedOutputList.containsAll(actualOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive().addPackage(AuthorsTestUtils.class.getPackage());
    }
}
