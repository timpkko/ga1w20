package com.teama1.ga1w20.arquillian.jpacontrollers.orderitems;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.jpacontrollers.OrderitemsJpaController;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test OrderitemsJpaController -getOrderitemsCount()
 * -findOrderitemsEntities()
 *
 * Custom queries: - getOrderitemsFromOrderId(Integer orderId) exception
 * throwing - getOrderitemsFromOrderIdIncludeRemoved(Integer orderId) exception
 * throwing
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class OrderitemsJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(OrderitemsJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    private final OrderitemsTestUtils orderitemsTestUtils = new OrderitemsTestUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    OrderitemsJpaController orderitemsJpaController;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getOrderitemsCount_Test() {
        int actualCount = orderitemsJpaController.getOrderitemsCount();
        int expectedCount = orderitemsTestUtils.getTotalNumOfOrderitemsExpected();
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Test
    public void findOrderitemsEntities_Test() {
        List<Orderitems> actualOutputList
                = orderitemsJpaController.findOrderitemsEntities();
        List<Orderitems> expectedOutputList
                = orderitemsTestUtils.getAllOrderitemsExpected();
        assertTrue(actualOutputList.containsAll(expectedOutputList));
        assertTrue(expectedOutputList.containsAll(actualOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS - EXCEPTION THROWING
    ////////////////////////////////////////////////////////////////////////////
    @Test(expected = NullPointerException.class)
    public void getOrderitemsFromOrderId_NullTest() {
        orderitemsJpaController.getOrderitemsFromOrderId((Integer) null);
    }

    @Test(expected = NullPointerException.class)
    public void getOrderitemsFromOrderIdIncludeRemoved_NullTest() {
        orderitemsJpaController.getOrderitemsFromOrderIdIncludeRemoved((Integer) null);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive().addPackage(GetByOrderIdIncludeParamTest.class.getPackage());
    }
}
