package com.teama1.ga1w20.arquillian.jpacontrollers.genres;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test GenresJpaController Custom query: - getGenresByBook(Books book)
 * - getGenresByBook(Long isbn)
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class GenresJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(GenresJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    GenresJpaController genresJpaController;

    //Total number of entities inserted in the database with the SQL script
    private final static int EXPECTED_COUNT = 4;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getGenresCountTest() {
        int actualCount = genresJpaController.getGenresCount();
        int expectedCount = EXPECTED_COUNT;
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Test(expected = NullPointerException.class)
    public void getGenresByBook_NullTest() {
        genresJpaController.getGenresByBook((Books) null);
    }

    @Test(expected = NullPointerException.class)
    public void getGenresByBookIsbn_NullTest() {
        genresJpaController.getGenresByBook((Long) null);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
