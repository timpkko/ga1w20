package com.teama1.ga1w20.arquillian.jpacontrollers.taxrates;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.TaxratesJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tests public methods for the Tax Rates JPA Controller. Taxrates objects can
 * be retrieved by: - Region (String) - User
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class TaxratesJPATest {

    private final static Logger LOG = LoggerFactory.getLogger(TaxratesJPATest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Controller
    @Inject
    public TaxratesJpaController controller;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test(expected = NoResultException.class)
    public void invalidRegionTest() throws NonexistentEntityException, NoResultException, NonUniqueResultException {
        String region = "CA";

        controller.getRateByRegion(region);
    }

    @Test(expected = NoResultException.class)
    public void nonexistentUserTest() throws NonUniqueResultException, NoResultException, NonexistentEntityException {
        Users user = new Users();
        user.setUserId(5000);

        controller.getRatesForUser(user);
    }

    @Test(expected = NoResultException.class)
    public void nonexistentUserIdTest() throws NonUniqueResultException, NoResultException, NonexistentEntityException {
        controller.getRatesForUser(5000);
    }

    @Test(expected = NullPointerException.class)
    public void testNullInputRatesByRegion() throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        controller.getRateByRegion(null);
    }

    @Test(expected = NullPointerException.class)
    public void testNullInputRatesByUserId() throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        Integer nullInt = null;

        controller.getRatesForUser(nullInt);
    }

    @Test(expected = NullPointerException.class)
    public void testNullInputRatesByUser() throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        Users user = null;

        controller.getRatesForUser(user);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(TaxratesRulePopulator.class.getPackage());
    }
}
