package com.teama1.ga1w20.arquillian.jpacontrollers.taxrates;

import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Taxrates;
import com.teama1.ga1w20.persistence.entities.Users;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Populates rulesets for testing TaxratesJpaController.
 *
 * @author Eira Garrett
 */
public class TaxratesRulePopulator {

    private final static Logger LOG = LoggerFactory.getLogger(TaxratesRulePopulator.class);

    //Rule objects
    private final String LOCALE1 = "AB";
    private final String LOCALE2 = "Pe";
    private final String LOCALE3 = "oN";
    private final String LOCALE4 = "ns";
    private final String LOCALE5 = "QC";

    private final Users USER1 = new Users();
    private final Users USER2 = new Users();
    private final Users USER3 = new Users();
    private final Users USER4 = new Users();
    private final Users USER5 = new Users();

    private final List<Taxrates> RESULTLIST1 = new ArrayList<>();
    private final List<Taxrates> RESULTLIST2 = new ArrayList<>();
    private final List<Taxrates> RESULTLIST3 = new ArrayList<>();
    private final List<Taxrates> RESULTLIST4 = new ArrayList<>();
    private final List<Taxrates> RESULTLIST5 = new ArrayList<>();

    public TaxratesRulePopulator() {
    }

    /**
     * Populates a ruleset for retrieving Taxrates objects by Locale.
     *
     * @return
     */
    public ParameterRule getTaxratesByLocaleRule() {
        //1 - AB: 5.000, 0.000, 0.000
        Taxrates rates1 = new Taxrates();
        rates1.setRegion("AB");
        rates1.setGstRate(new BigDecimal(5.000));
        rates1.setPstRate(BigDecimal.ZERO);
        rates1.setHstRate(BigDecimal.ZERO);
        RESULTLIST1.clear();
        RESULTLIST1.add(rates1);

        //2 - PE: 0.000, 0.000, 15.000
        Taxrates rates2 = new Taxrates();
        rates2.setRegion("PE");
        rates2.setGstRate(BigDecimal.ZERO);
        rates2.setPstRate(BigDecimal.ZERO);
        rates2.setHstRate(new BigDecimal(15.000));
        RESULTLIST2.clear();
        RESULTLIST2.add(rates2);

        //3 - ON: 0.000, 0.000, 13.000
        Taxrates rates3 = new Taxrates();
        rates3.setRegion("ON");
        rates3.setGstRate(BigDecimal.ZERO);
        rates3.setPstRate(BigDecimal.ZERO);
        rates3.setHstRate(new BigDecimal(13.000));
        RESULTLIST3.clear();
        RESULTLIST3.add(rates3);

        //4 - NS: 0.000, 0.000, 15.000
        Taxrates rates4 = new Taxrates();
        rates4.setRegion("NS");
        rates4.setGstRate(BigDecimal.ZERO);
        rates4.setPstRate(BigDecimal.ZERO);
        rates4.setHstRate(new BigDecimal(15.000));
        RESULTLIST4.clear();
        RESULTLIST4.add(rates4);

        //5 - QC: 5.000, 9.975, 0.000
        Taxrates rates5 = new Taxrates();
        rates5.setRegion("QC");
        rates5.setGstRate(new BigDecimal(5.000));
        rates5.setPstRate(new BigDecimal(9.975));
        rates5.setHstRate(BigDecimal.ZERO);
        RESULTLIST5.clear();
        RESULTLIST5.add(rates5);

        LOG.debug("Populated taxrates by locale rules with expected results: (AB, 5.000, 0.000, 0.000) // (PE, 0.000, 0.000, 15.000) // (ON, 0.000, 0.000, 13.000) // (NS, 0.000, 0.000, 15.00) // (QC, 5.000, 9.975, 0.000)");

        ParameterRule ruleSet = new ParameterRule("dynamic",
                new ParameterHolder<>(LOCALE1, RESULTLIST1),
                new ParameterHolder<>(LOCALE2, RESULTLIST2),
                new ParameterHolder<>(LOCALE3, RESULTLIST3),
                new ParameterHolder<>(LOCALE4, RESULTLIST4),
                new ParameterHolder<>(LOCALE5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getTaxratesByUserRule() {
        //1 - UserId 8 - QC (5.000, 9.975, 0.000)
        USER1.setUserId(8);
        USER1.setProvince("QC");

        Taxrates rates1 = new Taxrates("QC");
        rates1.setGstRate(new BigDecimal(5.000));
        rates1.setPstRate(new BigDecimal(9.975));
        rates1.setHstRate(BigDecimal.ZERO);
        RESULTLIST1.clear();
        RESULTLIST1.add(rates1);

        //2 - UserId 2 - MB (5.000, 7.000, 0.000)
        USER2.setUserId(2);
        USER2.setProvince("MB");

        Taxrates rates2 = new Taxrates("MB");
        rates2.setGstRate(new BigDecimal(5.000));
        rates2.setPstRate(new BigDecimal(7.000));
        rates2.setHstRate(BigDecimal.ZERO);
        RESULTLIST2.clear();
        RESULTLIST2.add(rates2);

        //3 - UserId 89 - BC (5.000, 7.000, 0.000)
        USER3.setUserId(89);
        USER3.setProvince("BC");

        Taxrates rates3 = new Taxrates("BC");
        rates3.setGstRate(new BigDecimal(5.000));
        rates3.setPstRate(new BigDecimal(7.000));
        rates3.setHstRate(BigDecimal.ZERO);
        RESULTLIST3.clear();
        RESULTLIST3.add(rates3);

        //4 - UserId 44 - YT (5.000, 0.000, 0.000)
        USER4.setUserId(44);
        USER4.setProvince("YT");

        Taxrates rates4 = new Taxrates("YT");
        rates4.setGstRate(new BigDecimal(5.000));
        rates4.setPstRate(BigDecimal.ZERO);
        rates4.setHstRate(BigDecimal.ZERO);
        RESULTLIST4.clear();
        RESULTLIST4.add(rates4);

        //5 - UserId 92 - NU (5.000, 0.000, 0.000)
        USER5.setUserId(92);
        USER5.setProvince("NU");

        Taxrates rates5 = new Taxrates("NU");
        rates5.setGstRate(new BigDecimal(5.000));
        rates5.setPstRate(BigDecimal.ZERO);
        rates5.setHstRate(BigDecimal.ZERO);
        RESULTLIST5.clear();
        RESULTLIST5.add(rates5);

        LOG.debug("Populating ruleset for retrieving taxrates by user: (userId 8 - QC) // (userId 2 - MB) // (userId 89 - BC) // (userId 44 - YT) // (userId 92 - NU)");

        ParameterRule ruleSet = new ParameterRule("dynamic",
                new ParameterHolder<>(USER1, RESULTLIST1),
                new ParameterHolder<>(USER2, RESULTLIST2),
                new ParameterHolder<>(USER3, RESULTLIST3),
                new ParameterHolder<>(USER4, RESULTLIST4),
                new ParameterHolder<>(USER5, RESULTLIST5)
        );

        return ruleSet;
    }
}
