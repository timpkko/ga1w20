package com.teama1.ga1w20.arquillian.jpacontrollers.orderitems;

import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.entities.Orders;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Utils class holding utils methods allowing, among other things to access
 * objects representing all the entities inserted in the database when populated
 * by the default test script.
 *
 * @see ArquillianUtils for default test script
 * @author Camillia E.
 */
public class OrderitemsTestUtils {

    private List<Orderitems> allOrderitemsList;

    /**
     * Get a list of objects representing all the entities inserted in the
     * database when populated by the default test script.
     *
     * @return List of objects
     */
    public List<Orderitems> getAllOrderitemsExpected() {
        if (allOrderitemsList != null) {
            return allOrderitemsList;
        }
        return createList();
    }

    /**
     * Get the number of entities inserted in the database when populated by the
     * default test script.
     *
     * @return
     */
    public int getTotalNumOfOrderitemsExpected() {
        if (allOrderitemsList != null) {
            return allOrderitemsList.size();
        }
        return createList().size();
    }

    /**
     * Allows taking the INSERT INTO statement from the populate script and
     * translate it to an object.
     *
     * @return Object representation of the arguments used in the INSERT INTO
     * MySQL script.
     */
    public Orderitems createOrderitem(Integer orderitemsId, Integer orderId,
            Long isbn, double price, boolean removed) {
        return new Orderitems(orderitemsId, new BigDecimal(price),
                new Books(isbn), new Orders(orderId), removed);
    }

    ///////////////////////////////////////////////////////////////////////////
    private List<Orderitems> createList() {
        allOrderitemsList = new ArrayList<>();
        allOrderitemsList.add(createOrderitem(1, 1,
                9780062941008L, 19.47, false));
        allOrderitemsList.add(createOrderitem(2, 2,
                9780062941008L, 18.95, false));
        allOrderitemsList.add(createOrderitem(3, 3,
                9780062941008L, 21.66, false));
        allOrderitemsList.add(createOrderitem(4, 4,
                9780062941008L, 18.78, false));
        allOrderitemsList.add(createOrderitem(5, 5,
                9780062941008L, 8.00, false));
        allOrderitemsList.add(createOrderitem(6, 6,
                9780062941008L, 8.00, false));
        allOrderitemsList.add(createOrderitem(7, 7,
                9780062941008L, 28.89, false));
        allOrderitemsList.add(createOrderitem(8, 8,
                9780062941008L, 15.94, false));
        allOrderitemsList.add(createOrderitem(9, 9,
                9780062941008L, 28.54, false));
        allOrderitemsList.add(createOrderitem(10, 10,
                9780062941008L, 8.00, false));
        allOrderitemsList.add(createOrderitem(17, 17,
                9780062953452L, 21.70, false));
        allOrderitemsList.add(createOrderitem(18, 18,
                9780062953414L, 36.04, false));
        allOrderitemsList.add(createOrderitem(23, 23,
                9780062498564L, 25.14, false));
        allOrderitemsList.add(createOrderitem(29, 29,
                9780312625993L, 26.42, false));
        allOrderitemsList.add(createOrderitem(35, 35,
                9780399246531L, 26.22, false));
        allOrderitemsList.add(createOrderitem(36, 36,
                9780763693558L, 11.00, false));
        allOrderitemsList.add(createOrderitem(39, 39,
                9780486468211L, 29.67, false));
        allOrderitemsList.add(createOrderitem(40, 40,
                9780486468211L, 11.84, false));
        allOrderitemsList.add(createOrderitem(42, 42,
                9780692848388L, 26.01, false));
        allOrderitemsList.add(createOrderitem(43, 43,
                9780692848388L, 7.45, false));
        allOrderitemsList.add(createOrderitem(44, 44,
                9780692848388L, 25.46, false));
        allOrderitemsList.add(createOrderitem(45, 45,
                9780547557991L, 8.00, false));
        allOrderitemsList.add(createOrderitem(52, 52,
                9780448405179L, 20.36, false));
        allOrderitemsList.add(createOrderitem(53, 53,
                9780448405179L, 5.04, false));
        allOrderitemsList.add(createOrderitem(62, 62,
                9780385320436L, 24.58, false));

        allOrderitemsList.add(createOrderitem(63, 63,
                9780385320436L, 20.66, false));
        allOrderitemsList.add(createOrderitem(64, 63,
                9780547557991L, 18, false));
        allOrderitemsList.add(createOrderitem(65, 63,
                9780448405179L, 17, true));
        allOrderitemsList.add(createOrderitem(66, 63,
                9780062941008L, 25, false));

        allOrderitemsList.add(createOrderitem(69, 69,
                9780448405179L, 17, false));
        allOrderitemsList.add(createOrderitem(70, 69,
                9780062941008L, 19.47, false));

        allOrderitemsList.add(createOrderitem(89, 88,
                9780606144841L, 20.56, false));
        allOrderitemsList.add(createOrderitem(90, 89,
                9780545132060L, 21.29, false));
        allOrderitemsList.add(createOrderitem(91, 90,
                9780810984226L, 26.77, false));
        allOrderitemsList.add(createOrderitem(93, 92,
                9780375832291L, 24.50, false));
        allOrderitemsList.add(createOrderitem(94, 93,
                9780375832291L, 29.51, false));
        allOrderitemsList.add(createOrderitem(96, 95,
                9780606267380L, 25.94, false));

        allOrderitemsList.add(createOrderitem(102, 101,
                9780062953452L, 21.70, false));
        allOrderitemsList.add(createOrderitem(103, 101,
                9780375832291L, 31.70, true));

        return allOrderitemsList;
    }
}
