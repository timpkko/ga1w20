package com.teama1.ga1w20.arquillian.jpacontrollers.users;

import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Credentials;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.entities.Users;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Populates rulesets for parameterized testing of UsersJpaController.
 *
 * Users can be retrieved by: - Credentials - Email (String) - Review
 *
 * @author Eira Garrett
 */
@SuppressWarnings("deprecation") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
public class UsersRulePopulator {

    private final Logger LOG = LoggerFactory.getLogger(UsersRulePopulator.class);

    private final List<Users> RESULTLIST1 = new ArrayList<>();
    private final List<Users> RESULTLIST2 = new ArrayList<>();
    private final List<Users> RESULTLIST3 = new ArrayList<>();
    private final List<Users> RESULTLIST4 = new ArrayList<>();
    private final List<Users> RESULTLIST5 = new ArrayList<>();

    //Email
    private final String EMAIL1 = "upton.veda@example.org";
    private final String EMAIL2 = "marcella88@example.org";
    private final String EMAIL3 = "erik88@example.org";
    private final String EMAIL4 = "POUROS.AUDREY@EXAMPLE.ORG";
    private final String EMAIL5 = "alVerTa30@example.oRG";

    //Credentials
    private final Credentials CREDENTIALS1 = new Credentials();
    private final Credentials CREDENTIALS2 = new Credentials();
    private final Credentials CREDENTIALS3 = new Credentials();
    private final Credentials CREDENTIALS4 = new Credentials();
    private final Credentials CREDENTIALS5 = new Credentials();

    //Review
    private final Reviews REVIEW1 = new Reviews();
    private final Reviews REVIEW2 = new Reviews();
    private final Reviews REVIEW3 = new Reviews();
    private final Reviews REVIEW4 = new Reviews();
    private final Reviews REVIEW5 = new Reviews();

    public UsersRulePopulator() {
    }

    /**
     * Populates a ruleset for testing the retrieval of Users by email address
     *
     * @return
     */
    public ParameterRule getUsersByEmail() {
        //1 - upton.veda@example.org, userId 4
        Users user1 = new Users();
        user1.setUserId(4);
        user1.setEmail("upton.veda@example.org");
        user1.setTitle("Mx.");
        user1.setLastname("Turner");
        user1.setFirstname("Carey");
        user1.setAddress1("4458 Drew Ville Suite 555");
        user1.setCity("Armandmouth");
        user1.setProvince("NS");
        user1.setCountry("CANADA");
        user1.setPostalcode("B2S 9T4");
        user1.setHomephone("479-249-3018");
        user1.setCellphone("1-971-771-5817");
        RESULTLIST1.clear();
        RESULTLIST1.add(user1);

        //2 - marcella88@example.org - userId 8
        Users user2 = new Users();
        user2.setUserId(8);
        user2.setEmail("marcella88@example.org");
        user2.setTitle("Mr.");
        user2.setLastname("Haag");
        user2.setFirstname("Giovanni");
        user2.setCompanyname("Jaskolski Inc");
        user2.setAddress1("963 Jacquelyn Garden");
        user2.setCity("Parisianborough");
        user2.setProvince("QC");
        user2.setCountry("CANADA");
        user2.setPostalcode("G6L 7V9");
        user2.setHomephone("676-477-2053x82402");
        user2.setCellphone("07563939668");
        RESULTLIST2.clear();
        RESULTLIST2.add(user2);

        //3 - erik88@example.org - userId 9
        Users user3 = new Users();
        user3.setUserId(9);
        user3.setEmail("erik88@example.org");
        user3.setTitle("Mx.");
        user3.setLastname("Schimmel");
        user3.setFirstname("Daphne");
        user3.setCompanyname("Botsford-Hartmann");
        user3.setAddress1("785 Waelchi Unions");
        user3.setCity("Chandlerville");
        user3.setProvince("BC");
        user3.setCountry("CANADA");
        user3.setPostalcode("V9X 4L8");
        user3.setHomephone("630-651-8487x83563");
        user3.setCellphone("02662638593");
        RESULTLIST3.clear();
        RESULTLIST3.add(user3);

        //4 - pouros.audrey@example.org - userId 23
        Users user4 = new Users();
        user4.setUserId(23);
        user4.setEmail("pouros.audrey@example.org");
        user4.setTitle("Mx.");
        user4.setLastname("Hayes");
        user4.setFirstname("Caesar");
        user4.setCompanyname("Langworth, Rice and Rodriguez");
        user4.setAddress1("3476 Leanne Lodge");
        user4.setCity("Shaniefort");
        user4.setProvince("PE");
        user4.setCountry("CANADA");
        user4.setPostalcode("A1A 1A1");
        user4.setHomephone("(648)512-2765");
        user4.setCellphone("888.701.7408");
        RESULTLIST4.clear();
        RESULTLIST4.add(user4);

        //5 - alverta30@example.org - userId 7
        Users user5 = new Users();
        user5.setUserId(7);
        user5.setEmail("alverta30@example.org");
        user5.setTitle("Mx.");
        user5.setLastname("Waters");
        user5.setFirstname("Louvenia");
        user5.setCompanyname("Quigley, Fahey and Mueller");
        user5.setAddress1("51096 Edwin Crest Apt. 144");
        user5.setCity("South Tyshawntown");
        user5.setProvince("QC");
        user5.setCountry("CANADA");
        user5.setPostalcode("G4X 7J6");
        user5.setHomephone("(836)657-4617x4788");
        user5.setCellphone("(115)541-9434");
        RESULTLIST5.clear();
        RESULTLIST5.add(user5);

        LOG.debug("Populating user by email ruleset with values: (upton.veda@example.org, userId 4) // (marcella88@example.org - userId 8) // (erik88@example.org - userId 9) // (pouros.audrey@example.org - userId 23) // (alverta30@example.org - userId 7)");

        ParameterRule ruleSet = new ParameterRule("dynamic",
                new ParameterHolder<>(EMAIL1, RESULTLIST1),
                new ParameterHolder<>(EMAIL2, RESULTLIST2),
                new ParameterHolder<>(EMAIL3, RESULTLIST3),
                new ParameterHolder<>(EMAIL4, RESULTLIST4),
                new ParameterHolder<>(EMAIL5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getUsersByCredentials() {
        //1
        CREDENTIALS1.setUserId(1);

        Users user1 = new Users();
        user1.setUserId(1);
        user1.setEmail("cst.send@gmail.com");

        RESULTLIST1.clear();
        RESULTLIST1.add(user1);

        //2
        CREDENTIALS2.setUserId(5);

        Users user2 = new Users();
        user2.setUserId(5);
        user2.setEmail("saul.gleason@example.com");

        RESULTLIST2.clear();
        RESULTLIST2.add(user2);

        //3
        CREDENTIALS3.setUserId(39);

        Users user3 = new Users();
        user3.setUserId(39);
        user3.setEmail("buford.hoeger@example.com");

        RESULTLIST3.clear();
        RESULTLIST3.add(user3);

        //4
        CREDENTIALS4.setUserId(29);

        Users user4 = new Users();
        user4.setUserId(29);
        user4.setEmail("eliseo39@example.net");

        RESULTLIST4.clear();
        RESULTLIST4.add(user4);

        //5
        CREDENTIALS5.setUserId(89);

        Users user5 = new Users();
        user5.setUserId(89);
        user5.setEmail("fay.ruthe@example.net");

        RESULTLIST5.clear();
        RESULTLIST5.add(user5);

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(CREDENTIALS1, RESULTLIST1),
                new ParameterHolder<>(CREDENTIALS2, RESULTLIST2),
                new ParameterHolder<>(CREDENTIALS3, RESULTLIST3),
                new ParameterHolder<>(CREDENTIALS4, RESULTLIST4),
                new ParameterHolder<>(CREDENTIALS5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getUsersByReview() {
        //1 - review_id 90, user_id 88
        Users user1 = new Users();
        user1.setUserId(88);
        user1.setEmail("eino.dickinson@example.net");
        user1.setTitle("Mx.");
        user1.setLastname("Abshire");
        user1.setFirstname("Kirsten");
        user1.setAddress1("953 Gorczany Park");
        user1.setCity("Gabriellemouth");
        user1.setProvince("AB");
        user1.setCountry("CANADA");
        user1.setPostalcode("A1A 1A1");
        user1.setHomephone("02271930898");
        user1.setCellphone("(221)495-5872");

        REVIEW1.setReviewId(new Long(90));
        REVIEW1.setUserId(user1);
        RESULTLIST1.clear();
        RESULTLIST1.add(user1);

        //2 - review_id 63, user_id 63
        Users user2 = new Users();
        user2.setUserId(63);
        user2.setEmail("xjohns@example.com");
        user2.setTitle("Ms.");
        user2.setLastname("Hilll");
        user2.setFirstname("Donny");
        user2.setCompanyname("Kautzer and Sons");
        user2.setAddress1("349 Brian Lights");
        user2.setCity("West Arianestad");
        user2.setProvince("NT");
        user2.setCountry("CANADA");
        user2.setPostalcode("A1A 1A1");
        user2.setHomephone("012.199.1028");
        user2.setCellphone("(562)303-1263");

        REVIEW2.setReviewId(new Long(63));
        REVIEW2.setUserId(user2);
        RESULTLIST2.clear();
        RESULTLIST2.add(user2);

        //3 - review_id 46, user_id 52
        Users user3 = new Users();
        user3.setUserId(52);
        user3.setEmail("durward29@example.org");
        user3.setTitle("Mr.");
        user3.setLastname("Kuhlman");
        user3.setFirstname("Blanca");
        user3.setCompanyname("Barrows PLC");
        user3.setAddress1("7713 Jamar Way Suite 083");
        user3.setCity("Beerchester");
        user3.setProvince("NL");
        user3.setCountry("CANADA");
        user3.setPostalcode("B3B 4B4");
        user3.setHomephone("467-191-8914");
        user3.setCellphone("683-604-6955");

        REVIEW3.setReviewId(new Long(46));
        REVIEW3.setUserId(user3);
        RESULTLIST3.clear();
        RESULTLIST3.add(user3);

        //4 - review_id 99, user_id 95
        Users user4 = new Users();
        user4.setUserId(95);
        user4.setEmail("zwolf@example.com");
        user4.setTitle("Ms.");
        user4.setLastname("Moore");
        user4.setFirstname("Heidi");
        user4.setCompanyname("Zemlak Group");
        user4.setAddress1("789 Greta Springs Apt. 784");
        user4.setCity("Harmonchester");
        user4.setProvince("PE");
        user4.setCountry("CANADA");
        user4.setPostalcode("A1A 1A1");
        user4.setHomephone("536-993-1781");
        user4.setCellphone("(478)624-8222");

        REVIEW4.setReviewId(new Long(99));
        REVIEW4.setUserId(user4);
        RESULTLIST4.clear();
        RESULTLIST4.add(user4);

        //5 - review_id 40, user_id 17
        Users user5 = new Users();
        user5.setUserId(17);
        user5.setEmail("seth77@example.net");
        user5.setTitle("Mx.");
        user5.setLastname("Reichert");
        user5.setFirstname("Santa");
        user5.setCompanyname("Jacobson-Cassin");
        user5.setAddress1("6811 Luis Ford Apt. 389");
        user5.setCity("Treutelfurt");
        user5.setProvince("MB");
        user5.setCountry("CANADA");
        user5.setPostalcode("R1N 1H9");
        user5.setHomephone("(565)753-8787");
        user5.setCellphone("127-055-2665");

        REVIEW5.setReviewId(new Long(40));
        REVIEW5.setUserId(user5);
        RESULTLIST5.clear();
        RESULTLIST5.add(user5);

        LOG.debug("Populating ruleset for retrieving users by review with review_id/user_id pairs: {90, 88} // {63, 63} // {46, 52} // {99, 95} // {40, 17}");

        ParameterRule ruleSet = new ParameterRule("dynamic",
                new ParameterHolder<>(REVIEW1, RESULTLIST1),
                new ParameterHolder<>(REVIEW2, RESULTLIST2),
                new ParameterHolder<>(REVIEW3, RESULTLIST3),
                new ParameterHolder<>(REVIEW4, RESULTLIST4),
                new ParameterHolder<>(REVIEW5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getUsersByCredentialsRule() {
        //1 - user_id: 1
        CREDENTIALS1.setUserId(1);

        Users user1 = new Users();
        user1.setUserId(1);
        user1.setEmail("cst.send@gmail.com");

        RESULTLIST1.clear();
        RESULTLIST1.add(user1);

        //2 - 2
        CREDENTIALS2.setUserId(2);

        Users user2 = new Users();
        user2.setUserId(2);
        user2.setEmail("cst.receive@gmail.com");

        RESULTLIST2.clear();
        RESULTLIST2.add(user2);

        //9 - 9
        CREDENTIALS3.setUserId(9);

        Users user3 = new Users();
        user3.setUserId(9);
        user3.setEmail("erik88@example.org");

        RESULTLIST3.clear();
        RESULTLIST3.add(user3);

        //4 - 18
        CREDENTIALS4.setUserId(18);

        Users user4 = new Users();
        user4.setUserId(18);
        user4.setAddress1("charlotte.mante@example.net");

        RESULTLIST4.clear();
        RESULTLIST4.add(user4);

        //5 - 7
        CREDENTIALS5.setUserId(7);

        Users user5 = new Users();
        user5.setUserId(7);
        user5.setEmail("alverta30@example.org");

        RESULTLIST5.clear();
        RESULTLIST5.add(user5);

        ParameterRule ruleSet = new ParameterRule("dynamicRulePopulator",
                new ParameterHolder<>(CREDENTIALS1, RESULTLIST1),
                new ParameterHolder<>(CREDENTIALS2, RESULTLIST2),
                new ParameterHolder<>(CREDENTIALS3, RESULTLIST3),
                new ParameterHolder<>(CREDENTIALS4, RESULTLIST4),
                new ParameterHolder<>(CREDENTIALS5, RESULTLIST5)
        );

        return ruleSet;
    }
}
