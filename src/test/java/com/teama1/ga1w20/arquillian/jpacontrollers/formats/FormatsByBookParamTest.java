package com.teama1.ga1w20.arquillian.jpacontrollers.formats;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A test class for FormatsJpaController.
 *
 * Formats may be retrieved by: - Book (Books object or isbn)
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class FormatsByBookParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(FormatsByBookParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Jpa Controller
    @Inject
    private FormatsJpaController formatController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //RuleSet containing testCases to test
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Books, List<Formats>> dynamicParameterHolder;

    //Field representing the actual value(s)
    List<Formats> actualOutputList;

    private final Books BOOK1 = new Books();
    private final Books BOOK2 = new Books();
    private final Books BOOK3 = new Books();
    private final Books BOOK4 = new Books();

    private final List<Formats> RESULTLIST1 = new ArrayList<>();
    private final List<Formats> RESULTLIST2 = new ArrayList<>();
    private final List<Formats> RESULTLIST3 = new ArrayList<>();
    private final List<Formats> EMPTY_RESULTLIST = new ArrayList<>();

    /**
     * Constructor
     */
    public FormatsByBookParamTest() {
        //Case with one result
        BOOK1.setIsbn(9780385320436L);
        RESULTLIST1.clear();
        RESULTLIST1.add(new Formats(16, "lit", "Microsoft Reader"));

        //Case with 2 results
        BOOK2.setIsbn(9780810984226L);
        RESULTLIST2.clear();
        RESULTLIST2.add(new Formats(10, "epub", "EPUB(IDPF)"));
        RESULTLIST2.add(new Formats(24, "pdf", "Portable Document Format"));

        //Case with 3 results
        BOOK3.setIsbn(9780606144841L);
        RESULTLIST3.clear();
        RESULTLIST3.add(new Formats(4, "cbt", "Comic Book Archive"));
        RESULTLIST3.add(new Formats(18, "mobi", "Mobipocket"));
        RESULTLIST3.add(new Formats(3, "cb7", "Comic Book Archive"));

        //Case with 0 results
        BOOK4.setIsbn(30L); //Inexistent book

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(BOOK1, RESULTLIST1),
                new ParameterHolder<>(BOOK2, RESULTLIST2),
                new ParameterHolder<>(BOOK3, RESULTLIST3),
                new ParameterHolder<>(BOOK4, EMPTY_RESULTLIST));
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Tests that the correct number of formats are returned when retrieving
     * formats by book isbn.
     */
    @Test
    public void formatsByBookIsbn_test() {
        actualOutputList = formatController.
                getFormatsByBook(dynamicParameterHolder.getInput().getIsbn());
        List<Formats> expectedOutputList = dynamicParameterHolder.getListOutput();

        //Test list size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue(actualOutputList.containsAll(dynamicParameterHolder.getListOutput()));
        assertTrue(dynamicParameterHolder.getListOutput().containsAll(actualOutputList));
    }

    /**
     * Ensures the correct formats are retrieved for a given book object.
     */
    @Test
    public void formatsByBookObject_test() {
        actualOutputList = formatController.
                getFormatsByBook(dynamicParameterHolder.getInput());
        List<Formats> expectedOutputList = dynamicParameterHolder.getListOutput();

        //Test list size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue(actualOutputList.containsAll(expectedOutputList));
        assertTrue(dynamicParameterHolder.getListOutput().containsAll(actualOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
