package com.teama1.ga1w20.arquillian.jpacontrollers.rssfeeds;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Rssfeeds;
import com.teama1.ga1w20.persistence.jpacontrollers.RssfeedsJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class RssfeedsJPATest {

    private final static Logger LOG = LoggerFactory.getLogger(RssfeedsJPATest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    private RssfeedsJpaController controller;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void testGetNumActiveFeeds() {
        List<Rssfeeds> list = controller.getActiveFeeds();

        assertEquals(6, list.size());
    }

    @Test
    public void testGetActiveFeeds() {
        List<Rssfeeds> expectedResultList = new ArrayList<>();
        expectedResultList.add(new Rssfeeds(1, "http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml", true));
        expectedResultList.add(new Rssfeeds(3, "http://feeds.bbci.co.uk/news/world/africa/rss.xml", true));
        expectedResultList.add(new Rssfeeds(4, "http://feeds.bbci.co.uk/news/world/latin_america/rss.xml", true));
        expectedResultList.add(new Rssfeeds(5, "https://rss.nytimes.com/services/xml/rss/nyt/World.xml", true));
        expectedResultList.add(new Rssfeeds(7, "https://www.npr.org/rss/podcast.php?id=510298", true));
        expectedResultList.add(new Rssfeeds(10, "https://www.erh.noaa.gov/car/RSS_feeds/car_top_news.xml", true));

        List<Rssfeeds> actualResultList = controller.getActiveFeeds();

        assertTrue(actualResultList.containsAll(expectedResultList) && expectedResultList.containsAll(actualResultList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
