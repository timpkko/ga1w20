package com.teama1.ga1w20.arquillian.jpacontrollers.credentials;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.CredentialsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test CredentialsJpaController getCredentialsCount()
 * getCredentialsByUser(User) exception throwing getCredentialsByUserId(int
 * userId) exception throwing
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class CredentialsJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(CredentialsJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    CredentialsJpaController credentialsJpaController;

    //Total number of entities inserted in the database with the SQL script
    private final static int EXPECTED_CREDENTIALS_COUNT = 33;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getCredentialsCountTest() {
        int actualCount = credentialsJpaController.getCredentialsCount();
        int expectedCount = EXPECTED_CREDENTIALS_COUNT;
        Assert.assertEquals(expectedCount, actualCount);
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS - EXCEPTION THROWING
    ////////////////////////////////////////////////////////////////////////////
    @Test(expected = NoResultException.class)
    public void getCredentialsByUserObject_NoResultTest() throws
            NonexistentEntityException, NonUniqueResultException, NoResultException {
        credentialsJpaController.getCredentialsByUser(new Users(200));
        fail("The expected exception was not thrown");
    }

    @Test(expected = NoResultException.class)
    public void getCredentialsByUserId_NoResultTest() throws
            NonexistentEntityException, NonUniqueResultException, NoResultException {
        credentialsJpaController.getCredentialsByUserId(200);
        fail("The expected exception was not thrown");
    }

    @Test(expected = NullPointerException.class)
    public void getCredentialsByUserObject_NullTest() throws
            NonexistentEntityException, NonUniqueResultException, NoResultException {
        credentialsJpaController.getCredentialsByUser((Users) null);
        fail("The expected exception was not thrown");
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
