package com.teama1.ga1w20.arquillian.jpacontrollers.books.bystring;

import com.teama1.ga1w20.arquillian.jpacontrollers.books.BooksTestUtils;
import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to test the getBooksByPublisherHint(String hint) method of the
 * BooksJpaController.
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class GetByPublisherNameParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetByPublisherNameParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Jpa Controller
    @Inject
    private BooksJpaController bookController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<String, List<Books>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<Books> actualOutputList;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    BooksTestUtils booksTestUtils = new BooksTestUtils();

    String input1, input2, input3, input4, input5, input6;

    private final List<Books> resultList1 = new ArrayList<>();
    private final List<Books> resultList2 = new ArrayList<>();
    private final List<Books> resultList3 = new ArrayList<>();
    private final List<Books> emptyResultList = new ArrayList<>();
    private List<Books> resultList5 = new ArrayList<>();

    /**
     * Constructor.
     */
    public GetByPublisherNameParamTest() {
        //Case : 1 result
        resultList1.clear();
        input1 = "ulet"; //Publisher 40
        resultList1.add(booksTestUtils.createTestBook(9780692848388L,
                "What Should Danny Do?", 40));
        resultList1.add(booksTestUtils.createTestBook(9780763693558L,
                "Alma and How She Got Her Name", 40));
        resultList1.add(booksTestUtils.createTestBook(9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)", 40));

        //Case
        input2 = "om   "; //1 publisher with 1 book (trailing spaces should be disregarded)
        resultList2.clear();
        resultList2.add(booksTestUtils.createTestBook(9780375832291L,
                "Babymouse #1: Queen of the World!",
                42));

        //Case : many publishers (leading spaces should be disregaded
        input3 = "   oo";
        resultList3.clear();
        resultList3.add(booksTestUtils.createTestBook(9780399246531L,
                "The Day You Begin", 17));
        resultList3.add(booksTestUtils.createTestBook(9780439706407L,
                "Bone #1: Out from Boneville", 45));
        resultList3.add(booksTestUtils.createTestBook(9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest,"
                + " Toughest, Cleverest, Shyest and Most "
                + "Surprising Animals on Earth", 24));
        resultList3.add(booksTestUtils.createTestBook(9780692848388L,
                "What Should Danny Do?", 40));
        resultList3.add(booksTestUtils.createTestBook(9780763693558L,
                "Alma and How She Got Her Name", 40));
        resultList3.add(booksTestUtils.createTestBook(9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)", 40));

        //Case : 1 publisher, 1 book, but marked as removed so should not be returned
        input4 = "ov";
        emptyResultList.clear();

        //Case :  Blank string, should return all books
        input5 = "      ";
        resultList5 = booksTestUtils.getAllBooksExpected();

        //Case : Non existent publisher
        input6 = "za";
        emptyResultList.clear();

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(input1, resultList1),
                new ParameterHolder<>(input2, resultList2),
                new ParameterHolder<>(input3, resultList3),
                new ParameterHolder<>(input4, emptyResultList),
                new ParameterHolder<>(input5, resultList5),
                new ParameterHolder<>(input6, emptyResultList)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Test if the actual list of Books found is the same as the expected list
     * of Books found
     */
    @Test
    public void getBooksByPublisherNameHint_test() {
        actualOutputList = bookController.
                getBooksByPublisherHint(dynamicParameterHolder.getInput());
        List<Books> expectedOutputList = dynamicParameterHolder.getListOutput();

        //Test for list content and size
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeTestScript(BooksTestUtils.BOOK_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(BooksTestUtils.class.getPackage())
                .addPackage(GetByPublisherNameParamTest.class.getPackage());
    }
}
