package com.teama1.ga1w20.arquillian.jpacontrollers.publishers;

import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Publishers;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Populates rulesets for parameterized testing of PublishersJpaController.
 * Publishers may be retrieved by: - Book - Name
 *
 * @author Eira Garrett
 */
public class PublishersRulePopulator {

    private final static Logger LOG = LoggerFactory.getLogger(PublishersRulePopulator.class);

    private final Books BOOK1 = new Books();
    private final Books BOOK2 = new Books();
    private final Books BOOK3 = new Books();
    private final Books BOOK4 = new Books();
    private final Books BOOK5 = new Books();

    private final String NAME1 = "Scholastic GRAPHIX";
    private final String NAME2 = "scholasTic grAphIx";
    private final String NAME3 = "harpercollins";
    private final String NAME4 = "Balzer + Bray";
    private final String NAME5 = "e";

    private final List<Publishers> RESULTLIST1 = new ArrayList<>();
    private final List<Publishers> RESULTLIST2 = new ArrayList<>();
    private final List<Publishers> RESULTLIST3 = new ArrayList<>();
    private final List<Publishers> RESULTLIST4 = new ArrayList<>();
    private final List<Publishers> RESULTLIST5 = new ArrayList<>();

    public PublishersRulePopulator() {
    }

    public ParameterRule getPublishersByBookRule() {
        //1 - isbn: 9780062941008, publisher_id: 28
        BOOK1.setIsbn(9780062941008L);

        Publishers p1 = new Publishers();
        p1.setPublisherId(28);
        p1.setName("HarperCollins");

        RESULTLIST1.clear();
        RESULTLIST1.add(p1);

        //2 - isbn: 9780385265201, publisher_id: 47
        BOOK2.setIsbn(9780385265201L);

        Publishers p2 = new Publishers();
        p2.setPublisherId(47);
        p2.setName("Three Rivers Press");

        RESULTLIST2.clear();
        RESULTLIST2.add(p2);

        //3 - isbn: 9780439706407, publisher_id: 45
        BOOK3.setIsbn(9780439706407L);

        Publishers p3 = new Publishers();
        p3.setPublisherId(45);
        p3.setName("Cartoon Books");

        RESULTLIST3.clear();
        RESULTLIST3.add(p3);

        //4 - isbn: 9780486468211, publisher_id: 21
        BOOK4.setIsbn(9780486468211L);

        Publishers p4 = new Publishers();
        p4.setPublisherId(21);
        p4.setName("Dover Publications");

        RESULTLIST4.clear();
        RESULTLIST4.add(p4);

        //5 - isbn: 9780545132060, publisher_id: 39
        BOOK5.setIsbn(9780545132060L);

        Publishers p5 = new Publishers();
        p5.setPublisherId(39);
        p5.setName("Scholastic GRAPHIX");

        RESULTLIST5.clear();
        RESULTLIST5.add(p5);

        LOG.debug("Populating ruleset for finding publishers by book with expected results: (isbn: 9780062941008, pubId: 28) // (isbn: 9780385265201, pubId 47) // (isbn: 9780439706407, pubId: 45) // (isbn: 9780486468211, pubId: 21) // (isbn: 9780545132060, pubId: 39)");

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(BOOK1, RESULTLIST1),
                new ParameterHolder<>(BOOK2, RESULTLIST2),
                new ParameterHolder<>(BOOK3, RESULTLIST3),
                new ParameterHolder<>(BOOK4, RESULTLIST4),
                new ParameterHolder<>(BOOK5, RESULTLIST5)
        );

        return ruleSet;
    }

    public ParameterRule getPublishersByNameRule() {
        //1 - Scholastic GRAPHIX
        Publishers p1 = new Publishers();
        p1.setPublisherId(39);
        p1.setName("Scholastic GRAPHIX");

        RESULTLIST1.clear();
        RESULTLIST1.add(p1);

        //2 - scholasTic grAphIx
        Publishers p2 = new Publishers();
        p2.setPublisherId(39);
        p2.setName("Scholastic GRAPHIX");

        RESULTLIST2.clear();
        RESULTLIST2.add(p2);

        //3 - harpercollins
        Publishers p3 = new Publishers();
        p3.setPublisherId(28);
        p3.setName("HarperCollins");

        RESULTLIST3.clear();
        RESULTLIST3.add(p3);

        //4 - Balzer + Bray
        Publishers p4 = new Publishers();
        p4.setPublisherId(8);
        p4.setName("Balzer + Bray");

        RESULTLIST4.clear();
        RESULTLIST4.add(p4);

        //5 - e
        Publishers p5 = new Publishers();
        p5.setPublisherId(8);
        p5.setName("Balzer + Bray");

        Publishers p6 = new Publishers();
        p6.setPublisherId(13);
        p6.setName("Feiwel & Friends");

        Publishers p7 = new Publishers();
        p7.setPublisherId(17);
        p7.setName("Nancy Paulsen Books");

        Publishers p8 = new Publishers();
        p8.setPublisherId(18);
        p8.setName("Candlewick Press");

        Publishers p9 = new Publishers();
        p9.setPublisherId(21);
        p9.setName("Dover Publications");

        Publishers p10 = new Publishers();
        p10.setPublisherId(23);
        p10.setName("Elon Books");

        Publishers p11 = new Publishers();
        p11.setPublisherId(24);
        p11.setName("HMH Books for Young Readers");

        Publishers p12 = new Publishers();
        p12.setPublisherId(26);
        p12.setName("Grosset & Dunlap");

        Publishers p13 = new Publishers();
        p13.setPublisherId(28);
        p13.setName("HarperCollins");

        Publishers p14 = new Publishers();
        p14.setPublisherId(30);
        p14.setName("Delacorte Books for Young Readers");

        Publishers p15 = new Publishers();
        p15.setPublisherId(38);
        p15.setName("First Second");

        Publishers p16 = new Publishers();
        p16.setPublisherId(40);
        p16.setName("Amulet Books");

        Publishers p17 = new Publishers();
        p17.setPublisherId(42);
        p17.setName("Random House");

        Publishers p18 = new Publishers();
        p18.setPublisherId(47);
        p18.setName("Three Rivers Press");

        RESULTLIST5.clear();
        RESULTLIST5.add(p5);
        RESULTLIST5.add(p6);
        RESULTLIST5.add(p7);
        RESULTLIST5.add(p8);
        RESULTLIST5.add(p9);
        RESULTLIST5.add(p10);
        RESULTLIST5.add(p11);
        RESULTLIST5.add(p12);
        RESULTLIST5.add(p13);
        RESULTLIST5.add(p14);
        RESULTLIST5.add(p15);
        RESULTLIST5.add(p16);
        RESULTLIST5.add(p17);
        RESULTLIST5.add(p18);

        LOG.debug("Populating ruleset for retrieving publishers by name with expected results: (Scholastic GRAPHIX - pubId 39) // (scholasTic grAphIx - pubId 39) // (harpercollins - pubId 28) // (Balzer + Bray - pubId 8) // (e - pubIds{8, 13, 17, 18, 21, 23, 24, 26, 28, 30, 38, 40, 42, 47})");

        ParameterRule ruleSet = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(NAME1, RESULTLIST1),
                new ParameterHolder<>(NAME2, RESULTLIST2),
                new ParameterHolder<>(NAME3, RESULTLIST3),
                new ParameterHolder<>(NAME4, RESULTLIST4),
                new ParameterHolder<>(NAME5, RESULTLIST5)
        );

        return ruleSet;
    }
}
