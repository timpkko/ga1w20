package com.teama1.ga1w20.arquillian.jpacontrollers.orders;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.*;
import com.teama1.ga1w20.persistence.jpacontrollers.OrdersJpaController;
import java.util.Date;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test methods in OrdersJpaController getOrdersCount()
 * getOrdersByUser(Users) exception throwing getOrdersByUser(Integer) exception
 * throwing getOrderByDate(Date) exception throwing getOrdersByDate(Date, Date)
 * exception throwing getOrdersContainingBook(Book) exception throwing
 * getNumberOfOrdersContainingBook(Book) exception throwing
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class OrdersJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(OrdersJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubsTest")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    OrdersJpaController ordersJpaController;

    //Total number of entities inserted in the database with the SQL script
    private final static int EXPECTED_COUNT = 34;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getOrdersCountTest() {
        int actualCount = ordersJpaController.getOrdersCount();
        int expectedCount = EXPECTED_COUNT;
        Assert.assertEquals(expectedCount, actualCount);
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS - EXCEPTION THROWING
    ////////////////////////////////////////////////////////////////////////////
    @Test(expected = NullPointerException.class)
    public void getOrdersByDate_NullTest() {
        ordersJpaController.getOrdersByDate((Date) null);
    }

    @Test(expected = NullPointerException.class)
    public void getOrdersBetweenDates_NullTest() {
        ordersJpaController.getOrdersByDate((Date) null, (Date) null);
    }

    @Test(expected = NullPointerException.class)
    public void getOrdersByUserObject_NullTest() {
        ordersJpaController.getOrdersByUser((Users) null);
    }

    @Test(expected = NullPointerException.class)
    public void getOrdersByUserId_NullTest() {
        ordersJpaController.getOrdersByUser((Integer) null);
    }

    @Test(expected = NullPointerException.class)
    public void getOrdersContainingBook_NullTest() {
        ordersJpaController.getOrdersContainingBook((Books) null);
    }

    @Test(expected = NullPointerException.class)
    public void getNumberOfOrdersContaingBook_NullTest() {
        ordersJpaController.getNumberOfOrdersContainingBook((Books) null);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeTestScript(OrdersTestUtils.ORDER_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
