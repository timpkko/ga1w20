package com.teama1.ga1w20.arquillian.jpacontrollers.banners;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Banners;
import com.teama1.ga1w20.persistence.jpacontrollers.BannersJpaController;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import static org.assertj.core.api.Assertions.assertThat;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testing BannersJpaController - getBannersCount() - findBannersEntities()
 *
 * And custom query - getAllActiveBanners(). thanks to running different scripts
 * which populate the Banners table with a different number of active banners.
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class BannersJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(BannersJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Utils
    private final ArquillianUtils testUtils = new ArquillianUtils();
    private final BannersTestUtils bannersTestUtils = new BannersTestUtils();

    //Jpa Controller
    @Inject
    private BannersJpaController bannerController;

    private final static String BANNER_SCRIPT_0 = "test-scripts/banners/populateBannersTable0.sql";
    private final static String BANNER_SCRIPT_1 = "test-scripts/banners/populateBannersTable1.sql";
    private final static String BANNER_SCRIPT_2 = "test-scripts/banners/populateBannersTable2.sql";
    private final static String BANNER_SCRIPT_3 = "test-scripts/banners/populateBannersTable3.sql";
    private final static String BANNER_SCRIPT_ALL = "test-scripts/banners/populateBannersTableAll.sql";

    ////////////////////////////////////////////////////////////////////////////
    //TESTS FIND/COUNT
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getBannersCountTest() throws SQLException {
        //Execute default test script
        testUtils.executeDefaultTestScript(dataSource);

        //Retrieve data
        int allBannersCount = bannerController.getBannersCount();

        //Test
        assertSame(allBannersCount, bannersTestUtils.getTotalNumOfBannersExpected());
    }

    @Test
    public void findBannerEntities_test() throws SQLException {
        //Execute default test script
        testUtils.executeDefaultTestScript(dataSource);

        List<Banners> actualList = bannerController.findBannersEntities();
        List<Banners> expectedList = bannersTestUtils.getAllBannersExpected();

        //Test size and content
        assertThat(actualList).hasSize(expectedList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedList.containsAll(actualList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualList.containsAll(expectedList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS getAllActiveBanners
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Populate the Banners table with banners but none active.
     */
    @Test
    public void getAllActiveBanners_noneActive_test() {
        //Populate Banners table with no active banners
        testUtils.executeTestScript(BANNER_SCRIPT_0, dataSource);

        //Retrieve
        List<Banners> actualListOfActiveBanners = bannerController.getAllActiveBanners();

        //Test
        assertTrue(actualListOfActiveBanners.isEmpty());
    }

    /**
     * Populate the Banners table with banners and only one active.
     */
    @Test
    public void getAllActiveBanners_oneActive_test() {
        //Populate Banners table with only one active banner.
        testUtils.executeTestScript(BANNER_SCRIPT_1, dataSource);

        //Expected
        List<Banners> expectedListOfActiveBanners = new ArrayList<Banners>();
        expectedListOfActiveBanners.add(new Banners(20, "http://www.christiansen.biz/", true));

        //Actual
        List<Banners> actualListOfActiveBanners = bannerController.getAllActiveBanners();

        //Test
        assertEquals(expectedListOfActiveBanners.size(), actualListOfActiveBanners.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedListOfActiveBanners.containsAll(actualListOfActiveBanners));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualListOfActiveBanners.containsAll(expectedListOfActiveBanners));
    }

    /**
     * Populate the Banners table with banners including 2 active ones.
     */
    @Test
    public void getAllActiveBanners_twoActives_test() {
        //Populate with test script for this specific test method
        testUtils.executeTestScript(BANNER_SCRIPT_2, dataSource);

        //Expected
        List<Banners> expectedListOfActiveBanners = new ArrayList<Banners>();
        expectedListOfActiveBanners.add(new Banners(20, "http://www.christiansen.biz/", true));
        expectedListOfActiveBanners.add(new Banners(10, "http://www.ziemann.com/", true));

        //Actual
        List<Banners> actualListOfActiveBanners = bannerController.getAllActiveBanners();

        //Test list size and content
        assertEquals(expectedListOfActiveBanners.size(), actualListOfActiveBanners.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedListOfActiveBanners.containsAll(actualListOfActiveBanners));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualListOfActiveBanners.containsAll(expectedListOfActiveBanners));
    }

    /**
     * Populate the Banners table with banners including 3 active ones.
     */
    @Test
    public void getAllActiveBanners_threeActives_test() {
        //Populate with test script for this specific test method
        testUtils.executeTestScript(BANNER_SCRIPT_3, dataSource);

        //Expected
        List<Banners> expectedListOfActiveBanners = new ArrayList<Banners>();
        expectedListOfActiveBanners.add(new Banners(20, "http://www.christiansen.biz/", true));
        expectedListOfActiveBanners.add(new Banners(10, "http://www.ziemann.com/", true));
        expectedListOfActiveBanners.add(new Banners(18, "http://www.runolfsdottir.net/", true));

        //Actual
        List<Banners> actualListOfActiveBanners = bannerController.getAllActiveBanners();

        //Test list size and content
        assertEquals(expectedListOfActiveBanners.size(), actualListOfActiveBanners.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedListOfActiveBanners.containsAll(actualListOfActiveBanners));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualListOfActiveBanners.containsAll(expectedListOfActiveBanners));
    }

    /**
     * Populate the Banners table with banners which are ALL active.
     */
    @Test
    public void getAllActiveBanners_AllActives_test() {
        //Populate with test script for this specific test method
        testUtils.executeTestScript(BANNER_SCRIPT_ALL, dataSource);

        //Actual
        List<Banners> actualListOfActiveBanners = bannerController.getAllActiveBanners();

        //Expected
        List<Banners> expectedListOfActiveBanners = new ArrayList<Banners>();
        expectedListOfActiveBanners.add(new Banners(1, "http://schadenmraz.com/"));
        expectedListOfActiveBanners.add(new Banners(2, "http://www.keeling.org/"));
        expectedListOfActiveBanners.add(new Banners(3, "http://www.sawayn.com/"));
        expectedListOfActiveBanners.add(new Banners(4, "http://www.cassin.com/"));
        expectedListOfActiveBanners.add(new Banners(5, "http://www.reillyzemlak.com/"));
        expectedListOfActiveBanners.add(new Banners(6, "http://www.wolff.biz/"));
        expectedListOfActiveBanners.add(new Banners(7, "http://dickinsonbernhard.com/"));
        expectedListOfActiveBanners.add(new Banners(8, "http://brekke.com/"));
        expectedListOfActiveBanners.add(new Banners(9, "http://www.hueldare.com/"));
        expectedListOfActiveBanners.add(new Banners(10, "http://www.ziemann.com/"));
        expectedListOfActiveBanners.add(new Banners(11, "http://www.braunborer.net/"));
        expectedListOfActiveBanners.add(new Banners(12, "http://hartmannbailey.com/"));
        expectedListOfActiveBanners.add(new Banners(13, "http://schinner.info/"));
        expectedListOfActiveBanners.add(new Banners(14, "http://reilly.com/"));
        expectedListOfActiveBanners.add(new Banners(15, "http://www.zulauf.com/"));
        expectedListOfActiveBanners.add(new Banners(16, "http://kunze.com/"));
        expectedListOfActiveBanners.add(new Banners(17, "http://www.barton.com/"));
        expectedListOfActiveBanners.add(new Banners(18, "http://www.runolfsdottir.net/"));
        expectedListOfActiveBanners.add(new Banners(19, "http://grimeskshlerin.info/"));
        expectedListOfActiveBanners.add(new Banners(20, "http://www.christiansen.biz/"));

        //Test list size and content
        assertEquals(expectedListOfActiveBanners.size(), actualListOfActiveBanners.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedListOfActiveBanners.containsAll(actualListOfActiveBanners));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualListOfActiveBanners.containsAll(expectedListOfActiveBanners));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        final WebArchive webArchive = ArchiveUtils.getWebArchive()
                .addAsResource(BANNER_SCRIPT_0)
                .addAsResource(BANNER_SCRIPT_1)
                .addAsResource(BANNER_SCRIPT_2)
                .addAsResource(BANNER_SCRIPT_3)
                .addAsResource(BANNER_SCRIPT_ALL)
                .addPackage(BannersTestUtils.class.getPackage());
        return webArchive;
    }
}
