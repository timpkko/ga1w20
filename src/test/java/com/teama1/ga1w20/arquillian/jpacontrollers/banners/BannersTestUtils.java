package com.teama1.ga1w20.arquillian.jpacontrollers.banners;

import com.teama1.ga1w20.persistence.entities.Banners;
import java.util.ArrayList;
import java.util.List;

/**
 * Utils class holding navigationBean methods allowing, among other things to
 * access objects representing all the entities inserted in the database when
 * populated by the default test script.
 *
 * @see ArquillianUtils for default test script
 * @author Camillia E.
 */
public class BannersTestUtils {

    private List<Banners> allEntitiesList;

    public List<Banners> getAllBannersExpected() {
        if (allEntitiesList != null) {
            return allEntitiesList;
        }

        return createList();
    }

    public int getTotalNumOfBannersExpected() {
        if (allEntitiesList != null) {
            return allEntitiesList.size();
        }

        return createList().size();
    }

    private List<Banners> createList() {
        allEntitiesList = new ArrayList<>();
        allEntitiesList.add(new Banners(1, "http://schadenmraz.com/"));
        allEntitiesList.add(new Banners(2, "http://www.keeling.org/"));
        allEntitiesList.add(new Banners(3, "http://www.sawayn.com/"));
        allEntitiesList.add(new Banners(4, "http://www.cassin.com/"));
        allEntitiesList.add(new Banners(5, "http://www.reillyzemlak.com/"));
        allEntitiesList.add(new Banners(6, "http://www.wolff.biz/"));
        allEntitiesList.add(new Banners(7, "http://dickinsonbernhard.com/"));
        allEntitiesList.add(new Banners(8, "http://brekke.com/"));
        allEntitiesList.add(new Banners(9, "http://www.hueldare.com/"));
        allEntitiesList.add(new Banners(10, "http://www.ziemann.com/"));
        allEntitiesList.add(new Banners(11, "http://www.braunborer.net/"));
        allEntitiesList.add(new Banners(12, "http://hartmannbailey.com/"));
        allEntitiesList.add(new Banners(13, "http://schinner.info/"));
        allEntitiesList.add(new Banners(14, "http://reilly.com/"));
        allEntitiesList.add(new Banners(15, "http://www.zulauf.com/"));
        allEntitiesList.add(new Banners(16, "http://kunze.com/"));
        allEntitiesList.add(new Banners(17, "http://www.barton.com/"));
        allEntitiesList.add(new Banners(18, "http://www.runolfsdottir.net/"));
        allEntitiesList.add(new Banners(19, "http://grimeskshlerin.info/"));
        allEntitiesList.add(new Banners(20, "http://www.christiansen.biz/"));
        return allEntitiesList;
    }
}
