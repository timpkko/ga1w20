package com.teama1.ga1w20.arquillian.jpacontrollers.authors;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import static org.assertj.core.api.Assertions.assertThat;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class for methods in AuthorsJpaController : - findAuthorEntities() -
 * getAuthoursCount() - getAuthorsByBook(Book) exception throwing -
 * getAuthorsByBook(Long isbn) exception throwing - getAuthorsLikeName(String)
 * exception throwing
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class AuthorsJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(AuthorsJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Utils
    private final ArquillianUtils utils = new ArquillianUtils();
    private final AuthorsTestUtils authorsTestUtils = new AuthorsTestUtils();

    //Jpa Controller
    @Inject
    private AuthorsJpaController authorController;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS FIND/COUNT
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getAuthorsCountTest() throws SQLException {
        int allAuthorsCount = authorController.getAuthorsCount();
        assertSame(allAuthorsCount, authorsTestUtils.getTotalNumOfAuthorsExpected());
    }

    @Test
    public void findAuthorEntities_listSizeTest() throws SQLException {
        List<Authors> allAuthors = authorController.findAuthorsEntities();
        assertThat(allAuthors).hasSize(authorsTestUtils.getTotalNumOfAuthorsExpected());
    }

    @Test
    public void findAuthorEntities_listContentTest() throws SQLException {
        List<Authors> actualList = authorController.findAuthorsEntities();
        List<Authors> expectedList = authorsTestUtils.getAllAuthorsExpected();

        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedList.containsAll(actualList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualList.containsAll(expectedList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS EXCEPTION THROWING
    ////////////////////////////////////////////////////////////////////////////
    @Test(expected = NullPointerException.class)
    public void getAuthorsByBookObject_NullTest() {
        authorController.getAuthorsByBook((Books) null);
    }

    @Test(expected = NullPointerException.class)
    public void getAuthorsByBookIsbn_NullTest() {
        authorController.getAuthorsByBook((Long) null);
    }

    @Test(expected = NullPointerException.class)
    public void getAuthorsByName_NullTest() {
        authorController.getAuthorLikeName(null);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive().addPackage(AuthorsTestUtils.class.getPackage());
    }
}
