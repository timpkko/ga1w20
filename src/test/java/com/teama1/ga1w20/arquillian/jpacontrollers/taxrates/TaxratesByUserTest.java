package com.teama1.ga1w20.arquillian.jpacontrollers.taxrates;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Taxrates;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.TaxratesJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class TaxratesByUserTest {

    private final static Logger LOG = LoggerFactory.getLogger(TaxratesByUserTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //JPA Controller
    @Inject
    private TaxratesJpaController taxratesController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Ruleset populator
    private TaxratesRulePopulator rulePopulator;

    @Rule
    public ParameterRule ratesByUser;
    private ParameterHolder<Users, List<Taxrates>> dynamic;
    private ParameterHolder<Users, List<Taxrates>> resultHolder;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    public TaxratesByUserTest() {
        rulePopulator = new TaxratesRulePopulator();
        ratesByUser = rulePopulator.getTaxratesByUserRule();
    }

    /**
     * Ensures that the correct Taxrates objects are returned based on a given
     * User object.
     */
    @Test
    public void testRatesByUser() {
        Taxrates rate = new Taxrates();

        try {
            rate = taxratesController.getRatesForUser(dynamic.getInput());
        } catch (NonexistentEntityException | NoResultException | NonUniqueResultException ex) {
            LOG.error("Something went funky: ", ex);
        }

        assertEquals(dynamic.getListOutput().get(0), rate);
    }

    /**
     * Ensures that the correct Taxrates object is returned based on a given
     * userId.
     */
    public void testRatesByUserId() {
        Taxrates rate = new Taxrates();

        try {
            rate = taxratesController.getRatesForUser(dynamic.getInput().getUserId());
        } catch (NonexistentEntityException | NoResultException | NonUniqueResultException ex) {
            LOG.error("Something went funky: ", ex);
        }

        assertEquals(dynamic.getListOutput().get(0), rate);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(TaxratesRulePopulator.class.getPackage());
    }
}
