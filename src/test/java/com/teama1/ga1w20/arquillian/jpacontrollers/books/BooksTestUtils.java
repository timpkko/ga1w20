package com.teama1.ga1w20.arquillian.jpacontrollers.books;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Publishers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;

/**
 * Utils class holding navigationBean methods allowing, among other things to
 * access objects representing all the entities inserted in the database when
 * populated by the book test script.
 *
 * @see ArquillianUtils for default test script
 * @author Camillia E.
 */
public class BooksTestUtils {

    public final static String BOOK_TESTS_SCRIPT = ArchiveUtils.BOOK_TESTS_SCRIPT;

    private List<Books> allEntitiesList;

    private List<Books> booksOnSale;

    /**
     * Get a list of objects representing all the entities inserted in the
     * database when populated by the default test script.
     *
     * @return List of objects
     */
    public List<Books> getAllBooksExpected() {
        if (allEntitiesList != null) {
            return allEntitiesList;
        }
        return createAllEntitiesList();
    }

    /**
     * Get the number of entities inserted in the database when populated by the
     * default test script.
     *
     * @return
     */
    public int getTotalNumOfBooksExpected() {
        if (allEntitiesList != null) {
            return allEntitiesList.size();
        }
        return createAllEntitiesList().size();
    }

    /**
     * Get a list of objects representing the book entities inserted in the
     * database when populated by the default test script, which are on sale.
     *
     * @return
     */
    public List<Books> getExpectedBooksOnSale() {
        if (booksOnSale == null) {
            booksOnSale = new ArrayList<>();
            booksOnSale.add(createTestBook(9780312625993L,
                    "The Forgiveness Garden", 13));
            booksOnSale.add(createTestBook(9780375832291L,
                    "Babymouse #1: Queen of the World!", 42));
            booksOnSale.add(createTestBook(9780399246531L,
                    "The Day You Begin", 17));
            booksOnSale.add(createTestBook(9780545132060L, "Smile",
                    39));
            booksOnSale.add(createTestBook(9780547557991L,
                    "The Animal Book: A Collection of the Fastest, Fiercest,"
                    + " Toughest, Cleverest, Shyest and Most "
                    + "Surprising Animals on Earth", 24));
            booksOnSale.add(createTestBook(9780606144841L,
                    "American Born Chinese", 38));
            booksOnSale.add(createTestBook(9780810984226L,
                    "How Mirka Got Her Sword (Hereville Book 1)",
                    40));
        }
        return booksOnSale;
    }

    /**
     * Allows taking the INSERT INTO statement from the populate script and
     * translate it to an object.
     *
     * @return Object representation of the arguments used in the INSERT INTO
     * MySQL script.
     */
    public Books createTestBook(Long isbn, String title, int publisherId) {
        return new Books(isbn, title, new Publishers(publisherId));
    }

    /**
     * Method to output the comparison of two lists holding books. It orders
     * them by isbn and print them.
     *
     * @param expectedOutputList
     * @param actualOutputList
     */
    public void logBookListsComparison(Logger classLog,
            List<Books> expectedOutputList, List<Books> actualOutputList) {
        Collections.sort(expectedOutputList,
                (o1, o2) -> o1.getIsbn().compareTo(o2.getIsbn()));
        Collections.sort(actualOutputList,
                (o1, o2) -> o1.getIsbn().compareTo(o2.getIsbn()));

        classLog.debug("\nExpected List: (" + expectedOutputList.size() + ") "
                + expectedOutputList);
        classLog.debug("\nActual List: (" + actualOutputList.size() + ") "
                + actualOutputList);
    }

    /**
     * Method to output the comparison of two lists holding books. Its orders
     * them by isbn, print them and print each book individually.
     *
     * @param expectedOutputList
     * @param actualOutputList
     */
    public void logBookListsComparisonWithDetails(Logger classLog,
            List<Books> expectedOutputList, List<Books> actualOutputList) {
        Collections.sort(expectedOutputList,
                (o1, o2) -> o1.getIsbn().compareTo(o2.getIsbn()));
        Collections.sort(actualOutputList,
                (o1, o2) -> o1.getIsbn().compareTo(o2.getIsbn()));

        classLog.debug("\nExpected List: (" + expectedOutputList.size() + ") "
                + expectedOutputList);
        classLog.debug("\nActual List: (" + actualOutputList.size() + ") "
                + actualOutputList);

        Books expected;
        Books actual;
        for (int i = 0; i < expectedOutputList.size()
                && i < actualOutputList.size(); i++) {
            expected = expectedOutputList.get(i);
            actual = actualOutputList.get(i);
            classLog.debug("\nExpected: " + expectedOutputList.get(i));
            classLog.debug("\nActual: " + actualOutputList.get(i));
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    private List<Books> createAllEntitiesList() {
        allEntitiesList = new ArrayList<>();
        allEntitiesList.add(createTestBook(9780062498564L,
                "On the Come Up", 8));
        allEntitiesList.add(createTestBook(9780062941008L,
                "Peanut Goes for the Gold", 28));
        allEntitiesList.add(createTestBook(9780062953414L,
                "The Candy Caper", 28));
        allEntitiesList.add(createTestBook(9780062953452L,
                "Busted by Breakfast", 28));
        allEntitiesList.add(createTestBook(9780312625993L,
                "The Forgiveness Garden", 13));
        allEntitiesList.add(createTestBook(9780375832291L,
                "Babymouse #1: Queen of the World!", 42));
        allEntitiesList.add(createTestBook(9780385265201L,
                "The Cartoon History of the Universe: Volumes 1-7: "
                + "From the Big Bang to Alexander the Great",
                47));
        allEntitiesList.add(createTestBook(9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions "
                + "& How They Came to Be", 47));
        allEntitiesList.add(createTestBook(9780399246531L,
                "The Day You Begin", 17));
        allEntitiesList.add(createTestBook(9780439706407L,
                "Bone #1: Out from Boneville", 45));
        allEntitiesList.add(createTestBook(9780439846806L,
                "The Stonekeeper (Amulet #1)", 39));
        allEntitiesList.add(createTestBook(9780448405179L,
                "What\'s Out There?: A Book about Space", 26));
        allEntitiesList.add(createTestBook(9780486468211L,
                "My First Human Body Book", 21));
        allEntitiesList.add(createTestBook(9780545132060L, "Smile",
                39));
        allEntitiesList.add(createTestBook(9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest,"
                + " Toughest, Cleverest, Shyest and Most "
                + "Surprising Animals on Earth", 24));
        allEntitiesList.add(createTestBook(9780606144841L,
                "American Born Chinese", 38));
        allEntitiesList.add(createTestBook(9780606267380L,
                "Drama", 40));
        allEntitiesList.add(createTestBook(9780692848388L,
                "What Should Danny Do?", 40));
        allEntitiesList.add(createTestBook(9780763693558L,
                "Alma and How She Got Her Name", 40));
        allEntitiesList.add(createTestBook(9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)",
                40));
        return allEntitiesList;
    }
}
