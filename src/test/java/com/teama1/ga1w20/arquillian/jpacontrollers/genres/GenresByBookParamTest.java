package com.teama1.ga1w20.arquillian.jpacontrollers.genres;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A test class for GenresJpaController.
 *
 * Genres may be retrieved by: - Book (Books object or isbn)
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class GenresByBookParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GenresByBookParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Jpa Controller
    @Inject
    private GenresJpaController genreController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    //RuleSet containing testCases to test
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<Books, List<Genres>> dynamicParameterHolder;

    //Field representing the actual value(s)
    List<Genres> actualOutputList;

    private final Books BOOK1 = new Books();
    private final Books BOOK2 = new Books();
    private final Books BOOK3 = new Books();
    private final Books BOOK4 = new Books();

    private final List<Genres> RESULTLIST1 = new ArrayList<>();
    private final List<Genres> RESULTLIST2 = new ArrayList<>();
    private final List<Genres> RESULTLIST3 = new ArrayList<>();
    private final List<Genres> RESULTLIST4 = new ArrayList<>();
    private final List<Genres> EMPTY_RESULTLIST = new ArrayList<>();

    /**
     * Constructor
     */
    public GenresByBookParamTest() {
        //Case with one result
        BOOK1.setIsbn(9780385320436L);
        RESULTLIST1.clear();
        RESULTLIST1.add(new Genres(1, "educational"));

        //Case with 2 results
        BOOK2.setIsbn(9780062498564L);
        RESULTLIST2.clear();
        RESULTLIST2.add(new Genres(3, "comic book"));
        RESULTLIST2.add(new Genres(5, "social themes"));

        //Case with 3 results
        BOOK3.setIsbn(9780062941008L);
        RESULTLIST3.clear();
        RESULTLIST3.add(new Genres(1, "educational"));
        RESULTLIST3.add(new Genres(3, "comic book"));
        RESULTLIST3.add(new Genres(5, "social themes"));

        //Case with 0 results
        BOOK4.setIsbn(30L); //Inexistent book
        RESULTLIST4.clear();

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(BOOK1, RESULTLIST1),
                new ParameterHolder<>(BOOK2, RESULTLIST2),
                new ParameterHolder<>(BOOK3, RESULTLIST3),
                new ParameterHolder<>(BOOK4, EMPTY_RESULTLIST));
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Tests that the correct number of genres are returned when retrieving
     * genres by book isbn.
     */
    @Test
    public void getGenresByBookIsbn_ListContent_Test() {
        actualOutputList = genreController.
                getGenresByBook(dynamicParameterHolder.getInput().getIsbn());
        List<Genres> expectedOutputList = dynamicParameterHolder.getListOutput();

        //Test list size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue(actualOutputList.containsAll(expectedOutputList));
        assertTrue(expectedOutputList.containsAll(actualOutputList));
    }

    /**
     * Ensures the correct genres are retrieved for a given book object.
     */
    @Test
    public void getGenresByBookObject_ListContent_Test() {
        actualOutputList = genreController.
                getGenresByBook(dynamicParameterHolder.getInput());
        List<Genres> expectedOutputList = dynamicParameterHolder.getListOutput();

        //Test list size and content
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue(actualOutputList.containsAll(expectedOutputList));
        assertTrue(expectedOutputList.containsAll(actualOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
