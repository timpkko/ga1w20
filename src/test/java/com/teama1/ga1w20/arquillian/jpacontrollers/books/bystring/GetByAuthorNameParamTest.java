package com.teama1.ga1w20.arquillian.jpacontrollers.books.bystring;

import com.teama1.ga1w20.arquillian.jpacontrollers.books.BooksTestUtils;
import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterHolder;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class to test the getBooksByAuthorHint(String hint) method of the
 * BooksJpaController.
 *
 * @book Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class GetByAuthorNameParamTest {

    private final static Logger LOG = LoggerFactory.getLogger(GetByAuthorNameParamTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Jpa Controller
    @Inject
    private BooksJpaController bookJpaController;

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    //Rule which contains the test cases to do parameterized testing on
    @Rule
    public ParameterRule testCases;

    //Dynamic field which is populated according to case, representing the exepected value(s)
    private ParameterHolder<String, List<Books>> dynamicParameterHolder;

    //Field representing the actual value(s)
    private List<Books> actualOutputList;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS SET-UP
    ////////////////////////////////////////////////////////////////////////////
    BooksTestUtils booksTestUtils = new BooksTestUtils();

    String input1, input2, input3, input4, input5, input6;

    private final List<Books> resultList1 = new ArrayList<>();
    private final List<Books> resultList2 = new ArrayList<>();
    private final List<Books> resultList3 = new ArrayList<>();
    private final List<Books> resultList4 = new ArrayList<>();
    private List<Books> resultList5 = new ArrayList<>();

    /**
     * Constructor
     */
    public GetByAuthorNameParamTest() {
        //Case : 1 result
        resultList1.clear();
        input1 = "llin";
        resultList1.add(booksTestUtils.createTestBook(9780448405179L,
                "What\'s Out There?: A Book about Space", 26));

        //Case : 2 results
        input2 = "Thom";
        resultList2.clear();
        resultList2.add(booksTestUtils.createTestBook(9780062498564L,
                "On the Come Up", 8));
        resultList2.add(booksTestUtils.createTestBook(9780312625993L,
                "The Forgiveness Garden",
                13));

        //Case : 4 results
        input3 = "AR";
        resultList3.clear();
        resultList3.add(booksTestUtils.createTestBook(9780385265201L,
                "The Cartoon History of the Universe: Volumes 1-7: "
                + "From the Big Bang to Alexander the Great",
                47));
        resultList3.add(booksTestUtils.createTestBook(9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be",
                47));
        resultList3.add(booksTestUtils.createTestBook(9780763693558L,
                "Alma and How She Got Her Name", 40));
        resultList3.add(booksTestUtils.createTestBook(9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)", 40));

        //Case :  20 results
        input4 = "e";
        resultList4.clear();
        resultList4.add(booksTestUtils.createTestBook(9780062498564L,
                "On the Come Up", 8));
        resultList4.add(booksTestUtils.createTestBook(9780062941008L,
                "Peanut Goes for the Gold", 28));
        resultList4.add(booksTestUtils.createTestBook(9780312625993L,
                "The Forgiveness Garden",
                13));
        resultList4.add(booksTestUtils.createTestBook(9780375832291L,
                "Babymouse #1: Queen of the World!",
                42));
        resultList4.add(booksTestUtils.createTestBook(9780385320436L,
                "Mistakes That Worked: 40 Familiar Inventions & How They Came to Be",
                47));
        resultList4.add(booksTestUtils.createTestBook(9780399246531L,
                "The Day You Begin", 17));
        resultList4.add(booksTestUtils.createTestBook(9780439706407L,
                "Bone #1: Out from Boneville", 45));
        resultList4.add(booksTestUtils.createTestBook(9780448405179L,
                "What\'s Out There?: A Book about Space", 26));
        resultList4.add(booksTestUtils.createTestBook(9780545132060L, "Smile",
                39));
        resultList4.add(booksTestUtils.createTestBook(9780547557991L,
                "The Animal Book: A Collection of the Fastest, Fiercest,"
                + " Toughest, Cleverest, Shyest and Most "
                + "Surprising Animals on Earth", 24));
        resultList4.add(booksTestUtils.createTestBook(9780606144841L,
                "American Born Chinese", 38));
        resultList4.add(booksTestUtils.createTestBook(9780692848388L,
                "What Should Danny Do?", 40));
        resultList4.add(booksTestUtils.createTestBook(9780763693558L,
                "Alma and How She Got Her Name", 40));
        resultList4.add(booksTestUtils.createTestBook(9780810984226L,
                "How Mirka Got Her Sword (Hereville Book 1)", 40));

        //Case :  Empty string, all books results
        input5 = "";
        resultList5 = booksTestUtils.getAllBooksExpected();

        testCases = new ParameterRule("dynamicParameterHolder",
                new ParameterHolder<>(input1, resultList1),
                new ParameterHolder<>(input2, resultList2),
                new ParameterHolder<>(input3, resultList3),
                new ParameterHolder<>(input4, resultList4),
                new ParameterHolder<>(input5, resultList5)
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Test if the actual list of Books found is the same as the expected list
     * of Books found
     */
    @Test
    public void getBooksByAuthorNameHint_test() {
        actualOutputList = bookJpaController.
                getBooksByAuthorHint(dynamicParameterHolder.getInput());
        List<Books> expectedOutputList = dynamicParameterHolder.getListOutput();

        //Test for list content and size
        assertEquals(expectedOutputList.size(), actualOutputList.size());
        assertTrue("The actual list seem to be containing extra expected objects.",
                expectedOutputList.containsAll(actualOutputList));
        assertTrue("The actual list does not contain all of the expected objects.",
                actualOutputList.containsAll(expectedOutputList));
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeTestScript(BooksTestUtils.BOOK_TESTS_SCRIPT, dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive()
                .addPackage(BooksTestUtils.class.getPackage())
                .addPackage(GetByAuthorNameParamTest.class.getPackage());
    }
}
