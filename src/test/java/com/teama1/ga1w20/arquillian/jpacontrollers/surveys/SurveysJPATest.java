package com.teama1.ga1w20.arquillian.jpacontrollers.surveys;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Surveys;
import com.teama1.ga1w20.persistence.jpacontrollers.SurveysJpaController;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tests custom queries for retrieving surveys. Lists of surveys may be
 * retrieved depending on whether the surveys are active or inactive.
 *
 * @author Camillia E., Eira Garrett
 */
@RunWith(Arquillian.class)
public class SurveysJPATest {

    private final static Logger LOG = LoggerFactory.getLogger(SurveysJPATest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils utils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    public SurveysJpaController controller;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void testGetNumActiveSurveys() {
        List<Surveys> activeSurveys = controller.getActiveSurveys();

        assertEquals(3, activeSurveys.size());
    }

    @Test
    public void testGetNumInactiveSurveys() {
        List<Surveys> inactiveSurveys = controller.getInactiveSurveys();

        assertEquals(17, inactiveSurveys.size());
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        utils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
