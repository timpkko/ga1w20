package com.teama1.ga1w20.arquillian.jpacontrollers.orders;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.entities.Users;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;

/**
 * navigationBean methods to test OrdersJpaController
 *
 * @author Camillia E.
 */
public class OrdersTestUtils {

    public final static String ORDER_TESTS_SCRIPT = ArchiveUtils.ORDER_TESTS_SCRIPT;

    //Format to read date strings
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Allows constructing an object entity out of values taken from the INSERT
     * INTO MySQL statements.
     *
     * @param orderId
     * @param userId
     * @param purchaseDateString
     * @return
     * @throws java.text.ParseException If a date string was not parse-able to a
     * date
     */
    public Orders createOrders(int orderId, int userId, String purchaseDateString)
            throws ParseException {
        Date purchaseDate = DATE_FORMAT.parse(purchaseDateString);
        return new Orders(orderId, new Users(userId), purchaseDate);
    }

    /**
     * Method to output the comparison of two lists holding orders. It orders
     * them by isbn and print them.
     *
     * @param expectedOutputList
     * @param actualOutputList
     */
    public void logListsComparison(Logger classLog, List<Orders> expectedOutputList, List<Orders> actualOutputList) {
        Collections.sort(expectedOutputList, (o1, o2) -> o1.getOrderId().compareTo(o2.getOrderId()));
        Collections.sort(actualOutputList, (o1, o2) -> o1.getOrderId().compareTo(o2.getOrderId()));
        classLog.debug("\nExpected List: (" + expectedOutputList.size() + ") " + expectedOutputList);
        classLog.debug("\nActual List: (" + actualOutputList.size() + ") " + actualOutputList);
        Orders expected;
        Orders actual;
        for (int i = 0; i < expectedOutputList.size() && i < actualOutputList.size(); i++) {
            expected = expectedOutputList.get(i);
            actual = actualOutputList.get(i);
            classLog.debug("\nExpected: " + expectedOutputList.get(i));
            classLog.debug("\nActual: " + actualOutputList.get(i));
            if (!Objects.equals(expected.getOrderId(), actual.getOrderId())) {
                classLog.debug("3");
            }
            if (!Objects.equals(expected.getUserId(), actual.getUserId())
                    && (expected.getUserId() == null || actual.getUserId() == null
                    || expected.getUserId().getUserId() == null || actual.getUserId().getUserId() == null
                    || !expected.getUserId().getUserId().equals(actual.getUserId().getUserId()))) {
                classLog.debug("4");
            }
            if (!Objects.equals(expected.getPurchaseDate(), actual.getPurchaseDate())) {
                classLog.debug("5");
            }

            if (!expectedOutputList.contains(actual)) {
                classLog.debug("Expected list does not contain actual object");
            }

            if (!actualOutputList.contains(expected)) {
                classLog.debug("Actual list does not contain expected object");
            }

        }
    }
}
