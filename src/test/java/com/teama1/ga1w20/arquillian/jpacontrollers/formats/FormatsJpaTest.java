package com.teama1.ga1w20.arquillian.jpacontrollers.formats;

import com.teama1.ga1w20.arquillian.utils.ArchiveUtils;
import com.teama1.ga1w20.arquillian.utils.ArquillianUtils;
import com.teama1.ga1w20.arquillian.utils.TestLogger;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test FormatsJpaController Custom queries: - getFormatsByBook(Books
 * book) - getFormatsByBook(Long isbn)
 *
 * @author Camillia E.
 */
@RunWith(Arquillian.class)
public class FormatsJpaTest {

    private final static Logger LOG = LoggerFactory.getLogger(FormatsJpaTest.class);

    @Resource(lookup = "java:app/jdbc/ecubs")
    private DataSource dataSource;

    private final ArquillianUtils testUtils = new ArquillianUtils();

    //Test Logger
    @Rule
    public MethodRule testLogger = new TestLogger(LOG);

    @Inject
    FormatsJpaController formatsJpaController;

    //Total number of entities inserted in the database with the SQL script
    private final static int EXPECTED_FORMATS_COUNT = 30;

    ////////////////////////////////////////////////////////////////////////////
    //TESTS
    ////////////////////////////////////////////////////////////////////////////
    @Test
    public void getFormatsCountTest() {
        int actualCount = formatsJpaController.getFormatsCount();
        int expectedCount = EXPECTED_FORMATS_COUNT;
        Assert.assertEquals(expectedCount, actualCount);
    }

    @Test(expected = NullPointerException.class)
    public void getFormatsByBook_NullTest() {
        formatsJpaController.getFormatsByBook((Books) null);
    }

    @Test(expected = NullPointerException.class)
    public void getFormatsByBookIsbn_NullTest() {
        formatsJpaController.getFormatsByBook((Long) null);
    }

    ////////////////////////////////////////////////////////////////////////////
    //DEPLOYMENT AND SEEDING
    ////////////////////////////////////////////////////////////////////////////
    //Seed database with test script before every test.
    @Before
    public void seedDatabase() {
        testUtils.executeDefaultTestScript(dataSource);
    }

    @Deployment
    public static WebArchive deploy() {
        return ArchiveUtils.getWebArchive();
    }
}
