package com.teama1.ga1w20.arquillian.utils.parameterized;

import java.util.Collection;

/**
 * A generic class for storing parameters and expected outputs for testing
 * purposes.May be instantiated to contain: - A single input object - A List of
 * results - A single result
 *
 *
 * @author Eira Garrett
 * @param <T> Input type
 * @param <V> Output type
 */
public class ParameterHolder<T, V extends Collection> {

    private T input;
    private V listOutput;

    public ParameterHolder() {
        this.input = null;
        this.listOutput = null;
    }

    public ParameterHolder(T input, V listOutput) {
        this.input = input;
        this.listOutput = listOutput;
    }

    public T getInput() {
        return this.input;
    }

    public void setInput(T input) {
        this.input = input;
    }

    public V getListOutput() {
        return this.listOutput;
    }

    public void setListOutput(V listOutput) {
        this.listOutput = listOutput;
    }

    @Override
    public String toString() {
        return "ParameterHolder{" + "input=" + input.toString() + ", listOutput=" + listOutput + '}';
    }
}
