package com.teama1.ga1w20.arquillian.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import javax.sql.DataSource;

/**
 * Utils class for testing the persistence layer.
 *
 * @author Camillia
 */
public class ArquillianUtils {

    private String defaultTestScriptAsString;
    private final String defaultTestScriptName = ArchiveUtils.DEFAULT_TEST_SCRIPT_NAME;
    private String createTablesScriptAsString;
    private final String createTablesScriptName = ArchiveUtils.CREATE_TEST_TABLES_SCRIPT;
    private String fullSeedScriptAsString;
    private final String fullSeedScriptName = ArchiveUtils.FULL_DB_SEED_SCRIPT;

    /**
     * Populate database with default test script.
     *
     * @param dataSource Datasource associated to the test class.
     * @author Camillia E.
     */
    public final void executeDefaultTestScript(DataSource dataSource) {
        Objects.requireNonNull(dataSource, "Data source must be non null");

        if (defaultTestScriptAsString == null) {
            defaultTestScriptAsString = loadAsString(defaultTestScriptName);
        }

        seedDatabase(defaultTestScriptAsString, dataSource);
    }

    public final void executeFullSeedScript(DataSource dataSource) {
        Objects.requireNonNull(dataSource, "Data source must not be null.");

        if (fullSeedScriptAsString == null) {
            fullSeedScriptAsString = loadAsString(fullSeedScriptName);
        }

        seedDatabase(fullSeedScriptAsString, dataSource);
    }

    /**
     * Populate database with only the create table statements.
     *
     * @param dataSource Datasource associated to the test class.
     * @author Camillia E.
     */
    public final void executeCreateTablesTestScript(DataSource dataSource) {
        Objects.requireNonNull(dataSource, "Data source must be non null");

        if (createTablesScriptAsString == null) {
            createTablesScriptAsString = loadAsString(createTablesScriptName);
        }

        seedDatabase(createTablesScriptAsString, dataSource);
    }

    public final void executeTestScript(String testScriptName, DataSource dataSource) {
        Objects.requireNonNull(dataSource, "Data source must be non null");
        Objects.requireNonNull(testScriptName, "testScriptName must be non null");

        String script = loadAsString(testScriptName);

        seedDatabase(script, dataSource);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Helper methods (Courtesy of Bartosz Majsak)
    ////////////////////////////////////////////////////////////////////////////
    private final void seedDatabase(String scriptAsString, DataSource dataSource) {
        try (Connection connection = dataSource.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    scriptAsString), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * @author Bartosz Majsak
     */
    private final String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path)) {
            return new Scanner(inputStream).useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private final List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    /**
     * @author Bartosz Majsak
     */
    private final boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}
