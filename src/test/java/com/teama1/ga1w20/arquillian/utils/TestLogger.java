package com.teama1.ga1w20.arquillian.utils;

import org.junit.rules.TestWatchman;
import org.junit.runners.model.FrameworkMethod;
import org.slf4j.Logger;

/**
 * Class to add as a rule to test classes to log according to test progress.
 *
 * @author Camillia
 */
@SuppressWarnings("deprecation") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
public class TestLogger extends TestWatchman {

    private Logger LOG;

    public TestLogger(Logger logger) {
        this.LOG = logger;
    }

    @Override
    public void starting(FrameworkMethod method) {
        LOG.info("[Method:" + method.getName() + "] - START TESTING");
    }

    @Override
    public void failed(Throwable e, FrameworkMethod method) {
        LOG.info("[Method:" + method.getName() + "] - RESULT: failed! ");
    }

    @Override
    public void succeeded(FrameworkMethod method) {
        LOG.info("[Method:" + method.getName() + "] - RESULT: success!");
    }

}
