package com.teama1.ga1w20.arquillian.utils;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.arquillian.utils.parameterized.ParameterRule;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.io.File;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

/**
 * Class with static method to deploy application for arquillian testing.
 *
 * @author Camillia
 */
public class ArchiveUtils {

    public static final String TEST_WAR_NAME = "test.war";
    public static final EmptyAsset BEANS_XML_PATH = EmptyAsset.INSTANCE;
    public static final String BEANS_XML_NAME = "beans.xml";
    public static final String WEB_XML_PATH = "src/main/webapp/WEB-INF/web.xml";
    public static final String PARAYA_RESOURCES_PATH = "src/main/webapp/WEB-INF/payara-resources.xml";
    public static final String PARAYA_RESOURCES_NAME = "payara-resources.xml";
    public static final String PERSISTENCE_XML_PATH = "src/main/resources/META-INF/persistence.xml";
    public static final String PERSISTENCE_XML_NAME = "META-INF/persistence.xml";
    public static final String LOG4J_XML_PATH = "src/main/resources/log4j2.xml";
    public static final String LOG4J_XML_NAME = "log4j2.xml";
    public static final String DEFAULT_TEST_SCRIPT_NAME = "test-scripts/createAndPopulateTestTables.sql";
    public static final String CREATE_TEST_TABLES_SCRIPT = "test-scripts/createTablesTest.sql";
    public static final String BOOK_TESTS_SCRIPT = "test-scripts/books/createAndPopulateForBookJpaTests.sql";
    public static final String ORDER_TESTS_SCRIPT = "test-scripts/orders/createAndPopulateForOrderJpaTests.sql";
    public static final String FULL_DB_SEED_SCRIPT = "createAndPopulateTables.sql";

    public static WebArchive getWebArchive() {
        File[] dependencies = Maven.resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web")
                .withTransitivity().asFile();

        return ShrinkWrap
                .create(WebArchive.class, TEST_WAR_NAME)
                .setWebXML(new File(WEB_XML_PATH))
                .addAsWebInfResource(BEANS_XML_PATH, BEANS_XML_NAME)
                .addAsWebInfResource(new File(PARAYA_RESOURCES_PATH), PARAYA_RESOURCES_NAME)
                .addAsResource(new File(PERSISTENCE_XML_PATH), PERSISTENCE_XML_NAME)
                .addAsResource(new File(LOG4J_XML_PATH), LOG4J_XML_NAME)
                .addAsResource(DEFAULT_TEST_SCRIPT_NAME)
                .addAsResource(CREATE_TEST_TABLES_SCRIPT)
                .addAsResource(BOOK_TESTS_SCRIPT)
                .addAsResource(ORDER_TESTS_SCRIPT)
                .addAsResource(FULL_DB_SEED_SCRIPT)
                .addAsLibraries(dependencies)
                .addPackage(BigDecimalHandler.class.getPackage())
                .addPackage(ParameterRule.class.getPackage())
                .addPackage(ArquillianUtils.class.getPackage())
                .addPackage(AuthorsJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(Authors.class.getPackage());
    }
}
