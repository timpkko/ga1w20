package com.teama1.ga1w20.selenium.search;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testing to see if a certain input result goes to the book page. It tests on
 * the search bar present on the navbar of the website.
 *
 * @author Timmy
 */
@RunWith(Parameterized.class)
public class SingleResultTitleTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(SingleResultTitleTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    private final String title;

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"On the Come Up"},
            {"Babymouse #1: Queen of the World!"},
            {"Mistakes That Worked: 40 Familiar Inventions & How They Came to Be"}
        });
    }

    public SingleResultTitleTestIT(String title) {
        this.title = title;
    }

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        // Search bar appears on the nav bar
        driver.manage().window().maximize();

        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test to see if searching for a specific title will go to that book page.
     *
     * @throws Exception
     */
    @Test
    public void testForSingleResultInSearchByTitle() throws Exception {
        // Type the title on the search bar of the navbar
        driver.findElement(By.id("searchBar")).sendKeys(title);
        // Press enter to search
        driver.findElement(By.id("searchBar")).sendKeys("\n");
        // Wait until it shows the book page, and look if the page's is the book title
        wait.until(ExpectedConditions.titleContains(title));
    }

}
