package com.teama1.ga1w20.selenium.books;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testing the Based on Previous Visit part of the client's front door.
 *
 * @author Timmy
 */
public class BasedOnPreviousVisitBooksTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(BasedOnPreviousVisitBooksTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test for the visibility of the Based on Previous Visit panel from a fresh
     * browser, with no cookies set. No carousel should not appear for this test
     * to pass.
     *
     *
     * @throws Exception
     */
    @Test
    public void testIsBasedOnPreviousVisitVisibleWithoutCookie() throws Exception {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("basedOnPreviousVisit")));
    }

    /**
     * Test for the visibility of the Based on Previous Visit panel after going
     * to a book page then returning to the index page.
     *
     * @throws Exception
     */
    @Test
    public void testIsBasedOnPreviousVisitVisibleAfterGoingToBookPage() throws Exception {
        // Go to a book page to set up the cookie
        driver.get("http://localhost:8080/ga1w20/book.xhtml?isbn=9781484782002");
        // Return to the index page
        driver.get("http://localhost:8080/ga1w20/");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("basedOnPreviousVisit")));
    }
}
