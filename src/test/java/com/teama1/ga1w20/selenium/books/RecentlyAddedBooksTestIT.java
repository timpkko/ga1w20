package com.teama1.ga1w20.selenium.books;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testing the Recently Added Books part of the client's front door.
 *
 * @author Timmy
 */
public class RecentlyAddedBooksTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(RecentlyAddedBooksTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();

        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test for the number of recently added books. There must be three books in
     * the carousel.
     *
     * @throws Exception
     */
    @Test
    public void testNumberOfRecentlyAddedBooks() throws Exception {
        WebElement recentlyAddedBooks = driver.findElement(By.id("recentlyAddedBooks"));
        WebElement carouselContainer = recentlyAddedBooks.findElement(By.className("ui-carousel-items"));
        List<WebElement> items = carouselContainer.findElements(By.tagName("li"));

        assertEquals(3, items.size());
    }
}
