package com.teama1.ga1w20.selenium.books;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This selenium test will test to make sure that when the user clicks on a
 * book,they will be redirected to the appropriate book page. The books that are
 * currently tested come from the Recently Added Books panel.
 *
 * @author @FishYeho, Timmy
 */
@RunWith(Parameterized.class)
public class BookRedirectTestIT {

    private WebDriver driver;

    private final int index;
    private final String expectedUrl;
    private final String expectedTitle;

    /**
     * Method that creates the parameterized tests
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        // Order goes as such: index, "expectedUrl", "expectedTitle"
        return Arrays.asList(new Object[][]{
            {0, "http://localhost:8080/ga1w20/book.xhtml?isbn=9781633226241",
                "ABC for Me: ABC What Can She Be?: Girls can be anything they want to be, from A to Z - E-Cubs Reading Club"},
            {1, "http://localhost:8080/ga1w20/book.xhtml?isbn=9780692848388",
                "What Should Danny Do? - E-Cubs Reading Club"},
            {2, "http://localhost:8080/ga1w20/book.xhtml?isbn=9781097142804",
                "RIDDLES & BRAIN TEASERS: BEST RIDDLES FOR CHALLENGING SMART KIDS - E-Cubs Reading Club"}});
    }

    public BookRedirectTestIT(int index, String expectedUrl, String expectedTitle) {
        this.index = index;
        this.expectedUrl = expectedUrl;
        this.expectedTitle = expectedTitle;
    }

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test method that will test if the user is redirected to the correct book
     * It will do so by comparing URLs.
     */
    @Test
    public void TestIfUserIsRedirectedToTheRightBookUsingURL() {
        // Given
        // Get the items from the carousel
        WebElement recentlyAddedBooks = driver.findElement(By.id("recentlyAddedBooks"));
        WebElement carouselContainer = recentlyAddedBooks.findElement(By.className("ui-carousel-items"));
        List<WebElement> items = carouselContainer.findElements(By.tagName("li"));

        // When
        // Click on the book
        WebElement bookPanel = items.get(index).findElement(By.tagName("a"));
        bookPanel.click();

        //Then
        assertEquals(expectedUrl, driver.getCurrentUrl());
    }

    /**
     * Test method that will test if the user is redirected to the correct book
     * It will do so by comparing HTML titles.
     */
    @Test
    public void TestIfUserIsRedirectedToTheRightBookUsingHTMLTitle() {
        // Given
        // Get the items from the carousel
        WebElement recentlyAddedBooks = driver.findElement(By.id("recentlyAddedBooks"));
        WebElement carouselContainer = recentlyAddedBooks.findElement(By.className("ui-carousel-items"));
        List<WebElement> items = carouselContainer.findElements(By.tagName("li"));

        // When
        // Click on the book
        WebElement bookPanel = items.get(index).findElement(By.tagName("a"));
        bookPanel.click();

        //Then
        assertEquals(expectedTitle, driver.getTitle());
    }
}
