package com.teama1.ga1w20.selenium.rssfeed;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A Selenium test class to test on the News Highlights on the front door.
 *
 * @author Timmy
 */
public class NewsHighlightTestIT {

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        // News Highlights only appear when the window is bigger
        driver.manage().window().maximize();

        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test to see if clicking on the first link will open a new tab
     *
     * @throws Exception
     */
    @Test
    public void testIfClickOpensNewTab() throws Exception {
        // Using the xpath expression to reach the first link of the news highlight.
        driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[2]/div/div[1]/a")).click();

        // Check if the number tabs increased by 1
        assertEquals(2, driver.getWindowHandles().size());
    }

}
