package com.teama1.ga1w20.selenium.banners;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A Selenium test class to test on the right banner ad present on the client's
 * front page.
 *
 * @author Timmy
 */
public class RightBannerTestIT {

    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test to see if clicking on the right banner will open a new tab.
     *
     * @throws Exception
     */
    @Test
    public void testIfClickOpensNewTab() throws Exception {
        // Click on the right banner ad
        driver.findElement(By.id("rightBanner")).click();
        // Check if the number tabs increased by 1
        assertEquals(2, driver.getWindowHandles().size());
    }

}
