package com.teama1.ga1w20.selenium.browsegenres;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a parameterized test on the Browse Genres links located in the
 * navbar. The test involves clicking on the link and checks if the link goes to
 * the correct genre page.
 *
 * @author Timmy
 */
@RunWith(Parameterized.class)
public class GoToGenrePageTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(GoToGenrePageTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    private final String genre;

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"Comic Book"},
            {"Educational"},
            {"Fantasy"},
            {"Puzzle"},
            {"Social Themes"}
        });
    }

    public GoToGenrePageTestIT(String genre) {
        this.genre = genre;
    }

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        driver.get("http://localhost:8080/ga1w20/");
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test if clicking on the genre link redirects the user to the page with
     * the correct genre as the header of the page.
     *
     * @throws Exception
     */
    @Test
    public void testForHeaderOfGenrePageIfMatchingLink() throws Exception {
        // Open the Browse Genres dropdown menu
        driver.findElement(By.id("navbarForm:browseGenres")).click();
        // Choose and click on the link that goes to the pecified genre page
        WebElement linksToGenre = driver.findElement(By.id("navbarForm:browseGenres")).findElement(By.linkText(genre));
        linksToGenre.click();
        // The h1 tag is used show the genre as the header of the page
        wait.until(ExpectedConditions.textToBe(By.tagName("h1"), "Genre : " + genre));
    }

}
