package com.teama1.ga1w20.selenium.browsegenres;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.List;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Selenium test class revolves on the Browse Genres portion of the navbar.
 *
 * @author Timmy
 */
public class BrowseGenresTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(BrowseGenresTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        driver.get("http://localhost:8080/ga1w20/");
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test for the number of links under the dropdown menu. The number of
     * genres must match with the number of genres in the database. In this case
     * there are five genres.
     *
     * @throws Exception
     */
    @Test
    public void testForNumberOfLinks() throws Exception {
        // Look for number of links in the browse genres dropdown menu
        List<WebElement> linksToGenre = driver.findElement(By.id("navbarForm:browseGenres")).findElements(By.tagName("li"));
        int numberOfLinksUnderDropdown = linksToGenre.size();

        assertEquals(5, numberOfLinksUnderDropdown);
    }

}
