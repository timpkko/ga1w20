package com.teama1.ga1w20.selenium.managerlinks;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This test class contains two test methods. The first will test to see if a
 * logged in manager can view the link to the manager front door. The second
 * will test to make sure that a regular client cannot see the link to the
 * manager front door
 *
 * @author @FishYeho
 */
public class ManagerLinksTestIT {

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        // Ads only appear when the window is bigger
        driver.manage().window().maximize();

        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test method that will check if the link to the manager page is enabled
     * for logged in managers
     */
    @Test
    public void testIfManagerLinkAppearsForManagers() throws Exception {
        //Given
        driver.findElement(By.id("navbarForm:loginBtn")).click();
        driver.findElement(By.id("username")).sendKeys("cst.receive@gmail.com");
        driver.findElement(By.id("password")).sendKeys("collegedawson");
        driver.findElement(By.id("loginForm:login")).click();

        //When
        // Click on the Manage dropdown
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("navbarForm:accountDropdown")));
        driver.findElement(By.id("navbarForm:accountDropdown")).click();

        // Then
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("navbarForm:management"))));
    }

    /**
     * This test will make sure that the a regular client cannot see the link to
     * the manager page We except it to throw the NoSuchElementException as the
     * link to the manager page should not be there
     */
    @Test
    public void testIfManagerLinkDoesNotAppearForClients() throws Exception {
        //Given
        driver.findElement(By.id("navbarForm:loginBtn")).click();
        driver.findElement(By.id("username")).sendKeys("cst.send@gmail.com");
        driver.findElement(By.id("password")).sendKeys("dawsoncollege");
        driver.findElement(By.id("loginForm:login")).click();

        // Click on the Manage dropdown
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("navbarForm:accountDropdown")));
        driver.findElement(By.id("navbarForm:accountDropdown")).click();

        //When
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("navbarForm:management")));
    }
}
