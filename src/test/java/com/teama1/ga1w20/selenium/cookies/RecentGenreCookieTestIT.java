package com.teama1.ga1w20.selenium.cookies;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a Selenium parameterized test that redirects the browser a book page
 * and read the value of the cookie. There are currently five cases,
 * representing the five genres.
 *
 * @author Timmy
 */
@RunWith(Parameterized.class)
public class RecentGenreCookieTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(RecentGenreCookieTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    private final String bookUrl;

    private final String genre;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"http://localhost:8080/ga1w20/book.xhtml?isbn=9781484782002", "Fantasy"},
            {"http://localhost:8080/ga1w20/book.xhtml?isbn=9780439846806", "\"Comic Book\""},
            {"http://localhost:8080/ga1w20/book.xhtml?isbn=9780385320436", "Educational"},
            {"http://localhost:8080/ga1w20/book.xhtml?isbn=9781629794235", "Puzzle"},
            {"http://localhost:8080/ga1w20/book.xhtml?isbn=9780399246531", "\"Social Themes\""}
        });
    }

    public RecentGenreCookieTestIT(String bookUrl, String genre) {
        this.bookUrl = bookUrl;
        this.genre = genre;
    }

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();

        driver.get("http://localhost:8080/ga1w20");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test if the correct genre is inserted in the cookie name recentGenre.
     *
     * @throws Exception
     */
    @Test
    public void testForGenreInCookie() throws Exception {
        // Go to a Fantasy book page
        driver.get(bookUrl);
        // Read the value of the cookie named "recentGenre"
        String genreInCookie = driver.manage().getCookieNamed("recentGenre").getValue();

        assertEquals(genre, genreInCookie);

    }
}
