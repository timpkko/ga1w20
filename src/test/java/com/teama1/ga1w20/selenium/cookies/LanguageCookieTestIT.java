package com.teama1.ga1w20.selenium.cookies;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Selenium test class that tests on the cookie that keeps state of the
 * language.
 *
 * @author Timmy
 */
public class LanguageCookieTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(LanguageCookieTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        driver.get("http://localhost:8080/ga1w20/");
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test the languageCookie after clicking on the button that changes the
     * language.
     *
     * @throws Exception
     */
    @Test
    public void testForLanguageInCookie() throws Exception {
        // Click on the FR button in the navbar
        driver.findElement(By.id("navbarForm:frButton")).click();
        // Read cookie
        String valueOfLanguageCookie = driver.manage().getCookieNamed("languageCookie").getValue();
        assertEquals("fr", valueOfLanguageCookie);
    }
}
