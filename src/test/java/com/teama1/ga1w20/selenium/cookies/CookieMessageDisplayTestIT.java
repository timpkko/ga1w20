package com.teama1.ga1w20.selenium.cookies;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Selenium test class to test the display of the cookie message depending on
 * if the cookies are allowed or blocked.
 *
 * @author Timmy
 */
public class CookieMessageDisplayTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(CookieMessageDisplayTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test if the cookie message displays with cookies blocked.
     *
     * @throws Exception
     */
    @Test
    public void testDisplayMessageIfCookiesAreBlocked() throws Exception {
        // Have the browser block cookies
        disableCookies();
        goToWebsite();

        // Check if the cookie message is displayed
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("cookieDisplay")));
    }

    /**
     * Test if the cookie message does not display with cookies allowed.
     *
     * @throws Exception
     */
    @Test
    public void testNotDisplayMessageIfCookiesAreAllowed() throws Exception {
        // Have the browser block cookies
        goToWebsite();

        // Check if the cookie message is displayed
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("cookieDisplay")));
    }

    /**
     * Helper method to block cookies before doing the test.
     */
    private void disableCookies() {
        ChromeOptions chromeOptions = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.cookies", 2);
        prefs.put("profile.block_third_party_cookies", true);
        prefs.put("cookies", true);
        chromeOptions.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver(chromeOptions);
    }

    /**
     * Helper method to redirect the browser to the website.
     */
    private void goToWebsite() {
        driver.get("http://localhost:8080/ga1w20");

        wait = new WebDriverWait(driver, 10);

        // Wait for the page to load, timeout after 10 seconds
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }
}
