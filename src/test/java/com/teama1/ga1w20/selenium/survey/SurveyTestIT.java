package com.teama1.ga1w20.selenium.survey;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testing the survey that is on the client's front door.
 *
 * @author Timmy
 */
public class SurveyTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(SurveyTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test the submit button without clicking on an option. A required message
     * should appear.
     *
     * @throws Exception
     */
    @Test
    public void testSubmitWithoutAnswer() throws Exception {
        // Type the title on the search bar of the navbar
        driver.findElement(By.id("surveyForm:submitSurvey")).click();
        // Wait until the required message appears
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("surveyForm:surveyMessage")));
    }

    /**
     * Test if the results appears after selecting the first option and submit
     * the form.
     *
     * @throws Exception
     */
    @Test
    public void testSubmitAnswer1() throws Exception {
        // Click on the first option
        driver.findElement(By.id("surveyForm:answersRadio0")).findElement(By.tagName("input")).click();
        // Submit
        driver.findElement(By.id("surveyForm:submitSurvey")).click();
        // Wait until the results appear
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("surveyForm:surveyResults")));
    }

    /**
     * Test if the results appears after selecting the second option and submit
     * the form.
     *
     * @throws Exception
     */
    @Test
    public void testSubmitAnswer2() throws Exception {
        // Click on the first option
        driver.findElement(By.id("surveyForm:answersRadio1")).findElement(By.tagName("input")).click();
        // Submit
        driver.findElement(By.id("surveyForm:submitSurvey")).click();
        // Wait until the results appear
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("surveyForm:surveyResults")));
    }

}
