package com.teama1.ga1w20.selenium.language;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Test if the title of the website changes when the language is switched.
 *
 * @author Timmy
 */
public class ChangeLanguageTestIT {

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        // Ads only appear when the window is bigger
        driver.manage().window().maximize();

        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test if the title of the website is the french variant for the bookstore
     * name, "Club de lecture E-Cubs" after clicking on the FR button.
     */
    @Test
    public void testIfTitleIsFrenchAfterSwitchingLanguage() {
        // Click on the change language
        driver.findElement(By.id("navbarForm:frButton")).click();
        wait.until(ExpectedConditions.titleIs("Club de lecture E-Cubs"));
    }

    /**
     * Test if the title of the website reverts back to english after clicking
     * the switch language twice.
     */
    @Test
    public void testIfTitleIsEnglishAfterSwitchingLanguageTwice() {
        // Click on the FR button to change website to French
        driver.findElement(By.id("navbarForm:frButton")).click();
        // Wait until the ANG button appears to be clicked later
        wait.until(ExpectedConditions.elementToBeClickable(By.id("navbarForm:enButton")));
        // Click on the ANG button to change website to English
        driver.findElement(By.id("navbarForm:enButton")).click();
        // Test to see if the title is in English.
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

}
