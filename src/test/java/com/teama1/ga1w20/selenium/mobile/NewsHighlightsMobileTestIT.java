package com.teama1.ga1w20.selenium.mobile;

import com.teama1.ga1w20.selenium.utils.MobileSetupUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This test class is to test on the visibility of the ads when the website is
 * on a mobile browser.
 *
 * @author Timmy
 */
public class NewsHighlightsMobileTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(NewsHighlightsMobileTestIT.class);

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = MobileSetupUtil.mobileWebDriver();

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        driver.get("http://localhost:8080/ga1w20/");
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test if clicking on the news link will open a new tab.
     *
     * @throws Exception
     */
    @Test
    public void testIfClickOpensNewTab() throws Exception {
        // Using the xpath expression to reach the first link of the news highlight.
        driver.findElement(By.id("newsHighlightsLast")).findElement(By.tagName("a")).click();

        // Check if the number tabs increased by 1
        assertEquals(2, driver.getWindowHandles().size());
    }

}
