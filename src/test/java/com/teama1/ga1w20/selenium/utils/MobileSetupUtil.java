package com.teama1.ga1w20.selenium.utils;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * This class is a utility class to setup a mobile environment for the Selenium
 * testing.
 *
 * @author Timmy
 */
public class MobileSetupUtil {

    /**
     * Setup the WebDriver to emulate a mobile device. Currently the browser
     * will act like a mobile browser inside a Nexus 5 device.
     *
     * Source: https://chromedriver.chromium.org/mobile-emulation
     *
     * @return WebDriver
     */
    public static WebDriver mobileWebDriver() {
        Map<String, String> mobileEmulation = new HashMap<>();

        mobileEmulation.put("deviceName", "Pixel 2");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

        return new ChromeDriver(chromeOptions);
    }

}
