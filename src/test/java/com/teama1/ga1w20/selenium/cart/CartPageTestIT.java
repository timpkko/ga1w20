package com.teama1.ga1w20.selenium.cart;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Testing on the Help Page button on the navbar.
 *
 * @author Timmy
 */
public class CartPageTestIT {

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    /**
     * Set up a new browser instance before every test.
     */
    @Before
    public void setupTest() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://localhost:8080/ga1w20/");

        // Wait for the page to load, timeout after 10 seconds
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleIs("E-Cubs Reading Club"));
    }

    /**
     * Close the browser after the test is done.
     */
    @After
    public void closeBrowser() {
        driver.quit();
    }

    /**
     * Test if the question mark on the navbar redirects the user to the Help
     * page.
     *
     * @throws Exception
     */
    @Test
    public void testIfRedirectIsCorrectAfterClickingOnCartIcon() throws Exception {
        // Click on the question mark icon
        driver.findElement(By.id("navbarForm:cartPageButton")).click();

        wait.until(ExpectedConditions.titleIs("Checkout - E-Cubs Reading Club"));
    }

}
