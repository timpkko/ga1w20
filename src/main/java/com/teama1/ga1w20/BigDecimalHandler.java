package com.teama1.ga1w20;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Class to compare and print BigDecimals
 *
 * @author Camillia
 */
public class BigDecimalHandler {

    private final int scale;

    public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

    /**
     *
     * @param scale If zero or positive, the scale is the number of digits to
     * the right of the decimal point. If negative, the unscaled value of the
     * number is multiplied by ten to the power of the negation of the scale.
     */
    public BigDecimalHandler(int scale) {
        this.scale = scale;
    }

    /**
     * Test if those two numbers are equal considering the scale of this
     * BigDecimalHandler and this ROUNDING_MODE of this class. Null-safe.
     *
     * @param b1
     * @param b2
     * @return true if they are equal considering the scale, false otherwise.
     */
    public boolean areEqual(BigDecimal b1, BigDecimal b2) {
        if (b1 == null && b2 == null) {
            return true;
        }
        if (b1 == null || b2 == null) {
            return false;
        }
        return (b1.setScale(this.scale, ROUNDING_MODE)
                .compareTo(b2.setScale(this.scale, ROUNDING_MODE)) == 0);
    }

    /**
     * Compare those two numbers considering the scale of this BigDecimalHandler
     * and the RoundindMode. Null safe. Null-safe
     *
     * @param b1
     * @param b2
     * @return true if they are equal considering the scale, false otherwise.
     */
    public int compare(BigDecimal b1, BigDecimal b2) {
        if (b1 == null && b2 == null) {
            return 0;
        }
        return b1.setScale(this.scale, ROUNDING_MODE)
                .compareTo(b2.setScale(this.scale, ROUNDING_MODE));
    }

    /**
     * Get a string representation of this number, considering the scale of this
     * BigDecimalHandler. Null-safe.
     *
     * @param b
     * @return String representation of this number, considering the scale of
     * this BigDecimalHandler
     */
    public String getScaledString(BigDecimal b) {
        if (b == null) {
            return "";
        }
        return b.setScale(this.scale, ROUNDING_MODE).toString();
    }
}
