package com.teama1.ga1w20.business.search;

import com.teama1.ga1w20.business.beans.SearchResultsBean;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.PublishersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * SearchUtil has methods to retrieve books in specific fields based on search
 * terms. The returned lists from the methods are lists of
 * {@link SearchResultsBean}. This utility class is used in
 * {@link SearchBackingBean}.
 *
 * @author Eira
 */
@Named
@RequestScoped
public class SearchUtil implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SearchUtil.class);

    @Inject
    private BooksJpaController booksJpaController;

    @Inject
    private AuthorsJpaController authorsJpaController;

    @Inject
    private GenresJpaController genresJpaController;

    @Inject
    private FormatsJpaController formatsJpaController;

    @Inject
    private PublishersJpaController publishersJpaController;

    /**
     * Returns a list of search results wherein the book title field contains
     * the search terms.
     *
     * @param terms The search terms
     * @return A List of SearchResultsBean objects
     */
    public List<SearchResultsBean> fetchBooksByTitle(String terms) {

        List<Books> bookList = booksJpaController.getBooksByTitleHint(terms);

        List<SearchResultsBean> resultsList = new ArrayList<>();

        bookList.forEach(book -> {
            resultsList.add(populateSearchResultsByBook(book));
        });

        return resultsList;
    }

    /**
     * Returns a list of search results wherein the authors field contains the
     * search terms.
     *
     * @param terms The search terms
     * @return A List of SearchResultsBean objects
     */
    public List<SearchResultsBean> fetchBooksByAuthor(String terms) {

        List<Books> bookList = booksJpaController.getBooksByAuthorHint(terms);

        List<SearchResultsBean> searchResults = new ArrayList<>();

        bookList.forEach(book -> {
            searchResults.add(populateSearchResultsByBook(book));
        });

        return searchResults;
    }

    /**
     * Returns a list of search results wherein the publisher field contains the
     * search terms.
     *
     * @param terms The search terms
     * @return A List of SearchResultsBean objects
     */
    public List<SearchResultsBean> fetchBooksByPublisher(String terms) {

        List<Books> bookList = booksJpaController.getBooksByPublisherHint(terms);

        List<SearchResultsBean> searchResults = new ArrayList<>();

        bookList.forEach(book -> {
            searchResults.add(populateSearchResultsByBook(book));
        });

        return searchResults;
    }

    /**
     * Returns a list of search results wherein the book ISBN field contains the
     * search terms.
     *
     * @param isbn The ISBN-13
     * @return A List of SearchResultsBean objects
     */
    public List<SearchResultsBean> fetchBooksByIsbn(long isbn) {

        List<SearchResultsBean> searchResults = new ArrayList<>();

        Books book = booksJpaController.getBookByIsbn(isbn);

        searchResults.add(populateSearchResultsByBook(book));

        return searchResults;

    }

    /**
     * Returns a list of search results wherein the genre field contains the
     * search terms.
     *
     * @param terms The search terms
     * @return A List of SearchResultsBean objects
     */
    public List<SearchResultsBean> fetchBooksByGenre(String terms) {

        List<Books> bookList = booksJpaController.getBooksByGenreHint(terms);

        List<SearchResultsBean> searchResults = new ArrayList<>();

        bookList.forEach(book -> {
            searchResults.add(populateSearchResultsByBook(book));
        });

        return searchResults;
    }

    /**
     * A helper method to take a Books object and use it to populate a
     * SearchResultsBean.
     *
     * @param book
     * @return
     */
    private SearchResultsBean populateSearchResultsByBook(Books book) {
        //title, description, isbn, price, genres, formats, authors, publisher
        SearchResultsBean resultsBean = new SearchResultsBean();

        resultsBean.setIsbn(book.getIsbn());

        resultsBean.setTitle(book.getTitle());

        resultsBean.setDescription(book.getDescription());

        resultsBean.setListPrice(book.getListPrice());

        resultsBean.setSalePrice(book.getSalePrice());

        List<Genres> genres = genresJpaController.getGenresByBook(book);

        List<String> genreNames = new ArrayList<>();

        genres.forEach(genre -> {
            genreNames.add(genre.getName());
        });

        resultsBean.setGenres(genreNames);

        List<Formats> formats = formatsJpaController.getFormatsByBook(book);

        List<String> formatNames = new ArrayList<>();

        formats.forEach(format -> {
            formatNames.add(String.format("%s (%s)", format.getName(), format.getExtension()));
        });

        resultsBean.setFormats(formatNames);

        List<Authors> authors = authorsJpaController.getAuthorsByBook(book);

        List<String> authorNames = new ArrayList<>();

        authors.forEach(author -> {
            StringBuilder authorName = new StringBuilder();
            authorName.append(author.getFirstname());
            if (author.getLastname() != null) {
                authorName.append(" ");
                authorName.append(author.getLastname());
            }
            authorNames.add(authorName.toString());
        });

        resultsBean.setAuthors(authorNames);

        try {
            resultsBean.setPublisher(publishersJpaController.getPublisherByBook(book).getName());
        } catch (NonexistentEntityException e) {
            LOG.error("No publisher found: ", e);
        }

        return resultsBean;
    }

}
