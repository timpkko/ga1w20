package com.teama1.ga1w20.business.search;

public enum SortBySearch {
    TITLE, AUTHOR, PUBLISHER, GENRE, ISBN;
}
