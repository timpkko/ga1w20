package com.teama1.ga1w20.business.filters;

import com.teama1.ga1w20.business.beans.SessionUserBean;
import java.io.IOException;
import javax.inject.Inject;
import javax.persistence.Transient;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter which makes sure any manager pages redirects to login if the user is
 * not logged in or if the logged in user is not a manager.
 *
 * @author Camillia
 */
@WebFilter(filterName = "ManagerFilter", urlPatterns = {"/manager", "/manager/*"})
public class ManagerFilter implements Filter {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(ManagerFilter.class);

    @Inject
    private SessionUserBean sessionBean; // As an instance variable

    /**
     * Checks if user is logged in and if it is a manager. If not it redirects
     * to the login.xhtml page.
     *
     * @param request
     * @param response
     * @param chain
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        LOG.info("ManagerFilter: doFilter");

        if (sessionBean == null || !sessionBean.isUserLoggedIn() || !sessionBean.isUserAManager()) {
            if (!sessionBean.isUserAManager()) {
                LOG.info("User is not a manager.");
            } else {
                LOG.info("User not logged in.");
            }

            //Redirect to login page
            String contextPath = ((HttpServletRequest) request)
                    .getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath
                    + "/login.xhtml");

        } else {
            LOG.info("Manager logged in: " + sessionBean.getUser().getFirstname());
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // Nothing to do here!
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to do here but must be overloaded
    }
}
