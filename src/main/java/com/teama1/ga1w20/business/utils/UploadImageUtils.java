package com.teama1.ga1w20.business.utils;

import com.teama1.ga1w20.Constants;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.persistence.Transient;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UploadImageUtils is an utility class with capabilities to save an image on
 * the web server, using the PrimeFaces' UploadedFile class.
 *
 * @author Timmy
 */
public class UploadImageUtils implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(UploadImageUtils.class);

    private UploadedFile uploadedImage;

    public UploadImageUtils(UploadedFile uploadedImage) {
        this.uploadedImage = uploadedImage;
    }

    public UploadedFile getUploadedImage() {
        return uploadedImage;
    }

    public void setUploadedImage(UploadedFile uploadedImage) {
        this.uploadedImage = uploadedImage;
    }

    /**
     * Save the image to the specified folder with a specified filename without
     * the extension. All images will be stored with the extension ".jpg".
     *
     * @param path Path to the folder where to save the image
     * @param filename The new filename of the image without extension
     * @throws IOException
     */
    public void saveToDisk(String path, String filename) throws IOException {
        Path completePath = Paths.get(
                FacesContext.getCurrentInstance().getExternalContext().getRealPath(path) + "/" + filename + Constants.IMAGES_EXTENSION);

        Files.deleteIfExists(completePath);

        Path imageFile = Files.createFile(completePath);

        try (InputStream inputStream = uploadedImage.getInputStream()) {
            Files.copy(inputStream, imageFile, StandardCopyOption.REPLACE_EXISTING);
        }

        LOG.info("New image saved to " + imageFile.toString());
    }

    /**
     * Save the image to the specified folder with a specified filename without
     * the extension.All images will be stored with the extension ".jpg".
     *
     * @param image BufferedImage
     * @param path Path to the folder where to save the image
     * @param filename The new filename of the image without extension
     * @throws IOException
     */
    public static void saveToDisk(BufferedImage image, String path, String filename) throws IOException {
        Path completePath = Paths.get(
                FacesContext.getCurrentInstance().getExternalContext().getRealPath(path)
                + "/" + filename + Constants.IMAGES_EXTENSION);

        Files.deleteIfExists(completePath);

        Path imageFile = Files.createFile(completePath);
        ImageIO.write(image, Constants.IMAGES_FORMAT, imageFile.toFile());

        LOG.info("New image saved to " + imageFile.toString());
    }

    /**
     * Resizes the image by a specified height while keeping the aspect ratio.
     *
     * @param height
     * @return The resized image
     * @throws IOException
     */
    public BufferedImage resizeImage(int height) throws IOException {
        try (InputStream imageStream = uploadedImage.getInputStream()) {
            BufferedImage bufferedUploadedImage = ImageIO.read(imageStream);

            // Get the height and width of the uploaded image
            double uploadedImageHeight = bufferedUploadedImage.getHeight();
            double uploadedImageWidth = bufferedUploadedImage.getWidth();

            // Calculate the width based on the new height and image
            double ratio = height / uploadedImageHeight;
            double newImageWidth = uploadedImageWidth * ratio;

            BufferedImage outputImage = new BufferedImage((int) newImageWidth,
                    height, bufferedUploadedImage.getType());

            Graphics graphics = outputImage.createGraphics();
            graphics.drawImage(bufferedUploadedImage, 0, 0, (int) newImageWidth, height, null);
            graphics.dispose();

            return outputImage;
        }
    }

}
