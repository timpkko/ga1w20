package com.teama1.ga1w20.business.utils;

import com.teama1.ga1w20.Constants;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains useful methods involving with using the ResourceBundles.
 *
 * @author Timmy, Camillia E.
 */
public class Messages {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(Messages.class);

    /**
     * Retrieves the string from the resource bundle. Returns an empty string if
     * the key does not exist. https://stackoverflow.com/a/13656194 (BalusC)
     *
     * @param key
     * @return
     */
    public static String getString(String key) {
        return getBundleString(FacesContext.getCurrentInstance(), key);
    }

    /**
     * Add message with the specified severity and the message associated to the
     * messageBundleKey to the component with the specified clientId. If
     * clientId is null, it will be a global message.
     *
     * @param severity
     * @param messageBundleKey
     * @param clientId Allowed to be null
     * @author Camillia E.
     */
    public static void addMessageToContext(Severity severity,
            String messageBundleKey, String clientId) {
        if (severity == null || messageBundleKey == null || messageBundleKey.isEmpty()) {
            LOG.error("addMessageToContext: Invalid parameters. "
                    + "Unable to add message.");
            return;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        String message = getBundleString(context, messageBundleKey);
        if (message.isEmpty()) {
            LOG.error("Unable to add message to context as the key could not be found");
            return;
        }
        FacesMessage facesMessage = new FacesMessage(severity,
                message, message);
        context.addMessage(clientId, facesMessage);
    }

    ///////////////////////////////////////////////////////////////////////////
    /**
     * Get internationalized messaged with associated bundle key. Returns an
     * empty string if the key does not exist.
     */
    private static String getBundleString(FacesContext context, String key) {
        ResourceBundle text = context.getApplication()
                .evaluateExpressionGet(context,
                        "#{" + Constants.LANGUAGE_BUNDLE_IDENTIFIER + "}",
                        ResourceBundle.class);

        // Returns an empty string to prevent NullPointer Exception
        if (!text.containsKey(key)) {
            LOG.error("Unable to find message with key : " + key);
            return "";
        }
        return text.getString(key);
    }

}
