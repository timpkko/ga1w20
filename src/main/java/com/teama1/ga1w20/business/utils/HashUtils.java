package com.teama1.ga1w20.business.utils;

import static com.teama1.ga1w20.Constants.HASH_ALGORITHM;
import static com.teama1.ga1w20.Constants.HASH_ITERATION_COUNT;
import static com.teama1.ga1w20.Constants.HASH_KEY_LENGTH;
import static com.teama1.ga1w20.Constants.SALT_MAX_NUM_BITS;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilities class for storing hashes associated to a new user, or to
 * authenticate at login.
 *
 * @author Camillia
 */
public class HashUtils {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(HashUtils.class);

    public byte[] hashPassword(String password, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory skf = SecretKeyFactory.getInstance(HASH_ALGORITHM);
        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(),
                HASH_ITERATION_COUNT, HASH_KEY_LENGTH);
        SecretKey key = skf.generateSecret(spec);
        byte[] hash = key.getEncoded();
        return hash;
    }

    public boolean compareHashes(byte[] bytes1, byte[] bytes2) {
        if (bytes1.length != bytes2.length) {
            return false;
        }
        for (int i = 0; i < bytes1.length; i++) {
            if (bytes1[i] != bytes2[i]) {
                return false;
            }
        }
        return true;
    }

    //Creates a Salt which will be stored as a CHAR(28)
    public String createSalt() {
        return new BigInteger(SALT_MAX_NUM_BITS, new SecureRandom()).toString(32);
    }
}
