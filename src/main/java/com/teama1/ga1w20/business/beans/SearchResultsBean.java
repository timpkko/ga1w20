package com.teama1.ga1w20.business.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A custom bean bridging books, genres, formats, and authors for the purposes
 * of displaying search results.
 *
 * @author Eira Garrett
 */
@Named("searchResultsBean")
@RequestScoped
public class SearchResultsBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SearchResultsBean.class);

    //Book-specific fields
    private String title;
    private String description;
    private Long isbn;
    private BigDecimal salePrice;
    private BigDecimal listPrice;

    //Genre-specific fields
    private List<String> genres;

    //Format-specific fields
    private List<String> formats;

    //Author-specific fields
    List<String> authors;

    //Publisher-specific fields
    String publisher;

    public SearchResultsBean() {
        this.title = "";
        this.description = "";
        this.isbn = null;
        this.salePrice = null;
        this.listPrice = null;

        this.genres = null;

        this.formats = null;

        this.authors = null;

        this.publisher = "";
    }

    public SearchResultsBean(String title, String description, Long isbn, BigDecimal salePrice, BigDecimal listPrice, List<String> genres, List<String> formats, List<String> authors, String publisher) {
        this.title = title;
        this.description = description;
        this.isbn = isbn;
        this.salePrice = salePrice;
        this.listPrice = listPrice;

        this.genres = genres;

        this.formats = formats;

        this.authors = authors;

        this.publisher = publisher;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getIsbn() {
        return this.isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public List<String> getGenres() {
        return this.genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public List<String> getFormats() {
        return this.formats;
    }

    public void setFormats(List<String> formats) {
        this.formats = formats;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public List<String> getAuthors() {
        return this.authors;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this == o) {
            return true;
        }

        if ((o instanceof SearchResultsBean) && (Objects.equals(((SearchResultsBean) o).isbn, this.isbn))) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.isbn);
        return hash;
    }
}
