package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.business.utils.Messages;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cross-field validation to make sure the user has entered a valid password and
 * that they entered the same password in both "Password" and "Confirm Password"
 * fields. A password is valid if it is between 8 and 20 characters, and has at
 * least: one lowercase character, one uppercase character, one digit and one
 * special character ( @ # $ % ! .).
 *
 * @author Camillia E.
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "passwordValidator")
public class PasswordValidator implements Validator {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(PasswordValidator.class);

    private final static String PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,20})";

    @Override
    public void validate(FacesContext facesContext, UIComponent uic, Object t) throws ValidatorException {
        UIInput passwordInputText = (UIInput) uic.getAttributes().get("passwordInput");
        UIInput passwordConfirmInputText = (UIInput) uic.getAttributes().get("passwordConfirmInput");

        //If both fields were inputed and not empty
        if (passwordInputText.getSubmittedValue() != null
                && passwordConfirmInputText.getSubmittedValue() != null
                && !((String) passwordInputText.getSubmittedValue()).isEmpty()
                && !((String) passwordConfirmInputText.getSubmittedValue()).isEmpty()) {
            String password = (String) passwordInputText.getSubmittedValue();
            String passwordConfirm = (String) passwordConfirmInputText.getSubmittedValue();

            //Validate the regex of at least one of them, might throw a validator exception
            validateRegex(password);

            //If passwords dont match, throw a Validation error
            if (!password.equals(passwordConfirm)) {
                FacesMessage facesMessage = new FacesMessage(Messages.getString("PasswordConfirmError"));
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(facesMessage);
            }
        } //If only the first password field is inputed and not empty
        else if (passwordInputText.getSubmittedValue() != null
                && !((String) passwordInputText.getSubmittedValue()).isEmpty()) {
            String password = (String) passwordInputText.getSubmittedValue();
            //Validate the regex, might throw a validator exception
            validateRegex(password);
        } //If only the confirm-password field is inputed and not empty
        else if (passwordConfirmInputText.getSubmittedValue() != null
                && !((String) passwordConfirmInputText.getSubmittedValue()).isEmpty()) {
            String passwordConfirm = (String) passwordConfirmInputText.getSubmittedValue();
            //Validate the regex, might throw a validator exception
            validateRegex(passwordConfirm);
        }
    }

    private void validateRegex(String input) {
        if (!input.matches(PATTERN)) {
            FacesMessage facesMessage = new FacesMessage(Messages.getString("PasswordRegexError"));
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(facesMessage);
        }
    }
}
