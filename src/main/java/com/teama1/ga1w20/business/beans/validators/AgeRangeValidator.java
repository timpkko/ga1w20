package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.business.utils.Messages;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validates the minAge and maxAge fields submitted to ensure that they create a
 * valid age range.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "ageRangeValidator")
public class AgeRangeValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        UIInput input1 = (UIInput) uic.getAttributes().get("minAge");
        UIInput input2 = (UIInput) uic.getAttributes().get("maxAge");

        if (input1.getSubmittedValue() != null && input2.getSubmittedValue() != null) {
            //The following ternary operators check if the submitted field is blank or not.
            //If it is, assign it to null to simplify the validation in the if statement following.
            //If it's not, parse it properly.
            Integer minAge = ((String) input1.getSubmittedValue()).isBlank()
                    ? null : Integer.parseInt((String) input1.getSubmittedValue());
            Integer maxAge = ((String) input2.getSubmittedValue()).isBlank()
                    ? null : Integer.parseInt((String) input2.getSubmittedValue());

            if (minAge != null && maxAge != null && minAge >= maxAge) {
                FacesMessage msg = new FacesMessage(Messages.getString("CreateBooksValidate_AgeRange"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }
}
