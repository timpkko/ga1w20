package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validator for inputed emails. Note: Declared as a CDI bean so
 * UsersJpaController can be injected.
 *
 * @author Camillia E.
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@Named("emailValidator")
@RequestScoped
public class EmailValidator implements Validator {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(EmailValidator.class);

    private final static String PATTERN = "^[A-Za-z0-9]+[A-Za-z0-9_.]*@[A-Za-z0-9_.]+\\.[A-Za-z]+$";

    @Inject
    private UsersJpaController userJpaController;

    /**
     * Validates it is not already linked to an existent user, and if it is
     * valid. An email is valid if is composed of letters, numbers, underscore
     * and dots. It must have at least one character, then one '@' character,
     * then at least one character, then a dot, then at least one character.
     * Valid: x@x.x , x.x@x.x Not valid: .x@x.x, .@x.x, x.x@x, x.x@x.
     *
     * @param context
     * @param component
     * @param value
     * @throws ValidatorException
     */
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value != null && !((String) value).isBlank()) {
            // Retrieve the value passed to this method
            String inputedEmail = ((String) value).strip();

            //Validate pattern, may throw validation error
            validatePattern(inputedEmail);

            //Is there an existent user with this email.
            boolean userFound = false;
            try {
                if (userJpaController.getUserByEmail(inputedEmail) != null) {
                    userFound = true;
                }
            } catch (NonUniqueResultException ex) {
                userFound = true;
            } catch (NoResultException ex) {
                userFound = false;
            }

            //If so, throw a validation error
            if (userFound) {
                String title = Messages.getString("Field") + ": " + inputedEmail;
                String message = Messages.getString("DuplicateEmail");
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        title, message);
                throw new ValidatorException(facesMessage);
            }
        }
    }

    /**
     * Validates the email entered doesn't belong to another user than the one
     * specified. If its not, go to standard email validator.
     *
     * @param inputedEmail Inputed email
     * @param userId ID user for whom the email was inputed
     * @throws ValidatorException
     */
    public void validate(String inputedEmail, Integer userId) throws ValidatorException {
        if (inputedEmail != null && userId != null && !inputedEmail.isBlank()) {
            // Sanitize the value passed to this method
            String sanitizedEmail = ((String) inputedEmail).strip();

            //Is there an other existent user with this email.
            boolean otherUserFound = false;
            Users userWithSameEmail;
            try {
                userWithSameEmail = userJpaController.getUserByEmail(sanitizedEmail);
                //If a user with email have been found but does not have 
                //same id
                if (userWithSameEmail != null
                        && !userWithSameEmail.getUserId().equals(userId)) {
                    otherUserFound = true;
                }
            } catch (NonUniqueResultException ex) {
                otherUserFound = true;
            } catch (NoResultException ex) {
                otherUserFound = false;
            }

            //If so, throw a validation error
            if (otherUserFound) {
                String title = Messages.getString("Field") + ": " + sanitizedEmail;
                String message = Messages.getString("DuplicateEmail");
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        title, message);
                throw new ValidatorException(facesMessage);
            } //Otherwise, validate pattern, might throw a validation error
            else {
                validatePattern(inputedEmail);
            }
        }
    }

    private void validatePattern(String input) {
        if (!input.matches(PATTERN)) {
            String title = Messages.getString("Field") + ": " + input;
            FacesMessage facesMessage
                    = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            title,
                            Messages.getString("EmailRegexError"));

            throw new ValidatorException(facesMessage);
        }
    }
}
