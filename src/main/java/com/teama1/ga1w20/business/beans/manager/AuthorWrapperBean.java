package com.teama1.ga1w20.business.beans.manager;

import com.teama1.ga1w20.persistence.entities.Authors;
import java.io.Serializable;
import java.util.Objects;

/**
 * This bean wraps an author with a boolean for whether it exists or not. It is
 * used in beans and xhtml pages where authors are created or edited.
 *
 * @author Michael
 */
public class AuthorWrapperBean implements Serializable {

    private Authors author;
    private boolean authorExistent;

    public AuthorWrapperBean(Authors author, boolean authorExistent) {
        this.author = author;
        this.authorExistent = authorExistent;
    }

    public Authors getAuthor() {
        return author;
    }

    public void setAuthor(Authors author) {
        this.author = author;
    }

    public boolean isAuthorExistent() {
        return authorExistent;
    }

    public void setAuthorExistent(boolean authorExistent) {
        this.authorExistent = authorExistent;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + Objects.hashCode(this.author);
        hash = 61 * hash + (this.authorExistent ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AuthorWrapperBean other = (AuthorWrapperBean) obj;
        if (this.authorExistent != other.authorExistent) {
            return false;
        }
        if (!this.author.equals(other.author)) {
            return false;
        }
        return true;
    }
}
