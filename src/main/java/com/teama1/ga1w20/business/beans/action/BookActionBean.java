package com.teama1.ga1w20.business.beans.action;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.beans.CookieBean;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Michael, Timmy
 */
@Named("bookActionBean")
@SessionScoped
public class BookActionBean implements Serializable {

    @Inject
    private AuthorsJpaController authorsController;
    @Inject
    private BooksJpaController bookController;
    @Inject
    private FormatsJpaController formatsController;
    @Inject
    private GenresJpaController genresController;
    @Inject
    private CookieBean cookieBean;

    public Books getBookByIsbn(Long isbn) {
        return bookController.findBooks(isbn);
    }

    public List<Authors> getAuthors(Books book) {
        return authorsController.getAuthorsByBook(book);
    }

    public String getCover(Long isbn) {
        return String.format(Constants.BOOK_COVERS_THUMBNAILS_SHORT_PATH.concat("/%d_thumb"), isbn).concat(Constants.IMAGES_EXTENSION);
    }

    public String goToBookPage(Long isbn) {
        return String.format("/book.xhtml?isbn=%d", isbn);
    }

    public List<Formats> getFormats(Books book) {
        return formatsController.getFormatsByBook(book);
    }

    public StreamedContent downloadBook(String title, String format) {
        return DefaultStreamedContent.builder()
                .name(title + "." + format)
                .stream(() -> FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream(Constants.DEFAULT_DOWNLOAD_BOOK_URL))
                .build();
    }

    public List<Genres> getGenres(Books book) {
        return genresController.getGenresByBook(book);
    }
}
