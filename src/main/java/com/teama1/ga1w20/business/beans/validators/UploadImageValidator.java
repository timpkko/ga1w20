/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.business.utils.Messages;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.persistence.Transient;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UploadImageValidator validates an UploadedFile to ensure that the uploaded
 * image meets the criteria.
 *
 * @author Timmy
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator("imageValidator")
public class UploadImageValidator implements Validator {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(UploadImageValidator.class);

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {

        // Null check
        if (value == null) {
            Messages.addMessageToContext(FacesMessage.SEVERITY_INFO, "Error", "ImageRequiredMsg");
            return;
        }

        UploadedFile uploadedImage = (UploadedFile) value;

        // Check if the file is uploaded
        if (uploadedImage.getSize() == 0) {
            // Check if the fileUpload tag is required
            UIInput inputComponent = (UIInput) uiComponent;
            if (!inputComponent.isRequired()) {
                return;
            }

            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("ImageRequiredMsg"));
            throw new ValidatorException(msg);
        }

        // Check if the file is not exceeding 1MB
        if (uploadedImage.getSize() > 1024000) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("ImageSizeLimitError"));
            throw new ValidatorException(msg);
        }

        // Check if the file is a jpg image
        if (!uploadedImage.getContentType().equals("image/jpeg")) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("ImageUploadWrongType"));
            throw new ValidatorException(msg);
        }
    }

}
