package com.teama1.ga1w20.business.beans;

import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Bean with session scope to manage the locale and i18n.
 *
 * @author Camillia, Shuey
 */
@Named("sessionLocaleBean")
@SessionScoped
public class SessionLocaleBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SessionLocaleBean.class);

    @Inject
    private CookieBean cookieBean;

    private Locale locale;

    /**
     * Sets locale to en-CA or fr-CA. Default being en-CA.
     */
    @PostConstruct
    public void init() {
        //Attempt to retrieve the cookies with the user's preferred language
        String languageCookieValue = cookieBean.getCookie(Constants.LANGUAGE_COOKIE_NAME);
        String countryCookieValue = cookieBean.getCookie(Constants.COUNTRY_COOKIE_NAME);

        if ((!languageCookieValue.isBlank()) && (!countryCookieValue.isBlank())) {
            locale = new Locale(languageCookieValue, countryCookieValue);
            FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
            return;
        }

        //If they were not stored in cookies, retrieve from request locale
        locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
        if (locale.getLanguage().compareToIgnoreCase("fr") == 0) {
            locale = new Locale("fr", "ca");
        } else {
            locale = new Locale("en", "ca");
        }
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    }

    public Locale getLocale() {
        return locale;
    }

    public String getLanguage() {
        return locale.getLanguage();
    }

    public void setLanguage(String language, String country) {
        locale = new Locale(language, country);

        //Since the user has changed the language of the website, we can assume
        //that it's their preferred language and set the langauge and country cookies
        cookieBean.addCookie(Constants.LANGUAGE_COOKIE_NAME, language);
        cookieBean.addCookie(Constants.COUNTRY_COOKIE_NAME, country);

        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    }
}
