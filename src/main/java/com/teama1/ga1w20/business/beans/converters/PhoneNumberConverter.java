package com.teama1.ga1w20.business.beans.converters;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter to be applied on string representing phone numbers, it keeps only
 * numbers and characters that specify an extension.
 *
 * @author Camillia E.
 */
@FacesConverter(value = "phoneNumberConverter")
public class PhoneNumberConverter implements Converter {

    /**
     * Keeps only numbers and characters that specify an extension from the
     * entered value in a HtmlInputText.
     */
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string != null && uic instanceof HtmlInputText) {
            return string.replaceAll("[^0-9#xno.]", "");
        }
        return string;
    }

    /**
     * Return the value as is, for presentation
     */
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object value) {
        return (value == null ? null : value.toString());
    }
}
