package com.teama1.ga1w20.business.beans.validators;

import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputnumber.InputNumber;

/**
 * Validates the wholesalePrice, listPrice and salePrice fields submitted to
 * ensure that the salePrice is in between the listPrice and wholesalePrice.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "salePriceValidator")
public class SalePriceValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        //Check to see where this validator is being called from.
        //Here, using instance of verifies if the call is coming from the
        //newBook or inventory page.
        if (uic.getAttributes().get("wholesale") instanceof InputNumber) {
            InputNumber input1 = (InputNumber) uic.getAttributes().get("wholesale");
            InputNumber input2 = (InputNumber) uic.getAttributes().get("list");
            InputNumber input3 = (InputNumber) uic.getAttributes().get("sale");

            if (input1.getSubmittedValue() != null && input2.getSubmittedValue() != null && input3.getSubmittedValue() != null) {
                //The following ternary operators check if the submitted field is blank or not.
                //If it is, assign it to null to make managing the validation easier in the if statement following.
                //If it's not, parse it properly.
                BigDecimal wholesalePrice = ((String) input1.getSubmittedValue()).isBlank()
                        ? null : new BigDecimal((String) input1.getSubmittedValue());
                BigDecimal listPrice = ((String) input2.getSubmittedValue()).isBlank()
                        ? null : new BigDecimal((String) input2.getSubmittedValue());
                BigDecimal salePrice = ((String) input3.getSubmittedValue()).isBlank()
                        ? null : new BigDecimal((String) input3.getSubmittedValue());

                if (wholesalePrice != null && listPrice != null && salePrice != null && (salePrice.compareTo(listPrice) >= 0 || salePrice.compareTo(wholesalePrice) < 0) && salePrice.compareTo(BigDecimal.ZERO) != 0) {
                    FacesMessage msg = new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksValidate_SaleListWhole}", String.class));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    throw new ValidatorException(msg);
                }
            }
        } else {
            BigDecimal wholesalePrice = (BigDecimal) uic.getAttributes().get("wholesale");
            BigDecimal listPrice = (BigDecimal) uic.getAttributes().get("list");
            InputNumber input3 = (InputNumber) uic.getAttributes().get("sale");

            if (input3.getSubmittedValue() != null) {
                //The following ternary operator checks if the submitted field is blank or not.
                //If it is, assign it to null to make managing the validation easier in the if statement following.
                //If it's not, parse it properly.
                BigDecimal salePrice = ((String) input3.getSubmittedValue()).isBlank()
                        ? null : new BigDecimal((String) input3.getSubmittedValue());

                if (wholesalePrice != null && listPrice != null && salePrice != null && (salePrice.compareTo(listPrice) >= 0 || salePrice.compareTo(wholesalePrice) < 0) && salePrice.compareTo(BigDecimal.ZERO) != 0) {
                    FacesMessage msg = new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksValidate_SaleListWhole}", String.class));
                    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                    fc.addMessage(null, msg);
                    throw new ValidatorException(msg);
                }
            }
        }
    }
}
