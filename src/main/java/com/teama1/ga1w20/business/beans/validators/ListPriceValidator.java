package com.teama1.ga1w20.business.beans.validators;

import java.math.BigDecimal;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputnumber.InputNumber;

/**
 * Validates the wholesalePrice and listPrice fields submitted to ensure that
 * the listPrice is greater than the wholesalePrice.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "listPriceValidator")
public class ListPriceValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        InputNumber input1 = (InputNumber) uic.getAttributes().get("wholesale");
        InputNumber input2 = (InputNumber) uic.getAttributes().get("list");

        if (input1.getSubmittedValue() != null && input2.getSubmittedValue() != null) {
            //The following ternary operators check if the submitted field is blank or not.
            //If it is, assign it to null to make managing the validation easier in the if statement following.
            //If it's not, parse it properly.
            BigDecimal wholesalePrice = ((String) input1.getSubmittedValue()).isBlank()
                    ? null : new BigDecimal((String) input1.getSubmittedValue());
            BigDecimal listPrice = ((String) input2.getSubmittedValue()).isBlank()
                    ? null : new BigDecimal((String) input2.getSubmittedValue());

            if (wholesalePrice != null && listPrice != null && listPrice.compareTo(wholesalePrice) < 0) {
                FacesMessage msg = new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksValidate_ListWhole}", String.class));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }
}
