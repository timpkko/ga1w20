package com.teama1.ga1w20.business.beans.manager;

import com.teama1.ga1w20.persistence.entities.Genres;
import java.io.Serializable;
import java.util.Objects;

/**
 * This bean wraps a genre with a boolean for whether it exists or not. It is
 * used in beans and xhtml pages where genres are created or edited.
 *
 * @author Michael
 */
public class GenreWrapperBean implements Serializable {

    private Genres genre;
    private boolean genreExistent;

    public GenreWrapperBean(Genres genre, boolean genreExistent) {
        this.genre = genre;
        this.genreExistent = genreExistent;
    }

    public Genres getGenre() {
        return genre;
    }

    public void setGenre(Genres genre) {
        this.genre = genre;
    }

    public boolean isGenreExistent() {
        return genreExistent;
    }

    public void setGenreExistent(boolean genreExistent) {
        this.genreExistent = genreExistent;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.genre);
        hash = 61 * hash + (this.genreExistent ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GenreWrapperBean other = (GenreWrapperBean) obj;
        if (this.genreExistent != other.genreExistent) {
            return false;
        }
        if (!this.genre.equals(other.genre)) {
            return false;
        }
        return true;
    }
}
