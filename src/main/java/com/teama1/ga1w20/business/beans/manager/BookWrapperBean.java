package com.teama1.ga1w20.business.beans.manager;

import com.teama1.ga1w20.persistence.entities.Books;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This bean wraps a book with all of its bridged entities. It is used in beans
 * and xhtml pages where books are edited.
 *
 * @author Michael
 */
public class BookWrapperBean implements Serializable {

    private Books book;
    private ArrayList<AuthorWrapperBean> authors;
    private ArrayList<GenreWrapperBean> genres;
    private ArrayList<FormatWrapperBean> formats;

    public BookWrapperBean() {

    }

    public BookWrapperBean(Books book) {
        this.book = book;
    }

    public BookWrapperBean(Books book, ArrayList<AuthorWrapperBean> authors, ArrayList<GenreWrapperBean> genres, ArrayList<FormatWrapperBean> formats) {
        this.book = book;
        this.authors = authors;
        this.genres = genres;
        this.formats = formats;
    }

    public Books getBook() {
        return book;
    }

    public void setBook(Books book) {
        this.book = book;
    }

    public ArrayList<AuthorWrapperBean> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<AuthorWrapperBean> authors) {
        this.authors = authors;
    }

    public ArrayList<GenreWrapperBean> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<GenreWrapperBean> genres) {
        this.genres = genres;
    }

    public ArrayList<FormatWrapperBean> getFormats() {
        return formats;
    }

    public void setFormats(ArrayList<FormatWrapperBean> formats) {
        this.formats = formats;
    }
}
