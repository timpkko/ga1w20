package com.teama1.ga1w20.business.beans;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Taxrates;
import com.teama1.ga1w20.persistence.jpacontrollers.TaxratesJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Session bean that is used to keep track of the books that the user adds to
 * their cart
 *
 * @author @FishYeho
 */
@Named("cartBean")
@SessionScoped
public class CartBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(CartBean.class);

    @Inject
    private SessionUserBean sessionBean;

    @Inject
    private TaxratesJpaController taxratesController;

    private List<Books> cart;
    private int userId;

    private String billingAddress;
    private String billingCity;
    private String billingProvince;
    private String creditCardType;

    /**
     * Constructor for a CartBean, creates an empty ArrayList to store the
     * user's books
     *
     * @author @FishYeho
     */
    public CartBean() {
        this.cart = new ArrayList<>();
        this.userId = 1;
    }

    /**
     * Get method that returns the cart private field
     *
     * @author @FishYeho
     * @return The cart private field
     */
    public List<Books> getCart() {
        return this.cart;
    }

    /**
     * Set method that sets the cart private field
     *
     * @author @FishYeho
     * @param cart
     */
    public void setCart(List<Books> cart) {
        this.cart = cart;
    }

    /**
     * Get method that returns the userId private field
     *
     * @author @FishYeho
     * @return The userId private field
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * Set method that sets the userId private field
     *
     * @author @FishYeho
     * @param userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Get method that returns the billingAddress private field
     *
     * @author @FishYeho
     * @return The billingAddress private field
     */
    public String getBillingAddress() {
        return this.billingAddress;
    }

    /**
     * Set method that sets the billingAddress private field
     *
     * @author @FishYeho
     * @param billingAddress
     */
    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * Get method that returns the billingCity private field
     *
     * @author @FishYeho
     * @return The billingCity private field
     */
    public String getBillingCity() {
        return this.billingCity;
    }

    /**
     * Set method that sets the billingCity private field
     *
     * @author @FishYeho
     * @param billingCity
     */
    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    /**
     * Get method that returns the billingProvince private field
     *
     * @return The billingProvince private field
     */
    public String getBillingProvince() {
        return this.billingProvince;
    }

    /**
     * Set method that sets the billingProvince private field
     *
     * @author @FishYeho
     * @param billingProvince
     */
    public void setBillingProvince(String billingProvince) {
        this.billingProvince = billingProvince;
    }

    /**
     * Get method that returns the creditCardType private field
     *
     * @author @FishYeho
     * @return The creditCardType private field
     */
    public String getCreditCardType() {
        return this.creditCardType;
    }

    /**
     * Set method that sets the creditCardType private field
     *
     * @author @FishYeho
     * @param creditCardType
     */
    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    /**
     * Method that adds a book to the cart ArrayList
     *
     * @author @FishYeho
     * @param book
     */
    public void addBook(Books book) {
        this.cart.add(book);
    }

    /**
     * Method that removes the specified book from the cart ArrayList
     *
     * @author @FishYeho
     * @param book
     */
    public void removeBook(Books book) {
        this.cart.remove(book);
    }

    /**
     * Method that checks if the specified book is already in the user's cart
     *
     * @author @FishYeho
     * @param book
     * @return true if the book is already in the cart
     */
    public boolean isBookInCart(Books book) {
        return this.cart.contains(book);
    }

    /**
     * Method that removes all books from the cart ArrayList
     *
     * @author @FishYeho
     */
    public void emptyCart() {
        this.cart = null;
    }

    /**
     * Method that calculates the total cost of the books inside of the CartBean
     * before taxes
     *
     * @author @FishYeho
     * @return A BigDecimal representing the total cost of the CartBean before
     * taxes
     */
    public BigDecimal calculateCartTotalBeforeTax() {
        BigDecimal cartTotalBeforeTax = BigDecimal.ZERO;

        for (Books book : this.getCart()) {
            //If the book is not on sale, then we use the list price
            if (book.getSalePrice().compareTo(BigDecimal.ZERO) == 0) {
                cartTotalBeforeTax = cartTotalBeforeTax.add(book.getListPrice());
            } //Otherwise, we use the sale price of the book
            else {
                cartTotalBeforeTax = cartTotalBeforeTax.add(book.getSalePrice());
            }
        }
        return cartTotalBeforeTax.round(new MathContext(4));
    }

    /**
     * Method that calculates the total cost of the books inside of the CartBean
     * after applying taxes
     *
     * @author @FishYeho
     * @return A BigDecimal representing the total cost of the CartBean after
     * taxes
     */
    public BigDecimal calculateCartTotalAfterTax() {
        BigDecimal cartTotalAfterTax = this.calculateCartTotalBeforeTax().add(this.calculateTax());
        return cartTotalAfterTax.round(new MathContext(4));
    }

    /**
     * Method that calculates the total amount of tax for the current user's
     * cart
     *
     * @author @FishYeho
     * @return A BigDecimal representing the total amount of tax for the user's
     * cart
     */
    public BigDecimal calculateTax() {
        BigDecimal cartTotalBeforeTax = this.calculateCartTotalBeforeTax();
        Taxrates taxratesForCurrentUser = null;

        try {
            taxratesForCurrentUser = taxratesController.getRatesForUser(sessionBean.getUser().getUserId());
        } catch (NonexistentEntityException nee) {
            FacesContext.getCurrentInstance().addMessage("invoice:invoice", new FacesMessage("Error", Messages.getString("SaveInvoiceErrMsg")));
        }

        BigDecimal gstRate = taxratesForCurrentUser.getGstRate().divide(new BigDecimal("100"));
        BigDecimal pstRate = taxratesForCurrentUser.getPstRate().divide(new BigDecimal("100"));
        BigDecimal hstRate = taxratesForCurrentUser.getHstRate().divide(new BigDecimal("100"));

        BigDecimal gstTotal = gstRate.multiply(cartTotalBeforeTax);
        BigDecimal pstTotal = pstRate.multiply(cartTotalBeforeTax);
        BigDecimal hstTotal = hstRate.multiply(cartTotalBeforeTax);

        BigDecimal totalTax = gstTotal.add(pstTotal).add(hstTotal);

        return totalTax.round(new MathContext(4));
    }
}
