/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.teama1.ga1w20.business.beans.validators;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RssFeedValidator is a validator that validates an RSS feed URL. The URL must
 * point to an XML (or RSS) file with the tag rss.
 *
 * @author Timmy
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator("rssfeedValidator")
public class RssFeedValidator implements Validator {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(RssFeedValidator.class);

    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }

        String url = value.toString();
        if (!checkForRssTag(url)) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }

    private boolean checkForRssTag(String url) {
        try {
            // Download the file from the URL
            URL rssFeedUrl = new URL(url);
            try (BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(rssFeedUrl.openStream()))) {

                String inputLine;
                // Check if line has an <rss> tag from the file
                while ((inputLine = bufferedReader.readLine()) != null) {
                    if (inputLine.contains("<rss ")) {
                        return true;
                    }
                }
            }

        } catch (MalformedURLException ex) {
            LOG.error("URL is in a wrong format: " + url);
        } catch (IOException ex) {
            LOG.error("Cannot open the stream: " + ex);
        }

        return false;
    }
}
