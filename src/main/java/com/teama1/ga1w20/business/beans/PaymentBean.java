package com.teama1.ga1w20.business.beans;

import com.teama1.ga1w20.business.beans.validators.LuhnCheck;
import java.io.Serializable;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.validation.constraints.Future;
import javax.validation.constraints.Size;

/**
 * Backing bean with JSR-303 Bean Validation
 *
 * @author Ken
 * @author @FishYeho
 */
@Named("paymentBean")
@RequestScoped
public class PaymentBean implements Serializable {

    @Size(min = 16, message = "Credit card must contain 16 digits")
    @LuhnCheck
    private String cardNumber = "";

    @Future
    private Date date = new Date();

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return this.cardNumber;
    }

    public void setDate(Date newValue) {
        date = newValue;
    }

    public Date getDate() {
        return date;
    }
}
