package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.jpacontrollers.TaxratesJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Validator for region. Checks it does exist in database.
 *
 * Note: Declared as a CDI bean so TaxratesJpaController can be injected.
 *
 * @author Camillia E.
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@Named("regionValidator")
@RequestScoped
public class RegionValidator implements Validator {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(RegionValidator.class);

    @Inject
    private TaxratesJpaController taxratesJpaController;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value != null && !((String) value).isBlank()) {
            // Retrieve the value passed to this method
            String inputedValue = ((String) value).strip();

            //Is the region existent in db
            boolean regionFound = false;
            try {
                if (taxratesJpaController.getRateByRegion(inputedValue) != null) {
                    regionFound = true;
                }
            } catch (NonUniqueResultException ex) {
                regionFound = true;
            } catch (NoResultException | NonexistentEntityException ex) {
                regionFound = false;
            }

            //If bot, throw a validation error
            if (!regionFound) {
                String message = Messages.getString("RegionNonExistent");
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        message, message);
                throw new ValidatorException(facesMessage);
            }
        }
    }
}
