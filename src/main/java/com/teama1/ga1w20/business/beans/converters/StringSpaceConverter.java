package com.teama1.ga1w20.business.beans.converters;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Converter to be applied on strings, which will trim any leading, in-between
 * and tailing white spaces
 *
 * @author Camillia E.
 */
@FacesConverter(value = "removeWhiteSpaceConverter")
public class StringSpaceConverter implements Converter {

    /**
     * Remove any whitespaces from the entered value in a HtmlInputText.
     */
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string != null && uic instanceof HtmlInputText) {
            return string.replaceAll("\\s", "");
        }
        return string;
    }

    /**
     * Return the value as is, for presentation
     */
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object value) {
        return (value == null ? null : value.toString());
    }
}
