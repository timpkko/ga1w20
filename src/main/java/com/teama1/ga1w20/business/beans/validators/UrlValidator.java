package com.teama1.ga1w20.business.beans.validators;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UrlValidator is a validator that validates an URL.
 *
 * @author Timmy
 */
@FacesValidator("com.ga1w20.urlValidator")
public class UrlValidator implements Validator {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(UrlValidator.class);

    /**
     * Validates an input by checking if it is formatted as an URL.
     *
     * @param facesContext
     * @param uiComponent
     * @param value
     * @throws ValidatorException
     */
    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
        if (value == null) {
            return;
        }

        // Get the value of the input
        String url = value.toString();
        // Throw error if the value is not an URL
        if (!isUrlFormatted(url)) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }

    /**
     * Checks if the string is well formatted as an URL and as an URI.
     *
     * @param url
     * @return
     */
    private boolean isUrlFormatted(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (MalformedURLException ex) {
            LOG.debug("Cannot parse URL: " + url + " because of exception: " + ex.getMessage());
        } catch (URISyntaxException ex) {
            LOG.debug("Cannot parse URI: " + url + " because of exception: " + ex.getMessage());
        }

        return false;
    }
}
