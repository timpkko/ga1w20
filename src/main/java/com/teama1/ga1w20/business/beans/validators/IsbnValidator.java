package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.persistence.entities.Books;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputnumber.InputNumber;

/**
 * Validates the ISBN-13 fields submitted for a duplicate ISBN-13.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "isbnValidator")
public class IsbnValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        InputNumber input = (InputNumber) uic.getAttributes().get("isbn");

        if (input.getSubmittedValue() != null) {
            //The following ternary operator checks if the submitted field is blank or not.
            //If it is, assign it to null to make managing the validation easier in the if statement following.
            //If it's not, parse it properly.
            Long isbn = ((String) input.getSubmittedValue().toString()).isBlank()
                    ? null : Long.parseLong((String) input.getSubmittedValue());
            List<Books> books = (List<Books>) uic.getAttributes().get("books");

            if (isDuplicate(isbn, books)) {
                FacesMessage msg = new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksValidate_DuplicateIsbn}", String.class));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }

    /**
     * Verifies if the current ISBN-13 the user wants to create already exists
     * in the database.
     *
     * @param isbn
     * @param books
     * @return whether the current ISBN-13 exists in the list of ISBN-13s.
     */
    private boolean isDuplicate(Long isbn, List<Books> books) {
        if (isbn != null) {
            for (Books b : books) {
                if (isbn.equals(b.getIsbn())) {
                    return true;
                }
            }
        }
        return false;
    }
}
