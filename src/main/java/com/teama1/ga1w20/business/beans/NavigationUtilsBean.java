package com.teama1.ga1w20.business.beans;

import com.teama1.ga1w20.Constants;
import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains public methods that perform general navigationBean for
 * our application
 *
 * @author Shuey, Camillia
 */
@Named("navigationBean")
@SessionScoped
public class NavigationUtilsBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(NavigationUtilsBean.class);

    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Method that will save the URL of the page that the user is currently on
     * to the Session object
     *
     * @author @FishYeho
     */
    public void savePageURL() {
        LOG.debug("savePageURL");
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        LOG.debug("savePageURL - url : " + request.getRequestURL().toString());
        this.setUrl(request.getRequestURL().toString());
    }

    /**
     * Method that will save the URL of the books page that the user is
     * currently on.
     *
     * @author Camillia E.
     */
    public void saveBookPageURL() {
        LOG.debug("saveBOOKPageURL");
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        String isbnParam = request.getParameter("isbn");
        //If there are no isbn parameter, do not save this URL
        if (isbnParam == null) {
            return;
        }
        String fullUrl = request.getRequestURL().toString() + "?isbn=" + isbnParam;
        this.setUrl(fullUrl);
    }

    /**
     * Method that will save the URL of the genre page that the user is
     * currently on.
     *
     * @author Camillia E.
     */
    public void saveGenrePageURL() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        String genreIdParam = request.getParameter("genreId");
        //If there are no genreId parameter, do not save this URL
        if (genreIdParam == null) {
            return;
        }
        String fullUrl = request.getRequestURL().toString() + "?genreId=" + genreIdParam;
        this.setUrl(fullUrl);
    }

    /**
     * Method that will redirect the user to the page that they were previously
     * on
     *
     * @author @FishYeho
     */
    public void redirectToPreviousPage() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (this.getUrl() == null) {
            try {
                context.getExternalContext().redirect(Constants.INDEX_PAGE_URL);
            } catch (IOException ex) {
                LOG.error("Redirect failed");
            }
            return;
        }

        try {
            context.getExternalContext().redirect(this.getUrl());
        } catch (IOException ex) {
            LOG.error("Redirect failed");
        }
    }

    /**
     * Verifies if the user came from the finalization page.
     *
     * @return True if the latest saved url page matches the finalization page.
     * @author Camillia
     */
    public boolean shouldRedirectToFinalization() {
        if (this.url == null) {
            return false;
        }
        return this.url.endsWith(Constants.FINALIZATION_PAGE_END_OF_URL);
    }

    /**
     * Verifies if the user came from the checkout page.
     *
     * @return True if the latest saved url page matches the checkout page.
     * @author Camillia
     */
    public boolean shouldRedirectToCheckout() {
        if (this.url == null) {
            return false;
        }
        return this.url.endsWith(Constants.CHECKOUT_PAGE_END_OF_URL);
    }

}
