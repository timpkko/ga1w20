package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.persistence.entities.Genres;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validates the genre fields submitted for a duplicate genre.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "genreValidator")
public class GenreValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        if (t != null) {
            String genreName = (String) t;
            List<Genres> genres = (List<Genres>) uic.getAttributes().get("genres");

            if (isDuplicate(genreName.toLowerCase().replaceAll("\\s", ""), genres)) {
                FacesMessage msg = new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksValidate_DuplicateGenre}", String.class));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }

    /**
     * Verifies if the current genre the user wants to create already exists in
     * the database.
     *
     * @param genreName
     * @param genres
     * @return whether the current genre exists in the list of genres.
     */
    private boolean isDuplicate(String genreName, List<Genres> genres) {
        if (genreName != null) {
            for (Genres g : genres) {
                if (genreName.equals(g.getName().toLowerCase().replaceAll("\\s", ""))) {
                    return true;
                }
            }
        }
        return false;
    }
}
