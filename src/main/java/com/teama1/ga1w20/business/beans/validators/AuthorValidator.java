package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Authors;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;

/**
 * Validates the author fields submitted for a duplicate author.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "authorValidator")
public class AuthorValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        InputText input1 = (InputText) uic.getAttributes().get("firstName");
        InputText input2 = (InputText) uic.getAttributes().get("lastName");

        if (input1.getSubmittedValue() != null && input2.getSubmittedValue() != null) {
            //The following ternary operators check if the submitted field is blank or not.
            //If it is, assign it to null to make managing the validation easier in the if statement following.
            //If it's not, parse it properly.
            String firstName = ((String) input1.getSubmittedValue()).isBlank()
                    ? null : ((String) input1.getSubmittedValue()).toLowerCase().replaceAll("\\s", "");
            String lastName = ((String) input2.getSubmittedValue()).isBlank()
                    ? null : ((String) input2.getSubmittedValue()).toLowerCase().replaceAll("\\s", "");
            List<Authors> authors = (List<Authors>) uic.getAttributes().get("authors");

            if (isDuplicate(firstName, lastName, authors)) {
                FacesMessage msg = new FacesMessage(Messages.getString("CreateBooksValidate_DuplicateAuthor"));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }

    /**
     * Verifies if the current author the user wants to create already exists in
     * the database.
     *
     * @param firstName
     * @param lastName
     * @param authors
     * @return whether the current author exists in the list of authors.
     */
    private boolean isDuplicate(String firstName, String lastName, List<Authors> authors) {
        if (firstName != null) {
            if (lastName != null) {
                for (Authors a : authors) {
                    if (a.getLastname() != null && firstName.equals(a.getFirstname().toLowerCase().replaceAll("\\s", "")) && lastName.equals(a.getLastname().toLowerCase().replaceAll("\\s", ""))) {
                        return true;
                    }
                }
            } else {
                for (Authors a : authors) {
                    if (a.getLastname() == null && firstName.equals(a.getFirstname().toLowerCase().replaceAll("\\s", ""))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
