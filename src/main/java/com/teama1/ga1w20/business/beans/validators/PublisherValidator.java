package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.persistence.entities.Publishers;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validates the publisher fields submitted for a duplicate publisher.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "publisherValidator")
public class PublisherValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        if (t != null) {
            String pubName = (String) t;
            List<Publishers> publishers = (List<Publishers>) uic.getAttributes().get("publishers");

            if (isDuplicate(pubName.toLowerCase().replaceAll("\\s", ""), publishers)) {
                FacesMessage msg = new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksValidate_DuplicatePublisher}", String.class));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }

    /**
     * Verifies if the current publisher the user wants to create already exists
     * in the database.
     *
     * @param pubName
     * @param publishers
     * @return whether the current publisher exists in the list of publishers.
     */
    private boolean isDuplicate(String pubName, List<Publishers> publishers) {
        if (pubName != null) {
            for (Publishers p : publishers) {
                if (pubName.equals(p.getName().toLowerCase().replaceAll("\\s", ""))) {
                    return true;
                }
            }
        }
        return false;
    }
}
