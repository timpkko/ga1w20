package com.teama1.ga1w20.business.beans;

import com.teama1.ga1w20.Constants;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.servlet.http.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to add new cookies for user's that visit the website
 *
 * @author @FishYeho
 */
@Named("cookieBean")
@SessionScoped
public class CookieBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(CookieBean.class);

    private String cookieName;
    private String cookieValue;
    private Date cookieExpiry;
    private boolean areCookiesSupported;

    public String getCookieName() {
        return this.cookieName;
    }

    public void setCookieName(String cookieName) {
        this.cookieName = cookieName;
    }

    public String getCookieValue() {
        return this.cookieValue;
    }

    public void setCookieValue(String cookieValue) {
        this.cookieValue = cookieValue;
    }

    public Date getCookieExpiry() {
        return this.cookieExpiry;
    }

    public void setCookieExpiry(Date cookieExpiry) {
        this.cookieExpiry = cookieExpiry;
    }

    public boolean doesBroswerSupportCookies() {
        return this.areCookiesSupported;
    }

    public void setAreCookiesSupported(boolean areCookiesSupported) {
        this.areCookiesSupported = areCookiesSupported;
    }

    /**
     * Method that returns the specified Cookie from the browser if it exists
     *
     * @author @FishYeho
     * @param cookieName
     * @return The String value of the specified Cookie
     */
    public String getCookie(String cookieName) {
        FacesContext context = FacesContext.getCurrentInstance();
        Cookie cookie = (Cookie) context.getExternalContext().getRequestCookieMap().get(cookieName);
        if (cookie == null) {
            return "";
        }
        return cookie.getValue();
    }

    /**
     * Method that will add the specified cookie to the user's browser
     *
     * @author @FishYeho
     * @param cookieName The name of the Cookie
     * @param cookieValue The value of the Cookie
     */
    public void addCookie(String cookieName, String cookieValue) {

        FacesContext context = FacesContext.getCurrentInstance();

        Map<String, Object> requestCookieMap = new HashMap<>();
        requestCookieMap.put("path", "/");
        requestCookieMap.put("maxAge", 31536000);

        //Add the user's cookie to the browser
        context.getExternalContext().addResponseCookie(cookieName, cookieValue, requestCookieMap);
    }

    /**
     * Method that will attempt to write a Cookie to the user's browser to test
     * if the browser accepts Cookies
     *
     * @author @FishYeho
     */
    public void tryToWriteCookie() {
        this.addCookie("cookiesEnabled", "true");

        //Redirect to the readCookie page
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            context.getExternalContext().redirect(Constants.READ_COOKIE_URL);
        } catch (IOException ex) {
            LOG.error("Redirect failed");
        }
    }

    /**
     * Method that will attempt to read the Cookie that was previously written
     * by the tryToWriteCookie method in order to determine if the user's
     * browser supports Cookies
     *
     * @author @FishYeho
     * @return True if the browser supports Cookies, otherwise false
     */
    public boolean tryToReadCookie() {

        FacesContext context = FacesContext.getCurrentInstance();
        Object cookie = context.getExternalContext().getRequestCookieMap().get("cookiesEnabled");

        //If the Cookie is not null, that means that the broswer supports Cookies
        if (cookie != null) {
            this.setAreCookiesSupported(true);
            try {
                context.getExternalContext().redirect(Constants.INDEX_PAGE_URL);
            } catch (IOException ex) {
                LOG.error("Redirect failed");
            }
            return true;
        }
        //Otherwise, the browser doesn't support Cookies
        this.setAreCookiesSupported(false);
        try {
            context.getExternalContext().redirect(Constants.INDEX_PAGE_URL);
        } catch (IOException ex) {
            LOG.error("Redirect failed");
        }
        return false;
    }
}
