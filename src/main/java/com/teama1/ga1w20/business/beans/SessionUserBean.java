package com.teama1.ga1w20.business.beans;

import com.teama1.ga1w20.persistence.entities.Credentials;
import com.teama1.ga1w20.persistence.entities.Users;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * Bean with session scope which keeps track of if a user is logged in, and
 * information about the logged in user. Used by login and logout methods in
 * LoginBackingBean.
 *
 * @author Camillia
 */
@Named("sessionUserBean")
@SessionScoped
public class SessionUserBean implements Serializable {

    private Users user;

    private Credentials userCredentials;

    ////////////////////////////////////////////////////////////////////////////
    //Public Methods
    ////////////////////////////////////////////////////////////////////////////
    public SessionUserBean() {
        user = null;
        userCredentials = null;
    }

    /**
     * Is there a logged in user?
     *
     * @return
     */
    public boolean isUserLoggedIn() {
        return user != null && userCredentials != null;
    }

    /**
     * Is there a logged-in user and if so, is it a manager?
     *
     * @return
     */
    public boolean isUserAManager() {
        return isUserLoggedIn() ? userCredentials.getManager() : false;
    }

    /**
     * Set user associated to the session.
     *
     * @param user
     * @param credentials
     */
    public void setLoggedInUser(Users user, Credentials credentials) {
        this.user = user;
        this.userCredentials = credentials;
    }

    /**
     * Unset the user that might have been associated to this session.
     */
    public void clearLoggedInUser() {
        this.user = null;
        this.userCredentials = null;
    }

    ////////////////////////////////////////////////////////////////////////////
    //Setters & Getters
    ////////////////////////////////////////////////////////////////////////////
    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Credentials getUserCredentials() {
        return userCredentials;
    }

    public void setUserCredentials(Credentials userCredentials) {
        this.userCredentials = userCredentials;
    }
}
