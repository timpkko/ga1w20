package com.teama1.ga1w20.business.beans.manager;

import com.teama1.ga1w20.persistence.entities.Formats;
import java.io.Serializable;
import java.util.Objects;

/**
 * This bean wraps a format with a boolean for whether it exists or not. It is
 * used in beans and xhtml pages where formats are created or edited.
 *
 * @author Michael
 */
public class FormatWrapperBean implements Serializable {

    private Formats format;
    private boolean formatExistent;

    public FormatWrapperBean(Formats format, boolean formatExistent) {
        this.format = format;
        this.formatExistent = formatExistent;
    }

    public Formats getFormat() {
        return format;
    }

    public void setFormat(Formats format) {
        this.format = format;
    }

    public boolean isFormatExistent() {
        return formatExistent;
    }

    public void setFormatExistent(boolean formatExistent) {
        this.formatExistent = formatExistent;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.format);
        hash = 13 * hash + (this.formatExistent ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FormatWrapperBean other = (FormatWrapperBean) obj;
        if (this.formatExistent != other.formatExistent) {
            return false;
        }
        if (!this.format.equals(other.format)) {
            return false;
        }
        return true;
    }
}
