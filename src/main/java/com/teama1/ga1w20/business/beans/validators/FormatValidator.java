package com.teama1.ga1w20.business.beans.validators;

import com.teama1.ga1w20.persistence.entities.Formats;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.component.inputtext.InputText;

/**
 * Validates the format fields submitted for a duplicate format.
 *
 * @author Michael
 */
@SuppressWarnings("unchecked") // SuppressWarnings annotations only added in the Validators, which must perform casting on uncertain input received from the framework, and on classes based on extant code on github.
@FacesValidator(value = "formatValidator")
public class FormatValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object t) throws ValidatorException {
        InputText input1 = (InputText) uic.getAttributes().get("extension");
        InputText input2 = (InputText) uic.getAttributes().get("formatName");

        if (input1.getSubmittedValue() != null && input2.getSubmittedValue() != null) {
            //The following ternary operators check if the submitted field is blank or not.
            //If it is, assign it to null to make managing the validation easier in the if statement following.
            //If it's not, parse it properly.
            String extension = ((String) input1.getSubmittedValue()).isBlank()
                    ? null : ((String) input1.getSubmittedValue()).toLowerCase().replaceAll("\\s", "");
            String formatName = ((String) input2.getSubmittedValue()).isBlank()
                    ? null : ((String) input2.getSubmittedValue()).toLowerCase().replaceAll("\\s", "");
            List<Formats> formats = (List<Formats>) uic.getAttributes().get("formats");

            if (isDuplicate(extension, formatName, formats)) {
                FacesMessage msg = new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksValidate_DuplicateFormat}", String.class));
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }

    /**
     * Verifies if the current format the user wants to create already exists in
     * the database.
     *
     * @param extension
     * @param formats
     * @return whether the current format exists in the list of formats.
     */
    private boolean isDuplicate(String extension, String formatName, List<Formats> formats) {
        if (extension != null && formatName != null) {
            for (Formats f : formats) {
                if (extension.equals(f.getExtension().toLowerCase().replaceAll("\\s", "")) && formatName.equals(f.getName().toLowerCase().replaceAll("\\s", ""))) {
                    return true;
                }
            }
        }
        return false;
    }
}
