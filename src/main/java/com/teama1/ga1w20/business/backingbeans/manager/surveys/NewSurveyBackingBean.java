package com.teama1.ga1w20.business.backingbeans.manager.surveys;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Surveys;
import com.teama1.ga1w20.persistence.jpacontrollers.SurveysJpaController;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.LoggerFactory;

/**
 * Holds the values of the input fields while creating a new survey from the
 * Survey Management page.
 *
 * @author Timmy
 */
@Named("newSurveyBacking")
@RequestScoped
public class NewSurveyBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(NewSurveyBackingBean.class);

    private String question;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;

    @Inject
    private SurveysJpaController surveysController;

    public void addNewSurvey() {
        Surveys survey = new Surveys();

        survey.setQuestion(question);

        survey.setAnswer1(answer1);
        survey.setAnswer2(answer2);
        survey.setAnswer3(answer3);
        survey.setAnswer4(answer4);

        survey.setVote1(0);
        survey.setVote2(0);
        survey.setVote3(0);
        survey.setVote4(0);

        survey.setActive(false);

        try {
            surveysController.create(survey);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Success"), Messages.getString("SuccessfulAddMsg")));

        } catch (IllegalStateException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Cannot create a new survey: ", ex);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Error"), Messages.getString("AddError")));
        }
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

}
