package com.teama1.ga1w20.business.backingbeans.manager.surveys;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Timmy
 */
@Named("surveysSearchBacking")
@RequestScoped
public class SurveysSearchBackingBean implements Serializable {

    private boolean surveyId = true;
    private boolean question = true;
    private boolean answer1 = true;
    private boolean answer2 = true;
    private boolean answer3 = true;
    private boolean answer4 = true;
    private boolean vote1 = true;
    private boolean vote2 = true;
    private boolean vote3 = true;
    private boolean vote4 = true;
    private boolean active = true;

    public boolean isSurveyId() {
        return surveyId;
    }

    public void setSurveyId(boolean surveyId) {
        this.surveyId = surveyId;
    }

    public boolean isQuestion() {
        return question;
    }

    public void setQuestion(boolean question) {
        this.question = question;
    }

    public boolean isAnswer1() {
        return answer1;
    }

    public void setAnswer1(boolean answer1) {
        this.answer1 = answer1;
    }

    public boolean isAnswer2() {
        return answer2;
    }

    public void setAnswer2(boolean answer2) {
        this.answer2 = answer2;
    }

    public boolean isAnswer3() {
        return answer3;
    }

    public void setAnswer3(boolean answer3) {
        this.answer3 = answer3;
    }

    public boolean isAnswer4() {
        return answer4;
    }

    public void setAnswer4(boolean answer4) {
        this.answer4 = answer4;
    }

    public boolean isVote1() {
        return vote1;
    }

    public void setVote1(boolean vote1) {
        this.vote1 = vote1;
    }

    public boolean isVote2() {
        return vote2;
    }

    public void setVote2(boolean vote2) {
        this.vote2 = vote2;
    }

    public boolean isVote3() {
        return vote3;
    }

    public void setVote3(boolean vote3) {
        this.vote3 = vote3;
    }

    public boolean isVote4() {
        return vote4;
    }

    public void setVote4(boolean vote4) {
        this.vote4 = vote4;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
