package com.teama1.ga1w20.business.backingbeans.manager.orders;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.jpacontrollers.OrderitemsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.OrdersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.queries.beans.OrdersQueryBean;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timmy
 */
@Named("ordersBacking")
@ViewScoped
public class OrdersBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(OrdersBackingBean.class);

    private List<OrdersQueryBean> filteredOrders;

    private List<OrdersQueryBean> orderList;

    private OrdersQueryBean selectedOrder;

    @Inject
    private OrdersJpaController ordersController;
    @Inject
    private OrdersSearchBackingBean ordersSearchBean;
    @Inject
    private OrderitemsJpaController orderitemsController;

    public List<OrdersQueryBean> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrdersQueryBean> orderList) {
        this.orderList = orderList;
    }

    public OrdersQueryBean getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(OrdersQueryBean selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    @PostConstruct
    public void init() {
        orderList = new ArrayList<>();
        for (Orders order : ordersController.findOrdersEntities()) {
            List<Orderitems> orderitems = orderitemsController.getOrderitemsFromOrderIdIncludeRemoved(order.getOrderId());
            // Create the bean
            OrdersQueryBean ordersBean = new OrdersQueryBean(order, orderitems);
            // If at least one of the orderitems is not removed, set the boolean in the bean
            // and break the loop
            for (Orderitems orderitem : ordersBean.getOrderitems()) {
                if (!orderitem.getRemoved()) {
                    ordersBean.setOrderRemove(false);
                    break;
                }
            }
            orderList.add(ordersBean);
        }
    }

    public List<OrdersQueryBean> getFilteredOrders() {
        return filteredOrders;
    }

    public void setFilteredOrders(List<OrdersQueryBean> filteredOrders) {
        this.filteredOrders = filteredOrders;
    }

    public void checkAllCheckboxes() {
        ordersSearchBean.setOrderId(true);
        ordersSearchBean.setUserId(true);
        ordersSearchBean.setPurchaseDate(true);
        ordersSearchBean.setIsbn(true);
        ordersSearchBean.setPrice(true);
        ordersSearchBean.setGst(true);
        ordersSearchBean.setHst(true);
        ordersSearchBean.setPst(true);
    }

    public void uncheckAllCheckboxes() {
        ordersSearchBean.setOrderId(false);
        ordersSearchBean.setUserId(false);
        ordersSearchBean.setPurchaseDate(false);
        ordersSearchBean.setIsbn(false);
        ordersSearchBean.setPrice(false);
        ordersSearchBean.setGst(false);
        ordersSearchBean.setHst(false);
        ordersSearchBean.setPst(false);
    }

    /**
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean globalFitlerFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        OrdersQueryBean order = (OrdersQueryBean) value;

        return getCriteria(order, filterText);
    }

    private boolean getCriteria(OrdersQueryBean order, String filterText) {
        boolean orderitemId = false;
        boolean orderId = false;
        boolean userId = false;
        boolean purchaseDate = false;
        boolean isbn = false;
        boolean price = false;
        boolean gst = false;
        boolean hst = false;
        boolean pst = false;

        int filterInt = getInteger(filterText);

        if (ordersSearchBean.isOrderitemId()) {
            orderitemId = order.getOrderitemId() == filterInt;
        }
        if (ordersSearchBean.isOrderId()) {
            orderId = order.getOrderId() == filterInt;
        }
        if (ordersSearchBean.isUserId()) {
            userId = order.getUserId() == filterInt;
        }
        if (ordersSearchBean.isPurchaseDate()) {
            purchaseDate = order.getPurchaseDate().toString().contains(filterText);
        }
        if (ordersSearchBean.isGst()) {
            gst = order.getGst().compareTo(new BigDecimal(filterInt)) < 1;
        }
        if (ordersSearchBean.isHst()) {
            hst = order.getHst().compareTo(new BigDecimal(filterInt)) < 1;
        }
        if (ordersSearchBean.isPst()) {
            pst = order.getPst().compareTo(new BigDecimal(filterInt)) < 1;
        }

        return orderitemId || orderId || userId || purchaseDate || isbn || price || gst || hst || pst;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Remove or not remove every orderitem of an order.
     *
     * @param orderBean
     */
    public void switchRemovedEntireOrder(OrdersQueryBean orderBean) {
        try {
            // Set all the orderitems based on the "remove entire order" switch
            for (Orderitems orderitem : orderBean.getOrderitems()) {
                orderitem.setRemoved(orderBean.isOrderRemove());
                orderitemsController.edit(orderitem);
            }

            if (orderBean.isOrderRemove()) {
                Messages.addMessageToContext(FacesMessage.SEVERITY_INFO, "OrderEntireRemoved", null);
            } else {
                Messages.addMessageToContext(FacesMessage.SEVERITY_INFO, "OrderEntireNotRemoved", null);
            }
        } catch (IllegalOrphanException | NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Cannot remove the entire order: ", ex);
        }
    }

    /**
     * Save the changes done to Orderitems of a specific order.
     */
    public void saveOrderitemChanges() {
        try {
            // Set to true for the "remove entire order" switch
            selectedOrder.setOrderRemove(true);
            for (Orderitems orderitem : selectedOrder.getOrderitems()) {
                orderitemsController.edit(orderitem);
                // If there is at least one not removed orderitem,
                // set "remove entire order" switch to false
                if (!orderitem.getRemoved()) {
                    selectedOrder.setOrderRemove(false);
                }
            }
            Messages.addMessageToContext(FacesMessage.SEVERITY_INFO, "SuccessfulEditMsg", null);
        } catch (IllegalOrphanException | NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Cannot save the changes to the orderitems: ", ex);
            Messages.addMessageToContext(FacesMessage.SEVERITY_INFO, "EditError", null);
        }
    }

}
