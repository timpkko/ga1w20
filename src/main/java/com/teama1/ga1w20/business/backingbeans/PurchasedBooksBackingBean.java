package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.business.beans.SessionUserBean;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * PurchasedBooksBackingBean is used to display the purchased books of the user.
 *
 * @author Timmy
 */
@Named
@RequestScoped
public class PurchasedBooksBackingBean implements Serializable {

    @Inject
    private BooksJpaController booksController;

    @Inject
    private SessionUserBean sessionBean;

    private List<Books> purchasedBooks;

    private Formats selectedFormat;

    @PostConstruct
    public void init() {
        this.purchasedBooks = booksController.getBooksBoughtByUser(sessionBean.getUser());
    }

    public List<Books> getPurchasedBooks() {
        return this.purchasedBooks;
    }

    public void setPurchasedBooks(List<Books> purchasedBooks) {
        this.purchasedBooks = purchasedBooks;
    }

    public Formats getSelectedFormat() {
        return selectedFormat;
    }

    public void setSelectedFormat(Formats selectedFormat) {
        this.selectedFormat = selectedFormat;
    }
}
