package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.beans.CookieBean;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ClientFrontDoorBackingBean is used to display the recently added books and
 * the books that are on special, on the client's home page.
 *
 * @author Timmy
 */
@Named("clientFrontDoorBacking")
@SessionScoped
public class ClientFrontDoorBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(ClientFrontDoorBackingBean.class);

    @Inject
    private BooksJpaController booksController;

    @Inject
    private CookieBean cookieBean;

    private boolean cookiePresent;

    private boolean hideCookieMsg;

    public boolean isHideCookieMsg() {
        return hideCookieMsg;
    }

    public void setHideCookieMsg(boolean hideCookieMsg) {
        this.hideCookieMsg = hideCookieMsg;
    }

    public void doHideCookieMsg() {
        setHideCookieMsg(true);
    }

    public boolean isCookiePresent() {
        cookiePresent = !cookieBean.getCookie(Constants.SUPPORTED_COOKIE_NAME).isBlank();
        return cookiePresent;
    }

    /**
     * Retrieves books from the same genre based on the genre of the last
     * visited book page.
     *
     * @return Books from a genre, limited to a constant number
     */
    public List<Books> getBooksFromLastGenre() {
        if (!cookiePresent) {
            return new ArrayList<>();
        }

        List<Books> booksFromLastGenre = booksController.getBooksByGenreHint(cookieBean.getCookie(Constants.GENRE_COOKIE_NAME));

        int booksListSize = booksFromLastGenre.size();

        // Cookie may be altered to have a non-existing genre as the value; skip this method
        if (booksListSize == 0) {
            return new ArrayList<>();
        }

        Collections.shuffle(booksFromLastGenre);

        // Limit the number of books by the constant or by the list's size if not greater than
        int maxBooksSize = booksListSize > Constants.BOOK_LIST_MAX_SIZE
                ? Constants.BOOK_LIST_MAX_SIZE : booksListSize;

        return booksFromLastGenre.subList(0, maxBooksSize);
    }

    /**
     * Retrieves three recently added books from the database.
     *
     * @return Three recently added books in a List
     */
    public List<Books> getRecentlyAddedBooks() {
        List<Books> recentlyAddedBooks = booksController.getMostRecentlyAdded();
        return recentlyAddedBooks;
    }

    /**
     * Retrieves all books in sale from the database.
     *
     * @return All books in sale from the database.
     */
    public List<Books> getSpecialsBooks() {
        List<Books> specialBooks = booksController.getBooksOnSale();
        Collections.shuffle(specialBooks);

        // Limit number of books to constant
        int maxSize = specialBooks.size();
        if (maxSize > Constants.BOOK_LIST_MAX_SIZE) {
            maxSize = Constants.BOOK_LIST_MAX_SIZE;
        }

        return specialBooks.subList(0, maxSize);
    }

}
