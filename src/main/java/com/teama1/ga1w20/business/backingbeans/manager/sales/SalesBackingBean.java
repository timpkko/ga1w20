package com.teama1.ga1w20.business.backingbeans.manager.sales;

import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.queries.ReportQueries;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Michael
 */
@Named("salesBacking")
@RequestScoped
public class SalesBackingBean implements Serializable {

    @Inject
    private SalesSearchBackingBean salesBean;
    @Inject
    private ReportQueries reports;

    private List<Books> filteredBooks;

    public List<Books> getFilteredBooks() {
        return filteredBooks;
    }

    public void setFilteredBooks(List<Books> filteredBooks) {
        this.filteredBooks = filteredBooks;
    }

    public BigDecimal getTotalSalesOfABook(Long isbn) {
        return reports.getTotalSalesOfABook(isbn);
    }

    public void checkAllCheckboxes() {
        salesBean.setIsbn(true);
        salesBean.setTitle(true);
        salesBean.setListPrice(true);
        salesBean.setSalePrice(true);
        salesBean.setWholesalePrice(true);
        salesBean.setRemoved(true);
    }

    public void uncheckAllCheckboxes() {
        salesBean.setIsbn(false);
        salesBean.setTitle(false);
        salesBean.setListPrice(false);
        salesBean.setSalePrice(false);
        salesBean.setWholesalePrice(false);
        salesBean.setRemoved(false);
    }

    /**
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean bookGlobalFilterFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        Books book = (Books) value;
        return getCriteria(book, filterText);
    }

    private boolean getCriteria(Books book, String filterText) {
        boolean isbn = false, title = false, listPrice = false, salePrice = false, wholesalePrice = false,
                age = false, minAge = false, maxAge = false, removed = false, totalSales = false;
        BigDecimal filterNum = new BigDecimal(getInteger(filterText));

        if (salesBean.isIsbn()) {
            isbn = book.getIsbn().toString().contains(filterText);
        }
        if (salesBean.isTitle()) {
            title = book.getTitle().toLowerCase().contains(filterText);
        }
        if (salesBean.isRemoved()) {
            removed = (book.getRemoved() && "removed".contains(filterText));
        }
        if (filterNum.compareTo(new BigDecimal(-1)) != 0) {
            if (salesBean.isListPrice()) {
                listPrice = book.getListPrice().compareTo(filterNum) < 1;
            }
            if (salesBean.isSalePrice()) {
                salePrice = book.getSalePrice().compareTo(filterNum) < 1;
            }
            if (salesBean.isWholesalePrice()) {
                wholesalePrice = book.getWholesalePrice().compareTo(filterNum) < 1;
            }
        }

        return isbn || title || listPrice || salePrice || wholesalePrice
                || age || minAge || maxAge || removed || totalSales;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }
}
