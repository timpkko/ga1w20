package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.business.beans.NavigationUtilsBean;
import com.teama1.ga1w20.business.beans.SessionUserBean;
import com.teama1.ga1w20.business.utils.HashUtils;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Credentials;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.CredentialsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for the login page.
 *
 * @author Camillia E.
 */
@Named("loginBackingBean")
@RequestScoped
public class LoginBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(LoginBackingBean.class);

    @Inject
    private CredentialsJpaController credentialsJpaController;

    @Inject
    private UsersJpaController usersJpaController;

    @Inject
    private SessionUserBean sessionUserBean;

    @Inject
    private NavigationUtilsBean navigationBean;

    @Transient
    private HashUtils hashUtils;

    ////////////////////////////////////////////////////////////////////////////
    //UI input fields
    private String email;

    private String password;

    ////////////////////////////////////////////////////////////////////////////
    //Login 
    ////////////////////////////////////////////////////////////////////////////
    public String login() {
        if (email == null) {
            LOG.error("Field email must be non null for register() method.");
            return forbidLogin();
        }
        if (password == null) {
            LOG.error("credentials must be non null for register() method.");
            return forbidLogin();
        }

        //Make sure both fields are inputed
        if (email.isBlank()) {
            LOG.error("Received value for inputed email was blank");
            return forbidLogin();
        }
        if (password.isBlank()) {
            LOG.error("Reveived value for inputed password is blank");
            return forbidLogin();
        }

        String strippedEmail = email.strip();
        String strippedPassword = password.strip();

        //Get user
        Users user;
        try {
            user = usersJpaController.getUserByEmail(strippedEmail);
        } //If user doesn't exist
        catch (NoResultException ex) {
            return forbidLogin();
        } catch (NonUniqueResultException ex) {
            LOG.error("Two users have been returned for inputed email ("
                    + email + ") Unable to proceed with Login.");
            return forbidLogin();
        }

        //If user doesn't exist
        if (user == null) {
            return forbidLogin();
        }

        //Find credentials related to users
        Credentials userCredentials = credentialsJpaController.findCredentials(user.getUserId());
        byte[] databaseHash = userCredentials.getHash();
        String salt = userCredentials.getSalt();

        if (hashUtils == null) {
            hashUtils = new HashUtils();
        }

        //Hash inputed password
        byte[] hashedInputPassword;
        try {
            hashedInputPassword = hashUtils.hashPassword(strippedPassword, salt);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            LOG.error("Error when trying to hash inputed password: " + ex.getMessage());
            return forbidLogin();
        }

        //Compare hashes
        if (hashUtils.compareHashes(databaseHash, hashedInputPassword)) {
            return authorizeLogin(user, userCredentials);
        } else {
            return forbidLogin();
        }
    }

    private String authorizeLogin(Users user, Credentials credentials) {
        //Set logged in user
        sessionUserBean.setLoggedInUser(user, credentials);

        //Redirect to Finalization page if the user needed to login before
        //finalizing their order
        if (navigationBean.shouldRedirectToFinalization()) {
            return "toFinalization";
        }

        //Redirect to Checkout page if the user needed to login before
        //finalizing their order
        if (navigationBean.shouldRedirectToCheckout()) {
            return "toCheckout";
        }

        //Redirect to index page
        if (sessionUserBean.isUserAManager()) {
            return "toManagerIndex";
        } else {
            return "toUserIndex";
        }
    }

    private String forbidLogin() {
        Messages.addMessageToContext(FacesMessage.SEVERITY_ERROR, "LoginError", null);
        return "toLogin";
    }

    ////////////////////////////////////////////////////////////////////////////
    //Logout
    ////////////////////////////////////////////////////////////////////////////
    public String logout() {
        //Clear logged in user
        sessionUserBean.clearLoggedInUser();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        // Set growl logout message
        Messages.addMessageToContext(FacesMessage.SEVERITY_INFO,
                "LogoutSuccessful", null);
        Messages.addMessageToContext(FacesMessage.SEVERITY_INFO,
                "LogoutSuccessful", "growlMsg");
        return "toUserIndex";
    }

    ////////////////////////////////////////////////////////////////////////////
    // Getters & Setters
    ////////////////////////////////////////////////////////////////////////////
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
