package com.teama1.ga1w20.business.backingbeans.manager.reviews;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.jpacontrollers.ReviewsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 * This backing bean is used to manage the reviews from the database. This is
 * also used in the Reviews Management page.
 *
 * @author Timmy
 */
@Named("reviewsManagementBacking")
@ViewScoped
public class ReviewsManagementBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ReviewsManagementBackingBean.class);

    @Inject
    private ReviewsJpaController reviewsController;

    @Inject
    private ReviewsSearchBackingBean reviewsSearch;

    private List<Reviews> allReviews;

    private List<Reviews> filteredAllReviews;

    private List<Reviews> pendingReviews;

    private List<Reviews> filteredPendingReviews;

    private Reviews selectedReview;

    public Reviews getSelectedReview() {
        return selectedReview;
    }

    public void setSelectedReview(Reviews selectedReview) {
        this.selectedReview = selectedReview;
    }

    public List<Reviews> getFilteredAllReviews() {
        return filteredAllReviews;
    }

    public void setFilteredAllReviews(List<Reviews> filteredAllReviews) {
        this.filteredAllReviews = filteredAllReviews;
    }

    public List<Reviews> getFilteredPendingReviews() {
        return filteredPendingReviews;
    }

    public void setFilteredPendingReviews(List<Reviews> filteredPendingReviews) {
        this.filteredPendingReviews = filteredPendingReviews;
    }

    public List<Reviews> getAllReviews() {
        return allReviews;
    }

    public List<Reviews> getPendingReviews() {
        return pendingReviews;
    }

    /**
     * Updates the lists of all reviews and pending reviews. This will be called
     * during the initialization, and is useful for updating the tables after a
     * change was made.
     */
    @PostConstruct
    public void updateTables() {
        allReviews = reviewsController.findReviewsEntities();
        pendingReviews = reviewsController.getModerationQueue();
    }

    public void checkAllCheckboxes() {
        reviewsSearch.setIsbn(true);
        reviewsSearch.setPostdate(true);
        reviewsSearch.setUserid(true);
        reviewsSearch.setRating(true);
        reviewsSearch.setText(true);
    }

    public void uncheckAllCheckboxes() {
        reviewsSearch.setIsbn(false);
        reviewsSearch.setPostdate(false);
        reviewsSearch.setUserid(false);
        reviewsSearch.setRating(false);
        reviewsSearch.setText(false);
    }

    /**
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean globalFitlerFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        Reviews review = (Reviews) value;

        return getCriteria(review, filterText);
    }

    /**
     * This method checks if the text from the search matches with any
     * information about the record. If there is at least one match, the record
     * is then displayed in the table. It takes account of what columns are
     * checked and unchecked, a useful ability to narrow the search results.
     *
     * @param ad
     * @param filterText
     * @return
     */
    private boolean getCriteria(Reviews review, String filterText) {
        boolean reviewId = false;
        boolean isbn = false;
        boolean postdate = false;
        boolean userid = false;
        boolean rating = false;
        boolean text = false;

        if (reviewsSearch.isIsbn()) {
            isbn = review.getIsbn().getIsbn().toString().contains(filterText);
        }
        if (reviewsSearch.isPostdate()) {
            postdate = review.getPostdate().toString().toLowerCase().contains(filterText);
        }
        if (reviewsSearch.isUserid()) {
            userid = review.getUserId().getUserId().toString().contains(filterText);
        }
        if (reviewsSearch.isRating()) {
            rating = review.getRating() == getInteger(filterText);
        }
        if (reviewsSearch.isText()) {
            text = review.getText().toLowerCase().contains(filterText);
        }

        return reviewId || isbn || postdate || userid || rating || text;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Changes the state of the approval of a review.
     */
    public void changeApprovedOfReview() {
        selectedReview.setApproved(!selectedReview.getApproved());

        FacesMessage msg = new FacesMessage();
        try {
            reviewsController.edit(selectedReview);
            msg.setSeverity(FacesMessage.SEVERITY_INFO);
            msg.setSummary(Messages.getString("Success"));
            msg.setDetail(Messages.getString("ReviewApprovedSuccess"));
            updateTables();
        } catch (RollbackFailureException ex) {
            LOG.error("Cannot rollback while editing the review:", ex);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary(Messages.getString("Error"));
            msg.setDetail(Messages.getString("EditError"));
        } catch (Exception ex) {
            LOG.error("Cannot edit the review because an exception was thrown: ", ex);
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            msg.setSummary(Messages.getString("Error"));
            msg.setDetail(Messages.getString("EditError"));
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
