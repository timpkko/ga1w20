package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * NavbarBackingBean has methods to redirect the user to what they clicked on
 * the navbar.
 *
 * @author Timmy, Camilla E.
 */
@Named
@SessionScoped
public class NavbarBackingBean implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(NavbarBackingBean.class);

    List<Genres> genres;

    @Inject
    GenresJpaController genresJpaController;

    @PostConstruct
    public void init() {
        this.genres = genresJpaController.findGenresEntities();
    }

    public List<Genres> getGenres() {
        return genres;
    }

    public void setGenres(List<Genres> genres) {
        this.genres = genres;
    }

    public String goToGenrePage(int genreId) {
        LOG.debug("going to genre page with id: " + genreId);
        return "/genre.xhtml?genreId=" + genreId + "&faces-redirect=true";
    }
}
