package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.entities.Publishers;
import com.teama1.ga1w20.persistence.jpacontrollers.PublishersJpaController;
import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.AllSalesQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TotalSalesQueryBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named("salesByPublisherBacking")
@RequestScoped
public class SalesByPublisherBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SalesByClientBackingBean.class);

    private List<AllSalesQueryBean> allSalesByPublisher;
    private TotalSalesQueryBean totalSalesByPublisher;

    private List<Publishers> publishers;
    private Publishers selectedPublisher;

    @Inject
    private ReportQueries reports;
    @Inject
    private PublishersJpaController publishersController;
    @Inject
    private DateRangeBackingBean dateRangeBean;

    @PostConstruct
    public void init() {
        this.publishers = publishersController.findPublishersEntities();
        Collections.sort(publishers, (Publishers p1, Publishers p2) -> {
            return p1.getName().compareToIgnoreCase(p2.getName());
        });
        this.selectedPublisher = this.publishers.get(0);

        updateTable();
    }

    public void updateTable() {
        if (dateRangeBean.getStartDate() == null || dateRangeBean.getEndDate() == null) {
            return;
        }

        try {
            this.totalSalesByPublisher = reports.getTotalSalesByPublisher(selectedPublisher.getPublisherId(),
                    dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
            this.allSalesByPublisher = reports.getAllSalesByPublisher(selectedPublisher.getPublisherId(),
                    dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
        } catch (PersistenceException e) {
            LOG.error("No records found", e);
            this.allSalesByPublisher = new ArrayList<>();
        }
    }

    public List<AllSalesQueryBean> getAllSalesByPublisher() {
        return allSalesByPublisher;
    }

    public TotalSalesQueryBean getTotalSalesByPublisher() {
        return totalSalesByPublisher;
    }

    public List<Publishers> getPublishers() {
        return this.publishers;
    }

    public Publishers getSelectedPublisher() {
        return this.selectedPublisher;
    }

    public void setSelectedPublisher(Publishers publisher) {
        this.selectedPublisher = publisher;
    }
}
