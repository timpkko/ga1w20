package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.SimpleBookQueryBean;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Michael
 */
@Named("stockBacking")
@RequestScoped
public class StockBackingBean implements Serializable {

    private List<SimpleBookQueryBean> books;

    @Inject
    private ReportQueries reports;

    @PostConstruct
    public void init() {
        this.books = reports.getStock();
    }

    public List<SimpleBookQueryBean> getBooks() {
        return books;
    }
}
