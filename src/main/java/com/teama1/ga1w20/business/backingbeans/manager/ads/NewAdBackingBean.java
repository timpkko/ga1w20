package com.teama1.ga1w20.business.backingbeans.manager.ads;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.business.utils.UploadImageUtils;
import com.teama1.ga1w20.persistence.entities.Banners;
import com.teama1.ga1w20.persistence.jpacontrollers.BannersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.LoggerFactory;

/**
 * Holds the values of the input fields while creating a new ad from the Ads
 * Management page.
 *
 * @author Timmy
 */
@Named("newAdBacking")
@RequestScoped
public class NewAdBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(NewAdBackingBean.class);

    private String url;

    private UploadedFile image;

    @Inject
    private BannersJpaController bannersController;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UploadedFile getImage() {
        return image;
    }

    public void setImage(UploadedFile image) {
        this.image = image;
    }

    public void addNewAd() {
        Banners ad = new Banners();
        UploadImageUtils imageUtil = new UploadImageUtils(image);

        ad.setUrl(url);
        ad.setActive(false);

        try {
            bannersController.create(ad);
            imageUtil.saveToDisk(Constants.BANNERS_PATH, ad.getAdId().toString());
            Messages.addMessageToContext(FacesMessage.SEVERITY_INFO, "Success", "SuccessfulAdMsg");
        } catch (IllegalStateException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Cannot create a new banner: ", ex);
            Messages.addMessageToContext(FacesMessage.SEVERITY_ERROR, "Error", "AddError");
            revertChanges(ad);
        } catch (IOException ex) {
            LOG.error("Cannot upload the image: " + ex);
            Messages.addMessageToContext(FacesMessage.SEVERITY_ERROR, "Error", "ImageUploadError");
            revertChanges(ad);
        }
    }

    private void revertChanges(Banners ad) {
        try {
            bannersController.destroy(ad.getAdId());
        } catch (IllegalOrphanException | NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Cannot delete the ad: ", ex);
        }
    }
}
