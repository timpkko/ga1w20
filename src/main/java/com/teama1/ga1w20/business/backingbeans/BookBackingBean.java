package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.beans.CartBean;
import com.teama1.ga1w20.business.beans.CookieBean;
import com.teama1.ga1w20.business.beans.SessionUserBean;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.entities.Publishers;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.PublishersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.ReviewsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * BookBackingBean is a backing bean to display information of a particular book
 * for the book page.
 *
 * @author Timmy
 */
@Named
@ViewScoped
public class BookBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(BookBackingBean.class);

    @Inject
    private BooksJpaController bookController;
    @Inject
    private AuthorsJpaController authorController;
    @Inject
    private ReviewsJpaController reviewsController;
    @Inject
    private PublishersJpaController publishersController;
    @Inject
    private GenresJpaController genresController;
    @Inject
    private FormatsJpaController formatsController;
    @Inject
    private CartBean cartBean;
    @Inject
    private CookieBean cookieBean;

    private Long isbn;

    private Books book;

    private List<Reviews> reviews;

    private boolean hasPurchased;

    private boolean addedToCart;

    @Inject
    private SessionUserBean sessionBean;

    @Inject
    private CheckoutBackingBean checkoutBean;

    /**
     * Checks if the user had purchased the book, and returns a boolean.
     *
     * @return State of the purchase
     */
    public boolean isHasPurchased() {
        // User must be logged in to know whether they purchased the book
        if (!sessionBean.isUserLoggedIn()) {
            this.hasPurchased = false;
        } else {
            List<Books> purchasedBooks = bookController.getBooksBoughtByUser(sessionBean.getUser());
            // Check if the user has bought the book
            if (purchasedBooks.contains(book)) {
                this.hasPurchased = true;
            }
        }

        return this.hasPurchased;
    }

    /**
     * Checks if the user had already added the book to their cart, and returns
     * a boolean.
     *
     * @return
     */
    public boolean isAddedToCart() {
        // Check if the book is added in the cart
        if (checkoutBean.getBooksInCart().contains(book)) {
            this.addedToCart = true;
        } else {
            this.addedToCart = false;
        }

        return this.addedToCart;
    }

    public Long getIsbn() {
        return this.isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public List<Authors> getAuthors() {
        return authorController.getAuthorsByBook(isbn);
    }

    /**
     * Returns a one-line comma-separated string of authors.
     *
     * @return
     */
    public String getFormattedAuthors() {
        List<Authors> authors = getAuthors();
        StringBuilder formattedAuthors = new StringBuilder();
        for (int i = 0; i < authors.size(); i++) {
            formattedAuthors.append(authors.get(i).getFirstname());
            formattedAuthors.append(" ");
            if (authors.get(i).getLastname() != null) {
                formattedAuthors.append(authors.get(i).getLastname());
            }
            if (i < authors.size() - 1) {
                formattedAuthors.append(", ");
            }
        }
        return formattedAuthors.toString();
    }

    public Books getBook() {
        if (isbn == null) {
            redirect404();
        }
        if (book == null) {
            try {
                this.book = bookController.getBookByIsbn(isbn);

                // The book page will not exist if the book is removed from the database
                if (this.book.getRemoved()) {
                    redirect404();
                }

                //Since the user viewed this book, we create a Cookie with the Genre of the book
                cookieBean.addCookie(Constants.GENRE_COOKIE_NAME, getGenres().get(0).getName());
            } catch (NoResultException e) {
                LOG.error("Book does not exist; ISBN: ", isbn);
                redirect404();
            }
        }

        return this.book;
    }

    /**
     * Calling this method will redirect the user to a 404 error page. This is
     * useful when the user tries to enter an ISBN that does not exist in the
     * database in the GET request.
     */
    private void redirect404() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext externalContext = context.getExternalContext();
            externalContext.setResponseStatus(HttpServletResponse.SC_NOT_FOUND);
            externalContext.dispatch(Constants.ERROR_PAGE_URL);
            context.responseComplete();
        } catch (IOException ex) {
            LOG.error("Cannot redirect to 404 page ", ex);
        }
    }

    public List<Reviews> getReviews() {
        if (reviews == null) {
            reviews = reviewsController.getApprovedReviewsByBook(isbn);
        }
        return reviews;
    }

    /**
     * Returns an average of ratings of the book's reviews.
     *
     * @return
     */
    public int getAverageRatings() {
        // Avoids division by 0
        if (reviews.isEmpty()) {
            return 0;
        }

        int avgRatings = 0;
        for (Reviews review : reviews) {
            avgRatings += review.getRating();
        }

        return avgRatings / reviews.size();
    }

    /**
     * This method might return null if no publisher is associated to this book
     * object.
     *
     * @return
     * @author Camillia
     */
    public Publishers getPublisher() {
        Publishers p = null;
        try {
            p = publishersController.getPublisherByBook(isbn);
        } catch (NonexistentEntityException ex) {
            LOG.warn("No publisher object is associated to the book with isbn " + isbn);
        }
        return p;
    }

    public String getCover() {
        return String.format(Constants.BOOK_COVERS_SHORT_PATH.concat("%d"), isbn).concat(Constants.IMAGES_EXTENSION);
    }

    public List<Genres> getGenres() {
        return genresController.getGenresByBook(isbn);
    }

    public List<Formats> getFormats() {
        return formatsController.getFormatsByBook(isbn);
    }

    /**
     * Retrieves books from the same author and/or from the same genre.
     *
     * @return
     */
    public List<Books> getRecommendedBooks() {
        Random rand = new Random();
        Set<Books> recommendedBooks = new HashSet<>();

        // Get three books from the same author(s)
        for (Authors author : getAuthors()) {
            for (Books bookOfAuthor : bookController.getBooksByAuthor(author)) {
                if (!Objects.equals(bookOfAuthor.getIsbn(), this.isbn)) {
                    recommendedBooks.add(bookOfAuthor);
                    if (recommendedBooks.size() == 3) {
                        break;
                    }
                }
            }
        }

        int countForGenres = 0;
        for (Genres genre : getGenres()) {
            List<Books> booksByGenre = bookController.getBooksByGenre(genre);
            int countOfBooks = booksByGenre.size();
            while (countForGenres < countOfBooks - 1 && countForGenres < 3) {
                Books selectedBook = booksByGenre.get(rand.nextInt(countOfBooks));
                // Avoid adding the book that is the same as the book page
                if (!Objects.equals(selectedBook.getIsbn(), this.isbn)) {
                    // Add the selected book to the set
                    // Only update the count if the book is new
                    if (recommendedBooks.add(selectedBook)) {
                        countForGenres++;
                    }
                }
            }
        }

        return new ArrayList<>(recommendedBooks);
    }
}
