package com.teama1.ga1w20.business.backingbeans.manager.reports;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author phant
 */
@Named("dateRangeBacking")
@RequestScoped
public class DateRangeBackingBean implements Serializable {

    private final Date minDate = (new Calendar.Builder().setDate(2000, 00, 01).build()).getTime();
    private final Date maxDate = new Date();

    private Date startDate;
    private Date endDate;

    @PostConstruct
    public void init() {
        // Starting date is 2000/01/01
        this.startDate = (Date) minDate.clone();

        // Get today's date as the end date is null
        this.endDate = (Date) maxDate.clone();
    }

    public Date getMinDate() {
        return this.minDate;
    }

    public Date getMaxDate() {
        return this.maxDate;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
