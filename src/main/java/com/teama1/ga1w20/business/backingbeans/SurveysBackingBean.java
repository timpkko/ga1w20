package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.persistence.entities.Surveys;
import com.teama1.ga1w20.persistence.jpacontrollers.SurveysJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.LoggerFactory;

/**
 * SurveysBackingBean is used to retrieve and store information for the survey
 * form in the client front door page.
 *
 * @author Timmy
 */
@Named
@RequestScoped
public class SurveysBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SurveysBackingBean.class);

    @Inject
    private SurveysJpaController surveysController;

    private String selectedAnswer;

    private Surveys activeSurvey;

    private Map<String, String> activeAnswers;

    private String totalVotes;

    // State whether user has already answered
    private boolean answered;

    @PostConstruct
    public void init() {
        List<Surveys> activeSurveys = surveysController.getActiveSurveys();
        if (!activeSurveys.isEmpty()) {
            Collections.shuffle(activeSurveys);
            activeSurvey = activeSurveys.get(0);
            this.activeAnswers = new LinkedHashMap<>();
            populateActiveAnswers();
        }
    }

    public String getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public Surveys getActiveSurvey() {
        if (activeSurvey == null) {
            setActiveSurvey();
        }
        return activeSurvey;
    }

    /**
     * Shuffle list to have a different survey everytime.
     *
     * @author Camillia
     */
    private void setActiveSurvey() {
        List<Surveys> activeSurveys = surveysController.getActiveSurveys();
        if (!activeSurveys.isEmpty()) {
            Collections.shuffle(activeSurveys);
            activeSurvey = activeSurveys.get(0);
        }
    }

    public void setActiveSurvey(Surveys activeSurvey) {
        this.activeSurvey = activeSurvey;
    }

    public boolean isAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }

    public Map<String, String> getActiveAnswers() {
        if (activeSurvey != null) {
            populateActiveAnswers();
        }
        return this.activeAnswers;
    }

    public String getTotalVotes() {
        this.totalVotes = "0";
        if (activeSurvey != null) {
            this.totalVotes = Integer.toString(activeSurvey.getVote1()
                    + activeSurvey.getVote2()
                    + activeSurvey.getVote3()
                    + activeSurvey.getVote4());
        }
        return this.totalVotes;
    }

    /**
     * Adds another count to the vote of the option the user has selected. This
     * is called when the user clicks on the Submit button.
     */
    public void completeSurvey() {
        // Check which option was chosen
        switch (selectedAnswer) {
            case "ans1": {
                int currentVote = activeSurvey.getVote1() + 1;
                activeSurvey.setVote1(currentVote);
                break;
            }
            case "ans2": {
                int currentVote = activeSurvey.getVote2() + 1;
                activeSurvey.setVote2(currentVote);
                break;
            }
            case "ans3": {
                int currentVote = activeSurvey.getVote3() + 1;
                activeSurvey.setVote3(currentVote);
                break;
            }
            case "ans4": {
                int currentVote = activeSurvey.getVote4() + 1;
                activeSurvey.setVote4(currentVote);
                break;
            }
            default:
                break;
        }
        // Persist to database
        try {
            surveysController.edit(activeSurvey);
            // Indicates that the survey has been completed
            answered = true;
        } catch (NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Failure to rollback when editing the survey record", ex);
        }
    }

    /**
     * Fill the activeAnswers map list with the itemValue name and the text of
     * the answer. The key corresponds to the itemValue name for the radio
     * buttons. The value corresponds to the answer itself.
     *
     * Note: There is the assumption that the third and forth options can be
     * empty, so they will not be included in the list. But the first and second
     * options must exist due to the nature of a survey.
     */
    private void populateActiveAnswers() {
        this.activeAnswers.clear();
        if (activeSurvey != null) {
            this.activeAnswers.put("ans1", activeSurvey.getAnswer1());
            this.activeAnswers.put("ans2", activeSurvey.getAnswer2());

            if (activeSurvey.getAnswer3() == null) {
                return;
            }
            this.activeAnswers.put("ans3", activeSurvey.getAnswer3());

            if (activeSurvey.getAnswer4() == null) {
                return;
            }
            this.activeAnswers.put("ans4", activeSurvey.getAnswer4());
        }

    }

}
