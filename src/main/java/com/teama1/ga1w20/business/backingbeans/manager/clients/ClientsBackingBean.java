package com.teama1.ga1w20.business.backingbeans.manager.clients;

import com.teama1.ga1w20.business.beans.validators.EmailValidator;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.PrimeFaces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ClientsBackingBean is used to manage the clients. With it, it can retrieve
 * the users from the database and edit their registration information.
 *
 * @author Timmy (+ Camillia)
 */
@Named("clientsBacking")
@SessionScoped
public class ClientsBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(ClientsBackingBean.class);

    @Inject
    private UsersJpaController usersController;
    @Inject
    private ClientsSearchBackingBean clientSearchBean;
    @Inject
    private EmailValidator emailValidator;

    private List<Users> clients;
    private List<Users> filteredClients;

    private Users selectedUser;

    @PostConstruct
    public void init() {
        this.clients = usersController.findUsersEntities();
        //To initialize the associated credentials of each user
        this.clients.forEach((client) -> {
            client.getCredentials();
        });
    }

    public Users getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(Users selectedUser) {
        this.selectedUser = selectedUser;
    }

    public List<Users> getClients() {
        return this.clients;
    }

    public List<Users> getFilteredClients() {
        return this.filteredClients;
    }

    public void setFilteredClients(List<Users> filteredClients) {
        this.filteredClients = filteredClients;
    }

    public void checkAllCheckboxes() {
        clientSearchBean.setAddress(true);
        clientSearchBean.setCellphone(true);
        clientSearchBean.setCity(true);
        clientSearchBean.setCompany(true);
        clientSearchBean.setCountry(true);
        clientSearchBean.setEmail(true);
        clientSearchBean.setFirstName(true);
        clientSearchBean.setHomephone(true);
        clientSearchBean.setLastName(true);
        clientSearchBean.setManager(true);
        clientSearchBean.setPostalCode(true);
        clientSearchBean.setProvince(true);
        clientSearchBean.setSecondAddress(true);
        clientSearchBean.setTitle(true);
        clientSearchBean.setUserId(true);
    }

    public void uncheckAllCheckboxes() {
        clientSearchBean.setAddress(false);
        clientSearchBean.setCellphone(false);
        clientSearchBean.setCity(false);
        clientSearchBean.setCompany(false);
        clientSearchBean.setCountry(false);
        clientSearchBean.setEmail(false);
        clientSearchBean.setFirstName(false);
        clientSearchBean.setHomephone(false);
        clientSearchBean.setLastName(false);
        clientSearchBean.setManager(false);
        clientSearchBean.setPostalCode(false);
        clientSearchBean.setProvince(false);
        clientSearchBean.setSecondAddress(false);
        clientSearchBean.setTitle(false);
        clientSearchBean.setUserId(false);
    }

    /**
     * Resets the dialog form inputs for the next client.
     *
     * @author Michael
     * @param formId
     */
    public void resetDialogForm(String formId) {
        PrimeFaces.current().resetInputs(formId);
    }

    /**
     * Make sure the email entered while editing doesn't belong to another user
     * than the one being currently-edited. If it's not, go to standard email
     * validator.
     *
     * @author Camillia E.
     * @param context
     * @param component
     * @param value
     */
    public void validateEmail(FacesContext context, UIComponent component,
            Object value) {
        Integer userId = (Integer) component.getAttributes().get("userId");
        if (value != null && !((String) value).isBlank() && userId != null) {
            emailValidator.validate((String) value, userId);
        }
    }

    /**
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean globalFitlerFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        Users client = (Users) value;

        return getCriteria(client, filterText);
    }

    private boolean getCriteria(Users client, String filterText) {
        boolean userId = false, lastName = false, firstName = false,
                email = false, country = false, city = false, address = false;
        boolean secondAddress = false, postalCode = false, province = false;
        boolean homephone = false, cellphone = false, company = false,
                title = false, manager = false;

        if (clientSearchBean.isUserId()) {
            userId = client.getUserId() == getInteger(filterText);
        }
        if (clientSearchBean.isLastName()) {
            lastName = client.getLastname().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isFirstName()) {
            firstName = client.getFirstname().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isEmail()) {
            email = client.getEmail().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isCountry()) {
            country = client.getCountry().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isCity()) {
            city = client.getCity().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isAddress()) {
            address = client.getAddress1().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isSecondAddress()) {
            if (client.getAddress2() != null) {
                secondAddress = client.getAddress2().toLowerCase().contains(filterText);
            }
        }
        if (clientSearchBean.isPostalCode()) {
            postalCode = client.getPostalcode().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isProvince()) {
            province = client.getProvince().toLowerCase().contains(filterText);
        }
        if (clientSearchBean.isHomephone()) {
            if (client.getHomephone() != null) {
                homephone = client.getHomephone().toLowerCase().contains(filterText);
            }
        }
        if (clientSearchBean.isCellphone()) {
            if (client.getCellphone() != null) {
                cellphone = client.getCellphone().toLowerCase().contains(filterText);
            }
        }
        if (clientSearchBean.isCompany()) {
            if (client.getCompanyname() != null) {
                company = client.getCompanyname().toLowerCase().contains(filterText);
            }
        }
        if (clientSearchBean.isTitle()) {
            if (client.getTitle() != null) {
                title = client.getTitle().toLowerCase().contains(filterText);
            }
        }
        if (clientSearchBean.isManager()) {
            if (client.getCredentials() != null) {
                manager = client.getCredentials().getManager() && "manager".contains(filterText);
            }
        }

        return userId || lastName || firstName || email || country || city
                || address || secondAddress || postalCode || province
                || homephone || cellphone || company || title || manager;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Persists the changes of a user's registration information to the
     * database.
     *
     */
    public void saveChanges() {
        FacesMessage msg;
        try {
            usersController.edit(selectedUser);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    Messages.getString("Success"),
                    Messages.getString("SuccessfulEditMsg"));
        } catch (NonexistentEntityException | IllegalOrphanException
                | SecurityException | HeuristicMixedException
                | HeuristicRollbackException | NotSupportedException
                | RollbackException | SystemException ex) {
            LOG.error(selectedUser.getUserId() + " cannot be edited", ex);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    Messages.getString("Error"),
                    Messages.getString("EditError"));
        }

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Get the value of total purchases of a user.
     *
     * @param user
     * @return
     */
    public BigDecimal getClientTotalPurchases(Users user) {
        try {
            return usersController.getTotalPurchasesByUser(user.getUserId());
        } catch (NonexistentEntityException | NoResultException | NonUniqueResultException ex) {
            LOG.warn("Cannot get total purchases of" + user.getUserId() + ":" + ex);
            return BigDecimal.ZERO;
        }
    }

}
