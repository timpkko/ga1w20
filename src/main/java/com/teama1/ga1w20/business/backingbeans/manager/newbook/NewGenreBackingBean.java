package com.teama1.ga1w20.business.backingbeans.manager.newbook;

import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This backing bean serves to create a new genre.
 *
 * @genre Michael
 */
@Named("newGenreBacking")
@ViewScoped
public class NewGenreBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(NewGenreBackingBean.class);

    @Inject
    private GenresJpaController genresController;

    private List<Genres> allGenres;
    private Genres genre;

    @PostConstruct
    public void init() {
        allGenres = genresController.findGenresEntities();
        genre = new Genres();
    }

    public void createGenre() {
        try {
            genre.setBookgenreList(null);
            genresController.create(genre);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage("create", new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateGenresCreated}", String.class)));
            ExternalContext ec = fc.getExternalContext();
            ec.getFlash().setKeepMessages(true);
            try {
                ec.redirect("../inventory.xhtml");
            } catch (IOException ex) {
                LOG.error("Could not redirect to inventory.", ex);
            }
        } catch (IllegalStateException | SecurityException
                | NotSupportedException | RollbackException
                | SystemException | HeuristicMixedException
                | HeuristicRollbackException ex) {
            LOG.error("Could not create genre.", ex);
        }
    }

    public List<Genres> getAllGenres() {
        return allGenres;
    }

    public void setAllGenres(List<Genres> allGenres) {
        this.allGenres = allGenres;
    }

    public Genres getGenre() {
        return genre;
    }

    public void setGenre(Genres genre) {
        this.genre = genre;
    }
}
