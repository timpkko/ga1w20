package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.AllSalesQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TotalSalesQueryBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael, Timmy
 */
@Named("salesByAuthorBacking")
@RequestScoped
public class SalesByAuthorBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SalesByAuthorBackingBean.class);

    private List<AllSalesQueryBean> allSalesByAuthor;
    private TotalSalesQueryBean totalSalesByAuthor;
    private List<Authors> authors;
    private Authors selectedAuthor;

    @Inject
    private ReportQueries reports;
    @Inject
    private AuthorsJpaController authorsController;
    @Inject
    private DateRangeBackingBean dateRangeBean;

    @PostConstruct
    public void init() {
        this.authors = authorsController.findAuthorsEntities();
        Collections.sort(authors, (Authors a1, Authors a2) -> {
            if (a1.getLastname() == null || a2.getLastname() == null) {
                if (a1.getLastname() == null && a2.getLastname() == null) {
                    return a1.getFirstname().compareToIgnoreCase(a2.getFirstname());
                } else if (a1.getLastname() == null) {
                    return a1.getFirstname().compareToIgnoreCase(a2.getLastname());
                } else {
                    return a1.getLastname().compareToIgnoreCase(a2.getFirstname());
                }
            } else {
                return a1.getLastname().compareToIgnoreCase(a2.getLastname());
            }
        });
        this.selectedAuthor = this.authors.get(0);

        updateTable();
    }

    public void updateTable() {
        if (dateRangeBean.getStartDate() == null || dateRangeBean.getEndDate() == null) {
            return;
        }

        try {
            this.totalSalesByAuthor = reports.getTotalSalesByAuthor(selectedAuthor.getAuthorId(),
                    dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
            this.allSalesByAuthor = reports.getAllSalesByAuthor(selectedAuthor.getAuthorId(),
                    dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
        } catch (PersistenceException e) {
            LOG.error("No records found", e);
            this.allSalesByAuthor = new ArrayList<>();
        }
    }

    public List<AllSalesQueryBean> getAllSalesByAuthor() {
        return allSalesByAuthor;
    }

    public TotalSalesQueryBean getTotalSalesByAuthor() {
        return totalSalesByAuthor;
    }

    public List<Authors> getAuthors() {
        return this.authors;
    }

    public Authors getSelectedAuthor() {
        return this.selectedAuthor;
    }

    public void setSelectedAuthor(Authors author) {
        this.selectedAuthor = author;
    }

    public String getAuthorName() {
        if (selectedAuthor.getLastname() == null) {
            return selectedAuthor.getFirstname();
        } else {
            return String.format("%s %s", selectedAuthor.getFirstname(), selectedAuthor.getLastname());
        }
    }

}
