package com.teama1.ga1w20.business.backingbeans.manager.reviews;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Timmy
 */
@Named("reviewsSearchBacking")
@RequestScoped
public class ReviewsSearchBackingBean implements Serializable {

    private boolean reviewId = true;
    private boolean isbn = true;
    private boolean postdate = true;
    private boolean userid = true;
    private boolean rating = true;
    private boolean text = true;

    public boolean isReviewId() {
        return reviewId;
    }

    public void setReviewId(boolean reviewId) {
        this.reviewId = reviewId;
    }

    public boolean isIsbn() {
        return isbn;
    }

    public void setIsbn(boolean isbn) {
        this.isbn = isbn;
    }

    public boolean isPostdate() {
        return postdate;
    }

    public void setPostdate(boolean postdate) {
        this.postdate = postdate;
    }

    public boolean isUserid() {
        return userid;
    }

    public void setUserid(boolean userid) {
        this.userid = userid;
    }

    public boolean isRating() {
        return rating;
    }

    public void setRating(boolean rating) {
        this.rating = rating;
    }

    public boolean isText() {
        return text;
    }

    public void setText(boolean text) {
        this.text = text;
    }

}
