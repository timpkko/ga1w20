package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing page for the genrePage.xhtml. Shows a caroussel with top sellers book
 * in that genre, and a list of the remaining books in the genre, ordered by
 * sales.
 *
 * @author Camillia
 */
@Named("genreBackingBean")
@ViewScoped
public class GenreBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(GenreBackingBean.class);

    @Inject
    GenresJpaController genresJpaController;

    @Inject
    BooksJpaController booksJpaController;

    //Genre id, received as an html parameter
    int genreId;

    Genres genre;

    String genreName;

    //List of top sellers in genre
    List<Books> topSellers;

    //List of remaining books in genre, ordered by sales
    List<Books> books;

    ////////////////////////////////////////////////////////////////////////////
    //Getters
    public int getGenreId() {
        return this.genreId;
    }

    /**
     * Might return null, if the genre could not be fetched.
     */
    public Genres getGenre() {
        if (genre == null) {
            genre = genresJpaController.findGenres(genreId);

            //If genre is still null after fetching it, redirect to 404
            if (genre == null) {
                LOG.error("genre could not be retrieved from received genreid Id. Can not render"
                        + "Browse Genre page.");
                redirect404();
            }
        }
        return genre;
    }

    public String getGenreName() {
        if (genre == null) {
            getGenre();
        }
        genreName = genre.getName();
        return genreName;
    }

    public List<Books> getBooks() {
        if (books == null) {
            buildBooksAndTopSellers();
        }
        return books;
    }

    public List<Books> getTopSellers() {
        if (topSellers == null) {
            buildBooksAndTopSellers();
        }
        return topSellers;
    }

    ////////////////////////////////////////////////////////////////////////////
    //Setters
    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public void setGenre(Genres genre) {
        this.genre = genre;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public void setBooks(List<Books> books) {
        this.books = books;
    }

    public void setTopSellers(List<Books> topSellers) {
        this.topSellers = topSellers;
    }

    ////////////////////////////////////////////////////////////////////////////
    //Helper methods
    private void buildBooksAndTopSellers() {
        if (genre == null) {
            getGenre();
        }

        books = booksJpaController.getTopSellersByGenre(genre.getName());
        System.out.println(books.size());

        if (books == null) {
            books = new ArrayList<>();
            topSellers = new ArrayList<>();
            return;
        }

        //Put top sellers in list to go in caroussel
        //and the remaining in the books list
        if (books.size() > Constants.TOP_SELLERS_LIST_SIZE) {
            topSellers = books.subList(0, Constants.TOP_SELLERS_LIST_SIZE);
            books = books.subList(Constants.TOP_SELLERS_LIST_SIZE, books.size());
        } //If the list is less than the number of books to be displayed in caroussel
        //do not divide list (and caroussel won't be displayed)
        else {
            topSellers = new ArrayList<>();
        }
    }

    /**
     * Calling this method will redirect the user to a 404 error page. This is
     * useful when the user tries to enter a genre id that does not exist in the
     * database in the GET request.
     *
     * @author Timmy
     */
    private void redirect404() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            ExternalContext externalContext = context.getExternalContext();
            externalContext.setResponseStatus(HttpServletResponse.SC_NOT_FOUND);
            externalContext.dispatch(Constants.ERROR_PAGE_URL);
            context.responseComplete();
        } catch (IOException ex) {
            LOG.error("Cannot redirect to 404 page ", ex);
        }
    }
}
