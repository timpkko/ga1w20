package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.TopQueryBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TopSellersBackingBean is used for displaying the top sellers in the report
 * page "topSellers.xhtml".
 *
 * @author Michael, Timmy
 */
@Named("topSellersBacking")
@RequestScoped
public class TopSellersBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(TopSellersBackingBean.class);

    private List<TopQueryBean> books;

    @Inject
    private ReportQueries reports;
    @Inject
    private DateRangeBackingBean dateRangeBean;

    /**
     * Initializes the book list where the start date is the first date and the
     * end date is today's date.
     */
    @PostConstruct
    private void init() {
        updateTable();
    }

    /**
     * Updates the list of books with what the query retrieve between the start
     * date and the end date.
     */
    public void updateTable() {
        if (dateRangeBean.getStartDate() == null || dateRangeBean.getEndDate() == null) {
            return;
        }

        try {
            this.books = reports.getTopSellers(dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
        } catch (PersistenceException e) {
            LOG.error("No records found", e);
            this.books = new ArrayList<>();
        }
    }

    /**
     * Gets the list of books based on what the query retrieves.
     *
     * @return List of books
     */
    public List<TopQueryBean> getBooks() {
        return this.books;
    }
}
