package com.teama1.ga1w20.business.backingbeans.manager.rssfeed;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Rssfeeds;
import com.teama1.ga1w20.persistence.jpacontrollers.RssfeedsJpaController;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.LoggerFactory;

/**
 * Holds the values of the input fields while creating a new ad from the Ads
 * Management page.
 *
 * @author Timmy
 */
@Named("newRssBacking")
@ViewScoped
public class NewRssBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(NewRssBackingBean.class);

    private String url;

    @Inject
    private RssfeedsJpaController rssfeedController;

    public void addNewRssfeed() {
        Rssfeeds rssfeed = new Rssfeeds();

        rssfeed.setUrl(url);
        rssfeed.setActive(false);

        try {
            rssfeedController.create(rssfeed);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Success"), Messages.getString("SuccessfulAddMsg")));
        } catch (IllegalStateException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Cannot create a new rss feed: ", ex);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Error"), Messages.getString("AddError")));
        }
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
