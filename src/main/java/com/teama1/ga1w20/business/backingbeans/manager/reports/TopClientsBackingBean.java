package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.TopQueryBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named("topClientsBacking")
@RequestScoped
public class TopClientsBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(TopClientsBackingBean.class);

    private List<TopQueryBean> clients;

    @Inject
    private ReportQueries reports;
    @Inject
    private DateRangeBackingBean dateRangeBean;

    @PostConstruct
    public void init() {
        updateTable();
    }

    /**
     * Updates the list of books with what the query retrieve between the start
     * date and the end date.
     */
    public void updateTable() {
        if (dateRangeBean.getStartDate() == null || dateRangeBean.getEndDate() == null) {
            return;
        }

        try {
            this.clients = reports.getTopClients(dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
        } catch (PersistenceException e) {
            LOG.error("No records found", e);
            this.clients = new ArrayList<>();
        }
    }

    public List<TopQueryBean> getClients() {
        return clients;
    }
}
