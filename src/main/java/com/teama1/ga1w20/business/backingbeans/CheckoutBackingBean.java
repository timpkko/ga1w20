package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.business.beans.CartBean;
import com.teama1.ga1w20.business.beans.NavigationUtilsBean;
import com.teama1.ga1w20.business.beans.SessionUserBean;
import com.teama1.ga1w20.persistence.entities.Books;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for the checkout page
 *
 * @author @FishYeho
 */
@Named("checkoutBackingBean")
@ViewScoped
public class CheckoutBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(CheckoutBackingBean.class);

    @Inject
    private CartBean cart;

    @Inject
    private SessionUserBean sessionBean;

    @Inject
    private NavigationUtilsBean navigationBean;

    /**
     * Method that instantiates the bean, required for all backing beans
     */
    @PostConstruct
    public void init() {

    }

    /**
     * Get method that returns the books currently inside the CartBean
     *
     * @author @FishYeho
     * @return A list of books currently inside the CartBean
     */
    public List<Books> getBooksInCart() {
        return cart.getCart();
    }

    /**
     * Method that takes a book as a parameter and removes it from the CartBean
     *
     * @author @FishYeho
     * @param book to be removed
     */
    public void removeFromCart(Books book) {
        cart.removeBook(book);
    }

    /**
     * Method that is called when the user hits the "checkout" button If the
     * user is logged-in, they will be directed to the finalization page If the
     * user is not logged-in, they will be redirected to the login page
     *
     * @author @FishYeho
     * @return A String indicating where to redirect the user
     */
    public String checkout() {
        if (sessionBean.isUserLoggedIn()) {
            return "toFinalization";
        }
        //Save the url of the checkout page so we can redirect to it after
        //the user logs in
        navigationBean.savePageURL();
        return "toLogin";
    }
}
