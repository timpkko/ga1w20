package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.AllSalesQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TotalSalesQueryBean;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timmy
 */
@Named("totalSalesBacking")
@RequestScoped
public class TotalSalesBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(TotalSalesBackingBean.class);

    @Inject
    private ReportQueries reports;

    private List<AllSalesQueryBean> allSales;
    private TotalSalesQueryBean totalSales;
    @Inject
    private DateRangeBackingBean dateRangeBean;

    @PostConstruct
    public void init() {
        updateTable();
    }

    public void updateTable() {
        if (dateRangeBean.getStartDate() == null || dateRangeBean.getEndDate() == null) {
            return;
        }

        try {
            this.totalSales = reports.getTotalSales(dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
            this.allSales = reports.getAllSales(dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
        } catch (PersistenceException e) {
            LOG.error("No records found", e);
            this.totalSales = new TotalSalesQueryBean(new BigDecimal(0), "");
            this.allSales = new ArrayList<>();
        }
    }

    public List<AllSalesQueryBean> getAllSales() {
        return allSales;
    }

    public TotalSalesQueryBean getTotalSales() {
        return totalSales;
    }
}
