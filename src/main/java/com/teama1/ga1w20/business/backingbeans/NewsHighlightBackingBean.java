package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.persistence.entities.Rssfeeds;
import com.teama1.ga1w20.persistence.jpacontrollers.RssfeedsJpaController;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * NewsHighlightBackingBean holds the URL of the first active RSS feed from the
 * database.
 *
 * @author Timmy
 */
@Named("newsHighlightBacking")
@ViewScoped
public class NewsHighlightBackingBean implements Serializable {

    private String url;

    @Inject
    private RssfeedsJpaController rssController;

    public String getUrl() {
        // Return a default rss if there are no active rssfeed from the database
        List<Rssfeeds> list = getListOfRssfeeds();
        if (list.isEmpty()) {
            url = Constants.DEFAULT_RSS_FEED_URL;
        } else {
            Collections.shuffle(list);
            url = list.get(0).getUrl();
        }

        return url;
    }

    private List<Rssfeeds> getListOfRssfeeds() {
        return rssController.getActiveFeeds();
    }

}
