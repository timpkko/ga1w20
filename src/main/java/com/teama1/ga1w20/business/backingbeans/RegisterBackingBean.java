package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.utils.HashUtils;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Credentials;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.CredentialsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for the register page
 *
 * @author Camillia E.
 */
@Named("registerBackingBean")
@RequestScoped
public class RegisterBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(RegisterBackingBean.class);

    @Inject
    private UsersJpaController userJpaController;

    @Inject
    private CredentialsJpaController credentialsJpaController;

    ////////////////////////////////////////////////////////////////////////////
    //UI-accessed fields
    private Users user;

    private String password;

    private String passwordConfirm;

    @PostConstruct
    public void init() {
        user = new Users();
    }

    ////////////////////////////////////////////////////////////////////////////
    // Register
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Create a Users and a Credentials object with the inputed information. Add
     * them to the database.
     */
    public String register() {
        if (user == null) {
            LOG.error("The user object must be non null for register() method.");
            displayRegisterError();
            return "toRegister";
        }
        if (password == null) {
            LOG.error("Field password must be non null for register() method.");
            displayRegisterError();
            return "toRegister";
        }
        if (passwordConfirm == null) {
            LOG.error("Field passwordConfirm must be non null for register() method.");
            displayRegisterError();
            return "toRegister";
        }

        user.setUserId(-3);
        user.setCountry(Constants.COUNTRY);

        try {
            userJpaController.create(user);
        } catch (IllegalStateException
                | SecurityException | NotSupportedException
                | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Problem when trying to create new user: " + ex.getMessage());
            displayRegisterError();
            return "toRegister";
        }

        Users createdUser;
        try {
            createdUser = userJpaController.getUserByEmail(user.getEmail());
        } catch (NonUniqueResultException ex) {
            LOG.error("Two users have been returned for email ("
                    + user.getEmail() + ") Unable to proceed with register.");
            displayRegisterError();
            return "toRegister";
        } catch (NoResultException ex) {
            LOG.error("The user doesnt seem to have been registered correctly.");
            displayRegisterError();
            return "toRegister";
        }

        if (createdUser == null) {
            LOG.error("The user doesnt seem to have been registered correctly.");
            displayRegisterError();
            return "toRegister";
        }

        //Create salt and hash password
        HashUtils hashUtils = new HashUtils();
        String salt = hashUtils.createSalt();
        byte[] hashedPassword;
        try {
            hashedPassword = hashUtils.hashPassword(password, salt);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            LOG.error("Unable to hash password for new user. Unable to "
                    + "proceed with registration. Exception: " + ex.getMessage());
            return abortRegistration(createdUser.getUserId());
        }
        if (hashedPassword == null) {
            LOG.error("Unable to hash password for new user. Unable to "
                    + "proceed with registration.");
            return abortRegistration(createdUser.getUserId());
        }

        //Create credentials and add to Credentials table
        Credentials credentials = new Credentials(createdUser.getUserId(),
                hashedPassword, salt, false);
        try {
            credentialsJpaController.create(credentials);
        } catch (PreexistingEntityException
                | IllegalStateException | SecurityException
                | HeuristicMixedException | HeuristicRollbackException
                | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Unable to create Credentials record for new user. "
                    + "Exception: " + ex.getLocalizedMessage());
            return abortRegistration(createdUser.getUserId());
        }

        //If it got here, the registration is a success.
        displayRegisterSuccess();

        return "toLogin";
    }

    private String abortRegistration(Integer createdUserId) {
        LOG.error("Aborting registration. Deleting newly created "
                + "record in Users table.");
        try {
            //Delete newly-inserted user
            userJpaController.destroy(createdUserId);
            displayRegisterError();
        } catch (IllegalOrphanException | NonexistentEntityException
                | SecurityException | HeuristicMixedException
                | HeuristicRollbackException | RollbackException
                | SystemException | NotSupportedException ex) {
            LOG.error("Deleting newly inserted record - UNSUCCESSFUL. "
                    + "Exception: " + ex.getMessage());
            displayRegisterError();
        }
        return "toRegister";
    }

    /**
     * Attach a Registration error message to the registration form.
     *
     * @param context
     * @param messageId
     */
    private void displayRegisterError() {
        Messages.addMessageToContext(FacesMessage.SEVERITY_ERROR,
                "RegisterError", "registerForm");
    }

    /**
     * Attach a Registration success message to the registration form.
     *
     * @param context
     * @param messageId
     */
    private void displayRegisterSuccess() {
        Messages.addMessageToContext(FacesMessage.SEVERITY_INFO,
                "RegisterSuccess", "growlMsg");
        Messages.addMessageToContext(FacesMessage.SEVERITY_INFO,
                "RegisterSuccess", null);
    }

    ////////////////////////////////////////////////////////////////////////////
    // Getters & Setters
    ////////////////////////////////////////////////////////////////////////////
    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
