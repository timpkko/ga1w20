package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.AllSalesQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TotalSalesQueryBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named("salesByClientBacking")
@RequestScoped
public class SalesByClientBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SalesByClientBackingBean.class);

    private List<AllSalesQueryBean> allSalesByClient;
    private TotalSalesQueryBean totalSalesByClient;

    private List<Users> clients;
    private Users selectedClient;

    @Inject
    private ReportQueries reports;
    @Inject
    private UsersJpaController usersController;
    @Inject
    private DateRangeBackingBean dateRangeBean;

    @PostConstruct
    public void init() {
        this.clients = usersController.findUsersEntities();
        Collections.sort(clients, (Users u1, Users u2) -> {
            if (u1.getLastname() == null || u2.getLastname() == null) {
                if (u1.getLastname() == null && u2.getLastname() == null) {
                    return u1.getFirstname().compareToIgnoreCase(u2.getFirstname());
                } else if (u1.getLastname() == null) {
                    return u1.getFirstname().compareToIgnoreCase(u2.getLastname());
                } else {
                    return u1.getLastname().compareToIgnoreCase(u2.getFirstname());
                }
            } else {
                return u1.getLastname().compareToIgnoreCase(u2.getLastname());
            }
        });
        this.selectedClient = this.clients.get(0);

        updateTable();
    }

    public void updateTable() {
        if (dateRangeBean.getStartDate() == null || dateRangeBean.getEndDate() == null) {
            return;
        }

        try {
            this.totalSalesByClient = reports.getTotalSalesByClient(selectedClient.getUserId(),
                    dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
            this.allSalesByClient = reports.getAllSalesByClient(selectedClient.getUserId(),
                    dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
        } catch (PersistenceException e) {
            LOG.error("No records found", e);
            this.allSalesByClient = new ArrayList<>();
        }
    }

    public List<AllSalesQueryBean> getAllSalesByClient() {
        return allSalesByClient;
    }

    public TotalSalesQueryBean getTotalSalesByClient() {
        return totalSalesByClient;
    }

    public List<Users> getClients() {
        return this.clients;
    }

    public Users getSelectedClient() {
        return this.selectedClient;
    }

    public void setSelectedClient(Users client) {
        this.selectedClient = client;
    }

    public String getClientName() {
        return String.format("%s, %s", this.selectedClient.getLastname(), this.selectedClient.getFirstname());
    }
}
