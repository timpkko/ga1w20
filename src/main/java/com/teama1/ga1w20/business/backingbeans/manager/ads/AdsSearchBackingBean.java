package com.teama1.ga1w20.business.backingbeans.manager.ads;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Timmy
 */
@Named("adsSearchBacking")
@RequestScoped
public class AdsSearchBackingBean implements Serializable {

    private boolean adId = true;
    private boolean url = true;
    private boolean active = true;

    public boolean isAdId() {
        return adId;
    }

    public void setAdId(boolean adId) {
        this.adId = adId;
    }

    public boolean isUrl() {
        return url;
    }

    public void setUrl(boolean url) {
        this.url = url;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
