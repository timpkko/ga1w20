package com.teama1.ga1w20.business.backingbeans.manager.clients;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Timmy
 */
@Named("clientsSearchBacking")
@RequestScoped
public class ClientsSearchBackingBean implements Serializable {

    private boolean userId = true;
    private boolean lastName = true;
    private boolean firstName = true;
    private boolean email = true;
    private boolean country = true;
    private boolean city = true;
    private boolean address = true;
    private boolean secondAddress = true;
    private boolean postalCode = true;
    private boolean province = true;
    private boolean homephone = true;
    private boolean cellphone = true;
    private boolean company = true;
    private boolean title = true;
    private boolean manager = true;

    public boolean isProvince() {
        return province;
    }

    public void setProvince(boolean province) {
        this.province = province;
    }

    public boolean isUserId() {
        return userId;
    }

    public void setUserId(boolean userId) {
        this.userId = userId;
    }

    public boolean isLastName() {
        return lastName;
    }

    public void setLastName(boolean lastName) {
        this.lastName = lastName;
    }

    public boolean isFirstName() {
        return firstName;
    }

    public void setFirstName(boolean firstName) {
        this.firstName = firstName;
    }

    public boolean isEmail() {
        return email;
    }

    public void setEmail(boolean email) {
        this.email = email;
    }

    public boolean isCountry() {
        return country;
    }

    public void setCountry(boolean country) {
        this.country = country;
    }

    public boolean isCity() {
        return city;
    }

    public void setCity(boolean city) {
        this.city = city;
    }

    public boolean isAddress() {
        return address;
    }

    public void setAddress(boolean address) {
        this.address = address;
    }

    public boolean isSecondAddress() {
        return secondAddress;
    }

    public void setSecondAddress(boolean secondAddress) {
        this.secondAddress = secondAddress;
    }

    public boolean isPostalCode() {
        return postalCode;
    }

    public void setPostalCode(boolean postalCode) {
        this.postalCode = postalCode;
    }

    public boolean isHomephone() {
        return homephone;
    }

    public void setHomephone(boolean homephone) {
        this.homephone = homephone;
    }

    public boolean isCellphone() {
        return cellphone;
    }

    public void setCellphone(boolean cellphone) {
        this.cellphone = cellphone;
    }

    public boolean isCompany() {
        return company;
    }

    public void setCompany(boolean company) {
        this.company = company;
    }

    public boolean isTitle() {
        return title;
    }

    public void setTitle(boolean title) {
        this.title = title;
    }

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }

}
