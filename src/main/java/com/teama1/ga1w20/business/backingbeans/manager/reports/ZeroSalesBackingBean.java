package com.teama1.ga1w20.business.backingbeans.manager.reports;

import com.teama1.ga1w20.persistence.queries.ReportQueries;
import com.teama1.ga1w20.persistence.queries.beans.SimpleBookQueryBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael, Timmy
 */
@Named("zeroSalesBacking")
@RequestScoped
public class ZeroSalesBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(ZeroSalesBackingBean.class);

    @Inject
    private ReportQueries reports;

    private List<SimpleBookQueryBean> books;

    @Inject
    private DateRangeBackingBean dateRangeBean;

    @PostConstruct
    public void init() {
        updateTable();
    }

    /**
     * Updates the list of books with what the query retrieve between the start
     * date and the end date.
     */
    public void updateTable() {
        if (dateRangeBean.getStartDate() == null || dateRangeBean.getEndDate() == null) {
            return;
        }

        try {
            this.books = reports.getZeroSales(dateRangeBean.getStartDate(), dateRangeBean.getEndDate());
        } catch (PersistenceException e) {
            LOG.info("No records found");
            this.books = new ArrayList<>();
        }
    }

    /**
     * Gets the list of books based on what the query retrieves.
     *
     * @return List of books
     */
    public List<SimpleBookQueryBean> getBooks() {
        return this.books;
    }
}
