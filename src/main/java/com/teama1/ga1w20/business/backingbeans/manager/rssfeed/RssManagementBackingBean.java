package com.teama1.ga1w20.business.backingbeans.manager.rssfeed;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Rssfeeds;
import com.teama1.ga1w20.persistence.jpacontrollers.RssfeedsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.PrimeFaces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timmy
 */
@Named("rssManagementBacking")
@ViewScoped
public class RssManagementBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(RssManagementBackingBean.class);

    @Inject
    private RssfeedsJpaController rssfeedController;

    private List<Rssfeeds> rssfeeds;
    private List<Rssfeeds> filteredRssfeeds;

    @Inject
    private RssSearchBackingBean rssSearchBean;

    private Rssfeeds selectedFeed;

    public List<Rssfeeds> getRssfeeds() {
        return rssfeeds;
    }

    public void setRssfeeds(List<Rssfeeds> rssfeeds) {
        this.rssfeeds = rssfeeds;
    }

    public List<Rssfeeds> getFilteredRssfeeds() {
        return filteredRssfeeds;
    }

    public void setFilteredRssfeeds(List<Rssfeeds> filteredRssfeeds) {
        this.filteredRssfeeds = filteredRssfeeds;
    }

    public Rssfeeds getSelectedFeed() {
        return selectedFeed;
    }

    public void setSelectedFeed(Rssfeeds selectedFeed) {
        this.selectedFeed = selectedFeed;
    }

    /**
     * Resets the dialog form inputs for the next rss feed.
     *
     * @author Michael
     * @param formId
     */
    public void resetDialogForm(String formId) {
        PrimeFaces.current().resetInputs(formId);
    }

    public void checkAllCheckboxes() {
        rssSearchBean.setFeedId(true);
        rssSearchBean.setUrl(true);
        rssSearchBean.setActive(true);
    }

    public void uncheckAllCheckboxes() {
        rssSearchBean.setFeedId(false);
        rssSearchBean.setUrl(false);
        rssSearchBean.setActive(false);
    }

    /**
     * Updates the list of ads.
     */
    @PostConstruct
    public void updateTable() {
        rssfeeds = rssfeedController.findRssfeedsEntities();
    }

    /**
     * Process the filtering of records from the keywords inputted in the
     * search. This method is necessary in order to use the filtering function
     * from PrimeFaces' component Datatable.
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean globalFitlerFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        Rssfeeds ad = (Rssfeeds) value;

        return getCriteria(ad, filterText);
    }

    /**
     * This method checks if the text from the search matches with any
     * information about the record. If there is at least one match, the record
     * is then displayed in the table. It takes account of what columns are
     * checked and unchecked, a useful ability to narrow the search results.
     *
     * @param ad
     * @param filterText
     * @return
     */
    private boolean getCriteria(Rssfeeds ad, String filterText) {
        // Those booleans are required to determines whether there
        // is a match or not.
        boolean adId = false;
        boolean url = false;
        boolean active = false;

        // Do the matching if only the column is checked
        if (rssSearchBean.isFeedId()) {
            adId = ad.getFeedId() == getInteger(filterText);
        }
        if (rssSearchBean.isUrl()) {
            url = ad.getUrl().toLowerCase().contains(filterText);
        }
        if (rssSearchBean.isActive()) {
            active = "active".contains(filterText);
        }

        return adId || url || active;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    /**
     * Persists the changes done to an existing RSS feed record, and updates the
     * list with the new changes. This does not happen if exceptions were caught
     * during the process.
     */
    public void saveChanges() {
        FacesMessage msg;
        try {
            rssfeedController.edit(selectedFeed);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Success"), Messages.getString("SuccessfulEditMsg"));
            updateTable();
        } catch (NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException e) {
            LOG.error(selectedFeed.getFeedId() + " cannot be edited", e);
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("EditError"));
        }
        selectedFeed = null;
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Deletes an RSS feed record from the database, and updates the list of
     * records to mirror the changes. This does not happen if exceptions were
     * caught during the process.
     */
    public void deleteRecord() {
        FacesMessage msg;
        try {
            rssfeedController.destroy(selectedFeed.getFeedId());
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Success"), Messages.getString("SuccessfulDeleteMsg"));
            updateTable();
        } catch (NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException e) {
            LOG.error(selectedFeed.getFeedId() + " cannot be deleted", e);
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("DeleteError"));
        }

        selectedFeed = null;
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
