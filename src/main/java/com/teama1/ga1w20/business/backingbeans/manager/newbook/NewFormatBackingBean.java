package com.teama1.ga1w20.business.backingbeans.manager.newbook;

import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This backing bean serves to create a new format.
 *
 * @format Michael
 */
@Named("newFormatBacking")
@ViewScoped
public class NewFormatBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(NewFormatBackingBean.class);

    @Inject
    private FormatsJpaController formatsController;

    private List<Formats> allFormats;
    private Formats format;

    @PostConstruct
    public void init() {
        allFormats = formatsController.findFormatsEntities();
        format = new Formats();
    }

    public void createFormat() {
        try {
            format.setBookformatList(null);
            formatsController.create(format);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage("create", new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateFormatsCreated}", String.class)));
            ExternalContext ec = fc.getExternalContext();
            ec.getFlash().setKeepMessages(true);
            try {
                ec.redirect("../inventory.xhtml");
            } catch (IOException ex) {
                LOG.error("Could not redirect to inventory.", ex);
            }
        } catch (RollbackFailureException | IllegalStateException
                | SecurityException | NotSupportedException
                | RollbackException | SystemException | HeuristicMixedException
                | HeuristicRollbackException ex) {
            LOG.error("Could not create format.", ex);
        }
    }

    public List<Formats> getAllFormats() {
        return allFormats;
    }

    public void setAllFormats(List<Formats> allFormats) {
        this.allFormats = allFormats;
    }

    public Formats getFormat() {
        return format;
    }

    public void setFormat(Formats format) {
        this.format = format;
    }
}
