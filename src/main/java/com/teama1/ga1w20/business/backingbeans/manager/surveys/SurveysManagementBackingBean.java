package com.teama1.ga1w20.business.backingbeans.manager.surveys;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Surveys;
import com.teama1.ga1w20.persistence.jpacontrollers.SurveysJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.PrimeFaces;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Timmy
 */
@Named("surveysManagementBacking")
@SessionScoped
public class SurveysManagementBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SurveysManagementBackingBean.class);

    @Inject
    private SurveysJpaController surveysController;

    private List<Surveys> surveys;
    private List<Surveys> filteredSurveys;

    @Inject
    private SurveysSearchBackingBean surveysSearchBean;

    private Surveys selectedSurvey;

    public Surveys getSelectedSurvey() {
        return selectedSurvey;
    }

    public void setSelectedSurvey(Surveys selectedSurvey) {
        this.selectedSurvey = selectedSurvey;
    }

    public List<Surveys> getSurveys() {
        this.surveys = surveysController.findSurveysEntities();
        return surveys;
    }

    public void setSurveys(List<Surveys> surveys) {
        this.surveys = surveys;
    }

    public List<Surveys> getFilteredSurveys() {
        return filteredSurveys;
    }

    public void setFilteredSurveys(List<Surveys> filteredSurveys) {
        this.filteredSurveys = filteredSurveys;
    }

    /**
     * Resets the dialog form inputs for the next survey.
     *
     * @author Michael
     * @param formId
     */
    public void resetDialogForm(String formId) {
        PrimeFaces.current().resetInputs(formId);
    }

    public void checkAllCheckboxes() {
        surveysSearchBean.setSurveyId(true);
        surveysSearchBean.setQuestion(true);
        surveysSearchBean.setAnswer1(true);
        surveysSearchBean.setAnswer2(true);
        surveysSearchBean.setAnswer3(true);
        surveysSearchBean.setAnswer4(true);
        surveysSearchBean.setVote1(true);
        surveysSearchBean.setVote2(true);
        surveysSearchBean.setVote3(true);
        surveysSearchBean.setVote4(true);
        surveysSearchBean.setActive(true);
    }

    public void uncheckAllCheckboxes() {
        surveysSearchBean.setSurveyId(false);
        surveysSearchBean.setQuestion(false);
        surveysSearchBean.setAnswer1(false);
        surveysSearchBean.setAnswer2(false);
        surveysSearchBean.setAnswer3(false);
        surveysSearchBean.setAnswer4(false);
        surveysSearchBean.setVote1(false);
        surveysSearchBean.setVote2(false);
        surveysSearchBean.setVote3(false);
        surveysSearchBean.setVote4(false);
        surveysSearchBean.setActive(false);
    }

    /**
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean globalFitlerFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        Surveys survey = (Surveys) value;

        return getCriteria(survey, filterText);
    }

    private boolean getCriteria(Surveys survey, String filterText) {
        boolean surveyId = false;
        boolean question = false;
        boolean answer1 = false;
        boolean answer2 = false;
        boolean answer3 = false;
        boolean answer4 = false;
        boolean vote1 = false;
        boolean vote2 = false;
        boolean vote3 = false;
        boolean vote4 = false;
        boolean active = false;

        if (surveysSearchBean.isSurveyId()) {
            surveyId = survey.getSurveyId() == getInteger(filterText);
        }
        if (surveysSearchBean.isQuestion()) {
            question = survey.getQuestion().toLowerCase().contains(filterText);
        }
        if (surveysSearchBean.isAnswer1()) {
            answer1 = survey.getAnswer1().toLowerCase().contains(filterText);
        }
        if (surveysSearchBean.isAnswer2()) {
            answer2 = survey.getAnswer2().toLowerCase().contains(filterText);
        }
        if (surveysSearchBean.isAnswer3()) {
            answer3 = survey.getAnswer3().toLowerCase().contains(filterText);
        }
        if (surveysSearchBean.isAnswer4()) {
            answer4 = survey.getAnswer4().toLowerCase().contains(filterText);
        }
        if (surveysSearchBean.isVote1()) {
            vote1 = survey.getVote1() == getInteger(filterText);
        }
        if (surveysSearchBean.isVote2()) {
            vote2 = survey.getVote1() == getInteger(filterText);
        }
        if (surveysSearchBean.isVote3()) {
            vote3 = survey.getVote1() == getInteger(filterText);
        }
        if (surveysSearchBean.isVote4()) {
            vote4 = survey.getVote1() == getInteger(filterText);
        }
        if (surveysSearchBean.isActive()) {
            active = "active".contains(filterText);
        }

        return surveyId || question || answer1 || answer2 || answer3 || answer4
                || vote1 || vote2 || vote3 || vote4 || active;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public void saveChanges() {
        FacesMessage msg;
        try {
            surveysController.edit(selectedSurvey);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Success"), Messages.getString("SuccessfulEditMsg"));
            selectedSurvey = null;
        } catch (NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException e) {
            LOG.error(selectedSurvey.getSurveyId() + " cannot be edited", e);
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("EditError"));
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Resets the votes of a survey to zero, and persists the changes.
     */
    public void zeroOutVotes() {
        // Set all the votes to 0
        selectedSurvey.setVote1(0);
        selectedSurvey.setVote2(0);
        selectedSurvey.setVote3(0);
        selectedSurvey.setVote4(0);

        saveChanges();
    }

}
