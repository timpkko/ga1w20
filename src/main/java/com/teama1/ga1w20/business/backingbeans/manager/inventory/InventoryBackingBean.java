package com.teama1.ga1w20.business.backingbeans.manager.inventory;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.beans.manager.AuthorWrapperBean;
import com.teama1.ga1w20.business.beans.manager.BookWrapperBean;
import com.teama1.ga1w20.business.beans.manager.FormatWrapperBean;
import com.teama1.ga1w20.business.beans.manager.GenreWrapperBean;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.business.utils.UploadImageUtils;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Bookauthor;
import com.teama1.ga1w20.persistence.entities.Bookformat;
import com.teama1.ga1w20.persistence.entities.Bookgenre;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.entities.Publishers;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BookauthorJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BookformatJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BookgenreJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.PublishersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.queries.ReportQueries;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named("inventoryBacking")
@ViewScoped
public class InventoryBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(InventoryBackingBean.class);

    @Inject
    private InventorySearchBackingBean inventoryBean;
    @Inject
    private ReportQueries reports;
    @Inject
    private BooksJpaController booksController;
    @Inject
    private AuthorsJpaController authorsController;
    @Inject
    private BookauthorJpaController bookauthorController;
    @Inject
    private PublishersJpaController publishersController;
    @Inject
    private GenresJpaController genresController;
    @Inject
    private BookgenreJpaController bookgenreController;
    @Inject
    private FormatsJpaController formatsController;
    @Inject
    private BookformatJpaController bookformatController;

    private final Date today = new Date();
    private List<BookWrapperBean> filteredBooks;

    private BookWrapperBean selectedBook;
    private UploadedFile selectedImage;
    private ArrayList<AuthorWrapperBean> selectedBookAuthors;
    private ArrayList<GenreWrapperBean> selectedBookGenres;
    private ArrayList<FormatWrapperBean> selectedBookFormats;
    private Integer selectedBookNumPages;
    private Integer selectedBookMinAge;

    private List<BookWrapperBean> allBooks;
    private List<Authors> allAuthors;
    private List<Publishers> allPublishers;
    private List<Genres> allGenres;
    private List<Formats> allFormats;

    /**
     * Sets up lists for the xhtml page and default parameters.
     */
    @PostConstruct
    public void init() {
        selectedBook = new BookWrapperBean(new Books());
        resetMainLists();
    }

    /////////////////////
    /////DATA TABLE//////
    /////////////////////
    public Date getToday() {
        return today;
    }

    public BigDecimal getTotalSalesOfABook(Long isbn) {
        return reports.getTotalSalesOfABook(isbn);
    }

    /**
     * Returns a human-readable string of the list of authors for a book.
     *
     * @param authorWrapperBeans
     * @return
     */
    public String getStringListOfAuthors(ArrayList<AuthorWrapperBean> authorWrapperBeans) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < authorWrapperBeans.size(); i++) {
            sb.append(authorWrapperBeans.get(i).getAuthor().getFirstname());
            if (authorWrapperBeans.get(i).getAuthor().getLastname() != null) {
                sb.append(" ").append(authorWrapperBeans.get(i).getAuthor().getLastname());
            }
            if (i != authorWrapperBeans.size() - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    /**
     * Returns a human-readable string of the list of genres for a book.
     *
     * @param genreWrapperBeans
     * @return
     */
    public String getStringListOfGenres(ArrayList<GenreWrapperBean> genreWrapperBeans) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < genreWrapperBeans.size(); i++) {
            sb.append(genreWrapperBeans.get(i).getGenre().getName());
            if (i != genreWrapperBeans.size() - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    /////////////////////
    ///////SORTING///////
    /////////////////////
    public List<BookWrapperBean> getFilteredBooks() {
        return filteredBooks;
    }

    public void setFilteredBooks(List<BookWrapperBean> filteredBooks) {
        this.filteredBooks = filteredBooks;
    }

    public void checkAllCheckboxes() {
        inventoryBean.setIsbn(true);
        inventoryBean.setTitle(true);
        inventoryBean.setAuthors(true);
        inventoryBean.setGenres(true);
        inventoryBean.setPublisher(true);
        inventoryBean.setPubDate(true);
        inventoryBean.setListPrice(true);
        inventoryBean.setSalePrice(true);
        inventoryBean.setWholesalePrice(true);
        inventoryBean.setMinAge(true);
        inventoryBean.setMaxAge(true);
        inventoryBean.setRemoved(true);
        inventoryBean.setTotalSales(true);
    }

    public void uncheckAllCheckboxes() {
        inventoryBean.setIsbn(false);
        inventoryBean.setTitle(false);
        inventoryBean.setPubDate(false);
        inventoryBean.setAuthors(true);
        inventoryBean.setGenres(true);
        inventoryBean.setPublisher(true);
        inventoryBean.setListPrice(false);
        inventoryBean.setSalePrice(false);
        inventoryBean.setWholesalePrice(false);
        inventoryBean.setMinAge(false);
        inventoryBean.setMaxAge(false);
        inventoryBean.setRemoved(false);
        inventoryBean.setTotalSales(false);
    }

    /**
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean bookGlobalFilterFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        BookWrapperBean book = (BookWrapperBean) value;
        return getCriteria(book, filterText);
    }

    /**
     * Criteria for the global filter function.
     *
     * @param book
     * @param filterText
     * @return
     */
    private boolean getCriteria(BookWrapperBean book, String filterText) {
        boolean isbn = false, title = false, authors = false, genres = false,
                publisher = false, pubDateString = false, pubDateNum = false,
                listPrice = false, salePrice = false, wholesalePrice = false,
                age = false, minAge = false, maxAge = false, removed = false,
                totalSales = false;
        BigDecimal filterNum = new BigDecimal(getInteger(filterText));

        if (inventoryBean.isIsbn()) {
            isbn = book.getBook().getIsbn().toString().contains(filterText);
        }
        if (inventoryBean.isTitle()) {
            title = book.getBook().getTitle().toLowerCase().trim().contains(filterText);
        }
        if (inventoryBean.isAuthors()) {
            authors = getStringListOfAuthors(book.getAuthors()).toLowerCase().trim().contains(filterText);
        }
        if (inventoryBean.isGenres()) {
            genres = getStringListOfGenres(book.getGenres()).toLowerCase().trim().contains(filterText);
        }
        if (inventoryBean.isPublisher()) {
            publisher = book.getBook().getPublisherId().getName().toLowerCase().trim().contains(filterText);
        }
        if (inventoryBean.isRemoved()) {
            removed = (book.getBook().getRemoved() && "removed".contains(filterText));
        }
        if (inventoryBean.isPubDate()) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                pubDateString = simpleDateFormat.format(book.getBook().getPubDate()).trim().contains(filterText);
            } catch (Exception e) {
                pubDateString = false;
            }
        }
        if (filterNum.compareTo(new BigDecimal(-1)) != 0) {
            if (inventoryBean.isPubDate()) {
                try {
                    BigInteger integer = filterNum.toBigInteger();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    pubDateNum = simpleDateFormat.format(book.getBook().getPubDate()).contains(integer.toString());
                } catch (Exception e) {
                    pubDateNum = false;
                }
            }
            if (inventoryBean.isListPrice()) {
                listPrice = book.getBook().getListPrice().compareTo(filterNum) < 1;
            }
            if (inventoryBean.isSalePrice()) {
                salePrice = book.getBook().getSalePrice().compareTo(filterNum) < 1;
            }
            if (inventoryBean.isWholesalePrice()) {
                wholesalePrice = book.getBook().getWholesalePrice().compareTo(filterNum) < 1;
            }
            if (inventoryBean.isMinAge() && inventoryBean.isMaxAge() && book.getBook().getMaxAge() != null) {
                age = book.getBook().getMinAge() <= filterNum.doubleValue() && book.getBook().getMaxAge() >= filterNum.doubleValue();
            } else {
                if (inventoryBean.isMinAge()) {
                    minAge = book.getBook().getMinAge() <= filterNum.doubleValue();
                }
                if (inventoryBean.isMaxAge()) {
                    maxAge = (book.getBook().getMaxAge() != null && book.getBook().getMaxAge() >= filterNum.doubleValue());
                }
            }
            if (inventoryBean.isTotalSales()) {
                BigDecimal total = getTotalSalesOfABook(book.getBook().getIsbn());
                if (total == null) {
                    totalSales = new BigDecimal(0).compareTo(filterNum) < 1;
                } else {
                    totalSales = total.compareTo(filterNum) < 1;
                }
            }
        }

        return isbn || title || authors || genres || publisher
                || pubDateString || pubDateNum || listPrice || salePrice
                || wholesalePrice || age || minAge || maxAge || removed
                || totalSales;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    /////////////////////
    ////SELECTED BOOK////
    /////////////////////
    public BookWrapperBean getSelectedBook() {
        return selectedBook;
    }

    public void setSelectedBook(BookWrapperBean selectedBook) {
        this.selectedBook = selectedBook;
    }

    public UploadedFile getSelectedImage() {
        return selectedImage;
    }

    public void setSelectedImage(UploadedFile selectedImage) {
        this.selectedImage = selectedImage;
    }

    public void setImage(FileUploadEvent event) {
        selectedImage = event.getFile();
    }

    public ArrayList<AuthorWrapperBean> getSelectedBookAuthors() {
        return selectedBookAuthors;
    }

    public void setSelectedBookAuthors(ArrayList<AuthorWrapperBean> selectedBookAuthors) {
        this.selectedBookAuthors = selectedBookAuthors;
    }

    public ArrayList<GenreWrapperBean> getSelectedBookGenres() {
        return selectedBookGenres;
    }

    public void setSelectedBookGenres(ArrayList<GenreWrapperBean> selectedBookGenres) {
        this.selectedBookGenres = selectedBookGenres;
    }

    public ArrayList<FormatWrapperBean> getSelectedBookFormats() {
        return selectedBookFormats;
    }

    public void setSelectedBookFormats(ArrayList<FormatWrapperBean> selectedBookFormats) {
        this.selectedBookFormats = selectedBookFormats;
    }

    public Integer getSelectedBookNumPages() {
        return selectedBookNumPages;
    }

    public void setSelectedBookNumPages(Integer selectedBookNumPages) {
        this.selectedBookNumPages = selectedBookNumPages;
    }

    public Integer getSelectedBookMinAge() {
        return selectedBookMinAge;
    }

    public void setSelectedBookMinAge(Integer selectedBookMinAge) {
        this.selectedBookMinAge = selectedBookMinAge;
    }

    /////////////////////
    /////ENTITY LISTS////
    /////////////////////
    public List<BookWrapperBean> getAllBooks() {
        return allBooks;
    }

    public void setAllBooks(List<BookWrapperBean> allBooks) {
        this.allBooks = allBooks;
    }

    public List<Authors> getAllAuthors() {
        return allAuthors;
    }

    public void setAllAuthors(List<Authors> allAuthors) {
        this.allAuthors = allAuthors;
    }

    public List<Publishers> getAllPublishers() {
        return allPublishers;
    }

    public void setAllPublishers(List<Publishers> allPublishers) {
        this.allPublishers = allPublishers;
    }

    public List<Genres> getAllGenres() {
        return allGenres;
    }

    public void setAllGenres(List<Genres> allGenres) {
        this.allGenres = allGenres;
    }

    public List<Formats> getAllFormats() {
        return allFormats;
    }

    public void setAllFormats(List<Formats> allFormats) {
        this.allFormats = allFormats;
    }

    /**
     * Resets the dialog form inputs for the next book.
     *
     * @author Michael
     * @param formId
     */
    public void resetDialogForm(String formId) {
        PrimeFaces.current().resetInputs(formId);
    }

    /**
     * Gets a copy of the author(s), genre(s) and format(s) associated to the
     * currently selectedBook.
     */
    public void fillSelectedBookLists() {
        resetDialogForm("bookEditForm");
        selectedBookAuthors = new ArrayList<>(selectedBook.getAuthors());
        selectedBookGenres = new ArrayList<>(selectedBook.getGenres());
        selectedBookFormats = new ArrayList<>(selectedBook.getFormats());
        selectedBookNumPages = selectedBook.getBook().getNumPages();
        selectedBookMinAge = selectedBook.getBook().getMinAge();
    }

    public void addAuthorToSelectedList() {
        selectedBookAuthors.add(new AuthorWrapperBean(new Authors(), true));;
    }

    public void addGenreToSelectedList() {
        selectedBookGenres.add(new GenreWrapperBean(new Genres(), true));
    }

    public void addFormatToSelectedList() {
        selectedBookFormats.add(new FormatWrapperBean(new Formats(), true));
    }

    public void removeAuthorFromSelectedList(AuthorWrapperBean author) {
        selectedBookAuthors.remove(author);
    }

    public void removeGenreFromSelectedList(GenreWrapperBean genre) {
        selectedBookGenres.remove(genre);
    }

    public void removeFormatFromSelectedList(FormatWrapperBean format) {
        selectedBookFormats.remove(format);
    }

    /**
     * Sorts and retrieves all needed entities from the database.
     *
     * When running ajax in the html of this page, the author, publisher, genre
     * and format lists get modified. All this method does is reset those lists
     * so that everything is still the same on partial submit.
     */
    private void resetMainLists() {
        //BOOK
        List<Books> books = booksController.findBooksEntities();
        allBooks = new ArrayList<>();
        for (Books book : books) {
            List<Authors> authors = authorsController.getAuthorsByBook(book);
            ArrayList<AuthorWrapperBean> authorWrapperBeans = new ArrayList<>();
            for (Authors a : authors) {
                authorWrapperBeans.add(new AuthorWrapperBean(a, true));
            }

            List<Genres> genres = genresController.getGenresByBook(book);
            ArrayList<GenreWrapperBean> genreWrapperBeans = new ArrayList<>();
            for (Genres g : genres) {
                genreWrapperBeans.add(new GenreWrapperBean(g, true));
            }

            List<Formats> formats = formatsController.getFormatsByBook(book);
            ArrayList<FormatWrapperBean> formatWrapperBeans = new ArrayList<>();
            for (Formats f : formats) {
                formatWrapperBeans.add(new FormatWrapperBean(f, true));
            }
            allBooks.add(new BookWrapperBean(book, authorWrapperBeans, genreWrapperBeans, formatWrapperBeans));
        }

        //AUTHOR
        allAuthors = authorsController.findAuthorsEntities();
        Collections.sort(allAuthors, (Authors a1, Authors a2) -> {
            if (a1.getLastname() == null || a2.getLastname() == null) {
                if (a1.getLastname() == null && a2.getLastname() == null) {
                    return a1.getFirstname().compareToIgnoreCase(a2.getFirstname());
                } else if (a1.getLastname() == null) {
                    return a1.getFirstname().compareToIgnoreCase(a2.getLastname());
                } else {
                    return a1.getLastname().compareToIgnoreCase(a2.getFirstname());
                }
            } else {
                return a1.getLastname().compareToIgnoreCase(a2.getLastname());
            }
        });

        //PUBLISHER
        allPublishers = publishersController.findPublishersEntities();
        Collections.sort(allPublishers, (Publishers p1, Publishers p2) -> {
            return p1.getName().compareToIgnoreCase(p2.getName());
        });

        //GENRE
        allGenres = genresController.findGenresEntities();
        Collections.sort(allGenres, (Genres g1, Genres g2) -> {
            return g1.getName().compareToIgnoreCase(g2.getName());
        });

        //FORMAT
        allFormats = formatsController.findFormatsEntities();
        Collections.sort(allFormats, (Formats f1, Formats f2) -> {
            return f1.getExtension().compareToIgnoreCase(f2.getExtension());
        });
    }

    /////////////////////
    ///////EDITING///////
    /////////////////////
    /**
     * Persists the changes done to an existing book record, and updates the
     * list with the new changes. This does not happen if exceptions were caught
     * during the process.
     */
    public void saveChanges() {
        FacesMessage msg;
        FacesContext fc = FacesContext.getCurrentInstance();

        //Deal with duplicate entities first
        if (hasDuplicateEntity(selectedBookAuthors, selectedBookGenres, selectedBookFormats)) {
            selectedBook = new BookWrapperBean(new Books());
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("DuplicateEntityBook_Edit"), Messages.getString("EditError"));
            fc.addMessage(null, msg);
            redirectToInventory(fc);
        } else {
            try {
                //Deal with images first
                if (selectedImage != null) {
                    UploadImageUtils coverImageUtil = new UploadImageUtils(selectedImage);
                    coverImageUtil.saveToDisk(Constants.BOOK_COVERS_PATH, selectedBook.getBook().getIsbn().toString());
                    BufferedImage thumbnailImageUtil = coverImageUtil.resizeImage(Constants.BOOK_THUMBNAIL_HEIGHT);
                    UploadImageUtils.saveToDisk(thumbnailImageUtil, Constants.BOOK_THUMBS_PATH, selectedBook.getBook().getIsbn().toString().concat("_thumb"));
                }

                /**
                 * Three things to do here:
                 *
                 * 1. If any bridged entities already exist, find them and use
                 * them. If not, create new ones.
                 *
                 * 2. If any bridged entities were removed by the manager, they
                 * must be removed here first before the book can be edited.
                 *
                 * 3. Use the edited lists, as well as the wrapped fields, to
                 * edit the book.
                 */
                //STEP 1
                List<Bookauthor> allBookauthors = bookauthorController.findBookauthorEntities();
                List<Bookgenre> allBookgenres = bookgenreController.findBookgenreEntities();
                List<Bookformat> allBookformats = bookformatController.findBookformatEntities();

                List<Bookauthor> bookAuthorsToEdit = new ArrayList<>();
                List<Bookgenre> bookGenresToEdit = new ArrayList<>();
                List<Bookformat> bookFormatsToEdit = new ArrayList<>();

                //For each selected entity in the modal, determine whether a 
                //bridging entity exists already. If it does, add it to the list.
                //If not, create it, and add it to the list once complete.
                for (AuthorWrapperBean a : selectedBookAuthors) {
                    Bookauthor ba = new Bookauthor(selectedBook.getBook(), a.getAuthor());
                    Integer dupId = isBookAuthorDuplicate(ba, allBookauthors);
                    if (dupId != null) {
                        Bookauthor existentBa = bookauthorController.findBookauthor(dupId);
                        bookAuthorsToEdit.add(existentBa);
                    } else {
                        bookauthorController.create(ba);
                        bookAuthorsToEdit.add(ba);
                    }
                }
                for (GenreWrapperBean g : selectedBookGenres) {
                    Bookgenre bg = new Bookgenre(selectedBook.getBook(), g.getGenre());
                    Integer dupId = isBookGenreDuplicate(bg, allBookgenres);
                    if (dupId != null) {
                        Bookgenre existentBg = bookgenreController.findBookgenre(dupId);
                        bookGenresToEdit.add(existentBg);
                    } else {
                        bookgenreController.create(bg);
                        bookGenresToEdit.add(bg);
                    }
                }
                for (FormatWrapperBean f : selectedBookFormats) {
                    Bookformat bf = new Bookformat(selectedBook.getBook(), f.getFormat());
                    Integer dupId = isBookFormatDuplicate(bf, allBookformats);
                    if (dupId != null) {
                        Bookformat existentBf = bookformatController.findBookformat(dupId);
                        bookFormatsToEdit.add(existentBf);
                    } else {
                        bookformatController.create(bf);
                        bookFormatsToEdit.add(bf);
                    }
                }

                //STEP 2
                destroyRemovedBridgedEntities(bookAuthorsToEdit, bookGenresToEdit, bookFormatsToEdit);

                //STEP 3
                selectedBook.getBook().setBookauthorList(bookAuthorsToEdit);
                selectedBook.getBook().setBookgenreList(bookGenresToEdit);
                selectedBook.getBook().setBookformatList(bookFormatsToEdit);
                selectedBook.getBook().setNumPages(selectedBookNumPages);
                selectedBook.getBook().setMinAge(selectedBookMinAge);

                booksController.edit(selectedBook.getBook());
                msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("Success"), Messages.getString("SuccessfulEditMsg"));
                resetMainLists();
            } catch (NonexistentEntityException | SecurityException | IllegalOrphanException
                    | NotSupportedException | RollbackException | SystemException
                    | HeuristicMixedException | HeuristicRollbackException e) {
                LOG.error(selectedBook.getBook().getIsbn() + " cannot be edited", e);
                msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("EditError"));
            } catch (IOException ex) {
                LOG.error("Image could not be uploaded", ex);
                msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("Error"), Messages.getString("EditError"));
            }

            selectedBook = new BookWrapperBean(new Books());
            fc.addMessage(null, msg);
            redirectToInventory(fc);
        }
    }

    /**
     * Redirects to inventory page.
     *
     * @param fc
     */
    private void redirectToInventory(FacesContext fc) {
        ExternalContext ec = fc.getExternalContext();
        ec.getFlash().setKeepMessages(true);
        try {
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        } catch (IOException ex) {
            LoggerFactory.getLogger(InventoryBackingBean.class).error("Could not redirect to inventory.", ex);
        }
    }

    /**
     * Verifies whether two duplicate entities were submitted, such as two
     * authors being exactly the same for the given book.
     *
     * @param authors
     * @param genres
     * @param formats
     * @return
     */
    private boolean hasDuplicateEntity(ArrayList<AuthorWrapperBean> authors,
            ArrayList<GenreWrapperBean> genres, ArrayList<FormatWrapperBean> formats) {
        Set<AuthorWrapperBean> setAuthors = new HashSet<>();
        Set<GenreWrapperBean> setGenres = new HashSet<>();
        Set<FormatWrapperBean> setFormats = new HashSet<>();

        // Set#add returns false if the set does not change, which
        // indicates that a duplicate element has been added.
        if (authors.stream().anyMatch((each) -> (!setAuthors.add(each)))) {
            return true;
        }
        if (genres.stream().anyMatch((each) -> (!setGenres.add(each)))) {
            return true;
        }
        if (formats.stream().anyMatch((each) -> (!setFormats.add(each)))) {
            return true;
        }
        return false;
    }

    /**
     * Verifies whether a duplicate bookauthor exists in the database.
     *
     * @param bookauthor
     * @param allBookauthor
     * @return the id of the bookauthor duplicate, or null if there is none
     */
    private Integer isBookAuthorDuplicate(Bookauthor bookauthor, List<Bookauthor> allBookauthor) {
        for (Bookauthor bg : allBookauthor) {
            if (bookauthor.getAuthorId().equals(bg.getAuthorId()) && bookauthor.getIsbn().equals(bg.getIsbn())) {
                return bg.getBookauthorId();
            }
        }
        return null;
    }

    /**
     * Verifies whether a duplicate bookgenre exists in the database.
     *
     * @param bookgenre
     * @param allBookgenre
     * @return the id of the bookgenre duplicate, or null if there is none
     */
    private Integer isBookGenreDuplicate(Bookgenre bookgenre, List<Bookgenre> allBookgenre) {
        for (Bookgenre bg : allBookgenre) {
            if (bookgenre.getGenreId().equals(bg.getGenreId()) && bookgenre.getIsbn().equals(bg.getIsbn())) {
                return bg.getBookgenreId();
            }
        }
        return null;
    }

    /**
     * Verifies whether a duplicate bookformat exists in the database.
     *
     * @param bookformat
     * @param allBookformat
     * @return the id of the bookformat duplicate, or null if there is none
     */
    private Integer isBookFormatDuplicate(Bookformat bookformat, List<Bookformat> allBookformat) {
        for (Bookformat bg : allBookformat) {
            if (bookformat.getFormatId().equals(bg.getFormatId()) && bookformat.getIsbn().equals(bg.getIsbn())) {
                return bg.getBookformatId();
            }
        }
        return null;
    }

    /**
     * Get all the bridging entities from the original book. Then, compare them
     * with the toEdit lists and remove the ones that are duplicates. This way,
     * the only ones left will be the removed ones.
     *
     * @param bookAuthorsToEdit
     * @param bookGenresToEdit
     * @param bookFormatsToEdit
     */
    private void destroyRemovedBridgedEntities(List<Bookauthor> bookAuthorsToEdit, List<Bookgenre> bookGenresToEdit, List<Bookformat> bookFormatsToEdit) {
        List<Bookauthor> bookAuthorsToBeRemoved = new ArrayList<>(selectedBook.getBook().getBookauthorList());
        List<Bookgenre> bookGenresToBeRemoved = new ArrayList<>(selectedBook.getBook().getBookgenreList());
        List<Bookformat> bookFormatsToBeRemoved = new ArrayList<>(selectedBook.getBook().getBookformatList());
        bookAuthorsToBeRemoved.removeAll(bookAuthorsToEdit);
        bookGenresToBeRemoved.removeAll(bookGenresToEdit);
        bookFormatsToBeRemoved.removeAll(bookFormatsToEdit);
        try {
            for (Bookauthor ba : bookAuthorsToBeRemoved) {
                bookauthorController.destroy(ba.getBookauthorId());
            }
            for (Bookgenre bg : bookGenresToBeRemoved) {
                bookgenreController.destroy(bg.getBookgenreId());
            }
            for (Bookformat bf : bookFormatsToBeRemoved) {
                bookformatController.destroy(bf.getBookformatId());
            }
        } catch (IllegalOrphanException | NonexistentEntityException
                | SecurityException | NotSupportedException | RollbackException
                | SystemException | HeuristicMixedException
                | HeuristicRollbackException illegalOrphanException) {
            LOG.error(illegalOrphanException.getMessage());
        }
    }
}
