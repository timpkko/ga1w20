package com.teama1.ga1w20.business.backingbeans.manager.rssfeed;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Timmy
 */
@Named("rssSearchBacking")
@RequestScoped
public class RssSearchBackingBean implements Serializable {

    private boolean feedId = true;
    private boolean url = true;
    private boolean active = true;

    public boolean isFeedId() {
        return feedId;
    }

    public void setFeedId(boolean feedId) {
        this.feedId = feedId;
    }

    public boolean isUrl() {
        return url;
    }

    public void setUrl(boolean url) {
        this.url = url;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
