package com.teama1.ga1w20.business.backingbeans.manager.ads;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.business.utils.UploadImageUtils;
import com.teama1.ga1w20.persistence.entities.Banners;
import com.teama1.ga1w20.persistence.jpacontrollers.BannersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.PrimeFaces;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This backing bean is used for managing the banner ads.
 *
 * @author Timmy
 */
@Named("adsManagementBacking")
@ViewScoped
public class AdsManagementBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(AdsManagementBackingBean.class);

    @Inject
    private BannersJpaController bannersController;

    private List<Banners> ads;
    private List<Banners> filteredAds;

    @Inject
    private AdsSearchBackingBean adsSearchBean;

    private Banners selectedAd;

    private UploadedFile selectedImage;

    public UploadedFile getSelectedImage() {
        return selectedImage;
    }

    public void setSelectedImage(UploadedFile selectedImage) {
        this.selectedImage = selectedImage;
    }

    public List<Banners> getAds() {
        this.ads = bannersController.findBannersEntities();
        return ads;
    }

    public List<Banners> getFilteredAds() {
        return filteredAds;
    }

    public void setFilteredAds(List<Banners> filteredAds) {
        this.filteredAds = filteredAds;
    }

    public Banners getSelectedAd() {
        return selectedAd;
    }

    public void setSelectedAd(Banners selectedAd) {
        this.selectedAd = selectedAd;
    }

    /**
     * Resets the dialog form inputs for the next ad.
     *
     * @author Michael
     * @param formId
     */
    public void resetDialogForm(String formId) {
        PrimeFaces.current().resetInputs(formId);
    }

    public void checkAllCheckboxes() {
        adsSearchBean.setAdId(true);
        adsSearchBean.setUrl(true);
        adsSearchBean.setActive(true);
    }

    public void uncheckAllCheckboxes() {
        adsSearchBean.setAdId(false);
        adsSearchBean.setUrl(false);
        adsSearchBean.setActive(false);
    }

    /**
     * https://www.primefaces.org/showcase/ui/data/datatable/filter.xhtml
     *
     * @param value
     * @param filter
     * @param locale
     * @return
     */
    public boolean globalFitlerFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (filterText == null || filterText.equals("")) {
            return true;
        }

        Banners ad = (Banners) value;

        return getCriteria(ad, filterText);
    }

    private boolean getCriteria(Banners ad, String filterText) {
        boolean adId = false;
        boolean url = false;
        boolean active = false;

        if (adsSearchBean.isAdId()) {
            adId = ad.getAdId() == getInteger(filterText);
        }
        if (adsSearchBean.isUrl()) {
            url = ad.getUrl().toLowerCase().contains(filterText);
        }
        if (adsSearchBean.isActive()) {
            active = "active".contains(filterText);
        }

        return adId || url || active;
    }

    private int getInteger(String string) {
        try {
            return Integer.valueOf(string);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public void saveChanges() {
        FacesMessage msg;
        try {
            bannersController.edit(selectedAd);

            if (selectedImage != null) {
                UploadImageUtils imageUtil = new UploadImageUtils(selectedImage);
                imageUtil.saveToDisk(Constants.BANNERS_PATH, selectedAd.getAdId().toString());
            }

            msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    Messages.getString("Success"), Messages.getString("SuccessfulEditMsg"));
        } catch (IllegalOrphanException
                | NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException e) {
            LOG.error(selectedAd.getAdId() + " cannot be edited", e);
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    Messages.getString("Error"), Messages.getString("EditError"));
        } catch (IOException ex) {
            LOG.error("Cannot save the image on disk:" + ex);
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    Messages.getString("Error"), Messages.getString("EditError"));
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @param bannerId Id associated to a banner
     * @return The URL of the image associated to the banner id
     * @author Camillia
     */
    public String getImageUrlAssociatedToBannerId(int bannerId) {
        return Constants.BANNERS_SHORT_PATH + bannerId + Constants.IMAGES_EXTENSION;
    }
}
