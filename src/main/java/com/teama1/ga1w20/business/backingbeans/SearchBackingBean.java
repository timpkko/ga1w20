package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.business.beans.SearchResultsBean;
import com.teama1.ga1w20.business.search.SearchUtil;
import com.teama1.ga1w20.business.search.SortBySearch;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages the retrieval, filtering, and display of search results.
 *
 * @author Eira Garrett, Timmy
 */
@Named("searchBackingBean")
@SessionScoped
public class SearchBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SearchBackingBean.class);

    @Inject
    private FormatsJpaController formatsJpaController;

    @Inject
    private SearchUtil searchUtil;

    private String searchTerms;
    private List<SearchResultsBean> results;
    private List<SearchResultsBean> backupResults;

    //Determines sort criterion
    private SortBySearch selectedSortCriterion;

    //Determines sort order
    private Boolean sortReversed;

    // Advanced search
    private String titleSearch;
    private String authorSearch;
    private String publisherSearch;
    private String genreSearch;
    private String isbnSearch;
    private String formatsSearch;

    // Formats
    private List<String> formatsList;
    private String selectedFormats;

    /**
     * A default constructor -- initializes empty search terms and an empty
     * ArrayList of Book objects.
     */
    @PostConstruct
    public void init() {
        searchTerms = "";
        results = new ArrayList<>();
        backupResults = new ArrayList<>();
        sortReversed = false;
        titleSearch = "";
        authorSearch = "";
        publisherSearch = "";
        genreSearch = "";
        isbnSearch = "";
        formatsList = convertFormatsToListString();
        selectedFormats = String.join(",", formatsList);
    }

    public SortBySearch getSelectedSortCriterion() {
        return selectedSortCriterion;
    }

    public void setSelectedSortCriterion(SortBySearch selectedSortCriterion) {
        this.selectedSortCriterion = selectedSortCriterion;
    }

    public void setSearchTerms(String terms) {
        searchTerms = terms;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public void setResults(ArrayList<SearchResultsBean> resultList) {
        results = resultList;
    }

    public List<SearchResultsBean> getResults() {
        return results;
    }

    public Boolean getSortReversed() {
        return sortReversed;
    }

    public void setSortReversed(Boolean sortReversed) {
        this.sortReversed = sortReversed;
    }

    public String getTitleSearch() {
        return titleSearch;
    }

    public void setTitleSearch(String titleSearch) {
        this.titleSearch = titleSearch;
    }

    public String getAuthorSearch() {
        return authorSearch;
    }

    public void setAuthorSearch(String authorSearch) {
        this.authorSearch = authorSearch;
    }

    public String getPublisherSearch() {
        return publisherSearch;
    }

    public void setPublisherSearch(String publisherSearch) {
        this.publisherSearch = publisherSearch;
    }

    public String getGenreSearch() {
        return genreSearch;
    }

    public void setGenreSearch(String genreSearch) {
        this.genreSearch = genreSearch;
    }

    public String getIsbnSearch() {
        return isbnSearch;
    }

    public void setIsbnSearch(String isbnSearch) {
        this.isbnSearch = isbnSearch;
    }

    public String getFormatsSearch() {
        return formatsSearch;
    }

    public void setFormatsSearch(String formatsSearch) {
        this.formatsSearch = formatsSearch;
    }

    public String getSelectedFormats() {
        return selectedFormats;
    }

    public List<String> getFormatsList() {
        return formatsList;
    }

    public void setSelectedFormats(String selectedFormats) {
        this.selectedFormats = selectedFormats;
    }

    /**
     * Searches books using the advanced search form.
     *
     * @return
     */
    public String refineSearchPage() {
        searchBooksWithCriteria();
        return goToSearchOrBookPage();
    }

    /**
     * Searches the books by searching through all the fields.
     *
     * @return
     */
    public String goToSearchPage() {
        searchTerms = (searchTerms == null) ? "" : searchTerms;
        searchForBooks();
        return goToSearchOrBookPage();
    }

    /**
     * Redirects the user depending on the search results. If there are multiple
     * results, it redirects them to the search page. If there is only one
     * result, it redirects them to the book page of the result.
     *
     * @return The URL of the page
     */
    private String goToSearchOrBookPage() {
        if (results.size() == 1) {
            SearchResultsBean firstResult = results.get(0);
            return String.format("/book.xhtml?isbn=%d&faces-redirect=true", firstResult.getIsbn());
        }

        return "/search.xhtml&faces-redirect=true";
    }

    /**
     * Searches for books based on the search terms. It does a global search
     * through the titles, authors, publishers, genres, and ISBN-13. This is
     * called from the search bar in the navbar.
     *
     */
    private void searchForBooks() {
        searchTerms = searchTerms.trim();
        searchTerms = (searchTerms == null) ? "" : searchTerms;

        Set<SearchResultsBean> resultsSet = new HashSet<>();
        resultsSet.addAll(searchUtil.fetchBooksByTitle(searchTerms));
        resultsSet.addAll(searchUtil.fetchBooksByAuthor(searchTerms));
        resultsSet.addAll(searchUtil.fetchBooksByPublisher(searchTerms));
        resultsSet.addAll(searchUtil.fetchBooksByGenre(searchTerms));
        // Get the book by isbn if the search terms have numbers
        try {
            long isbn = Long.parseLong(searchTerms);
            resultsSet.addAll(searchUtil.fetchBooksByIsbn(isbn));
        } catch (NumberFormatException e) {
            LOG.debug("No numbers in search terms, skipping fetchBooksByIsbn");
        }

        addBooksToResults(resultsSet);

        backupResultsList();
    }

    /**
     * Searches for books based on the inputs in the specific search fields. The
     * current specific fields are title, author, publisher, genre and ISBN.
     */
    private void searchBooksWithCriteria() {
        Set<SearchResultsBean> resultsSet = new HashSet<>();

        // For every search field that is not empty, search for books from their field
        if (!(titleSearch == null || titleSearch.isBlank())) {
            resultsSet.addAll(searchUtil.fetchBooksByTitle(titleSearch));
        }
        if (!(authorSearch == null || authorSearch.isBlank())) {
            resultsSet.addAll(searchUtil.fetchBooksByAuthor(authorSearch));
        }
        if (!(publisherSearch == null || publisherSearch.isBlank())) {
            resultsSet.addAll(searchUtil.fetchBooksByPublisher(publisherSearch));
        }
        if (!(genreSearch == null || genreSearch.isBlank())) {
            resultsSet.addAll(searchUtil.fetchBooksByGenre(genreSearch));
        }
        if (!(isbnSearch == null || isbnSearch.isBlank())) {
            // Get the book by isbn if the search terms have numbers
            try {
                long isbn = Long.parseLong(isbnSearch);
                resultsSet.addAll(searchUtil.fetchBooksByIsbn(isbn));
            } catch (NumberFormatException e) {
                LOG.debug("No numbers in search terms, skipping fetchBooksByIsbn");
            }
        }

        if (resultsSet.isEmpty()) {
            resultsSet.addAll(searchUtil.fetchBooksByTitle(" "));
        }

        addBooksToResults(resultsSet);

        backupResultsList();
    }

    /**
     * Converts a list of Formats to a list of String. This is necessary when
     * using BootsFaces's selectMultiMenu UI component, as it accepts only
     * Strings. The string is then used as the item value.
     *
     * Output of the string: "format_name (format_extension)"
     *
     * @return
     * @author Timmy
     */
    private List<String> convertFormatsToListString() {
        List<String> formatsInString = new ArrayList<>();

        for (Formats format : formatsJpaController.findFormatsEntities()) {
            formatsInString.add(String.format("%s (%s)", format.getName(), format.getExtension()));
        }

        return formatsInString;
    }

    /**
     * Updates the list the results based on the selected formats.
     *
     * @author Timmy
     */
    public void filterBooksByFormats() {

        // BootsFaces's selectMultiMenu outputs as a comma-seperated list
        String[] selectedFormatsList = selectedFormats.split(",");

        // Clear the results before filtering
        results.clear();

        // Add to results the results that match the selected formats
        for (SearchResultsBean result : backupResults) {
            for (String formatId : selectedFormatsList) {
                if (result.getFormats().contains(formatId)) {
                    results.add(result);
                    break;
                }
            }
        }
    }

    /**
     * Sorts the search result list based on the item value of the radio buttons
     * below the Sort By label.
     *
     * Ability to sort by - Title - Author - Genre - Publisher - ISBN-13
     *
     * Notes: A book may have multiple authors and multiple genres, therefore
     * the items are sorted by the first author/genre from their list.
     *
     * @author Timmy
     */
    public void sortBooksByCriteria() {
        switch (this.selectedSortCriterion) {
            case TITLE:
                if (sortReversed) {
                    results.sort(Comparator.comparing(SearchResultsBean::getTitle).reversed());
                } else {
                    results.sort(Comparator.comparing(SearchResultsBean::getTitle));
                }
                break;
            case AUTHOR:
                results.sort((result1, result2) -> {
                    String author1 = result1.getAuthors().get(0);
                    String author2 = result2.getAuthors().get(0);
                    if (sortReversed) {
                        return author2.compareToIgnoreCase(author1);
                    } else {
                        return author1.compareToIgnoreCase(author2);
                    }
                });
                break;
            case GENRE:
                results.sort((result1, result2) -> {
                    String genre1 = result1.getGenres().get(0);
                    String genre2 = result2.getGenres().get(0);
                    if (sortReversed) {
                        return genre2.compareToIgnoreCase(genre1);
                    } else {
                        return genre1.compareToIgnoreCase(genre2);
                    }
                });
                break;
            case PUBLISHER:
                if (sortReversed) {
                    results.sort(Comparator.comparing(SearchResultsBean::getPublisher).reversed());
                } else {
                    results.sort(Comparator.comparing(SearchResultsBean::getPublisher));
                }
                break;
            case ISBN:
                if (sortReversed) {
                    results.sort(Comparator.comparing(SearchResultsBean::getIsbn).reversed());
                } else {
                    results.sort(Comparator.comparing(SearchResultsBean::getIsbn));
                }
                break;
            default:
                break;
        }
    }

    /**
     * Retrieves the values of the SortBySearch enum. Used to list the options
     * for sorting.
     *
     * @return Array of SortBySearch
     */
    public SortBySearch[] getSortByValues() {
        return SortBySearch.values();
    }

    private void addBooksToResults(Set<SearchResultsBean> resultsSet) {
        results.clear();
        results.addAll(resultsSet);
    }

    /**
     * Clears the backup list of results and backup the current list.
     */
    private void backupResultsList() {
        backupResults = List.copyOf(results);
    }
}
