package com.teama1.ga1w20.business.backingbeans.manager.newbook;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.business.backingbeans.manager.inventory.InventoryBackingBean;
import com.teama1.ga1w20.business.beans.manager.AuthorWrapperBean;
import com.teama1.ga1w20.business.beans.manager.FormatWrapperBean;
import com.teama1.ga1w20.business.beans.manager.GenreWrapperBean;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.business.utils.UploadImageUtils;
import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Bookauthor;
import com.teama1.ga1w20.persistence.entities.Bookformat;
import com.teama1.ga1w20.persistence.entities.Bookgenre;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.entities.Publishers;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BookauthorJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BookformatJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BookgenreJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.FormatsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.GenresJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.PublishersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This backing bean serves to create a new book, as well as new authors,
 * publishers, genres and formats if needed.
 *
 * @author Michael
 */
@Named("newBookBacking")
@ViewScoped
public class NewBookBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(NewBookBackingBean.class);

    @Inject
    private BooksJpaController booksController;
    @Inject
    private AuthorsJpaController authorsController;
    @Inject
    private BookauthorJpaController bookauthorController;
    @Inject
    private PublishersJpaController publishersController;
    @Inject
    private GenresJpaController genresController;
    @Inject
    private BookgenreJpaController bookgenreController;
    @Inject
    private FormatsJpaController formatsController;
    @Inject
    private BookformatJpaController bookformatController;

    private final Date today = new Date();

    private List<Books> allBooks;
    private Books book;
    private Integer bookNumPages;
    private BigDecimal bookSalePrice;
    private Integer bookMinAge;
    private UploadedFile bookCoverImage;

    private List<Authors> allAuthors;
    private Authors selectedAuthor;
    private ArrayList<AuthorWrapperBean> authors;

    private boolean publisherExistent;
    private List<Publishers> allPublishers;
    private Publishers selectedPublisher;
    private Publishers newPublisher;

    private List<Genres> allGenres;
    private Genres selectedGenre;
    private ArrayList<GenreWrapperBean> genres;

    private List<Formats> allFormats;
    private Formats selectedFormat;
    private ArrayList<FormatWrapperBean> formats;

    /**
     * Sets up lists for the xhtml page and default parameters.
     */
    @PostConstruct
    public void init() {
        //BOOK
        allBooks = booksController.findBooksEntities();
        book = new Books();
        book.setAcquiredStamp(new Date());
        book.setRemoved(false);

        resetMainLists();

        //AUTHOR
        selectedAuthor = allAuthors.get(0);
        authors = new ArrayList<>();
        authors.add(new AuthorWrapperBean(new Authors(), true));

        //PUBLISHER
        publisherExistent = true;
        selectedPublisher = allPublishers.get(0);
        newPublisher = new Publishers();

        //GENRE
        selectedGenre = allGenres.get(0);
        genres = new ArrayList<>();
        genres.add(new GenreWrapperBean(new Genres(), true));

        //FORMAT
        selectedFormat = allFormats.get(0);
        formats = new ArrayList<>();
        formats.add(new FormatWrapperBean(new Formats(), true));
    }

    /**
     * Creates all needed entities for the creation of the book, and then the
     * book itself.
     */
    public void createBook() {
        FacesMessage msg;
        FacesContext fc = FacesContext.getCurrentInstance();

        //Deal with duplicate entities first
        if (hasDuplicateEntity(authors, genres, formats)) {
            book = new Books();
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("DuplicateEntityBook_Create"), Messages.getString("EditError"));
            fc.addMessage(null, msg);
            redirectToInventory(fc);
        } else {
            if (bookCoverImage == null) {
                Messages.addMessageToContext(FacesMessage.SEVERITY_INFO, "Error", "ImageRequiredMsg");
                LOG.error("The image could not be uploaded.");
                return;
            }
            checkNullableFields();

            try {
                //AUTHOR
                ArrayList<Bookauthor> bookauthorList = new ArrayList<>();
                for (AuthorWrapperBean author : authors) {
                    if (!author.isAuthorExistent()) {
                        author.getAuthor().setBookauthorList(null);
                        authorsController.create(author.getAuthor());
                    }
                    bookauthorList.add(new Bookauthor(book, author.getAuthor()));
                }

                //PUBLISHER
                if (publisherExistent == true) {
                    book.setPublisherId(selectedPublisher);
                } else {
                    publishersController.create(newPublisher);
                    book.setPublisherId(newPublisher);
                }

                //GENRE
                ArrayList<Bookgenre> bookgenreList = new ArrayList<>();
                for (GenreWrapperBean genre : genres) {
                    if (!genre.isGenreExistent()) {
                        genre.getGenre().setBookgenreList(null);
                        genresController.create(genre.getGenre());
                    }
                    bookgenreList.add(new Bookgenre(book, genre.getGenre()));
                }

                //FORMAT
                ArrayList<Bookformat> bookformatList = new ArrayList<>();
                for (FormatWrapperBean format : formats) {
                    if (!format.isFormatExistent()) {
                        format.getFormat().setBookformatList(null);
                        formatsController.create(format.getFormat());
                    }
                    bookformatList.add(new Bookformat(book, format.getFormat()));
                }

                book.setNumPages(bookNumPages);
                book.setMinAge(bookMinAge);
                booksController.create(book);

                //BRIDGING
                for (Bookauthor ba : bookauthorList) {
                    bookauthorController.create(ba);
                }
                for (Bookgenre bg : bookgenreList) {
                    bookgenreController.create(bg);
                }
                for (Bookformat bf : bookformatList) {
                    bookformatController.create(bf);
                }
                book.setBookauthorList(bookauthorList);
                book.setBookgenreList(bookgenreList);
                book.setBookformatList(bookformatList);
                booksController.edit(book);

                //Deal with images only after the book was properly entered
                try {
                    UploadImageUtils coverImageUtil = new UploadImageUtils(bookCoverImage);
                    coverImageUtil.saveToDisk(Constants.BOOK_COVERS_PATH, book.getIsbn().toString());
                    BufferedImage thumbnailImageUtil = coverImageUtil.resizeImage(Constants.BOOK_THUMBNAIL_HEIGHT);
                    UploadImageUtils.saveToDisk(thumbnailImageUtil, Constants.BOOK_THUMBS_PATH, book.getIsbn().toString().concat("_thumb"));
                } catch (IOException ex) {
                    LOG.error("One or both images could not be uploaded.", ex);
                }

                fc.addMessage("create", new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateBooksCreated}", String.class)));
                redirectToInventory(fc);
            } catch (RollbackFailureException | IllegalStateException
                    | SecurityException | NotSupportedException | RollbackException
                    | SystemException | HeuristicMixedException
                    | HeuristicRollbackException | IllegalOrphanException
                    | NonexistentEntityException ex) {
                LOG.error("Could not create book.", ex);
            }
        }
    }

    /**
     * Redirects to inventory page.
     *
     * @param fc
     */
    private void redirectToInventory(FacesContext fc) {
        ExternalContext ec = fc.getExternalContext();
        ec.getFlash().setKeepMessages(true);
        try {
            ec.redirect("../inventory.xhtml");
        } catch (IOException ex) {
            LoggerFactory.getLogger(InventoryBackingBean.class).error("Could not redirect to inventory.", ex);
        }
    }

    /**
     * Verifies whether two duplicate entities were submitted, such as two
     * authors being exactly the same for the given book.
     *
     * @param authors
     * @param genres
     * @param formats
     * @return
     */
    private boolean hasDuplicateEntity(ArrayList<AuthorWrapperBean> authors,
            ArrayList<GenreWrapperBean> genres, ArrayList<FormatWrapperBean> formats) {
        Set<AuthorWrapperBean> setAuthors = new HashSet<>();
        Set<GenreWrapperBean> setGenres = new HashSet<>();
        Set<FormatWrapperBean> setFormats = new HashSet<>();

        // Set#add returns false if the set does not change, which
        // indicates that a duplicate element has been added.
        if (authors.stream().anyMatch((each) -> (!setAuthors.add(each)))) {
            return true;
        }
        if (genres.stream().anyMatch((each) -> (!setGenres.add(each)))) {
            return true;
        }
        if (formats.stream().anyMatch((each) -> (!setFormats.add(each)))) {
            return true;
        }
        return false;
    }

    /////////////////////
    ////////BOOK/////////
    /////////////////////
    public List<Books> getAllBooks() {
        return allBooks;
    }

    public void setAllBooks(List<Books> allBooks) {
        this.allBooks = allBooks;
    }

    public Books getBook() {
        return book;
    }

    public void setBook(Books book) {
        this.book = book;
    }

    public Integer getBookNumPages() {
        return bookNumPages;
    }

    public void setBookNumPages(Integer bookNumPages) {
        this.bookNumPages = bookNumPages;
    }

    public BigDecimal getBookSalePrice() {
        return bookSalePrice;
    }

    public void setBookSalePrice(BigDecimal bookSalePrice) {
        this.bookSalePrice = bookSalePrice;
    }

    public Integer getBookMinAge() {
        return bookMinAge;
    }

    public void setBookMinAge(Integer bookMinAge) {
        this.bookMinAge = bookMinAge;
    }

    public UploadedFile getBookCoverImage() {
        return bookCoverImage;
    }

    public void setBookCoverImage(UploadedFile bookCoverImage) {
        this.bookCoverImage = bookCoverImage;
    }

    public Date getToday() {
        return today;
    }

    /////////////////////
    ///////AUTHOR////////
    /////////////////////
    public List<Authors> getAllAuthors() {
        return allAuthors;
    }

    public void setAllAuthors(List<Authors> allAuthors) {
        this.allAuthors = allAuthors;
    }

    public Authors getSelectedAuthor() {
        return selectedAuthor;
    }

    public void setSelectedAuthor(Authors selectedAuthor) {
        this.selectedAuthor = selectedAuthor;
    }

    public ArrayList<AuthorWrapperBean> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<AuthorWrapperBean> authors) {
        this.authors = authors;
    }

    public void addAuthorWrapperBeanToList() {
        resetMainLists();
        authors.add(new AuthorWrapperBean(new Authors(), true));
    }

    public void removeAuthorWrapperBeanFromList(AuthorWrapperBean bean) {
        authors.remove(bean);
    }

    /////////////////////
    //////PUBLISHER//////
    /////////////////////
    public boolean isPublisherExistent() {
        return publisherExistent;
    }

    public void setPublisherExistent(boolean publisherExistent) {
        this.publisherExistent = publisherExistent;
    }

    public List<Publishers> getAllPublishers() {
        return allPublishers;
    }

    public void setAllPublishers(List<Publishers> allPublishers) {
        this.allPublishers = allPublishers;
    }

    public Publishers getSelectedPublisher() {
        return selectedPublisher;
    }

    public void setSelectedPublisher(Publishers selectedPublisher) {
        this.selectedPublisher = selectedPublisher;
    }

    public Publishers getNewPublisher() {
        return newPublisher;
    }

    public void setNewPublisher(Publishers newPublisher) {
        this.newPublisher = newPublisher;
    }

    /////////////////////
    ////////GENRE////////
    /////////////////////
    public List<Genres> getAllGenres() {
        return allGenres;
    }

    public void setAllGenres(List<Genres> allGenres) {
        this.allGenres = allGenres;
    }

    public Genres getSelectedGenre() {
        return selectedGenre;
    }

    public void setSelectedGenre(Genres selectedGenre) {
        this.selectedGenre = selectedGenre;
    }

    public ArrayList<GenreWrapperBean> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<GenreWrapperBean> genres) {
        this.genres = genres;
    }

    public void addGenreWrapperBeanToList() {
        resetMainLists();
        genres.add(new GenreWrapperBean(new Genres(), true));
    }

    public void removeGenreWrapperBeanFromList(GenreWrapperBean bean) {
        genres.remove(bean);
    }

    /////////////////////
    ///////FORMAT////////
    /////////////////////
    public List<Formats> getAllFormats() {
        return allFormats;
    }

    public void setAllFormats(List<Formats> allFormats) {
        this.allFormats = allFormats;
    }

    public Formats getSelectedFormat() {
        return selectedFormat;
    }

    public void setSelectedFormat(Formats selectedFormat) {
        this.selectedFormat = selectedFormat;
    }

    public ArrayList<FormatWrapperBean> getFormats() {
        return formats;
    }

    public void setFormats(ArrayList<FormatWrapperBean> formats) {
        this.formats = formats;
    }

    public void addFormatWrapperBeanToList() {
        resetMainLists();
        formats.add(new FormatWrapperBean(new Formats(), true));
    }

    public void removeFormatWrapperBeanFromList(FormatWrapperBean bean) {
        formats.remove(bean);
    }

    /////////////////////
    ////////MISC/////////
    /////////////////////
    /**
     * Sorts and retrieves all needed entities from the database.
     *
     * When running ajax in the html of this page, the author, publisher, genre
     * and format lists get modified. All this method does is reset those lists
     * so that everything is still the same on partial submit.
     */
    private void resetMainLists() {
        //AUTHOR
        allAuthors = authorsController.findAuthorsEntities();
        Collections.sort(allAuthors, (Authors a1, Authors a2) -> {
            if (a1.getLastname() == null || a2.getLastname() == null) {
                if (a1.getLastname() == null && a2.getLastname() == null) {
                    return a1.getFirstname().compareToIgnoreCase(a2.getFirstname());
                } else if (a1.getLastname() == null) {
                    return a1.getFirstname().compareToIgnoreCase(a2.getLastname());
                } else {
                    return a1.getLastname().compareToIgnoreCase(a2.getFirstname());
                }
            } else {
                return a1.getLastname().compareToIgnoreCase(a2.getLastname());
            }
        });

        //PUBLISHER
        allPublishers = publishersController.findPublishersEntities();
        Collections.sort(allPublishers, (Publishers p1, Publishers p2) -> {
            return p1.getName().compareToIgnoreCase(p2.getName());
        });

        //GENRE
        allGenres = genresController.findGenresEntities();
        Collections.sort(allGenres, (Genres g1, Genres g2) -> {
            return g1.getName().compareToIgnoreCase(g2.getName());
        });

        //FORMAT
        allFormats = formatsController.findFormatsEntities();
        Collections.sort(allFormats, (Formats f1, Formats f2) -> {
            return f1.getExtension().compareToIgnoreCase(f2.getExtension());
        });
    }

    /**
     * Checks and repairs non-required fields that are left blank when entities
     * are sent to be created.
     *
     * Because of how the submission works in JSF/Primefaces, the entity fields
     * that are left empty and NOT required, such as Last Name for author, are
     * entered as '' (empty spaces) or they give unwanted results. This causes
     * issues when trying to sort, and it is basically bad information.
     */
    private void checkNullableFields() {
        if (book.getSalePrice() == null) {
            book.setSalePrice(BigDecimal.ZERO);
        }
        for (AuthorWrapperBean author : authors) {
            if (!author.isAuthorExistent()) {
                if (author.getAuthor().getLastname().isBlank()) {
                    author.getAuthor().setLastname(null);
                }
                if (author.getAuthor().getTitle().isBlank()) {
                    author.getAuthor().setTitle(null);
                }
            }
        }
    }
}
