package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.persistence.entities.Banners;
import com.teama1.ga1w20.persistence.jpacontrollers.BannersJpaController;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * BannersBackingsBean interacts with the banner ads.
 *
 * @author Timmy, Camillia
 */
@Named
@RequestScoped
public class BannersBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(BannersBackingBean.class);

    @Inject
    private BannersJpaController bannersController;

    // Banner on the left of the page
    private Banners firstBanner;

    // Banner on the right of the page
    private Banners secondBanner;

    private String firstBannerImageURL;

    private String secondBannerImageURL;

    public Banners getFirstBanner() {
        return firstBanner;
    }

    public Banners getSecondBanner() {
        return secondBanner;
    }

    public String getFirstBannerImageURL() {
        return firstBannerImageURL;
    }

    public void setFirstBannerImageURL(String firstBannerImageURL) {
        this.firstBannerImageURL = firstBannerImageURL;
    }

    public String getSecondBannerImageURL() {
        return secondBannerImageURL;
    }

    public void setSecondBannerImageURL(String secondBannerImageURL) {
        this.secondBannerImageURL = secondBannerImageURL;
    }

    /**
     * Initialize the properties with active banners from the database.
     */
    @PostConstruct
    public void init() {
        List<Banners> activeBanners = bannersController.getAllActiveBanners();

        switch (activeBanners.size()) {
            // If there are no active banners, both properties will be empty Banners
            case 0:
                this.firstBanner = null;
                this.secondBanner = null;
                break;
            // If there is only one active banner, both properties will have the same banner
            case 1:
                this.firstBanner = activeBanners.get(0);
                this.firstBannerImageURL = getImageUrlAssociatedToBannerId(this.firstBanner.getAdId());
                this.secondBanner = null;
                break;
            // If there are more than one, first active banner in the list will go to firstBanner; 
            // second active banner will go to secondBanner
            default:
                Collections.shuffle(activeBanners);
                this.firstBanner = activeBanners.get(0);
                this.firstBannerImageURL = getImageUrlAssociatedToBannerId(this.firstBanner.getAdId());
                this.secondBanner = activeBanners.get(1);
                this.secondBannerImageURL = getImageUrlAssociatedToBannerId(this.secondBanner.getAdId());
        }
    }

    /**
     * @param bannerId Id associated to a banner
     * @return The URL of the image associated to the banner id
     * @author Camillia
     */
    private String getImageUrlAssociatedToBannerId(int bannerId) {
        return Constants.BANNERS_SHORT_PATH + bannerId + Constants.IMAGES_EXTENSION;
    }
}
