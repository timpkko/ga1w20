package com.teama1.ga1w20.business.backingbeans.manager;

import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.BooksJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael, Timmy
 */
@Named("booksEditView")
@ViewScoped
public class EditBooksBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(EditBooksBackingBean.class);

    private List<Books> books;

    @Inject
    private BooksJpaController booksController;

    @PostConstruct
    public void init() {
        books = booksController.findBooksEntities();
    }

    public List<Books> getBooks() {
        return books;
    }

    /**
     *
     * @param event
     */
    public void onRowEdit(RowEditEvent event) {
        Books book = ((Books) event.getObject());
        FacesMessage msg;
        try {
            booksController.edit((Books) event.getObject());
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, Messages.getString("BookEditSuccessful"), book.getTitle());
        } catch (IllegalOrphanException
                | NonexistentEntityException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException e) {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, Messages.getString("BookEditUnsuccessful"), book.getTitle());
            LOG.error(book.getTitle() + " cannot be edited", e);
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        Books book = ((Books) event.getObject());
        FacesMessage msg = new FacesMessage("Edit Cancelled", book.getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
