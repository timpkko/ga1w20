package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.business.beans.CartBean;
import com.teama1.ga1w20.business.beans.SessionUserBean;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.OrderitemsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.OrdersJpaController;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for the finalization page
 *
 * @author @FishYeho
 */
@Named("finalizationBackingBean")
@ViewScoped
public class FinalizationBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(FinalizationBackingBean.class);

    @Inject
    private OrdersJpaController ordersController;

    @Inject
    private OrderitemsJpaController orderitemsController;

    @Inject
    private SessionUserBean sessionBean;

    @Inject
    private CartBean cartBean;

    private Orders ordersEntity;
    private Orderitems orderItemsEntity;

    /**
     * Method that instantiates the bean, required for all backing beans
     *
     * @author @FishYeho
     */
    @PostConstruct
    public void init() {
        this.ordersEntity = new Orders();
        this.orderItemsEntity = new Orderitems();
    }

    /**
     * Get method that returns the ordersEntity private field
     *
     * @author @FishYeho
     * @return the private field ordersEntity
     */
    public Orders getOrdersEntity() {
        return this.ordersEntity;
    }

    /**
     * Set method that sets the ordersEntity private field
     *
     * @author @FishYeho
     * @param ordersEntity
     */
    public void setOrdersEntity(Orders ordersEntity) {
        this.ordersEntity = ordersEntity;
    }

    /**
     * Method that enters the order that a user just placed into the database
     * using the OrdersJpaController
     *
     * @author @FishYeho
     * @return A String which redirects the user to the appropriate page
     */
    public String saveInvoiceToDB() {

        //If the user is logged in, we can proceed with placing their order
        if (sessionBean.isUserLoggedIn()) {
            //Retrieve the logged-in user
            Users loggedInUser = sessionBean.getUser();

            //First we need to create an empty order, to get it's orderId
            this.createEmptyOrder(loggedInUser);

            //Then we fetch the new order from the database that needs to be finalized with OrderItems
            Orders orderToFinalize = this.getTheOrderToFinalize(loggedInUser);

            //Now we are ready to create a list of OrderItems
            List<Orderitems> orderitemsList = new ArrayList<>();

            //Get the books from the cart so we can create the OrderItems
            List<Books> booksInTheCart = cartBean.getCart();

            //Create all of the OrderItems and add them to the list
            for (Books book : booksInTheCart) {
                this.orderItemsEntity.setOrderId(orderToFinalize);
                this.orderItemsEntity.setIsbn(book);
                this.orderItemsEntity.setPrice(book.getSalePrice() != BigDecimal.ZERO ? book.getSalePrice() : book.getListPrice());
                this.orderItemsEntity.setRemoved(false);

                //Create the OrderItem
                try {
                    this.orderitemsController.create(orderItemsEntity);
                } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                    //If an error occurs, we keep the user on the Finalization page and display an error message
                    FacesContext.getCurrentInstance().addMessage("growlMsg", new FacesMessage("Error", Messages.getString("SaveInvoiceErrMsg")));
                    return "toFinalization";
                }
                //Add the new OrderItem to the list of OrderItems
                orderitemsList.add(orderItemsEntity);
            }

            //And finally, we can add the list of OrderItems to the Order
            orderToFinalize.setOrderitemsList(orderitemsList);
            return "toInvoice";
        }
        //If the user is not logged in, then they are redirected to the login page
        return "toLogin";
    }

    /**
     * Method that will create an Order for the specified user with an empty
     * OrderItems list. This is done because in order to finalize the Order of a
     * user, we need to use the OrderId of an Order to create an OrderItem due
     * to the primary/foreign key relationship
     *
     * @author FishYeho
     * @param loggedInUser
     */
    public void createEmptyOrder(Users loggedInUser) {
        this.ordersEntity.setUserId(loggedInUser);
        this.ordersEntity.setGstRate(loggedInUser.getTaxrates().getGstRate());
        this.ordersEntity.setPstRate(loggedInUser.getTaxrates().getPstRate());
        this.ordersEntity.setHstRate(loggedInUser.getTaxrates().getHstRate());
        this.ordersEntity.setPurchaseDate(new Date());
        this.ordersEntity.setOrderitemsList(null);

        try {
            this.ordersController.create(ordersEntity);
        } catch (IllegalStateException | SecurityException | NotSupportedException | RollbackException | SystemException | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

    /**
     * Method that will retrieve the empty order that the user just placed that
     * needs to be finalized
     *
     * @author FishYeho
     * @param loggedInUser
     * @return The order that needs to be finalized
     */
    public Orders getTheOrderToFinalize(Users loggedInUser) {
        List<Orders> loggedInUsersOrders = ordersController.getOrdersByUser(loggedInUser);
        Orders orderToFinalize = null;
        for (Orders order : loggedInUsersOrders) {
            if (order.getOrderitemsList().isEmpty()) {
                orderToFinalize = order;
            }
        }
        return orderToFinalize;
    }
}
