package com.teama1.ga1w20.business.backingbeans.manager.newbook;

import com.teama1.ga1w20.persistence.entities.Publishers;
import com.teama1.ga1w20.persistence.jpacontrollers.PublishersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This backing bean serves to create a new publisher.
 *
 * @publisher Michael
 */
@Named("newPublisherBacking")
@ViewScoped
public class NewPublisherBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(NewPublisherBackingBean.class);

    @Inject
    private PublishersJpaController publishersController;

    private List<Publishers> allPublishers;
    private Publishers publisher;

    @PostConstruct
    public void init() {
        allPublishers = publishersController.findPublishersEntities();
        publisher = new Publishers();
    }

    public void createPublisher() {
        try {
            publishersController.create(publisher);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage("create", new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreatePublishersCreated}", String.class)));
            ExternalContext ec = fc.getExternalContext();
            ec.getFlash().setKeepMessages(true);
            try {
                ec.redirect("../inventory.xhtml");
            } catch (IOException ex) {
                LOG.error("Could not redirect to inventory.", ex);
            }
        } catch (RollbackFailureException | IllegalStateException
                | SecurityException | NotSupportedException | RollbackException
                | SystemException | HeuristicMixedException
                | HeuristicRollbackException ex) {
            LOG.error("Could not create publisher.", ex);
        }
    }

    public List<Publishers> getAllPublishers() {
        return allPublishers;
    }

    public void setAllPublishers(List<Publishers> allPublishers) {
        this.allPublishers = allPublishers;
    }

    public Publishers getPublisher() {
        return publisher;
    }

    public void setPublisher(Publishers publisher) {
        this.publisher = publisher;
    }
}
