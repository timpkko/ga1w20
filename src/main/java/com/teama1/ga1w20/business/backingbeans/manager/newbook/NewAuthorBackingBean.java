package com.teama1.ga1w20.business.backingbeans.manager.newbook;

import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This backing bean serves to create a new author.
 *
 * @author Michael
 */
@Named("newAuthorBacking")
@ViewScoped
public class NewAuthorBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(NewAuthorBackingBean.class);

    @Inject
    private AuthorsJpaController authorsController;

    private List<Authors> allAuthors;
    private Authors author;

    @PostConstruct
    public void init() {
        allAuthors = authorsController.findAuthorsEntities();
        author = new Authors();
    }

    public void createAuthor() {
        try {
            checkNullableFields();
            author.setBookauthorList(null);
            authorsController.create(author);
            FacesContext fc = FacesContext.getCurrentInstance();
            fc.addMessage("create", new FacesMessage(fc.getApplication().evaluateExpressionGet(fc, "#{msgs.CreateAuthorsCreated}", String.class)));
            ExternalContext ec = fc.getExternalContext();
            ec.getFlash().setKeepMessages(true);
            try {
                ec.redirect("../inventory.xhtml");
            } catch (IOException ex) {
                LOG.error("Could not redirect to inventory.", ex);
            }
        } catch (IllegalStateException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Could not create author.", ex);
        }
    }

    public List<Authors> getAllAuthors() {
        return allAuthors;
    }

    public void setAllAuthors(List<Authors> allAuthors) {
        this.allAuthors = allAuthors;
    }

    public Authors getAuthor() {
        return author;
    }

    public void setAuthor(Authors author) {
        this.author = author;
    }

    /**
     * Checks and repairs non-required fields that are left blank when entities
     * are sent to be created.
     *
     * Because of how the submission works in JSF/Primefaces, the entity fields
     * that are left empty and NOT required, such as Last Name for author, are
     * entered as '' (empty spaces) or they give unwanted results. This causes
     * issues when trying to sort, and it is basically bad information.
     */
    private void checkNullableFields() {
        if (author.getLastname().isBlank()) {
            author.setLastname(null);
        }
        if (author.getTitle().isBlank()) {
            author.setTitle(null);
        }
    }
}
