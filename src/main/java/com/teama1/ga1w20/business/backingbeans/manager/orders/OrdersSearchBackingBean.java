package com.teama1.ga1w20.business.backingbeans.manager.orders;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Timmy
 */
@Named("ordersSearchBacking")
@RequestScoped
public class OrdersSearchBackingBean implements Serializable {

    private boolean orderitemId = true;
    private boolean orderId = true;
    private boolean userId = true;
    private boolean purchaseDate = true;
    private boolean isbn = true;
    private boolean price = true;
    private boolean gst = true;
    private boolean hst = true;
    private boolean pst = true;

    public boolean isOrderitemId() {
        return orderitemId;
    }

    public void setOrderitemId(boolean orderitemId) {
        this.orderitemId = orderitemId;
    }

    public boolean isOrderId() {
        return orderId;
    }

    public void setOrderId(boolean orderId) {
        this.orderId = orderId;
    }

    public boolean isUserId() {
        return userId;
    }

    public void setUserId(boolean userId) {
        this.userId = userId;
    }

    public boolean isPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(boolean purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public boolean isIsbn() {
        return isbn;
    }

    public void setIsbn(boolean isbn) {
        this.isbn = isbn;
    }

    public boolean isPrice() {
        return price;
    }

    public void setPrice(boolean price) {
        this.price = price;
    }

    public boolean isGst() {
        return gst;
    }

    public void setGst(boolean gst) {
        this.gst = gst;
    }

    public boolean isHst() {
        return hst;
    }

    public void setHst(boolean hst) {
        this.hst = hst;
    }

    public boolean isPst() {
        return pst;
    }

    public void setPst(boolean pst) {
        this.pst = pst;
    }

}
