package com.teama1.ga1w20.business.backingbeans.manager.sales;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Michael
 */
@Named("salesSearchBacking")
@RequestScoped
public class SalesSearchBackingBean implements Serializable {

    private boolean isbn = true;
    private boolean title = true;
    private boolean listPrice = true;
    private boolean salePrice = true;
    private boolean wholesalePrice = true;
    private boolean removed = true;

    public boolean isIsbn() {
        return isbn;
    }

    public void setIsbn(boolean isbn) {
        this.isbn = isbn;
    }

    public boolean isTitle() {
        return title;
    }

    public void setTitle(boolean title) {
        this.title = title;
    }

    public boolean isListPrice() {
        return listPrice;
    }

    public void setListPrice(boolean listPrice) {
        this.listPrice = listPrice;
    }

    public boolean isSalePrice() {
        return salePrice;
    }

    public void setSalePrice(boolean salePrice) {
        this.salePrice = salePrice;
    }

    public boolean isWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(boolean wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
}
