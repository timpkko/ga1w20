package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.business.beans.CartBean;
import com.teama1.ga1w20.business.beans.SessionUserBean;
import com.teama1.ga1w20.business.utils.Messages;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.Transient;
import jodd.mail.Email;
import jodd.mail.MailServer;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for the invoice page
 *
 * @author @FishYeho
 */
@Named("invoiceBackingBean")
@ViewScoped
public class InvoiceBackingBean implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(InvoiceBackingBean.class);

    @Inject
    private UsersJpaController usersController;

    @Inject
    private SessionUserBean sessionBean;

    @Inject
    private CartBean cartBean;

    private String emailSender;
    private String smtpPassword;
    private String smtpServerName;
    private String emailRecipient;
    private Users loggedInUser;

    /**
     * Constructor that retrieves the email credentials from the web.xml file
     * and retrieves the email address of the logged-in user
     *
     * @author @FishYeho
     */
    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();

        emailSender = context.getExternalContext().getInitParameter("emailSender");
        smtpPassword = context.getExternalContext().getInitParameter("smtpPassword");
        smtpServerName = context.getExternalContext().getInitParameter("smtpServerName");
        loggedInUser = sessionBean.getUser();
        emailRecipient = usersController.findUsers(loggedInUser.getUserId()).getEmail();
    }

    /**
     * Method that will send a user's invoice to their email address
     *
     * @author @FishYeho and @OmniProf
     */
    public void sendEmail() {

        // Create am SMTP server object
        SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(smtpServerName)
                .auth(emailSender, smtpPassword)
                //.debugMode(true)
                .buildSmtpMailServer();

        //Initialize the email object
        Email email = null;

        // Using the fluent style of coding create a plain and html text message
        try {
            email = Email.create().from(emailSender)
                    .to(emailRecipient)
                    .subject("Your E-Cubs Invoice")
                    .textMessage("Thank you for your purchase!" + " Here is the invoice from your last order:")
                    .htmlMessage(this.buildHtmlMessage());
        } catch (NonexistentEntityException nee) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", Messages.getString("SendEmailErrMsg")));
            return;
        }

        // Like a file we open the session, send the message and close the
        // session. Session automatically closed by using try-with-resources
        try ( // A session is the object responsible for communicating with the server
                SendMailSession session = smtpServer.createSession()) {
            session.open();
            session.sendMail(email);
            session.close();
            LOG.info("Email sent");
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(Messages.getString("Success"), Messages.getString("EmailSent")));
    }

    /**
     * Method that will build the HTML invoice to be sent to the user
     *
     * @author @FishYeho
     * @throws NonexistentEntityException
     * @return The built HTML string
     */
    private String buildHtmlMessage() throws NonexistentEntityException {
        String billingAddress = cartBean.getBillingAddress();
        String billingCity = cartBean.getBillingCity();
        String billingProvince = cartBean.getBillingProvince();
        String creditCardType = cartBean.getCreditCardType();
        String beforeTax = cartBean.calculateCartTotalBeforeTax().toString();
        String afterTax = cartBean.calculateCartTotalAfterTax().toString();
        String tax = cartBean.calculateTax().toString();

        StringBuilder builder = new StringBuilder();
        builder.append("Thank you for your purchase! ");
        builder.append("Here is your electronic invoice:").append("<br/>").append("<br/>");
        builder.append("Billing Address: ").append(billingAddress).append(", ").append(billingCity).append(", ").append(billingProvince).append("<br/>");
        builder.append("Payment Method: ").append(creditCardType).append("<br/>").append("<br/>");
        builder.append("Order Summary: ").append("<br/>");
        builder.append("Total Before Tax: $").append(beforeTax).append("<br/>");
        builder.append("Estimated Tax: $").append(tax).append("<br/>");
        builder.append("Grand Total: $").append(afterTax);

        return builder.toString();
    }
}
