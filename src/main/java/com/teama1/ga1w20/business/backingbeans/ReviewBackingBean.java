package com.teama1.ga1w20.business.backingbeans;

import com.teama1.ga1w20.business.beans.SessionUserBean;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.jpacontrollers.ReviewsJpaController;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.slf4j.LoggerFactory;

/**
 * ReviewBackingBean is used to retrieve reviews of a specific book. This is
 * used in the book page.
 *
 * @author Timmy-Pierre Keo
 */
@Named("reviewBackingBean")
@RequestScoped
public class ReviewBackingBean implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ReviewBackingBean.class);

    @Inject
    private ReviewsJpaController reviewsController;

    @Inject
    private SessionUserBean sessionBean;

    @Inject
    private BookBackingBean bookBean;

    private String reviewText;
    private Integer rating;

    private boolean successful;

    @PostConstruct
    public void init() {
        rating = 1;
        reviewText = "";
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean succesful) {
        this.successful = succesful;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void saveReview() {
        Reviews newReview = new Reviews();

        newReview.setIsbn(bookBean.getBook());
        newReview.setPostdate(new Date());
        newReview.setApproved(false);
        newReview.setRating(rating);
        newReview.setUserId(sessionBean.getUser());
        newReview.setText(reviewText);

        try {
            reviewsController.create(newReview);
            successful = true;
        } catch (IllegalStateException | SecurityException
                | NotSupportedException | RollbackException | SystemException
                | HeuristicMixedException | HeuristicRollbackException ex) {
            LOG.error("Cannot rollback when trying to add the new review: ", ex);
        }
    }
}
