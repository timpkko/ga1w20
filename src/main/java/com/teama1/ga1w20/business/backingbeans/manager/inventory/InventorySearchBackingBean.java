package com.teama1.ga1w20.business.backingbeans.manager.inventory;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author Michael
 */
@Named("inventorySearchBacking")
@RequestScoped
public class InventorySearchBackingBean implements Serializable {

    private boolean isbn = true;
    private boolean title = true;
    private boolean authors = true;
    private boolean genres = true;
    private boolean publisher = true;
    private boolean pubDate = true;
    private boolean listPrice = true;
    private boolean salePrice = true;
    private boolean wholesalePrice = true;
    private boolean minAge = true;
    private boolean maxAge = true;
    private boolean removed = true;
    private boolean totalSales = true;

    public boolean isIsbn() {
        return isbn;
    }

    public void setIsbn(boolean isbn) {
        this.isbn = isbn;
    }

    public boolean isTitle() {
        return title;
    }

    public void setTitle(boolean title) {
        this.title = title;
    }

    public boolean isAuthors() {
        return authors;
    }

    public void setAuthors(boolean authors) {
        this.authors = authors;
    }

    public boolean isGenres() {
        return genres;
    }

    public void setGenres(boolean genres) {
        this.genres = genres;
    }

    public boolean isPublisher() {
        return publisher;
    }

    public void setPublisher(boolean publisher) {
        this.publisher = publisher;
    }

    public boolean isPubDate() {
        return pubDate;
    }

    public void setPubDate(boolean pubDate) {
        this.pubDate = pubDate;
    }

    public boolean isListPrice() {
        return listPrice;
    }

    public void setListPrice(boolean listPrice) {
        this.listPrice = listPrice;
    }

    public boolean isSalePrice() {
        return salePrice;
    }

    public void setSalePrice(boolean salePrice) {
        this.salePrice = salePrice;
    }

    public boolean isWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(boolean wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public boolean isMinAge() {
        return minAge;
    }

    public void setMinAge(boolean minAge) {
        this.minAge = minAge;
    }

    public boolean isMaxAge() {
        return maxAge;
    }

    public void setMaxAge(boolean maxAge) {
        this.maxAge = maxAge;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isTotalSales() {
        return totalSales;
    }

    public void setTotalSales(boolean totalSales) {
        this.totalSales = totalSales;
    }
}
