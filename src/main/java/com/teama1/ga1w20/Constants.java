package com.teama1.ga1w20;

public class Constants {

    private Constants() {
    }

    /**
     * Number of decimals to consider when handling prices (associated to Books
     * and Orders by example)
     *
     */
    public static final int PRICE_SCALE = 2;

    /**
     * Number of decimals to consider when handling tax rates (in TaxRates and
     * Orderitems by example)
     *
     */
    public static final int RATE_SCALE = 3;

    /**
     * Maximum valid value for ratings associated to a Reviews object
     */
    public static final int MAX_REVIEW_RATING = 5;

    /**
     * Minimum valid value for ratings associated to a Reviews object
     */
    public static final int MIN_REVIEW_RATING = 0;

    /**
     * Web application made only for Canada
     */
    public final static String COUNTRY = "Canada";

    /**
     * *********************
     */
    /*Cookie name constants*/
    public final static String GENRE_COOKIE_NAME = "recentGenre";

    public final static String LANGUAGE_COOKIE_NAME = "languageCookie";

    public final static String COUNTRY_COOKIE_NAME = "countryCookie";

    public final static String SUPPORTED_COOKIE_NAME = "cookiesEnabled";
    /**
     * *********************
     */
    /*Constants for hashing*/
    public final static int HASH_LENGTH = 32;

    public final static int HASH_ITERATION_COUNT = 1024;

    public final static int HASH_KEY_LENGTH = 256;

    public final static String HASH_ALGORITHM = "PBKDF2WithHmacSHA512";

    public final static int SALT_MAX_NUM_BITS = 140;

    /**
     * *********************
     */
    public final static String WEB_APP_URL = "/ga1w20";

    /**
     * As specified in faces-config.xml
     */
    public final static String LANGUAGE_BUNDLE_IDENTIFIER = "msgs";

    /*Constants for the resources paths*/
    public final static String BANNERS_PATH = "/resources/banners/";

    public final static String BOOK_COVERS_PATH = "/resources/covers/";

    public final static String BOOK_THUMBS_PATH = "/resources/covers/thumb/";

    public final static String BANNERS_SHORT_PATH = "/banners/";

    public final static String BOOK_COVERS_SHORT_PATH = "/covers/";

    public final static String BOOK_COVERS_THUMBNAILS_SHORT_PATH = "/covers/thumb/";

    /*Constants for redirects*/
    public final static String FINALIZATION_PAGE_END_OF_URL = "/finalization.xhtml";

    public final static String CHECKOUT_PAGE_END_OF_URL = "/checkout.xhtml";

    public final static String INDEX_PAGE_URL = WEB_APP_URL + "/index.xhtml";

    public final static String READ_COOKIE_URL = WEB_APP_URL + "/readCookie.xhtml";

    public final static String ERROR_PAGE_URL = "404.xhtml";

    /**
     * *********************
     */
    /* URL to file for when a user downloads a book. This feature is not entirely 
    implemented, so it will always be that file that will b uploaded.
     */
    public final static String DEFAULT_DOWNLOAD_BOOK_URL
            = "/WEB-INF/download/ebook.epub";

    /*If there are no active RSS feeds in DB, get news from the URL*/
    public final static String DEFAULT_RSS_FEED_URL
            = "http://feeds.bbci.co.uk/news/world/us_and_canada/rss.xml";

    /* Format and extension of images for Banners ads and Books covers */
    public final static String IMAGES_FORMAT = "jpg";

    public final static String IMAGES_EXTENSION = ".jpg";

    /**
     * *********************
     */
    /*Constants for book thumbnail cover sizes*/
    public final static int BOOK_THUMBNAIL_HEIGHT = 240;

    /**
     * Number of maximum books to display per section in the Client Front Door
     * page
     */
    public final static int BOOK_LIST_MAX_SIZE = 6;

    /**
     * Number of top sellers to show in genre page
     */
    public final static int TOP_SELLERS_LIST_SIZE = 6;

}
