package com.teama1.ga1w20.persistence.queries.beans;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * The TopQueryBean holds only the necessary fields for the report pages.
 *
 * @author Michael
 */
public class TopQueryBean implements Serializable {

    private BigDecimal totalSales;
    private long id;
    //Name of the client or seller to which the bean is referring to
    private String object;

    //Handler to compare and print the totalSales field
    private BigDecimalHandler bigDecimalHandler;

    public TopQueryBean() {
    }

    public TopQueryBean(BigDecimal totalSales, long id, String object) {
        this.totalSales = totalSales;
        this.id = id;
        this.object = object;
    }

    public TopQueryBean(BigDecimal totalSales, int id, String object) {
        this.totalSales = totalSales;
        this.id = id;
        this.object = object;
    }

    public BigDecimal getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(BigDecimal totalSales) {
        this.totalSales = totalSales;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        String t = (totalSales == null) ? "0.00" : bigDecimalHandler.getScaledString(totalSales);
        String o = (object == null) ? "" : object;

        return "TopQueryBean{"
                + "totalSales=" + t
                + ", id=" + id + ", object=" + o + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = (this.totalSales == null) ? hash : 37 * hash + Objects.hashCode(this.totalSales);
        hash = 37 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = (this.object == null) ? hash : 37 * hash + Objects.hashCode(this.object);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TopQueryBean other = (TopQueryBean) obj;
        if (this.id != other.id) {
            return false;
        }

        if ((this.object == null && other.object != null) || (this.object != null && other.object == null)) {
            return false;
        }

        if (this.object == null && other.object == null) {
            if (!Objects.equals(this.object, other.object)) {
                return false;
            }
        }

        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        if ((this.totalSales == null && other.totalSales != null) || (this.totalSales != null && other.totalSales == null)) {
            return false;
        }

        if (this.totalSales != null && other.totalSales != null) {
            if (!bigDecimalHandler.areEqual(this.totalSales, other.totalSales)) {
                return false;
            }
        }

        if (!Objects.equals(this.id, other.id)) {
            return false;
        }

        return true;
    }

}
