package com.teama1.ga1w20.persistence.queries.beans;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * The AllSalesQueryBean holds only the necessary fields for the report pages.
 *
 * @author Michael
 */
public class AllSalesQueryBean implements Serializable {

    private BigDecimal price;
    private Date purchaseDate;
    private long isbn;
    //Name of the author, client or publisher to which the bean is referring to
    private String name;
    //Title of the book
    private String title;

    //Handler to compare and print the prices field
    private BigDecimalHandler bigDecimalHandler;

    public AllSalesQueryBean() {
    }

    public AllSalesQueryBean(BigDecimal price, Date purchaseDate, long isbn, String name, String title) {
        this.price = price;
        this.purchaseDate = purchaseDate;
        this.isbn = isbn;
        this.name = name;
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        String pDate = (purchaseDate == null) ? "date unknown" : purchaseDate.toString();
        String n = (name == null) ? "name not found" : name;
        String t = (title == null) ? "title not found" : title;
        String p = (price == null) ? "no price listed" : bigDecimalHandler.getScaledString(price);

        return "AllSalesQueryBean{"
                + "price=" + p
                + ", purchaseDate=" + pDate + ", isbn=" + isbn
                + ", name=" + n + ", title=" + t + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;

        if (this.price != null) {
            hash = 67 * hash + Objects.hashCode(this.price);
        }

        if (this.purchaseDate != null) {
            hash = 67 * hash + Objects.hashCode(this.purchaseDate);
        }

        hash = 67 * hash + (int) (this.isbn ^ (this.isbn >>> 32));

        if (this.name != null) {
            hash = 67 * hash + Objects.hashCode(this.name);
        }

        if (this.title != null) {
            hash = 67 * hash + Objects.hashCode(this.title);
        }

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AllSalesQueryBean other = (AllSalesQueryBean) obj;

        if (this.isbn != other.isbn) {
            return false;
        }

        if ((this.name == null && other.name != null) || (other.name == null && this.name != null)) {
            return false;
        }

        if (this.name != null && other.name != null) {
            if (!Objects.equals(this.name, other.name)) {
                if (this.name.compareToIgnoreCase(other.name) != 0) {
                    return false;
                }
            }
        }

        if ((this.title == null && other.title != null) || (this.title != null && other.title == null)) {
            return false;
        }

        if (this.title != null && other.title != null) {
            if (!Objects.equals(this.title, other.title)) {
                if (this.title.compareToIgnoreCase(other.title) != 0) {
                    return false;
                }
            }
        }

        if ((this.purchaseDate == null && other.purchaseDate != null) || (this.purchaseDate != null && other.purchaseDate == null)) {
            return false;
        }

        if (this.purchaseDate != null && other.purchaseDate != null) {
            if (!Objects.equals(this.purchaseDate, other.purchaseDate)) {
                return false;
            }
        }

        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        if ((this.price == null && other.price != null) || (this.price != null && other.price == null)) {
            return false;
        }
        if (this.price != null && other.price != null) {
            if (!bigDecimalHandler.areEqual(this.price, other.price)) {
                return false;
            }
        }

        return true;
    }
}
