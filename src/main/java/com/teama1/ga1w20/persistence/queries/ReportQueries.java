package com.teama1.ga1w20.persistence.queries;

import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Authors_;
import com.teama1.ga1w20.persistence.entities.Bookauthor;
import com.teama1.ga1w20.persistence.entities.Bookauthor_;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Books_;
import com.teama1.ga1w20.persistence.entities.Credentials;
import com.teama1.ga1w20.persistence.entities.Credentials_;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.entities.Orderitems_;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.entities.Orders_;
import com.teama1.ga1w20.persistence.entities.Publishers;
import com.teama1.ga1w20.persistence.entities.Publishers_;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.entities.Users_;
import com.teama1.ga1w20.persistence.jpacontrollers.AuthorsJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.PublishersJpaController;
import com.teama1.ga1w20.persistence.jpacontrollers.UsersJpaController;
import com.teama1.ga1w20.persistence.queries.beans.AllSalesQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.SimpleBookQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TopQueryBean;
import com.teama1.ga1w20.persistence.queries.beans.TotalSalesQueryBean;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

/**
 * All report queries needed for the website.
 *
 * @author Michael
 */
@Named("reports")
@RequestScoped
public class ReportQueries implements Serializable {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private UsersJpaController usersJpaController;

    @Inject
    private AuthorsJpaController authorsJpaController;

    @Inject
    private PublishersJpaController publishersJpaController;

    public ReportQueries() {

    }

    /**
     * Retrieves all books ordered by total sales within the given date range.
     *
     * @param first
     * @param second
     * @return List of TopQueryBean containing total sales, isbn and book title.
     */
    public List<TopQueryBean> getTopSellers(Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TopQueryBean> topSellersQuery = cb.createQuery(TopQueryBean.class);
        Root<Books> books = topSellersQuery.from(Books.class);
        Join<Books, Orderitems> booksOrderitems = books.join(Books_.orderitemsList, JoinType.INNER);

        //Create the sum expression so that it may be reused later on when ordering
        Expression<BigDecimal> totalSales = cb.sum(booksOrderitems.get(Orderitems_.price));
        topSellersQuery.select(cb.construct(TopQueryBean.class, totalSales, books.get(Books_.isbn), books.get(Books_.title)))
                .where(booksOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates))
                .groupBy(books.get(Books_.isbn))
                .orderBy(cb.desc(totalSales));

        TypedQuery<TopQueryBean> query = entityManager.createQuery(topSellersQuery);
        return query.getResultList();
    }

    /**
     * Retrieves all clients ordered by total purchases within the given date
     * range.
     *
     * @param first
     * @param second
     * @return List of TopQueryBean containing total purchases, userId and
     * client name.
     */
    public List<TopQueryBean> getTopClients(Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TopQueryBean> topClientsQuery = cb.createQuery(TopQueryBean.class);
        Root<Users> users = topClientsQuery.from(Users.class);
        Join<Users, Credentials> usersCredentials = users.join(Users_.credentials, JoinType.INNER);
        Join<Users, Orders> usersOrders = users.join(Users_.ordersList, JoinType.INNER);
        Join<Orders, Orderitems> usersOrdersOrderitems = usersOrders.join(Orders_.orderitemsList, JoinType.INNER);

        //Create the sum expression so that it may be reused later on when ordering
        Expression<String> fullName = cb.concat(cb.concat(users.get(Users_.firstname), " "), users.get(Users_.lastname));
        Expression<BigDecimal> totalSales = cb.sum(usersOrdersOrderitems.get(Orderitems_.price));
        topClientsQuery.select(cb.construct(TopQueryBean.class, totalSales, users.get(Users_.userId), fullName))
                .where(usersOrdersOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates),
                        cb.equal(usersCredentials.get(Credentials_.manager), 0))
                .groupBy(users.get(Users_.userId))
                .orderBy(cb.desc(totalSales));

        TypedQuery<TopQueryBean> query = entityManager.createQuery(topClientsQuery);
        return query.getResultList();
    }

    /**
     * Retrieves all books that never sold within the given date range.
     *
     * @param first
     * @param second
     * @return List of SimpleBookQueryBean containing list, sale, and wholesale
     * price, as well as isbn and title.
     */
    public List<SimpleBookQueryBean> getZeroSales(Date first, Date second) {
        List<Long> ISBNsBetweenTwoDates = getISBNsBetweenTwoDates(first, second);

        if (ISBNsBetweenTwoDates.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SimpleBookQueryBean> zeroSalesQuery = cb.createQuery(SimpleBookQueryBean.class);
        Root<Books> books = zeroSalesQuery.from(Books.class);

        zeroSalesQuery.select(cb.construct(SimpleBookQueryBean.class, books.get(Books_.listPrice),
                books.get(Books_.salePrice), books.get(Books_.wholesalePrice), books.get(Books_.isbn), books.get(Books_.title)))
                .where(books.get(Books_.isbn).in(ISBNsBetweenTwoDates).not());

        TypedQuery<SimpleBookQueryBean> query = entityManager.createQuery(zeroSalesQuery);
        return query.getResultList();
    }

    /**
     * Retrieves the total sales of a book.
     *
     * @param isbn
     * @return BigDecimal containing total sales.
     */
    public BigDecimal getTotalSalesOfABook(Long isbn) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<BigDecimal> totalSalesQuery = cb.createQuery(BigDecimal.class);
        Root<Orderitems> orderitems = totalSalesQuery.from(Orderitems.class);
        Join<Orderitems, Books> orderitemsBooks = orderitems.join(Orderitems_.isbn);

        totalSalesQuery.select(cb.sum(orderitems.get(Orderitems_.price)))
                .where(cb.equal(orderitemsBooks.get(Books_.isbn), isbn));

        TypedQuery<BigDecimal> query = entityManager.createQuery(totalSalesQuery);
        return query.getSingleResult();
    }

    /**
     * Retrieves the total of all items sold within the given date range.
     *
     * @param first
     * @param second
     * @return TotalSalesQueryBean containing total sales and empty string.
     */
    public TotalSalesQueryBean getTotalSales(Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            return new TotalSalesQueryBean(
                    new BigDecimal(0.00),
                    ""
            );
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TotalSalesQueryBean> totalSalesQuery = cb.createQuery(TotalSalesQueryBean.class);
        Root<Orderitems> orderitems = totalSalesQuery.from(Orderitems.class);

        Join<Orderitems, Orders> orderItemsToOrders = orderitems.join(Orderitems_.orderId);

        totalSalesQuery.select(cb.construct(TotalSalesQueryBean.class, cb.sum(orderitems.get(Orderitems_.price)), cb.literal("")))
                .where(orderItemsToOrders.get(Orders_.orderId).in(orderIdsBetweenTwoDates));

        TypedQuery<TotalSalesQueryBean> query = entityManager.createQuery(totalSalesQuery);
        return query.getSingleResult();
    }

    /**
     * Retrieves all items sold within the given date range ordered by purchase
     * date.
     *
     * @param first
     * @param second
     * @return List of AllSalesQueryBean containing price, purchase date, isbn,
     * empty string, title.
     */
    public List<AllSalesQueryBean> getAllSales(Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AllSalesQueryBean> totalSalesQuery = cb.createQuery(AllSalesQueryBean.class);
        Root<Orders> orders = totalSalesQuery.from(Orders.class);
        Join<Orders, Orderitems> ordersOrderitems = orders.join(Orders_.orderitemsList);
        Join<Orderitems, Books> ordersOrderitemsBooks = ordersOrderitems.join(Orderitems_.isbn);

        totalSalesQuery.select(cb.construct(AllSalesQueryBean.class, ordersOrderitems.get(Orderitems_.price),
                orders.get(Orders_.purchaseDate), ordersOrderitemsBooks.get(Books_.isbn),
                cb.literal(""), ordersOrderitemsBooks.get(Books_.title)))
                .where(ordersOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates))
                .orderBy(cb.desc(orders.get(Orders_.purchaseDate)));

        TypedQuery<AllSalesQueryBean> query = entityManager.createQuery(totalSalesQuery);
        return query.getResultList();
    }

    /**
     * Retrieves the total purchased of a given client within the given date
     * range.
     *
     * @param clnId
     * @param first
     * @param second
     * @return TotalSalesQueryBean containing total purchases and name of
     * client.
     */
    public TotalSalesQueryBean getTotalSalesByClient(Integer clnId, Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            Users user = usersJpaController.findUsers(clnId);

            return new TotalSalesQueryBean(
                    new BigDecimal(0.00),
                    user.getFirstname() + " " + user.getLastname()
            );
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TotalSalesQueryBean> totalSalesClientQuery = cb.createQuery(TotalSalesQueryBean.class);
        Root<Users> users = totalSalesClientQuery.from(Users.class);
        Join<Users, Credentials> usersCredentials = users.join(Users_.credentials, JoinType.INNER);
        Join<Users, Orders> usersOrders = users.join(Users_.ordersList, JoinType.INNER);
        Join<Orders, Orderitems> usersOrdersOrderitems = usersOrders.join(Orders_.orderitemsList, JoinType.INNER);

        //Create an Expression to concatinate first name and last name
        Expression<String> fullName = cb.concat(cb.concat(users.get(Users_.firstname), " "), users.get(Users_.lastname));
        totalSalesClientQuery.select(cb.construct(TotalSalesQueryBean.class, cb.sum(usersOrdersOrderitems.get(Orderitems_.price)), fullName))
                .where(usersOrdersOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates),
                        cb.equal(users.get(Users_.userId), clnId), cb.equal(usersCredentials.get(Credentials_.manager), 0));

        TypedQuery<TotalSalesQueryBean> query = entityManager.createQuery(totalSalesClientQuery);
        TotalSalesQueryBean bean = query.getSingleResult();

        if (bean.getName() == null) {
            Users user = usersJpaController.findUsers(clnId);

            bean.setName(user.getFirstname() + " " + user.getLastname());
        }

        return bean;
    }

    /**
     * Retrieves all items purchased by a given client within the given date
     * range ordered by purchase date.
     *
     * @param clnId
     * @param first
     * @param second
     * @return List of AllSalesQueryBean containing price, purchase date, isbn,
     * full name, title.
     */
    public List<AllSalesQueryBean> getAllSalesByClient(Integer clnId, Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AllSalesQueryBean> allSalesClientQuery = cb.createQuery(AllSalesQueryBean.class);
        Root<Users> users = allSalesClientQuery.from(Users.class);
        Join<Users, Credentials> usersCredentials = users.join(Users_.credentials, JoinType.INNER);
        Join<Users, Orders> usersOrders = users.join(Users_.ordersList, JoinType.INNER);
        Join<Orders, Orderitems> usersOrdersOrderitems = usersOrders.join(Orders_.orderitemsList, JoinType.INNER);
        Join<Orderitems, Books> usersOrdersOrderitemsBooks = usersOrdersOrderitems.join(Orderitems_.isbn, JoinType.INNER);

        //Create an Expression to concatinate first name and last name
        Expression<String> fullName = cb.concat(cb.concat(users.get(Users_.firstname), " "), users.get(Users_.lastname));
        allSalesClientQuery.select(cb.construct(AllSalesQueryBean.class, usersOrdersOrderitems.get(Orderitems_.price),
                usersOrders.get(Orders_.purchaseDate), usersOrdersOrderitemsBooks.get(Books_.isbn),
                fullName, usersOrdersOrderitemsBooks.get(Books_.title)))
                .where(usersOrdersOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates),
                        cb.equal(users.get(Users_.userId), clnId), cb.equal(usersCredentials.get(Credentials_.manager), 0))
                .orderBy(cb.desc(usersOrders.get(Orders_.purchaseDate)));

        TypedQuery<AllSalesQueryBean> query = entityManager.createQuery(allSalesClientQuery);
        return query.getResultList();
    }

    /**
     * Retrieves the total sales of a given author within the given date range.
     *
     * @param authId
     * @param first
     * @param second
     * @return TotalSalesQueryBean containing total sold and name of author.
     */
    public TotalSalesQueryBean getTotalSalesByAuthor(Integer authId, Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            Authors author = authorsJpaController.findAuthors(authId);

            return new TotalSalesQueryBean(
                    new BigDecimal(0.00),
                    author.getFirstname() + " " + author.getLastname()
            );
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TotalSalesQueryBean> totalSalesAuthQuery = cb.createQuery(TotalSalesQueryBean.class);
        Root<Authors> authors = totalSalesAuthQuery.from(Authors.class);
        Join<Authors, Bookauthor> authorsBookauthor = authors.join(Authors_.bookauthorList, JoinType.INNER);
        Join<Bookauthor, Books> authorsBookauthorBooks = authorsBookauthor.join(Bookauthor_.isbn, JoinType.INNER);
        Join<Books, Orderitems> authorsBookauthorBooksOrderitems = authorsBookauthorBooks.join(Books_.orderitemsList, JoinType.INNER);

        //Create an Expression to concatinate first name and last name
        Expression<String> fullName = cb.concat(cb.concat(authors.get(Authors_.firstname), " "), authors.get(Authors_.lastname));
        totalSalesAuthQuery.select(cb.construct(TotalSalesQueryBean.class, cb.sum(authorsBookauthorBooksOrderitems.get(Orderitems_.price)), fullName))
                .where(authorsBookauthorBooksOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates), cb.equal(authors.get(Authors_.authorId), authId));

        TypedQuery<TotalSalesQueryBean> query = entityManager.createQuery(totalSalesAuthQuery);
        TotalSalesQueryBean bean = query.getSingleResult();

        if (bean.getName() == null) {
            Authors author = authorsJpaController.findAuthors(authId);

            bean.setName(author.getFirstname() + " " + author.getLastname());
        }

        return bean;
    }

    /**
     * Retrieves all items sold by a given author within the given date range
     * ordered by purchase date.
     *
     * @param authId
     * @param first
     * @param second
     * @return List of AllSalesQueryBean containing price, purchase date, isbn,
     * full name, title.
     */
    public List<AllSalesQueryBean> getAllSalesByAuthor(Integer authId, Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AllSalesQueryBean> allSalesAuthQuery = cb.createQuery(AllSalesQueryBean.class);
        Root<Authors> authors = allSalesAuthQuery.from(Authors.class);
        Join<Authors, Bookauthor> authorsBookauthor = authors.join(Authors_.bookauthorList, JoinType.INNER);
        Join<Bookauthor, Books> authorsBookauthorBooks = authorsBookauthor.join(Bookauthor_.isbn, JoinType.INNER);
        Join<Books, Orderitems> authorsBookauthorBooksOrderitems = authorsBookauthorBooks.join(Books_.orderitemsList, JoinType.INNER);
        Join<Orderitems, Orders> authorsBookauthorBooksOrderitemsOrders = authorsBookauthorBooksOrderitems.join(Orderitems_.orderId, JoinType.INNER);

        //Create an Expression to concatinate first name and last name
        Expression<String> fullName = cb.concat(cb.concat(authors.get(Authors_.firstname), " "), authors.get(Authors_.lastname));
        allSalesAuthQuery
                .where(
                        cb.and(
                                authorsBookauthorBooksOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates),
                                cb.equal(authors.get(Authors_.authorId), authId)
                        )
                )
                .orderBy(cb.desc(authorsBookauthorBooksOrderitemsOrders.get(Orders_.purchaseDate)))
                .select(
                        cb.construct(
                                AllSalesQueryBean.class,
                                authorsBookauthorBooksOrderitems.get(Orderitems_.price),
                                authorsBookauthorBooksOrderitemsOrders.get(Orders_.purchaseDate),
                                authorsBookauthorBooks.get(Books_.isbn),
                                fullName,
                                authorsBookauthorBooks.get(Books_.title)
                        )
                );

        TypedQuery<AllSalesQueryBean> query = entityManager.createQuery(allSalesAuthQuery);
        return query.getResultList();
    }

    /**
     * Retrieves the total sales of a given publisher within the given date
     * range.
     *
     * @param pubId
     * @param first
     * @param second
     * @return TotalSalesQueryBean containing total sold and name of publisher.
     */
    public TotalSalesQueryBean getTotalSalesByPublisher(Integer pubId, Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            Publishers pub = publishersJpaController.findPublishers(pubId);

            TotalSalesQueryBean bean = new TotalSalesQueryBean(
                    new BigDecimal(0.00),
                    pub.getName()
            );

            return bean;
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TotalSalesQueryBean> totalSalesPubQuery = cb.createQuery(TotalSalesQueryBean.class);
        Root<Publishers> publishers = totalSalesPubQuery.from(Publishers.class);
        Join<Publishers, Books> publishersBooks = publishers.join(Publishers_.booksList, JoinType.INNER);
        Join<Books, Orderitems> publishersBooksOrderitems = publishersBooks.join(Books_.orderitemsList, JoinType.INNER);

        totalSalesPubQuery.select(cb.construct(TotalSalesQueryBean.class, cb.sum(publishersBooksOrderitems.get(Orderitems_.price)), publishers.get(Publishers_.name)))
                .where(publishersBooksOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates), cb.equal(publishers.get(Publishers_.publisherId), pubId));

        TypedQuery<TotalSalesQueryBean> query = entityManager.createQuery(totalSalesPubQuery);
        TotalSalesQueryBean bean = query.getSingleResult();

        if (bean.getName() == null) {
            bean.setName(publishersJpaController.findPublishers(pubId).getName());
        }

        return bean;
    }

    /**
     * Retrieves all items purchased by a given client within the given date
     * range ordered by purchase date.
     *
     * @param pubId
     * @param first
     * @param second
     * @return List of AllSalesQueryBean containing price, purchase date, isbn,
     * pub name, title.
     */
    public List<AllSalesQueryBean> getAllSalesByPublisher(Integer pubId, Date first, Date second) {
        List<Integer> orderIdsBetweenTwoDates = getOrderIdsBetweenTwoDates(first, second);

        if (orderIdsBetweenTwoDates.isEmpty()) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AllSalesQueryBean> allSalesPubQuery = cb.createQuery(AllSalesQueryBean.class);
        Root<Publishers> publishers = allSalesPubQuery.from(Publishers.class);
        Join<Publishers, Books> publishersBooks = publishers.join(Publishers_.booksList, JoinType.INNER);
        Join<Books, Orderitems> publishersBooksOrderitems = publishersBooks.join(Books_.orderitemsList, JoinType.INNER);
        Join<Orderitems, Orders> publishersBooksOrderitemsOrders = publishersBooksOrderitems.join(Orderitems_.orderId, JoinType.INNER);

        //Create the sum expression so that it may be reused later on when ordering
        allSalesPubQuery.select(cb.construct(AllSalesQueryBean.class, publishersBooksOrderitems.get(Orderitems_.price),
                publishersBooksOrderitemsOrders.get(Orders_.purchaseDate), publishersBooks.get(Books_.isbn),
                publishers.get(Publishers_.name), publishersBooks.get(Books_.title)))
                .where(publishersBooksOrderitems.get(Orderitems_.orderId).in(orderIdsBetweenTwoDates), cb.equal(publishers.get(Publishers_.publisherId), pubId))
                .orderBy(cb.desc(publishersBooksOrderitemsOrders.get(Orders_.purchaseDate)));

        TypedQuery<AllSalesQueryBean> query = entityManager.createQuery(allSalesPubQuery);
        return query.getResultList();
    }

    /**
     * Retrieves current stock.
     *
     * @return List of SimpleBookQueryBean containing list, sale, and wholesale
     * price, as well as isbn and title.
     */
    public List<SimpleBookQueryBean> getStock() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SimpleBookQueryBean> stockQuery = cb.createQuery(SimpleBookQueryBean.class);
        Root<Books> books = stockQuery.from(Books.class);

        stockQuery.select(cb.construct(SimpleBookQueryBean.class, books.get(Books_.listPrice),
                books.get(Books_.salePrice), books.get(Books_.wholesalePrice), books.get(Books_.isbn), books.get(Books_.title)));

        TypedQuery<SimpleBookQueryBean> query = entityManager.createQuery(stockQuery);
        return query.getResultList();
    }

    /**
     * Retrieves all orderIds within the given date range.
     *
     * @param first
     * @param second
     * @return List of orderIds.
     */
    public List<Integer> getOrderIdsBetweenTwoDates(Date first, Date second) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Integer> orderIdsBetweenTwoDatesQuery = cb.createQuery(Integer.class);
        Root<Orders> orders = orderIdsBetweenTwoDatesQuery.from(Orders.class);

        orderIdsBetweenTwoDatesQuery.select(orders.get(Orders_.orderId))
                .where(cb.between(orders.get(Orders_.purchaseDate), cb.literal(first), cb.literal(second)));

        TypedQuery<Integer> query = entityManager.createQuery(orderIdsBetweenTwoDatesQuery);
        return query.getResultList();
    }

    /**
     * Retrieves all isbns within the given date range.
     *
     * @param first
     * @param second
     * @return List of isbns.
     */
    public List<Long> getISBNsBetweenTwoDates(Date first, Date second) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> ISBNsBetweenTwoDatesQuery = cb.createQuery(Long.class);
        Root<Orders> orders = ISBNsBetweenTwoDatesQuery.from(Orders.class);
        Join<Orders, Orderitems> ordersOrderitems = orders.join(Orders_.orderitemsList, JoinType.INNER);

        ISBNsBetweenTwoDatesQuery.select(ordersOrderitems.get(Orderitems_.isbn).get(Books_.isbn)).distinct(true)
                .where(cb.between(orders.get(Orders_.purchaseDate), cb.literal(first), cb.literal(second)));

        TypedQuery<Long> query = entityManager.createQuery(ISBNsBetweenTwoDatesQuery);
        return query.getResultList();
    }
}
