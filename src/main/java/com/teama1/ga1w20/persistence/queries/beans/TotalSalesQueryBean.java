package com.teama1.ga1w20.persistence.queries.beans;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * The TotalSalesQueryBean holds only the necessary fields for the report pages.
 *
 * @author Michael
 */
public class TotalSalesQueryBean implements Serializable {

    private BigDecimal totalSales;
    //Name of the client or book to which the bean is referring to
    private String name;

    //Handler to compare and print the totalSales field
    private BigDecimalHandler bigDecimalHandler;

    public TotalSalesQueryBean() {
    }

    public TotalSalesQueryBean(BigDecimal totalSales, String name) {
        this.totalSales = totalSales;
        this.name = name;
    }

    public BigDecimal getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(BigDecimal totalSales) {
        this.totalSales = totalSales;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        if (totalSales == null) {
            totalSales = new BigDecimal(0.00);
        }

        String ts = (totalSales == null) ? "0.00" : bigDecimalHandler.getScaledString(totalSales);
        String n = (name == null) ? "" : name;

        return "TotalSalesQueryBean{"
                + "totalSales=" + ts
                + ", name=" + n + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = (this.totalSales == null) ? hash : 53 * hash + Objects.hashCode(this.totalSales);
        hash = (this.name == null) ? hash : 53 * hash + Objects.hashCode(this.name);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final TotalSalesQueryBean other = (TotalSalesQueryBean) obj;

        if (this.totalSales == null && other.totalSales != null) {
            return false;
        }

        if (other.totalSales == null && this.totalSales != null) {
            return false;
        }

        if (this.name == null && other.name != null) {
            return false;
        }

        if (other.name == null && this.name != null) {
            return false;
        }

        if (!Objects.equals(this.name, other.name)) {
            return false;
        }

        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        if (this.totalSales != null && other.totalSales != null) {
            if (!bigDecimalHandler.areEqual(this.totalSales, other.totalSales)) {
                return false;
            }
        }
        return true;
    }
}
