package com.teama1.ga1w20.persistence.queries.beans;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * The SimpleBookQueryBean holds only the necessary fields for the report pages.
 *
 * @author Michael
 */
public class SimpleBookQueryBean implements Serializable {

    private BigDecimal listPrice;
    private BigDecimal salePrice;
    private BigDecimal wholesalePrice;
    private long isbn;
    private String title;

    //Handler to compare and print the prices field
    private BigDecimalHandler bigDecimalHandler;

    public SimpleBookQueryBean() {
    }

    public SimpleBookQueryBean(BigDecimal listPrice, BigDecimal salePrice, BigDecimal wholesalePrice, long isbn, String title) {
        this.listPrice = listPrice;
        this.salePrice = salePrice;
        this.wholesalePrice = wholesalePrice;
        this.isbn = isbn;
        this.title = title;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public BigDecimal getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(BigDecimal wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        String lPrice = (listPrice == null) ? "List Price Not Found" : bigDecimalHandler.getScaledString(listPrice);
        String sPrice = (salePrice == null) ? "Sale Price Not Found" : bigDecimalHandler.getScaledString(salePrice);
        String wPrice = (wholesalePrice == null) ? "Wholesale Price Not Found" : bigDecimalHandler.getScaledString(wholesalePrice);
        String t = (title == null) ? "Title Not Found" : title;

        return "SimpleBookQueryBean{"
                + "listPrice=" + lPrice
                + ", salePrice=" + sPrice
                + ", wholesalePrice=" + wPrice
                + ", isbn=" + isbn + ", title=" + t + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = (this.listPrice == null) ? hash : 89 * hash + Objects.hashCode(this.listPrice);
        hash = (this.salePrice == null) ? hash : 89 * hash + Objects.hashCode(this.salePrice);
        hash = (this.wholesalePrice == null) ? hash : 89 * hash + Objects.hashCode(this.wholesalePrice);
        hash = 89 * hash + (int) (this.isbn ^ (this.isbn >>> 32));
        hash = (this.title == null) ? hash : 89 * hash + Objects.hashCode(this.title);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleBookQueryBean other = (SimpleBookQueryBean) obj;
        if (this.isbn != other.isbn) {
            return false;
        }

        if ((this.title == null && other.title != null) || (this.title != null && other.title == null)) {
            return false;
        }

        if (this.title != null && other.title != null) {
            if (!Objects.equals(this.title, other.title)) {
                if (this.title.compareToIgnoreCase(other.title) != 0) {
                    return false;
                }
            }
        }

        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        if ((this.listPrice == null && other.listPrice != null) || (this.listPrice != null && other.listPrice == null)) {
            return false;
        }

        if (this.listPrice != null && other.listPrice != null) {
            if (!bigDecimalHandler.areEqual(this.listPrice, other.listPrice)) {
                return false;
            }
        }

        if ((this.salePrice == null && other.salePrice != null) || (this.salePrice != null && other.salePrice == null)) {
            return false;
        }

        if (this.salePrice != null && other.salePrice != null) {
            if (!bigDecimalHandler.areEqual(this.salePrice, other.salePrice)) {
                return false;
            }
        }

        if ((this.wholesalePrice == null && other.wholesalePrice != null) || (this.wholesalePrice != null && other.wholesalePrice == null)) {
            return false;
        }

        if (this.wholesalePrice != null && other.wholesalePrice != null) {
            if (!bigDecimalHandler.areEqual(this.wholesalePrice, other.wholesalePrice)) {
                return false;
            }
        }

        return true;
    }

}
