package com.teama1.ga1w20.persistence.queries.beans;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.entities.Orders;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * OrdersQueryBean holds the necessary fields for the Order Management page. The
 * isbn, orderid and userid fields just hold the id instead of pointing to other
 * objects.
 *
 * @author Timmy
 */
public class OrdersQueryBean implements Serializable {

    private int orderitemId;
    private int orderId;
    private int userId;
    private Date purchaseDate;
    private BigDecimal gst;
    private BigDecimal hst;
    private BigDecimal pst;
    private List<Orderitems> orderitems;
    private boolean orderRemove;

    //Handler to compare and print the prices field
    private BigDecimalHandler bigDecimalHandler;

    public List<Orderitems> getOrderitems() {
        return orderitems;
    }

    public void setOrderitems(List<Orderitems> orderitems) {
        this.orderitems = orderitems;
    }

    public boolean isOrderRemove() {
        return orderRemove;
    }

    public void setOrderRemove(boolean orderRemove) {
        this.orderRemove = orderRemove;
    }

    public OrdersQueryBean() {
    }

    public OrdersQueryBean(Orders orders, List<Orderitems> orderitems) {
        this.orderId = orders.getOrderId();
        this.userId = orders.getUserId().getUserId();
        this.purchaseDate = orders.getPurchaseDate();
        this.gst = orders.getGstRate();
        this.hst = orders.getHstRate();
        this.pst = orders.getPstRate();
        this.orderitems = orderitems;
        this.orderRemove = true;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public BigDecimal getGst() {
        return gst;
    }

    public void setGst(BigDecimal gst) {
        this.gst = gst;
    }

    public BigDecimal getHst() {
        return hst;
    }

    public void setHst(BigDecimal hst) {
        this.hst = hst;
    }

    public BigDecimal getPst() {
        return pst;
    }

    public void setPst(BigDecimal pst) {
        this.pst = pst;
    }

    public int getOrderitemId() {
        return orderitemId;
    }

    public void setOrderitemId(int orderitemId) {
        this.orderitemId = orderitemId;
    }

    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        String pDate = (purchaseDate == null) ? "Order Date Unknown" : purchaseDate.toString();
        String g = (gst == null) ? "GST Not Found" : bigDecimalHandler.getScaledString(gst);
        String h = (hst == null) ? "HST Not Found" : bigDecimalHandler.getScaledString(hst);
        String ps = (pst == null) ? "PST Not Found" : bigDecimalHandler.getScaledString(pst);

        return "OrdersQueryBean{" + "orderitemId=" + orderitemId + ", orderId=" + orderId + ", userId=" + userId + ", purchaseDate=" + pDate
                + ", gst=" + g
                + ", hst=" + h
                + ", pst=" + ps + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + this.orderitemId;
        hash = 67 * hash + this.orderId;
        hash = 67 * hash + this.userId;
        hash = (this.purchaseDate == null) ? hash : 67 * hash + Objects.hashCode(this.purchaseDate);
        hash = (this.gst == null) ? hash : 67 * hash + Objects.hashCode(this.gst);
        hash = (this.hst == null) ? hash : 67 * hash + Objects.hashCode(this.hst);
        hash = (this.pst == null) ? hash : 67 * hash + Objects.hashCode(this.pst);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdersQueryBean other = (OrdersQueryBean) obj;
        if (this.orderitemId != other.orderitemId) {
            return false;
        }
        if (this.orderId != other.orderId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        if ((this.purchaseDate == null && other.purchaseDate != null || this.purchaseDate != null && other.purchaseDate == null)) {
            return false;
        }

        if (this.purchaseDate != null && other.purchaseDate != null) {
            if (!Objects.equals(this.purchaseDate, other.purchaseDate)) {
                return false;
            }
        }

        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        if ((this.gst == null && other.gst != null) || (this.gst != null && other.gst == null)) {
            return false;
        }

        if (this.gst != null && other.gst != null) {
            if (!bigDecimalHandler.areEqual(this.gst, other.gst)) {
                return false;
            }
        }

        if ((this.pst == null && other.pst != null) || (this.pst != null && other.pst == null)) {
            return false;
        }

        if (this.pst != null && other.pst != null) {
            if (!bigDecimalHandler.areEqual(this.hst, other.hst)) {
                return false;
            }
        }

        if ((this.pst == null && other.pst != null) || (this.pst != null && other.pst == null)) {
            return false;
        }

        if (this.pst != null && other.pst != null) {
            if (!bigDecimalHandler.areEqual(this.pst, other.pst)) {
                return false;
            }
        }

        return true;
    }

}
