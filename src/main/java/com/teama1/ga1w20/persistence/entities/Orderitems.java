package com.teama1.ga1w20.persistence.entities;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "orderitems", catalog = "ga1w20")
public class Orderitems implements Serializable {

    private static final long serialVersionUID = 1L;

    //Handler to compare and print the price field
    @Transient
    private BigDecimalHandler bigDecimalHandler;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "orderitems_id")
    private Integer orderitemsId;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "price")
    private BigDecimal price;

    @JoinColumn(name = "isbn", referencedColumnName = "isbn")
    @ManyToOne(optional = false)
    private Books isbn;

    @JoinColumn(name = "order_id", referencedColumnName = "order_id")
    @ManyToOne(optional = false)
    private Orders orderId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "removed")
    private boolean removed;

    public Orderitems() {
    }

    public Orderitems(Integer orderitemsId) {
        this.orderitemsId = orderitemsId;
    }

    public Orderitems(Integer orderitemsId, BigDecimal price, Books isbn, Orders orderId, boolean removed) {
        this.orderitemsId = orderitemsId;
        this.price = price;
        this.isbn = isbn;
        this.orderId = orderId;
        this.removed = removed;
    }

    public Integer getOrderitemsId() {
        return orderitemsId;
    }

    public void setOrderitemsId(Integer orderitemsId) {
        this.orderitemsId = orderitemsId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Books getIsbn() {
        return isbn;
    }

    public void setIsbn(Books isbn) {
        this.isbn = isbn;
    }

    public Orders getOrderId() {
        return orderId;
    }

    public void setOrderId(Orders orderId) {
        this.orderId = orderId;
    }

    public boolean getRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    /**
     * Does not compare list fields. When considering the associated order, it
     * considers only its id.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.orderitemsId);
        if (this.orderId != null && this.orderId.getOrderId() != null) {
            hash = 37 * hash + Objects.hashCode(this.orderId.getOrderId());
        }
        if (this.isbn != null && this.isbn.getIsbn() != null) {
            hash = 37 * hash + Objects.hashCode(this.isbn.getIsbn());
        }
        hash = 37 * hash + Objects.hashCode(this.price);
        hash = 37 * hash + (this.removed ? 1 : 0);
        return hash;
    }

    /**
     * Does not compare list fields. When considering the associated order, it
     * considers only its id.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Orderitems other = (Orderitems) obj;
        if (!Objects.equals(this.orderitemsId, other.orderitemsId)) {
            return false;
        }
        if (!Objects.equals(this.orderId, other.orderId)
                && (this.orderId == null || other.orderId == null
                || this.orderId.getOrderId() == null
                || other.orderId.getOrderId() == null
                || !this.orderId.getOrderId().equals(other.orderId.getOrderId()))) {
            return false;
        }

        if (!Objects.equals(this.isbn, other.isbn)
                && (this.isbn == null || other.isbn == null
                || this.isbn.getIsbn() == null
                || other.isbn.getIsbn() == null
                || !this.isbn.getIsbn().equals(other.isbn.getIsbn()))) {
            return false;
        }
        if (this.removed != other.removed) {
            return false;
        }

        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }
        if (!bigDecimalHandler.areEqual(this.price, other.price)) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
        }

        String orderIdMsg = "No Associated Order";
        if (orderId != null && orderId.getOrderId() != null) {
            orderIdMsg = orderId.getOrderId().toString();
        }

        String isbnMsg = "No associated Book";
        if (isbn != null && isbn.getIsbn() != null) {
            isbnMsg = isbn.getIsbn().toString();
        }

        return "Orderitems{"
                + "orderitemsId=" + orderitemsId
                + ", orderId=" + orderIdMsg
                + ", isbn=" + isbnMsg
                + ", price=" + (bigDecimalHandler.getScaledString(price))
                + ", removed=" + removed + '}';
    }

}
