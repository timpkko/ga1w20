package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bookauthor", catalog = "ga1w20")
public class Bookauthor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bookauthor_id")
    private Integer bookauthorId;

    @JoinColumn(name = "author_id", referencedColumnName = "author_id")
    @ManyToOne(optional = false)
    private Authors authorId;

    @JoinColumn(name = "isbn", referencedColumnName = "isbn")
    @ManyToOne(optional = false)
    private Books isbn;

    public Bookauthor() {
    }

    public Bookauthor(Integer bookauthorId) {
        this.bookauthorId = bookauthorId;
    }

    public Bookauthor(Books isbn, Authors authorId) {
        this.isbn = isbn;
        this.authorId = authorId;
    }

    public Integer getBookauthorId() {
        return bookauthorId;
    }

    public void setBookauthorId(Integer bookauthorId) {
        this.bookauthorId = bookauthorId;
    }

    public Authors getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Authors authorId) {
        this.authorId = authorId;
    }

    public Books getIsbn() {
        return isbn;
    }

    public void setIsbn(Books isbn) {
        this.isbn = isbn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookauthorId != null ? bookauthorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookauthor)) {
            return false;
        }
        Bookauthor other = (Bookauthor) object;
        if ((this.bookauthorId == null && other.bookauthorId != null)
                || (this.bookauthorId != null && !this.bookauthorId.equals(other.bookauthorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bookauthor[ bookauthorId=" + bookauthorId + " ]";
    }

}
