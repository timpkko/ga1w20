package com.teama1.ga1w20.persistence.entities;

import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "reviews", catalog = "ga1w20")
public class Reviews implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "review_id")
    private Long reviewId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "postdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date postdate;

    //Must be between minimum and maximum review constants
    @Min(value = Constants.MIN_REVIEW_RATING)
    @Max(value = Constants.MAX_REVIEW_RATING)
    @Basic(optional = false)
    @NotNull
    @Column(name = "rating")
    private int rating;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 750)
    @Column(name = "text")
    private String text;

    @Column(name = "approved")
    private Boolean approved;

    @JoinColumn(name = "isbn", referencedColumnName = "isbn")
    @ManyToOne(optional = false)
    private Books isbn;

    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private Users userId;

    public Reviews() {
    }

    public Reviews(Long reviewId) {
        this.reviewId = reviewId;
    }

    public Reviews(Long reviewId, Date postdate, int rating, String text) {
        this.reviewId = reviewId;
        this.postdate = postdate;
        this.rating = rating;
        this.text = text;
    }

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    public Date getPostdate() {
        return postdate;
    }

    public void setPostdate(Date postdate) {
        this.postdate = postdate;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Books getIsbn() {
        return isbn;
    }

    public void setIsbn(Books isbn) {
        this.isbn = isbn;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    /**
     * Does not consider the approved field neither the list fields.
     * Case-insensitive computation. Null fields-safe.
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        if (this.reviewId != null) {
            hash = 37 * hash + Objects.hashCode(this.reviewId);
        }
        if (this.postdate != null) {
            hash = 37 * hash + Objects.hashCode(this.postdate);
        }
        if (this.text != null) {
            hash = 37 * hash + Objects.hashCode(this.text.toLowerCase());
        }
        hash = 37 * hash + this.rating;
        return hash;
    }

    /**
     * Does not consider the approved field neither the list fields.
     * Case-insensitive comparison. Null fields-safe.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reviews other = (Reviews) obj;
        if (!Objects.equals(this.reviewId, other.reviewId)) {
            return false;
        }
        if (this.rating != other.rating) {
            return false;
        }
        if (!Objects.equals(this.postdate, other.postdate)) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)
                && (this.text == null || other.text == null
                || !this.text.equalsIgnoreCase(other.text))) {
            return false;
        }
        return true;
    }

    /**
     * Null fields-safe.
     *
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Reviews{"
                + "reviewId=" + reviewId
                + ", userId=" + (userId == null || userId.getUserId() == null ? "No Associated User" : userId.getUserId())
                + ", isbn=" + ((isbn == null || isbn.getIsbn() == null) ? "No Associated Book" : isbn.getIsbn())
                + ", postdate=" + postdate
                + ", rating=" + rating
                + ", text=" + text
                + ", approved=" + approved
                + '}';
    }
}
