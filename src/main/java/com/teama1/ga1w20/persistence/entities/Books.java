package com.teama1.ga1w20.persistence.entities;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "books", catalog = "ga1w20")
public class Books implements Serializable {

    //Handler to compare and print the prices fields
    @Transient
    private BigDecimalHandler bigDecimalHandler;

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(Books.class);

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "isbn")
    private Long isbn;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 260)
    @Column(name = "title")
    private String title;

    @Basic(optional = false)
    @NotNull
    @Column(name = "pub_date")
    @Temporal(TemporalType.DATE)
    private Date pubDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "num_pages")
    private int numPages;

    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "description")
    private String description;

    //Must be at least 0
    @Min(value = 0)
    @Basic(optional = false)
    @NotNull
    @Column(name = "wholesale_price")
    private BigDecimal wholesalePrice;

    //Must be at least 0
    @Min(value = 0)
    @Basic(optional = false)
    @NotNull
    @Column(name = "list_price")
    private BigDecimal listPrice;

    //Must be at least 0
    @Min(value = 0)
    @Basic(optional = false)
    @NotNull
    @Column(name = "sale_price")
    private BigDecimal salePrice;

    //Must be at least 0
    @Min(value = 0)
    @Basic(optional = false)
    @NotNull
    @Column(name = "min_age")
    private int minAge;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "max_age")
    private Integer maxAge;

    @Basic(optional = false)
    @NotNull
    @Column(name = "acquired_stamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date acquiredStamp;

    @Basic(optional = false)
    @NotNull
    @Column(name = "removed")
    private boolean removed;

    @OneToMany(mappedBy = "isbn")
    private List<Bookgenre> bookgenreList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "isbn")
    private List<Bookformat> bookformatList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "isbn")
    private List<Bookauthor> bookauthorList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "isbn")
    private List<Orderitems> orderitemsList;

    @JoinColumn(name = "publisher_id", referencedColumnName = "publisher_id")
    @ManyToOne(optional = false)
    private Publishers publisherId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "isbn")
    private List<Reviews> reviewsList;

    public Books() {
    }

    public Books(Long isbn) {
        this.isbn = isbn;
    }

    public Books(Long isbn, String title, Publishers publisher) {
        this.isbn = isbn;
        this.title = title;
        this.publisherId = publisher;
    }

    public Books(Long isbn, String title, Date pubDate, int numPages, String description, BigDecimal wholesalePrice, BigDecimal listPrice, BigDecimal salePrice, int minAge, Date acquiredStamp, boolean removed) {
        this.isbn = isbn;
        this.title = title;
        this.pubDate = pubDate;
        this.numPages = numPages;
        this.description = description;
        this.wholesalePrice = wholesalePrice;
        this.listPrice = listPrice;
        this.salePrice = salePrice;
        this.minAge = minAge;
        this.acquiredStamp = acquiredStamp;
        this.removed = removed;
    }

    public Books(Long isbn, String title, Date pubDate, int numPages, BigDecimal wholesalePrice, BigDecimal listPrice, BigDecimal salePrice, Integer minAge, Integer maxAge, boolean removed) {
        this.isbn = isbn;
        this.title = title;
        this.pubDate = pubDate;
        this.numPages = numPages;
        this.wholesalePrice = wholesalePrice;
        this.listPrice = listPrice;
        this.salePrice = salePrice;
        this.minAge = minAge;
        this.removed = removed;
    }

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(BigDecimal wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public Date getAcquiredStamp() {
        return acquiredStamp;
    }

    public void setAcquiredStamp(Date acquiredStamp) {
        this.acquiredStamp = acquiredStamp;
    }

    public boolean getRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public List<Bookgenre> getBookgenreList() {
        return bookgenreList;
    }

    public void setBookgenreList(List<Bookgenre> bookgenreList) {
        this.bookgenreList = bookgenreList;
    }

    public List<Bookformat> getBookformatList() {
        return bookformatList;
    }

    public void setBookformatList(List<Bookformat> bookformatList) {
        this.bookformatList = bookformatList;
    }

    public List<Bookauthor> getBookauthorList() {
        return bookauthorList;
    }

    public void setBookauthorList(List<Bookauthor> bookauthorList) {
        this.bookauthorList = bookauthorList;
    }

    public List<Orderitems> getOrderitemsList() {
        return orderitemsList;
    }

    public void setOrderitemsList(List<Orderitems> orderitemsList) {
        this.orderitemsList = orderitemsList;
    }

    public Publishers getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Publishers publisherId) {
        this.publisherId = publisherId;
    }

    public List<Reviews> getReviewsList() {
        return reviewsList;
    }

    public void setReviewsList(List<Reviews> reviewsList) {
        this.reviewsList = reviewsList;
    }

    /**
     * Takes into consideration the isbn only.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.isbn);
        return hash;
    }

    /**
     * Takes into consideration the isbn only.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Books other = (Books) obj;
        if (!Objects.equals(this.isbn, other.isbn)) {
            return false;
        }
        return true;
    }

    /**
     * Made full if statements, as for some reason, ternary operators didn't
     * work for us.
     *
     * @return String reprensation of this object
     * @author Camillia E., Eira G.
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Books{");
        if (isbn != null) {
            sb.append("isbn=" + isbn);
        }
        if (title != null) {
            if (title.length() > 23) {
                sb.append(", title=" + title.substring(0, 20) + "...");
            } else {
                sb.append(", title=" + title);
            }
        }
        if (publisherId != null) {
            if (publisherId.getPublisherId() != null) {
                sb.append(", publisherId=").append(publisherId.getPublisherId());
            } else {
                sb.append(", publisherId=IdNotAvailable");
            }
        } else {
            sb.append(", publisherId=" + "NoAssociatedPublisher");
        }
        if (pubDate != null) {
            sb.append(", pubDate=" + pubDate);
        }
        sb.append(", numPages=" + numPages);
        sb.append(", minAge=" + minAge);
        if (maxAge != null) {
            sb.append(", maxAge=" + maxAge);
        }
        if (description != null) {
            if (description.length() > 23) {
                sb.append(", description=" + description.substring(0, 20) + "...");
            } else {
                sb.append(", description=" + description);
            }
        }
        if (wholesalePrice != null) {
            if (bigDecimalHandler == null) {
                bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
            }
            sb.append(", wholesalePrice=" + bigDecimalHandler.getScaledString(wholesalePrice));
        }
        if (listPrice != null) {
            if (bigDecimalHandler == null) {
                bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
            }
            sb.append(", listPrice=" + bigDecimalHandler.getScaledString(listPrice));
        }

        if (salePrice != null) {
            if (bigDecimalHandler == null) {
                bigDecimalHandler = new BigDecimalHandler(Constants.PRICE_SCALE);
            }
            sb.append(", salePrice=" + bigDecimalHandler.getScaledString(salePrice));
        }

        if (acquiredStamp != null) {
            sb.append(", acquiredStamp=" + acquiredStamp);
        }
        sb.append(", removed=" + removed);

        if (bookgenreList != null) {
            sb.append(", bookgenreList=" + bookgenreList);
        }
        if (bookformatList != null) {
            sb.append(", bookformatList=" + bookformatList);
        }
        if (bookauthorList != null) {
            sb.append(", bookauthorList=" + bookgenreList);
        }
        if (orderitemsList != null) {
            sb.append(", orderitemsList=" + bookgenreList);
        }
        if (reviewsList != null) {
            sb.append(", reviewsList=" + bookgenreList);
        }
        sb.append('}');
        return sb.toString();
    }

    public String toShortString() {
        return "{Books isbn=" + isbn + "}";
    }

    public String toVeryShortString() {
        return "{Books}";
    }
}
