package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "genres", catalog = "ga1w20")
public class Genres implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "genre_id")
    private Integer genreId;

    @NotNull
    @Size(max = 60)
    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "genreId")
    private List<Bookgenre> bookgenreList;

    public Genres() {
    }

    public Genres(Integer genreId) {
        this.genreId = genreId;
    }

    public Genres(Integer genreId, String name) {
        this.genreId = genreId;
        this.name = name;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Bookgenre> getBookgenreList() {
        return bookgenreList;
    }

    public void setBookgenreList(List<Bookgenre> bookgenreList) {
        this.bookgenreList = bookgenreList;
    }

    /**
     * Case-insensitive computation.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        if (this.genreId != null) {
            hash = 47 * hash + Objects.hashCode(this.genreId);
        }
        if (this.name != null) {
            hash = 47 * hash + Objects.hashCode(this.name.toLowerCase());
        }
        return hash;
    }

    /**
     * Case-insensitive comparison. Does not compare list fields.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Genres other = (Genres) obj;

        if (!Objects.equals(this.genreId, other.genreId)) {

            return false;
        }
        if (!Objects.equals(this.name, other.name)
                && (this.name == null || other.name == null
                || !this.name.equalsIgnoreCase(other.name))) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Genres{" + "genreId=" + genreId + ", name=" + name
                + ", bookgenreList=" + bookgenreList + '}';
    }
}
