package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bookgenre", catalog = "ga1w20")
public class Bookgenre implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bookgenre_id")
    private Integer bookgenreId;
    @JoinColumn(name = "isbn", referencedColumnName = "isbn")
    @ManyToOne
    private Books isbn;
    @JoinColumn(name = "genre_id", referencedColumnName = "genre_id")
    @ManyToOne(optional = false)
    private Genres genreId;

    public Bookgenre() {
    }

    public Bookgenre(Integer bookgenreId) {
        this.bookgenreId = bookgenreId;
    }

    public Bookgenre(Books isbn, Genres genreId) {
        this.isbn = isbn;
        this.genreId = genreId;
    }

    public Integer getBookgenreId() {
        return bookgenreId;
    }

    public void setBookgenreId(Integer bookgenreId) {
        this.bookgenreId = bookgenreId;
    }

    public Books getIsbn() {
        return isbn;
    }

    public void setIsbn(Books isbn) {
        this.isbn = isbn;
    }

    public Genres getGenreId() {
        return genreId;
    }

    public void setGenreId(Genres genreId) {
        this.genreId = genreId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookgenreId != null ? bookgenreId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookgenre)) {
            return false;
        }
        Bookgenre other = (Bookgenre) object;
        if ((this.bookgenreId == null && other.bookgenreId != null) || (this.bookgenreId != null && !this.bookgenreId.equals(other.bookgenreId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bookgenre[ bookgenreId=" + bookgenreId + " ]";
    }

}
