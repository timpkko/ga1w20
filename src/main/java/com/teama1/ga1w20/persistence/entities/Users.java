package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "users", catalog = "ga1w20")
public class Users implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(Users.class);

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_id")
    private Integer userId;

    //Email must have at least one character followed by a '@' and at
    //least another character, a dot, and at least another character
    @Pattern(regexp = "^(.+)@(.+).(.+)$")
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "email")
    private String email;

    @Size(max = 60)
    @Column(name = "title")
    private String title;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "lastname")
    private String lastname;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "firstname")
    private String firstname;

    @Size(max = 150)
    @Column(name = "companyname")
    private String companyname;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "address1")
    private String address1;

    @Size(max = 300)
    @Column(name = "address2")
    private String address2;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "city")
    private String city;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "province")
    private String province;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "country")
    private String country;

    @Basic(optional = false)
    @NotNull
    @Size(min = 6, max = 6)
    @Column(name = "postalcode")
    private String postalcode;

    @Size(max = 20)
    @Column(name = "homephone")
    private String homephone;

    @Size(max = 20)
    @Column(name = "cellphone")
    private String cellphone;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "users")
    private Credentials credentials;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<Reviews> reviewsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<Orders> ordersList;

    @JoinColumn(name = "province", referencedColumnName = "region", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Taxrates taxrates;

    public Users() {
    }

    public Users(Integer userId) {
        this.userId = userId;
    }

    public Users(Integer userId, String email) {
        this.userId = userId;
    }

    public Users(Integer userId, String email, String lastname, String firstname, String address1, String city, String province, String country, String postalcode) {
        this.userId = userId;
        this.email = email;
        this.lastname = lastname;
        this.firstname = firstname;
        this.address1 = address1;
        this.city = city;
        this.province = province;
        this.country = country;
        this.postalcode = postalcode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getHomephone() {
        return homephone;
    }

    public void setHomephone(String homephone) {
        this.homephone = homephone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public List<Reviews> getReviewsList() {
        return reviewsList;
    }

    public void setReviewsList(List<Reviews> reviewsList) {
        this.reviewsList = reviewsList;
    }

    public List<Orders> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    public Taxrates getTaxrates() {
        return taxrates;
    }

    public void setTaxrates(Taxrates taxrates) {
        this.taxrates = taxrates;
    }

    /**
     * Only considers the userId and email. Case-insensitive computation. Null
     * fields - safe.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.userId);
        if (this.email != null) {
            hash = 59 * hash + Objects.hashCode(this.email.toLowerCase());
        }
        return hash;
    }

    /**
     * Only considers the userId and email. Case-insensitive computation.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Users other = (Users) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)
                && (this.email == null || other.email == null
                || !this.email.equalsIgnoreCase(other.email))) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        String managerMsg = "No associated credentials";
        if (this.credentials != null) {
            managerMsg = this.credentials.getManager() + "";
        }
        return "Users{" + "userId=" + userId
                + ", email=" + email
                + ", lastname=" + lastname
                + ", firstname=" + firstname
                + ", title=" + title
                + ", manager=" + managerMsg
                + ", companyname=" + companyname
                + ", address1=" + address1
                + ", address2=" + address2
                + ", city=" + city
                + ", province=" + province
                + ", country=" + country
                + ", postalcode=" + postalcode
                + ", homephone=" + homephone
                + ", cellphone=" + cellphone
                + ", reviewsList=" + reviewsList
                + ", ordersList=" + ordersList
                + ", taxrates=" + taxrates + '}';
    }
}
