package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "rssfeeds", catalog = "ga1w20")
public class Rssfeeds implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "feed_id")
    private Integer feedId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2083)
    @Column(name = "url")
    private String url;

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    public Rssfeeds() {
    }

    public Rssfeeds(Integer feedId) {
        this.feedId = feedId;
    }

    public Rssfeeds(Integer feedId, String url, boolean active) {
        this.feedId = feedId;
        this.url = url;
        this.active = active;
    }

    public Integer getFeedId() {
        return feedId;
    }

    public void setFeedId(Integer feedId) {
        this.feedId = feedId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Does not compare list fields or the approved field. Case-insensitive
     * computation. Null fields-safe.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        if (this.feedId != null) {
            hash = 89 * hash + Objects.hashCode(this.feedId);
        }
        if (this.url != null) {
            hash = 89 * hash + Objects.hashCode(this.url.toLowerCase());
        }
        return hash;
    }

    /**
     * Does not compare list fields or the approved field. Case-insensitive
     * comparison. Null fields-safe.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rssfeeds other = (Rssfeeds) obj;
        if (!Objects.equals(this.feedId, other.feedId)) {
            return false;
        }
        if (!Objects.equals(this.url, other.url)
                && (this.url == null || other.url == null
                || !this.url.equalsIgnoreCase(other.url))) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Rssfeeds{"
                + "feedId=" + feedId
                + ", url=" + url
                + ", active=" + active + '}';
    }
}
