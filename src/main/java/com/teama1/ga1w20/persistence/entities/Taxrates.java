package com.teama1.ga1w20.persistence.entities;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "taxrates", catalog = "ga1w20")
public class Taxrates implements Serializable {

    //Handler to compare and print the rates fields
    @Transient
    private BigDecimalHandler bigDecimalHandler;

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "region")
    private String region;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "gst_rate")
    private BigDecimal gstRate;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "pst_rate")
    private BigDecimal pstRate;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "hst_rate")
    private BigDecimal hstRate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taxrates")
    private List<Users> usersList;

    public Taxrates() {
    }

    public Taxrates(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public BigDecimal getPstRate() {
        return pstRate;
    }

    public void setPstRate(BigDecimal pstRate) {
        this.pstRate = pstRate;
    }

    public BigDecimal getHstRate() {
        return hstRate;
    }

    public void setHstRate(BigDecimal hstRate) {
        this.hstRate = hstRate;
    }

    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    /**
     * Only considers the region field. Case-insensitive comparison. Null
     * fields-safe.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        if (this.region != null) {
            hash = 83 * hash + Objects.hashCode(this.region.toLowerCase());
        }
        return hash;
    }

    /**
     * Only considers the region field. Case-insensitive comparison.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Taxrates other = (Taxrates) obj;
        if (!Objects.equals(this.region, other.region)
                && (this.region == null || other.region == null
                || !this.region.equalsIgnoreCase(other.region))) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.RATE_SCALE);
        }
        return "Taxrates{" + "region=" + region
                + ", gstRate=" + bigDecimalHandler.getScaledString(gstRate)
                + ", pstRate=" + bigDecimalHandler.getScaledString(pstRate)
                + ", hstRate=" + bigDecimalHandler.getScaledString(hstRate)
                + ", usersList=" + usersList + '}';
    }
}
