package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "formats", catalog = "ga1w20")
public class Formats implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "format_id")
    private Integer formatId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "extension")
    private String extension;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "formatId")
    private List<Bookformat> bookformatList;

    public Formats() {
    }

    public Formats(Integer formatId) {
        this.formatId = formatId;
    }

    public Formats(Integer formatId, String extension, String name) {
        this.formatId = formatId;
        this.extension = extension;
        this.name = name;
    }

    public Integer getFormatId() {
        return formatId;
    }

    public void setFormatId(Integer formatId) {
        this.formatId = formatId;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Bookformat> getBookformatList() {
        return bookformatList;
    }

    public void setBookformatList(List<Bookformat> bookformatList) {
        this.bookformatList = bookformatList;
    }

    /**
     * Takes into consideration the formatId and extension fields. Null-safe.
     * Case-insensitive computation.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        if (this.formatId != null) {
            hash = 89 * hash + Objects.hashCode(this.formatId);
        }
        if (this.extension != null) {
            hash = 89 * hash + Objects.hashCode(this.extension.toLowerCase());
        }
        return hash;
    }

    /**
     *
     * Takes into consideration the formatId and extension fields.
     * Case-insensitive comparison.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Formats other = (Formats) obj;
        if (!Objects.equals(this.formatId, other.formatId)) {
            return false;
        }
        if (!Objects.equals(this.extension, other.extension)
                && (this.extension == null || other.extension == null
                || !this.extension.equalsIgnoreCase(other.extension))) {
            return false;
        }

        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Formats{" + "formatId=" + formatId
                + ", extension=" + extension
                + ", name=" + name
                + ", bookformatList=" + bookformatList + '}';
    }
}
