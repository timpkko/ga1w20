package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Table(name = "authors", catalog = "ga1w20")
public class Authors implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(Authors.class);

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "author_id")
    private Integer authorId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "firstname")
    private String firstname;

    @Size(max = 60)
    @Column(name = "lastname")
    private String lastname;

    @Size(max = 20)
    @Column(name = "title")
    private String title;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "authorId")
    private List<Bookauthor> bookauthorList;

    public Authors() {
    }

    public Authors(Integer authorId) {
        this.authorId = authorId;
    }

    public Authors(Integer authorId, String firstname) {
        this.authorId = authorId;
        this.firstname = firstname;
    }

    public Authors(Integer authorId, String firstname, String lastname) {
        this.authorId = authorId;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Authors(Integer authorId, String firstname, String lastname, String title) {
        this.authorId = authorId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.title = title;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Bookauthor> getBookauthorList() {
        return bookauthorList;
    }

    public void setBookauthorList(List<Bookauthor> bookauthorList) {
        this.bookauthorList = bookauthorList;
    }

    /**
     * @return String representation of this object. Null fields -safe.
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Authors{" + "authorId=" + authorId
                + ", firstname=" + firstname
                + ", lastname=" + lastname + ", title="
                + title + ", bookauthorList="
                + bookauthorList + '}';
    }

    /**
     * Case-insensitive computation. Takes in consideration, id, firstname,
     * lastname. Not title. Null fields -safe.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        if (this.authorId != null) {
            hash = 41 * hash + Objects.hashCode(this.authorId);
        }
        if (this.firstname != null) {
            hash = 41 * hash + Objects.hashCode(this.firstname.toLowerCase());
        }
        if (this.lastname != null) {
            hash = 41 * hash + Objects.hashCode(this.lastname.toLowerCase());
        }
        return hash;
    }

    /**
     * Case-insensitive comparison. Does not compare lists. Takes in
     * consideration, id, firstname, lastname. Not title. Null fields -safe.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Authors other = (Authors) obj;
        if (!Objects.equals(this.authorId, other.authorId)) {
            return false;
        }

        if (!Objects.equals(this.firstname, other.firstname)
                && !this.firstname.equalsIgnoreCase(other.firstname)) {
            return false;
        }
        if (!Objects.equals(this.lastname, other.lastname)
                && !this.lastname.equalsIgnoreCase(other.lastname)) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)
                && !this.title.equalsIgnoreCase(other.title)) {
            return false;
        }
        return true;
    }
}
