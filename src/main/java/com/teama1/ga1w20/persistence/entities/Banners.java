package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "banners")
public class Banners implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ad_id")
    private Integer adId;

    @Basic(optional = false)
    @NotNull
    @Size(max = 2083)
    @Column(name = "url")
    private String url;

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    public Banners() {
    }

    public Banners(Integer adId) {
        this.adId = adId;
    }

    public Banners(Integer adId, boolean active) {
        this.adId = adId;
        this.active = active;
    }

    public Banners(Integer adId, String url) {
        this.adId = adId;
        this.url = url;
        this.active = false;
    }

    public Banners(Integer adId, String url, boolean active) {
        this.adId = adId;
        this.active = active;
        this.url = url;
    }

    public Integer getAdId() {
        return adId;
    }

    public void setAdId(Integer adId) {
        this.adId = adId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Case-insensitive computation. Takes into consideration id and url. Not
     * the active field. Null fields - safe.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        if (this.adId != null) {
            hash = 83 * hash + Objects.hashCode(this.adId);
        }
        if (this.url != null) {
            hash = 83 * hash + Objects.hashCode(this.url.toLowerCase());
        }
        return hash;
    }

    /**
     * Case-insensitive comparison. Does not compare lists. Takes into
     * consideration id and url. Not the active field.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Banners other = (Banners) obj;
        if (!Objects.equals(this.adId, other.adId)) {
            return false;
        }
        if (!Objects.equals(this.url, other.url)
                && (this.url == null || other.url == null
                || !this.url.equalsIgnoreCase(other.url))) {
            return false;
        }

        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Banners{" + "adId=" + adId
                + ", url=" + url + ", active=" + active + '}';
    }

}
