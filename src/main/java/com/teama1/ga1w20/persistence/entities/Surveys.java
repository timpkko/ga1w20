package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "surveys", catalog = "ga1w20")
public class Surveys implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "survey_id")
    private Integer surveyId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 400)
    @Column(name = "question")
    private String question;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "answer_1")
    private String answer1;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "answer_2")
    private String answer2;

    @Size(max = 100)
    @Column(name = "answer_3")
    private String answer3;

    @Size(max = 100)
    @Column(name = "answer_4")
    private String answer4;

    @Column(name = "vote_1")
    private Integer vote1;

    @Column(name = "vote_2")
    private Integer vote2;

    @Column(name = "vote_3")
    private Integer vote3;

    @Column(name = "vote_4")
    private Integer vote4;

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    public Surveys() {
    }

    public Surveys(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public Surveys(Integer surveyId, String question, String answer1, String answer2, boolean active) {
        this.surveyId = surveyId;
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.active = active;
    }

    public Integer getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Integer surveyId) {
        this.surveyId = surveyId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public Integer getVote1() {
        return vote1;
    }

    public void setVote1(Integer vote1) {
        this.vote1 = vote1;
    }

    public Integer getVote2() {
        return vote2;
    }

    public void setVote2(Integer vote2) {
        this.vote2 = vote2;
    }

    public Integer getVote3() {
        return vote3;
    }

    public void setVote3(Integer vote3) {
        this.vote3 = vote3;
    }

    public Integer getVote4() {
        return vote4;
    }

    public void setVote4(Integer vote4) {
        this.vote4 = vote4;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Considers the surveyId, the question, the answers. No other fields.
     * Case-insensitive comparison for string fields. Null fields - safe.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        if (this.surveyId != null) {
            hash = 97 * hash + Objects.hashCode(this.surveyId);
        }
        if (this.question != null) {
            hash = 97 * hash + Objects.hashCode(this.question.toLowerCase());
        }
        if (this.answer1 != null) {
            hash = 97 * hash + Objects.hashCode(this.answer1.toLowerCase());
        }
        if (this.answer2 != null) {
            hash = 97 * hash + Objects.hashCode(this.answer2.toLowerCase());
        }
        if (this.answer3 != null) {
            hash = 97 * hash + Objects.hashCode(this.answer3.toLowerCase());
        }
        if (this.answer4 != null) {
            hash = 97 * hash + Objects.hashCode(this.answer4.toLowerCase());
        }
        return hash;
    }

    /**
     * Considers the surveyId, the question, the answers. No other fields.
     * Case-insensitive comparison for string fields. Null fields - safe.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Surveys other = (Surveys) obj;
        if (!Objects.equals(this.surveyId, other.surveyId)) {
            return false;
        }
        if (!Objects.equals(this.question, other.question)
                && (this.question == null || other.question == null
                || !this.question.equalsIgnoreCase(other.question))) {
            return false;
        }

        if (!Objects.equals(this.answer1, other.answer1)
                && (this.answer1 == null || other.answer1 == null
                || !this.answer1.equalsIgnoreCase(other.answer1))) {
            return false;
        }
        if (!Objects.equals(this.answer2, other.answer2)
                && (this.answer2 == null || other.answer2 == null
                || !this.answer2.equalsIgnoreCase(other.answer2))) {
            return false;
        }
        if (!Objects.equals(this.answer3, other.answer3)
                && (this.answer3 == null || other.answer3 == null
                || !this.answer3.equalsIgnoreCase(other.answer3))) {
            return false;
        }
        if (!Objects.equals(this.answer4, other.answer4)
                && (this.answer4 == null || other.answer4 == null
                || !this.answer4.equalsIgnoreCase(other.answer4))) {
            return false;
        }
        return true;
    }

    /**
     * Null fields - safe.
     *
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Surveys{" + "surveyId=" + surveyId
                + ", question=" + question
                + ", answer1=" + answer1
                + ", answer2=" + answer2
                + ", answer3=" + answer3
                + ", answer4=" + answer4
                + ", vote1=" + vote1
                + ", vote2=" + vote2
                + ", vote3=" + vote3
                + ", vote4=" + vote4
                + ", active=" + active + '}';
    }
}
