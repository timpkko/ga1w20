package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "publishers", catalog = "ga1w20")
public class Publishers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "publisher_id")
    private Integer publisherId;

    @NotNull
    @Size(max = 60)
    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "publisherId")
    private List<Books> booksList;

    public Publishers() {
    }

    public Publishers(Integer publisherId) {
        this.publisherId = publisherId;
    }

    public Publishers(Integer publisherId, String name) {
        this.publisherId = publisherId;
        this.name = name;
    }

    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Books> getBooksList() {
        return booksList;
    }

    public void setBooksList(List<Books> booksList) {
        this.booksList = booksList;
    }

    /**
     * Case-insensitive computation
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 5;
        if (this.publisherId != null) {
            hash = 71 * hash + Objects.hashCode(this.publisherId);
        }
        if (this.name != null) {
            hash = 71 * hash + Objects.hashCode(this.name.toLowerCase());
        }
        return hash;
    }

    /**
     * Case-insensitive comparison. Does not compare list fields.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Publishers other = (Publishers) obj;

        if (!Objects.equals(this.publisherId, other.publisherId)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)
                && (this.name == null || other.name == null
                || !this.name.equalsIgnoreCase(other.name))) {
            return false;
        }

        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        return "Publishers{" + "publisherId=" + publisherId
                + ", name=" + name + ", booksList=" + booksList + '}';
    }
}
