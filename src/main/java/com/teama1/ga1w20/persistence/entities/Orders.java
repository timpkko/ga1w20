package com.teama1.ga1w20.persistence.entities;

import com.teama1.ga1w20.BigDecimalHandler;
import com.teama1.ga1w20.Constants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "orders", catalog = "ga1w20")
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    //Handler to compare and print the price field
    @Transient
    private BigDecimalHandler bigDecimalHandler;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "order_id")
    private Integer orderId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "purchase_date")
    @Temporal(TemporalType.DATE)
    private Date purchaseDate;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "gst_rate")
    private BigDecimal gstRate;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "pst_rate")
    private BigDecimal pstRate;

    //Must be at least 0
    @Min(value = 0)
    @Column(name = "hst_rate")
    private BigDecimal hstRate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderId")
    private List<Orderitems> orderitemsList;

    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @ManyToOne(optional = false)
    private Users userId;

    public Orders() {
    }

    public Orders(Integer orderId) {
        this.orderId = orderId;
    }

    public Orders(Integer orderId, Date purchaseDate) {
        this.orderId = orderId;
        this.purchaseDate = purchaseDate;
    }

    public Orders(Integer orderId, Users userId, Date purchaseDate) {
        this.orderId = orderId;
        this.purchaseDate = purchaseDate;
        this.userId = userId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public BigDecimal getGstRate() {
        return gstRate;
    }

    public void setGstRate(BigDecimal gstRate) {
        this.gstRate = gstRate;
    }

    public BigDecimal getPstRate() {
        return pstRate;
    }

    public void setPstRate(BigDecimal pstRate) {
        this.pstRate = pstRate;
    }

    public BigDecimal getHstRate() {
        return hstRate;
    }

    public void setHstRate(BigDecimal hstRate) {
        this.hstRate = hstRate;
    }

    public List<Orderitems> getOrderitemsList() {
        return orderitemsList;
    }

    public void setOrderitemsList(List<Orderitems> orderitemsList) {
        this.orderitemsList = orderitemsList;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    /**
     * Does not consider tax rates or list fields. When considering the
     * associated user, just considers its id.
     *
     * @return
     * @author Camillia E.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.orderId);
        hash = 53 * hash + Objects.hashCode(this.purchaseDate);
        if (this.userId != null && this.userId.getUserId() != null) {
            hash = 53 * hash + Objects.hashCode(this.userId.getUserId());
        }
        return hash;
    }

    /**
     * Does not consider tax rates or list fields. When considering the
     * associated user, just considers its id.
     *
     * @param obj
     * @return
     * @author Camillia E.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Orders other = (Orders) obj;
        if (!Objects.equals(this.orderId, other.orderId)) {
            return false;
        }
        if (!Objects.equals(this.getUserId(), other.getUserId())
                && (this.getUserId() == null || other.getUserId() == null
                || this.getUserId().getUserId() == null || other.getUserId().getUserId() == null
                || !this.getUserId().getUserId().equals(other.getUserId().getUserId()))) {
            return false;
        }
        if (!Objects.equals(this.purchaseDate, other.purchaseDate)) {
            return false;
        }
        return true;
    }

    /**
     * @return String representation of this object
     * @author Camillia E., Eira G.
     */
    @Override
    public String toString() {
        if (bigDecimalHandler == null) {
            bigDecimalHandler = new BigDecimalHandler(Constants.RATE_SCALE);
        }
        String userIdMsg = "NoAssociatedUser";
        if (userId != null) {
            userIdMsg = "" + userId.getUserId();
        }
        return "Orders{" + "orderId=" + orderId
                + ", userId=" + userIdMsg
                + ", purchaseDate=" + purchaseDate
                + ", gstRate=" + bigDecimalHandler.getScaledString(gstRate)
                + ", pstRate=" + bigDecimalHandler.getScaledString(pstRate)
                + ", hstRate=" + bigDecimalHandler.getScaledString(hstRate)
                + ", orderitemsList=" + orderitemsList
                + '}';
    }
}
