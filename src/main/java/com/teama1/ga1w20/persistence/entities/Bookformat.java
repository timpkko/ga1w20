package com.teama1.ga1w20.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bookformat", catalog = "ga1w20")
public class Bookformat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bookformat_id")
    private Integer bookformatId;

    @JoinColumn(name = "isbn", referencedColumnName = "isbn")
    @ManyToOne(optional = false)
    private Books isbn;

    @JoinColumn(name = "format_id", referencedColumnName = "format_id")
    @ManyToOne(optional = false)
    private Formats formatId;

    public Bookformat() {
    }

    public Bookformat(Integer bookformatId) {
        this.bookformatId = bookformatId;
    }

    public Bookformat(Books isbn, Formats formatId) {
        this.isbn = isbn;
        this.formatId = formatId;
    }

    public Integer getBookformatId() {
        return bookformatId;
    }

    public void setBookformatId(Integer bookformatId) {
        this.bookformatId = bookformatId;
    }

    public Books getIsbn() {
        return isbn;
    }

    public void setIsbn(Books isbn) {
        this.isbn = isbn;
    }

    public Formats getFormatId() {
        return formatId;
    }

    public void setFormatId(Formats formatId) {
        this.formatId = formatId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookformatId != null ? bookformatId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookformat)) {
            return false;
        }
        Bookformat other = (Bookformat) object;
        if ((this.bookformatId == null && other.bookformatId != null)
                || (this.bookformatId != null && !this.bookformatId.equals(other.bookformatId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bookformat[ bookformatId=" + bookformatId + " ]";
    }

}
