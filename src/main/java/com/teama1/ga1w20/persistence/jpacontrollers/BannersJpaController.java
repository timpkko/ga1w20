package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Banners;
import com.teama1.ga1w20.persistence.entities.Banners_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@SessionScoped
public class BannersJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(BannersJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Banners banners) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            entityManager.persist(banners);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding the banner: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findBanners(banners.getAdId()) != null) {
                LOG.error("banner with id " + banners.getAdId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Banners banners) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            banners = entityManager.merge(banners);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when editing the banner: " + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = banners.getAdId();
            if (findBanners(id) == null) {
                LOG.error("The banner with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Banners banners;
            try {
                banners = entityManager.getReference(Banners.class, id);
                banners.getAdId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The banners with id "
                        + id + " does not exists."
                        + enfe.getLocalizedMessage() == null ? "" : enfe.getLocalizedMessage());
            }
            entityManager.remove(banners);
            userTransaction.commit();

        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException
                | SecurityException | HeuristicMixedException
                | HeuristicRollbackException | NotSupportedException
                | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the banner: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Banners> findBannersEntities() {
        return findBannersEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Banners> findBannersEntities(int maxResults, int firstResult) {
        return findBannersEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Banners> findBannersEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Banners> cq = entityManager.getCriteriaBuilder().createQuery(Banners.class);
        cq.select(cq.from(Banners.class));
        TypedQuery<Banners> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Banners findBanners(Integer id) {
        return entityManager.find(Banners.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getBannersCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Banners> rt = cq.from(Banners.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    ////////////////////////////////////////////////////////////////////////////
    //CUSTOM QUERIES
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Returns a List containing all currently active banners. If no banners are
     * active, list will be empty.
     *
     * @return A List of all active Banners.
     *
     * @author Eira Garrett
     */
    public List<Banners> getAllActiveBanners() {
        //SELECT * FROM banners
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Banners> cQuery = cBuilder.createQuery(Banners.class);
        Root<Banners> bannerTable = cQuery.from(Banners.class);

        //WHERE active = true
        cQuery.where(cBuilder.equal(bannerTable.get(Banners_.active), true));

        TypedQuery<Banners> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }
}
