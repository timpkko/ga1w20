package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Bookgenre;
import com.teama1.ga1w20.persistence.entities.Bookgenre_;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Books_;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.entities.Genres_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class GenresJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(GenresJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Genres genres) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        if (genres.getBookgenreList() == null) {
            genres.setBookgenreList(new ArrayList<Bookgenre>());
        }

        try {
            userTransaction.begin();
            List<Bookgenre> attachedBookgenreList = new ArrayList<Bookgenre>();
            for (Bookgenre bookgenreListBookgenreToAttach : genres.getBookgenreList()) {
                bookgenreListBookgenreToAttach = entityManager.getReference(bookgenreListBookgenreToAttach.getClass(), bookgenreListBookgenreToAttach.getBookgenreId());
                attachedBookgenreList.add(bookgenreListBookgenreToAttach);
            }
            genres.setBookgenreList(attachedBookgenreList);
            entityManager.persist(genres);
            for (Bookgenre bookgenreListBookgenre : genres.getBookgenreList()) {
                Genres oldGenreIdOfBookgenreListBookgenre = bookgenreListBookgenre.getGenreId();
                bookgenreListBookgenre.setGenreId(genres);
                bookgenreListBookgenre = entityManager.merge(bookgenreListBookgenre);
                if (oldGenreIdOfBookgenreListBookgenre != null) {
                    oldGenreIdOfBookgenreListBookgenre.getBookgenreList().remove(bookgenreListBookgenre);
                    oldGenreIdOfBookgenreListBookgenre = entityManager.merge(oldGenreIdOfBookgenreListBookgenre);
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException
                | HeuristicMixedException | HeuristicRollbackException
                | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding the genre: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findGenres(genres.getGenreId()) != null) {
                LOG.error("genre with id " + genres.getGenreId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Genres genres) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Genres persistentGenres = entityManager.find(Genres.class, genres.getGenreId());
            List<Bookgenre> bookgenreListOld = persistentGenres.getBookgenreList();
            List<Bookgenre> bookgenreListNew = genres.getBookgenreList();
            List<String> illegalOrphanMessages = null;
            for (Bookgenre bookgenreListOldBookgenre : bookgenreListOld) {
                if (!bookgenreListNew.contains(bookgenreListOldBookgenre)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Bookgenre " + bookgenreListOldBookgenre + " since its genreId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Bookgenre> attachedBookgenreListNew = new ArrayList<Bookgenre>();
            for (Bookgenre bookgenreListNewBookgenreToAttach : bookgenreListNew) {
                bookgenreListNewBookgenreToAttach = entityManager.getReference(bookgenreListNewBookgenreToAttach.getClass(), bookgenreListNewBookgenreToAttach.getBookgenreId());
                attachedBookgenreListNew.add(bookgenreListNewBookgenreToAttach);
            }
            bookgenreListNew = attachedBookgenreListNew;
            genres.setBookgenreList(bookgenreListNew);
            genres = entityManager.merge(genres);
            for (Bookgenre bookgenreListNewBookgenre : bookgenreListNew) {
                if (!bookgenreListOld.contains(bookgenreListNewBookgenre)) {
                    Genres oldGenreIdOfBookgenreListNewBookgenre = bookgenreListNewBookgenre.getGenreId();
                    bookgenreListNewBookgenre.setGenreId(genres);
                    bookgenreListNewBookgenre = entityManager.merge(bookgenreListNewBookgenre);
                    if (oldGenreIdOfBookgenreListNewBookgenre != null && !oldGenreIdOfBookgenreListNewBookgenre.equals(genres)) {
                        oldGenreIdOfBookgenreListNewBookgenre.getBookgenreList().remove(bookgenreListNewBookgenre);
                        oldGenreIdOfBookgenreListNewBookgenre = entityManager.merge(oldGenreIdOfBookgenreListNewBookgenre);
                    }
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the genre: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = genres.getGenreId();
            if (findGenres(id) == null) {
                throw new NonexistentEntityException("The genres with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Genres genres;
            try {
                genres = entityManager.getReference(Genres.class, id);
                genres.getGenreId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The genres with id " + id + " does not exist.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Bookgenre> bookgenreListOrphanCheck = genres.getBookgenreList();
            for (Bookgenre bookgenreListOrphanCheckBookgenre : bookgenreListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Genres (" + genres + ") cannot be destroyed since the Bookgenre " + bookgenreListOrphanCheckBookgenre + " in its bookgenreList field has a non-nullable genreId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            entityManager.remove(genres);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the genre: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findGenres(id) == null) {
                throw new NonexistentEntityException("The genres with id "
                        + id + " does not exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Genres> findGenresEntities() {
        return findGenresEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Genres> findGenresEntities(int maxResults, int firstResult) {
        return findGenresEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Genres> findGenresEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Genres> cq = entityManager.getCriteriaBuilder().createQuery(Genres.class);
        cq.select(cq.from(Genres.class));
        TypedQuery<Genres> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Genres findGenres(Integer id) {
        return entityManager.find(Genres.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getGenresCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Genres> rt = cq.from(Genres.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    //////////////////////////////////////////
    /////////// GET GENRES BY BOOK ///////////
    //////////////////////////////////////////
    /**
     * Retrieves the genres associated with a particular book.
     *
     * Native SQL Example Query: - SELECT * FROM genres JOIN bookgenre USING
     * (genre_id) JOIN books USING (isbn) WHERE books.isbn = 1235689728937489;
     *
     * @param book A Books object.
     *
     * @return A List of Genres associated with the book.
     *
     * @author Eira Garrett
     */
    public List<Genres> getGenresByBook(Books book) {
        Objects.requireNonNull(book, "book must be set");

        //SELECT * FROM genres
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Genres> cQuery = cBuilder.createQuery(Genres.class);
        Root<Genres> genresTable = cQuery.from(Genres.class);

        //JOIN bookgenre USING (genre_id) JOIN books USING (isbn)
        Join<Genres, Bookgenre> genresToBooks = genresTable.join(Genres_.bookgenreList);

        //WHERE books.isbn = :isbn
        Predicate matchingIsbn = cBuilder.equal(genresToBooks.get(Bookgenre_.isbn).get(Books_.isbn), book.getIsbn());
        cQuery.where(matchingIsbn);

        //Generate query
        TypedQuery<Genres> tq = entityManager.createQuery(cQuery);
        List<Genres> resultList = tq.getResultList();

        return resultList;
    }

    /**
     * Gets a list of the genres associated with a particular book. Searches by
     * ISBN.
     *
     * Native SQL Example Query: - SELECT * FROM genres JOIN bookgenre USING
     * (genre_id) JOIN books USING (isbn) WHERE books.isbn = 1235689728937489;
     *
     *
     * @param isbn A Long representing the ISBN of the book for which to search.
     *
     * @return A List of Genres.
     *
     * @author Eira Garrett
     */
    public List<Genres> getGenresByBook(Long isbn) {
        Objects.requireNonNull(isbn, "isbn must be set");

        //SELECT * FROM genres
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Genres> cQuery = cBuilder.createQuery(Genres.class);
        Root<Genres> genresTable = cQuery.from(Genres.class);

        //JOIN bookgenre USING (genre_id) JOIN books USING (isbn)
        Join<Genres, Bookgenre> genresToBooks = genresTable.join(Genres_.bookgenreList);

        //WHERE books.isbn = :isbn
        Predicate matchingIsbn = cBuilder.equal(genresToBooks.get(Bookgenre_.isbn).get(Books_.isbn), isbn);
        cQuery.where(matchingIsbn);

        //Generate query
        TypedQuery<Genres> tq = entityManager.createQuery(cQuery);
        List<Genres> resultList = tq.getResultList();

        return resultList;
    }
}
