package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.entities.Orderitems_;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.entities.Orders_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class OrderitemsJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(OrderitemsJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Orderitems orderitems) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Books isbn = orderitems.getIsbn();
            if (isbn != null) {
                isbn = entityManager.getReference(isbn.getClass(), isbn.getIsbn());
                orderitems.setIsbn(isbn);
            }
            Orders orderId = orderitems.getOrderId();
            if (orderId != null) {
                orderId = entityManager.getReference(orderId.getClass(), orderId.getOrderId());
                orderitems.setOrderId(orderId);
            }
            entityManager.persist(orderitems);
            if (isbn != null) {
                isbn.getOrderitemsList().add(orderitems);
                isbn = entityManager.merge(isbn);
            }
            if (orderId != null) {
                orderId.getOrderitemsList().add(orderitems);
                orderId = entityManager.merge(orderId);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding the record in Orderitems: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findOrderitems(orderitems.getOrderitemsId()) != null) {
                LOG.error("orderitem with id " + orderitems.getOrderitemsId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Orderitems orderitems) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Orderitems persistentOrderitems = entityManager.find(Orderitems.class, orderitems.getOrderitemsId());
            Books isbnOld = persistentOrderitems.getIsbn();
            Books isbnNew = orderitems.getIsbn();
            Orders orderIdOld = persistentOrderitems.getOrderId();
            Orders orderIdNew = orderitems.getOrderId();
            if (isbnNew != null) {
                isbnNew = entityManager.getReference(isbnNew.getClass(), isbnNew.getIsbn());
                orderitems.setIsbn(isbnNew);
            }
            if (orderIdNew != null) {
                orderIdNew = entityManager.getReference(orderIdNew.getClass(), orderIdNew.getOrderId());
                orderitems.setOrderId(orderIdNew);
            }
            orderitems = entityManager.merge(orderitems);
            if (isbnOld != null && !isbnOld.equals(isbnNew)) {
                isbnOld.getOrderitemsList().remove(orderitems);
                isbnOld = entityManager.merge(isbnOld);
            }
            if (isbnNew != null && !isbnNew.equals(isbnOld)) {
                isbnNew.getOrderitemsList().add(orderitems);
                isbnNew = entityManager.merge(isbnNew);
            }
            if (orderIdOld != null && !orderIdOld.equals(orderIdNew)) {
                orderIdOld.getOrderitemsList().remove(orderitems);
                orderIdOld = entityManager.merge(orderIdOld);
            }
            if (orderIdNew != null && !orderIdNew.equals(orderIdOld)) {
                orderIdNew.getOrderitemsList().add(orderitems);
                orderIdNew = entityManager.merge(orderIdNew);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the record in Orderitems: " + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = orderitems.getOrderitemsId();
            if (findOrderitems(id) == null) {
                throw new NonexistentEntityException("The orderitems with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Orderitems orderitems;
            try {
                orderitems = entityManager.getReference(Orderitems.class, id);
                orderitems.getOrderitemsId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orderitems with id "
                        + id + " does not exist.", enfe);
            }
            Books isbn = orderitems.getIsbn();
            if (isbn != null) {
                isbn.getOrderitemsList().remove(orderitems);
                isbn = entityManager.merge(isbn);
            }
            Orders orderId = orderitems.getOrderId();
            if (orderId != null) {
                orderId.getOrderitemsList().remove(orderitems);
                orderId = entityManager.merge(orderId);
            }
            entityManager.remove(orderitems);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the record in Orderitems: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Orderitems> findOrderitemsEntities() {
        return findOrderitemsEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Orderitems> findOrderitemsEntities(int maxResults, int firstResult) {
        return findOrderitemsEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Orderitems> findOrderitemsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Orderitems> cq = entityManager.getCriteriaBuilder().createQuery(Orderitems.class);
        cq.select(cq.from(Orderitems.class));
        TypedQuery<Orderitems> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Orderitems findOrderitems(Integer id) {
        return entityManager.find(Orderitems.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getOrderitemsCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Orderitems> rt = cq.from(Orderitems.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    //////////////////////////////////////////
    //////////////// CUSTOM //////////////////
    //////////////////////////////////////////
    /**
     * Retrieves the list of Orderitems associated to a given orderId.
     *
     * @param orderId The order Id.
     *
     * @return the list of Orderitems object.
     *
     * @author Michael Masciotra
     */
    public List<Orderitems> getOrderitemsFromOrderId(Integer orderId) {
        Objects.requireNonNull(orderId, "orderId must be set");

        //SELECT * FROM orders
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Orderitems> cQuery = cBuilder.createQuery(Orderitems.class);
        Root<Orders> ordersTable = cQuery.from(Orders.class);

        //JOIN orderitems USING (order_id)
        Join<Orders, Orderitems> ordersToOrderitems = ordersTable.join(Orders_.orderitemsList);

        //SELECT fields INTO Orderitems
        cQuery.select(cBuilder.construct(Orderitems.class, ordersToOrderitems.get(Orderitems_.orderitemsId),
                ordersToOrderitems.get(Orderitems_.price), ordersToOrderitems.get(Orderitems_.isbn),
                ordersToOrderitems.get(Orderitems_.orderId), ordersToOrderitems.get(Orderitems_.removed)));

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(ordersToOrderitems.get(Orderitems_.orderId).get(Orders_.orderId), orderId), cBuilder.isFalse(ordersToOrderitems.get(Orderitems_.removed)));

        TypedQuery<Orderitems> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Retrieves the list of Orderitems associated to a given orderId. Only the
     * non-removed ones.
     *
     * @param orderId The order Id.
     *
     * @return the list of Orderitems object.
     *
     * @author Michael Masciotra
     */
    public List<Orderitems> getOrderitemsFromOrderIdIncludeRemoved(Integer orderId) {
        Objects.requireNonNull(orderId, "orderId must be set");

        //SELECT * FROM orders
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Orderitems> cQuery = cBuilder.createQuery(Orderitems.class);
        Root<Orders> ordersTable = cQuery.from(Orders.class);

        //JOIN orderitems USING (order_id)
        Join<Orders, Orderitems> ordersToOrderitems = ordersTable.join(Orders_.orderitemsList);

        //SELECT fields INTO Orderitems
        cQuery.select(cBuilder.construct(Orderitems.class, ordersToOrderitems.get(Orderitems_.orderitemsId),
                ordersToOrderitems.get(Orderitems_.price), ordersToOrderitems.get(Orderitems_.isbn),
                ordersToOrderitems.get(Orderitems_.orderId), ordersToOrderitems.get(Orderitems_.removed)));

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(ordersToOrderitems.get(Orderitems_.orderId).get(Orders_.orderId), orderId));

        TypedQuery<Orderitems> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

}
