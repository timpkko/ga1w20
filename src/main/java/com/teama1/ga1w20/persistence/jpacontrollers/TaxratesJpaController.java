package com.teama1.ga1w20.persistence.jpacontrollers;
//CUSTOM QUERIES HERE

import com.teama1.ga1w20.persistence.entities.*;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@SessionScoped
public class TaxratesJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(TaxratesJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Taxrates taxrates) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            entityManager.persist(taxrates);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException
                | HeuristicMixedException | HeuristicRollbackException
                | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when adding the taxrate: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findTaxrates(taxrates.getRegion()) != null) {
                LOG.error("Taxrate " + taxrates.getRegion() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Taxrates taxrates) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            taxrates = entityManager.merge(taxrates);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException
                | HeuristicMixedException | HeuristicRollbackException
                | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the taxrate: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));
            String id = taxrates.getRegion();
            if (findTaxrates(id) == null) {
                throw new NonexistentEntityException("The taxrates with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(String id) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Taxrates taxrates;
            try {
                taxrates = entityManager.getReference(Taxrates.class, id);
                taxrates.getRegion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The taxrates with id " + id + " does not exist.", enfe);
            }
            entityManager.remove(taxrates);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the taxrate: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Taxrates> findTaxratesEntities() {
        return findTaxratesEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Taxrates> findTaxratesEntities(int maxResults, int firstResult) {
        return findTaxratesEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Taxrates> findTaxratesEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Taxrates> cq = entityManager.getCriteriaBuilder().createQuery(Taxrates.class);
        cq.select(cq.from(Taxrates.class));
        TypedQuery<Taxrates> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Taxrates findTaxrates(String id) {
        return entityManager.find(Taxrates.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getTaxratesCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Taxrates> rt = cq.from(Taxrates.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    //////////////////////////////////////////////
    ////////////// TAX RATE BY USER //////////////
    //////////////////////////////////////////////
    /**
     * Retrieves the tax rates associated with a given user.
     *
     * Native SQL Example Query: - SELECT * FROM taxrates JOIN users ON
     * taxrates.region = users.location WHERE users.user_id = 1;
     *
     * @param user A Users object to use as search terms.
     *
     * @return A Taxrates object containing all tax rates for the given user
     * based on their location.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Taxrates getRatesForUser(Users user) throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        Objects.requireNonNull(user, "User must not be null.");

        //SELECT * FROM taxrates
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Taxrates> cQuery = cBuilder.createQuery(Taxrates.class);
        Root<Taxrates> ratesTable = cQuery.from(Taxrates.class);

        //JOIN users ON taxrates.region = users.province
        Join<Taxrates, Users> ratesToUsers = ratesTable.join(Taxrates_.usersList);

        //WHERE users.user_id = :id
        cQuery.where(cBuilder.equal(ratesToUsers.get(Users_.userId), user.getUserId()));

        TypedQuery<Taxrates> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    /**
     * Retrieves the tax rates associated with a given user.
     *
     * Native SQL Example Query: - SELECT * FROM taxrates JOIN users ON
     * taxrates.region = users.location WHERE users.user_id = 1;
     *
     * @param userId The ID number associated with a user.
     *
     * @return A Taxrates object containing all tax rates for the given user
     * based on their location.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Taxrates getRatesForUser(Integer userId) throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        Objects.requireNonNull(userId, "User ID must not be null.");

        //SELECT * FROM taxrates
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Taxrates> cQuery = cBuilder.createQuery(Taxrates.class);
        Root<Taxrates> ratesTable = cQuery.from(Taxrates.class);

        //JOIN users ON taxrates.region = users.location
        Join<Taxrates, Users> ratesToUsers = ratesTable.join(Taxrates_.usersList);

        //WHERE users.user_id = :id
        cQuery.where(cBuilder.equal(ratesToUsers.get(Users_.userId), userId));

        TypedQuery<Taxrates> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    ////////////////////////////////////////
    ////////// TAX RATE BY REGION //////////
    ////////////////////////////////////////
    /**
     * Retrieves the taxrates object associated with a given province. Case
     * insensitive.
     *
     * Native SQL Example Query: - SELECT * FROM taxrates WHERE region = "PEI";
     *
     * Valid example inputs include: - PEI - pei
     *
     * @param province A String representing the province for which to search.
     *
     * @return The Taxrates object associated with the province in question, if
     * it exists.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Taxrates getRateByRegion(String province) throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        Objects.requireNonNull(province, "Province must not be null.");
        province = province.trim();
        //SELECT * FROM taxrates
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Taxrates> cQuery = cBuilder.createQuery(Taxrates.class);
        Root<Taxrates> ratesTable = cQuery.from(Taxrates.class);

        //WHERE region = :province
        cQuery.where(
                cBuilder.equal(
                        cBuilder.lower(ratesTable.get(Taxrates_.region)),
                        province.toLowerCase()
                )
        );

        TypedQuery<Taxrates> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }
}
