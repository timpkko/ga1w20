package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Bookauthor;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class BookauthorJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(BookauthorJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Bookauthor bookauthor) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Authors authorId = bookauthor.getAuthorId();
            if (authorId != null) {
                authorId = entityManager.getReference(authorId.getClass(), authorId.getAuthorId());
                bookauthor.setAuthorId(authorId);
            }
            Books isbn = bookauthor.getIsbn();
            if (isbn != null) {
                isbn = entityManager.getReference(isbn.getClass(), isbn.getIsbn());
                bookauthor.setIsbn(isbn);
            }
            entityManager.persist(bookauthor);
            if (authorId != null) {
                authorId.getBookauthorList().add(bookauthor);
                authorId = entityManager.merge(authorId);
            }
            if (isbn != null) {
                isbn.getBookauthorList().add(bookauthor);
                isbn = entityManager.merge(isbn);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding a record in Bookauthor: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findBookauthor(bookauthor.getBookauthorId()) != null) {
                LOG.error("bookauthor with id " + bookauthor.getBookauthorId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Bookauthor bookauthor) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Bookauthor persistentBookauthor = entityManager.find(Bookauthor.class, bookauthor.getBookauthorId());
            Authors authorIdOld = persistentBookauthor.getAuthorId();
            Authors authorIdNew = bookauthor.getAuthorId();
            Books isbnOld = persistentBookauthor.getIsbn();
            Books isbnNew = bookauthor.getIsbn();
            if (authorIdNew != null) {
                authorIdNew = entityManager.getReference(authorIdNew.getClass(), authorIdNew.getAuthorId());
                bookauthor.setAuthorId(authorIdNew);
            }
            if (isbnNew != null) {
                isbnNew = entityManager.getReference(isbnNew.getClass(), isbnNew.getIsbn());
                bookauthor.setIsbn(isbnNew);
            }
            bookauthor = entityManager.merge(bookauthor);
            if (authorIdOld != null && !authorIdOld.equals(authorIdNew)) {
                authorIdOld.getBookauthorList().remove(bookauthor);
                authorIdOld = entityManager.merge(authorIdOld);
            }
            if (authorIdNew != null && !authorIdNew.equals(authorIdOld)) {
                authorIdNew.getBookauthorList().add(bookauthor);
                authorIdNew = entityManager.merge(authorIdNew);
            }
            if (isbnOld != null && !isbnOld.equals(isbnNew)) {
                isbnOld.getBookauthorList().remove(bookauthor);
                isbnOld = entityManager.merge(isbnOld);
            }
            if (isbnNew != null && !isbnNew.equals(isbnOld)) {
                isbnNew.getBookauthorList().add(bookauthor);
                isbnNew = entityManager.merge(isbnNew);
            }
            userTransaction.commit();

        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when editing the record in Bookauthor: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = bookauthor.getBookauthorId();
            if (findBookauthor(id) == null) {
                LOG.error("The bookauthor with id " + id + " does not exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Bookauthor bookauthor;
            try {
                bookauthor = entityManager.getReference(Bookauthor.class, id);
                bookauthor.getBookauthorId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bookauthor with id " + id + " does not exists.", enfe);
            }
            Authors authorId = bookauthor.getAuthorId();
            if (authorId != null) {
                authorId.getBookauthorList().remove(bookauthor);
                authorId = entityManager.merge(authorId);
            }
            Books isbn = bookauthor.getIsbn();
            if (isbn != null) {
                isbn.getBookauthorList().remove(bookauthor);
                isbn = entityManager.merge(isbn);
            }
            entityManager.remove(bookauthor);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException
                | HeuristicMixedException | HeuristicRollbackException | NotSupportedException
                | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the record in Bookauthor: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Bookauthor> findBookauthorEntities() {
        return findBookauthorEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Bookauthor> findBookauthorEntities(int maxResults, int firstResult) {
        return findBookauthorEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Bookauthor> findBookauthorEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Bookauthor> cq = entityManager.getCriteriaBuilder().createQuery(Bookauthor.class);
        cq.select(cq.from(Bookauthor.class));
        TypedQuery<Bookauthor> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Bookauthor findBookauthor(Integer id) {
        return entityManager.find(Bookauthor.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getBookauthorCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Bookauthor> rt = cq.from(Bookauthor.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

}
