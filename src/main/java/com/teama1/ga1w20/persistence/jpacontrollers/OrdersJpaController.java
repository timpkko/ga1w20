package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Books_;
import com.teama1.ga1w20.persistence.entities.Orderitems;
import com.teama1.ga1w20.persistence.entities.Orderitems_;
import com.teama1.ga1w20.persistence.entities.Orders;
import com.teama1.ga1w20.persistence.entities.Orders_;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.entities.Users_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class OrdersJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(OrdersJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Orders orders) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {

        if (orders.getOrderitemsList() == null) {
            orders.setOrderitemsList(new ArrayList<Orderitems>());
        }

        try {
            userTransaction.begin();
            Users userId = orders.getUserId();
            if (userId != null) {
                userId = entityManager.getReference(userId.getClass(), userId.getUserId());
                orders.setUserId(userId);
            }
            List<Orderitems> attachedOrderitemsList = new ArrayList<Orderitems>();
            for (Orderitems orderitemsListOrderitemsToAttach : orders.getOrderitemsList()) {
                orderitemsListOrderitemsToAttach = entityManager.getReference(orderitemsListOrderitemsToAttach.getClass(), orderitemsListOrderitemsToAttach.getOrderitemsId());
                attachedOrderitemsList.add(orderitemsListOrderitemsToAttach);
            }
            orders.setOrderitemsList(attachedOrderitemsList);
            entityManager.persist(orders);
            if (userId != null) {
                userId.getOrdersList().add(orders);
                userId = entityManager.merge(userId);
            }
            for (Orderitems orderitemsListOrderitems : orders.getOrderitemsList()) {
                Orders oldOrderIdOfOrderitemsListOrderitems = orderitemsListOrderitems.getOrderId();
                orderitemsListOrderitems.setOrderId(orders);
                orderitemsListOrderitems = entityManager.merge(orderitemsListOrderitems);
                if (oldOrderIdOfOrderitemsListOrderitems != null) {
                    oldOrderIdOfOrderitemsListOrderitems.getOrderitemsList().remove(orderitemsListOrderitems);
                    oldOrderIdOfOrderitemsListOrderitems = entityManager.merge(oldOrderIdOfOrderitemsListOrderitems);
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when adding the order: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findOrders(orders.getOrderId()) != null) {
                LOG.error("order with id " + orders.getOrderId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("An error occurred attempting to roll  back the transaction." + re.getMessage());
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Orders orders) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Orders persistentOrders = entityManager.find(Orders.class, orders.getOrderId());
            Users userIdOld = persistentOrders.getUserId();
            Users userIdNew = orders.getUserId();
            List<Orderitems> orderitemsListOld = persistentOrders.getOrderitemsList();
            List<Orderitems> orderitemsListNew = orders.getOrderitemsList();
            List<String> illegalOrphanMessages = null;
            for (Orderitems orderitemsListOldOrderitems : orderitemsListOld) {
                if (!orderitemsListNew.contains(orderitemsListOldOrderitems)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Orderitems " + orderitemsListOldOrderitems + " since its orderId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (userIdNew != null) {
                userIdNew = entityManager.getReference(userIdNew.getClass(), userIdNew.getUserId());
                orders.setUserId(userIdNew);
            }
            List<Orderitems> attachedOrderitemsListNew = new ArrayList<Orderitems>();
            for (Orderitems orderitemsListNewOrderitemsToAttach : orderitemsListNew) {
                orderitemsListNewOrderitemsToAttach = entityManager.getReference(orderitemsListNewOrderitemsToAttach.getClass(), orderitemsListNewOrderitemsToAttach.getOrderitemsId());
                attachedOrderitemsListNew.add(orderitemsListNewOrderitemsToAttach);
            }
            orderitemsListNew = attachedOrderitemsListNew;
            orders.setOrderitemsList(orderitemsListNew);
            orders = entityManager.merge(orders);
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getOrdersList().remove(orders);
                userIdOld = entityManager.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getOrdersList().add(orders);
                userIdNew = entityManager.merge(userIdNew);
            }
            for (Orderitems orderitemsListNewOrderitems : orderitemsListNew) {
                if (!orderitemsListOld.contains(orderitemsListNewOrderitems)) {
                    Orders oldOrderIdOfOrderitemsListNewOrderitems = orderitemsListNewOrderitems.getOrderId();
                    orderitemsListNewOrderitems.setOrderId(orders);
                    orderitemsListNewOrderitems = entityManager.merge(orderitemsListNewOrderitems);
                    if (oldOrderIdOfOrderitemsListNewOrderitems != null && !oldOrderIdOfOrderitemsListNewOrderitems.equals(orders)) {
                        oldOrderIdOfOrderitemsListNewOrderitems.getOrderitemsList().remove(orderitemsListNewOrderitems);
                        oldOrderIdOfOrderitemsListNewOrderitems = entityManager.merge(oldOrderIdOfOrderitemsListNewOrderitems);
                    }
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the order: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = orders.getOrderId();
            if (findOrders(id) == null) {
                throw new NonexistentEntityException("The orders with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("An error occurred attempting to roll  back the transaction." + re.getMessage());
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Orders orders;
            try {
                orders = entityManager.getReference(Orders.class, id);
                orders.getOrderId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orders with id " + id + " does not exist.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Orderitems> orderitemsListOrphanCheck = orders.getOrderitemsList();
            for (Orderitems orderitemsListOrphanCheckOrderitems : orderitemsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Orders (" + orders + ") cannot be destroyed since the Orderitems " + orderitemsListOrphanCheckOrderitems + " in its orderitemsList field has a non-nullable orderId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Users userId = orders.getUserId();
            if (userId != null) {
                userId.getOrdersList().remove(orders);
                userId = entityManager.merge(userId);
            }
            entityManager.remove(orders);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when deleting the order: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("An error occurred attempting to roll  back the transaction." + re.getMessage());
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Orders> findOrdersEntities() {
        return findOrdersEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Orders> findOrdersEntities(int maxResults, int firstResult) {
        return findOrdersEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Orders> findOrdersEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Orders> cq = entityManager.getCriteriaBuilder().createQuery(Orders.class);
        cq.select(cq.from(Orders.class));
        TypedQuery<Orders> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Orders findOrders(Integer id) {
        return entityManager.find(Orders.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getOrdersCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Orders> rt = cq.from(Orders.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return (q.getSingleResult().intValue());
    }

    /////////////////////////////////////////////
    ///////////// ORDERS BY USER ////////////////
    /////////////////////////////////////////////
    /**
     * Returns all Orders associated with a given User.
     *
     * Native SQL Example Query: - SELECT * FROM orders WHERE user_id = 1;
     *
     * @param user A User object.
     *
     * @return The Orders associated with the User, if they exist.
     *
     * @author Eira Garrett
     */
    public List<Orders> getOrdersByUser(Users user) {
        Objects.requireNonNull(user, "user must be set");

        //SELECT * FROM orders
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Orders> cQuery = cBuilder.createQuery(Orders.class);
        Root<Orders> ordersTable = cQuery.from(Orders.class);

        //WHERE user_id = :id
        cQuery.where(cBuilder.equal(ordersTable.get(Orders_.userId).get(Users_.userId), user.getUserId()));

        TypedQuery<Orders> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Returns all Orders associated with a given User.
     *
     * Native SQL Example Query: - SELECT * FROM orders WHERE user_id = 1;
     *
     * @param userId The ID of a user.
     *
     * @return All orders associated with the given user id, if any.
     *
     * @author Eira Garrett
     */
    public List<Orders> getOrdersByUser(Integer userId) {
        Objects.requireNonNull(userId, "userId must be set");

        //SELECT * FROM orders
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Orders> cQuery = cBuilder.createQuery(Orders.class);
        Root<Orders> ordersTable = cQuery.from(Orders.class);

        //WHERE user_id = :id
        cQuery.where(cBuilder.equal(ordersTable.get(Orders_.userId).get(Users_.userId), userId));

        TypedQuery<Orders> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    //////////////////////////////////
    ////// ORDERS BY ORDER DATE //////
    //////////////////////////////////
    /**
     * Gets a list of orders made on a particular date.
     *
     * Native SQL Example Query: - SELECT * FROM orders WHERE purchase_date =
     * 2019/03/10;
     *
     * @param date A Date object.
     *
     * @return A List of orders made on the given date, if any.
     *
     * @author Eira Garrett
     */
    public List<Orders> getOrdersByDate(Date date) {
        Objects.requireNonNull(date, "date must be set");

        //SELECT * FROM orders
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Orders> cQuery = cBuilder.createQuery(Orders.class);
        Root<Orders> ordersTable = cQuery.from(Orders.class);

        //WHERE purchase_date = :date
        cQuery.where(cBuilder.equal(ordersTable.get(Orders_.purchaseDate), date));

        TypedQuery<Orders> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Retrieves a list of orders made between two dates.
     *
     * Native SQL Example Query: - SELECT * FROM orders WHERE purchase_date
     * BETWEEN 2019/03/10 AND 2019/03/14 ORDER BY purchase_date;
     *
     * @param start The start of the date range.
     * @param end The end of the date range.
     *
     * @return Orders made between the two given dates, if any.
     *
     * @author Eira Garrett
     */
    public List<Orders> getOrdersByDate(Date start, Date end) {
        Objects.requireNonNull(start, "start date must be set");
        Objects.requireNonNull(end, "end date must be set");

        //SELECT * FROM orders
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Orders> cQuery = cBuilder.createQuery(Orders.class);
        Root<Orders> ordersTable = cQuery.from(Orders.class);

        //WHERE purchase_date BETWEEN :start AND :end ORDER BY purchase_date
        cQuery.where(cBuilder.between(ordersTable.get(Orders_.purchaseDate), start, end))
                .orderBy(cBuilder.asc(ordersTable.get(Orders_.purchaseDate)));

        TypedQuery<Orders> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    //////////////////////////////////////////
    //////////// ORDERS BY BOOK //////////////
    //////////////////////////////////////////
    /**
     * Retrieves a list of all orders containing a given book.
     *
     * Native SQL Example Query: - SELECT * FROM orders JOIN orderitems USING
     * (order_id) JOIN books USING (isbn) WHERE books.isbn = 9780062941008;
     *
     * @param book A Books object representing the book for which to search.
     *
     * @return A List of all Orders containing the given book.
     *
     * @author Eira Garrett
     */
    public List<Orders> getOrdersContainingBook(Books book) {
        Objects.requireNonNull(book, "book must be set");

        //SELECT * FROM orders
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Orders> cQuery = cBuilder.createQuery(Orders.class);
        Root<Orders> ordersTable = cQuery.from(Orders.class);

        //JOIN orderitems USING (order_id)
        Join<Orders, Orderitems> ordersToBooks = ordersTable.join(Orders_.orderitemsList);

        //WHERE books.isbn = :isbn AND orderitems.removed = false
        cQuery.where(
                cBuilder.equal(ordersToBooks.get(Orderitems_.isbn).get(Books_.isbn), book.getIsbn()),
                cBuilder.equal(ordersToBooks.get(Orderitems_.removed), false)
        );

        TypedQuery<Orders> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Retrieves a count of all orders containing a given book.
     *
     * @param book A Book object.
     *
     * @return An Integer containing the count of all orders containing the
     * given book.
     *
     * @author Eira Garrett
     */
    public Integer getNumberOfOrdersContainingBook(Books book) {
        Objects.requireNonNull(book, "book must be set");
        return getOrdersContainingBook(book).size();
    }
}
