package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Books_;
import com.teama1.ga1w20.persistence.entities.Reviews;
import com.teama1.ga1w20.persistence.entities.Reviews_;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.entities.Users_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides methods for retrieving Reviews records by the following criteria: -
 * Reviews by book - Books object, isbn; approved or all; paginated or not. -
 * Reviews by user - Users object, userId; approved or all. - Entries in
 * moderation queue - reviews that have not been approved.
 *
 * @author Michael, Eira Garrett
 */
@Named
@SessionScoped
public class ReviewsJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(ReviewsJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Reviews reviews) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Books isbn = reviews.getIsbn();
            if (isbn != null) {
                isbn = entityManager.getReference(isbn.getClass(), isbn.getIsbn());
                reviews.setIsbn(isbn);
            }
            Users userId = reviews.getUserId();
            if (userId != null) {
                userId = entityManager.getReference(userId.getClass(), userId.getUserId());
                reviews.setUserId(userId);
            }
            entityManager.persist(reviews);

            if (isbn != null) {
                isbn.getReviewsList().add(reviews);
                isbn = entityManager.merge(isbn);
            }
            if (userId != null) {
                userId.getReviewsList().add(reviews);
                userId = entityManager.merge(userId);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when adding the review: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findReviews(reviews.getReviewId()) != null) {
                LOG.error("review " + reviews.getReviewId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Reviews reviews) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            userTransaction.begin();
            Reviews persistentReviews = entityManager.find(Reviews.class, reviews.getReviewId());
            Books isbnOld = persistentReviews.getIsbn();
            Books isbnNew = reviews.getIsbn();
            Users userIdOld = persistentReviews.getUserId();
            Users userIdNew = reviews.getUserId();
            if (isbnNew != null) {
                isbnNew = entityManager.getReference(isbnNew.getClass(), isbnNew.getIsbn());
                reviews.setIsbn(isbnNew);
            }
            if (userIdNew != null) {
                userIdNew = entityManager.getReference(userIdNew.getClass(), userIdNew.getUserId());
                reviews.setUserId(userIdNew);
            }
            reviews = entityManager.merge(reviews);
            if (isbnOld != null && !isbnOld.equals(isbnNew)) {
                isbnOld.getReviewsList().remove(reviews);
                isbnOld = entityManager.merge(isbnOld);
            }
            if (isbnNew != null && !isbnNew.equals(isbnOld)) {
                isbnNew.getReviewsList().add(reviews);
                isbnNew = entityManager.merge(isbnNew);
            }
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getReviewsList().remove(reviews);
                userIdOld = entityManager.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getReviewsList().add(reviews);
                userIdNew = entityManager.merge(userIdNew);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the review: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Long id = reviews.getReviewId();
            if (findReviews(id) == null) {
                throw new NonexistentEntityException("The reviews with id "
                        + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Long id) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Reviews reviews;
            try {
                reviews = entityManager.getReference(Reviews.class, id);
                reviews.getReviewId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reviews with id " + id + " does not exist.", enfe);
            }
            Books isbn = reviews.getIsbn();
            if (isbn != null) {
                isbn.getReviewsList().remove(reviews);
                isbn = entityManager.merge(isbn);
            }
            Users userId = reviews.getUserId();
            if (userId != null) {
                userId.getReviewsList().remove(reviews);
                userId = entityManager.merge(userId);
            }
            entityManager.remove(reviews);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the review: " + (ex.getMessage() == null ? "" : ex.getMessage()));
            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Reviews> findReviewsEntities() {
        return findReviewsEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Reviews> findReviewsEntities(int maxResults, int firstResult) {
        return findReviewsEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Reviews> findReviewsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Reviews> cq = entityManager.getCriteriaBuilder().createQuery(Reviews.class);
        cq.select(cq.from(Reviews.class));
        TypedQuery<Reviews> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Reviews findReviews(Long id) {
        return entityManager.find(Reviews.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getReviewsCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Reviews> rt = cq.from(Reviews.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    //    -- All approved reviews for the current isbn:
//    SELECT * FROM reviews WHERE (isbn = 9780062941008) AND (approved = true);
    /////////////////////////////////////
    //////// REVIEWS BY BOOK ////////////
    /////////////////////////////////////
    /**
     * Retrieves all manager-approved reviews for a given book. May return an
     * empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN books USING (isbn)
     * WHERE books.isbn = 9780062941008 AND approved = true;
     *
     * @param book A Books object.
     *
     * @return A List of all reviews associated with this books object, if any.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getApprovedReviewsByBook(Books book) {
        Objects.requireNonNull(book, "Book must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN books USING (isbn)
        Join<Reviews, Books> reviewsToBooks = reviewsTable.join(Reviews_.isbn);

        //WHERE books.isbn = :isbn AND reviews.approved = true
        cQuery.where(
                cBuilder.equal(reviewsToBooks.get(Books_.isbn), book.getIsbn()),
                cBuilder.equal(reviewsTable.get(Reviews_.approved), true)
        );

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Retrieves all manager-approved reviews for a given book. May return an
     * empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN books USING (isbn)
     * WHERE books.isbn = 9780062941008 AND approved = true;
     *
     * @param isbn A Long representing the isbn of the book for which to search.
     *
     * @return A List of all reviews associated with this books object, if any.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getApprovedReviewsByBook(Long isbn) {
        Objects.requireNonNull(isbn, "ISBN must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN books USING (isbn)
        Join<Reviews, Books> reviewsToBooks = reviewsTable.join(Reviews_.isbn);

        //WHERE books.isbn = :isbn AND reviews.approved = true
        cQuery.where(
                cBuilder.equal(reviewsToBooks.get(Books_.isbn), isbn),
                cBuilder.equal(reviewsTable.get(Reviews_.approved), true)
        );

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Retrieves all manager-approved reviews for a given book. May return an
     * empty list. This version facilitates pagination.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN books USING (isbn)
     * WHERE books.isbn = 9780062941008 AND approved = true;
     *
     * @param book A Books object.
     * @param page The current page.
     * @param numResultsPerPage The maximum number of results per page.
     *
     * @return A List of all reviews associated with this books object, if any.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getApprovedReviewsByBook(Books book, int page, int numResultsPerPage) {
        Objects.requireNonNull(book, "Book must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN books USING (isbn)
        Join<Reviews, Books> reviewsToBooks = reviewsTable.join(Reviews_.isbn);

        //WHERE books.isbn = :isbn AND reviews.approved = true
        cQuery.where(
                cBuilder.equal(reviewsToBooks.get(Books_.isbn), book.getIsbn()),
                cBuilder.equal(reviewsTable.get(Reviews_.approved), true)
        );

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        tq.setFirstResult(page * numResultsPerPage - 1);
        tq.setMaxResults(numResultsPerPage);

        return tq.getResultList();
    }

    /**
     * Retrieves all manager-approved reviews for a given book. May return an
     * empty list. This version facilitates pagination.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN books USING (isbn)
     * WHERE books.isbn = 9780062941008 AND approved = true;
     *
     * @param isbn A Long representing the ISBN of a given book.
     * @param page The current page.
     * @param numResultsPerPage The maximum number of results per page.
     *
     * @return A List of all reviews associated with this books object, if any.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getApprovedReviewsByBook(Long isbn, int page, int numResultsPerPage) {
        Objects.requireNonNull(isbn, "ISBN must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN books USING (isbn)
        Join<Reviews, Books> reviewsToBooks = reviewsTable.join(Reviews_.isbn);

        //WHERE books.isbn = :isbn AND reviews.approved = true
        cQuery.where(
                cBuilder.equal(reviewsToBooks.get(Books_.isbn), isbn),
                cBuilder.equal(reviewsTable.get(Reviews_.approved), true)
        );

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        tq.setFirstResult(page * numResultsPerPage - 1);
        tq.setMaxResults(numResultsPerPage);

        return tq.getResultList();
    }

    /**
     * Retrieves all reviews for a given book, regardless of whether or not
     * they've been approved by a manager. May return an empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN books USING (isbn)
     * WHERE books.isbn = 9780062941008;
     *
     * @param book A Books object.
     *
     * @return A List of all reviews associated with this books object, if any.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getAllReviewsByBook(Books book) {
        Objects.requireNonNull(book, "Book must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN books USING (isbn)
        Join<Reviews, Books> reviewsToBooks = reviewsTable.join(Reviews_.isbn);

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(reviewsToBooks.get(Books_.isbn), book.getIsbn()));

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Retrieves all reviews for a given book, regardless of whether or not
     * they've been approved by a manager. May return an empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN books USING (isbn)
     * WHERE books.isbn = 9780062941008;
     *
     * @param isbn A Long representing the isbn of the book for which to search.
     *
     * @return A List of all reviews associated with this books object, if any.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getAllReviewsByBook(Long isbn) {
        Objects.requireNonNull(isbn, "ISBN must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN books USING (isbn)
        Join<Reviews, Books> reviewsToBooks = reviewsTable.join(Reviews_.isbn);

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(reviewsToBooks.get(Books_.isbn), isbn));

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    ///////////////////////////////////////////
    //////////// REVIEWS BY USER //////////////
    ///////////////////////////////////////////
    /**
     * Gets a list of all manager-approved reviews made by a given user. May
     * return an empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN users USING
     * (user_id) WHERE users.user_id = 1 AND reviews.approved = true;
     *
     * @param user A User object.
     *
     * @return A List of all reviews associated with the user, if any exist.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getApprovedReviewsByUser(Users user) {
        Objects.requireNonNull(user, "User must not be null");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN users USING (user_id)
        Join<Reviews, Users> reviewsToUsers = reviewsTable.join(Reviews_.userId);

        //WHERE users.user_id = :id AND reviews.approved = true
        cQuery.where(
                cBuilder.equal(reviewsToUsers.get(Users_.userId), user.getUserId()),
                cBuilder.equal(reviewsTable.get(Reviews_.approved), true)
        );

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Gets a list of all manager-approved reviews made by a given user. May
     * return an empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN users USING
     * (user_id) WHERE users.user_id = 1 AND reviews.approved = true;
     *
     * @param userId The ID number of a given user.
     *
     * @return A List of all reviews associated with the user, if any exist.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getApprovedReviewsByUser(Integer userId) {
        Objects.requireNonNull(userId, "User ID must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN users USING (user_id)
        Join<Reviews, Users> reviewsToUsers = reviewsTable.join(Reviews_.userId);

        //WHERE users.user_id = :id AND reviews.approved = true
        cQuery.where(
                cBuilder.equal(reviewsToUsers.get(Users_.userId), userId),
                cBuilder.equal(reviewsTable.get(Reviews_.approved), true)
        );

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Gets a list of all reviews made by a given user, regardless of manager
     * approval. May return an empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN users USING
     * (user_id) WHERE users.user_id = 1;
     *
     * @param user A User object.
     *
     * @return A List of all reviews associated with the user, if any exist.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getAllReviewsByUser(Users user) {
        Objects.requireNonNull(user, "User must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN users USING (user_id)
        Join<Reviews, Users> reviewsToUsers = reviewsTable.join(Reviews_.userId);

        //WHERE users.user_id = :id
        cQuery.where(cBuilder.equal(reviewsToUsers.get(Users_.userId), user.getUserId()));

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Gets a list of all reviews made by a given user, regardless of manager
     * approval. May return an empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews JOIN users USING
     * (user_id) WHERE users.user_id = 1;
     *
     * @param userId The ID number of a given user.
     *
     * @return A List of all reviews associated with the user, if any exist.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getAllReviewsByUser(Integer userId) {
        Objects.requireNonNull(userId, "User ID must not be null.");

        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //JOIN users USING (user_id)
        Join<Reviews, Users> reviewsToUsers = reviewsTable.join(Reviews_.userId);

        //WHERE users.user_id = :id
        cQuery.where(cBuilder.equal(reviewsToUsers.get(Users_.userId), userId));

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    ///////////////////////////////
    ////// MODERATION QUEUE ///////
    ///////////////////////////////
    /**
     * Gets a list of all reviews which have not yet been approved by a manager.
     * May return an empty list.
     *
     * Native SQL Example Query: - SELECT * FROM reviews WHERE approved = false;
     *
     * @return A List of all reviews which have not yet been approved by a
     * manager, if any.
     *
     * @author Eira Garrett
     */
    public List<Reviews> getModerationQueue() {
        //SELECT * FROM reviews
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Reviews> cQuery = cBuilder.createQuery(Reviews.class);
        Root<Reviews> reviewsTable = cQuery.from(Reviews.class);

        //WHERE approved = false
        cQuery.where(cBuilder.equal(reviewsTable.get(Reviews_.approved), false));

        TypedQuery<Reviews> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }
}
