package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Credentials;
import com.teama1.ga1w20.persistence.entities.Credentials_;
import com.teama1.ga1w20.persistence.entities.Users;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@SessionScoped
public class CredentialsJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(CredentialsJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Credentials credentials) throws
            PreexistingEntityException, IllegalStateException,
            SecurityException, HeuristicMixedException,
            HeuristicRollbackException, NotSupportedException,
            RollbackException, SystemException {
        try {
            userTransaction.begin();
            entityManager.persist(credentials);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when adding a credentials: "
                    + ex.getLocalizedMessage() == null ? "" : ex.getLocalizedMessage());

            if (findCredentials(credentials.getUserId()) != null) {
                LOG.error("Credentials with id " + credentials + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Credentials credentials) throws NonexistentEntityException,
            SecurityException, NotSupportedException, RollbackException,
            SystemException, HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            credentials = entityManager.merge(credentials);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when editing the credentials: " + ex.getMessage());
            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");

            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = credentials.getUserId();
                if (findCredentials(id) == null) {
                    throw new NonexistentEntityException("The credentials with id " + id + " does not exist.");
                }
            }
            throw ex;
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Credentials credentials;
            try {
                credentials = entityManager.getReference(Credentials.class, id);
                credentials.getUserId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The credentials with id " + id + " does not exist.", enfe);
            }
            entityManager.remove(credentials);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when deleting the credentials: " + ex.getMessage());
            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");

            }
            throw ex;
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Credentials> findCredentialsEntities() {
        return findCredentialsEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Credentials> findCredentialsEntities(int maxResults, int firstResult) {
        return findCredentialsEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Credentials> findCredentialsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Credentials> cq = entityManager.getCriteriaBuilder().createQuery(Credentials.class);
        cq.select(cq.from(Credentials.class));
        TypedQuery<Credentials> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Credentials findCredentials(Integer id) {
        return entityManager.find(Credentials.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getCredentialsCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Credentials> rt = cq.from(Credentials.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    ///////////////////////////////
    ///// CREDENTIALS BY USER /////
    ///////////////////////////////
    /**
     * Gets the credentials associated with a given User object.
     *
     * Native SQL Example Query: - SELECT * FROM credentials WHERE user_id = 1;
     *
     * @param user The User object for which to retrieve the associated
     * credentials.
     *
     * @return The Credentials object associated with the user.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Credentials getCredentialsByUser(Users user) throws
            NonexistentEntityException, NonUniqueResultException, NoResultException {
        Objects.requireNonNull(user, "user must be set");

        //SELECT * FROM credentials
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Credentials> cQuery = cBuilder.createQuery(Credentials.class);
        Root<Credentials> credentialsTable = cQuery.from(Credentials.class);

        //WHERE user_id = :id
        cQuery.where(cBuilder.equal(credentialsTable.get(Credentials_.userId), user.getUserId()));

        TypedQuery<Credentials> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    /**
     * Gets the Credentials associated with a given user id.
     *
     * Native SQL Example Query: - SELECT * FROM credentials WHERE user_id = 1;
     *
     * @param id An Integer representing the id of a given user.
     *
     * @return The Credentials object associated with the user, if it exists.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Credentials getCredentialsByUserId(Integer id) throws
            NonexistentEntityException, NonUniqueResultException, NoResultException {
        Objects.requireNonNull(id, "id must be set");

        //SELECT * FROM credentials
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Credentials> cQuery = cBuilder.createQuery(Credentials.class);
        Root<Credentials> credentialsTable = cQuery.from(Credentials.class);

        //WHERE user_id = :id
        cQuery.where(cBuilder.equal(credentialsTable.get(Credentials_.userId), id));

        TypedQuery<Credentials> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }
}
