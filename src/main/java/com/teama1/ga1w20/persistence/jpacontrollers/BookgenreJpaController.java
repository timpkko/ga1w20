package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Bookgenre;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Genres;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@SessionScoped
public class BookgenreJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(BookgenreJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Bookgenre bookgenre) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Books isbn = bookgenre.getIsbn();
            if (isbn != null) {
                isbn = entityManager.getReference(isbn.getClass(), isbn.getIsbn());
                bookgenre.setIsbn(isbn);
            }
            Genres genreId = bookgenre.getGenreId();
            if (genreId != null) {
                genreId = entityManager.getReference(genreId.getClass(), genreId.getGenreId());
                bookgenre.setGenreId(genreId);
            }
            entityManager.persist(bookgenre);
            if (isbn != null) {
                isbn.getBookgenreList().add(bookgenre);
                isbn = entityManager.merge(isbn);
            }
            if (genreId != null) {
                genreId.getBookgenreList().add(bookgenre);
                genreId = entityManager.merge(genreId);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("ERROR--------------------------");
            LOG.error("Exception thrown when adding the record in Bookgenre: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findBookgenre(bookgenre.getBookgenreId()) != null) {
                LOG.error("Bookgenre with id " + bookgenre.getBookgenreId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }

        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Bookgenre bookgenre) throws NonexistentEntityException,
            SecurityException, NotSupportedException, RollbackException,
            SystemException, HeuristicMixedException, HeuristicRollbackException {

        try {
            userTransaction.begin();
            Bookgenre persistentBookgenre = entityManager.find(Bookgenre.class, bookgenre.getBookgenreId());
            Books isbnOld = persistentBookgenre.getIsbn();
            Books isbnNew = bookgenre.getIsbn();
            Genres genreIdOld = persistentBookgenre.getGenreId();
            Genres genreIdNew = bookgenre.getGenreId();
            if (isbnNew != null) {
                isbnNew = entityManager.getReference(isbnNew.getClass(), isbnNew.getIsbn());
                bookgenre.setIsbn(isbnNew);
            }
            if (genreIdNew != null) {
                genreIdNew = entityManager.getReference(genreIdNew.getClass(), genreIdNew.getGenreId());
                bookgenre.setGenreId(genreIdNew);
            }
            bookgenre = entityManager.merge(bookgenre);
            if (isbnOld != null && !isbnOld.equals(isbnNew)) {
                isbnOld.getBookgenreList().remove(bookgenre);
                isbnOld = entityManager.merge(isbnOld);
            }
            if (isbnNew != null && !isbnNew.equals(isbnOld)) {
                isbnNew.getBookgenreList().add(bookgenre);
                isbnNew = entityManager.merge(isbnNew);
            }
            if (genreIdOld != null && !genreIdOld.equals(genreIdNew)) {
                genreIdOld.getBookgenreList().remove(bookgenre);
                genreIdOld = entityManager.merge(genreIdOld);
            }
            if (genreIdNew != null && !genreIdNew.equals(genreIdOld)) {
                genreIdNew.getBookgenreList().add(bookgenre);
                genreIdNew = entityManager.merge(genreIdNew);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the record in Bookgenre: " + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = bookgenre.getBookgenreId();
            if (findBookgenre(id) == null) {
                LOG.error("The bookgenre with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Bookgenre bookgenre;
            try {
                bookgenre = entityManager.getReference(Bookgenre.class, id);
                bookgenre.getBookgenreId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bookgenre with id " + id + " does not exist.", enfe);
            }
            Books isbn = bookgenre.getIsbn();
            if (isbn != null) {
                isbn.getBookgenreList().remove(bookgenre);
                isbn = entityManager.merge(isbn);
            }
            Genres genreId = bookgenre.getGenreId();
            if (genreId != null) {
                genreId.getBookgenreList().remove(bookgenre);
                genreId = entityManager.merge(genreId);
            }
            entityManager.remove(bookgenre);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when deleting the record in Bookgenre: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findBookgenre(id) == null) {
                LOG.error("The bookgenre with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Bookgenre> findBookgenreEntities() {
        return findBookgenreEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Bookgenre> findBookgenreEntities(int maxResults, int firstResult) {
        return findBookgenreEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Bookgenre> findBookgenreEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Bookgenre> cq = entityManager.getCriteriaBuilder().createQuery(Bookgenre.class);
        cq.select(cq.from(Bookgenre.class));
        TypedQuery<Bookgenre> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Bookgenre findBookgenre(Integer id) {
        return entityManager.find(Bookgenre.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getBookgenreCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Bookgenre> rt = cq.from(Bookgenre.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

}
