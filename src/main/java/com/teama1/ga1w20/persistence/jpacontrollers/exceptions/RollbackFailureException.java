package com.teama1.ga1w20.persistence.jpacontrollers.exceptions;

public class RollbackFailureException extends Exception {

    public RollbackFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public RollbackFailureException(String message) {
        super(message);
    }
}
