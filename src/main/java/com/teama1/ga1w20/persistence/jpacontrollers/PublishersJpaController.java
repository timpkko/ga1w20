package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Books_;
import com.teama1.ga1w20.persistence.entities.Publishers;
import com.teama1.ga1w20.persistence.entities.Publishers_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class PublishersJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(PublishersJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Publishers publishers) throws RollbackFailureException,
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        if (publishers.getBooksList() == null) {
            publishers.setBooksList(new ArrayList<Books>());
        }

        try {
            userTransaction.begin();
            List<Books> attachedBooksList = new ArrayList<Books>();
            for (Books booksListBooksToAttach : publishers.getBooksList()) {
                booksListBooksToAttach = entityManager.getReference(booksListBooksToAttach.getClass(), booksListBooksToAttach.getIsbn());
                attachedBooksList.add(booksListBooksToAttach);
            }
            publishers.setBooksList(attachedBooksList);
            entityManager.persist(publishers);
            for (Books booksListBooks : publishers.getBooksList()) {
                Publishers oldPublisherIdOfBooksListBooks = booksListBooks.getPublisherId();
                booksListBooks.setPublisherId(publishers);
                booksListBooks = entityManager.merge(booksListBooks);
                if (oldPublisherIdOfBooksListBooks != null) {
                    oldPublisherIdOfBooksListBooks.getBooksList().remove(booksListBooks);
                    oldPublisherIdOfBooksListBooks = entityManager.merge(oldPublisherIdOfBooksListBooks);
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding the publisher: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findPublishers(publishers.getPublisherId()) != null) {
                LOG.error("publisher with id " + publishers.getPublisherId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("An error occurred attempting to roll  back the transaction." + re.getMessage());
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Publishers publishers) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        try {
            userTransaction.begin();
            Publishers persistentPublishers = entityManager.find(Publishers.class, publishers.getPublisherId());
            List<Books> booksListOld = persistentPublishers.getBooksList();
            List<Books> booksListNew = publishers.getBooksList();
            List<String> illegalOrphanMessages = null;
            for (Books booksListOldBooks : booksListOld) {
                if (!booksListNew.contains(booksListOldBooks)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Books " + booksListOldBooks + " since its publisherId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Books> attachedBooksListNew = new ArrayList<Books>();
            for (Books booksListNewBooksToAttach : booksListNew) {
                booksListNewBooksToAttach = entityManager.getReference(booksListNewBooksToAttach.getClass(), booksListNewBooksToAttach.getIsbn());
                attachedBooksListNew.add(booksListNewBooksToAttach);
            }
            booksListNew = attachedBooksListNew;
            publishers.setBooksList(booksListNew);
            publishers = entityManager.merge(publishers);
            for (Books booksListNewBooks : booksListNew) {
                if (!booksListOld.contains(booksListNewBooks)) {
                    Publishers oldPublisherIdOfBooksListNewBooks = booksListNewBooks.getPublisherId();
                    booksListNewBooks.setPublisherId(publishers);
                    booksListNewBooks = entityManager.merge(booksListNewBooks);
                    if (oldPublisherIdOfBooksListNewBooks != null && !oldPublisherIdOfBooksListNewBooks.equals(publishers)) {
                        oldPublisherIdOfBooksListNewBooks.getBooksList().remove(booksListNewBooks);
                        oldPublisherIdOfBooksListNewBooks = entityManager.merge(oldPublisherIdOfBooksListNewBooks);
                    }
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the publisher: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = publishers.getPublisherId();
            if (findPublishers(id) == null) {
                throw new NonexistentEntityException("The publishers with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("An error occurred attempting to roll  back the transaction."
                        + re.getMessage() == null ? "" : re.getMessage());
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Publishers publishers;
            try {
                publishers = entityManager.getReference(Publishers.class, id);
                publishers.getPublisherId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publishers with id "
                        + id + " does not exist." + enfe.getLocalizedMessage() == null ? "" : enfe.getLocalizedMessage());
            }
            List<String> illegalOrphanMessages = null;
            List<Books> booksListOrphanCheck = publishers.getBooksList();
            for (Books booksListOrphanCheckBooks : booksListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Publishers (" + publishers + ") cannot be destroyed since the Books " + booksListOrphanCheckBooks + " in its booksList field has a non-nullable publisherId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            entityManager.remove(publishers);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the publisher: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));
            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful.");

            }
            throw ex;
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Publishers> findPublishersEntities() {
        return findPublishersEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Publishers> findPublishersEntities(int maxResults, int firstResult) {
        return findPublishersEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Publishers> findPublishersEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Publishers> cq = entityManager.getCriteriaBuilder().createQuery(Publishers.class);
        cq.select(cq.from(Publishers.class));
        TypedQuery<Publishers> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Publishers findPublishers(Integer id) {
        return entityManager.find(Publishers.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getPublishersCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Publishers> rt = cq.from(Publishers.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    ////////////////////////////////////
    /////// PUBLISHER BY BOOK //////////
    ////////////////////////////////////
    /**
     * Returns the publisher associated with a given book.
     *
     * Native SQL Example Query: - SELECT * FROM publishers JOIN books USING
     * (publisher_id) WHERE books.isbn = 9780062941008;
     *
     * @param book A Books object.
     *
     * @return The Publishers object associated with the book.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Publishers getPublisherByBook(Books book) throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        Objects.requireNonNull(book, "Book must not be null.");

        //SELECT * FROM publishers
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Publishers> cQuery = cBuilder.createQuery(Publishers.class);
        Root<Publishers> publishersTable = cQuery.from(Publishers.class);

        //JOIN books USING (publisher_id)
        Join<Publishers, Books> publishersToBooks = publishersTable.join(Publishers_.booksList);

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(publishersToBooks.get(Books_.isbn), book.getIsbn()));

        TypedQuery<Publishers> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    /**
     * Returns the publisher associated with a given book.
     *
     * Native SQL Example Query: - SELECT * FROM publishers JOIN books USING
     * (publisher_id) WHERE books.isbn = 9780062941008;
     *
     * @param isbn A Long representing the ISBN of a book.
     *
     * @return The Publishers object associated with the book.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Publishers getPublisherByBook(Long isbn) throws NonexistentEntityException, NonUniqueResultException, NoResultException {
        Objects.requireNonNull(isbn, "ISBN must not be null.");

        //SELECT * FROM publishers
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Publishers> cQuery = cBuilder.createQuery(Publishers.class);
        Root<Publishers> publishersTable = cQuery.from(Publishers.class);

        //JOIN books USING (publisher_id)
        Join<Publishers, Books> publishersToBooks = publishersTable.join(Publishers_.booksList);

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(publishersToBooks.get(Books_.isbn), isbn));

        TypedQuery<Publishers> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    ////////////////////////////////////
    //////// PUBLISHER BY NAME /////////
    ////////////////////////////////////
    /**
     * Returns a publisher when searching by name. Case insensitive. Takes
     * approximate matches.
     *
     * Native SQL Example Query: - SELECT * FROM publishers WHERE name LIKE
     * "McGraw-Hill";
     *
     * @param name The publisher's name. May be approximate.
     *
     * @return A List of publishers with similar names, if any.
     *
     * @author Eira Garrett
     */
    public List<Publishers> getPublishersByName(String name) {
        Objects.requireNonNull(name, "Name must not be null.");

        if (name.isBlank()) {
            return findPublishersEntities();
        }
        name = name.strip();

        //SELECT * FROM publishers
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Publishers> cQuery = cBuilder.createQuery(Publishers.class);
        Root<Publishers> publishersTable = cQuery.from(Publishers.class);

        //WHERE name LIKE :name
        cQuery.where(cBuilder.like(cBuilder.lower(publishersTable.get(Publishers_.name)), "%" + name.toLowerCase() + "%"));

        TypedQuery<Publishers> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }
}
