package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Authors;
import com.teama1.ga1w20.persistence.entities.Authors_;
import com.teama1.ga1w20.persistence.entities.Bookauthor;
import com.teama1.ga1w20.persistence.entities.Bookauthor_;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Books_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@RequestScoped
public class AuthorsJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(AuthorsJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Authors authors) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        if (authors.getBookauthorList() == null) {
            authors.setBookauthorList(new ArrayList<Bookauthor>());
        }

        try {
            userTransaction.begin();
            List<Bookauthor> attachedBookauthorList = new ArrayList<Bookauthor>();
            for (Bookauthor bookauthorListBookauthorToAttach : authors.getBookauthorList()) {
                bookauthorListBookauthorToAttach = entityManager.getReference(bookauthorListBookauthorToAttach.getClass(), bookauthorListBookauthorToAttach.getBookauthorId());
                attachedBookauthorList.add(bookauthorListBookauthorToAttach);
            }
            authors.setBookauthorList(attachedBookauthorList);

            entityManager.persist(authors);
            for (Bookauthor bookauthorListBookauthor : authors.getBookauthorList()) {
                Authors oldAuthorIdOfBookauthorListBookauthor = bookauthorListBookauthor.getAuthorId();
                bookauthorListBookauthor.setAuthorId(authors);
                bookauthorListBookauthor = entityManager.merge(bookauthorListBookauthor);
                if (oldAuthorIdOfBookauthorListBookauthor != null) {
                    oldAuthorIdOfBookauthorListBookauthor.getBookauthorList().remove(bookauthorListBookauthor);
                    oldAuthorIdOfBookauthorListBookauthor = entityManager.merge(oldAuthorIdOfBookauthorListBookauthor);
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("ERROR--------------------------");
            LOG.error("Exception  when adding an author: " + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findAuthors(authors.getAuthorId()) != null) {
                LOG.error("author with id " + authors.getAuthorId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Authors authors) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Authors persistentAuthors = entityManager.find(Authors.class, authors.getAuthorId());
            List<Bookauthor> bookauthorListOld = persistentAuthors.getBookauthorList();
            List<Bookauthor> bookauthorListNew = authors.getBookauthorList();
            List<String> illegalOrphanMessages = null;
            for (Bookauthor bookauthorListOldBookauthor : bookauthorListOld) {
                if (!bookauthorListNew.contains(bookauthorListOldBookauthor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Bookauthor " + bookauthorListOldBookauthor + " since its authorId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Bookauthor> attachedBookauthorListNew = new ArrayList<Bookauthor>();
            for (Bookauthor bookauthorListNewBookauthorToAttach : bookauthorListNew) {
                bookauthorListNewBookauthorToAttach = entityManager.getReference(bookauthorListNewBookauthorToAttach.getClass(), bookauthorListNewBookauthorToAttach.getBookauthorId());
                attachedBookauthorListNew.add(bookauthorListNewBookauthorToAttach);
            }
            bookauthorListNew = attachedBookauthorListNew;
            authors.setBookauthorList(bookauthorListNew);
            authors = entityManager.merge(authors);
            for (Bookauthor bookauthorListNewBookauthor : bookauthorListNew) {
                if (!bookauthorListOld.contains(bookauthorListNewBookauthor)) {
                    Authors oldAuthorIdOfBookauthorListNewBookauthor = bookauthorListNewBookauthor.getAuthorId();
                    bookauthorListNewBookauthor.setAuthorId(authors);
                    bookauthorListNewBookauthor = entityManager.merge(bookauthorListNewBookauthor);
                    if (oldAuthorIdOfBookauthorListNewBookauthor != null && !oldAuthorIdOfBookauthorListNewBookauthor.equals(authors)) {
                        oldAuthorIdOfBookauthorListNewBookauthor.getBookauthorList().remove(bookauthorListNewBookauthor);
                        oldAuthorIdOfBookauthorListNewBookauthor = entityManager.merge(oldAuthorIdOfBookauthorListNewBookauthor);
                    }
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception when editing the author: " + ex.getMessage());

            Integer id = authors.getAuthorId();
            if (findAuthors(id) == null) {
                LOG.error("The author with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Authors authors;
            try {
                authors = entityManager.getReference(Authors.class, id);
                authors.getAuthorId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The authors with id " + id + " does not exist. " + enfe.getMessage());
            }
            List<String> illegalOrphanMessages = null;
            List<Bookauthor> bookauthorListOrphanCheck = authors.getBookauthorList();
            for (Bookauthor bookauthorListOrphanCheckBookauthor : bookauthorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Authors (" + authors + ") cannot be destroyed since the Bookauthor " + bookauthorListOrphanCheckBookauthor + " in its bookauthorList field has a non-nullable authorId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            entityManager.remove(authors);
            userTransaction.commit();

        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception  when deleting the author: " + ((ex.getMessage() == null ? "" : ex.getMessage()) == null ? "" : ex.getMessage()));

            if (findAuthors(id) == null) {
                LOG.error("The author with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Authors> findAuthorsEntities() {
        return findAuthorsEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Authors> findAuthorsEntities(int maxResults, int firstResult) {
        return findAuthorsEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Authors> findAuthorsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Authors> cq = entityManager.getCriteriaBuilder().createQuery(Authors.class);
        cq.select(cq.from(Authors.class));
        TypedQuery<Authors> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Authors findAuthors(Integer id) {
        return entityManager.find(Authors.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getAuthorsCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Authors> rt = cq.from(Authors.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    ///////////////////////////////////////////////
    ////////////// AUTHOR(S) BY BOOK //////////////
    ///////////////////////////////////////////////
    /**
     * Returns the author(s) associated with a particular book. Searches by Book
     * object.
     *
     * Native SQL Example Query: - SELECT * FROM authors JOIN bookauthor USING
     * (author_id) WHERE bookauthor.isbn = 1234567890;
     *
     * @param book A Books object.
     * @return A List of Authors.
     *
     * @author Eira Garrett
     */
    public List<Authors> getAuthorsByBook(Books book) {
        Objects.requireNonNull(book, "book must be set");
        //SELECT * FROM authors
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Authors> cQuery = cBuilder.createQuery(Authors.class);
        Root<Authors> authorTable = cQuery.from(Authors.class);

        //Selects Authors object(s) associated with the matching isbn
        Join<Authors, Bookauthor> bookAuthor = authorTable.join(Authors_.bookauthorList);
        Predicate isbnMatches = cBuilder.equal(bookAuthor.get(Bookauthor_.isbn).get(Books_.isbn), book.getIsbn());
        cQuery.where(isbnMatches);

        //Extract result(s)
        TypedQuery<Authors> tq = entityManager.createQuery(cQuery);
        List<Authors> resultList = tq.getResultList();

        return resultList;
    }

    /**
     * Returns the author(s) associated with a particular book. Searches by
     * ISBN.
     *
     * Native SQL Example Query: - SELECT * FROM authors JOIN bookauthor USING
     * (author_id) WHERE bookauthor.isbn = 1234567890;
     *
     * @param isbn The isbn of the book for which to search.
     * @return A List of Authors.
     *
     * @author Eira Garrett
     */
    public List<Authors> getAuthorsByBook(Long isbn) {
        Objects.requireNonNull(isbn, "isbn must be set");
        //SELECT * FROM authors
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Authors> cQuery = cBuilder.createQuery(Authors.class);
        Root<Authors> authorTable = cQuery.from(Authors.class);
        //Selects Authors object(s) associated with the matching isbn
        Join<Authors, Bookauthor> bookAuthor = authorTable.join(Authors_.bookauthorList);
        Predicate isbnMatches = cBuilder.equal(bookAuthor.get(Bookauthor_.isbn).get(Books_.isbn), isbn);
        cQuery.where(isbnMatches);

        //Extract result(s)
        TypedQuery<Authors> tq = entityManager.createQuery(cQuery);
        List<Authors> resultList = tq.getResultList();

        return resultList;
    }

    /**
     * Searches for authors by first or last name. Case insensitive. This method
     * finds approximate matches.
     *
     * Valid Example Inputs: - "Thomas Pynchon" - "thomas pynchon" - "Pynchon,
     * Thomas" - "Pynchon,Thomas" - "Thomas" - "Pynchon"
     *
     *
     *
     * @param term The search terms.
     *
     * @return A List of Authors.
     *
     * @author Eira Garrett
     */
    public List<Authors> getAuthorLikeName(String term) {
        Objects.requireNonNull(term, "term must be set");

        if (term.isEmpty()) {
            return findAuthorsEntities();
        }

        String[] names;

        List<Authors> output = new ArrayList<>();

        //Split input string on spaces or commas if spaces or commas exist
        if (term.contains(" ")) {
            names = term.split(" ");
        } else if (term.contains(",")) {
            names = term.split(",");

            //remove any whitespaces
            for (String name : names) {
                name = name.trim();
                name = "%".concat(name).concat("%");
            }
        } else {
            names = new String[]{"%".concat(term).concat("%")};
        }

        //SELECT * FROM authors
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Authors> cQuery = cBuilder.createQuery(Authors.class);
        Root<Authors> authorsTable = cQuery.from(Authors.class);

        //WHERE firstname = :name OR lastname = :name
        Expression<String> firstNameLower = cBuilder.lower(authorsTable.get(Authors_.firstname));
        Expression<String> lastNameLower = cBuilder.lower(authorsTable.get(Authors_.lastname));

        //Populate output list with resultset
        for (String name : names) {
            Predicate firstNameMatches = cBuilder.like(firstNameLower, name.toLowerCase());
            Predicate lastNameMatches = cBuilder.like(lastNameLower, name.toLowerCase());

            Predicate firstOrLastNameMatch = cBuilder.or(firstNameMatches, lastNameMatches);

            cQuery.where(firstOrLastNameMatch);

            TypedQuery<Authors> tq = entityManager.createQuery(cQuery);
            List<Authors> resultList = tq.getResultList();

            output.addAll(resultList);
        }

        return output;
    }
}
