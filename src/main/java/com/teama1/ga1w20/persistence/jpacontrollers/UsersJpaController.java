package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.*;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@SessionScoped
public class UsersJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(UsersJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Users users) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        if (users.getReviewsList() == null) {
            users.setReviewsList(new ArrayList<Reviews>());
        }
        if (users.getOrdersList() == null) {
            users.setOrdersList(new ArrayList<Orders>());
        }

        try {
            userTransaction.begin();
            List<Reviews> attachedReviewsList = new ArrayList<Reviews>();
            for (Reviews reviewsListReviewsToAttach : users.getReviewsList()) {
                reviewsListReviewsToAttach = entityManager.getReference(reviewsListReviewsToAttach.getClass(), reviewsListReviewsToAttach.getReviewId());
                attachedReviewsList.add(reviewsListReviewsToAttach);
            }
            users.setReviewsList(attachedReviewsList);
            List<Orders> attachedOrdersList = new ArrayList<Orders>();
            for (Orders ordersListOrdersToAttach : users.getOrdersList()) {
                ordersListOrdersToAttach = entityManager.getReference(ordersListOrdersToAttach.getClass(), ordersListOrdersToAttach.getOrderId());
                attachedOrdersList.add(ordersListOrdersToAttach);
            }
            users.setOrdersList(attachedOrdersList);
            try {
                entityManager.persist(users);
            } catch (ConstraintViolationException e) {
                LOG.error("Exception: ");
                e.getConstraintViolations().forEach(err -> LOG.error(err.toString()));
            }

            for (Reviews reviewsListReviews : users.getReviewsList()) {
                Users oldUserIdOfReviewsListReviews = reviewsListReviews.getUserId();
                reviewsListReviews.setUserId(users);
                reviewsListReviews = entityManager.merge(reviewsListReviews);
                if (oldUserIdOfReviewsListReviews != null) {
                    oldUserIdOfReviewsListReviews.getReviewsList().remove(reviewsListReviews);
                    oldUserIdOfReviewsListReviews = entityManager.merge(oldUserIdOfReviewsListReviews);
                }
            }
            for (Orders ordersListOrders : users.getOrdersList()) {
                Users oldUserIdOfOrdersListOrders = ordersListOrders.getUserId();
                ordersListOrders.setUserId(users);
                ordersListOrders = entityManager.merge(ordersListOrders);
                if (oldUserIdOfOrdersListOrders != null) {
                    oldUserIdOfOrdersListOrders.getOrdersList().remove(ordersListOrders);
                    oldUserIdOfOrdersListOrders = entityManager.merge(oldUserIdOfOrdersListOrders);
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when adding the user: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findUsers(users.getUserId()) != null) {
                LOG.error("user with id " + users.getUserId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Users users) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Users persistentUsers = entityManager.find(Users.class, users.getUserId());
            List<Reviews> reviewsListOld = persistentUsers.getReviewsList();
            List<Reviews> reviewsListNew = users.getReviewsList();
            List<Orders> ordersListOld = persistentUsers.getOrdersList();
            List<Orders> ordersListNew = users.getOrdersList();
            List<String> illegalOrphanMessages = null;
            for (Reviews reviewsListOldReviews : reviewsListOld) {
                if (!reviewsListNew.contains(reviewsListOldReviews)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Reviews " + reviewsListOldReviews + " since its userId field is not nullable.");
                }
            }
            for (Orders ordersListOldOrders : ordersListOld) {
                if (!ordersListNew.contains(ordersListOldOrders)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Orders " + ordersListOldOrders + " since its userId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Reviews> attachedReviewsListNew = new ArrayList<Reviews>();
            for (Reviews reviewsListNewReviewsToAttach : reviewsListNew) {
                reviewsListNewReviewsToAttach = entityManager.getReference(reviewsListNewReviewsToAttach.getClass(), reviewsListNewReviewsToAttach.getReviewId());
                attachedReviewsListNew.add(reviewsListNewReviewsToAttach);
            }
            reviewsListNew = attachedReviewsListNew;
            users.setReviewsList(reviewsListNew);
            List<Orders> attachedOrdersListNew = new ArrayList<Orders>();
            for (Orders ordersListNewOrdersToAttach : ordersListNew) {
                ordersListNewOrdersToAttach = entityManager.getReference(ordersListNewOrdersToAttach.getClass(), ordersListNewOrdersToAttach.getOrderId());
                attachedOrdersListNew.add(ordersListNewOrdersToAttach);
            }
            ordersListNew = attachedOrdersListNew;
            users.setOrdersList(ordersListNew);
            users = entityManager.merge(users);
            for (Reviews reviewsListNewReviews : reviewsListNew) {
                if (!reviewsListOld.contains(reviewsListNewReviews)) {
                    Users oldUserIdOfReviewsListNewReviews = reviewsListNewReviews.getUserId();
                    reviewsListNewReviews.setUserId(users);
                    reviewsListNewReviews = entityManager.merge(reviewsListNewReviews);
                    if (oldUserIdOfReviewsListNewReviews != null && !oldUserIdOfReviewsListNewReviews.equals(users)) {
                        oldUserIdOfReviewsListNewReviews.getReviewsList().remove(reviewsListNewReviews);
                        oldUserIdOfReviewsListNewReviews = entityManager.merge(oldUserIdOfReviewsListNewReviews);
                    }
                }
            }
            for (Orders ordersListNewOrders : ordersListNew) {
                if (!ordersListOld.contains(ordersListNewOrders)) {
                    Users oldUserIdOfOrdersListNewOrders = ordersListNewOrders.getUserId();
                    ordersListNewOrders.setUserId(users);
                    ordersListNewOrders = entityManager.merge(ordersListNewOrders);
                    if (oldUserIdOfOrdersListNewOrders != null && !oldUserIdOfOrdersListNewOrders.equals(users)) {
                        oldUserIdOfOrdersListNewOrders.getOrdersList().remove(ordersListNewOrders);
                        oldUserIdOfOrdersListNewOrders = entityManager.merge(oldUserIdOfOrdersListNewOrders);
                    }
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the user: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = users.getUserId();
            if (findUsers(id) == null) {
                throw new NonexistentEntityException("The users with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application ,
     * IllegalStateException,
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SystemException,
            HeuristicMixedException, HeuristicRollbackException,
            RollbackException, NotSupportedException, SecurityException {

        try {
            userTransaction.begin();
            Users users;
            try {
                users = entityManager.getReference(Users.class, id);
                users.getUserId();
            } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id "
                        + id + " does not exist. " + enfe.getMessage());
            }
            List<String> illegalOrphanMessages = null;
            List<Reviews> reviewsListOrphanCheck = users.getReviewsList();
            for (Reviews reviewsListOrphanCheckReviews : reviewsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Reviews " + reviewsListOrphanCheckReviews + " in its reviewsList field has a non-nullable userId field.");
            }
            List<Orders> ordersListOrphanCheck = users.getOrdersList();
            for (Orders ordersListOrphanCheckOrders : ordersListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the Orders " + ordersListOrphanCheckOrders + " in its ordersList field has a non-nullable userId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            entityManager.remove(users);
            userTransaction.commit();
        } catch (IllegalOrphanException | NonexistentEntityException | SecurityException | HeuristicMixedException | HeuristicRollbackException | RollbackException | SystemException | NotSupportedException ex) {

            LOG.error("Exception thrown when deleting the user: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Users> cq = entityManager.getCriteriaBuilder().createQuery(Users.class);
        cq.select(cq.from(Users.class));
        TypedQuery<Users> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Users findUsers(Integer id) {
        return entityManager.find(Users.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getUsersCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Users> rt = cq.from(Users.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return (q.getSingleResult().intValue());
    }

    ///////////////////////////////////////
    ////////// USERS BY EMAIL /////////////
    ///////////////////////////////////////
    /**
     * Finds the User associated with a given email address.
     *
     * Native SQL Example Query: - SELECT * FROM users WHERE email =
     * "firstname.lastname@email.com";
     *
     * @param email A String containing an email address.
     *
     * @return The User associated with the email address.
     *
     * @author Eira Garrett
     */
    public Users getUserByEmail(String email) throws NonUniqueResultException, NoResultException {
        Objects.requireNonNull(email, "Parameter email must be non-null");
        email = email.strip();

        //SELECT * FROM users
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> cQuery = cBuilder.createQuery(Users.class);
        Root<Users> usersTable = cQuery.from(Users.class);

        //WHERE email = :email
        cQuery.where(
                cBuilder.equal(
                        cBuilder.lower(usersTable.get(Users_.email)),
                        email.toLowerCase()
                )
        );

        TypedQuery<Users> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    ////////////////////////////////////////
    //////// USERS BY CREDENTIALS //////////
    ////////////////////////////////////////
    /**
     * Gets the user object associated with a given credentials object.
     *
     * Native SQL Example Query - SELECT * FROM users JOIN credentials USING
     * (user_id) WHERE credentials.credential_id = 1;
     *
     * @param credentials A credentials object.
     *
     * @return The user associated with the credentials object.
     *
     * @author Eira Garrett
     */
    public Users getUserByCredentials(Credentials credentials) throws NonUniqueResultException, NoResultException {
        Objects.requireNonNull(credentials, "credentials must be non-null");

        //SELECT * FROM users
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> cQuery = cBuilder.createQuery(Users.class);
        Root<Users> usersTable = cQuery.from(Users.class);

        //JOIN credentials USING (user_id)
        Join<Users, Credentials> usersToCredentials = usersTable.join(Users_.credentials);

        //WHERE credentials.credential_id = :id
        cQuery.where(cBuilder.equal(usersToCredentials.get(Credentials_.userId), credentials.getUserId()));

        TypedQuery<Users> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    ///////////////////////////////////////
    ////////// GET MANAGERS ///////////////
    ///////////////////////////////////////
    /**
     * Retrieves the Users objects associated with all managers.
     *
     * Native SQL Example Query: - SELECT * FROM users JOIN credentials USING
     * (user_id) WHERE credentials.manager = true;
     *
     * @return A List of all Users who are managers.
     *
     * @author Eira Garrett
     */
    public List<Users> getAllManagers() {
        //SELECT * FROM users
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> cQuery = cBuilder.createQuery(Users.class);
        Root<Users> usersTable = cQuery.from(Users.class);

        //JOIN credentials USING (user_id)
        Join<Users, Credentials> usersToCredentials = usersTable.join(Users_.credentials);

        //WHERE credentials.manager = true
        cQuery.where(cBuilder.isTrue(usersToCredentials.get(Credentials_.manager)));

        TypedQuery<Users> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    ///////////////////////////////////////
    ///////// GET USER BY REVIEW //////////
    ///////////////////////////////////////
    /**
     * Retrieves the Users object associated with the user who wrote a given
     * review.
     *
     * Native SQL Example Query: - SELECT * FROM users JOIN reviews USING
     * (user_id) WHERE reviews.review_id = 1;
     *
     * @param review A Reviews object.
     *
     * @return The Users object associated with the review.
     *
     * @throws NonexistentEntityException
     *
     * @author Eira Garrett
     */
    public Users getUserByReview(Reviews review) throws NonexistentEntityException, NoResultException, NonUniqueResultException {
        Objects.requireNonNull(review, "Parameter review must be non=null");

        //SELECT * FROM users
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Users> cQuery = cBuilder.createQuery(Users.class);
        Root<Users> usersTable = cQuery.from(Users.class);

        //JOIN reviews USING (user_id)
        Join<Users, Reviews> usersToReviews = usersTable.join(Users_.reviewsList);

        //WHERE review_id = :id
        cQuery.where(cBuilder.equal(usersToReviews.get(Reviews_.reviewId), review.getReviewId()));

        TypedQuery<Users> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    /**
     * Gets the total value of all purchases a given user has made.
     *
     * @param userId
     * @return
     * @throws NonexistentEntityException
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public BigDecimal getTotalPurchasesByUser(Integer userId) throws NonexistentEntityException, NoResultException, NonUniqueResultException {
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BigDecimal> cQuery = cBuilder.createQuery(BigDecimal.class);
        Root<Users> usersTable = cQuery.from(Users.class);

        Join<Users, Orders> usersToOrders = usersTable.join(Users_.ordersList);
        Join<Orders, Orderitems> ordersToOrderitems = usersToOrders.join(Orders_.orderitemsList);

        cQuery.where(
                cBuilder.equal(usersTable.get(Users_.userId), userId),
                cBuilder.isFalse(ordersToOrderitems.get(Orderitems_.removed))
        );

        cQuery.groupBy(usersTable.get(Users_.userId));

        cQuery.select(cBuilder.sum(ordersToOrderitems.get(Orderitems_.price)));

        TypedQuery<BigDecimal> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }
}
