package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Bookformat;
import com.teama1.ga1w20.persistence.entities.Bookformat_;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Books_;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.entities.Formats_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@SessionScoped
public class FormatsJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(FormatsJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Formats formats) throws RollbackFailureException,
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        if (formats.getBookformatList() == null) {
            formats.setBookformatList(new ArrayList<Bookformat>());
        }

        try {
            userTransaction.begin();
            List<Bookformat> attachedBookformatList = new ArrayList<Bookformat>();
            for (Bookformat bookformatListBookformatToAttach : formats.getBookformatList()) {
                bookformatListBookformatToAttach = entityManager.getReference(bookformatListBookformatToAttach.getClass(), bookformatListBookformatToAttach.getBookformatId());
                attachedBookformatList.add(bookformatListBookformatToAttach);
            }
            formats.setBookformatList(attachedBookformatList);
            entityManager.persist(formats);
            for (Bookformat bookformatListBookformat : formats.getBookformatList()) {
                Formats oldFormatIdOfBookformatListBookformat = bookformatListBookformat.getFormatId();
                bookformatListBookformat.setFormatId(formats);
                bookformatListBookformat = entityManager.merge(bookformatListBookformat);
                if (oldFormatIdOfBookformatListBookformat != null) {
                    oldFormatIdOfBookformatListBookformat.getBookformatList().remove(bookformatListBookformat);
                    oldFormatIdOfBookformatListBookformat = entityManager.merge(oldFormatIdOfBookformatListBookformat);
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException
                | HeuristicRollbackException | NotSupportedException
                | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when adding the format: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findFormats(formats.getFormatId()) != null) {
                LOG.error("format with id " + formats.getFormatId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Formats formats) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Formats persistentFormats = entityManager.find(Formats.class, formats.getFormatId());
            List<Bookformat> bookformatListOld = persistentFormats.getBookformatList();
            List<Bookformat> bookformatListNew = formats.getBookformatList();
            List<String> illegalOrphanMessages = null;
            for (Bookformat bookformatListOldBookformat : bookformatListOld) {
                if (!bookformatListNew.contains(bookformatListOldBookformat)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Bookformat " + bookformatListOldBookformat + " since its formatId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Bookformat> attachedBookformatListNew = new ArrayList<Bookformat>();
            for (Bookformat bookformatListNewBookformatToAttach : bookformatListNew) {
                bookformatListNewBookformatToAttach = entityManager.getReference(bookformatListNewBookformatToAttach.getClass(), bookformatListNewBookformatToAttach.getBookformatId());
                attachedBookformatListNew.add(bookformatListNewBookformatToAttach);
            }
            bookformatListNew = attachedBookformatListNew;
            formats.setBookformatList(bookformatListNew);
            formats = entityManager.merge(formats);
            for (Bookformat bookformatListNewBookformat : bookformatListNew) {
                if (!bookformatListOld.contains(bookformatListNewBookformat)) {
                    Formats oldFormatIdOfBookformatListNewBookformat = bookformatListNewBookformat.getFormatId();
                    bookformatListNewBookformat.setFormatId(formats);
                    bookformatListNewBookformat = entityManager.merge(bookformatListNewBookformat);
                    if (oldFormatIdOfBookformatListNewBookformat != null && !oldFormatIdOfBookformatListNewBookformat.equals(formats)) {
                        oldFormatIdOfBookformatListNewBookformat.getBookformatList().remove(bookformatListNewBookformat);
                        oldFormatIdOfBookformatListNewBookformat = entityManager.merge(oldFormatIdOfBookformatListNewBookformat);
                    }
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the format: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = formats.getFormatId();
            if (findFormats(id) == null) {
                LOG.error("The formats with id " + id + " does not exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Formats formats;
            try {
                formats = entityManager.getReference(Formats.class, id);
                formats.getFormatId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The formats with id " + id + " does not exist.", enfe);
            }

            List<String> illegalOrphanMessages = null;
            List<Bookformat> bookformatListOrphanCheck = formats.getBookformatList();
            for (Bookformat bookformatListOrphanCheckBookformat : bookformatListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Formats (" + formats + ") cannot be destroyed since the Bookformat " + bookformatListOrphanCheckBookformat + " in its bookformatList field has a non-nullable formatId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            entityManager.remove(formats);
            userTransaction.commit();

        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | NonexistentEntityException
                | IllegalStateException | SecurityException
                | HeuristicMixedException | HeuristicRollbackException
                | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the format: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            }
            throw ex;
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Formats> findFormatsEntities() {
        return findFormatsEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Formats> findFormatsEntities(int maxResults, int firstResult) {
        return findFormatsEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Formats> findFormatsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Formats> cq = entityManager.getCriteriaBuilder().createQuery(Formats.class);
        cq.select(cq.from(Formats.class));
        TypedQuery<Formats> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Formats findFormats(Integer id) {
        return entityManager.find(Formats.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getFormatsCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Formats> rt = cq.from(Formats.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    ///////////////////////////////////////////////////////////////////////////
    //CUSTOM QUERIES
    ///////////////////////////////////////////////////////////////////////////
    /**
     * Returns all formats associated with a given Book.
     *
     * Native SQL Example Query: - SELECT * FROM formats JOIN bookformat USING
     * (format_id) JOIN books USING (isbn) WHERE books.isbn = 9780062941008;
     *
     * @param book The Book object for which to retrieve the associated formats.
     *
     * @return A List of Formats.
     *
     * @author Eira Garrett
     */
    public List<Formats> getFormatsByBook(Books book) {
        Objects.requireNonNull(book, "book must be set");

        //SELECT * FROM formats
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Formats> cQuery = cBuilder.createQuery(Formats.class);
        Root<Formats> formatsTable = cQuery.from(Formats.class);

        //JOIN bookformat USING (format_id) JOIN books USING (isbn)
        Join<Formats, Bookformat> bookFormat = formatsTable.join(Formats_.bookformatList);

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(bookFormat.get(Bookformat_.isbn).get(Books_.isbn), book.getIsbn()));

        TypedQuery<Formats> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Returns all formats associated with a given Book. Searches by ISBN.
     *
     * Native SQL Example Query: - SELECT * FROM formats JOIN bookformat USING
     * (format_id) JOIN books USING (isbn) WHERE books.isbn = 9780062941008;
     *
     * @param isbn A Long representing the ISBN of the book for which to search.
     *
     * @return A List of Formats.
     *
     * @author Eira Garrett
     */
    public List<Formats> getFormatsByBook(Long isbn) {
        Objects.requireNonNull(isbn, "isbn must be set");

        //SELECT * FROM formats
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Formats> cQuery = cBuilder.createQuery(Formats.class);
        Root<Formats> formatsTable = cQuery.from(Formats.class);

        //JOIN bookformat USING (format_id) JOIN books USING (isbn)
        Join<Formats, Bookformat> bookFormat = formatsTable.join(Formats_.bookformatList);

        //WHERE books.isbn = :isbn
        cQuery.where(cBuilder.equal(bookFormat.get(Bookformat_.isbn).get(Books_.isbn), isbn));

        TypedQuery<Formats> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }
}
