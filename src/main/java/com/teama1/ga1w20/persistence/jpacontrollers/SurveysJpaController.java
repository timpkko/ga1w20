package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Surveys;
import com.teama1.ga1w20.persistence.entities.Surveys_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class SurveysJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(SurveysJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Surveys surveys) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            entityManager.persist(surveys);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when adding the survey: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findSurveys(surveys.getSurveyId()) != null) {
                LOG.error("survey " + surveys.getSurveyId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Surveys surveys) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            surveys = entityManager.merge(surveys);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when editing the survey: " + (ex.getMessage() == null ? "" : ex.getMessage()));
            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");

            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = surveys.getSurveyId();
                if (findSurveys(id) == null) {
                    throw new NonexistentEntityException("The surveys with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Surveys surveys;
            try {
                surveys = entityManager.getReference(Surveys.class, id);
                surveys.getSurveyId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The surveys with id " + id + " no longer exists.", enfe);
            }
            entityManager.remove(surveys);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when deleting the author: " + (ex.getMessage() == null ? "" : ex.getMessage()));
            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");

            }
            throw ex;
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Surveys> findSurveysEntities() {
        return findSurveysEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Surveys> findSurveysEntities(int maxResults, int firstResult) {
        return findSurveysEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Surveys> findSurveysEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Surveys> cq = entityManager.getCriteriaBuilder().createQuery(Surveys.class);
        cq.select(cq.from(Surveys.class));
        TypedQuery<Surveys> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Surveys findSurveys(Integer id) {
        return entityManager.find(Surveys.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getSurveysCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Surveys> rt = cq.from(Surveys.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    ///////////////////////////////////
    ////////// ACTIVE SURVEY //////////
    ///////////////////////////////////
    /**
     * Returns a List of currently active surveys. May return an empty list;
     * under current site implementation, should return only one survey.
     *
     * Native SQL Example Query - SELECT * FROM surveys WHERE active = true;
     *
     * @return A List of currently active surveys, if any.
     *
     * @author Eira Garrett
     */
    public List<Surveys> getActiveSurveys() {
        //SELECT * FROM surveys
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Surveys> cQuery = cBuilder.createQuery(Surveys.class);
        Root<Surveys> surveysTable = cQuery.from(Surveys.class);

        //WHERE active = true
        cQuery.where(cBuilder.isTrue(surveysTable.get(Surveys_.active)));

        TypedQuery<Surveys> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    ///////////////////////////////////
    //////// INACTIVE SURVEYS /////////
    ///////////////////////////////////
    /**
     * Returns a list of inactive surveys. List may be empty.
     *
     * @return A List of currently inactive surveys.
     *
     * @author Eira Garrett
     */
    public List<Surveys> getInactiveSurveys() {
        //SELECT * FROM surveys
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Surveys> cQuery = cBuilder.createQuery(Surveys.class);
        Root<Surveys> surveysTable = cQuery.from(Surveys.class);

        //WHERE active = false
        cQuery.where(cBuilder.isFalse(surveysTable.get(Surveys_.active)));

        TypedQuery<Surveys> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }
}
