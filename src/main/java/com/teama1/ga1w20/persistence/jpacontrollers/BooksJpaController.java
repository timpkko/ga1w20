package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.*;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JPA Controller for the books table. Includes queries to retrieve a Books
 * table record or List of Books table records, searching by various fields.
 *
 * @author Eira Garrett
 */
@Named
@SessionScoped
public class BooksJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(BooksJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Books books) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        if (books.getBookgenreList() == null) {
            books.setBookgenreList(new ArrayList<>());
        }
        if (books.getBookformatList() == null) {
            books.setBookformatList(new ArrayList<>());
        }
        if (books.getBookauthorList() == null) {
            books.setBookauthorList(new ArrayList<>());
        }
        if (books.getOrderitemsList() == null) {
            books.setOrderitemsList(new ArrayList<>());
        }
        if (books.getReviewsList() == null) {
            books.setReviewsList(new ArrayList<>());
        }

        try {
            userTransaction.begin();
            Publishers publisherId = books.getPublisherId();
            if (publisherId != null) {
                publisherId = entityManager.getReference(publisherId.getClass(), publisherId.getPublisherId());
                books.setPublisherId(publisherId);
            }
            List<Bookgenre> attachedBookgenreList = new ArrayList<>();
            for (Bookgenre bookgenreListBookgenreToAttach : books.getBookgenreList()) {
                bookgenreListBookgenreToAttach = entityManager.getReference(bookgenreListBookgenreToAttach.getClass(), bookgenreListBookgenreToAttach.getBookgenreId());
                attachedBookgenreList.add(bookgenreListBookgenreToAttach);
            }
            books.setBookgenreList(attachedBookgenreList);
            List<Bookformat> attachedBookformatList = new ArrayList<>();
            for (Bookformat bookformatListBookformatToAttach : books.getBookformatList()) {
                bookformatListBookformatToAttach = entityManager.getReference(bookformatListBookformatToAttach.getClass(), bookformatListBookformatToAttach.getBookformatId());
                attachedBookformatList.add(bookformatListBookformatToAttach);
            }
            books.setBookformatList(attachedBookformatList);
            List<Bookauthor> attachedBookauthorList = new ArrayList<>();
            for (Bookauthor bookauthorListBookauthorToAttach : books.getBookauthorList()) {
                bookauthorListBookauthorToAttach = entityManager.getReference(bookauthorListBookauthorToAttach.getClass(), bookauthorListBookauthorToAttach.getBookauthorId());
                attachedBookauthorList.add(bookauthorListBookauthorToAttach);
            }
            books.setBookauthorList(attachedBookauthorList);
            List<Orderitems> attachedOrderitemsList = new ArrayList<>();
            for (Orderitems orderitemsListOrderitemsToAttach : books.getOrderitemsList()) {
                orderitemsListOrderitemsToAttach = entityManager.getReference(orderitemsListOrderitemsToAttach.getClass(), orderitemsListOrderitemsToAttach.getOrderitemsId());
                attachedOrderitemsList.add(orderitemsListOrderitemsToAttach);
            }
            books.setOrderitemsList(attachedOrderitemsList);
            List<Reviews> attachedReviewsList = new ArrayList<>();
            for (Reviews reviewsListReviewsToAttach : books.getReviewsList()) {
                reviewsListReviewsToAttach = entityManager.getReference(reviewsListReviewsToAttach.getClass(), reviewsListReviewsToAttach.getReviewId());
                attachedReviewsList.add(reviewsListReviewsToAttach);
            }
            books.setReviewsList(attachedReviewsList);
            entityManager.persist(books);
            if (publisherId != null) {
                publisherId.getBooksList().add(books);
                publisherId = entityManager.merge(publisherId);
            }
            for (Bookgenre bookgenreListBookgenre : books.getBookgenreList()) {
                Books oldIsbnOfBookgenreListBookgenre = bookgenreListBookgenre.getIsbn();
                bookgenreListBookgenre.setIsbn(books);
                bookgenreListBookgenre = entityManager.merge(bookgenreListBookgenre);
                if (oldIsbnOfBookgenreListBookgenre != null) {
                    oldIsbnOfBookgenreListBookgenre.getBookgenreList().remove(bookgenreListBookgenre);
                    oldIsbnOfBookgenreListBookgenre = entityManager.merge(oldIsbnOfBookgenreListBookgenre);
                }
            }
            for (Bookformat bookformatListBookformat : books.getBookformatList()) {
                Books oldIsbnOfBookformatListBookformat = bookformatListBookformat.getIsbn();
                bookformatListBookformat.setIsbn(books);
                bookformatListBookformat = entityManager.merge(bookformatListBookformat);
                if (oldIsbnOfBookformatListBookformat != null) {
                    oldIsbnOfBookformatListBookformat.getBookformatList().remove(bookformatListBookformat);
                    oldIsbnOfBookformatListBookformat = entityManager.merge(oldIsbnOfBookformatListBookformat);
                }
            }
            for (Bookauthor bookauthorListBookauthor : books.getBookauthorList()) {
                Books oldIsbnOfBookauthorListBookauthor = bookauthorListBookauthor.getIsbn();
                bookauthorListBookauthor.setIsbn(books);
                bookauthorListBookauthor = entityManager.merge(bookauthorListBookauthor);
                if (oldIsbnOfBookauthorListBookauthor != null) {
                    oldIsbnOfBookauthorListBookauthor.getBookauthorList().remove(bookauthorListBookauthor);
                    oldIsbnOfBookauthorListBookauthor = entityManager.merge(oldIsbnOfBookauthorListBookauthor);
                }
            }
            for (Orderitems orderitemsListOrderitems : books.getOrderitemsList()) {
                Books oldIsbnOfOrderitemsListOrderitems = orderitemsListOrderitems.getIsbn();
                orderitemsListOrderitems.setIsbn(books);
                orderitemsListOrderitems = entityManager.merge(orderitemsListOrderitems);
                if (oldIsbnOfOrderitemsListOrderitems != null) {
                    oldIsbnOfOrderitemsListOrderitems.getOrderitemsList().remove(orderitemsListOrderitems);
                    oldIsbnOfOrderitemsListOrderitems = entityManager.merge(oldIsbnOfOrderitemsListOrderitems);
                }
            }
            for (Reviews reviewsListReviews : books.getReviewsList()) {
                Books oldIsbnOfReviewsListReviews = reviewsListReviews.getIsbn();
                reviewsListReviews.setIsbn(books);
                reviewsListReviews = entityManager.merge(reviewsListReviews);
                if (oldIsbnOfReviewsListReviews != null) {
                    oldIsbnOfReviewsListReviews.getReviewsList().remove(reviewsListReviews);
                    oldIsbnOfReviewsListReviews = entityManager.merge(oldIsbnOfReviewsListReviews);
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding the book !! " + ex.getMessage());

            if (findBooks(books.getIsbn()) != null) {
                LOG.error("Books with isbn " + books.getIsbn() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception NOT successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Books books) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {

        try {
            userTransaction.begin();
            Books persistentBooks = entityManager.find(Books.class, books.getIsbn());
            Publishers publisherIdOld = persistentBooks.getPublisherId();
            Publishers publisherIdNew = books.getPublisherId();
            List<Bookgenre> bookgenreListOld = persistentBooks.getBookgenreList();
            List<Bookgenre> bookgenreListNew = books.getBookgenreList();
            List<Bookformat> bookformatListOld = persistentBooks.getBookformatList();
            List<Bookformat> bookformatListNew = books.getBookformatList();
            List<Bookauthor> bookauthorListOld = persistentBooks.getBookauthorList();
            List<Bookauthor> bookauthorListNew = books.getBookauthorList();
            List<Orderitems> orderitemsListOld = persistentBooks.getOrderitemsList();
            List<Orderitems> orderitemsListNew = books.getOrderitemsList();
            List<Reviews> reviewsListOld = persistentBooks.getReviewsList();
            List<Reviews> reviewsListNew = books.getReviewsList();
            List<String> illegalOrphanMessages = null;
            for (Bookformat bookformatListOldBookformat : bookformatListOld) {
                if (!bookformatListNew.contains(bookformatListOldBookformat)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<>();
                    }
                    illegalOrphanMessages.add("You must retain Bookformat " + bookformatListOldBookformat + " since its isbn field is not nullable.");
                }
            }
            for (Bookauthor bookauthorListOldBookauthor : bookauthorListOld) {
                if (!bookauthorListNew.contains(bookauthorListOldBookauthor)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<>();
                    }
                    illegalOrphanMessages.add("You must retain Bookauthor " + bookauthorListOldBookauthor + " since its isbn field is not nullable.");
                }
            }
            for (Orderitems orderitemsListOldOrderitems : orderitemsListOld) {
                if (!orderitemsListNew.contains(orderitemsListOldOrderitems)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<>();
                    }
                    illegalOrphanMessages.add("You must retain Orderitems " + orderitemsListOldOrderitems + " since its isbn field is not nullable.");
                }
            }
            for (Reviews reviewsListOldReviews : reviewsListOld) {
                if (!reviewsListNew.contains(reviewsListOldReviews)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<>();
                    }
                    illegalOrphanMessages.add("You must retain Reviews " + reviewsListOldReviews + " since its isbn field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (publisherIdNew != null) {
                publisherIdNew = entityManager.getReference(publisherIdNew.getClass(), publisherIdNew.getPublisherId());
                books.setPublisherId(publisherIdNew);
            }
            List<Bookgenre> attachedBookgenreListNew = new ArrayList<>();
            for (Bookgenre bookgenreListNewBookgenreToAttach : bookgenreListNew) {
                bookgenreListNewBookgenreToAttach = entityManager.getReference(bookgenreListNewBookgenreToAttach.getClass(), bookgenreListNewBookgenreToAttach.getBookgenreId());
                attachedBookgenreListNew.add(bookgenreListNewBookgenreToAttach);
            }
            bookgenreListNew = attachedBookgenreListNew;
            books.setBookgenreList(bookgenreListNew);
            List<Bookformat> attachedBookformatListNew = new ArrayList<>();
            for (Bookformat bookformatListNewBookformatToAttach : bookformatListNew) {
                bookformatListNewBookformatToAttach = entityManager.getReference(bookformatListNewBookformatToAttach.getClass(), bookformatListNewBookformatToAttach.getBookformatId());
                attachedBookformatListNew.add(bookformatListNewBookformatToAttach);
            }
            bookformatListNew = attachedBookformatListNew;
            books.setBookformatList(bookformatListNew);
            List<Bookauthor> attachedBookauthorListNew = new ArrayList<>();
            for (Bookauthor bookauthorListNewBookauthorToAttach : bookauthorListNew) {
                bookauthorListNewBookauthorToAttach = entityManager.getReference(bookauthorListNewBookauthorToAttach.getClass(), bookauthorListNewBookauthorToAttach.getBookauthorId());
                attachedBookauthorListNew.add(bookauthorListNewBookauthorToAttach);
            }
            bookauthorListNew = attachedBookauthorListNew;
            books.setBookauthorList(bookauthorListNew);
            List<Orderitems> attachedOrderitemsListNew = new ArrayList<>();
            for (Orderitems orderitemsListNewOrderitemsToAttach : orderitemsListNew) {
                orderitemsListNewOrderitemsToAttach = entityManager.getReference(orderitemsListNewOrderitemsToAttach.getClass(), orderitemsListNewOrderitemsToAttach.getOrderitemsId());
                attachedOrderitemsListNew.add(orderitemsListNewOrderitemsToAttach);
            }
            orderitemsListNew = attachedOrderitemsListNew;
            books.setOrderitemsList(orderitemsListNew);
            List<Reviews> attachedReviewsListNew = new ArrayList<>();
            for (Reviews reviewsListNewReviewsToAttach : reviewsListNew) {
                reviewsListNewReviewsToAttach = entityManager.getReference(reviewsListNewReviewsToAttach.getClass(), reviewsListNewReviewsToAttach.getReviewId());
                attachedReviewsListNew.add(reviewsListNewReviewsToAttach);
            }
            reviewsListNew = attachedReviewsListNew;
            books.setReviewsList(reviewsListNew);
            books = entityManager.merge(books);
            if (publisherIdOld != null && !publisherIdOld.equals(publisherIdNew)) {
                publisherIdOld.getBooksList().remove(books);
                publisherIdOld = entityManager.merge(publisherIdOld);
            }
            if (publisherIdNew != null && !publisherIdNew.equals(publisherIdOld)) {
                publisherIdNew.getBooksList().add(books);
                publisherIdNew = entityManager.merge(publisherIdNew);
            }
            for (Bookgenre bookgenreListOldBookgenre : bookgenreListOld) {
                if (!bookgenreListNew.contains(bookgenreListOldBookgenre)) {
                    bookgenreListOldBookgenre.setIsbn(null);
                    bookgenreListOldBookgenre = entityManager.merge(bookgenreListOldBookgenre);
                }
            }
            for (Bookgenre bookgenreListNewBookgenre : bookgenreListNew) {
                if (!bookgenreListOld.contains(bookgenreListNewBookgenre)) {
                    Books oldIsbnOfBookgenreListNewBookgenre = bookgenreListNewBookgenre.getIsbn();
                    bookgenreListNewBookgenre.setIsbn(books);
                    bookgenreListNewBookgenre = entityManager.merge(bookgenreListNewBookgenre);
                    if (oldIsbnOfBookgenreListNewBookgenre != null && !oldIsbnOfBookgenreListNewBookgenre.equals(books)) {
                        oldIsbnOfBookgenreListNewBookgenre.getBookgenreList().remove(bookgenreListNewBookgenre);
                        oldIsbnOfBookgenreListNewBookgenre = entityManager.merge(oldIsbnOfBookgenreListNewBookgenre);
                    }
                }
            }
            for (Bookformat bookformatListNewBookformat : bookformatListNew) {
                if (!bookformatListOld.contains(bookformatListNewBookformat)) {
                    Books oldIsbnOfBookformatListNewBookformat = bookformatListNewBookformat.getIsbn();
                    bookformatListNewBookformat.setIsbn(books);
                    bookformatListNewBookformat = entityManager.merge(bookformatListNewBookformat);
                    if (oldIsbnOfBookformatListNewBookformat != null && !oldIsbnOfBookformatListNewBookformat.equals(books)) {
                        oldIsbnOfBookformatListNewBookformat.getBookformatList().remove(bookformatListNewBookformat);
                        oldIsbnOfBookformatListNewBookformat = entityManager.merge(oldIsbnOfBookformatListNewBookformat);
                    }
                }
            }
            for (Bookauthor bookauthorListNewBookauthor : bookauthorListNew) {
                if (!bookauthorListOld.contains(bookauthorListNewBookauthor)) {
                    Books oldIsbnOfBookauthorListNewBookauthor = bookauthorListNewBookauthor.getIsbn();
                    bookauthorListNewBookauthor.setIsbn(books);
                    bookauthorListNewBookauthor = entityManager.merge(bookauthorListNewBookauthor);
                    if (oldIsbnOfBookauthorListNewBookauthor != null && !oldIsbnOfBookauthorListNewBookauthor.equals(books)) {
                        oldIsbnOfBookauthorListNewBookauthor.getBookauthorList().remove(bookauthorListNewBookauthor);
                        oldIsbnOfBookauthorListNewBookauthor = entityManager.merge(oldIsbnOfBookauthorListNewBookauthor);
                    }
                }
            }
            for (Orderitems orderitemsListNewOrderitems : orderitemsListNew) {
                if (!orderitemsListOld.contains(orderitemsListNewOrderitems)) {
                    Books oldIsbnOfOrderitemsListNewOrderitems = orderitemsListNewOrderitems.getIsbn();
                    orderitemsListNewOrderitems.setIsbn(books);
                    orderitemsListNewOrderitems = entityManager.merge(orderitemsListNewOrderitems);
                    if (oldIsbnOfOrderitemsListNewOrderitems != null && !oldIsbnOfOrderitemsListNewOrderitems.equals(books)) {
                        oldIsbnOfOrderitemsListNewOrderitems.getOrderitemsList().remove(orderitemsListNewOrderitems);
                        oldIsbnOfOrderitemsListNewOrderitems = entityManager.merge(oldIsbnOfOrderitemsListNewOrderitems);
                    }
                }
            }
            for (Reviews reviewsListNewReviews : reviewsListNew) {
                if (!reviewsListOld.contains(reviewsListNewReviews)) {
                    Books oldIsbnOfReviewsListNewReviews = reviewsListNewReviews.getIsbn();
                    reviewsListNewReviews.setIsbn(books);
                    reviewsListNewReviews = entityManager.merge(reviewsListNewReviews);
                    if (oldIsbnOfReviewsListNewReviews != null && !oldIsbnOfReviewsListNewReviews.equals(books)) {
                        oldIsbnOfReviewsListNewReviews.getReviewsList().remove(reviewsListNewReviews);
                        oldIsbnOfReviewsListNewReviews = entityManager.merge(oldIsbnOfReviewsListNewReviews);
                    }
                }
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the book! "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Long id = books.getIsbn();
            if (findBooks(id) == null) {
                LOG.error("The book with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception successful");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception NOT successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Long id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Books books;
            try {
                books = entityManager.getReference(Books.class, id);
                books.getIsbn();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The books with id " + id
                        + " does not exist." + (enfe.getMessage() == null ? "" : enfe.getMessage()));
            }
            List<String> illegalOrphanMessages = null;
            List<Bookformat> bookformatListOrphanCheck = books.getBookformatList();
            for (Bookformat bookformatListOrphanCheckBookformat : bookformatListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<>();
                }
                illegalOrphanMessages.add("This Books (" + books + ") cannot be destroyed since the Bookformat " + bookformatListOrphanCheckBookformat + " in its bookformatList field has a non-nullable isbn field.");
            }
            List<Bookauthor> bookauthorListOrphanCheck = books.getBookauthorList();
            for (Bookauthor bookauthorListOrphanCheckBookauthor : bookauthorListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<>();
                }
                illegalOrphanMessages.add("This Books (" + books + ") cannot be destroyed since the Bookauthor " + bookauthorListOrphanCheckBookauthor + " in its bookauthorList field has a non-nullable isbn field.");
            }
            List<Orderitems> orderitemsListOrphanCheck = books.getOrderitemsList();
            for (Orderitems orderitemsListOrphanCheckOrderitems : orderitemsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<>();
                }
                illegalOrphanMessages.add("This Books (" + books + ") cannot be destroyed since the Orderitems " + orderitemsListOrphanCheckOrderitems + " in its orderitemsList field has a non-nullable isbn field.");
            }
            List<Reviews> reviewsListOrphanCheck = books.getReviewsList();
            for (Reviews reviewsListOrphanCheckReviews : reviewsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<>();
                }
                illegalOrphanMessages.add("This Books (" + books + ") cannot be destroyed since the Reviews " + reviewsListOrphanCheckReviews + " in its reviewsList field has a non-nullable isbn field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Publishers publisherId = books.getPublisherId();
            if (publisherId != null) {
                publisherId.getBooksList().remove(books);
                publisherId = entityManager.merge(publisherId);
            }
            List<Bookgenre> bookgenreList = books.getBookgenreList();
            for (Bookgenre bookgenreListBookgenre : bookgenreList) {
                bookgenreListBookgenre.setIsbn(null);
                bookgenreListBookgenre = entityManager.merge(bookgenreListBookgenre);
            }
            entityManager.remove(books);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalOrphanException | NonexistentEntityException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the book!"
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findBooks(id) == null) {
                LOG.error("The book with id " + id + " does not exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception successful!");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception NOT successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Books> findBooksEntities() {
        return findBooksEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Books> findBooksEntities(int maxResults, int firstResult) {
        return findBooksEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Books> findBooksEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Books> cq = entityManager.getCriteriaBuilder().createQuery(Books.class);
        cq.select(cq.from(Books.class));
        TypedQuery<Books> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Books findBooks(Long id) {
        return entityManager.find(Books.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getBooksCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Books> rt = cq.from(Books.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////// BOOK BY ISBN ////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Fetches the record for the book with the given ISBN.
     *
     * Native SQL Example Query: - SELECT * FROM books WHERE isbn = 12345678900;
     *
     * @param isbn The isbn for which to search.
     *
     * @return A Books object.
     */
    public Books getBookByIsbn(Long isbn) throws NonUniqueResultException, NoResultException {
        Objects.requireNonNull(isbn, "isbn must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //WHERE isbn = :isbn
        Predicate isbnMatch = cBuilder.equal(booksTable.get(Books_.isbn), isbn);
        cQuery.where(isbnMatch);

        //Generate query and execute
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);

        return tq.getSingleResult();
    }

    ////////////////////////////////////////////////////////////////////////////
    ///////////// BOOKS BY GENRE /////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Returns a list of non-removed books associated to this genre (id). Case
     * insensitive.
     *
     * Native SQL Example Query: - SELECT * FROM books JOIN bookgenre USING
     * (isbn) JOIN genres USING (genre_id) WHERE genre_id=3 AND books.removed =
     * false;
     *
     * @param genre The genre for which to search.
     * @return A List of Books.
     */
    public List<Books> getBooksByGenre(Genres genre) {
        Objects.requireNonNull(genre, "genre must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //JOIN bookgenre USING (isbn) JOIN genres USING (genre_id)
        Join<Books, Bookgenre> booksToBookgenre = booksTable.join(Books_.bookgenreList, JoinType.INNER);
        Join<Bookgenre, Genres> bookgenreToGenre = booksToBookgenre.join(Bookgenre_.genreId, JoinType.INNER);

        //WHERE lower(genres.name) = (:genre) AND books.removed = false
        Predicate notRemoved = cBuilder.equal(booksTable.get(Books_.removed), false);
        Predicate genreMatches = cBuilder.equal(bookgenreToGenre.get(Genres_.genreId), genre.getGenreId());
        cQuery.where(notRemoved, genreMatches);

        //Generate query and execute
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        return tq.getResultList();
    }

    /**
     * Returns a list of books whose genre matches or is similar to the search
     * terms. Case insensitive.
     *
     * Native SQL Example Query: - SELECT * FROM books JOIN bookgenre USING
     * (isbn) JOIN genres USING (genre_id) WHERE LOWER(genres.name) LIKE
     * LOWER("social themes") AND books.removed = false;
     *
     * @param hint The search terms.
     *
     * @return A List of Books.
     */
    public List<Books> getBooksByGenreHint(String hint) {
        Objects.requireNonNull(hint, "hint must be set");

        if (hint.isBlank()) {
            return findBooksEntities();
        }
        hint = hint.strip();

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //JOIN bookgenre USING (isbn) JOIN genres USING (genre_id)
        Join<Books, Bookgenre> booksToBookgenre = booksTable.join(Books_.bookgenreList, JoinType.INNER);
        Join<Bookgenre, Genres> booksToGenre = booksToBookgenre.join(Bookgenre_.genreId, JoinType.INNER);

        //WHERE LOWER(genres.name) LIKE LOWER(:hint) AND books.removed = false
        Predicate notRemoved = cBuilder.equal(booksTable.get(Books_.removed), false);
        Expression<String> genreLower = cBuilder.lower(booksToGenre.get(Genres_.name));
        Predicate genreLike = cBuilder.like(genreLower.as(String.class), "%" + hint.toLowerCase() + "%");
        cQuery.where(notRemoved, genreLike).distinct(true);

        //Generate and execute query
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        List<Books> resultList = tq.getResultList();

        return resultList;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////////////////// BOOKS BY AUTHOR //////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Returns a list of non-removed books based on the Authors object
     * (especially its id).
     *
     * @param author The Authors object
     * @return A list of Books
     * @author Timmy
     */
    public List<Books> getBooksByAuthor(Authors author) {
        Objects.requireNonNull(author, "author must be set");

        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        Join<Books, Bookauthor> booksToAuthors = booksTable.join(Books_.bookauthorList, JoinType.INNER);
        cQuery.where(
                cBuilder.equal(booksToAuthors.get(Bookauthor_.authorId).get(Authors_.authorId), author.getAuthorId()),
                cBuilder.isFalse(booksTable.get(Books_.removed))
        );

        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        return tq.getResultList();
    }

    /**
     * Returns a list of books for which the author's first or last name is
     * similar to the search term. Case insensitive.
     *
     * Native SQL Example Query: - SELECT * FROM books b INNER JOIN bookauthor
     * ba USING (isbn) INNER JOIN authors a USING (author_id) WHERE
     * (((LOWER(a.firstname) LIKE LOWER('%jonathan%')) OR (LOWER(a.lastname)
     * LIKE LOWER('%jonathan%')) AND (b.removed = false));
     *
     * @param hint The search terms.
     *
     * @return A List of Books.
     * @author Eira Garrett
     */
    public List<Books> getBooksByAuthorHint(String hint) {
        Objects.requireNonNull(hint, "hint must be set");

        if (hint.isBlank()) {
            return findBooksEntities();
        }
        hint = hint.strip();

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //INNER JOIN bookauthor USING (isbn) INNER JOIN authors USING (author_id)
        Join<Books, Bookauthor> booksToBookauthor = booksTable.join(Books_.bookauthorList, JoinType.INNER);
        Join<Bookauthor, Authors> booksToAuthors = booksToBookauthor.join(Bookauthor_.authorId, JoinType.INNER);

        //books.removed = false
        Predicate notRemoved = cBuilder.equal(booksTable.get(Books_.removed), false);

        //LOWER(authors.firstname) ...
        Expression<String> firstNameLower = cBuilder.lower(booksToAuthors.get(Authors_.firstname));

        //... LIKE LOWER(:hint)
        Predicate firstNameLike = cBuilder.like(firstNameLower, "%" + hint.toLowerCase() + "%");

        //LOWER(authors.lastname)...
        Expression<String> lastNameLower = cBuilder.lower(booksToAuthors.get(Authors_.lastname));

        //... LIKE LOWER(:hint)
        Predicate lastNameLike = cBuilder.like(lastNameLower, "%" + hint.toLowerCase() + "%");

        //((LOWER(authors.firstname) LIKE (:hint)) OR (LOWER(authors.lastname) LIKE (:hint))
        Predicate firstOrLastNameLike = cBuilder.or(firstNameLike, lastNameLike);

        //WHERE ((LOWER(authors.firstname) LIKE (:hint)) OR (LOWER(authors.lastname) LIKE (:hint)) AND books.removed = false
        cQuery.where(firstOrLastNameLike, notRemoved).distinct(true);

        //Generate query and execute
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        List<Books> resultList = tq.getResultList();

        return resultList;
    }

    ////////////////////////////////////////////////////////////////////////////
    /////////// BOOKS BY TITLE ////////////
    ///////////////////////////////////////////////////////////////////////////
    /**
     * Returns a list of Book objects wherein the title field approximately
     * matches the search terms. Case insensitive.
     *
     * Native SQL Example Query: - SELECT * FROM books WHERE LOWER(title) LIKE
     * LOWER("%gold%") AND removed = false;
     *
     * @param hint The search terms.
     *
     * @return A List of Books.
     * @author Eira Garrett
     */
    public List<Books> getBooksByTitleHint(String hint) {
        Objects.requireNonNull(hint, "hint must be set");

        if (hint.isBlank()) {
            return findBooksEntities();
        }
        hint = hint.strip();

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //WHERE lower(title) LIKE lower(:hint) AND removed = false
        Expression<String> hintLower = cBuilder.lower(booksTable.get(Books_.title));
        Predicate titleLike = cBuilder.like(hintLower.as(String.class), "%" + hint.toLowerCase() + "%");
        Predicate notRemoved = cBuilder.equal(booksTable.get(Books_.removed), false);
        cQuery.where(titleLike, notRemoved);

        //Generate query and execute -- returns ALL matching results
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        List<Books> resultList = tq.getResultList();

        return resultList;
    }

    /////////////////////////////////////////////
    /////////// BOOKS BY PUBLISHER //////////////
    /////////////////////////////////////////////
    /**
     * Returns a list of Books whose publishers match the given search terms
     * exactly. Case insensitive.
     *
     * Native SQL Example Query: - SELECT * FROM books INNER JOIN publishers
     * USING (publisher_id) WHERE LOWER(publishers.name) = LOWER("McGraw-Hill")
     * AND books.removed = false;
     *
     * @param publisher The search terms.
     *
     * @return A List of Books.
     */
    public List<Books> getBooksByPublisher(Publishers publisher) {
        Objects.requireNonNull(publisher, "publisher must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //INNER JOIN publishers USING (publisher_id)
        Join<Books, Publishers> booksToPublishers = booksTable.join(Books_.publisherId, JoinType.INNER);

        //WHERE LOWER(publishers.name) = LOWER(:publisher) AND b.removed = false
        Predicate notRemoved = cBuilder.equal(booksTable.get(Books_.removed), false);
        Predicate publisherMatch = cBuilder.equal(booksToPublishers.get(Publishers_.publisherId), publisher.getPublisherId());
        cQuery.where(notRemoved, publisherMatch);

        //Generate query and execute
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        List<Books> resultList = tq.getResultList();

        return resultList;
    }

    /**
     * Returns a list of Books whose publishers' names are similar to the given
     * search terms. Case insensitive.
     *
     * Native SQL Example Query: - SELECT * FROM books INNER JOIN publishers
     * USING (publisher_id) WHERE LOWER(publishers.name) LIKE
     * LOWER("McGraw-Hill") AND books.removed = false;
     *
     * @param publisher The search terms.
     *
     * @return A List of Books.
     */
    public List<Books> getBooksByPublisherHint(String publisher) {
        Objects.requireNonNull(publisher, "publisher must be set");

        if (publisher.isBlank()) {
            return findBooksEntities();
        }
        publisher = publisher.strip();

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //INNER JOIN publishers USING (publisher_id)
        Join<Books, Publishers> booksToPublishers = booksTable.join(Books_.publisherId, JoinType.INNER);

        //WHERE LOWER(publishers.name) LIKE LOWER(:publisher) AND books.removed = false
        Predicate notRemoved = cBuilder.equal(booksTable.get(Books_.removed), false);
        Expression<String> publisherNameLower = cBuilder.lower(booksToPublishers.get(Publishers_.name));
        Predicate publisherLike = cBuilder.like(publisherNameLower.as(String.class), "%" + publisher.toLowerCase() + "%");
        cQuery.where(notRemoved, publisherLike);

        //Generate query and execute
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        List<Books> resultList = tq.getResultList();

        return resultList;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///////////// 3 MOST RECENTLY-ADDED ////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Returns a list of the 3 books most recently added to the store.
     *
     * Native SQL Example Query: - SELECT * FROM books WHERE removed = false
     * ORDER BY acquired_stamp DESC LIMIT 3;
     *
     * @return A List of Books.
     */
    public List<Books> getMostRecentlyAdded() {
        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> bookTable = cQuery.from(Books.class);

        //WHERE removed = false ORDER BY acquired_stamp DESC
        Predicate notRemoved = cBuilder.equal(bookTable.get(Books_.removed), false);
        cQuery.where(notRemoved);
        cQuery.orderBy(cBuilder.desc(bookTable.get(Books_.acquiredStamp)));

        //LIMIT 3
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        tq.setMaxResults(3);

        //Execute
        List<Books> resultList = tq.getResultList();

        return resultList;
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////// BOOKS ON SALE /////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Returns a list of all books that are currently on sale.
     *
     * 'On sale' here requires the sale price to be greater than zero, less than
     * the list price, and greater than the wholesale price.
     *
     * Native SQL Example Query: - SELECT * FROM books WHERE removed = false AND
     * (sale_price > 0 AND sale_price < list_price AND sale_price >
     * wholesale_price);
     *
     * @return A List of Books.
     */
    public List<Books> getBooksOnSale() {
        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //WHERE removed = false AND (sale_price > 0 AND sale_price < list_price AND sale_price > wholesale_price)
        Predicate notRemoved = cBuilder.equal(booksTable.get(Books_.removed), false);
        Predicate saleNotZero = cBuilder.gt(booksTable.get(Books_.salePrice), 0);
        Predicate saleLessThanList = cBuilder.lt(booksTable.get(Books_.salePrice), booksTable.get(Books_.listPrice));
        Predicate saleGreaterThanWholesale = cBuilder.ge(booksTable.get(Books_.salePrice), booksTable.get(Books_.wholesalePrice));
        cQuery.where(notRemoved, saleNotZero, saleLessThanList, saleGreaterThanWholesale);

        //Generate query and execute for ALL results
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        List<Books> resultList = tq.getResultList();

        return resultList;
    }

    ///////////////////////////////
    /////// BOOKS BY FORMAT ///////
    ///////////////////////////////
    /**
     * Fetches a List of all books available in a given format. Accepts either a
     * name or extension. Case insensitive. Supports pagination.
     *
     * Native SQL Example Query: - SELECT * FROM books JOIN bookformat USING
     * (isbn) JOIN formats USING (format_id) WHERE (formats.name LIKE "Epub" OR
     * formats.extension LIKE ".epub");
     *
     * @param format A Format object.
     *
     * @return A List of Books available in the given format.
     *
     * @author Eira Garrett
     */
    public List<Books> getBooksByFormat(Formats format) {
        Objects.requireNonNull(format, "format must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //JOIN bookformat USING (isbn) JOIN formats USING (format_id)
        Join<Books, Bookformat> bookFormat = booksTable.join(Books_.bookformatList);
        Join<Bookformat, Formats> bookformatToFormat = bookFormat.join(Bookformat_.formatId);

        //WHERE (formats.name LIKE :name OR formats.extension LIKE :name)
        cQuery.where(
                cBuilder.equal(bookformatToFormat.get(Formats_.formatId), format.getFormatId()),
                cBuilder.isFalse(booksTable.get(Books_.removed))
        );
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    ////////////////////////////////////
    //////// BOOKS BY ORDER ////////////
    ////////////////////////////////////
    /**
     * Get a list of all books in a given order.
     *
     * Native SQL Example Query: - SELECT * FROM books JOIN orderitems USING
     * (isbn) JOIN orders USING (order_id) WHERE orders.order_id = 1;
     *
     * @param order The order object.
     *
     * @return A list of Books associated with the order in question.
     *
     * @author Eira Garrett
     */
    public List<Books> getBooksByOrder(Orders order) {
        Objects.requireNonNull(order, "order must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //JOIN orderitems USING (isbn) JOIN orders USING (order_id)
        Join<Books, Orderitems> booksToOrderItems = booksTable.join(Books_.orderitemsList);
        Join<Orderitems, Orders> booksToOrders = booksToOrderItems.join(Orderitems_.orderId);

        //WHERE orders.order_id = :id AND orderitems.removed = false
        cQuery.where(
                cBuilder.equal(booksToOrders.get(Orders_.orderId), order.getOrderId()),
                cBuilder.isFalse(booksToOrderItems.get(Orderitems_.removed))
        );

        TypedQuery<Books> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Get a list of all books in a given order.
     *
     * Native SQL Example Query: - SELECT * FROM books JOIN orderitems USING
     * (isbn) JOIN orders USING (order_id) WHERE orders.order_id = 1;
     *
     * @param orderId The ID number of the order.
     *
     * @return A list of Books associated with the order in question.
     *
     * @author Eira Garrett
     */
    public List<Books> getBooksByOrder(Integer orderId) {
        Objects.requireNonNull(orderId, "orderId must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //JOIN orderitems USING (isbn) JOIN orders USING (order_id)
        Join<Books, Orderitems> booksToOrderItems = booksTable.join(Books_.orderitemsList);
        Join<Orderitems, Orders> booksToOrders = booksToOrderItems.join(Orderitems_.orderId);

        //WHERE orders.order_id = :id
        cQuery.where(
                cBuilder.equal(booksToOrders.get(Orders_.orderId), orderId),
                cBuilder.isFalse(booksToOrderItems.get(Orderitems_.removed))
        );
        TypedQuery<Books> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /////////////////////////////////////////////
    ////////// BOOKS BY DESCRIPTION /////////////
    /////////////////////////////////////////////
    /**
     * Gets a list of non-removed books containing the search terms in their
     * description. Case insensitive. May return an empty list.
     *
     * @param searchTerm The search terms.
     *
     * @return A List of Books containing the search terms in their description,
     * if any.
     *
     * @author Eira Garrett
     */
    public List<Books> getBooksByDescription(String searchTerm) {
        Objects.requireNonNull(searchTerm, "searchTerm must be set");

        if (searchTerm.isBlank()) {
            return findBooksEntities();
        }
        searchTerm = searchTerm.strip();

        //SELECT * FROM BOOKS
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //WHERE description LIKE :searchTerm
        cQuery.where(
                cBuilder.like(
                        cBuilder.lower(booksTable.get(Books_.description)),
                        "%" + searchTerm.toLowerCase() + "%"
                ),
                cBuilder.isFalse(booksTable.get(Books_.removed))
        );

        TypedQuery<Books> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /////////////////////////////////////////////
    ///////////// BOOKS BY USER /////////////////
    /////////////////////////////////////////////
    /**
     * Gets a list of books purchased by a given user. May return an empty list.
     * It returns all books, including the ones marked as 'removed'.
     *
     * Native SQL Example Query: - SELECT * FROM users JOIN orderitems USING
     * (isbn) JOIN orders USING (order_id) JOIN users USING (user_id) WHERE
     * users.user_id = 1;
     *
     * @param user A Users object, with the id set.
     *
     * @return Any books purchased by the user.
     *
     * @author Eira Garrett
     */
    public List<Books> getBooksBoughtByUser(Users user) {
        Objects.requireNonNull(user, "user must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //JOIN orderitems USING (isbn) JOIN orders USING (order_id) JOIN users USING (user_id)
        Join<Books, Orderitems> booksToOrderItems = booksTable.join(Books_.orderitemsList);
        Join<Orderitems, Orders> orderitemsToOrders = booksToOrderItems.join(Orderitems_.orderId);
        Join<Orders, Users> booksToUsers = orderitemsToOrders.join(Orders_.userId);

        //WHERE users.user_id = :id AND orderitems.removed = false
        cQuery.where(
                cBuilder.equal(booksToUsers.get(Users_.userId), user.getUserId())
        );

        TypedQuery<Books> tq = entityManager.createQuery(cQuery);
        return tq.getResultList();
    }

    /**
     * Gets a list of books purchased by a given user. May return an empty list.
     * It returns all books, including the ones marked as 'removed'.
     *
     * Native SQL Example Query: - SELECT * FROM users JOIN orderitems USING
     * (isbn) JOIN orders USING (order_id) JOIN users USING (user_id) WHERE
     * users.user_id = 1;
     *
     * @param userId The ID of a given user.
     *
     * @return Any books purchased by the user.
     *
     * @author Eira Garrett
     */
    public List<Books> getBooksBoughtByUser(Integer userId) {
        Objects.requireNonNull(userId, "userId must be set");

        //SELECT * FROM books
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Books> cQuery = cBuilder.createQuery(Books.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        //JOIN orderitems USING (isbn) JOIN orders USING (order_id) JOIN users USING (user_id)
        Join<Books, Orderitems> booksToOrderItems = booksTable.join(Books_.orderitemsList);
        Join<Orderitems, Orders> orderitemsToOrders = booksToOrderItems.join(Orderitems_.orderId);
        Join<Orders, Users> booksToUsers = orderitemsToOrders.join(Orders_.userId);

        //WHERE users.user_id = :id AND orderitems.removed = false
        cQuery.where(
                cBuilder.equal(booksToUsers.get(Users_.userId), userId)
        );

        TypedQuery<Books> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }

    /**
     * Returns a list of all books in a given genre, ordered by max sales in
     * that genre.
     *
     * @param name
     * @return
     *
     * @author Eira Garrett
     */
    public List<Books> getTopSellersByGenre(String name) throws NonUniqueResultException, NoResultException {
        Objects.requireNonNull(name, "The genre name must not be null.");

        List<Books> getByGenre = getBooksByGenreHint(name);

        HashMap<Books, BigDecimal> set = new HashMap<>();

        getByGenre.forEach(book -> {
            set.put(book, getTotalSalesByBook(book));
        });

        List<Map.Entry<Books, BigDecimal>> list
                = new LinkedList<>(set.entrySet());

        // Sort the list 
        Collections.sort(list, (Map.Entry<Books, BigDecimal> o1, Map.Entry<Books, BigDecimal> o2) -> (o2.getValue()).compareTo(o1.getValue()));

        HashMap<Books, BigDecimal> temp = new LinkedHashMap<>();
        for (Map.Entry<Books, BigDecimal> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }

        return new ArrayList<>(temp.keySet());
    }

    /**
     * A helper method to total all sales of a given book.
     *
     * @param book
     * @return
     *
     * @author Eira Garrett
     */
    private BigDecimal getTotalSalesByBook(Books book) {
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<BigDecimal> cQuery = cBuilder.createQuery(BigDecimal.class);
        Root<Books> booksTable = cQuery.from(Books.class);

        Join<Books, Orderitems> booksToOrderitems = booksTable.join(Books_.orderitemsList);

        cQuery.groupBy(booksTable.get(Books_.isbn));
        cQuery.having(cBuilder.equal(booksTable.get(Books_.isbn), book.getIsbn()));

        cQuery.select(cBuilder.sum(booksToOrderitems.get(Orderitems_.price)));

        TypedQuery<BigDecimal> tq = entityManager.createQuery(cQuery);

        try {
            return tq.getSingleResult();
        } catch (NonUniqueResultException | NoResultException ex) {
            LOG.warn(book.getTitle() + " has not been sold once. Returning zero");
            return BigDecimal.ZERO;
        }

    }
}
