package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Bookformat;
import com.teama1.ga1w20.persistence.entities.Books;
import com.teama1.ga1w20.persistence.entities.Formats;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.IllegalOrphanException;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class BookformatJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(BookformatJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Bookformat bookformat) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Books isbn = bookformat.getIsbn();
            if (isbn != null) {
                isbn = entityManager.getReference(isbn.getClass(), isbn.getIsbn());
                bookformat.setIsbn(isbn);
            }
            Formats formatId = bookformat.getFormatId();
            if (formatId != null) {
                formatId = entityManager.getReference(formatId.getClass(), formatId.getFormatId());
                bookformat.setFormatId(formatId);
            }
            entityManager.persist(bookformat);
            if (isbn != null) {
                isbn.getBookformatList().add(bookformat);
                isbn = entityManager.merge(isbn);
            }
            if (formatId != null) {
                formatId.getBookformatList().add(bookformat);
                formatId = entityManager.merge(formatId);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding a record in Bookformat: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findBookformat(bookformat.getBookformatId()) != null) {
                LOG.error("Bookformat with id " + bookformat.getBookformatId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Bookformat bookformat) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Bookformat persistentBookformat = entityManager.find(Bookformat.class, bookformat.getBookformatId());
            Books isbnOld = persistentBookformat.getIsbn();
            Books isbnNew = bookformat.getIsbn();
            Formats formatIdOld = persistentBookformat.getFormatId();
            Formats formatIdNew = bookformat.getFormatId();
            if (isbnNew != null) {
                isbnNew = entityManager.getReference(isbnNew.getClass(), isbnNew.getIsbn());
                bookformat.setIsbn(isbnNew);
            }
            if (formatIdNew != null) {
                formatIdNew = entityManager.getReference(formatIdNew.getClass(), formatIdNew.getFormatId());
                bookformat.setFormatId(formatIdNew);
            }
            bookformat = entityManager.merge(bookformat);
            if (isbnOld != null && !isbnOld.equals(isbnNew)) {
                isbnOld.getBookformatList().remove(bookformat);
                isbnOld = entityManager.merge(isbnOld);
            }
            if (isbnNew != null && !isbnNew.equals(isbnOld)) {
                isbnNew.getBookformatList().add(bookformat);
                isbnNew = entityManager.merge(isbnNew);
            }
            if (formatIdOld != null && !formatIdOld.equals(formatIdNew)) {
                formatIdOld.getBookformatList().remove(bookformat);
                formatIdOld = entityManager.merge(formatIdOld);
            }
            if (formatIdNew != null && !formatIdNew.equals(formatIdOld)) {
                formatIdNew.getBookformatList().add(bookformat);
                formatIdNew = entityManager.merge(formatIdNew);
            }
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException
                | HeuristicRollbackException | NotSupportedException
                | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the record in Bookformat: " + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = bookformat.getBookformatId();
            if (findBookformat(id) == null) {
                LOG.error("The bookformat with id " + id + " does not exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws IllegalOrphanException,
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Bookformat bookformat;
            try {
                bookformat = entityManager.getReference(Bookformat.class, id);
                bookformat.getBookformatId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bookformat with id " + id + " does not exist.", enfe);
            }
            Books isbn = bookformat.getIsbn();
            if (isbn != null) {
                isbn.getBookformatList().remove(bookformat);
                isbn = entityManager.merge(isbn);
            }
            Formats formatId = bookformat.getFormatId();
            if (formatId != null) {
                formatId.getBookformatList().remove(bookformat);
                formatId = entityManager.merge(formatId);
            }
            entityManager.remove(bookformat);
            userTransaction.commit();

        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException | SecurityException
                | HeuristicMixedException | HeuristicRollbackException | NotSupportedException
                | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the record in Bookformat: " + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findBookformat(id) == null) {
                LOG.error("The bookformat with id " + id + " does not exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Bookformat> findBookformatEntities() {
        return findBookformatEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Bookformat> findBookformatEntities(int maxResults, int firstResult) {
        return findBookformatEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Bookformat> findBookformatEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Bookformat> cq = entityManager.getCriteriaBuilder().createQuery(Bookformat.class);
        cq.select(cq.from(Bookformat.class));
        TypedQuery<Bookformat> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Bookformat findBookformat(Integer id) {
        return entityManager.find(Bookformat.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getBookformatCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Bookformat> rt = cq.from(Bookformat.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

}
