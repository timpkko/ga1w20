package com.teama1.ga1w20.persistence.jpacontrollers;

import com.teama1.ga1w20.persistence.entities.Rssfeeds;
import com.teama1.ga1w20.persistence.entities.Rssfeeds_;
import com.teama1.ga1w20.persistence.jpacontrollers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
@Named
@SessionScoped
public class RssfeedsJpaController implements Serializable {

    @Transient
    private final static Logger LOG = LoggerFactory.getLogger(RssfeedsJpaController.class);

    @Resource
    private UserTransaction userTransaction;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void create(Rssfeeds rssfeeds) throws
            IllegalStateException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            entityManager.persist(rssfeeds);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("Exception thrown when adding the rssfeed: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            if (findRssfeeds(rssfeeds.getFeedId()) != null) {
                LOG.error("rssfeed " + rssfeeds.getFeedId() + " already exists.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void edit(Rssfeeds rssfeeds) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            rssfeeds = entityManager.merge(rssfeeds);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when editing the rssfeed: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            Integer id = rssfeeds.getFeedId();
            if (findRssfeeds(id) == null) {
                throw new NonexistentEntityException("The rssfeeds with id " + id + " does not exist.");
            }

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }
        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public void destroy(Integer id) throws
            NonexistentEntityException, SecurityException,
            NotSupportedException, RollbackException, SystemException,
            HeuristicMixedException, HeuristicRollbackException {
        try {
            userTransaction.begin();
            Rssfeeds rssfeeds;
            try {
                rssfeeds = entityManager.getReference(Rssfeeds.class, id);
                rssfeeds.getFeedId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rssfeeds with id "
                        + id + " does not exist. " + enfe.getMessage() == null ? "" : enfe.getMessage());
            }
            entityManager.remove(rssfeeds);
            userTransaction.commit();
        } /*If an exception happens, log and try to rollback the transaction.
        If the rollback creates a second exception, log that exception
        and throw the original one. In all cases, the original exception is 
        thrown.*/ catch (NonexistentEntityException | IllegalStateException
                | SecurityException | HeuristicMixedException
                | HeuristicRollbackException | NotSupportedException
                | RollbackException | SystemException ex) {

            LOG.error("Exception thrown when deleting the rssfeed: "
                    + (ex.getMessage() == null ? "" : ex.getMessage()));

            try {
                userTransaction.rollback();
                LOG.error("Rollback after exception : Successful.");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                LOG.error("Rollback after exception : NOT Successful!");
            } finally {
                throw ex;
            }

        }
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Rssfeeds> findRssfeedsEntities() {
        return findRssfeedsEntities(true, -1, -1);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public List<Rssfeeds> findRssfeedsEntities(int maxResults, int firstResult) {
        return findRssfeedsEntities(false, maxResults, firstResult);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    private List<Rssfeeds> findRssfeedsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery<Rssfeeds> cq = entityManager.getCriteriaBuilder().createQuery(Rssfeeds.class);
        cq.select(cq.from(Rssfeeds.class));
        TypedQuery<Rssfeeds> q = entityManager.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public Rssfeeds findRssfeeds(Integer id) {
        return entityManager.find(Rssfeeds.class, id);
    }

    /**
     * Auto-generated + personally adapted to web application
     *
     * @author Camillia Elachqar
     */
    public int getRssfeedsCount() throws NonUniqueResultException, NoResultException {
        CriteriaQuery<Long> cq = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<Rssfeeds> rt = cq.from(Rssfeeds.class);
        cq.select(entityManager.getCriteriaBuilder().count(rt));
        TypedQuery<Long> q = entityManager.createQuery(cq);
        return q.getSingleResult().intValue();
    }

    ////////////////////////////////////
    ////////// ACTIVE FEEDS ////////////
    ////////////////////////////////////
    /**
     * Gets a List of all currently-active RSS feeds.
     *
     * Native SQL Example Query: - SELECT * FROM rssfeeds WHERE active = true;
     *
     * @return A List of Rssfeeds objects.
     *
     * @author Eira Garrett
     */
    public List<Rssfeeds> getActiveFeeds() {
        //SELECT * FROM rssfeeds
        CriteriaBuilder cBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Rssfeeds> cQuery = cBuilder.createQuery(Rssfeeds.class);
        Root<Rssfeeds> feedsTable = cQuery.from(Rssfeeds.class);

        //WHERE active = true
        cQuery.where(cBuilder.isTrue(feedsTable.get(Rssfeeds_.active)));

        TypedQuery<Rssfeeds> tq = entityManager.createQuery(cQuery);

        return tq.getResultList();
    }
}
